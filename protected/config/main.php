<?php
  return array(

	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Pro arenda',
	'defaultController'=>'index',
    'sourceLanguage' => 'ru',
    'language' => 'en',

	'import'=>array(
		'application.models.*',
		'application.components.*',
	),
	'modules'=>array(
	  'gii'=>array(
		  'class'=>'system.gii.GiiModule',
		  'password'=>'123',
		  // 'ipFilters'=>array(…список IP…),
		  // 'newFileMode'=>0666,
		  // 'newDirMode'=>0777,
	  ),
	),

	'components'=>array(
		'clientScript' => array(
			'class' => 'CClientScript',
			'scriptMap' => array(
				'jquery.js' => '/js/jquery-1.8.3.js',
             ),
			'packages' => array(
				'Jquery' => array(
					'baseUrl' => 'js',
					'js' => array('jquery-1.8.3.js'),
				),
				'Base' => array(
					'depends' => array('BaseCss', 'BaseJs'),
				),
				'BaseCss' => array(
					'baseUrl' => 'css',
					'css' => array('reset.css', 'style.css'),

				),
				'BaseJs' => array(
					'baseUrl' => 'js',
					'js' => array('jquery.lightbox_me.js', 'client.js', 'new.js', 'functions.js'),
					'depends' => array('Jquery'),
				),

				'jquery-ui' => array(
					'baseUrl' => 'css',
					'css' => array( 'jquery-ui.css'),
					'depends' => array('jquery-ui-js'),
				),
				'jquery-ui-js' => array(
					'baseUrl' => '',
					'js' => array('js/jquery-ui.js'),
					'depends' => array('Jquery'),
				),

				'registration' => array(
					'baseUrl' => 'js',
					'js' => array('registration.js'),
					'depends' => array('BaseJs'),
				),

				'jquery.cookie' => array(
					'baseUrl' => 'js',
					'js' => array('jquery.cookie.js'),
					'depends' => array('BaseJs'),
				),

				'landing_start' => array(
					'baseUrl' => 'css',
					'css' => array('landing.css'),
				),

				'CarouselCss' => array(
					'baseUrl' => 'css',
					'css' => array('carousel.css', 'jquery.ui.css', 'jquery.ui.css'),
					'depends' => array( 'BaseCss'),

				),
				'CarouselJs' => array(
					'baseUrl' => 'js',
					'js' => array('jquery.jcarousel.js', 'main02.js'),
					'depends' => array( 'BaseJs'),
				),


			),
		),

	    'db'=>array(
		    'class'=>'application.extensions.PHPPDO.CPdoDbConnection',					
            'connectionString'=>'mysql:host=localhost;dbname=comarenda',
            #'class'=>'system.db.CDbConnection',            
			#'connectionString'=>'mysql:unix_socket=/var/run/mysqld/www-data-socket5.0;dbname=proarenda',			
            'username'=>'root',
            'password'=>'root',
            'charset'=>'utf8',
			'schemaCachingDuration'=>3600,
		),

	    'urlManager'=>array(
            'showScriptName'=>false,
            'urlFormat'=>'path',
            'rules'=>array(
	            'page/agent'=>array('Manual_pages/For_estate_agency', 'urlSuffix'=>'.html'),
                'page/<name>'=>array('page/view', 'urlSuffix'=>'.html'),
                'cabinet'=>array('cabinet/view', 'urlSuffix'=>'.html'),
                'how_work'=>array('Manual_pages/Howwork', 'urlSuffix'=>'.html'),
                'registration'=>array('registration/view', 'urlSuffix'=>'.html'),
                'exit'=>array('authorization/logout', 'urlSuffix'=>'.cgi'),
                'person/<id>'=>array('person/card', 'urlSuffix'=>'.html'),
                'person/<id>/my-objects'=>array('person/objects', 'urlSuffix'=>'.html')
            ),
        ),

        'request'=>array(
            'enableCookieValidation'=>true,
        ),

		'user'=>array(
			'loginUrl'=>array('authorization/login'),
			#'allowAutoLogin'=>true,
		),

		#'errorHandler'=>array(
        #    'errorAction'=>'error/error',
        #),

		/*'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),*/

	),
	
	'params'=>array(
		'web_root'=>$_SERVER['DOCUMENT_ROOT'],
//                'site_base'=>'http://project.tuning-star.ru/',
		'site_base'=>'http://com-arenda.loc/',
		'admin_mail'=>'info@com-arenda.ru',
		'autoscriptum'=>'<br /><br /><br /><hr><br />Это письмо сформировано автоматически и не требует ответа.<br />По имеющимся вопросам просьба писать по адресу office@com-arenda.ru<br />Благодарим за использование нашего ресурса<br />Администрация сайта Com-Arenda.ru',
	),	

  );
?>
