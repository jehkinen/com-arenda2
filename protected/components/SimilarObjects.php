<?php class SimilarObjects extends CWidget{

	public $district;
	public $price;
	public $square;
	public $kind;
	public $selfId;

	public function run(){		$criteria = new CDbCriteria;
		$criteria->alias = 'I';
		$criteria->select = 'I.id, I.kind, C.shname as "class", I.special, I.city, S.name as "street", I.house, CAST(I.total_item_sqr as UNSIGNED) as "total_item_sqr", CAST(I.price as UNSIGNED) as "price", (Select COALESCE(F.name_gallery, "") from images F where F.id_item=I.id order by F.id asc limit 1) as "hasFoto"';
		$criteria->join = 'join streets S on S.id=I.street join classes C on C.id=I.class';
		$criteria->condition = 'I.id <> :si and I.kind=:kn and I.district=:ds and I.status=6 and itemType<>1 and (I.house is not null and I.house<>"") and I.price between :minp and :maxp and I.total_item_sqr between :mins and :maxs';
		//$criteria->condition = 'I.id<>:si and I.kind=:kn and I.district=:ds and I.status=6 and (I.house is not null and I.house<>"") and I.price between :minp and :maxp and I.total_item_sqr between :mins and :maxs';
		// (Select COALESCE(MAX(F.id), 0) from images F where F.id_item=I.id)
		$criteria->params = array(':si'=>$this->selfId, ':kn'=>$this->kind, ':ds'=>$this->district, ':minp'=>$this->price*0.8, ':maxp'=>$this->price*1.2, ':mins'=>$this->square*0.8, ':maxs'=>$this->square*1.2);
		$criteria->order = 'I.special desc, RAND()'; // , I.created desc';
		$criteria->limit = 6;
		//CVarDumper::Dump($criteria, 3, true); die();
		$Objs = Search::model()->findAll($criteria);
		switch ($this->kind) {
			case 1 : {$s = 'Склад / производство'; break;}
			case 2 : {$s = 'Торговая площадь'; break;}
			default: $s = 'Офис';
		}
		if (!empty($Objs))			$this->render('similar_objects', array('items'=>$Objs, 'lbKind'=>$s));
	}
}?>