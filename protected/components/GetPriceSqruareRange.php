<?php class GetPriceSqruareRange extends CWidget{
	public $maxPrice;
	public $minPrice;
	public $maxSquare;
	public $minSquare;

	public function run(){
		$criteria = new CDbCriteria;
		$criteria->select = 'CAST(MAX(price) as UNSIGNED) as "maxPrice", CAST(MIN(price) as UNSIGNED) as "minPrice", CAST(MAX(total_item_sqr) as UNSIGNED) as "maxSquare", CAST(MIN(total_item_sqr) as UNSIGNED) as "minSquare"';
		$criteria->condition = 'status=6 and itemType<>1';
		$Row = ItemDetail::model()->find($criteria);
		if (empty($Row)) {
			$this->maxPrice = $this->minPrice = $this->maxSquare = $this->minSquare = 0;
		} else {
			$this->maxPrice = $Row->maxPrice;
			$this->minPrice = $Row->minPrice;
			$this->maxSquare = $Row->maxSquare;
			$this->minSquare = $Row->minSquare;
		}	}
}?>