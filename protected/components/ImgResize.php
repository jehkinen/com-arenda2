<?php
  class ImgResize extends CWidget{
    public $return;
    public $src;
    public $dest;
    public $width;
    public $height;
    public $rgb = 0xffffff;
    public $quality = 80;
    public function run(){
      $size = getimagesize($this->src);
      if ($size){        $format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
        $icfunc = 'imagecreatefrom'.$format;
        if (function_exists($icfunc)){
          $x_ratio = $this->width / $size[0];
          $y_ratio = $this->height / $size[1];
          $ratio = min($x_ratio, $y_ratio);
          $use_x_ratio = ($x_ratio == $ratio);
		  /*
          $new_width   = $use_x_ratio  ? $this->width  : floor($size[0] * $ratio);
          $new_height  = !$use_x_ratio ? $this->height : floor($size[1] * $ratio);
		  */
          $new_width   = floor($size[0] * $ratio);
          $new_height  = floor($size[1] * $ratio);
          //$new_left    = $use_x_ratio  ? 0 : floor(($this->width - $new_width) / 2);
          //$new_top     = !$use_x_ratio ? 0 : floor(($this->height - $new_height) / 2);
          $isrc = $icfunc($this->src);
          //$idest = imagecreatetruecolor($this->width, $this->height);
          $idest = imagecreatetruecolor($new_width, $new_height);
          imagefill($idest, 0, 0, $this->rgb);
          //imagecopyresampled($idest, $isrc, $new_left, $new_top, 0, 0, $new_width, $new_height, $size[0], $size[1]);
          imagecopyresampled($idest, $isrc, 0, 0, 0, 0, $new_width, $new_height, $size[0], $size[1]);
          $createfunc = 'image'.$format;
          if (function_exists($createfunc)){          	if ($createfunc == 'imagepng') $this->quality = 7;
          	$createfunc($idest, $this->dest, $this->quality);          }
          imagedestroy($isrc);
          imagedestroy($idest);
          $this->return = true;
        }else
          $this->return = false;      }
    }
  }
?>