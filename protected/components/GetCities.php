<?php class GetCities extends CWidget{

	public $sel_name;

	public function run(){
		$criteria = new CDbCriteria;
		$criteria->condition = 'id is not null';
		$criteria->order = 'name';
		$cities = City::model()->findAll($criteria);
		$this->render('get_menu_city', array('items'=>$cities, 'active'=>$this->sel_name));
	}
}?>