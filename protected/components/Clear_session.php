<?php class Clear_session extends CWidget{
	public function run(){		$usr = Yii::app()->user;

		unset($usr->ses_parent);
		unset($usr->ses_hasparent);
		unset($usr->ses_kind);
		unset($usr->ses_itype);
		unset($usr->ses_city);
		unset($usr->ses_struc);
		unset($usr->ses_func);
		unset($usr->ses_class);
		unset($usr->ses_name);
		unset($usr->ses_distr);
		unset($usr->ses_strname);
		unset($usr->ses_house);
		unset($usr->ses_housing);
		unset($usr->ses_storey);
		unset($usr->ses_storeys);
		unset($usr->ses_cconds);
		unset($usr->ses_about);
		unset($usr->ses_price);
		unset($usr->ses_putils);
		unset($usr->ses_tisqr);
		unset($usr->ses_slift);
		unset($usr->ses_inet);
		unset($usr->ses_providers);
		unset($usr->ses_telephony);
		unset($usr->ses_tcomp);
		unset($usr->ses_cellh);
		unset($usr->ses_workh);
		unset($usr->ses_cathead);
		unset($usr->ses_fload);
		unset($usr->ses_epower);
		unset($usr->ses_gatesq);
		unset($usr->ses_gatesh);
		unset($usr->ses_repair);
		unset($usr->ses_park);
		unset($usr->ses_fixqty);
		unset($usr->ses_video);
		unset($usr->ses_access);
		unset($usr->ses_welfare);
		unset($usr->ses_heating);
		unset($usr->ses_palcap);
		unset($usr->ses_shelcap);
		unset($usr->ses_flooring);
		unset($usr->ses_ventil);
		unset($usr->ses_aircon);
		unset($usr->ses_firefi);
		unset($usr->ses_reklam);
		unset($usr->ses_rampant);
		unset($usr->ses_aways);
		unset($usr->ses_rways);
	}
}?>