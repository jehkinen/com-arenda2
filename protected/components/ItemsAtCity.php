<?php class ItemsAtCity extends CWidget{

	public function run(){		$qty = ItemDetail::model()->count('status=6 and city=:ct', array(':ct'=>Yii::app()->user->getState('glcity')));
		if (!empty($qty)) {
			$c = $qty % 10;
			if ($c>1 && $c<5)		$s = 'а';
			elseif ($qty!=11 && $c==1)	$s = '';
			else				$s = 'ов';			$this->render('items_at_city', array('count'=>$qty, 'suff'=>$s));
		}
	}
}?>