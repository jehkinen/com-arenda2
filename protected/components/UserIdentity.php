<?php
  class UserIdentity extends CUserIdentity{

	const ERROR_USER_DELETE=10;
	const ERROR_USER_LOCK=11;
	const ERROR_NO_ACTIVATE=12;
	const ERROR_FLOOD=50;

    private $_requisite = array();

    public function authenticate(){

		$ip = $_SERVER['REMOTE_ADDR'];
		$criteria = new CDbCriteria;
		$criteria->select = 'COUNT(*) as "qty"';
		$criteria->condition = 'ip=:ip and floodtime>DATE_SUB(NOW(), INTERVAL 30 MINUTE) and what=:wt';
		$criteria->params = array(':ip'=>$ip, ':wt'=>'pass');
//		$Log = FloodLog::model()->find($criteria);
//		if ($Log->qty>5) {
//			$this->errorCode = self::ERROR_FLOOD;
//			return !$this->errorCode;
//		}
			
      $record = User::model()->findByAttributes(array('username'=>$this->username));
      //$record = User::model()->find('username=:un or e_mail=:em', array(':un'=>$this->username, ':em'=>$this->username));
	  $fail = true;
      if ($record === null)
        $this->errorCode = self::ERROR_USERNAME_INVALID;

      else if ($record->password !== md5($this->password))
             $this->errorCode = self::ERROR_PASSWORD_INVALID;
           else if ($record->state<4) {
				switch ($record->state) {
					case 0 : { $this->errorCode = self::ERROR_USER_DELETE; break; }
					case 1 : { $this->errorCode = self::ERROR_USER_LOCK; break; }
					case 2 : { $this->errorCode = self::ERROR_NO_ACTIVATE; break; }
				}             
           } else {
             $this->_requisite['user_id'] = $record->id;
             $this->_requisite['user_role'] = $record->role;
             $this->_requisite['user_state'] = $record->state;
             $this->setState('name', $record->name);
             $this->setState('so_name', $record->so_name);
             $this->setState('otchestvo', $record->otchestvo);
             $this->errorCode = self::ERROR_NONE;
			 $fail = false;
           }
		if ($fail) {
			$filter = new CHtmlPurifier();
			$txt = 'Login: "'.$filter->purify($this->username).'" Password: "'.$filter->purify($this->password).'"';
			$Flood = new FloodLog();
			$Flood->what = 'pass';
			$Flood->ip = $ip;
			$Flood->txt = substr($txt, 0, 511);
			$Flood->save(false);
		}
      return !$this->errorCode;
    }

    public function getId(){
      return $this->_requisite;
    }

  }
?>