<?php class LeftNews extends CWidget{

	public $qty;

	public function run(){
		$criteria = new CDbCriteria;
		$criteria->order = 'stick desc, id desc';
		$criteria->limit = $this->qty;
		$news = News::model()->findAll($criteria);
		if (!empty($news))
			$this->render('left_news', array('items'=>$news));
	}
}?>