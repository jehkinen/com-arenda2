<?php class OftenSeek extends CWidget{

	public function run(){
		$criteria = new CDbCriteria;
		$criteria->alias = 'F';
		$criteria->select = 'D.name as "district", F.kind, F.priceMin, F.priceMax, F.squareMin, F.squareMax, COUNT(*) as "qty", MAX(F.id) as id';
		$criteria->join = 'Left join districts D on D.id=F.district';
		$criteria->condition = 'F.city=:ct';
		$criteria->params = array(':ct'=>Yii::app()->user->getState('glcity','Екатеринбург'));
		$criteria->group = 'D.name, F.kind, F.priceMin, F.priceMax, F.squareMin, F.squareMax';
		$criteria->order = 'COUNT(*) desc';
		$criteria->limit = 3;
		$rows = FindStat::model()->findAll($criteria);
		$this->render('often_seek', array('items'=>$rows));
	}
}?>