<?php class InformAboutTender extends CWidget{
	public $bidId;
	public $ownId;

	public function run() {
		// Реализация опции Кабинета Собственника "Уведомлять меня о поступлении заявок от арендаторов"
		$owner = User::model()->findByPk($this->ownId);
		if (!empty($owner) && $owner->info_order) {
			if ($owner->info_flag_email && isset($owner->info_email)) {
				// Отсылаем письмо
				$mail = new Mailer();
				$mail->IsHTML(true);
				$mail->From = Yii::app()->params->admin_mail;
				$mail->FromName = 'Уведомление';
				$mail->Subject  = 'Новая заявка арендатора';
				$s = 'Настоящим уведомляем Вас, что поступила новая заявка от арендатора.<br />Один или более из Ваших объектов подходят под ее условия.<br />Детально ознакомиться с заявкой можно по адресу:<br />'.Yii::app()->params->site_base.'bid/view/id/'.$this->bidId;
				$mail->Body = $s.Yii::app()->params->autoscriptum;
				$mail->AddAddress($owner->info_email);
				//$mail->AddAddress('dpriest@list.ru'); 
				if ($mail->Send())
					$succ = 1;
				else	$succ = 0;
				$cmd = Yii::app()->db->createCommand('Insert into log_send_messages (kind, success, reciever, address, txt) Values (1, '.$succ.', '.$this->ownId.', '."'$owner->info_email', '$s');");
				$cmd->execute();
				sleep(1); // Задержка, чтобы не обвинили в рассылке спама
				set_time_limit(10); // Продление времени работы скрипта, чтобы успеть всем разослать
			}
			if ($owner->info_flag_phone && isset($owner->info_phone)) {
				$s = 'Поступила новая заявка от арендатора.';
				if (sms('7'.$owner->info_phone, $s,'Com-Arenda'))
					$succ = 1;
				else	$succ = 0;
				$cmd = Yii::app()->db->createCommand('Insert into log_send_messages (kind, success, reciever, address, txt) Values (0, '.$succ.', '.$this->ownId.', '."'$owner->info_phone', '$s');");
				$cmd->execute();
			}
		}
	}
}?>