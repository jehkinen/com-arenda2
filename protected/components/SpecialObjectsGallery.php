<?php class SpecialObjectsGallery extends CWidget{

	public function run(){
		$criteria = new CDbCriteria;
		$criteria->alias = 'I';
		$criteria->select = 'I.id, I.kind, I.city, S.name as "street", D.name as "district", I.house, CAST(I.total_item_sqr as UNSIGNED) as "total_item_sqr", CAST(I.price as UNSIGNED) as "price", (Select COALESCE(F.name_gallery, "") from images F where F.id_item=I.id order by F.id asc limit 1) as "hasFoto"';
		$criteria->join = 'join streets S on S.id=I.street join districts D on D.id=I.district';
		$criteria->condition = 'I.special=1 and I.status=6 and itemType<>1 and (I.house is not null and I.house<>"")';// and I.city=:ct';
		// Пока не в каждом городе есть 
		// $criteria->params = array(':ct'=>Yii::app()->user->getState('glcity', 'Екатеринбург'));
		$criteria->order = 'RAND()';
		$criteria->limit = 15;
		$Objs = Search::model()->findAll($criteria);
		if (!empty($Objs))			$this->render('special_objects_gallery', array('items'=>$Objs));
	}
}?>