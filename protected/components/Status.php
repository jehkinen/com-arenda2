<?php class Status extends CWidget{

	public function run(){

		switch (Yii::app()->user->id['user_role']){
			case 'admin':{
				$status_name = 'администратор';
				$kabinet_name = 'Вход в админку';
				$kabinet_link = 'control/index';
				break;
			}
			case 'owner':{
				$status_name = 'собственник';
				$kabinet_name = 'Личный кабинет';
				$kabinet_link = 'cabinet/view';
				break;
				}
			case 'tenant':{
				$status_name = 'арендатор';
				$kabinet_name = 'Личный кабинет';
				$kabinet_link = 'cabinet/view';
				break;}
		}
		$filter = new CHtmlPurifier();
		$so_name = $filter->purify(Yii::app()->user->so_name);
		$name =  $filter->purify(Yii::app()->user->name);
		$otchestvo =  $filter->purify(Yii::app()->user->otchestvo);

		$this->render('status', array(
			'so_name'		=>$so_name,
			'name'			=>$name,
			'otchestvo'		=>$otchestvo,
			'status_name'	=>$status_name,
			'kabinet_name'	=>$kabinet_name,
			'kabinet_link'	=>$kabinet_link
		));

	}
}?>