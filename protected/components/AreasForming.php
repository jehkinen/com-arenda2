<?php
class AreasForming extends CWidget{
	public function run(){
		//Фомирование списка областей, с со связанныеми с ними городами
		$result = array();
		$areas =  Chtml::listData( Area::model()->findAll(array('order'=>"`name` ASC")), 'name', 'id');
		$all_cities = array(''=>''); //Список всех городов
		foreach($areas as $area_name=>$id){
			$area = Area::model()->findByPk($id);
			$cities = Chtml::listData($area->cities, 'name','name');
			sort($cities, SORT_NATURAL | SORT_FLAG_CASE);
			//Если у области нет ни одного города, то не показываем её
			if(count($cities)>0){

				foreach($cities as $i){
					$all_cities[$i] = $i;
				}
				$result[$area_name] = $cities;
			}
		}
		sort($all_cities, SORT_NATURAL | SORT_FLAG_CASE);
		$this->render('areas_list_popup', array('result'=>$result, 'all_cities'=>$all_cities, 'areas'=>$areas));
	}
}
?>