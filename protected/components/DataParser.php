<?php
  class DataParser extends CWidget{
    public $data_db;
    public $month;
    public $day;
    public $year;

    public function run(){
      $date_write = date('j M Y', strtotime($this->data_db));
      list($this->day, $en, $this->year) = explode(' ', $date_write);
      switch ($en){
        case 'Jan':
          $ru = 'января';
          break;
        case 'Feb':
          $ru = 'февраля';
          break;
        case 'Mar':
          $ru = 'марта';
          break;
        case 'Apr':
          $ru = 'апреля';
          break;
        case 'May':
          $ru = 'мая';
          break;
        case 'Jun':
          $ru = 'июня';
          break;
        case 'Jul':
          $ru = 'июля';
          break;
        case 'Aug':
          $ru = 'августа';
          break;
        case 'Sep':
          $ru = 'сентября';
          break;
        case 'Oct':
          $ru = 'октября';
          break;
        case 'Nov':
          $ru = 'ноября';
          break;
        case 'Dec':
          $ru = 'декабря';
          break;
        default:
          $ru = $en;
      }
      $this->month = $ru;
    }
  }
?>