<?php
  class LoadFileName extends CWidget{
    public $in_text;
    public $len;
    public $out_text;
    public function run(){
      $str = mb_strtolower($this->in_text, 'utf-8');
  	  $str = preg_replace("/[^a-zA-Zа-яА-ЯЁё0-9\s\-\_\,\.]/u", "", strip_tags($str));
      $str = preg_replace("/(\s)+/", " ", trim($str));
      $tr = array(
        "а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"e","ж"=>"j","з"=>"z",
        "и"=>"i","й"=>"i","к"=>"k","л"=>"l","м"=>"m","н"=>"n","о"=>"o","п"=>"p",
        "р"=>"r","с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h","ц"=>"c","ч"=>"ch",
        "ш"=>"sh","щ"=>"sch","ъ"=>"y","ы"=>"i","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
        "-"=>"-", " "=>"_"
      );

      $str_total = strtr($str,$tr);

      if (!empty($this->len)){        if (strlen($str_total) > $this->len){          $expansion = substr($str_total, strrpos($str_total, '.'));
          $str_total = substr($str_total, 0, $this->len).$expansion;        }      }

      $this->out_text = $str_total;
    }
  }
?>