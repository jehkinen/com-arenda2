<?php class ChangeItemStatus extends CWidget{
/* Описание статусов объекта (поле status)
0 - деактивирован администратором
1 - показы отменены
2 - сдан // временно отменен
3 - ожидает модерации
5 - приостановлены показы // временно отменен
6 - идут показы
*/
	public $itemId;

	public function run() {
		// Реализация оповещений при изменении статуса объекта
		$item = Search::model()->findByPk($this->itemId);
		if (!empty($item) && ($item->itemType!=1)) {
			@require_once('protected/extensions/mailer/class.mailer.php');
			@require_once('protected/extensions/smspilot/smspilot.php');
			switch ($item->status) {
				case 0 :
				case 1 : { // информировать меня при деактивации (сдаче) объекта из блокнота
						$criteria = new CDbCriteria;
						$criteria->alias = 'N';
						$criteria->select = 'N.tenant, U.info_deactivate as "tenInfoDeactivate", U.info_email as "tenInfoMail", U.info_flag_email as "tenInfoFlagMail", U.info_phone as "tenInfoPhone", U.info_flag_phone as "tenInfoFlagPhone"';
						$criteria->join = 'join users U on U.id=N.tenant';
						$criteria->condition = 'N.item=:id';
						$criteria->params = array(':id'=>$item->id);
						$Tenants = Notepads::model()->findAll($criteria);
						foreach ($Tenants as $t) {
							if ($t->tenInfoDeactivate) {
								if ($t->tenInfoFlagMail && isset($t->tenInfoMail)) {
									$mail = new Mailer();
									$mail->IsHTML(true);
									$mail->From = Yii::app()->params->admin_mail;
									$mail->FromName = 'Уведомление';
									$mail->Subject  = 'Объект сдан';
									$s = 'Настоящим уведомляем Вас, что размещенный в Вашем Блокноте объект '.Yii::app()->params->site_base.'object/detail/id/'.$item->id.' сдан.';
									$mail->Body = $s.Yii::app()->params->autoscriptum;
									$mail->AddAddress($t->tenInfoMail);
									//$mail->AddAddress('dpriest@list.ru'); 
									if ($mail->Send())
										$succ = 1;
									else	$succ = 0;
									$cmd = Yii::app()->db->createCommand('Insert into log_send_messages (kind, success, reciever, address, txt) Values (1, '.$succ.', '.$t->tenant.', '."'$t->tenInfoMail', '$s');");
									$cmd->execute();
									sleep(1); // Задержка, чтобы не обвинили в рассылке спама
									set_time_limit(10); // Продление времени работы скрипта, чтобы успеть всем разослать
								}
								if ($t->tenInfoFlagPhone && isset($t->tenInfoPhone)) {
									$s = 'Размещенный в Вашем Блокноте объект '.Yii::app()->params->site_base.'object/detail/id/'.$item->id.' сдан.';
									if (sms('7'.$t->tenInfoPhone, $s,'Com-Arenda'))
										$succ = 1;
									else	$succ = 0;
									$cmd = Yii::app()->db->createCommand('Insert into log_send_messages (kind, success, reciever, address, txt) Values (0, '.$succ.', '.$t->tenant.', '."'$t->tenInfoPhone', '$s');");
									$cmd->execute();
								}
							}
						}
				break;}
				case 6 : { // Статус = показывается
						// извещать меня о поступлении новых объектов (e-mail)
						// автоматически копировать подходящие объекты в блокнот
						$itemIsRelevant = false;
						$relevantNonCalculated = true;
						$Tenants = User::model()->findAll('role=:rl', array(':rl'=>'tenant'));
						foreach ($Tenants as $t) {
							// "Извещать меня о поступлении новых объектов (e-mail)"
							if ($t->info_moder) {
								$relevantNonCalculated = false;
								$itemIsRelevant = $this->RelevantToTenders($item, $t->id);
								if ($itemIsRelevant && $t->info_flag_email && isset($t->info_email)) { 
									// Отсылаем письмо
									$mail = new Mailer();
									$mail->IsHTML(true);
									$mail->From = Yii::app()->params->admin_mail;
									$mail->FromName = 'Уведомление';
									$mail->Subject  = 'Новый объект';
									$s = 'Настоящим уведомляем Вас, на нашем сайте размещен объект, удовлетворяющий Вашим критериям поиска.<br />Полная информация доступна по адресу:<br />'.Yii::app()->params->site_base.'object/detail/id/'.$item->id;
									$mail->Body = $s.Yii::app()->params->autoscriptum;
									$mail->AddAddress($t->info_email);
									//$mail->AddAddress('dpriest@list.ru'); 
									if ($mail->Send())
										$succ = 1;
									else	$succ = 0;
									$cmd = Yii::app()->db->createCommand('Insert into log_send_messages (kind, success, reciever, address, txt) Values (1, '.$succ.', '.$t->id.', '."'$t->info_email', '$s');");
									$cmd->execute();
									sleep(1); // Задержка, чтобы не обвинили в рассылке спама
									set_time_limit(10); // Продление времени работы скрипта, чтобы успеть всем разослать
								}
								if ($itemIsRelevant && $t->info_flag_phone && isset($t->info_phone)) {
									$s = 'На нашем сайте размещен объект, удовлетворяющий Вашим критериям поиска.';
									if (sms('7'.$t->info_phone, $s,'Com-Arenda'))
										$succ = 1;
									else	$succ = 0;
									$cmd = Yii::app()->db->createCommand('Insert into log_send_messages (kind, success, reciever, address, txt) Values (0, '.$succ.', '.$t->id.', '."'$t->info_phone', '$s');");
									$cmd->execute();
								}
							}
							//  "Автоматически копировать подходящие объекты в блокнот"
							if ($t->auto_notepad) { 
								// Запись в блокнот, если объект удовлетворяет условиям
								// Проверим, нет ли уже данного объекта в блокноте (например, при повторной модерации и т.п.)
								$exists = Notepads::model()->exists('tenant=:tn and item=:it', array(':tn'=>$t->id, ':it'=>$this->itemId));
								if (!$exists) {
									if ($relevantNonCalculated) {
										$relevantNonCalculated = false;
										$itemIsRelevant = $this->RelevantToTenders($item, $t->id);
									}
									if ($itemIsRelevant) {
										$NewRec = new Notepads;
										$NewRec->status = 0;
										$NewRec->item = $this->itemId;
										$NewRec->tenant = $t->id;
										$NewRec->save(false);
									}
								}
							}
						}
						// Извещать меня об успешной модерации объектов (e-mail)
						$owner = User::model()->findByPk($item->owner_id);
						if (!empty($owner) && $owner->info_moder && ($item->status_changed==null) ) {
							if ($owner->info_flag_email && isset($owner->info_email) ) {
								// Отсылаем письмо
								$mail = new Mailer();
								$mail->IsHTML(true);
								$mail->From = Yii::app()->params->admin_mail;
								$mail->Subject  = 'Модерация объекта';
								$s = 'Настоящим уведомляем Вас, что размещение объекта одобрено.<br />Посетителям объект доступен по адресу:<br />'.Yii::app()->params->site_base.'object/detail/id/'.$item->id.Yii::app()->params->autoscriptum;
								$mail->Body = $s;
								$mail->AddAddress($owner->info_email);
								//$mail->AddAddress('dpriest@list.ru'); 
								if ($mail->Send())
									$succ = 1;
								else	$succ = 0;
								$cmd = Yii::app()->db->createCommand('Insert into log_send_messages (kind, success, reciever, address, txt) Values (1, '.$succ.', '.$item->owner_id.', '."'$owner->info_email', '$s');");
								$cmd->execute();
							}
							if ($owner->info_flag_phone && isset($owner->info_phone)) {
								$s = 'Размещение объекта одобрено.';
								if (sms('7'.$owner->info_phone, $s,'Com-Arenda'))
									$succ = 1;
								else	$succ = 0;
								$cmd = Yii::app()->db->createCommand('Insert into log_send_messages (kind, success, reciever, address, txt) Values (0, '.$succ.', '.$item->owner_id.', '."'$owner->info_phone', '$s');");
								$cmd->execute();
							}
						}
				break;}
			}
		}
	}
	
	function RelevantToTenders($item, $tenId) {
		$Tenders = Tenders::model()->findAll('who_id=:wi', array(':wi'=>$tenId));
		foreach ($Tenders as $t) {
			if ( ($item->kind == $t->kind) &&
				($item->city == $t->city) &&
				(($t->district==0) || ($item->district == $t->district)) &&
				(($t->kind_structure==0) || ($item->kind_structure == $t->kind_structure)) &&
				(($t->functionality==0) || ($item->functionality == $t->functionality)) &&
				(($t->class==0) || ($item->class == $t->class)) &&
				(($t->street==0) || ($item->street == $t->street)) &&
				(($t->repair==0) || ($item->repair == $t->repair)) &&
				(($t->parking==0) || ($item->parking == $t->parking)) &&
				(($t->ventilation==0) || ($item->ventilation == $t->ventilation)) &&

				(empty($t->service_lift) || ($item->service_lift == 1)) &&
				(empty($t->internet) || ($item->internet == 1)) &&
				(empty($t->telephony) || ($item->telephony == 1)) &&
				(empty($t->firefighting) || ($item->firefighting == 1)) &&
				(empty($t->heating) || ($item->heating == 1)) &&
				(empty($t->cathead) || ($item->cathead == 1)) &&
				(empty($t->rampant) || ($item->rampant == 1)) &&
				(empty($t->autoways) || ($item->autoways == 1)) &&
				(empty($t->railways) || ($item->railways == 1)) &&

				(empty($t->gates_height1) || ($item->gates_height >= $t->gates_height1)) &&
				(empty($t->gates_height2) || ($item->gates_height <= $t->gates_height2)) &&
				(empty($t->ceiling_height1) || ($item->ceiling_height >= $t->ceiling_height1)) &&
				(empty($t->ceiling_height2) || ($item->ceiling_height <= $t->ceiling_height2)) &&
				(empty($t->priceMin) || ($item->price >= $t->priceMin)) &&
				(empty($t->priceMax) || ($item->price <= $t->priceMax)) &&
				(empty($t->squareMin) || ($item->total_item_sqr >= $t->squareMin)) &&
				(empty($t->squareMax) || ($item->total_item_sqr <= $t->squareMax)) &&
				(empty($t->storeyMin) || ($item->storey >= $t->storeyMin)) &&
				(empty($t->storeyMax) || ($item->storey <= $t->storeyMax))

			) return true;
		}
		return false;
	}
}?>