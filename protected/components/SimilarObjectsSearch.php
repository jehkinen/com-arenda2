<?php class SimilarObjectsSearch extends CWidget{

	public function run(){

		$vals = Yii::app()->user;
		if ( !(isset($vals->ses_sqr1) || isset($vals->ses_sqr2) || isset($vals->ses_prc1) || isset($vals->ses_prc2)) )
			return;
		$whereString = '(I.itemType<>1 and I.status=6 and (I.house is not null and I.house<>"")'; // Переменная для формирования WHERE-предложения sql-запроса
		$Params = array(); // Массив параметров, передаваемых в запрос

		if (isset($vals->glcity)) { // Город
			$whereString .= ' and I.city=:ct';
			$Params[':ct'] = $vals->getState('glcity');
		}
		if (isset($vals->ses_kind)) { // Тип помещения (офис/склад и т.п.
			$kind = $vals->getState('ses_kind');
		} else $kind = 0;
		$whereString .= ' and I.kind=:kd';
		$Params[':kd'] = $kind;
		if (isset($vals->ses_raion)) { // Район
			$whereString .= ' and I.district=:ds';
			$Params[':ds'] = $vals->getState('ses_raion');
		}
		$whereString .= ') and (1=2';
		if (isset($vals->ses_sqr1) && ($vals->getState('ses_sqr1')!='')) { // Площадь помещения. Минимально приемлемая
			$whereString .= ' or I.total_item_sqr between :minmins and :minmaxs';
			$Params[':minmins'] = $vals->getState('ses_sqr1')*0.8;
			$Params[':minmaxs'] = $vals->getState('ses_sqr1')-1;
		}
		if (isset($vals->ses_sqr2) && ($vals->getState('ses_sqr2')!='')) { // Площадь помещения. Максимально допустимая
			$whereString .= ' or I.total_item_sqr between :maxmins and :maxmaxs';
			$Params[':maxmins'] = $vals->getState('ses_sqr2')+1;
			$Params[':maxmaxs'] = $vals->getState('ses_sqr2')*1.2;
		}
		if (isset($vals->ses_prc1) && ($vals->getState('ses_prc1')!='')) { // Арендная ставка. Минимально приемлемая
			$whereString .= ' or I.price between :minminp and :minmaxp';
			$Params[':minminp'] = $vals->getState('ses_prc1')*0.8;
			$Params[':minmaxp'] = $vals->getState('ses_prc1')-5;
		}
		if (isset($vals->ses_prc2) && ($vals->getState('ses_prc2')!='')) { // Арендная ставка. Максимально допустимая
			$whereString .= ' or I.price between :maxminp and :maxmaxp';
			$Params[':maxminp'] = $vals->getState('ses_prc2')+5;
			$Params[':maxmaxp'] = $vals->getState('ses_prc2')*1.2;
		}
		$whereString .= ')';
		$criteria = new CDbCriteria;
		$criteria->alias = 'I';
		$criteria->select = 'I.id, I.kind, C.shname as "class", I.special, I.city, S.name as "street", I.house, CAST(I.total_item_sqr as UNSIGNED) as "total_item_sqr", CAST(I.price as UNSIGNED) as "price", (Select COALESCE(F.name_gallery, "") from images F where F.id_item=I.id order by F.id asc limit 1) as "hasFoto"';
		$criteria->join = 'join streets S on S.id=I.street join classes C on C.id=I.class';
		$criteria->condition = $whereString;
		$criteria->params = $Params;
		$criteria->order = 'I.special desc, RAND()'; // , I.created desc';
		$criteria->limit = 6;
		//CVarDumper::Dump($criteria, 3, true); die();
		$Objs = Search::model()->findAll($criteria);
		switch ($kind) {
			case 1 : {$s = 'Склад / производство'; break;}
			case 2 : {$s = 'Торговая площадь'; break;}
			default: $s = 'Офис';
		}
		if (!empty($Objs))			$this->render('similar_objects_search', array('items'=>$Objs, 'lbKind'=>$s));
	}
}?>