<?php
  class PDFCreate extends CWidget{
    public $item;
    public $images;
    public $id;
	public $forHtml;
	public $directory;

	public function run(){
		include_once('protected/extensions/mpdf53/mpdf.php');
		$bUrl = Yii::app()->baseUrl;
		$html   = '';
		$s = '';

		$yes ='<td><div class="yes"><img src="images/ico_mc_01.png" alt="" /> есть</div></td>';
		$no ='<td><div class="no"><img src="images/no.png" alt="" /> нет</div></td>';

		$i = $this->item;

		switch ($i->kind) {
			case 1 : {$lbClass = 'торгового помещения'; break;}
			case 2 : {$lbClass = 'склада/производственного помещения'; break;}
			default: $lbClass = 'офиса';
		}

		switch ($i->itemType) {
			case 0 : { // Простой объект
				switch ($i->kind) {
					case 1 : {$lbTitle = 'Торговое помещение'; break;}
					case 2 : {$lbTitle = 'Склад или производственное помещение'; break;}
					default: $lbTitle = 'Офисное помещение';
				}
				break;
			} 
			case 1 : { // Объект-контейнер
				switch ($i->kind) {
					case 1 : {$lbTitle = 'Торговый центр'; break;}
					case 2 : {$lbTitle = 'Складской или производственный центр'; break;}
					default: $lbTitle = 'Офисный центр';
				}
				$lbTitle.=' "'.$i->name.'"';
				break;
			}
			case 2 : { // Вложенный объект
				switch ($i->kind) {
					case 1 : {$lbTitle = 'Помещение в торговом центре'; break;}
					case 2 : {$lbTitle = 'Помещение в складском или производственном центре'; break;}
					default: $lbTitle = 'Офис в офисном центре';
				}
				$lbTitle.=' "'.$i->parentName.'"';
				break;
			}
		}

		$get = $this->widget('DateFormat', array('kind'=>1,'dbDate'=>$i->created));
		$html .= '<div id="header"><img style="float:right;" src="images/logo4.jpg" alt="" /></div>';
		$html .= '<div id="content">';
			$html .= '<span class="date">'.$get->date.'</span>';
			$html .= '<h1>'.$lbTitle.'</h1>';
			$html .= '<em>Аренда '.$lbClass.' класса &laquo;'.$i->class.'&raquo;</em>';
			//$html .= '<div id="price">'.(float)($i->price*$i->total_item_sqr).' руб!!.</div>';
			$html .= '<table class="price_table"><tr><td>'.(float)($i->price*$i->total_item_sqr).' руб.</td></tr></table>';

			$html .= '<div id="table"><table class="info">';
				$html .= '<tr><td>Общая площадь:</td><td class="space"></td><td>'.(float)$i->total_item_sqr.' м<sup>2</sup></td></tr>';
				$html .= '<tr><td>Контактное лицо:</td><td class="space"></td><td>'.$i->firstname.' '.$i->lastname.'.</td></tr>';
				$html .= '<tr><td>Телефоны:</td><td class="space"></td><td>'.$i->phone.'</td></tr>';
			$html .= '</table></div>';
			if (!empty($i->about))
				$html .= $i->about.'<br /><br />';
			$html .= '<div id="details-title-box"><div id="details-title">Детальная информация</div><div class="clear"></div></div>';
			$html .= '<table class="details">';
				$html.= '<tr><td>Функциональное назначение</td><td>'.$i->functionality.'</td></tr>';
				$html.= '<tr class="odd"><td>Район</td><td>'.$i->district.'</td></tr>';
				$html.= '<tr><td>Улица</td><td>'.$i->street.'</td></tr>';
				$html.= '<tr class="odd"><td>Дом</td><td>'.$i->house.'</td></tr>';
				$html.= '<tr><td>Корпус</td><td>'.$i->housing.'</td></tr>';
				$html.= '<tr class="odd"><td>Тип сооружения</td><td>'.$i->kind_structure.'</td></tr>';
				$html.= '<tr><td>Название</td><td>'.$i->name.'</td></tr>';
				$html.= '<tr class="odd"><td>Класс</td><td>'.$i->class.'</td></tr>';
				$html.= '<tr><td>Этаж</td><td>'.$i->storey.'</td></tr>';
				$html.= '<tr class="odd"><td>Этажность</td><td>'.$i->storeys.'</td></tr>';

			switch ($i->kind){
				case 0 : {
					$s.='<tr><td></td><td></td></tr>';
						$s.='<tr class="odd"><td>Коммунальные услуги</td><td>'.$i->pay_utilities.'</td></tr>';
						$s.='<tr><td>Состояние ремонта</td><td>'.$i->repair.'</td></tr>';
						$s.='<tr class="odd"><td>Покрытие пола</td><td>'.$i->flooring.'</td></tr>';
						$s.='<tr><td>Видеонаблюдение</td>';
						if($i->video==1) 	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr class="odd"><td>Система пожаротушения</td>';
						if($i->firefighting==1) 	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr><td>Пропускная система</td>';
						if($i->access==1) 	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr class="odd"><td>Система вентиляции</td><td>'.$i->ventilation.'</td></tr>';
						$s.='<tr><td>Кондиционирование</td>';
						if($i->air_condit==1) 	$s.=$yes; else	$s.=$no;
					$s.='<tr class="odd"><td></td><td></td></tr>';
						$s.='<tr><td>Форма договора</td><td>'.$i->contr_condition.'</td></tr>';
						$s.='<tr class="odd"><td>Интернет</td>';
						if($i->internet==1) 	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr><td>Провайдер</td><td>'.$i->providers.'</td></tr>';
						$s.='<tr class="odd"><td>Телефония</td>';
						if($i->telephony==1) 	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr><td>Телефонная компания</td><td>'.$i->tel_company.'</td></tr>';
						$s.='<tr class="odd"><td>Парковка</td><td>'.$i->parking.'</td></tr>';
						$s.='<tr><td>Количество закрепленных машиномест</td><td>'.$i->fixed_qty.'</td></tr>';
						$s.='<tr class="odd"><td>Возможность размещения рекламы</td>';
						if($i->can_reclame==1) 	$s.=$yes; else	$s.=$no;
						$s.='</tr>';
					break;
				}
				case 1 : {
					$s.='<tr><td></td><td></td></tr>';
						$s.='<tr class="odd"><td>Коммунальные услуги</td><td>'.$i->pay_utilities.'</td></tr>';
						$s.='<tr><td>Состояние ремонта</td><td>'.$i->repair.'</td></tr>';
						$s.='<tr class="odd"><td>Покрытие пола</td><td>'.$i->flooring.'</td></tr>';
						$s.='<tr><td>Высота потолков</td><td>'.$i->ceiling_height.'</td></tr>';
						$s.='<tr class="odd"><td>Рабочая высота помещения</td><td>'.$i->work_height.'</td></tr>';
						$s.='<tr><td>Нагрузка на пол</td><td>'.$i->floor_load.'</td></tr>';
						$s.='<tr class="odd"><td>Электр.мощность</td><td>'.$i->el_power.'</td></tr>';
					$s.='<tr><td></td><td></td></tr>';
						$s.='<tr class="odd"><td>Грузовой лифт</td>';
						if($i->service_lift==1) 	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr><td>Бытовые помещения</td><td>'.$i->welfare.'</td></tr>';
						$s.='<tr class="odd"><td>Пандус</td>';
						if($i->rampant==1) 	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr><td>Видеонаблюдение</td>';
						if($i->video==1) 	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr class="odd"><td>Система пожаротушения</td>';
						if($i->firefighting==1) 	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr><td>Система вентиляции</td><td>'.$i->ventilation.'</td></tr>';
						$s.='<tr class="odd"><td>Кондиционирование</td>';
						if($i->air_condit==1)	$s.=$yes; else	$s.=$no;
					$s.='<tr><td></td><td></td></tr>';
						$s.='<tr class="odd"><td>Форма договора</td><td>'.$i->contr_condition.'</td></tr>';
						$s.='<tr><td>Интернет</td>';
						if($i->internet==1)	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr class="odd"><td>Провайдер</td><td>'.$i->providers.'</td></tr>';
						$s.='<tr><td>Телефония</td>';
						if($i->telephony==1)	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr class="odd"><td>Телефонная компания</td><td>'.$i->tel_company.'</td></tr>';
						$s.='<tr><td>Парковка</td><td>'.$i->parking.'</td></tr>';
						$s.='<tr class="odd"><td>Количество закрепленных машиномест</td><td>'.$i->fixed_qty.'</td></tr>';
						$s.='<tr><td>Возможность размещения рекламы</td>';
						if($i->can_reclame==1)	$s.=$yes; else	$s.=$no;
						$s.='</tr>';
					break;
				}
				case 2 : {
					$s.='<tr><td></td><td></td></tr>';
						$s.='<tr class="odd"><td>Коммунальные услуги</td><td>'.$i->pay_utilities.'</td></tr>';
						$s.='<tr><td>Состояние ремонта</td><td>'.$i->repair.'</td></tr>';
						$s.='<tr class="odd"><td>Покрытие пола</td><td>'.$i->flooring.'</td></tr>';
						$s.='<tr><td>Высота потолков</td><td>'.$i->ceiling_height.'</td></tr>';
						$s.='<tr class="odd"><td>Рабочая высота помещения</td><td>'.$i->work_height.'</td></tr>';
						$s.='<tr><td>Нагрузка на пол</td><td>'.$i->floor_load.'</td></tr>';
						$s.='<tr class="odd"><td>Электр.мощность</td><td>'.$i->el_power.'</td></tr>';
						$s.='<tr><td>Системы отопления и водоснабжения</td>';
						if($i->heating==1) 	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr class="odd"><td class="td_01">Пропускная система</td>';
						if($i->access==1) 	$s.=$yes; else	$s.=$no;
					$s.='</tr><tr><td></td><td></td></tr>';
						$s.='<tr class="odd"><td>Грузовой лифт</td>';
						if($i->service_lift==1) 	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr><td>Бытовые помещения</td><td>'.$i->welfare.'</td></tr>';
						$s.='<tr class="odd"><td>Пандус</td>';
						if($i->rampant==1) 	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr><td>Видеонаблюдение</td>';
						if($i->video==1) 	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr class="odd"><td>Система пожаротушения</td>';
						if($i->firefighting==1) 	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr><td>Система вентиляции</td><td>'.$i->ventilation.'</td></tr>';
						$s.='<tr class="odd"><td>Количество ворот</td><td>'.$i->gates_qty.'</td></tr>';
						$s.='<tr><td>Высота ворот</td><td>'.$i->gates_height.'</td></tr>';
						$s.='<tr class="odd"><td>Кран-балка</td>';
						if($i->cathead==1) 	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr><td>Емкость паллетного хранения</td><td>'.$i->pallet_capacity.'</td></tr>';
						$s.='<tr class="odd"><td>Емкость полочного хранения</td><td>'.$i->shelf_capacity.'</td></tr>';
					$s.='<tr><td></td><td></td></tr>';
						$s.='<tr class="odd"><td>Автомобильные подъездные пути</td>';
						if($i->autoways==1) 	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr><td>Железнодорожные подъездные пути</td>';
						if($i->railways==1) 	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr class="odd"><td>Форма договора</td><td>'.$i->contr_condition.'</td></tr>';
						$s.='<tr><td>Интернет</td>';
						if($i->internet==1) 	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr class="odd"><td>Провайдер</td><td>'.$i->providers.'</td></tr>';
						$s.='<tr><td>Телефония</td>';
						if($i->telephony==1) 	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr class="odd"><td>Телефонная компания</td><td>'.$i->tel_company.'</td></tr>';
						$s.='<tr><td>Парковка</td><td>'.$i->parking.'</td></tr>';
						$s.='<tr class="odd"><td>Количество закрепленных машиномест</td><td>'.$i->fixed_qty.'</td></tr>';
						$s.='<tr><td>Возможность размещения рекламы</td>';
						if($i->can_reclame==1) 	$s.=$yes; else	$s.=$no;
						$s.='</tr>';
					break;
				}
				default: {
					$s.='<tr><td></td><td></td></tr>';
						$s.='<tr class="odd"><td>Общая площадь помещения</td><td>'.$i->total_item_sqr.' м<sup>2</sup></td></tr>';
					$s.='<tr><td></td><td></td></tr>';
						$s.='<tr class="odd"><td>Состояние ремонта</td><td>'.$i->repair.'</td></tr>';
						$s.='<tr><td>Парковка</td><td>'.$i->parking.'</td></tr>';
						$s.='<tr class="odd"><td>Количество закрепленных машиномест</td><td>'.$i->fixed_qty.'</td></tr>';
						$s.='<tr><td>Грузовой лифт</td>';
						if($i->service_lift==1)	$s.=$yes; else	$s.=$no;
					$s.='<tr class="odd"><td></td><td></td></tr>';
						$s.='<tr><td>Видеонаблюдение</td>';
						if($i->video==1)	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr class="odd"><td>Пропускная система</td>';
						if($i->access==1)	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr><td>Система пожаротушения</td>';
						if($i->firefighting==1) 	$s.=$yes; else	$s.=$no;
					$s.='<tr class="odd"><td></td><td></td></tr>';
						$s.='<tr><td>Интернет</td>';
						if($i->internet==1)	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr class="odd"><td>Провайдер</td><td>'.$i->providers.'</td></tr>';
						$s.='<tr><td>Телефония</td>';
						if($i->telephony==1)	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr class="odd"><td>Телефонная компания</td><td>'.$i->tel_company.'</td></tr>';
					$s.='<tr><td></td><td></td></tr>';
						$s.='<tr class="odd"><td>Форма договора</td><td>'.$i->contr_condition.'</td></tr>';
						$s.='<tr><td>Бытовые помещения</td><td>'.$i->welfare.'</td></tr>';
						$s.='<tr class="odd"><td>Коммунальные услуги</td><td>'.$i->pay_utilities.'</td></tr>';
						$s.='<tr><td>Системы отопления и водоснабжения</td>';
						if($i->heating==1)	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr class="odd"><td>Возможность рекламы</td>';
						if($i->can_reclame==1)	$s.=$yes; else	$s.=$no;
					$s.='<tr><td></td><td></td></tr>';
						$s.='<tr class="odd"><td>Емкость паллетного хранения</td><td>'.$i->pallet_capacity.'</td></tr>';
						$s.='<tr><td>Емкость полочного хранения</td><td>'.$i->shelf_capacity.'</td></tr>';
						$s.='<tr class="odd"><td>Высота потолков</td><td>'.$i->ceiling_height.'</td></tr>';
						$s.='<tr><td>Рабочая высота помещения</td><td>'.$i->work_height.'</td></tr>';
					$s.='<tr class="odd"><td></td><td></td></tr>';
						$s.='<tr><td>Кран-балка</td>';
						if($i->cathead==1)	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr class="odd"><td>Покрытие пола</td><td>'.$i->flooring.'</td></tr>';
						$s.='<tr><td>Нагрузка на пол</td><td>'.$i->floor_load.'</td></tr>';
						$s.='<tr class="odd"><td>Система вентиляции</td><td>'.$i->ventilation.'</td></tr>';
						$s.='<tr><td>Кондиционирование</td>';
						if($i->air_condit==1)	$s.=$yes; else	$s.=$no;
					$s.='<tr class="odd"><td></td><td></td></tr>';
						$s.='<tr><td>Электр.мощность</td><td>'.$i->el_power.'</td></tr>';
						$s.='<tr class="odd"><td>Количество ворот</td><td>'.$i->gates_qty.'</td></tr>';
						$s.='<tr><td>Высота ворот</td><td>'.$i->gates_height.'</td></tr>';
						$s.='<tr class="odd"><td>Пандус</td>';
						if($i->rampant==1)	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr><td>Автомобильные подъездные пути</td>';
						if($i->autoways==1)	$s.=$yes; else	$s.=$no;
						$s.='</tr><tr class="odd"><td>Железнодорожные подъездные пути</td>';
						if($i->railways==1)	$s.=$yes; else	$s.=$no;	
				}
			}

			$html .= $s;

			$html.= '</table>';
		$html .= '</div>';

		if ($this->forHtml && !empty($this->images)){
			$html .= '<div id="right">';
			foreach ($this->images as $img){
				$html .= '<p><img src="storage/images/_thumbs/'.$img->name_medium.'" /></p>';
			}
			$html .= '</div>';
			$html .= '<div class="clear"></div>';
		}

		$mpdf = new mPDF('utf-8', 'A4', '8', '', 20, 20, 7, 7, 10, 10);
		$mpdf->charset_in = 'utf-8';

		$stylesheet = file_get_contents('css/pdf.css'); /*подключаем css*/
		$mpdf->WriteHTML($stylesheet, 1);

		$mpdf->list_indent_first_level = 0;
		$mpdf->WriteHTML($html, 2);
		// $mpdf->Output('object_detail_'.$this->id.'.pdf', 'I'); -- Это вывод непосредственно в браузер
		$mpdf->Output($this->directory.'obj'.$this->id.'.pdf', 'F');
	}
}?>