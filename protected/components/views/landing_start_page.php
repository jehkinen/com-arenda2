
<? $img_url = Yii::app()->baseUrl.'/images/landing_popup';?>
<?Yii::app()->clientScript->registerPackage('landing_start');?>




<div id="b-landing-popup__window" class="b-landding__inner">
	<div class="b-landing__padder">
		<ins class="i-close"></ins>
		<img class="landing-logo" src="<?echo $img_url;?>/landing_popup_logo.jpg">
		<div class="b-project__description">
			<h1>О проекте</h1>
			<p>
                <b>Com-arenda.ru</b> – это новый информационный портал
				для поиска и автоматического подбора объектов
				коммерческой недвижимости в аренду: офисов, складов и
				производственных помещений. Личный кабинет и блокнот позволяет
				хранить и управлять неограниченным количеством объектов а SMS и
				е-mail уведомления с портала абсолютно бесплатны и позволяют вам
				оперативно реагировать на ситуацию.
			</p>
            <div class="clearfix"></div>
		</div>
        <div class="clearfix"></div>
		<div class="b-main-landing__navigation__inner">
			<ul class="b-main-landing__navigation">
				<li id="#content_1"><ins class="landing-icon" id="one-icon"></ins>Всем</li>
				<li id="#content_2"><ins class="landing-icon" id="two-icon"></ins>Собственнику</li>
				<li id="#content_3"><ins class="landing-icon" id="three-icon"></ins>Агенствам недвижимости</li>
			</ul>
            <div class="clearfix"></div>
        </div>

		<div class="b-tab_content">

            <div class="b-one-content" id="content_1">
                <ul>
                    <li>
                        <b>SMS и е-mail:</b> Копируйте выбранные
                        объекты в блокнот и будьте в курсе
                        статуса объекта. Если помещение
                        перестает быть актуальным, вы узнаете об этом
                        через SMS – Бесплатно!
                    </li>
                    <li>
                        <b>Личный кабинет:</b> для хранения неограниченного
                        количества объектов и заявок
                    </li>
                    <li>
                        <b>Удобно:</b> Вы можете скачивать и распечатывать
                        данные по объектам в виде PDF-файла
                        с фотографиями и
                        описаниями.
                    </li>
                </ul>

                <div class="clearfix"></div>
            </div>

			<div class="b-one-content" id="content_2">
			   <ul>
				   <li>
			           Мы ищем партнеров: Среди агентств недвижимости
					   для взаимовыгодного сотрудничества  во все крупных
					   и средних городах России (Москва, Санкт Петербург,
					   Екатеринбург, Новосибирск, и т.д.)
				   </li>
			   </ul>
                <div class="clearfix"></div>
			</div>

            <div class="b-one-content" id="content_3">
                <ul>
                    <li> <b>Быстрое размещение объектов без регистрации!</b> </li>
                    <li> <b>Объекты – контейнеры:</b> вам не придется постоянно
                        создавать однотипные объявления и многократно
                        указывать повторяющиеся параметры
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
		</div>

        <ins class="arrow left-arrow"></ins>
        <ins class="arrow right-arrow"></ins>

		<div class="b-sky_flexuous-horizont">
			<div class="b-sky_flexuous-horizont__padder">
                 <a href="catalog">Каталог бизнес центров,</a>&nbsp;Выбирайте место для вашего бизнеса
                <div class="clearfix"></div>
            </div>
		</div>
		<div  class="b-simple-steps">
			<div class="b-one-little-step">Первые шаги: </div>
			<div class="b-steps__list">
				<a href="/registration.html">Зарегистрироваться</a>
				<a href="/sendorder/view">Быстро разместить объявление, без регистрации</a>
				<a href="/how_work.html">Подробно узнать как работает сервис</a>
			</div>
			<div class="clear"></div>
		</div>
		<div class="b-landing__footer">
			<div class="b-goto_mainpage"><a href="/">Перейти на главную страницу</a></div>
            <fieldset class="b-dont-open__when-start">
                <div class="fcheck01 ue">
                    <input class="dn" name="dont_open" type="checkbox" checked="1">
                </div>
                <label class="">Не показывать при открытии</label>
            </fieldset>
            <div class="clearfix"></div>
		</div>
	</div>
</div>


<script>
    $(function(){
        $('body').append('<div class="b-landing__overlay"></div>');
        $('.b-landing__overlay, #b-landing-popup__window .i-close').click(function(){
            $('.b-landing__overlay').hide();
            $('#b-landing-popup__window').hide();
	        if( $('.b-dont-open__when-start input').is(':checked')) {
                $.cookie('show_when_start', 0, { path: "/", expires: 70 });
            }
        });


        hide_content();

        function hide_content(){
            $('.b-one-content').each(function (){
                $(this).hide();
            });
        }


        $('.b-landding__inner .arrow').click(function (){
	        var active_li = $('.b-main-landing__navigation li.active');

            if($(this).hasClass('right-arrow')){
	            var next_li = active_li.next();
	            if( next_li.length !=0) {
                    next_li.click();
                }
	            else{
                    $('.b-main-landing__navigation li:first').click();
	            }
            }

            else if($(this).hasClass('left-arrow')) {
                var prev_li = active_li.prev();
                if(  prev_li.length !=0) {
                     prev_li.click();
                }
                else{
                    $('.b-main-landing__navigation li:last').click();
                }
            }
        });

        $('.b-main-landing__navigation li').click(function (){
            if( !$(this).hasClass('active')){
                //Удалим сначала у всех активный класс
                $('.b-main-landing__navigation li').each(function(){
                    $(this).removeClass('active');
                });
                //Добавим к тому, по которому кликнули
                $(this).addClass('active');
                var point_id = $(this).attr('id');
                hide_content();
                $(point_id).show();
            }
            return false;
        });

        $('.b-main-landing__navigation li:first').click();

    });
</script>