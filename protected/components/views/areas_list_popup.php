<?
$cs = Yii::app()->clientScript;
//POS_END - файлы подкл. перед закрывающим тегом </html>
$cs->registerScriptFile(Yii::app()->baseUrl.'/js/areasPopupList.js',CClientScript::POS_END);
$cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.jscrollpane.js',CClientScript::POS_END);
$cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.mousewheel.js',CClientScript::POS_END);
$cs->registerCssFile(Yii::app()->baseUrl.'/css/jscrollpane.css');
?>


<div id="Popup2">
	<div class="b-popup__padder">
	    <label class="i-cityselect__label">Выбор города </label>
	    <select id="combobox">
			<?foreach($all_cities as $item){?>
	        <option value="<?echo $item?>"><?echo $item?></option>
			<?}?>
	    </select>

	    <a id="Popup2Close"></a><br>
	    <div class="areas_list_inner">
	        <h2 class="M10L"> <ins class="i-shadow"></ins> Область</h2>
	        <ul class="areas_list">
				<?foreach ($result as $area_name=>$list){?>
	            <li id="<? echo 'link_'.$areas[$area_name];?>"><?echo $area_name?></li>
				<?}?>
	        </ul>
	    </div>

	    <div class="city_container">
	        <h2 class="M10L"><ins class="i-shadow"></ins> Населенный пункт</h2>

			<?foreach ($result as $area_name=>$list){?>
	        <ul class="city_list" id="<? echo 'area_'.$areas[$area_name];?>">
				<?foreach ($list as $city){?>
	            <li><?echo $city?></li>
				<?}?>
	        </ul>
			<?}?>

	    </div>

	    <div class="b-city-add">
	        <p class="b-city-add__info">
	            Если в нашем списке нет вашего города, то вы можете подать заявку на добавление города, указав ваши контактные данные для оповещения о добавлении.
	        </p>
	        <fieldset class="city-add_fieldset city_inner">

				<?php echo CHtml::textField('add-new-city', '', array('name'=>'add-new-city','id'=>'city-add-input', 'onfocus'=>"this.placeholder = ''", 'onblur'=>"this.placeholder = 'Город ...'", 'placeholder'=>'Город ...' )); ?>
	            <p class="error" style="display: none;">Введите корректный город</p>
	        </fieldset>

	        <fieldset class="city-add_fieldset email-inner">
				 <?php echo CHtml::textField('notification-email', '', array('name'=>'notification-email','id'=>'city-add-email','onfocus'=>"this.placeholder = ''", 'onblur'=>"this.placeholder = 'Ваш E-mail ...'", 'placeholder'=>'Ваш E-mail ...')); ?>
				 <p class="error" style="display: none;">Введите корректный адрес</p>
			</fieldset>

	        <fieldset class="city-add_fieldset notificate">
	            <div class="fcheck01 ue">
				    <?php echo CHtml::checkBox('notificate-me', '', array('class'=>'dn','name'=>'notificate-me','id'=>"notificate-me")); ?>
	            </div>
	            <label class="label-in-city-add">Уведомить меня о добавлении города</label>
	        </fieldset>


	        <input type="button" id="add-new-city" value="Добавить">
	        <script>
	            $(function() {

	                $('.city_list, .areas_list').jScrollPane(
	                        {
	                            verticalDragMinHeight: 34,
	                            verticalDragMaxHeight: 34,
	                            horizontalDragMinWidth: 7,
	                            horizontalDragMaxWidth: 7
	                        }
	                );

	                if( $('.areas_list .jspVerticalBar').length!=0) {
	                    $('#Popup2 .city_list').css('border-left', 'none');

                    }else{
                        $('#Popup2 .areas_list_inner .i-shadow').hide();
	                }


	                function close_popup(){
	                    $('#Popup2Close').click();
	                    $('#SendOrder_city').val($('#city-add-input').val());
		            }

	                    $('#add-new-city').bind('click', function(){

	                        $('.city-add_fieldset .error').hide();

		                    function validate(){
	                            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;  //регулярка для провеки email

	                            var $mail_error = $('.email-inner .error');
	                            var $city_error =   $('.city_inner .error');
	                            var validation = true;

	                            if($('#city-add-input').val() ==''){
	                                $city_error.show();
	                                validation = false;
	                            }

	                            if( $('#notificate-me').is(':checked') ){
	                                var mail = $('#city-add-email').val();
		                            if ( !emailReg.test(mail) || mail==''  ){
		                                $mail_error.show();
		                                validation = false;
		                            }
	                            }
	                            return validation;
	                        }

		                    if(validate()){
	                            var notificate_val = 'no';
			                    if($('#notificate-me').is(':checked')){
	                                notificate_val  = 'yes';
	                            }
		                        $.ajax({
		                            url: "ajaxadmins/addNewCityRequest",
		                            data: {
			                            city:$('#city-add-input').val(),
			                            email: $('#city-add-email').val(),
			                            notificate: notificate_val,
		                            },
		                            type: 'get',
		                            dataType: 'json',
		                            success: function(data){
		                                alert(data);
		                            }
		                        });
			                    close_popup();
	                        }
	                        return false;
	                    });
	            });
	        </script>
	    </div>
    </div>
</div>
<div id="Popup2Fog" style="display: none;"></div>