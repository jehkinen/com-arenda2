<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <title>Error!</title>
</head>

<body>
<div align="center">
 <div style="width: 400px; clear: both; margin: 1em 0; padding: 1em 15px; background: #FFCCCC; background: <?php echo $this->color; ?>; border: 1px solid <?php echo $this->color_border; ?>;">
  <b><?php echo $this->title; ?></b><br /><br />
  <?php echo $this->msg; ?><br /><br />
  <a href="javascript: history.go(-1);">Вернуться назад</a>
 </div>
</div>
</body>

</html>
