<script type="text/javascript">
$(document).ready(function(){
	$('#thumbsRecl a').click(function(){
		var Small_obj = $(this);
		var mediumPath = Small_obj.attr('href');
		var largePath = Small_obj.attr('title');
		$('#firstImgRecl').attr({'src':mediumPath, 'alt':largePath});
		return false;
	});
	$('#firstImgRecl').click(function(){
		var openPic = $(this).attr('alt');
		window.open('http://comarenda/storage/images/'+openPic, '_blank');
	});
});
</script>
<?php $bUrl = Yii::app()->baseUrl;
	switch ($item->kind) {
		case 1 : {$lbClass = 'торгового помещения'; break;}
		case 2 : {$lbClass = 'склада/производственного помещения'; break;}
		default: $lbClass = 'офиса';
	}
?>
	<div class="left_o_centr">
		<div class="title"><a href="#">Офисный центр "Олимп"</a></div>
		<div class="foto_card">
			<img id="firstImgRecl" style="cursor: hand;" src="<?php echo $bUrl;?>/storage/images/_thumbs/<?php echo $aFotos[0]->name_medium;?>" alt="<?php echo $aFotos[0]->img;?>" />
			<div id="thumbsRecl" class="card_mini_gall">
<?php foreach ($aFotos as $f): ?>
				<a href="<?php echo $bUrl;?>/storage/images/_thumbs/<?php echo $f->name_medium;?>" title="<?php echo $f->img;?>"><img src="<?php echo $bUrl;?>/storage/images/_thumbs/<?php echo $f->name_small;?>" alt="" /></a>
<?php endforeach;?>
			</div>
		</div>
		<h3><?php echo $item->city;?>, <?php echo $item->street;?>, <?php echo $item->house;?></h3>
		<i>Аренда <?php echo $lbClass;?> класса "<?php echo $item->class;?>"</i><br /><?php echo number_format($item->total_item_sqr, 2, '.', ' ');?> м<sup>2</sup> / <?php echo number_format($item->price, 2, '.', ' ');?> р.<br />
		<?php echo $item->about;?>
	</div>
	