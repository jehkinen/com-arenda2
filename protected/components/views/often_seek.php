<?php if (!empty($items)):?>
Часто ищут:
<ul>
<?php	foreach($items as $i) {
		$s = ' помещение';
		switch ($i->kind) {
			case 1: {$s = 'торговое'.$s; break;}
			case 2: {$s = 'складское\производственное'.$s; break;}
			default: $s = 'офис';
		}
		if (isset($i->district))
			$s.= ', '.$i->district;
		
		$p = ''; $sep = ', '; 
		if (isset($i->squareMin)) {
			if (isset($i->squareMax))
					$p = (float)$i->squareMin.'-'.(float)$i->squareMax;
			else	$p = 'от '.(float)$i->squareMin;
		} elseif (isset($i->squareMax))
			$p = 'до '.(float)$i->squareMax;
		if ($p!='') {
			$s.= $sep.$p.' м2';
			$sep = '; ';
		}

		$p = '';
		if (isset($i->priceMin)) {
			if (isset($i->priceMax))
					$p = (float)$i->priceMin.'-'.(float)$i->priceMax;
			else	$p = 'от '.(float)$i->priceMin;
		} elseif (isset($i->priceMax))
			$p = 'до '.(float)$i->priceMax;
		if ($p!='') $s.= $sep.$p.' руб.';
		echo '<li>'.CHtml::link($s, array('search/most', 'id'=>$i->id)).'</li>';
}?>
</ul>
<?php endif;?>