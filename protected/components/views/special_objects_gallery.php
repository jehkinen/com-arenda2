<?php $bUrl = Yii::app()->baseUrl; ?>
<ul id="mycarousel" class="jcarousel-skin-tango">
<?php foreach($items as $i):?>
<li>
	<a href="<?php echo $bUrl.'objects/detail/id/'.$i->id;?>" class="abs_on_hover">
<?php if (!empty($i->hasFoto)) $s = '/storage/images/_thumbs/'.$i->hasFoto; else $s = '/images/gallery_bl.jpg';	echo CHtml::image($bUrl.$s);?>
		<div><?php
switch ($i->kind) {
	case 1 : {$s = 'Торговая площадь'; break;}
	case 2 : {$s = 'Склад / Производство'; break;}
	default: $s = 'Офисное помещение';
}
echo $i->city.'<br />'.$i->district.', '.$i->total_item_sqr;?>м<sup>2</sup></div>
		<span class="hover"><?php echo $s;?></span>
		<span class="hover"><?php echo 'ул.'.$i->street.' '.$i->house;?></span>
		<span class="hover price"><?php echo number_format($i->price, 0, '.', ' ');?> руб./месяц</span>
	</a>
</li>
<?php endforeach;?>
</ul>
