<?php $bUrl = Yii::app()->baseUrl; ?>
<div class="green_top_border_block clear_both">
	<div class="also_to_view_wrap">
		<div class="recommend"><span>Рекомендуем к просмотру</span><div class="border"></div></div>
		<ul class="also_to_view">
<?php foreach($items as $i):?>
			<li><a href="objects/detail/id/<?php echo $i->id;?>">
					<?php if (empty($i->hasFoto)) $s = $bUrl.'/images/noFoto-sm.png'; else $s = $bUrl.'/storage/images/_thumbs/'.$i->hasFoto; echo CHtml::image($s);?>
					<div><?php echo $lbKind;?><br /><?php echo $i->street.', '.$i->house;?>; <?php echo $i->total_item_sqr;?>м<sup>2</sup></div>
				</a>
			</li>
<?php endforeach;?>
		</ul>
		</div>
	</div>
</div><!-- / SIMILAR green_top_border_block clear_both -->
