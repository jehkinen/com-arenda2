<div class="title_site"><?php echo CHtml::link('Новости', array('news/all'));?></div>
<?php foreach($items as $i):?>
<div class="left_news_bl">
	<div class="title"><?php echo CHtml::link($i->header, array('news/view', 'id'=>$i->id));?> <?php $get = $this->widget('DateFormat', array('kind'=>1,'dbDate'=>$i->created)); echo $get->date;?></div>
<?php if(isset($i->small_pic)) echo CHtml::image(Yii::app()->baseUrl.'/storage/images/news/'.$i->small_pic, '', array('class'=>'img_left'));?>
	<p><?php echo($i->anounce);?></p>
</div>
<?php endforeach;?>