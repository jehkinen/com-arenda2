<script type="text/javascript">
	$(document).ready(function(){
		$('#thumbs li a').click(function(){
			var Small_obj = $(this);
			var mediumPath = Small_obj.attr('href');
			var largePath = Small_obj.attr('title');
			$('#firstImg').attr({'src':mediumPath, 'alt':largePath});
			return false;
		});

		$('#firstImg').click(function(){
			var openPic = $(this).attr('alt');
			window.open('<?php echo Yii::app()->params->site_base;?>storage/images/'+openPic, '_blank');
		});
	});
</script>
<?php $bUrl = Yii::app()->baseUrl;?>
	<div><?php echo CHtml::link($camp->name, array('objects/detail', 'id'=>$camp->item), array('class'=>'header_link'));?></div>
	<div class="gallery_block_wrap">
		<img id="firstImg" style="cursor: hand;" src="<?php echo $bUrl;?>/storage/images/_thumbs/<?php echo $aFoto[0]->name_medium;?>" alt="<?php echo $aFoto[0]->img;?>" />
		<ul id="thumbs" class="gallery_block">
<?php foreach ($aFoto as $f): ?>
			<li><a href="<?php echo $bUrl;?>/storage/images/_thumbs/<?php echo $f->name_medium;?>" title="<?php echo $f->img;?>"><img src="<?php echo $bUrl;?>/storage/images/_thumbs/<?php echo $f->name_small;?>" alt="" /></a></li>
<?php endforeach;?>
		</ul>
	</div>
	<div class="gray_block"><?php echo $camp->txt;?></div>
