<?php /* Переопределенный виджет. Оригинальный файл yii-1.1.6\framework\web\widgets\pagers\ CLinkComarendaPager.php */

class CLinkComarendaPager extends CBasePager
{
	const CSS_FIRST_PAGE='firstLink';
	const CSS_LAST_PAGE='lastLink';
	const CSS_PREVIOUS_PAGE='prev';
	const CSS_NEXT_PAGE='next';
	const CSS_INTERNAL_PAGE='page';
	const CSS_HIDDEN_PAGE='hidden';
	const CSS_SELECTED_PAGE='active';

	public $maxButtonCount=10;
	public $nextPageLabel;
	public $prevPageLabel;
	public $firstPageLabel;
	public $lastPageLabel;
	public $header;
	public $footer='';
	public $cssFile;
	public $htmlOptions=array();

	/**
	 * Initializes the pager by setting some default property values.
	 */
	public function init()
	{
		if($this->nextPageLabel===null)
			$this->nextPageLabel=Yii::t('yii','Next &gt;');
		if($this->prevPageLabel===null)
			$this->prevPageLabel=Yii::t('yii','&lt; Previous');
		if($this->firstPageLabel===null)
			$this->firstPageLabel=Yii::t('yii','&lt;&lt; First');
		if($this->lastPageLabel===null)
			$this->lastPageLabel=Yii::t('yii','Last &gt;&gt;');
		if($this->header===null)
			$this->header=Yii::t('yii','Go to page: ');

		/*
		if(!isset($this->htmlOptions['id']))
			$this->htmlOptions['id']=$this->getId();
		*/
		if(!isset($this->htmlOptions['class']))
			$this->htmlOptions['class']='pagination02';
	}

	public function run()
	{
		$buttons=$this->createPageButtons();
		if(empty($buttons))
			return;
		//echo '<div class="number_str_site">'."\n";
		//echo '<span>'.$this->header.'</span>'."\n";
		echo CHtml::tag('div',$this->htmlOptions,implode("\t\n",$buttons));
		//echo $this->footer;
		//echo "\n".'</div>'."\n";
	}

	protected function createPageButtons()
	{
		if(($pageCount=$this->getPageCount())<=1)
			return array();

		list($beginPage,$endPage)=$this->getPageRange();
		$currentPage=$this->getCurrentPage(false); // currentPage is calculated in getPageRange()
		$buttons=array();

		// first page
		if (!empty($this->firstPageLabel))
			$buttons[]=$this->createPageButton($this->firstPageLabel,0,self::CSS_FIRST_PAGE,$currentPage<=0,false);

		// prev page
		if(($page=$currentPage-1)<0)
			$page=0;
		if ($beginPage>0)
			$buttons[]=$this->createPageButton($this->prevPageLabel,$page,self::CSS_PREVIOUS_PAGE,$currentPage<=0,false);

		// internal pages
		for($i=$beginPage;$i<=$endPage;++$i)
			$buttons[]=$this->createPageButton($i+1,$i,self::CSS_INTERNAL_PAGE,false,$i==$currentPage);

		// next page
		if(($page=$currentPage+1)>=$pageCount-1)
			$page=$pageCount-1;
		if ($endPage+1!=$pageCount)
			$buttons[]=$this->createPageButton($this->nextPageLabel,$page,self::CSS_NEXT_PAGE,$currentPage>=$pageCount-1,false);

		// last page
		if (!empty($this->lastPageLabel))
			$buttons[]=$this->createPageButton($this->lastPageLabel,$pageCount-1,self::CSS_LAST_PAGE,$currentPage>=$pageCount-1,false);

		return $buttons;
	}

	/**
	 * Creates a page button.
	 * You may override this method to customize the page buttons.
	 * @param string $label the text label for the button
	 * @param integer $page the page number
	 * @param string $class the CSS class for the page button. This could be 'page', 'first', 'last', 'next' or 'previous'.
	 * @param boolean $hidden whether this page button is visible
	 * @param boolean $selected whether this page button is selected
	 * @return string the generated button
	 */
	protected function createPageButton($label,$page,$class,$hidden,$selected)
	{
		if($hidden || $selected)
			$class.=' '.($hidden ? self::CSS_HIDDEN_PAGE : self::CSS_SELECTED_PAGE);
		if ($selected)	$s = $label;
		else			$s = CHtml::link($label,$this->createPageUrl($page));
		return '<span class="'.$class.'">'.$s.'</span>';
	}

	/**
	 * @return array the begin and end pages that need to be displayed.
	 */
	protected function getPageRange()
	{
		$currentPage=$this->getCurrentPage();
		$pageCount=$this->getPageCount();

		$beginPage=max(0, $currentPage-(int)($this->maxButtonCount/2));
		if(($endPage=$beginPage+$this->maxButtonCount-1)>=$pageCount)
		{
			$endPage=$pageCount-1;
			$beginPage=max(0,$endPage-$this->maxButtonCount+1);
		}
		return array($beginPage,$endPage);
	}
}
