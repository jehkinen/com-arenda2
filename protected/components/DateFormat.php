<?php
class DateFormat extends CWidget{
	public $dbDate;
	public $kind;
	public $date;
	public $isEmpty;
	public function run(){
		switch ($this->kind) {
			case 1: {
				list($y, $m, $d) = explode('-', substr($this->dbDate, 0, 10));
				$this->date = $d.'.'.$m.'.'.$y;
				$this->isEmpty = ($d==0) || ($m==0) || ($y==0);				
			break;}
			case 2: {
				list($y, $m, $d) = explode('-', substr($this->dbDate, 0, 10));
				$this->date = $d.'.'.$m;
				$this->isEmpty = ($d==0) || ($m==0) || ($y==0);				
			break;}
			case 5: {
				list($y, $m, $d) = explode('-', substr($this->dbDate, 0, 10));
				list($h, $n, $s) = explode(':', substr($this->dbDate, 11));
				$this->date = $d.'.'.$m.'.'.$y.' '.$h.':'.$n.'.'.$s;
				$this->isEmpty = ($d==0) || ($m==0) || ($y==0);				
			break;}
		}
	}
}
?>