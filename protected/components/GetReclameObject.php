<?php class GetReclameObject extends CWidget{

	public $sel_name;
	public $kind;
	public $city;

	public function run(){
		$id_obj = 55;
		// itemType =1 !!
		$criteria = new CDbCriteria;
		$criteria->alias = 'I';
		$criteria->select = 'I.id, I.parent, I.kind, I.city, COALESCE(I.name, "без имени") as name, COALESCE(CL.shname, "нет") as class, COALESCE(S.name, "нет") as street, COALESCE(I.house, "нет") as house, COALESCE(I.total_item_sqr, "0") as total_item_sqr, I.price, I.about';
		$criteria->join = 'left join streets S on S.id=I.street '.
			'left join classes CL on CL.id=I.class ';
		$criteria->condition = 'I.id=:id';
		$criteria->params = array(':id'=>$id_obj);
		$Row = ItemDetail::model()->find($criteria);


		$criteria->alias = '';
		$criteria->join = '';
		$criteria->select = '*';
		$criteria->condition = 'id_item=:id';
		$criteria->params = array(':id'=>$id_obj);
		$criteria->order = 'id';
		$Fotos = Images::model()->findAll($criteria);


		$this->render('get_reclame_object', array(
			'item'=>$Row, 
			'aFotos'=>$Fotos,
			'active'=>$this->sel_name
		));
	}
}?>