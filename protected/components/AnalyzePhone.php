<?php
class AnalyzePhone extends CWidget{
	public $phone;
	public $isCellphone;

	public function run(){
		$a = array('(', ')', ' ', '+', '-');
		$tel = str_replace($a, '', $this->phone);
		if (($tel{0}=='8') || ($tel{0}=='7'))
			$tel = substr($tel, 1);
		if ((strlen($tel)==10) && ($tel{0}=='9')) {
			$this->phone = $tel;
			$this->isCellphone = true;
		}
		else	$this->isCellphone = false;
	}
}
?>