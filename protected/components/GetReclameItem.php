<?php class GetReclameItem extends CWidget{

	public $onPage;

	public function run(){
		$usr = Yii::app()->user;
		$id = 0;
		switch ($this->onPage) {
			case 'main'		: {
				if (isset($usr->recl_i_main))
						$s = $usr->getState('recl_i_main');
				else	$s = $this->ExtractFromDb('onMain');
				$id = $this->GetItem($s, 'recl_i_main');
				break;}
			case 'serp'		: {
				if (isset($usr->recl_i_serp))
						$s = $usr->getState('recl_i_serp');
				else	$s = $this->ExtractFromDb('onSearch');
				$id = $this->GetItem($s, 'recl_i_serp');
				break;}
			case 'any'		: {
				if (isset($usr->recl_i_any))
						$s = $usr->getState('recl_i_any');
				else	$s = $this->ExtractFromDb('onAny');
				$id = $this->GetItem($s, 'recl_i_any');
				break;}
		}
		if ($id>0) {
			$camp = ReclameItem::model()->findByPK($id);
			if (!empty($camp)) {
				$criteria = new CDbCriteria;
				$criteria->select = '*';
				$criteria->condition = 'id_item=:id';
				$criteria->params = array(':id'=>$camp->item);
				$criteria->order = 'id';
				$Fotos = Images::model()->findAll($criteria);
				if (!empty($Fotos))
					$this->render('get_reclame_item', array('camp'=>$camp, 'aFoto'=>$Fotos));
			}
		}
	}
	
	private function ExtractFromDb($page){
		$criteria = new CDbCriteria;
		$criteria->alias = 'I';
		$criteria->select = 'I.id';
		$criteria->condition = 'I.isActive=1 and I.'.$page.'=1 and I.start<DATE(NOW()) and I.stop>DATE(NOW()) and I.qty>(Select COUNT(*) from reclame_item_stat S where S.camp=I.id)';
		$criteria->order = 'RAND()';
		$rows = ReclameItem::model()->findAll($criteria);
		$s = '';
		if (!empty($rows)) {
			foreach ($rows as $r) $s.=','.$r->id;
			$s = substr($s, 1);
		}
		return $s;
	}
	
	private function GetItem($str, $sesName){
		if ($str=='') return 0;
		else {
			$a = explode(',', $str);
			$i = array_shift($a);
			array_push($a, $i);
			Yii::app()->user->setState($sesName, implode(',', $a));
			// Занесем в статистику факт показа
			$s = 'Insert into reclame_item_stat (camp, ip) values ('.$i.", '".$_SERVER['REMOTE_ADDR']."');";
			$cmd = Yii::app()->db->createCommand($s);
			$cmd->execute();
		}
		return $i;
	}
}?>