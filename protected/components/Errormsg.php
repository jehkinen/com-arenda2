<?php
  class Errormsg extends CWidget{

    public $title = 'Ошибка!';
    public $msg   = '';
    public $color = 'white';
    public $color_border = 'blue';

    public function init(){
      if (!empty($this->msg))
      $this->render('errormsg');
    }

    public function create($date = array()){
      if (!empty($date['title'])){
        $this->title = $date['title'];
      }
      if (!empty($date['msg'])){
        $this->msg = $date['msg'];
      }
      if (!empty($date['color'])){
        $this->color = $date['color'];
      }
      if (!empty($date['color_border'])){        $this->color_border = $date['color_border'];      }

    }

    public function returnConfig(){
      return array('title' => $this->title, 'msg' => $this->msg, 'color' => $this->color, 'color_border' => $this->color_border);
    }

  }
?>