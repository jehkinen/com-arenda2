<?php class GetReclameBanner extends CWidget{

	public $onPage;
	public $width;
	public $height;

	public function run(){
		$usr = Yii::app()->user;
		$id = 0; $ids = '';
		$place = $this->onPage.$this->width.'x'.$this->height;
		switch ($this->onPage) {
			case 'main'		: {
				$s = $usr->getState('recl_ban_'.$place, '');
				if ($s=='')	$s = $this->ExtractFromDb('onMain', $this->width, $this->height);
				$id = $this->GetItem($s, 'recl_ban_'.$place);
				break;}
			case 'serp'		: {
				$s = $usr->getState('recl_ban_'.$place, '');
				if ($s=='')	$s = $this->ExtractFromDb('onSearch', $this->width, $this->height);
				if (($this->height==400) || ($this->width==1000))
						$id = $this->GetItem($s, 'recl_ban_'.$place);
				else	$ids = $this->GetItems($s, 'recl_ban_'.$place);
				break;}
			case 'any'		: {
				$s = $usr->getState('recl_ban_'.$place, '');
				if ($s=='')	$s = $this->ExtractFromDb('onAny', $this->width, $this->height);
				if (($this->height==400) || ($this->width==1000))
						$id = $this->GetItem($s, 'recl_ban_'.$place);
				else	$ids = $this->GetItems($s, 'recl_ban_'.$place);
				break;}
		}
		if ($id>0) {
			$camp = ReclameBanner::model()->findByPK($id);
			if (!empty($camp)) {
				if ($this->width==1000)
						$this->render('get_reclame_banner_top', array('ban'=>$camp));
				else	$this->render('get_reclame_banner', array('ban'=>$camp));
			}
		} elseif ($ids!='') {
			$camps = ReclameBanner::model()->findAllBySql('Select * from reclame_banners where id IN ('.$ids.');');
			if (!empty($camps))
				$this->render('get_reclame_banners', array('bans'=>$camps));
		}
	}
	
	private function ExtractFromDb($page, $wid, $hei){
		$criteria = new CDbCriteria;
		$criteria->alias = 'I';
		$criteria->select = 'I.id';
		$criteria->condition = 'I.isActive=1 and I.'.$page.'=1 and I.width='.$wid.' and I.height='.$hei.' and I.start<DATE(NOW()) and I.stop>DATE(NOW()) and I.qty>(Select COUNT(*) from reclame_banner_stat S where S.camp=I.id)';
		$criteria->order = 'RAND()';
		$rows = ReclameBanner::model()->findAll($criteria);
		$s = '';
		if (!empty($rows)) {
			foreach ($rows as $r) $s.=','.$r->id;
			$s = substr($s, 1);
		}
		return $s;
	}
	
	private function GetItem($str, $sesName){
		if ($str=='') return 0;
		else {
			$a = explode(',', $str);
			$i = array_shift($a);
			array_push($a, $i);
			Yii::app()->user->setState($sesName, implode(',', $a));
			// Занесем в статистику факт показа
			$s = 'Insert into reclame_banner_stat (camp, ip) values ('.$i.", '".$_SERVER['REMOTE_ADDR']."');";
			$cmd = Yii::app()->db->createCommand($s);
			$cmd->execute();
		}
		return $i;
	}

	private function GetItems($str, $sesName){
		if ($str=='') return '';
		else {
			$a = explode(',', $str);
			$i = array_shift($a);
			array_push($a, $i);
			$r = implode(',', $a);
			Yii::app()->user->setState($sesName, $r);
			// Занесем в статистику факт показа
			$s = ''; $remIp = ", '".$_SERVER['REMOTE_ADDR']."'),";
			for ($i=0;$i<count($a);$i++)
				$s.= '('.$i.$remIp;
			$s = 'Insert into reclame_banner_stat (camp, ip) values '.substr($s, 0, -1).';';
			$cmd = Yii::app()->db->createCommand($s);
			$cmd->execute();
		}
		return $r;
	}

}?>