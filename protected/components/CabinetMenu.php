<?php class CabinetMenu extends CWidget{

	public $active;

	public function run(){
		$Items = array();
		if (Yii::app()->user->id['user_role']=='tenant') {
			$items = array('notepad', 'orders', 'page', 'setts');
			$names = array('Блокнот', 'Мои заявки', 'Моя страница', 'Настройки');
			$links = array('cabinet/notepad', 'cabinet/myorders', 'cabinet/mypage', 'cabinet/view');
		}
		else {
			$items = array('objects', 'orders', 'page', 'setts');
			$names = array('Мои объекты', 'Заявки', 'Моя страница', 'Настройки');
			$links = array('cabinet/objects', 'cabinet/orders', 'cabinet/mypage', 'cabinet/view');
		}
		for ($i=0;$i<4;$i++)
			if ($items[$i]==$this->active)
					$Items[] = '<span>'.$names[$i].'</span>';
			else	$Items[] = CHtml::link($names[$i], array($links[$i]));
		$this->render('cabinet_menu', array('items'=>$Items));
	}
}?>