<?php
  class PDFCreate extends CWidget{
    public $item;
    public $images;
    public $id;
	public function run(){
		include_once('protected/extensions/mpdf53/mpdf.php');
		$bUrl = Yii::app()->baseUrl;
		$html   = '';
		$string = '';
		$yes = '<img src="'.$bUrl.'/images/ico_mc_01.png" alt="" /> есть';
		$no  = '<img src="'.$bUrl.'/images/no.png" alt="" /> нет';
		
		$i = $this->item;

		$html  = '<table border="1">';
		$html .= '<tr><td>'.$i->city.', '.$i->street.', '.$i->house.'</td></tr>';
		$html .= '<tr><td><i>Аренда офиса класса '.$i->class.'</i><br />'.$i->total_item_sqr.' м<sup>2</sup> / '.$i->price.' р..<br />'.$i->about.'</td></tr>';
		$html .= '<tr><td>Телефон: '.$i->phone.'</td></tr>';
		$html .= '<tr><td>'.$i->firstname.' '.$i->lastname.'</td></tr>';
		$html .= '<tr><td>Функциональное назначение '.$i->functionality.'</td></tr>';
		$html .= '<tr><td>Район '.$i->district.'</td></tr>';
		$html .= '<tr><td>Улица '.$i->street.'</td></tr>';
		$html .= '<tr><td>Дом '.$i->house.'</td></tr>';
		$html .= '<tr><td>Корпус '.$i->housing.'</td></tr>';
		$html .= '<tr><td>Тип сооружения '.$i->kind_structure.'</td></tr>';
		$html .= '<tr><td>Название '.$i->name.'</td></tr>';
		$html .= '<tr><td>Класс '.$i->class.'</td></tr>';
		$html .= '<tr><td>Этаж '.$i->storey.'</td></tr>';
		$html .= '<tr><td>Этажность '.$i->storeys.'</td></tr>';

		switch ($i->kind){
		  case 0 : {
			$string  = '<tr><td>Коммунальные услуги '.$i->pay_utilities.'</td></tr>';
			$string .= '<tr><td>Состояние ремонта '.$i->repair.'</td></tr>';
			$string .= '<tr><td>Покрытие пола '.$i->flooring.'</td></tr>';
			$string .= '<tr><td>Видеонаблюдение '; if($i->video==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Система пожаротушения '; if($i->firefighting==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Пропускная система '; if($i->access==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Система вентиляции '.$i->ventilation.'</td></tr>';
			$string .= '<tr><td>Кондиционирование '; if($i->air_condit==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Форма договора '.$i->contr_condition.'</td></tr>';
			$string .= '<tr><td>Интернет '; if($i->internet==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Провайдер '.$i->providers.'</td></tr>';
			$string .= '<tr><td>Телефония '; if($i->telephony==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Телефонная компания '.$i->tel_company.'</td></tr>';
			$string .= '<tr><td>Парковка '.$i->parking.'</td></tr>';
			$string .= '<tr><td>Количество закрепленных машиномест '.$i->fixed_qty.'</td></tr>';
			$string .= '<tr><td>Возможность размещения рекламы '; if($i->can_reclame==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Дополнительная информация '.$i->about.'</td></tr>';
			break;
		  }
		  case 1 : {
			$string .= '<tr><td>Коммунальные услуги '.$i->pay_utilities.'</td></tr>';
			$string .= '<tr><td>Состояние ремонта '.$i->repair.'</td></tr>';
			$string .= '<tr><td>Покрытие пола '.$i->flooring.'</td></tr>';
			$string .= '<tr><td>Высота потолков '.$i->ceiling_height.'</td></tr>';
			$string .= '<tr><td>Рабочая высота помещения '.$i->work_height.'</td></tr>';
			$string .= '<tr><td>Нагрузка на пол '.$i->floor_load.'</td></tr>';
			$string .= '<tr><td>Электр.мощность '.$i->el_power.'</td></tr>';
			$string .= '<tr><td>Грузовой лифт '; if($i->service_lift==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Бытовые помещения '.$i->welfare.'</td></tr>';
			$string .= '<tr><td>Пандус '; if($i->rampant==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Видеонаблюдение '; if($i->video==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Система пожаротушения '; if($i->firefighting==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Система вентиляции '.$i->ventilation.'</td></tr>';
			$string .= '<tr><td>Кондиционирование '; if($i->air_condit==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Форма договора '.$i->contr_condition.'</td></tr>';
			$string .= '<tr><td>Интернет '; if($i->internet==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Провайдер '.$i->providers.'</td></tr>';
			$string .= '<tr><td>Телефония '; if($i->telephony==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Телефонная компания '.$i->tel_company.'</td></tr>';
			$string .= '<tr><td>Парковка '.$i->parking.'</td></tr>';
			$string .= '<tr><td>Количество закрепленных машиномест '.$i->fixed_qty.'</td></tr>';
			$string .= '<tr><td>Возможность размещения рекламы '; if($i->can_reclame==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Дополнительная информация '.$i->about.'</td></tr>';
			break;
		  }
		  case 2 : {
			$string  = '<tr><td>Коммунальные услуги '.$i->pay_utilities.'</td></tr>';
			$string .= '<tr><td>Состояние ремонта '.$i->repair.'</td></tr>';
			$string .= '<tr><td>Покрытие пола '.$i->flooring.'</td></tr>';
			$string .= '<tr><td>Высота потолков '.$i->ceiling_height.'</td></tr>';
			$string .= '<tr><td>Рабочая высота помещения '.$i->work_height.'</td></tr>';
			$string .= '<tr><td>Нагрузка на пол '.$i->floor_load.'</td></tr>';
			$string .= '<tr><td>Электр.мощность '.$i->el_power.'</td></tr>';
			$string .= '<tr><td>Системы отопления и водоснабжения '; if($i->heating==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Пропускная система '; if($i->access==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Грузовой лифт '; if($i->service_lift==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Бытовые помещения '.$i->welfare.'</td></tr>';
			$string .= '<tr><td>Пандус '; if($i->rampant==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Видеонаблюдение '; if($i->video==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Система пожаротушения '; if($i->firefighting==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Система вентиляции '.$i->ventilation.'</td></tr>';
			$string .= '<tr><td>Количество ворот '.$i->gates_qty.'</td></tr>';
			$string .= '<tr><td>Высота ворот '.$i->gates_height.'</td></tr>';
			$string .= '<tr><td>Кран-балка '; if($i->cathead==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Емкость паллетного хранения '.$i->pallet_capacity.'</td></tr>';
			$string .= '<tr><td>Емкость полочного хранения '.$i->shelf_capacity.'</td></tr>';
			$string .= '<tr><td>Автомобильные подъездные пути '; if($i->autoways==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Железнодорожные подъездные пути '; if($i->railways==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Форма договора '.$i->contr_condition.'</td></tr>';
			$string .= '<tr><td>Интернет '; if($i->internet==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Провайдер '.$i->providers.'</td></tr>';
			$string .= '<tr><td>Телефония '; if($i->telephony==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Телефонная компания '.$i->tel_company.'</td></tr>';
			$string .= '<tr><td>Парковка '.$i->parking.'</td></tr>';
			$string .= '<tr><td>Количество закрепленных машиномест '.$i->fixed_qty.'</td></tr>';
			$string .= '<tr><td>Возможность размещения рекламы '; if($i->can_reclame==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Дополнительная информация '.$i->about.'</td></tr>';
			break;
		  }
		  default:
			$string  = '<tr><td>Общая площадь помещения '.$i->total_item_sqr.'</td></tr>';
			$string .= '<tr><td>Состояние ремонта '.$i->repair.'</td></tr>';
			$string .= '<tr><td>Парковка '.$i->parking.'</td></tr>';
			$string .= '<tr><td>Количество закрепленных машиномест '.$i->fixed_qty.'</td></tr>';
			$string .= '<tr><td>Грузовой лифт '; if($i->service_lift==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Видеонаблюдение '; if($i->video==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Пропускная система '; if($i->access==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Система пожаротушения '; if($i->firefighting==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Интернет '; if($i->internet==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Провайдер '.$i->providers.'</td></tr>';
			$string .= '<tr><td>Телефония '; if($i->telephony==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Телефонная компания '.$i->tel_company.'</td></tr>';
			$string .= '<tr><td>Форма договора '.$i->contr_condition.'</td></tr>';
			$string .= '<tr><td>Бытовые помещения '.$i->welfare.'</td></tr>';
			$string  = '<tr><td>Коммунальные услуги '.$i->pay_utilities.'</td></tr>';
			$string .= '<tr><td>Системы отопления и водоснабжения '; if($i->heating==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Возможность рекламы '; if($i->can_reclame==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Емкость паллетного хранения '.$i->pallet_capacity.'</td></tr>';
			$string .= '<tr><td>Емкость полочного хранения '.$i->shelf_capacity.'</td></tr>';
			$string .= '<tr><td>Высота потолков '.$i->ceiling_height.'</td></tr>';
			$string .= '<tr><td>Рабочая высота помещения '.$i->work_height.'</td></tr>';
			$string .= '<tr><td>Кран-балка '; if($i->cathead==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Покрытие пола '.$i->flooring.'</td></tr>';
			$string .= '<tr><td>Нагрузка на пол '.$i->floor_load.'</td></tr>';
			$string .= '<tr><td>Система вентиляции '.$i->ventilation.'</td></tr>';
			$string .= '<tr><td>Кондиционирование '; if($i->air_condit==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Электр.мощность '.$i->el_power.'</td></tr>';
			$string .= '<tr><td>Количество ворот '.$i->gates_qty.'</td></tr>';
			$string .= '<tr><td>Высота ворот '.$i->gates_height.'</td></tr>';
			$string  = '<tr><td>Пандус '; if($i->rampant==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Автомобильные подъездные пути '; if($i->autoways==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Железнодорожные подъездные пути '; if($i->railways==1) $string .= $yes; else $string .= $no; $string .= '</td></tr>';
			$string .= '<tr><td>Дополнительная информация '.$i->about.'</td></tr>';
		}

		$html .= $string;
		$html .= '</table>';

		if (!empty($this->images)){			$html .= '<br />Фотографии:<br />';		  foreach ($this->images as $img){			$html .= '<img src="storage/images/'.$img->img.'" /><br />';		  }		}

		$mpdf = new mPDF('utf-8', 'A4', '8', '', 10, 10, 7, 7, 10, 10);
		$mpdf->charset_in = 'utf-8';

		$stylesheet = file_get_contents('css/pdf.css'); /*подключаем css*/
		$mpdf->WriteHTML($stylesheet, 1);

		$mpdf->list_indent_first_level = 0;
		$mpdf->WriteHTML($html, 2);
		$mpdf->Output('object_detail_'.$this->id.'.pdf', 'I');
    }
  }
?>