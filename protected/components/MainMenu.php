<?php
  class MainMenu extends CWidget{
    public function run(){
      $criteria = new CDbCriteria;
      $criteria->select = 'name, name_ru';
      $criteria->condition = 'menu=:var and name != :exception';
      $criteria->params = array(':var'=>'1', ':exception'=>'index');
      $criteria->order = 'sort';
      $Static_obj = Statics::model()->findAll($criteria);
      if (!empty($Static_obj)){        foreach ($Static_obj as $key=>$object){          $names_mass[] = $object->name;
          $namesRu_mass[] = $object->name_ru;        }
        $count_of_menu = count($Static_obj);      }
      $this->render('mainmenu', array(
                                      'count_of_menu' =>$count_of_menu,
                                      'names_mass'    =>$names_mass,
                                      'namesRu_mass'  =>$namesRu_mass,
                                      ));
    }
  }
?>