<?php
if (!empty($_POST['apikey']))
	define('SMSPILOT_APIKEY', $_POST['apikey']);
	
include('smspilot.php');

if (isset($_POST['sms'])) { // пришел текст из формы?
	
	if ( sms($_POST['phone'], $_POST['sms'], $_POST['from'] ) ) { // сообщение отправилось?
		
		$result = 'Ваше сообщение успешно отправлено, ответ вервера: '.sms_success();

		$status = sms_status();
		
		$ids = '';
		foreach ( $status as $s ) $ids .= $s['id'].',';
		
		$result .= '<p>ID: '.substr($ids,0,-1).'</p>';
		
		$result .= '<pre>'.print_r( $status , true ).'</pre>';
		
	} else
	
		$result = '<span style="color: red">Ошибка! ' . sms_error() .'</span>';
}

if (isset($_GET['info'])) { // проверка ключа
	
	if ($info = sms_info()) { // получаем данные о ключе
		
		$result_info = '<pre>';
		
		foreach( $info as $k => $v)
			$result_info .= "$k = $v\n";
		
		$result_info .= '</pre>';
		
	} else
		$result_info = '<span style="color: red">Ошибка! ' . sms_error() .'</span>';
}

if (isset($_GET['status'])) {
	if ($status = sms_check( $_POST['id'] )) {
		
		$result_status = '<table border="1"><tr><th>id</th><th>phone</th><th>zone</th><th>status</th></tr>';
		
		foreach( $status as $s)
			$result_status .= '<tr><td>'.$s['id'].'</td><td>'.$s['phone'].'</td><td>'.$s['zone'].'</td><td>'.$s['status'].'</td></tr>';
			
		$result_status .= '</table><br /><br />';

	} else {
		$result_status = '<span style="color: red">Ошибка! ' . sms_error() .'</span>';
	}
	
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SMS Pilot - Отправка СМС</title>
<style type="text/css">
<!--
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 13px;
}
a:link {
	color: #06C;
	text-decoration: underline;
}
a:visited {
	text-decoration: underline;
	color: #06C;
}
a:hover {
	text-decoration: none;
	color: #06C;
}
a:active {
	text-decoration: underline;
	color: #06C;
}
-->
</style>
</head>

<body>
<h1>Пример работы со шлюзом отправки СМС</h1>
<p><a href="http://www.smspilot.ru/apikey.php">http://www.smspilot.ru/apikey.php</a></p>
<h2>Отправить сообщение</h2>
<?php echo (isset($result)) ? $result : ''; ?>
<form action="?send" method="post">
API-ключ<br />
<input type="text" name="apikey" size="80" value="<?php echo (isset($_POST['apikey'])) ? $_POST['apikey'] : SMSPILOT_APIKEY; ?>" /> <a href="http://www.smspilot.ru/apikey.php" target="_blank">что это?</a><br />
<br />
Текст:<br />
<textarea name="sms" cols="60" rows="4"><?php echo (isset($_POST['sms'])) ? $_POST['sms'] : ''; ?></textarea><br />
<br />
Телефон:<br />
<input type="text" name="phone" value="<?php echo (isset($_POST['phone'])) ? $_POST['phone'] : ''; ?>" /><br />
<br />
Отправитель (не обязательно):<br />
<input type="text" name="from" value="<?php echo (isset($_POST['from'])) ? $_POST['from'] : ''; ?>" /><br />
<br />

<input type="submit" value="Отправить SMS" />
</form>
<h2>Проверить API-ключ</h2>
<form action="?info" method="post">
<?php echo (isset($result_info)) ? $result_info : ''; ?>
API-ключ<br />
<input type="text" name="apikey" size="80" value="<?php echo (isset($_POST['apikey'])) ? $_POST['apikey'] : SMSPILOT_APIKEY; ?>" /><br />
<br />
<input type="submit" value="Информация о ключе" />
</form>
<h2>Проверить статус SMS</h2>
<form action="?status" method="post">
<?php echo (isset($result_status)) ? $result_status : ''; ?>
API-ключ<br />
<input type="text" name="apikey" size="80" value="<?php echo (isset($_POST['apikey'])) ? $_POST['apikey'] : SMSPILOT_APIKEY; ?>" /><br />
<br />
ID:<br />
<input type="text" name="id" value="<?php echo (isset($_POST['id'])) ? $_POST['id'] : ''; ?>" /> можно несколько через запятую<br />

<input type="submit" value="Проверить статус SMS" />
</form>

</body>
</html>