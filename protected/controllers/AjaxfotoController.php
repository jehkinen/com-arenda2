<?php	class AjaxfotoController extends CController{


	public function actionUpload_T(){

		ini_set('memory_limit', '128M');
		if (filesize($_FILES['myfile']['tmp_name']) < (1024*1024*6)){ // Ограничение размера фотки в 6 мегабайт
		
			$uploaddir = 'storage/images/personal/temp/';
		
			$rand_part = date('His').rand(1000, 9999);
			$translit_obj = $this->widget('LoadFileName', array('in_text'=>$_FILES['myfile']['name'], 'len'=>100));
			$translit = $translit_obj->out_text;
			$name = substr($translit, 0, strrpos($translit, '.'));
			$ext  = substr($translit, strrpos($translit, '.'));
			$fName = $name.'_yyx'.$rand_part.$ext;
			$uploadfile = $uploaddir.basename($fName);
			$uploadfileorig = str_replace('_yyx', '_orig', $uploadfile);
			
			if (move_uploaded_file($_FILES['myfile']['tmp_name'], $uploadfile)){
				copy ($uploadfile, $uploadfileorig);
				$infoIMG = getimagesize($uploadfile);
				$width_out = $infoIMG[0];
				if (($width_out>1024) || ($infoIMG[1]>900)){
					require_once 'protected/extensions/watermark/WideImage.php';
					if ($width_out>1024) $width_out = 1024;
					$qualityIMG = 100;
					if ($infoIMG['mime'] == 'image/png') $qualityIMG = 1;
					WideImage::load($uploadfile)->resizeDown(1024, 900, 'inside')->saveToFile($uploadfile, $qualityIMG);
				}
				echo $fName.'|'.$width_out;
			}
		} 
		else echo false;	
	}

	public function actionCrop_T(){
	
		ini_set('memory_limit', '128M');
		if (!isset($_POST['file']) || empty($_POST['file']) || !isset($_POST['x1']) || !isset($_POST['y1']) || !isset($_POST['x2']) || !isset($_POST['y2'])
			//|| !isset(Yii::app()->user->id['user_id']) || empty(Yii::app()->user->id['user_id'])
			)
		CApplication::end();
		$x1 = $_POST['x1'];
		$y1 = $_POST['y1'];
		$x2 = $_POST['x2'];
		$y2 = $_POST['y2'];
		if (is_numeric($x1) && is_numeric($y1) && is_numeric($x2) && is_numeric($y2)) {
			$x1 = (int)ceil($x1);
			$y1 = (int)ceil($y1);
			$x2 = (int)ceil($x2);
			$y2 = (int)ceil($y2);
			$x1 = abs($x1);
			$y1 = abs($y1);
			$x2 = abs($x2);
			$y2 = abs($y2);
		} else CApplication::end();

		$filename = $_POST['file'];
		$file_temp = 'storage/images/personal/temp/'.$filename;
		$file_orig = str_replace('_yyx', '_orig', $file_temp); 

		if (file_exists($file_orig) && realpath($file_orig) && file_exists($file_temp) && realpath($file_temp)){
			@require_once 'protected/extensions/watermark/WideImage.php';

			$width = abs($x2 - $x1);
			$height = abs($y2 - $y1);

			$infoIMG = getimagesize($file_orig);
			if (($infoIMG[0]>1024) || ($infoIMG[1]>900)) { // Если длина или ширина оригинальной фотки превышеает установленную величину, то был resizeDown
				$tmpImg = getimagesize($file_temp);
				$scale = $infoIMG[1] / $tmpImg[1];
				$x1 = (int)ceil($x1*$scale);
				$y1 = (int)ceil($y1*$scale);
				$width = (int)ceil($width*$scale);
				$height = (int)ceil($height*$scale);				
			}
			$qualityIMG = 100;
			if ($infoIMG['mime'] == 'image/png') $qualityIMG = 1;
			WideImage::load($file_orig)->crop($x1, $y1, $width, $height)->resize(308, 231)->saveToFile('storage/images/personal/'.$filename, $qualityIMG);

			@unlink ($file_temp);

			/*
			$criteria = new CDbCriteria;
            $criteria->condition = 'id=:var';
            $criteria->params = array(':var'=>Yii::app()->user->id['user_id']);
            $Users_obj = User::model()->find($criteria);
			if (!empty($Users_obj)){
				if ($Users_obj->photo != '') @unlink ('storage/images/personal/'.$Users_obj->photo);
				$Users_obj->photo = $filename;
				$Users_obj->save(false);
			}
			*/
		}
	}

	public function actionCancel_T(){
	
		if (!isset($_POST['file']) || empty($_POST['file']) ||
			!isset($_POST['type']) || empty($_POST['type'])
			// || !isset(Yii::app()->user->id['user_id']) || empty(Yii::app()->user->id['user_id'])
			)
		CApplication::end();
		
		$type = $_POST['type'];
		
		if ($type == 'user') $basedir = 'storage/images/personal';
		elseif ($type == 'temp') $basedir = 'storage/images/personal/temp';
			else CApplication::end();
		
		$file_to_delete = $_POST['file'];
		$path = realpath($basedir.'/'.$file_to_delete);
		if ($path){
			$ext_allowed = array('jpg','png','jpeg','gif');
			$ext = mb_strtolower(substr(strrchr($file_to_delete, '.'), 1), 'UTF-8');
			if (!in_array($ext, $ext_allowed))
				CApplication::end();
			@unlink($path);
			$path = str_replace('_yyx', '_orig', $path); 
			@unlink($path);
			/*
			if ($type == 'user'){
				$criteria = new CDbCriteria;
				$criteria->condition = 'id=:var';
				$criteria->params = array(':var'=>Yii::app()->user->id['user_id']);
				$Users_obj = User::model()->find($criteria);
				if (!empty($Users_obj)){
					$Users_obj->photo = null;
					$Users_obj->save(false);
				}
			}
			*/
		}
	}

	public function actionDelete_T(){
	
		if (!isset($_POST['file']) || empty($_POST['file']))
			CApplication::end();
		
		$basedir = 'storage/images/personal/';
		$file = $_POST['file'];
		if (strpos($file, $basedir)===false)
			return false;
		if ($file[0]=='/')
			$file = substr($file, 1);
		$path = realpath($file);
		if ($path){
			$ext_allowed = array('jpg','png','jpeg','gif');
			$ext = mb_strtolower(substr(strrchr($file, '.'), 1), 'UTF-8');
			if (!in_array($ext, $ext_allowed))
				CApplication::end();
			//@unlink($path);
			$path = str_replace($basedir, $basedir.'temp/', $file);			
			//@unlink($path);
			$path = str_replace('_yyx', '_orig', $path); 
			//@unlink($path);
			// $this->widget('AjaxDebug', array('mess'=>$path));
			/*
			if ($type == 'user'){
				$criteria = new CDbCriteria;
				$criteria->condition = 'id=:var';
				$criteria->params = array(':var'=>Yii::app()->user->id['user_id']);
				$Users_obj = User::model()->find($criteria);
				if (!empty($Users_obj)){
					$Users_obj->photo = null;
					$Users_obj->save(false);
				}
			}
			*/
		}
	}

	public function actionDelete(){
	
		if (!isset($_POST['file']) || empty($_POST['file']))
			CApplication::end();
		$this->DeletePictures($_POST['file']);
	}

	private function DeletePictures ($file) {
		$dir_t = 'storage/images/_thumbs/';
		// Проверка на работу с файлами нужного каталога
		if (strpos($file, $dir_t)===false)
			return false;
		if ($file[0]=='/')	// Обработаем начальный слеш
			$file = substr($file, 1);
		// Выделим имя файла из относительного пути
		$file = str_replace($dir_t, '', $file);
		$criteria = new CDbCriteria;
		$criteria->alias = 'F';
		$criteria->select = 'O.owner_id as userId, F.id as id, F.img, F.name_small, F.name_medium, F.name_gallery';
		$criteria->join = 'join items O on O.id=F.id_item';
		$criteria->condition = 'F.name_gallery=:pr';
		$criteria->params = array(':pr'=>$file);
		$Fotos = Images::model()->find($criteria);
		if (empty($Fotos)) CApplication::end();
		// Сделаем проверку на допустимость текущего посетителя работать с этими фотографиями		
		//$criteria->params = array(':var'=>Yii::app()->user->id['user_id']);
		
		// Удалим последовательно фотографии, а затем и запись из БД
		@unlink(realpath('storage/images/'.$Fotos->img));
		@unlink(realpath($dir_t.$Fotos->name_small));
		@unlink(realpath($dir_t.$Fotos->name_medium));
		@unlink(realpath($dir_t.$Fotos->name_gallery));
		// Попытаемся удалить оригинальный файл нового образца
		@unlink(realpath('storage/images/'.str_replace('yyx_big_', 'orig', $Fotos->img)));
		$Fotos->delete();
		//$this->widget('AjaxDebug', array('mess'=>'Success'));
	}

	public function actionCancel(){

		if (!isset($_POST['file']) || empty($_POST['file']))	CApplication::end();

		$basedir = 'storage/images/temp/';		
		$file = $_POST['file'];
		$path = realpath($basedir.$file);
		if ($path){
			$ext_allowed = array('jpg','png','jpeg','gif');
			$ext = mb_strtolower(substr(strrchr($file, '.'), 1), 'UTF-8');
			if (!in_array($ext, $ext_allowed))
				CApplication::end();
			@unlink($path);
			$path = str_replace('_yyx', '_orig', $path); 
			$path = str_replace($basedir, 'storage/images/', $path); 
			@unlink($path);
		}
	}

	public function actionUpload(){

		ini_set('memory_limit', '128M');
		if (filesize($_FILES['myfile']['tmp_name']) < (1024*1024*6)){ // Ограничение размера фотки в 6 мегабайт
		
			$uploaddir = 'storage/images/temp/';

			$rand_part = date('His').rand(1000, 9999);
			$translit_obj = $this->widget('LoadFileName', array('in_text'=>$_FILES['myfile']['name'], 'len'=>100));
			$translit = $translit_obj->out_text;
			$name = substr($translit, 0, strrpos($translit, '.'));
			$ext  = substr($translit, strrpos($translit, '.'));
			$fName = $name.'_yyx'.$rand_part.$ext;
			$uploadfile = $uploaddir.basename($fName);
			$uploadfileorig = str_replace('_yyx', '_orig', $uploadfile);
			$uploadfileorig = str_replace($uploaddir, 'storage/images/', $uploadfileorig);
			
			if (move_uploaded_file($_FILES['myfile']['tmp_name'], $uploadfile)){
				copy ($uploadfile, $uploadfileorig);
				$infoIMG = getimagesize($uploadfile);
				$width_out = $infoIMG[0];
				if (($width_out>1024) || ($infoIMG[1]>900)){
					require_once 'protected/extensions/watermark/WideImage.php';
					if ($width_out>1024) $width_out = 1024;
					$qualityIMG = 100;
					if ($infoIMG['mime'] == 'image/png') $qualityIMG = 1;
					WideImage::load($uploadfile)->resizeDown(1024, 900, 'inside')->saveToFile($uploadfile, $qualityIMG);
				}
				echo $fName.'|'.$width_out;
			}
		} 
		else echo false;	
	}

	public function actionCrop(){
	
		ini_set('memory_limit', '128M');
		if (!isset($_POST['file']) || empty($_POST['file']) || !isset($_POST['x1']) || !isset($_POST['y1']) || !isset($_POST['x2']) || !isset($_POST['y2'])
			|| !isset($_POST['oid']) || empty($_POST['oid'] )
			//|| !isset(Yii::app()->user->id['user_id']) || empty(Yii::app()->user->id['user_id'])
		) Yii::app()->end();
		$x1 = $_POST['x1']; $y1 = $_POST['y1']; $x2 = $_POST['x2']; $y2 = $_POST['y2'];
		$id = $_POST['oid'];
		if (is_numeric($x1) && is_numeric($y1) && is_numeric($x2) && is_numeric($y2) && is_numeric($id)) {
			$x1 = (int)ceil($x1);
			$y1 = (int)ceil($y1);
			$x2 = (int)ceil($x2);
			$y2 = (int)ceil($y2);
			$x1 = abs($x1);
			$y1 = abs($y1);
			$x2 = abs($x2);
			$y2 = abs($y2);
		} else Yii::app()->end(); // с исп. CApplication::end почему то была ошибка

		$dir = 'storage/images/';
		$dir_t = $dir.'temp/';
		$filename = $_POST['file'];
		$file_temp = $dir_t.$filename;
		$file_orig = str_replace('_yyx', '_orig', $file_temp); 
		$file_orig = str_replace($dir_t, $dir, $file_orig); 

		$s = '';
		if (file_exists($file_orig) && realpath($file_orig) && file_exists($file_temp) && realpath($file_temp)){
			@require_once 'protected/extensions/watermark/WideImage.php';

			$width = abs($x2 - $x1);
			$height = abs($y2 - $y1);

			$infoIMG = getimagesize($file_orig);
			if (($infoIMG[0]>1024) || ($infoIMG[1]>900)) { // Если длина или ширина оригинальной фотки превышеает установленную величину, то был resizeDown
				$tmpImg = getimagesize($file_temp);
				$scale = $infoIMG[1] / $tmpImg[1];
				$x1 = (int)ceil($x1*$scale);
				$y1 = (int)ceil($y1*$scale);
				$width = (int)ceil($width*$scale);
				$height = (int)ceil($height*$scale);				
			}
			$qualityIMG = 100;
			if ($infoIMG['mime'] == 'image/png') $qualityIMG = 1;
			$f_small = str_replace('_yyx', '_yyx_small_', $filename);
			$f_med = str_replace('_yyx', '_yyx_medium_', $filename);

			$f_gal = $s = str_replace('_yyx', '_yyx_gallery_', $filename);
			$f_img = str_replace('_yyx', '_yyx_big_', $filename);
			if ($infoIMG[0]<1050)
				$watermark = WideImage::load('images/watermark200.png');
			else	$watermark = WideImage::load('images/watermark400.png');
			WideImage::load($file_orig)->crop($x1, $y1, $width, $height)->merge($watermark, 'right - 5', 'bottom - 5', 100)->resize(67, 51)->saveToFile($dir.'_thumbs/'.$f_small, $qualityIMG);
			WideImage::load($file_orig)->crop($x1, $y1, $width, $height)->merge($watermark, 'right - 5', 'bottom - 5', 100)->resize(400, 300)->saveToFile($dir.'_thumbs/'.$f_med, $qualityIMG);
			//WideImage::load($file_orig)->crop($x1, $y1, $width, $height)->merge($watermark, 'right - 5', 'bottom - 5', 100)->resize(113, 85)->saveToFile($dir.'_thumbs/'.$f_gal, $qualityIMG);
			WideImage::load($file_orig)->crop($x1, $y1, $width, $height)->merge($watermark, 'right - 5', 'bottom - 5', 100)->resize(70, 70)->saveToFile($dir.'_thumbs/'.$f_gal, $qualityIMG);
			WideImage::load($file_orig)->crop($x1, $y1, $width, $height)->merge($watermark, 'right - 5', 'bottom - 5', 100)->saveToFile($dir.$f_img, $qualityIMG);
			@unlink ($file_temp);
			// Занесем запись в БД
			$Fotos = new Images();
			$Fotos->img = $f_img;
			$Fotos->name_small = $f_small;
			$Fotos->name_medium = $f_med;
			$Fotos->name_gallery = $f_gal;
			$Fotos->id_item = $id;
			$Fotos->save(false);
			// Удалим предыдущую фотку (реализуем сценарий "Замена фотки")
			if (isset($_POST['old']) && !empty($_POST['old']))
				$this->DeletePictures($_POST['old']);			
		}
		echo $s;
	}
	

}?>