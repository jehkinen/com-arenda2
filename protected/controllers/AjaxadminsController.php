﻿<?php class AjaxadminsController extends CController{

	public function actionShowmail(){
		if (!isset($_POST['id'])) {
			echo 'Письмо не найдено';
			return;
		}
		$Mail = LogSendMessages::model()->findByPK($_POST['id']);
		if (empty($Mail))
			echo 'Ошибка при получении текста';
		else	echo $Mail->txt;
	}

	public function actionChange_user_state() {	
		if (!isset($_POST['id']) || !isset($_POST['stat']))
			throw new CHttpException(404);

		$user = AdminUsers::model()->findByPK($_POST['id']);
		if (empty($user))
			CApplication::end();

		//$this->widget('AjaxDebug', array('mess'=>$user->state));
		$user->state = $_POST['stat'];
		$user->save(false);	
	}

	#-----------------------------------------------
	# Модерирование объектов в админке
	#-----------------------------------------------

	public function actionAdmin_allow(){
		if (!isset($_POST['id']))
			throw new CHttpException(404);
		$id = substr($_POST['id'], 2);
		$item = Search::model()->findByPK($id);
		if (!empty($item)) {
			$item->status = 6;
			$item->status_changed = null;
			if ($item->save(false))
				$this->widget('ChangeItemStatus', array('itemId'=>$item->id));
		}
	}

	public function actionAdmin_disallow(){

		if (!isset($_POST['id']))
			throw new CHttpException(404);
		if (isset($_POST['msg']) && $_POST['msg']!='') {
			$filter = new CHtmlPurifier();
			$message = $filter->purify(trim($_POST['msg'])); # В этой переменной содержится текст из textarea
		} else $message = '';

		$id = substr($_POST['id'], 2);
		$item = Search::model()->findByPK($id);
		if (!empty($item)) {
			$item->status_changed = null;
			$item->status = 0;
			if ($item->save(false) && $message!='') {
				$user = User::model()->findByPK($item->owner_id);
				require_once('protected/extensions/mailer/class.mailer.php');
				// Отсылаем письмо
				$mail = new Mailer();
				$mail->IsHTML(true);
				$mail->From = Yii::app()->params->admin_mail;
				$mail->FromName = 'Уведомление';
				$mail->Subject  = 'Новый объект';
				$mail->Body = $message.'<br />'.Yii::app()->params->autoscriptum;
				$mail->AddAddress($user->e_mail);
				// $mail->AddAddress('dpriest@list.ru'); 
				if ($mail->Send())
					$succ = 1;
				else	$succ = 0;
				$cmd = Yii::app()->db->createCommand('Insert into log_send_messages (kind, success, reciever, address, txt) Values (2, '.$succ.', '.$user->id.', '."'$user->e_mail', '$message');");
				$cmd->execute();
			}
		}
	}

	public function actionsendPartersOffers(){

		$model = new Partnership;

		if(Yii::app()->getRequest()->getIsAjaxRequest())
		{
			echo CActiveForm::validate( array( $model));
			Yii::app()->end();
		}

		if(isset($_POST['Partnership']))
		{
		$model->attributes=$_POST['Partnership'];
		$message = $model->text;

		@require_once('protected/extensions/mailer/class.mailer.php');
		// Отсылаем письмо
		$mail = new Mailer();
		$mail->IsHTML(true);
		$mail->From = $model->email; ;
		$mail->FromName = $model->name;
		$mail->Subject  = 'Предложение партнёрства';
		$mail->Body = $message.'<br />'.Yii::app()->params->autoscriptum;
		$mail->AddAddress(Yii::app()->params->admin_mail);
			if ($mail->Send())
				$succ = 1;
			else	$succ = 0;
			$cmd = Yii::app()->db->createCommand('Insert into log_send_messages (kind, success, reciever, address, txt) Values (2, '.$succ.', 0, '."'$model->email', '$message');");
			$cmd->execute();
		$this->redirect('/');
		}
	}

	public function actionaddNewCityRequest($city, $email=NULL, $notificate=false ){
		//Пользователь не нашёл своего города в списке и попросил добавить его.
		if(Yii::app()->getRequest()->getIsAjaxRequest()){
			$filter = new CHtmlPurifier();

			$city = $filter->purify($city);
			$message = 'Пользователь просил добавить город: '.$city.'<br />';

			if($notificate=='yes'){
				$email = $filter->purify($email);
				$message .= 'Уведомить пользователя после добавления города по адресу: '.$email."\n";
			}

			@require_once('protected/extensions/mailer/class.mailer.php');
			// Отсылаем письмо
			$mail = new Mailer();
			$mail->IsHTML(true);

			$mail->FromName  = ($notificate=='yes') ? $email: '';
			$mail->Subject  = 'Заявка на добавление нового города: ';
			$mail->Body = $message;
			$admin_mail = Yii::app()->params->admin_mail;
			//$admin_mail = 'jackdancem@gmail.com';
			$mail->AddAddress($admin_mail);
			$mail->Send();
   		    echo json_encode('Спасибо, ваша заявка на добавление города принята!');

		}
		return false;
	}


public function filters(){
		return array(
			array(
				'application.filters.AccessFilter - sendPartersOffers, addNewCityRequest',
				'role'=>'admin'
			),
		);
	}

}?>