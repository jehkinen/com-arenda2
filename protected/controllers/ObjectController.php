<?php class ObjectController extends CController{

	public $pageDescription;
	public $pageKeywords;
	public $layout = 'inner01';

	public function actionAdd(){

		# --валидация переменной step--
		if (!isset($_GET['step']) || empty($_GET['step'])){
			$this->widget('Clear_session');
			$this->redirect(array('object/add_error'));
		}

		$usr = Yii::app()->user;
		$step = $_GET['step'];

		$has_errors = false;
		switch ($step){
			case 'step1':{break;}
			case 'step2':{
				if (!$usr->hasState('ses_kind'))
					$has_errors = true;
				break;}
			case 'step3':{
				if (!$usr->hasState('ses_class')) // Проверка на прохождение предыдущего этапа
					$has_errors = true;
				break;}
			case 'step4':{
				if (!$usr->hasState('ses_price'))
					$has_errors = true;
				break;}
			case 'step5':{
				if (!$usr->hasState('ses_firefi'))
					$has_errors = true;
				break;}
			default:{$has_errors = true;}
		}

		if ($has_errors){
			$this->widget('Clear_session');
			$this->redirect(array('object/add_error'));
		}
		#------------------------------

		$this->pageTitle = 'Добавление нового объекта';
		$this->pageDescription = '';
		$this->pageKeywords = '';

		switch($step){
		case 'step1':{

			$form = new Objects_add('step1');

			if (!empty($_POST['Objects_add'])){
				$form->attributes = $_POST['Objects_add'];
				if (!$usr->hasState('ses_kind'))
					$usr->setState('ses_kind', $_POST['Objects_add']['kind']);
				$usr->setState('ses_itype', $_POST['Objects_add']['itemType']);
				$usr->setState('ses_parent', $_POST['Objects_add']['parent']);
				$usr->setState('ses_city', $_POST['Objects_add']['city']);
				if ($form->validate()){
					$this->redirect(array('object/add', 'step'=>'step2'));
				}
			}

			$listKinds = array(0=>'офис', 1=>'торговое', 2=>'Склад\Производство');
			if ($usr->isGuest)
				$listTypes = array(0=>'Помещение');
			else $listTypes = array(0=>'Отдельное помещение', 1=>'Много помещений в одном месте (создать центр недвижимости)', 2=>'Добавить помещение в существующий центр недвижимости');
			$listItems = null;
			// Устанавливаем значение по умолчанию или выставляем предыдущее выбранное
			$form->itemType = $usr->getState('ses_itype', 0);
			$form->kind = $usr->getState('ses_kind');
			// При возврате на первый шаг, установим выбранный пользователем Родительский объект
			if (($form->itemType==2) && ($usr->hasState('ses_parent'))) {
				$criteria = new CDbCriteria;
				$criteria->alias = 'I';
				$criteria->select = 'I.id, I.city, S.name as street, I.house';
				$criteria->join = 'join streets S on S.id=I.street';
				$criteria->condition = 'owner_id=:oi and parent=:pr';
				$criteria->params = array(':oi'=>$usr->id['user_id'], ':pr'=>0);
				$criteria->order = 'I.city, S.name, I.house, I.id';
				$Items = Search::model()->findAll($criteria);
				if (!empty($Items)) {
					$listItems = array();
					foreach($Items as $it)
						$listItems[$it->id] = $it->city.', ул. '.$it->street.' дом '.$it->house;
					$form->parent = $usr->getState('ses_parent');
				}
			}
			$criteria = new CDbCriteria;
			$list_city = CHtml::listData(City::model()->findAll($criteria), 'name', 'name');

			$this->render('add1', array(
				'listKinds'	=>$listKinds,
				'listTypes'	=>$listTypes,
				'listItems'	=>$listItems,
				'form'		=>$form,
				'list_city'	=>$list_city,
				'vcity'		=>$usr->getState('ses_city', '')
			));

			break;
		}

		case 'step2':{

			$form = new Objects_add('step2');
			ini_set('memory_limit', '64M');

			if (!empty($_POST['Objects_add'])){
				$form->attributes = $_POST['Objects_add'];
				$form->kind = $usr->getState('ses_kind', 0);
				$form->itemType = $usr->getState('ses_itype', 0);
				if (isset($usr->ses_hasparent) && $usr->getState('ses_hasparent')==1) {
					// Если у объекта есть родитель, значит он унаследовал значения ряда атбрибутов, которые хранятся в сессии
					$form->kind_structure = $usr->getState('ses_struc');
					$form->class = $usr->getState('ses_class');
					//$form->name = $usr->getState('ses_name');
					$form->district = $usr->getState('ses_distr');
					$form->street = $usr->getState('ses_strname');
					$form->house = $usr->getState('ses_house');
					$form->storeys = $usr->getState('ses_storeys');
				} else {
					$usr->setState('ses_struc', $_POST['Objects_add']['kind_structure']);
					$usr->setState('ses_distr', $_POST['Objects_add']['district']);
					$usr->setState('ses_strname', $_POST['Objects_add']['street']);
					$usr->setState('ses_house', $_POST['Objects_add']['house']);
					$usr->setState('ses_storeys', $_POST['Objects_add']['storeys']);
					$usr->setState('ses_class', $_POST['Objects_add']['class']);
				}
				$usr->setState('ses_name', $_POST['Objects_add']['name']);
				$usr->setState('ses_func', $_POST['Objects_add']['functionality']);
				$usr->setState('ses_housing', $_POST['Objects_add']['housing']);
				$usr->setState('ses_storey', $_POST['Objects_add']['storey']);
				if ($form->validate()){ // Как правильно написать проверку на зависимость одного поля от другого? Например. если "этаж" больше "этажности"??
					$this->redirect(array('object/add', 'step'=>'step3'));
				}
			}

			// Если выбран Родительский объект, получим и унаследуем его атрибуты
			if (($usr->getState('ses_itype')==2) && ($usr->hasState('ses_parent')))
				$this->LoadParentAttribs();
			else unset($usr->ses_hasparent);

			$list_strucs = CHtml::listData(KindStructure::model()->findAll(), 'id', 'shname');
			array_unshift($list_strucs, 'Не указан');
			$list_funcs = CHtml::listData(Functionality::model()->findAll(), 'id', 'shname');
			$list_class = CHtml::listData(Classes::model()->findAll(), 'id', 'shname');
			$form->city = $usr->getState('ses_city', '');
			$criteria = new CDbCriteria;
			$criteria->order = 'name';
			$criteria->condition = 'city=:c';
			$criteria->params = array(':c'=>$form->city);
			$list_distrs = CHtml::listData(District::model()->findAll($criteria), 'id', 'name');
			$list_streets = CHtml::listData(Street::model()->findAll($criteria), 'id', 'name');
			$this->render('add2', array(
				'form'			=>$form,
				'list_strucs'	=>$list_strucs,
				'vstruc'		=>$usr->getState('ses_struc', ''),
				'list_funcs'	=>$list_funcs,
				'vfunc'			=>$usr->getState('ses_func', ''),
				'list_class'	=>$list_class,
				'vclass'		=>$usr->getState('ses_class', ''),
				'vname'			=>$usr->getState('ses_name', ''),
				'list_distrs'	=>$list_distrs,
				'vdistr'		=>$usr->getState('ses_distr', ''),
				'list_streets'	=>$list_streets,
				'vstrname'		=>$usr->getState('ses_strname', ''),
				'vhouse'		=>$usr->getState('ses_house', ''),
				'vhousing'		=>$usr->getState('ses_housing', ''),
				'vstorey'		=>$usr->getState('ses_storey', ''),
				'vstoreys'		=>$usr->getState('ses_storeys', ''),
				'kind'			=>$usr->getState('ses_kind', 0),
				'item_type'		=>$usr->getState('ses_itype', 0),
				'hasparent'		=>$usr->getState('ses_hasparent', 0)
			));

			break;
		}
		case 'step3':{

			$form = new Objects_add('step3');

			if (!empty($_POST['Objects_add'])){
				$form->attributes = $_POST['Objects_add'];
				$form->kind = $usr->getState('ses_kind', 0);
				$form->itemType = $usr->getState('ses_itype', 0);
				$usr->setState('ses_cconds', $_POST['Objects_add']['contr_condition']);
				$usr->setState('ses_price', $_POST['Objects_add']['price']);
				$usr->setState('ses_putils', $_POST['Objects_add']['pay_utilities']);
				$usr->setState('ses_tisqr', $_POST['Objects_add']['total_item_sqr']);
				$usr->setState('ses_slift', $_POST['Objects_add']['service_lift']);
				$usr->setState('ses_inet', $_POST['Objects_add']['internet']);
				$usr->setState('ses_providers', $_POST['Objects_add']['providers']);
				$usr->setState('ses_telephony', $_POST['Objects_add']['telephony']);
				$usr->setState('ses_tcomp', $_POST['Objects_add']['tel_company']);
				$usr->setState('ses_cellh', $_POST['Objects_add']['ceiling_height']);
				$usr->setState('ses_workh', $_POST['Objects_add']['work_height']);
				$usr->setState('ses_cathead', $_POST['Objects_add']['cathead']);
				$usr->setState('ses_fload', $_POST['Objects_add']['floor_load']);
				$usr->setState('ses_epower', $_POST['Objects_add']['el_power']);
				$usr->setState('ses_gatesq', $_POST['Objects_add']['gates_qty']);
				$usr->setState('ses_gatesh', $_POST['Objects_add']['gates_height']);

				if ($form->validate()){ // Обработка десятичного разделителя явл. запятой ??
					$this->redirect(array('object/add', 'step'=>'step4'));
				}
			}
			$list_cconds = CHtml::listData(ContrForms::model()->findAll(), 'id', 'shname');
			array_unshift($list_cconds, 'Не указано');
			$list_putils = CHtml::listData(PayUtil::model()->findAll(), 'id', 'shname');
			array_unshift($list_putils, 'Не указано');
			$form->city = $usr->getState('ses_city', '');
			$criteria = new CDbCriteria;
			$criteria->order = 'shname';
			$criteria->condition = 'city=:c';
			$criteria->params = array(':c'=>$form->city);
			$list_providers = CHtml::listData(NetProvider::model()->findAll($criteria), 'id', 'shname');
			array_unshift($list_providers, 'Не важно');
			$list_tcomp = CHtml::listData(TelCompany::model()->findAll($criteria), 'id', 'shname');
			array_unshift($list_tcomp, 'Не важно');
			$this->render('add3', array(
				'form'			=>$form,
				'list_cconds'	=>$list_cconds,
				'vcconds'		=>$usr->getState('ses_cconds', ''),
				'vprice'		=>$usr->getState('ses_price', ''),
				'list_putils'	=>$list_putils,
				'vputils'		=>$usr->getState('ses_putils', ''),
				'vtisqr'		=>$usr->getState('ses_tisqr', ''),
				'vslift'		=>$usr->getState('ses_slift', ''),
				'vinet'			=>$usr->getState('ses_inet', ''),
				'list_providers'=>$list_providers,
				'vproviders'	=>$usr->getState('ses_providers', ''),
				'vtelephony'	=>$usr->getState('ses_telephony', ''),
				'list_tcomp'	=>$list_tcomp,
				'vtcomp'		=>$usr->getState('ses_tcomp', ''),
				'vcellh'		=>$usr->getState('ses_cellh', ''),
				'vworkh'		=>$usr->getState('ses_workh', ''),
				'vcathead'		=>$usr->getState('ses_cathead', ''),
				'vfload'		=>$usr->getState('ses_fload', ''),
				'vepower'		=>$usr->getState('ses_epower', ''),
				'vgatesq'		=>$usr->getState('ses_gatesq', ''),
				'vgatesh'		=>$usr->getState('ses_gatesh', ''),
				'kind'			=>$usr->getState('ses_kind', 0),
				'item_type'		=>$usr->getState('ses_itype', 0),
				'hasparent'		=>$usr->getState('ses_hasparent', 0)
			));

			break;
		}

		case 'step4':{

			$form = new Objects_add('step4');

			if (!empty($_POST['Objects_add'])){
				$form->attributes = $_POST['Objects_add'];
				$form->kind = $usr->getState('ses_kind', 0);
				$form->itemType = $usr->getState('ses_itype', 0);
				$usr->setState('ses_repair', $_POST['Objects_add']['repair']);
				$usr->setState('ses_park', $_POST['Objects_add']['parking']);
				$usr->setState('ses_fixqty', $_POST['Objects_add']['fixed_qty']);
				$usr->setState('ses_video', $_POST['Objects_add']['video']);
				$usr->setState('ses_access', $_POST['Objects_add']['access']);
				$usr->setState('ses_welfare', $_POST['Objects_add']['welfare']);
				$usr->setState('ses_heating', $_POST['Objects_add']['heating']);
				$usr->setState('ses_palcap', $_POST['Objects_add']['pallet_capacity']);
				$usr->setState('ses_shelcap', $_POST['Objects_add']['shelf_capacity']);
				$usr->setState('ses_flooring', $_POST['Objects_add']['flooring']);
				$usr->setState('ses_ventil', $_POST['Objects_add']['ventilation']);
				$usr->setState('ses_aircon', $_POST['Objects_add']['air_condit']);
				$usr->setState('ses_firefi', $_POST['Objects_add']['firefighting']);
				$usr->setState('ses_reklam', $_POST['Objects_add']['can_reclame']);
				$usr->setState('ses_rampant', $_POST['Objects_add']['rampant']);
				$usr->setState('ses_aways', $_POST['Objects_add']['autoways']);
				$usr->setState('ses_rways', $_POST['Objects_add']['railways']);
				$filter = new CHtmlPurifier();
				$s = $filter->purify($_POST['Objects_add']['about']);
				if (!empty($s))
					$s = trim(htmlspecialchars($s, ENT_QUOTES));
				if (empty($s)) 
						$usr->setState('ses_about', '');
				else	$usr->setState('ses_about', $s);

				if ($form->validate()){ // Обработка десятичного разделителя явл. запятой ??
					$this->redirect(array('object/add', 'step'=>'step5'));
				}
			}
			$list_repair = CHtml::listData(Repair::model()->findAll(), 'id', 'shname');
			array_unshift($list_repair, 'Не указана');
			$list_park = CHtml::listData(Parking::model()->findAll(), 'id', 'shname');
			array_unshift($list_park, 'Не указана');
			$list_flooring = CHtml::listData(Flooring::model()->findAll(), 'id', 'shname');
			array_unshift($list_flooring, 'Не указано');
			$list_ventil = CHtml::listData(Ventilation::model()->findAll(), 'id', 'shname');
			array_unshift($list_ventil, 'Не указана');
			$this->render('add4', array(
				'form'			=>$form,
				'list_repair'	=>$list_repair,
				'vrepair'		=>$usr->getState('ses_repair', ''),
				'list_park'		=>$list_park,
				'vpark'			=>$usr->getState('ses_park', ''),
				'vfixqty'		=>$usr->getState('ses_fixqty', ''),
				'vvideo'		=>$usr->getState('ses_video', ''),
				'vaccess'		=>$usr->getState('ses_access', ''),
				'vwelfare'		=>$usr->getState('ses_welfare', ''),
				'vheating'		=>$usr->getState('ses_heating', ''),
				'vpalcap'		=>$usr->getState('ses_palcap', ''),
				'vshelcap'		=>$usr->getState('ses_shelcap', ''),
				'list_flooring'	=>$list_flooring,
				'vflooring'		=>$usr->getState('ses_flooring', ''),
				'list_ventil'	=>$list_ventil,
				'vventil'		=>$usr->getState('ses_ventil', ''),
				'vaircon'		=>$usr->getState('ses_aircon', ''),
				'vfirefi'		=>$usr->getState('ses_firefi', ''),
				'vreklam'		=>$usr->getState('ses_reklam', ''),
				'vrampant'		=>$usr->getState('ses_rampant', ''),
				'vaways'		=>$usr->getState('ses_aways', ''),
				'vrways'		=>$usr->getState('ses_rways', ''),
				'vabout'		=>$usr->getState('ses_about', ''),
				'kind'			=>$usr->getState('ses_kind', 0),
				'item_type'		=>$usr->getState('ses_itype', 0),
				'hasparent'		=>$usr->getState('ses_hasparent', 0)
			));
			break;
		}

		case 'step5':{
			ini_set('memory_limit', '64M');
			ini_set('upload_max_filesize', '6M');
			$form = new Objects_add('step5');

			if (!empty($_POST['Objects_add'])){
				$form->attributes = $_POST['Objects_add'];
/*
				$form->image[] = CUploadedFile::getInstance($form,'[1]image');
				$form->image[] = CUploadedFile::getInstance($form,'[2]image');
				$form->image[] = CUploadedFile::getInstance($form,'[3]image');
				$form->image[] = CUploadedFile::getInstance($form,'[4]image');
				$form->image[] = CUploadedFile::getInstance($form,'[5]image');
*/
				$form->docums[] = CUploadedFile::getInstance($form,'[1]docums');
				$form->docums[] = CUploadedFile::getInstance($form,'[2]docums');
				$form->docums[] = CUploadedFile::getInstance($form,'[3]docums');
				$form->docname[] = trim($_POST['Objects_add'][1]['docname']);
				$form->docname[] = trim($_POST['Objects_add'][2]['docname']);
				$form->docname[] = trim($_POST['Objects_add'][3]['docname']);

				if ($form->validate()){

					$form->status = 3;  // При добавлении нового объекта выставляем ему статус "ожидает модерации"
					$form->parent = $usr->getState('ses_parent', 0);
					$form->kind = $usr->getState('ses_kind', 0);
					$form->itemType = $usr->getState('ses_itype', 0);
					$form->city = $usr->getState('ses_city', '');
					$form->kind_structure = $usr->getState('ses_struc', '');
					$form->functionality = $usr->getState('ses_func', '');
					$form->class = $usr->getState('ses_class', '');
					$form->name = $usr->getState('ses_name', '');
					$form->district = $usr->getState('ses_distr', '');
					$form->street = $usr->getState('ses_strname', '');
					$form->house = $usr->getState('ses_house', '');
					$form->housing = $usr->getState('ses_housing', '');
					$form->storey = $usr->getState('ses_storey', '');
					$form->storeys = $usr->getState('ses_storeys', '');
					$form->contr_condition = $usr->getState('ses_cconds', '');
					$form->about = $usr->getState('ses_about', '');
					$form->pay_utilities = $usr->getState('ses_putils', '');
					$form->service_lift = $usr->getState('ses_slift', 0);
					$form->internet = $usr->getState('ses_inet', 0);
					$form->providers = $usr->getState('ses_providers', '');
					$form->telephony = $usr->getState('ses_telephony', 0);
					$form->tel_company = $usr->getState('ses_tcomp', '');
					$form->cathead = $usr->getState('ses_cathead', 0);
					$form->gates_qty = $usr->getState('ses_gatesq', '');

					$form->repair = $usr->getState('ses_repair', '');
					$form->parking = $usr->getState('ses_park', '');
					$form->fixed_qty = $usr->getState('ses_fixqty', '');
					$form->video = $usr->getState('ses_video', '');
					$form->access = $usr->getState('ses_access', 0);
					$form->welfare = $usr->getState('ses_welfare', '');
					$form->heating = $usr->getState('ses_heating', '');
					$form->pallet_capacity = $usr->getState('ses_palcap', '');
					$form->shelf_capacity = $usr->getState('ses_shelcap', '');
					$form->flooring = $usr->getState('ses_flooring', '');
					$form->ventilation = $usr->getState('ses_ventil', '');
					$form->air_condit = $usr->getState('ses_aircon', 0);
					$form->firefighting = $usr->getState('ses_firefi', 0);
					$form->can_reclame = $usr->getState('ses_reklam', 0);
					$form->rampant = $usr->getState('ses_rampant', '');
					$form->autoways = $usr->getState('ses_aways', 0);
					$form->railways = $usr->getState('ses_rways', 0);

					# Для удобства отделил чистолые значения (type=text) от остальных
					# Для винды надо четко указывать что пришло знаечение NULL для числовых полей,
					# иначе приходит значение '' что не может быть действительным для числа

					if ($usr->getState('ses_price', '') != '') $form->price = $usr->getState('ses_price');
					else $form->price = NULL;

					if ($usr->getState('ses_tisqr', '') != '') $form->total_item_sqr = $usr->getState('ses_tisqr');
					else $form->total_item_sqr = NULL;

					if ($usr->getState('ses_cellh', '') != '') $form->ceiling_height = $usr->getState('ses_cellh');
					else $form->ceiling_height = NULL;

					if ($usr->getState('ses_workh', '') != '') $form->work_height = $usr->getState('ses_workh');
					else $form->work_height = NULL;

					if ($usr->getState('ses_fload', '') != '') $form->floor_load = $usr->getState('ses_fload');
					else $form->floor_load = NULL;

					if ($usr->getState('ses_epower', '') != '') $form->el_power = $usr->getState('ses_epower');
					else $form->el_power = NULL;

					if ($usr->getState('ses_gatesh', '') != '') $form->gates_height = $usr->getState('ses_gatesh');
					else $form->gates_height = NULL;

					if ($usr->getState('ses_fixqty', '') != '') $form->fixed_qty = $usr->getState('ses_fixqty');
					else $form->fixed_qty = NULL;

					if ($usr->getState('ses_palcap', '') != '') $form->pallet_capacity = $usr->getState('ses_palcap');
					else $form->pallet_capacity = NULL;

					if ($usr->getState('ses_shelcap', '') != '') $form->shelf_capacity = $usr->getState('ses_palcap');
					else $form->shelf_capacity = NULL;

					if (!$usr->isGuest && $usr->id['user_role'] == 'owner')
						$form->owner_id = $usr->id['user_id'];

					if ($form->save(false)){
						#----------------------------Фото-----------------------------
						if (!empty($form->image)){
							foreach ($form->image as $file){
								if (!empty($file)){

									$translit_obj = $this->widget('LoadFileName', array('in_text'=>$file->name, 'len'=>200));
									$img_translit = $translit_obj->out_text;
									$img_translit_name = substr($img_translit, 0, strrpos($img_translit, '.'));
									$img_translit_expansion = substr($img_translit, strrpos($img_translit, '.'));
									$random = mt_rand(10000, 99999);
									$img_name = $img_translit_name.'_'.$random.$img_translit_expansion;
									$img_name_medium = $img_translit_name.'_'.$random.'_medium'.$img_translit_expansion;
									$img_name_small = $img_translit_name.'_'.$random.'_small'.$img_translit_expansion;
									$img_name_gallery = $img_translit_name.'_'.$random.'_gallery'.$img_translit_expansion;
									if ($file->saveAs('storage/images/'.$img_name)){
										$image_add = new Images;
										$image_add->id_item = $form->id;
										$image_add->img = $img_name;
										$image_add->name_medium = $img_name_medium;
										$image_add->name_small = $img_name_small;
										$image_add->name_gallery = $img_name_gallery;
										$image_add->save(false);
										$chek_medium = $this->widget('ImgResize', array('src'=>'storage/images/'.$img_name, 'dest'=>'storage/images/_thumbs/'.$img_name_medium, 'width'=>300, 'height'=>3000));
										$chek_small = $this->widget('ImgResize', array('src'=>'storage/images/'.$img_name, 'dest'=>'storage/images/_thumbs/'.$img_name_small, 'width'=>500, 'height'=>50));
										$chek_small = $this->widget('ImgResize', array('src'=>'storage/images/'.$img_name, 'dest'=>'storage/images/_thumbs/'.$img_name_gallery, 'width'=>850, 'height'=>85));
									}
								}
							}
						}
						#-------------------------Прикрепленные файлы------------------------------------
						if (!empty($form->docums)) {
							$docs = $form->docums;
							$names= $form->docname;
							for ($i=0;$i<3;$i++) {
								if (!empty($docs[$i])) {
									$translit_obj = $this->widget('LoadFileName', array('in_text'=>$docs[$i]->name, 'len'=>200));
									$translit = $translit_obj->out_text;
									$f_name = substr($translit, 0, strrpos($translit, '.'));
									$f_ext = substr($translit, strrpos($translit, '.'));
									$random = mt_rand(10000, 99999);
									$name = $f_name.'_'.$random.$f_ext;
									if ($docs[$i]->saveAs('storage/files/objs/'.$name)) {
										$rec = new Attach;
										$rec->item = $form->id;
										$rec->name = $names[$i];
										$rec->filename = $name;
										$rec->save(false);
									}
								}
							}
						}
						#------------------------- / Прикрепленные файлы------------------------------------
						$this->widget('Clear_session');
						$this->redirect(array('object/adding/id/'.$form->id));
					}
				}
			}
			$this->render('add5', array(
				'form'		=>$form,
				'kind'		=>$usr->getState('ses_kind', 0),
				'hasparent'	=>$usr->getState('ses_hasparent', 0)
			));
			break;
		}
		}
	}

	private function LoadParentAttribs () {
		$usr = Yii::app()->user;
		// Если выбран Родительский объект, получим и унаследуем его атрибуты
		$usr->setState('ses_hasparent', 1);
		$Parent = Search::model()->findByPK($usr->getState('ses_parent'));
		if (!empty($Parent)) { // Уточнить список наследуемых атрибутов
			$usr->setState('ses_city', $Parent->city);
			$usr->setState('ses_struc', $Parent->kind_structure);
			//$usr->setState('ses_func', $Parent->functionality);
			$usr->setState('ses_class', $Parent->class);
			//$usr->setState('ses_name', $Parent->name);
			$usr->setState('ses_distr', $Parent->district);
			$usr->setState('ses_strname', $Parent->street);
			$usr->setState('ses_house', $Parent->house);
			$usr->setState('ses_housing', $Parent->housing);
			//$usr->setState('ses_storey', $Parent->storey);
			$usr->setState('ses_storeys', $Parent->storeys);

			//$usr->setState('ses_cconds', $Parent->contr_condition);
			//$usr->setState('ses_about', $Parent->about);
			//$usr->setState('ses_price', $Parent->price);
			//$usr->setState('ses_putils', $Parent->pay_utilities);
			//$usr->setState('ses_tisqr', $Parent->total_item_sqr);
			$usr->setState('ses_slift', $Parent->service_lift);
			//$usr->setState('ses_inet', $Parent->internet);
			//$usr->setState('ses_providers', $Parent->providers);
			//$usr->setState('ses_telephony', $Parent->telephony);
			//$usr->setState('ses_tcomp', $Parent->tel_company);
			$usr->setState('ses_cellh', $Parent->ceiling_height);
			$usr->setState('ses_workh', $Parent->work_height);
			$usr->setState('ses_cathead', $Parent->cathead);
			$usr->setState('ses_fload', $Parent->floor_load);
			$usr->setState('ses_epower', $Parent->el_power);
			$usr->setState('ses_gatesq', $Parent->gates_qty);
			$usr->setState('ses_gatesh', $Parent->gates_height);

			//$usr->setState('ses_repair', $Parent->repair);
			$usr->setState('ses_park', $Parent->parking);
			$usr->setState('ses_fixqty', $Parent->fixed_qty);
			$usr->setState('ses_video', $Parent->video);
			$usr->setState('ses_access', $Parent->access);
			//$usr->setState('ses_welfare', $Parent->welfare);
			$usr->setState('ses_heating', $Parent->heating);
			//$usr->setState('ses_palcap', $Parent->pallet_capacity);
			//$usr->setState('ses_shelcap', $Parent->shelf_capacity);
			//$usr->setState('ses_flooring', $Parent->flooring);
			$usr->setState('ses_ventil', $Parent->ventilation);
			$usr->setState('ses_aircon', $Parent->air_condit);
			$usr->setState('ses_firefi', $Parent->firefighting);
			$usr->setState('ses_reklam', $Parent->can_reclame);
			$usr->setState('ses_rampant', $Parent->rampant);
			$usr->setState('ses_aways', $Parent->autoways);
			$usr->setState('ses_rways', $Parent->railways);
		}
	}

	public function actionEdit(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$oObject = Objects_edit::model()->findByPK($get->id_out);
		if (empty($oObject))
			throw new CHttpException(404);
		// Проверка на принадлежность объекта пользователю.
		if ((Yii::app()->user->id['user_role'] != 'admin') && ($oObject->owner_id != Yii::app()->user->id['user_id'])) {
			$this->render('filter-edit-tenant');
			return 0;
		}
		if (!empty($_POST['Objects_edit'])){
			$oObject->attributes = $_POST['Objects_edit'];
			$filter = new CHtmlPurifier();
			$s = $filter->purify($oObject->about);
			if (!empty($s))
				$s = trim(htmlspecialchars($s, ENT_QUOTES));
			if (empty($s)) 
					$oObject->about = null;
			else	$oObject->about = $s;
			//CVarDumper::Dump($oObject, 3, true); die();
			if($oObject->validate()){
				// Сбросить статус в Ожидает модерации и Дату обновления статуса
				$oObject->status = 3;
				$oObject->status_changed = new CDbExpression('NOW()');
				if ($oObject->save(false)) {
					$this->redirect(array('owner/view'), false, 302);
					// Удалим (если есть) файл-архив с описанием объекта
					$s = Yii::app()->params->web_root;
					$i = strrpos($s, '/'); $a = strlen($s)-1;
					if ($i != $a)	$s.='/';
					$a = $s.'storage/files/zipobj/obj'.$oObject->id.'.zip';
					if (file_exists($a))
						@unlink ($a);
					// Удалим (если есть) pdf-файл с описанием объекта
					$a = $s.'storage/files/pdfobj/obj'.$oObject->id.'.pdf';
					if (file_exists($a))
						@unlink ($a);
				}
			}
		}

		ini_set('memory_limit', '64M');
		$list_strucs = CHtml::listData(KindStructure::model()->findAll(), 'id', 'shname');
		array_unshift($list_strucs, 'Не указан');
		$list_funcs = CHtml::listData(Functionality::model()->findAll(), 'id', 'shname');
		$list_class = CHtml::listData(Classes::model()->findAll(), 'id', 'shname');
		$criteria = new CDbCriteria;
		$criteria->order = 'name';
		$criteria->condition = 'city=:c';
		$criteria->params = array(':c'=>$oObject->city);
		$list_distrs = CHtml::listData(District::model()->findAll($criteria), 'id', 'name');
		$list_streets = CHtml::listData(Street::model()->findAll($criteria), 'id', 'name');

		$list_cconds = CHtml::listData(ContrForms::model()->findAll(), 'id', 'shname');
		array_unshift($list_cconds, 'Не указано');
		$list_putils = CHtml::listData(PayUtil::model()->findAll(), 'id', 'shname');
		array_unshift($list_putils, 'Не указано');
		$list_providers = CHtml::listData(NetProvider::model()->findAll($criteria), 'id', 'shname');
		array_unshift($list_providers, 'Не важно');
		$list_tcomp = CHtml::listData(TelCompany::model()->findAll($criteria), 'id', 'shname');
		array_unshift($list_tcomp, 'Не важно');

		$list_repair = CHtml::listData(Repair::model()->findAll(), 'id', 'shname');
		array_unshift($list_repair, 'Не указана');
		$list_park = CHtml::listData(Parking::model()->findAll(), 'id', 'shname');
		array_unshift($list_park, 'Не указана');
		$list_flooring = CHtml::listData(Flooring::model()->findAll(), 'id', 'shname');
		array_unshift($list_flooring, 'Не указано');
		$list_ventil = CHtml::listData(Ventilation::model()->findAll(), 'id', 'shname');
		array_unshift($list_ventil, 'Не указана');


		$this->render('edit', array('form' =>$oObject,
			'list_strucs'	=>$list_strucs,
			'list_funcs'	=>$list_funcs,
			'list_class'	=>$list_class,
			'list_distrs'	=>$list_distrs,
			'list_streets'	=>$list_streets,
			'list_cconds'	=>$list_cconds,
			'list_putils'	=>$list_putils,
			'list_providers'=>$list_providers,
			'list_tcomp'	=>$list_tcomp,
			'list_repair'	=>$list_repair,
			'list_park'		=>$list_park,
			'list_flooring'	=>$list_flooring,
			'list_ventil'	=>$list_ventil
		));
	
	}

	public function actionImagesedit(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;
		$criteria = new CDbCriteria;
		$criteria->alias = 'I';
		$criteria->select = 'I.id, I.parent, I.itemType, I.kind, I.owner_id, I.city, COALESCE(I.name, "") as name, COALESCE(S.name, "не указана") as street, COALESCE(I.house, "дом не указан") as house, COALESCE(I.storey, "не указан") as storey, COALESCE(II.name, "<без названия>") as parentName';
		$criteria->join = 'left join streets S on S.id=I.street '.
			'left join items II on II.id=I.parent ';
		$criteria->condition = 'I.id=:id';
		$criteria->params = array(':id'=>$id);
		$Row = ItemDetail::model()->find($criteria);
		$vals = Yii::app()->user;
		if (empty($Row) || $vals->isGuest || ($vals->id['user_role'] != 'admin' && ($vals->id['user_id'] != $Row->owner_id)))
			throw new CHttpException(404);
		$criteria = new CDbCriteria;
		$criteria->condition = 'id_item=:ii';
		$criteria->params = array(':ii'=>$id);
		$criteria->order = 'id asc';
		$Fotos = Images::model()->findAll($criteria);
//Появлялась ошибка и закомментил, $news?
		//$this->pageTitle = $news->header;
		///$this->pageDescription = $news->description;
		//$this->pageKeywords = $news->keywords;
		$this->render('imagesedit', array('fotos'	=>	$Fotos, 'obj'	=>	$Row)); 
	}

	public function actionAdd_new(){
		$this->widget('Clear_session');
		$this->redirect(array('object/add', 'step'=>'step1'));
	}

	public function actionAdding(){
		if (isset($_GET['id']) && is_numeric($_GET['id'])) 
			$id = abs($_GET['id']);
		else $id = 0;
		$this->render('adding', array('id'=>$id));
	}

	public function actionAdd_error(){
		$this->render('add_error');
	}

	public function filters(){
		return array(
			array(
				'application.filters.ObjectAddFilter + Add'
			),
			array(
				'application.filters.ObjectEditFilter + Edit'
			));
	}

}?>