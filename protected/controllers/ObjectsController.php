<?php class ObjectsController extends CController{

	public $pageDescription;
	public $pageKeywords;
	public $pageClass = 'serp';
	public $layout = 'inner02';
	public $cssFiles = array('carousel.css');
	public $jsFiles = array('jquery.jcarousel.js', 'obj-detail.js');

	public function actionDownload(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id_obj = $get->id_out;
		$s = Yii::app()->params->web_root;
		$i = strrpos($s, '/'); $a = strlen($s)-1;
		if ($i != $a)	$s.='/';
		$fileDir = 'storage/files/pdfobj/';
		$fileName = 'obj'.$id_obj.'.pdf';
		$fPath = $s.$fileDir;
		$absName = $fPath.$fileName;
		if (!file_exists($absName)) {
			// Сгенерируем и поместим PDF-файл в каталог
			$this->PDFCreate ($id_obj, $fPath);
		}
		$this->redirect(array($fileDir.$fileName));
	}

	public function actionContain(){

		$this->pageTitle = 'Состав Центра Недвижимости';
		$this->pageDescription = '';
		$this->pageKeywords = '';

		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id_obj = $get->id_out;
		$criteria = new CDbCriteria;
		$criteria->alias = 'I';
		$criteria->select = 'I.id, I.parent, I.created, I.status, I.status_changed, I.special, I.kind, COALESCE(I.name, "без имени") as name, CAST(I.total_item_sqr as UNSIGNED) as "total_item_sqr", CAST(I.price as UNSIGNED) as "price", (Select F.name_gallery from images F where F.id_item=I.id order by F.id asc limit 1) as "hasFoto"';
		$criteria->condition = 'I.parent=:id and I.status=6';
		$criteria->params = array(':id'=>$id_obj);
		$Rows = ItemDetail::model()->findAll($criteria);

		$criteria->select = 'I.id, I.kind, I.status, COALESCE(I.name, "без имени") as name, (Select COUNT(*) from items C where C.parent=I.id) as repair, I.owner_id, U.username as "login", LEFT(U.so_name, 1) as "lastname", U.name as "firstname", U.phone as "phone", I.city, COALESCE(D.name, "нет") as district, COALESCE(CL.shname, "нет") as class, COALESCE(S.name, "нет") as street, COALESCE(I.house, "нет") as house, I.about';
		$criteria->join = 'left join users U on U.id=I.owner_id '.
			'left join districts D on D.id=I.district '.
			'left join streets S on S.id=I.street '.
			'left join classes CL on CL.id=I.class ';
		$criteria->condition = 'I.id=:id';
		$criteria->params = array(':id'=>$id_obj);
		$Center = ItemDetail::model()->find($criteria);

		if (empty($Center)) {
			throw new CHttpException(404);
		} elseif ($Center->status!=6) {
			$this->render('bad-status', array());
			return 0;
		}
		$criteria->alias = '';
		$criteria->join = '';
		$criteria->select = '*';
		$criteria->condition = 'id_item=:id';
		$criteria->params = array(':id'=>$id_obj);
		$criteria->order = 'id';
		$Fotos = Images::model()->findAll($criteria);

		$this->render('contain', array(
			'items'	=>$Rows,
			'aFotos'=>$Fotos,
			'obj'	=>$Center
		));
	}

	public function actionDetail(){

		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id_obj = $get->id_out;
		$vals = Yii::app()->user;
		$isTenant = (!$vals->isGuest && $vals->id['user_role'] == 'tenant');
		$criteria = new CDbCriteria;
		$criteria->alias = 'I';
		$criteria->select = 'I.id, I.parent, I.created, I.status, I.status_changed, I.itemType, I.special, I.kind, I.owner_id, U.username as "login", LEFT(U.so_name, 1) as "lastname", U.name as "firstname", U.phone as "phone", I.city, COALESCE(D.name, "нет") as district, I.district as districtID, COALESCE(KS.shname, "нет") as kind_structure, COALESCE(FN.shname, "нет") as functionality, COALESCE(I.name, "без имени") as name, COALESCE(CL.shname, "нет") as class, COALESCE(S.name, "нет") as street, COALESCE(I.house, "нет") as house, COALESCE(I.housing, "нет") as housing, COALESCE(I.storey, "-") as storey, COALESCE(I.storeys, "-") as storeys, I.service_lift, COALESCE(I.total_item_sqr, "0") as total_item_sqr, COALESCE(RP.shname, "нет") as repair, COALESCE(PK.shname, "нет") as parking, I.fixed_qty, I.video, I.access, I.price, COALESCE(PU.shname, "нет") as pay_utilities, I.internet, COALESCE(IP.shname, "\"нет\"") as providers, I.telephony, COALESCE(TC.shname, "\"нет\"") as tel_company, COALESCE(CC.shname, "нет") as contr_condition, I.about, COALESCE(I.welfare, "нет") as welfare, COALESCE(I.heating, 0) as heating, COALESCE(I.pallet_capacity, "нет") as pallet_capacity, COALESCE(I.shelf_capacity, "нет") as shelf_capacity, COALESCE(I.ceiling_height, "нет") as ceiling_height, COALESCE(I.work_height, "нет") as work_height, COALESCE(I.cathead, 0) as cathead, COALESCE(FL.shname, "нет") as flooring, COALESCE(I.floor_load, "нет") as floor_load, COALESCE(V.shname, "нет") as ventilation, I.air_condit, I.firefighting, I.can_reclame, COALESCE(I.el_power, "нет") as el_power, COALESCE(I.gates_qty, "нет") as gates_qty, COALESCE(I.gates_height, "нет") as gates_height, COALESCE(I.rampant, 0) as rampant, I.autoways, I.railways, COALESCE(II.name, "<без названия>") as parentName, COALESCE(II.kind, 0) as parentKind';
		$criteria->join = 'left join users U on U.id=I.owner_id '.
			'left join districts D on D.id=I.district '.
			'left join streets S on S.id=I.street '.
			'left join kind_structure KS on KS.id=I.kind_structure '.
			'left join functionality FN on FN.id=I.functionality '.
			'left join classes CL on CL.id=I.class '.
			'left join repair RP on RP.id=I.repair '.
			'left join parking PK on PK.id=I.parking '.
			'left join pay_utilities PU on PU.id=I.pay_utilities '.
			'left join net_providers IP on IP.id=I.providers '.
			'left join tel_companies TC on TC.id=I.tel_company '.
			'left join contr_forms CC on CC.id=I.contr_condition '.
			'left join flooring FL on FL.id=I.flooring '.
			'left join ventilation V on V.id=I.ventilation '.
			'left join items II on II.id=I.parent ';
		$criteria->condition = 'I.id=:id';
		$criteria->params = array(':id'=>$id_obj);
		$Row = ItemDetail::model()->find($criteria);

		if (empty($Row))
			throw new CHttpException(404);
		elseif ($Row->status!=6) {
			$this->render('bad-status', array());
			return 0;
		} elseif ($Row->itemType==1) { // Это объект-контейнер
			$this->redirect(array('objects/contain/id/'.$id_obj));
		}

		// Сохранение стастистики показа объкта
		$this->widget('PutStatFindShow', array('items'=>$id_obj));

		$criteria->alias = '';
		$criteria->join = '';
		$criteria->select = '*';
		$criteria->condition = 'id_item=:id';
		$criteria->params = array(':id'=>$id_obj);
		$criteria->order = 'id';
		$Fotos = Images::model()->findAll($criteria);
		// Прикрепленные файлы
		$criteria->condition = 'item=:id';
		$criteria->params = array(':id'=>$id_obj);
		$criteria->order = 'id';
		$Attachs = Attach::model()->findAll($criteria);
		// Проверим, есть ли объект в блокноте
		$isAtNotepad = false;
		if ($isTenant) {
			$criteria->condition = 'item=:id and tenant=:tn';
			$criteria->params = array(':id'=>$id_obj, ':tn'=>$vals->id['user_id']);
			$criteria->order = '';
			$Notepad = Notepads::model()->findAll($criteria);
			$isAtNotepad = !empty($Notepad);
		}

		$this->pageTitle = 'Просмотр объекта';
		$this->pageDescription = '';
		$this->pageKeywords = '';

		$this->render('detail', array(
			'id_obj'=>$id_obj,
			'item'	=>$Row,
			'aFotos'=>$Fotos,
			'aFiles'=>$Attachs,
			'atNotepad'=>$isAtNotepad,
			'userTenant'=>$isTenant
		));
	}

	public function actionPrint(){
		$this->layout = 'print01';

		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id_obj = $get->id_out;
		$criteria = new CDbCriteria;
		$criteria->alias = 'I';
		$criteria->select = 'I.id, I.parent, I.created, I.status, I.status_changed, I.itemType, I.special, I.kind, I.owner_id, U.username as "login", LEFT(U.so_name, 1) as "lastname", U.name as "firstname", U.phone as "phone", I.city, COALESCE(D.name, "нет") as district, I.district as districtID, COALESCE(KS.shname, "нет") as kind_structure, COALESCE(FN.shname, "нет") as functionality, COALESCE(I.name, "без имени") as name, COALESCE(CL.shname, "нет") as class, COALESCE(S.name, "нет") as street, COALESCE(I.house, "нет") as house, COALESCE(I.housing, "нет") as housing, COALESCE(I.storey, "-") as storey, COALESCE(I.storeys, "-") as storeys, I.service_lift, COALESCE(I.total_item_sqr, "0") as total_item_sqr, COALESCE(RP.shname, "нет") as repair, COALESCE(PK.shname, "нет") as parking, I.fixed_qty, I.video, I.access, I.price, COALESCE(PU.shname, "нет") as pay_utilities, I.internet, COALESCE(IP.shname, "\"нет\"") as providers, I.telephony, COALESCE(TC.shname, "\"нет\"") as tel_company, COALESCE(CC.shname, "нет") as contr_condition, I.about, COALESCE(I.welfare, "нет") as welfare, COALESCE(I.heating, 0) as heating, COALESCE(I.pallet_capacity, "нет") as pallet_capacity, COALESCE(I.shelf_capacity, "нет") as shelf_capacity, COALESCE(I.ceiling_height, "нет") as ceiling_height, COALESCE(I.work_height, "нет") as work_height, COALESCE(I.cathead, 0) as cathead, COALESCE(FL.shname, "нет") as flooring, COALESCE(I.floor_load, "нет") as floor_load, COALESCE(V.shname, "нет") as ventilation, I.air_condit, I.firefighting, I.can_reclame, COALESCE(I.el_power, "нет") as el_power, COALESCE(I.gates_qty, "нет") as gates_qty, COALESCE(I.gates_height, "нет") as gates_height, COALESCE(I.rampant, 0) as rampant, I.autoways, I.railways, COALESCE(II.name, "<без названия>") as parentName';
		$criteria->join = 'left join users U on U.id=I.owner_id '.
			'left join districts D on D.id=I.district '.
			'left join streets S on S.id=I.street '.
			'left join kind_structure KS on KS.id=I.kind_structure '.
			'left join functionality FN on FN.id=I.functionality '.
			'left join classes CL on CL.id=I.class '.
			'left join repair RP on RP.id=I.repair '.
			'left join parking PK on PK.id=I.parking '.
			'left join pay_utilities PU on PU.id=I.pay_utilities '.
			'left join net_providers IP on IP.id=I.providers '.
			'left join tel_companies TC on TC.id=I.tel_company '.
			'left join contr_forms CC on CC.id=I.contr_condition '.
			'left join flooring FL on FL.id=I.flooring '.
			'left join ventilation V on V.id=I.ventilation '.
			'left join items II on II.id=I.parent ';
		$criteria->condition = 'I.id=:id';
		$criteria->params = array(':id'=>$id_obj);
		$Row = ItemDetail::model()->find($criteria);

		if (empty($Row))
			throw new CHttpException(404);
		elseif ($Row->status!=6) {
			$this->render('bad-status', array());
			return 0;
		}

		$criteria->alias = '';
		$criteria->join = '';
		$criteria->select = '*';
		$criteria->condition = 'id_item=:id';
		$criteria->params = array(':id'=>$id_obj);
		$criteria->order = 'id';
		$Fotos = Images::model()->findAll($criteria);
		// Прикрепленные файлы
		$criteria->condition = 'item=:id';
		$criteria->params = array(':id'=>$id_obj);
		$criteria->order = 'id';
		$Attachs = Attach::model()->findAll($criteria);
		
		$this->pageTitle = 'Печать карточки объекта';
		$this->pageDescription = '';
		$this->pageKeywords = '';

		$this->render('print-detail', array(
			'back_url'=>$this->createAbsoluteUrl('objects/detail', array('id'=>$id_obj)),
			'item'	=>$Row,
			'aFotos'=>$Fotos,
			'aFiles'=>$Attachs,
		));
	}

	public function actionLoad() {
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id_obj = $get->id_out;
		$s = Yii::app()->params->web_root;
		$i = strrpos($s, '/'); $a = strlen($s)-1;
		if ($i != $a)	$s.='/';
		$pathImages = $s.'storage/images/';
		$pathAttach = $s.'storage/files/objs/';
		$archName = 'obj'.$id_obj.'.zip';
		$fPath = $s.'storage/files/zipobj/';
		$fName = $fPath.$archName;
		if (!file_exists($fName)) {
			// Сгенерируем и поместим PDF-файл в каталог
			$this->PDFCreate ($id_obj, $fPath, false);
			$files = ' '.$fPath.'obj'.$id_obj.'.pdf';
			// Подготовим файлы фоток
			$Fotos = Images::model()->findAll('id_item=:ii', array(':ii'=>$id_obj));
			if (!empty($Fotos)) {
				foreach ($Fotos as $foto) {
					copy ($pathImages.$foto->img, $fPath.$foto->img);
					$files.=' '.$fPath.$foto->img;
				}
			}
			// Подготовим файлы прикрепленные к объекту
			$Docs = Attach::model()->findAll('item=:ii', array(':ii'=>$id_obj));
			if (!empty($Docs)) {
				foreach ($Docs as $doc) {
					copy ($pathAttach.$doc->filename, $fPath.$doc->filename);
					$files.=' '.$fPath.$doc->filename;
				}
			}
			// Создание архива
			//exec ('gzip -c1'.$files.' > '.$fPath.$archName);
			//exec ('tar -cf '.$fPath.'aaa.tar '.$files.' ; gzip '.$fPath.'aaa.tar ; mv '.$fPath.'aaa.tar.gz '.$fName.' ; rm '.$files);
			exec ('zip -j '.$fName.$files.' ; rm '.$files);
		}
		$this->redirect(array('storage/files/zipobj/'.$archName)); 
	}
	
	private function PDFCreate ($id_obj = 0, $dir = '', $forHtml = true) {

		$criteria = new CDbCriteria;
		$criteria->alias = 'I';
		$criteria->select = 'I.id, I.parent, I.created, I.status, I.status_changed, I.itemType, I.special, I.kind, I.owner_id, U.username as "login", LEFT(U.so_name, 1) as "lastname", U.name as "firstname", U.phone as "phone", I.city, COALESCE(D.name, "нет") as district, COALESCE(KS.shname, "нет") as kind_structure, COALESCE(FN.shname, "нет") as functionality, COALESCE(I.name, "без имени") as name, COALESCE(CL.shname, "нет") as class, COALESCE(S.name, "нет") as street, COALESCE(I.house, "нет") as house, COALESCE(I.housing, "нет") as housing, COALESCE(I.storey, "-") as storey, COALESCE(I.storeys, "-") as storeys, I.service_lift, COALESCE(I.total_item_sqr, "0") as total_item_sqr, COALESCE(RP.shname, "нет") as repair, COALESCE(PK.shname, "нет") as parking, I.fixed_qty, I.video, I.access, I.price, COALESCE(PU.shname, "нет") as pay_utilities, I.internet, COALESCE(IP.shname, "\"нет\"") as providers, I.telephony, COALESCE(TC.shname, "\"нет\"") as tel_company, COALESCE(CC.shname, "нет") as contr_condition, I.about, COALESCE(I.welfare, "нет") as welfare, COALESCE(I.heating, 0) as heating, COALESCE(I.pallet_capacity, "нет") as pallet_capacity, COALESCE(I.shelf_capacity, "нет") as shelf_capacity, COALESCE(I.ceiling_height, "нет") as ceiling_height, COALESCE(I.work_height, "нет") as work_height, COALESCE(I.cathead, 0) as cathead, COALESCE(FL.shname, "нет") as flooring, COALESCE(I.floor_load, "нет") as floor_load, COALESCE(V.shname, "нет") as ventilation, I.air_condit, I.firefighting, I.can_reclame, COALESCE(I.el_power, "нет") as el_power, COALESCE(I.gates_qty, "нет") as gates_qty, COALESCE(I.gates_height, "нет") as gates_height, COALESCE(I.rampant, 0) as rampant, I.autoways, I.railways, COALESCE(II.name, "<без названия>") as parentName';
		$criteria->join = 'left join users U on U.id=I.owner_id '.
			'left join districts D on D.id=I.district '.
			'left join streets S on S.id=I.street '.
			'left join kind_structure KS on KS.id=I.kind_structure '.
			'left join functionality FN on FN.id=I.functionality '.
			'left join classes CL on CL.id=I.class '.
			'left join repair RP on RP.id=I.repair '.
			'left join parking PK on PK.id=I.parking '.
			'left join pay_utilities PU on PU.id=I.pay_utilities '.
			'left join net_providers IP on IP.id=I.providers '.
			'left join tel_companies TC on TC.id=I.tel_company '.
			'left join contr_forms CC on CC.id=I.contr_condition '.
			'left join flooring FL on FL.id=I.flooring '.
			'left join ventilation V on V.id=I.ventilation '.
			'left join items II on II.id=I.parent ';
		$criteria->condition = 'I.id=:id';
		$criteria->params = array(':id'=>$id_obj);
		$Row = ItemDetail::model()->find($criteria);

		if (empty($Row) || ($Row->status!=6))
			throw new CHttpException(404);

		if ($forHtml) {
			$criteria->alias = '';
			$criteria->join = '';
			$criteria->select = '*';
			$criteria->condition = 'id_item=:id';
			$criteria->params = array(':id'=>$id_obj);
			$criteria->order = 'id';
			$Fotos = Images::model()->findAll($criteria);
		}

		ini_set('memory_limit', '64M');
		if ($forHtml) 
				$this->widget('PDFCreate', array('item'=>$Row, 'images'=>$Fotos, 'id'=>$id_obj, 'directory'=>$dir, 'forHtml'=>true));
		else	$this->widget('PDFCreate', array('item'=>$Row, 'images'=>array(), 'id'=>$id_obj, 'directory'=>$dir, 'forHtml'=>false));
	}

}?>