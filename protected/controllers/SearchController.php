<?php class SearchController extends CController{
	public $defaultAction = 'view';
	public $layout = 'serp02';
	public $pageClass = 'serp';	public $pageDescription;
	public $pageKeywords;
	//public $cssFiles = array('jqtransform');
	//public $jsFiles = array('jquery.jqtransform', 'jquery2');
	public $list_districts;	public $ses_vals;
	public function actionView(){
	
		//CVarDumper::Dump($_POST, 3, true); die();
		//$this->redirect(array('/'));
		//CApplication::end();
		$vals = Yii::app()->user;
		$kind = 0;		$sDistrict = 'Не важен';

		if (isset($_POST['btFind'])){
			$whereString = 'I.itemType<>1 and I.status=6'; // Переменная для формирования WHERE-предложения sql-запроса			$Params = array(); // Массив параметров, передаваемых в запрос			$NewRec = new FindStat; 
			if (isset($vals->glcity)) { // Город
				$whereString .= ' and I.city=:ct';
				$Params[':ct'] = $vals->getState('glcity');
				$NewRec->city = $vals->getState('glcity');			}
			if ((isset($_POST['tip_sooruj'])) && ($_POST['tip_sooruj']!=0)) { // Тип сооружения
				$whereString .= ' and I.kind_structure=:ks';
				$Params[':ks'] = $_POST['tip_sooruj'];
				$vals->setState('ses_tip_sooruj', $_POST['tip_sooruj']);
				$NewRec->kind_structure = $_POST['tip_sooruj'];			} elseif (isset($vals->ses_tip_sooruj))	unset ($vals->ses_tip_sooruj);
			if ((isset($_POST['kind'])) && ($_POST['kind']!='')) { // Тип сдаваемого помещения
				$whereString .= ' and I.kind=:kd';
				$v = $this->CheckUnsignInteger($_POST['kind']);
				$Params[':kd'] = $v;				
				$vals->setState('ses_kind', $v);
				$kind = $v;
				$NewRec->kind = $v;			}
			if ((isset($_POST['funkcional'])) && ($_POST['funkcional']!=0)) { // Функциональное назначение
				$whereString .= ' and I.functionality=:fn';
				$Params[':fn'] = $_POST['funkcional'];
				$vals->setState('ses_funkcional', $_POST['funkcional']);
				$NewRec->functionality = $_POST['funkcional'];			} elseif (isset($vals->ses_funkcional))	unset ($vals->ses_funkcional);
			if ((isset($_POST['klass'])) && ($_POST['klass']!=0)) { // Класс (классность) объекта
				$whereString .= ' and I.class=:cl';
				$Params[':cl'] = $_POST['klass'];
				$vals->setState('ses_klass', $_POST['klass']);				$NewRec->class = $_POST['klass'];			} elseif (isset($vals->ses_klass))	unset ($vals->ses_klass);
			if ((isset($_POST['raion'])) && ($_POST['raion']!=0)) { // Район
				$whereString .= ' and I.district=:ds';
				$Params[':ds'] = $_POST['raion'];
				$vals->setState('ses_raion', $_POST['raion']);
				$NewRec->district = $_POST['raion'];				// Получим название района для надписи на странице над таблицей с результатами поиска				$Distr = District::model()->findByPK($_POST['raion']);				if (!empty($Distr))	$sDistrict = $Distr->name;			} elseif (isset($vals->ses_raion))	unset ($vals->ses_raion);
			if ((isset($_POST['ulica'])) && ($_POST['ulica']!=0)) { // Улица
				$whereString .= ' and I.street=:st';
				$Params[':st'] = $_POST['ulica'];
				$vals->setState('ses_ulica', $_POST['ulica']);
				$NewRec->street = $_POST['ulica'];			} elseif (isset($vals->ses_ulica))	unset ($vals->ses_ulica);
			if ((isset($_POST['etaj1'])) && ($_POST['etaj1']!='')) { // Этаж. Минимально приемлемый
				$whereString .= ' and I.storey >= :et1';
				$v = $this->CheckUnsignInteger($_POST['etaj1']);
				$Params[':et1'] = (int)$v;
				$vals->setState('ses_etaj1', $v);
				$NewRec->storeyMin = (int)$v;			} elseif (isset($vals->ses_etaj1))	unset ($vals->ses_etaj1);
			if ((isset($_POST['etaj2'])) && ($_POST['etaj2']!='')) { // Этаж. Максимально допустимый
				$whereString .= ' and I.storey <= :et2';
				$v = $this->CheckUnsignInteger($_POST['etaj2']);
				$Params[':et2'] = (int)$v;
				$vals->setState('ses_etaj2', $v);
				$NewRec->storeyMax = (int)$v;			} elseif (isset($vals->ses_etaj2))	unset ($vals->ses_etaj2);
			if (isset($_POST['gruz_lift']) && ($_POST['gruz_lift'] == 'on')) { // Грузовой лифт
				$whereString .= ' and I.service_lift=:gl';
				$Params[':gl'] = 1; #CheckBox
				$vals->setState('ses_gruz_lift', 1);
				$NewRec->service_lift = 1;			} elseif (isset($vals->ses_gruz_lift))	unset ($vals->ses_gruz_lift);
			if ((isset($_POST['sqr1'])) && ($_POST['sqr1']!='')) { // Площадь помещения. Минимально приемлемая
				$whereString .= ' and I.total_item_sqr >= :sq1';
				$v = $this->CheckUnsignDecimal($_POST['sqr1']);
				$Params[':sq1'] = (float)$v;
				$vals->setState('ses_sqr1', $v);
				$NewRec->squareMin = (float)$v;			} elseif (isset($vals->ses_sqr1))	unset ($vals->ses_sqr1);
			if ((isset($_POST['sqr2'])) && ($_POST['sqr2']!='')) { // Площадь помещения. Максимально допустимая
				$whereString .= ' and I.total_item_sqr <= :sq2';
				$v = $this->CheckUnsignDecimal($_POST['sqr2']);
				$Params[':sq2'] = (float)$v;
				$vals->setState('ses_sqr2', $v);
				$NewRec->squareMax = (float)$v;			} elseif (isset($vals->ses_sqr2))	unset ($vals->ses_sqr2);
			if ((isset($_POST['sosremont'])) && ($_POST['sosremont']!=0)) { // Состояние ремонта
				$whereString .= ' and I.repair=:rp';
				$Params[':rp'] = $_POST['sosremont'];
				$vals->setState('ses_sosremont', $_POST['sosremont']);
				$NewRec->repair = $_POST['sosremont'];			} elseif (isset($vals->ses_sosremont))	unset ($vals->ses_sosremont);
			if ((isset($_POST['parkovka'])) && ($_POST['parkovka']!=0)) { // Наличие парковки
				$whereString .= ' and I.parking=:pk';
				$Params[':pk'] = $_POST['parkovka'];
				$vals->setState('ses_parkovka', $_POST['parkovka']);				$NewRec->parking = $_POST['parkovka'];
			} elseif (isset($vals->ses_parkovka))	unset ($vals->ses_parkovka);
			if ((isset($_POST['prc1'])) && ($_POST['prc1']!='')) { // Арендная ставка. Минимально приемлемая
				$whereString .= ' and I.price >= :pr1';
				$v = $this->CheckUnsignDecimal($_POST['prc1']);
				$Params[':pr1'] = (float)$v;
				$vals->setState('ses_prc1', $v);				$NewRec->priceMin = (float)$v;
			} elseif (isset($vals->ses_prc1))	unset ($vals->ses_prc1);
			if ((isset($_POST['prc2'])) && ($_POST['prc2']!='')) { // Арендная ставка. Максимально допустимая
				$whereString .= ' and I.price <= :pr2';
				$v = $this->CheckUnsignDecimal($_POST['prc2']);
				$Params[':pr2'] = (float)$v;
				$vals->setState('ses_prc2', $v);
				$NewRec->priceMax = (float)$v;			} elseif (isset($vals->ses_prc2))	unset ($vals->ses_prc2);
			if (isset($_POST['est_inet']) && ($_POST['est_inet'] == 'on')) { // Наличие Интернет
				$whereString .= ' and I.internet=:nt';
				$Params[':nt'] = 1; #CheckBox
				$vals->setState('ses_est_inet', 1);				$NewRec->internet = 1;
			} elseif (isset($vals->ses_est_inet))	unset ($vals->ses_est_inet);
			if (isset($_POST['est_telef']) && ($_POST['est_telef'] == 'on')) { // Наличие Телефонии
				$whereString .= ' and I.telephony=:tf';
				$Params[':tf'] = 1; #CheckBox
				$vals->setState('ses_est_telef', 1);				$NewRec->telephony = 1;
			} elseif (isset($vals->ses_est_telef))	unset ($vals->ses_est_telef);
			if (isset($_POST['est_voda']) && ($_POST['est_voda'] == 'on')) { // Наличие отопления и водоснабжения
				$whereString .= ' and I.heating=:ht';
				$Params[':ht'] = 1; #CheckBox
				$vals->setState('ses_est_voda', 1);				$NewRec->heating = 1;
			} elseif (isset($vals->ses_est_voda))	unset ($vals->ses_est_voda);
			if ((isset($_POST['potolok1'])) && ($_POST['potolok1']!='')) { // Высота потолков. Минимальная
				$whereString .= ' and I.ceiling_height >= :ch1';
				$v = $this->CheckUnsignDecimal($_POST['potolok1']);
				$Params[':ch1'] = (float)$v;
				$vals->setState('ses_potolok1', $v);				$NewRec->ceiling_height1 = (float)$v;
			} elseif (isset($vals->ses_potolok1))	unset ($vals->ses_potolok1);
			if ((isset($_POST['potolok2'])) && ($_POST['potolok2']!='')) { // Высота потолков. Максимальная
				$whereString .= ' and I.ceiling_height <= :ch2';
				$v = $this->CheckUnsignDecimal($_POST['potolok2']);
				$Params[':ch2'] = (float)$v;
				$vals->setState('ses_potolok2', $v);
				$NewRec->ceiling_height2 = (float)$v;			} elseif (isset($vals->ses_potolok2))	unset ($vals->ses_potolok2);
			if (isset($_POST['kranb']) && ($_POST['kranb'] == 'on')) { // Кран-балка
				$whereString .= ' and I.cathead=:chd';
				$Params[':chd'] = 1; #CheckBox
				$vals->setState('ses_kranb', 1);				$NewRec->cathead = 1;
			} elseif (isset($vals->ses_kranb))	unset ($vals->ses_kranb);
			if (isset($_POST['ventil']) && ($_POST['ventil'] != 0)) { //  Вентиляция
				$whereString .= ' and I.ventilation=:vl';
				$Params[':vl'] = $_POST['ventil'];
				$vals->setState('ses_ventil', $_POST['ventil']);				$NewRec->ventilation = $_POST['ventil'];
			} elseif (isset($vals->ses_ventil))	unset ($vals->ses_ventil);
			if (isset($_POST['pozhar']) && ($_POST['pozhar'] == 'on')) { //  Пожаротушение
				$whereString .= ' and I.firefighting=:ff';
				$Params[':ff'] = 1; #CheckBox
				$vals->setState('ses_pozhar', 1);				$NewRec->firefighting = 1;
			} elseif (isset($vals->ses_pozhar))	unset ($vals->ses_pozhar);
			if ((isset($_POST['vorota1'])) && ($_POST['vorota1']!='')) { // Высота ворот. Минимальная
				$whereString .= ' and I.gates_height >= :gh1';
				$v = $this->CheckUnsignDecimal($_POST['vorota1']);
				$Params[':gh1'] = (float)$v;
				$vals->setState('ses_vorota1', $v);				$NewRec->gates_height1 = (float)$v;
			} elseif (isset($vals->ses_vorota1))	unset ($vals->ses_vorota1);
			if ((isset($_POST['vorota2'])) && ($_POST['vorota2']!='')) { // Высота ворот. Максимальная
				$whereString .= ' and I.gates_height <= :gh2';
				$v = $this->CheckUnsignDecimal($_POST['vorota2']);
				$Params[':gh2'] = (float)$v;
				$vals->setState('ses_vorota2', $v);
				$NewRec->gates_height2 = (float)$v;			} elseif (isset($vals->ses_vorota2))	unset ($vals->ses_vorota2);
			if (isset($_POST['pandus']) && ($_POST['pandus'] == 'on')) { //  Пандус
				$whereString .= ' and I.rampant=:rm';
				$Params[':rm'] = 1; #CheckBox
				$vals->setState('ses_pandus', 1);				$NewRec->rampant = 1;
			} elseif (isset($vals->ses_pandus))	unset ($vals->ses_pandus);
			if (isset($_POST['autoways']) && ($_POST['autoways'] == 'on')) { //  Автомобильные пп
				$whereString .= ' and I.autoways=:aw';
				$Params[':aw'] = 1; #CheckBox
				$vals->setState('ses_aways', 1);				$NewRec->autoways = 1;
			} elseif (isset($vals->ses_aways))	unset ($vals->ses_aways);
			if (isset($_POST['railways']) && ($_POST['railways'] == 'on')) { //  Ж/Д подъездные пути
				$whereString .= ' and I.railways=:rw';
				$Params[':rw'] = 1; #CheckBox
				$vals->setState('ses_rways', 1);				$NewRec->railways = 1;
			} elseif (isset($vals->ses_rways))	unset ($vals->ses_rways);
			/*  Заполним статистику поиска */			// Округляем цену до 10, а площади до 5			$rPrMin = $this->CheckDecimal($NewRec->priceMin);			if ($rPrMin>0) {				$rPrMin = (int)(ceil($rPrMin / 10))*10;				$NewRec->priceMin = $rPrMin;			}			$rPrMax = $this->CheckDecimal($NewRec->priceMax);			if ($rPrMax>0) {				$rPrMax = (int)(ceil($rPrMax / 10))*10;				$NewRec->priceMax = $rPrMax;			}			$rSqMin = $this->CheckDecimal($NewRec->squareMin);			if ($rSqMin>0) {				$rSqMin = (int)(ceil($rSqMin / 5))*5;				$NewRec->squareMin = $rSqMin;			}			$rSqMax = $this->CheckDecimal($NewRec->squareMax);			if ($rSqMax>0) {				$rSqMax = (int)(ceil($rSqMax / 5))*5;				$NewRec->squareMax = $rSqMax;			}			if ( ($NewRec->district + $rPrMin + $rPrMax + $rSqMin + $rSqMax)>0 || ($NewRec->service_lift + $NewRec->internet + $NewRec->telephony + $NewRec->heating + $NewRec->cathead + $NewRec->rampant + $NewRec->firefighting + $NewRec->autoways + $NewRec->railways > 1)) {				if ($vals->isGuest)						$NewRec->who_id = 0;				else	$NewRec->who_id = $vals->id['user_id'];				$NewRec->ip = "'".$_SERVER['REMOTE_ADDR']."'";				$NewRec->save(false);			}
			$this->redirect(array('search/table'));			$isTenant = (!$vals->isGuest && $vals->id['user_role'] == 'tenant');			if ($isTenant)
				$toNotepad = ', (Select COALESCE(N.id, 0) from notepad N where N.item=I.id and N.tenant='.$vals->id['user_id'].') as "inNotepad"';
			else $toNotepad = '';			$criteria = new CDbCriteria;
			$criteria->alias = 'I';
			$criteria->select = 'I.id, I.special, I.city, D.name as "district", S.name as "street", I.house, I.total_item_sqr, I.price, I.created, U.username as "login", LEFT(U.so_name, 1) as "lastname", U.name as "firstname", U.phone as "phone", (Select F.name_small from images F where F.id_item=I.id order by F.id asc limit 1) as "hasFoto"'.$toNotepad;
			$criteria->join = 'join streets S on S.id=I.street join districts D on D.id=I.district left join users U on U.id=I.owner_id';
			$criteria->condition = $whereString;			//di($Params);
			$criteria->params = $Params;
			$criteria->order = 'I.special desc, I.created desc';
			$count_of_search = Search::model()->count($criteria);
			/* Постраничная разбивка */
			$pages = new CPagination($count_of_search);
			$pages->pageSize = '25';
			$pages->route = 'table';
			$pages->applyLimit($criteria);

			$Search_obj = Search::model()->findAll($criteria);
			/* Сохранение стастистики показов объкта при поиске
			if (!empty($Search_obj)) {
			$ids = array();
			for ($i=0;$i<count($Search_obj);$i++)
					$ids[] = $Search_obj[$i]->id;
			$this->widget('PutStatFindShow', array('items'=>$ids));
			}
			*/
			/* Получаем значения для фильтра Район (Зависит от выбранного города) */			$criteria = new CDbCriteria;			$criteria->order = 'name';			$criteria->condition = 'city=:ct';			$criteria->params = array(':ct'=>$vals->getState('glcity', ''));			$list_districts = CHtml::listData( District::model()->findAll($criteria), 'id', 'name');			array_unshift($list_districts, 'Не важен');			$this->list_districts = $list_districts;			// Получим значения сессий для установки значений фильтров			$this->ses_vals['vraion'] = $vals->getState('ses_raion', 0);			$this->ses_vals['vsqr1'] = $vals->getState('ses_sqr1', '');			$this->ses_vals['vsqr2'] = $vals->getState('ses_sqr2', '');			$this->ses_vals['vprc1'] = $vals->getState('ses_prc1', '');			$this->ses_vals['vprc2'] = $vals->getState('ses_prc2', '');
			$this->pageTitle = 'Поиск объекта';
			$this->pageDescription = '';
			$this->pageKeywords = '';
			$vals->setState('ses_raion_str', $sDistrict);
			$this->render('resoult', array(
				'Search_obj'	=>$Search_obj,
				'count_of_search'	=>$count_of_search,
				'pages'			=>$pages,
				'sortCol'		=>0,				'sortDir'		=>0,
				'kindTab'		=>$kind,				'sCity'			=>$vals->getState('glcity', 'Не задан'),				'sDistrict'		=>$sDistrict,
				'userTenant'	=>$isTenant // Показывать ли ссылку Добавить в блокнот
			));
		} elseif (isset($_POST['btReset']) || isset($_GET['clear'])){
			unset ($vals->ses_tip_sooruj);
			unset ($vals->ses_funkcional);
			unset ($vals->ses_klass);
			unset ($vals->ses_raion);			unset ($vals->ses_raion_str);
			unset ($vals->ses_ulica);
			unset ($vals->ses_etaj1);
			unset ($vals->ses_etaj2);
			unset ($vals->ses_gruz_lift);
			unset ($vals->ses_sqr1);
			unset ($vals->ses_sqr2);
			unset ($vals->ses_sosremont);
			unset ($vals->ses_parkovka);
			unset ($vals->ses_prc1);
			unset ($vals->ses_prc2);
			unset ($vals->ses_est_inet);
			unset ($vals->ses_est_telef);
			unset ($vals->ses_est_voda);
			unset ($vals->ses_potolok1);
			unset ($vals->ses_potolok2);
			unset ($vals->ses_kranb);
			unset ($vals->ses_ventil);
			unset ($vals->ses_pozhar);
			unset ($vals->ses_vorota1);
			unset ($vals->ses_vorota2);
			unset ($vals->ses_pandus);
			unset ($vals->ses_aways);
			unset ($vals->ses_rways);
			$this->redirect(array('search/table'));
		} else {			$this->redirect(array('search/table'));		}
	}

	public function actionTable() {
		$vals = Yii::app()->user;
		$whereString = 'I.itemType<>1 and I.status=6'; // Переменная для формирования WHERE-предложения sql-запроса		$Params = array(); // Массив параметров, передаваемых в запрос

		if (isset($vals->glcity)) { // Город
			$whereString .= ' and I.city=:ct';
			$Params[':ct'] = $vals->getState('glcity');
		}
		if (isset($vals->ses_tip_sooruj)) { // Тип сооружения
			$whereString .= ' and I.kind_structure=:ks';
			$Params[':ks'] = $vals->getState('ses_tip_sooruj');
		}
		if (isset($vals->ses_kind)) { // Тип помещения (офис/склад и т.п.
			$kind = $vals->getState('ses_kind');
		} else $kind = 0;
		$whereString .= ' and I.kind=:kd';
		$Params[':kd'] = $kind;
		if (isset($vals->ses_funkcional)) { // Функциональное назначение
			$whereString .= ' and I.functionality=:fn';
			$Params[':fn'] = $vals->getState('ses_funkcional');
		}
		if (isset($vals->ses_klass)) { // Класс (классность) объекта
			$whereString .= ' and I.class=:cl';
			$Params[':cl'] = $vals->getState('ses_klass');
		}
		if (isset($vals->ses_raion)) { // Район
			$whereString .= ' and I.district=:ds';
			$Params[':ds'] = $vals->getState('ses_raion');
		}
		if (isset($vals->ses_ulica)) { // Улица
			$whereString .= ' and I.street=:st';
			$Params[':st'] = $vals->getState('ses_ulica');
		}
		if (isset($vals->ses_etaj1) && ($vals->getState('ses_etaj1')!='')) { // Этаж. Минимально приемлемый
			$whereString .= ' and I.storey >= :et1';
			$Params[':et1'] = $vals->getState('ses_etaj1');
		}
		if (isset($vals->ses_etaj2) && ($vals->getState('ses_etaj2')!='')) { // Этаж. Максимально допустимый
			$whereString .= ' and I.storey <= :et2';
			$Params[':et2'] = $vals->getState('ses_etaj2');
		}
		if (isset($vals->ses_gruz_lift)) { // Грузовой лифт
			$whereString .= ' and I.service_lift=:gl';
			$Params[':gl'] = $vals->getState('ses_gruz_lift');
		}
		if (isset($vals->ses_sqr1) && ($vals->getState('ses_sqr1')!='')) { // Площадь помещения. Минимально приемлемая
			$whereString .= ' and I.total_item_sqr >= :sq1';
			$Params[':sq1'] = $vals->getState('ses_sqr1');
		}
		if (isset($vals->ses_sqr2) && ($vals->getState('ses_sqr2')!='')) { // Площадь помещения. Максимально допустимая
			$whereString .= ' and I.total_item_sqr <= :sq2';
			$Params[':sq2'] = $vals->getState('ses_sqr2');
		}
		if (isset($vals->ses_sosremont)) { // Состояние ремонта
			$whereString .= ' and I.repair=:rp';
			$Params[':rp'] = $vals->getState('ses_sosremont');
		}
		if (isset($vals->ses_parkovka)) { // Наличие парковки
			$whereString .= ' and I.parking=:pk';
			$Params[':pk'] = $vals->getState('ses_parkovka');
		}
		if (isset($vals->ses_prc1) && ($vals->getState('ses_prc1')!='')) { // Арендная ставка. Минимально приемлемая
			$whereString .= ' and I.price >= :pr1';
			$Params[':pr1'] = $vals->getState('ses_prc1');
		}
		if (isset($vals->ses_prc2) && ($vals->getState('ses_prc2')!='')) { // Арендная ставка. Максимально допустимая
			$whereString .= ' and I.price <= :pr2';
			$Params[':pr2'] = $vals->getState('ses_prc2');
		}
		if (isset($vals->ses_est_inet)) { // Наличие Интернет
			$whereString .= ' and I.internet=:nt';
			$Params[':nt'] = $vals->getState('ses_est_inet');
		}
		if (isset($vals->ses_est_telef)) { // Наличие Телефонии
			$whereString .= ' and I.telephony=:tf';
			$Params[':tf'] = $vals->getState('ses_est_telef');
		}
		if (isset($vals->ses_est_voda)) { // Наличие отопления и водоснабжения
			$whereString .= ' and I.heating=:ht';
			$Params[':ht'] = $vals->getState('ses_est_voda');
		}
		if (isset($vals->ses_potolok1) && ($vals->getState('ses_potolok1')!='')) { // Высота потолков. Минимальная
			$whereString .= ' and I.ceiling_height >= :ch1';
			$Params[':ch1'] = $vals->getState('ses_potolok1');
		}
		if (isset($vals->ses_potolok2) && ($vals->getState('ses_potolok2')!='')) { // Высота потолков. Максимальная
			$whereString .= ' and I.ceiling_height <= :ch2';
			$Params[':ch2'] = $vals->getState('ses_potolok2');
		}
		if (isset($vals->ses_kranb)) { // Кран-балка
			$whereString .= ' and I.cathead=:chd';
			$Params[':chd'] = $vals->getState('ses_kranb');
		}
		if (isset($vals->ses_ventil)) { //  Вентиляция
			$whereString .= ' and I.ventilation=:vl';
			$Params[':vl'] = $vals->getState('ses_ventil');
		}
		if (isset($vals->ses_pozhar)) { //  Пожаротушение
			$whereString .= ' and I.firefighting=:ff';
			$Params[':ff'] = $vals->getState('ses_pozhar');
		}
		if (isset($vals->ses_vorota1) && ($vals->getState('ses_vorota1')!='')) { // Высота ворот. Минимальная
			$whereString .= ' and I.gates_height >= :gh1';
			$Params[':gh1'] = $vals->getState('ses_vorota1');
		}
		if (isset($vals->ses_vorota2) && ($vals->getState('ses_vorota2')!='')) { // Высота ворот. Максимальная
			$whereString .= ' and I.gates_height <= :gh2';
			$Params[':gh2'] = $vals->getState('ses_vorota2');
		}
		if (isset($vals->ses_pandus)) { //  Пандус
			$whereString .= ' and I.rampant=:rm';
			$Params[':rm'] = $vals->getState('ses_pandus');
		}
		if (isset($vals->ses_aways)) { //  Автомобильные подъездные пути
			$whereString .= ' and I.autoways=:aw';
			$Params[':aw'] = $vals->getState('ses_aways');
		}
		if (isset($vals->ses_rways)) { //  Железнодорожные подъездные пути
			$whereString .= ' and I.railways=:rw';
			$Params[':rw'] = $vals->getState('ses_rways');
		}
		$sortOrd = 0;
		if (isset($_GET['sort'])){
			$sortCol = $_GET['sort'];
			switch ($sortCol) {
				case 2 : {$order = 'D.name'; break;}
				case 3 : {$order = 'S.name, I.house'; break;}
				case 4 : {$order = 'I.total_item_sqr'; break;}
				case 5 : {$order = 'I.price'; break;}
				case 6 : {$order = 'I.price*I.total_item_sqr'; break;}
				case 7 : {$order = 'I.created'; break;}
			}
			if (isset($_GET['type'])) {
				if ($sortCol==3)
					$order = 'S.name desc, I.house desc';
				else	$order .= ' desc';				$sortOrd = 1;
			}			
		} else {
			$sortCol = 0;
			$order = 'I.special desc, I.created desc';
		}

		$isTenant = (!$vals->isGuest && $vals->id['user_role'] == 'tenant');
		if ($isTenant)
			$toNotepad = ', (Select COALESCE(N.id, 0) from notepad N where N.item=I.id and N.tenant='.$vals->id['user_id'].') as "inNotepad"';
		else $toNotepad = '';
		$criteria = new CDbCriteria;
		$criteria->alias = 'I';
		$criteria->select = 'I.id, I.special, I.city, D.name as "district", S.name as "street", I.house, I.total_item_sqr, I.price, I.created, I.owner_id, U.username as "login", LEFT(U.so_name, 1) as "lastname", U.name as "firstname", U.phone as "phone", (Select F.name_small from images F where F.id_item=I.id order by F.id asc limit 1) as "hasFoto"'.$toNotepad;		$criteria->join = 'join streets S on S.id=I.street join districts D on D.id=I.district left join users U on U.id=I.owner_id';		$criteria->condition = $whereString;
		$criteria->params = $Params;
		$criteria->order = $order;

		$count_of_search = Search::model()->count($criteria);
		/* Постраничная разбивка */
		$pages = new CPagination($count_of_search);
		$pages->pageSize = '25';
		$pages->applyLimit($criteria);

		$Search_obj = Search::model()->findAll($criteria);
		/* Сохранение стастистики показов объкта при поиске
		if (!empty($Search_obj)) {
			$ids = array();
			for ($i=0;$i<count($Search_obj);$i++)
				$ids[] = $Search_obj[$i]->id;
			$this->widget('PutStatFindShow', array('items'=>$ids));
		}
		*/

		/* Получаем значения для фильтра Район (Зависит от выбранного города) */		$criteria = new CDbCriteria;		$criteria->order = 'name';		$criteria->condition = 'city=:ct';		$criteria->params = array(':ct'=>$vals->getState('glcity', ''));		$list_districts = CHtml::listData( District::model()->findAll($criteria), 'id', 'name');		array_unshift($list_districts, 'Не важен');		$this->list_districts = $list_districts;		// Получим значения сессий для установки значений фильтров		$this->ses_vals['vraion'] = $vals->getState('ses_raion', 0);		$this->ses_vals['vsqr1'] = $vals->getState('ses_sqr1', '');		$this->ses_vals['vsqr2'] = $vals->getState('ses_sqr2', '');		$this->ses_vals['vprc1'] = $vals->getState('ses_prc1', '');		$this->ses_vals['vprc2'] = $vals->getState('ses_prc2', '');		$this->pageTitle = 'Поиск объекта';
		$this->pageDescription = '';
		$this->pageKeywords = '';

		$this->render('resoult', array(
			'Search_obj'	=>$Search_obj,
			'count_of_search'=>$count_of_search,
			'pages'			=>$pages,
			'sortCol'		=>$sortCol,
			'sortDir'		=>$sortOrd,			'kindTab'		=>$kind,
			'sCity'			=>$vals->getState('glcity', 'Не задан'),			'sDistrict'		=>$vals->getState('ses_raion_str', 'Не задан'),			'userTenant'	=>$isTenant // Показывать ли ссылку Добавить в блокнот
		));
	}
	public function actionMost(){		$get = $this->widget('GetChek', array('get_mass'=>$_GET));		$Row = FindStat::model()->find('id=:id', array(':id'=>$get->id_out));		if (empty($Row))			$this->redirect(array('search/table'));		$vals = Yii::app()->user;		$vals->setState('ses_kind', $Row->kind);		if ($Row->district == 0)				unset ($vals->ses_raion);		else	$vals->setState('ses_raion', $Row->district);		if ($Row->kind_structure == 0)				unset ($vals->ses_tip_sooruj);		else	$vals->setState('ses_tip_sooruj', $Row->kind_structure);		if ($Row->functionality == 0)				unset ($vals->ses_funkcional);		else	$vals->setState('ses_funkcional', $Row->functionality);		if ($Row->class == 0)				unset ($vals->ses_klass);		else	$vals->setState('ses_klass', $Row->class);		if ($Row->street == 0)				unset ($vals->ses_ulica);		else	$vals->setState('ses_ulica', $Row->street);		if (empty($Row->storeyMin))				unset ($vals->ses_etaj1);		else	$vals->setState('ses_etaj1', $Row->storeyMin);		if (empty($Row->storeyMax))				unset ($vals->ses_etaj2);		else	$vals->setState('ses_etaj2', $Row->storeyMax);		if (empty($Row->service_lift))				unset ($vals->ses_gruz_lift);		else	$vals->setState('ses_gruz_lift', 1);		if (empty($Row->squareMin))				unset ($vals->ses_sqr1);		else	$vals->setState('ses_sqr1', $Row->squareMin);		if (empty($Row->squareMax))				unset ($vals->ses_sqr2);		else	$vals->setState('ses_sqr2', $Row->squareMax);		if ($Row->repair == 0)				unset ($vals->ses_sosremont);		else	$vals->setState('ses_sosremont', $Row->repair);		if ($Row->parking == 0)				unset ($vals->ses_parkovka);		else	$vals->setState('ses_parkovka', $Row->parking);		if (empty($Row->priceMin))				unset ($vals->ses_prc1);		else	$vals->setState('ses_prc1', $Row->priceMin);		if (empty($Row->priceMax))				unset ($vals->ses_prc2);		else	$vals->setState('ses_prc2', $Row->priceMax);		if (empty($Row->internet))				unset ($vals->ses_est_inet);		else	$vals->setState('ses_est_inet', 1);		if (empty($Row->telephony))				unset ($vals->ses_est_telef);		else	$vals->setState('ses_est_telef', 1);		if (empty($Row->heating))				unset ($vals->ses_est_voda);		else	$vals->setState('ses_est_voda', 1);		if (empty($Row->ceiling_height1))				unset ($vals->ses_potolok1);		else	$vals->setState('ses_potolok1', $Row->ceiling_height1);		if (empty($Row->ceiling_height2))				unset ($vals->ses_potolok2);		else	$vals->setState('ses_potolok2', $Row->ceiling_height2);		if (empty($Row->cathead))				unset ($vals->ses_kranb);		else	$vals->setState('ses_kranb', 1);		if ($Row->ventilation == 0)				unset ($vals->ses_ventil);		else	$vals->setState('ses_ventil', $Row->ventilation);		if (empty($Row->firefighting))				unset ($vals->ses_pozhar);		else	$vals->setState('ses_pozhar', 1);		if (empty($Row->gates_height1))				unset ($vals->ses_vorota1);		else	$vals->setState('ses_vorota1', $Row->gates_height1);		if (empty($Row->gates_height2))				unset ($vals->ses_vorota2);		else	$vals->setState('ses_vorota2', $Row->gates_height2);		if (empty($Row->rampant))				unset ($vals->ses_pandus);		else	$vals->setState('ses_pandus', 1);		if (empty($Row->autoways))				unset ($vals->ses_aways);		else	$vals->setState('ses_aways', 1);		if (empty($Row->railways))				unset ($vals->ses_rways);		else	$vals->setState('ses_rways', 1);		$this->redirect(array('search/table'));	}
	function CheckUnsignInteger($v) {
		if (is_numeric($v) && ($v>0))
			return $v;
		else return 0;
	}
	function CheckUnsignDecimal($v) {
		$d = $this->CheckDecimal($v);
		if ($d>0)	return $d;
		else		return 0;		
	}

	function CheckDecimal($v) {		if (empty($v)) return 0;
		$d = str_replace(',', '.', $v);
		$p = strpos($d, '.');
		if ($p!==false) {
			$p = strpos($d, '.', $p+1);
			if ($p!==false)
				$d = substr($d, 0, $p);
		}
		if (is_numeric($d) && ($d>0))
			return (float)$d;
		else return 0;
	}
}?>