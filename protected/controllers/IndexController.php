<?php class IndexController extends CController{

	public $defaultAction = 'view';
	public $layout = 'index02';
	public $pageDescription;
	public $pageKeywords;
	//public $cssFiles = array('jqtransform');
	//public $jsFiles = array('jquery.jqtransform', 'jquery2');
	public $list_districts;
	public $ses_vals;

	public function actionView(){
		$vals = Yii::app()->user;
		// Обработаем изменение выбора города
		/*  Числовой идентификатор города
		if (isset($_GET['city'])) {
			$idCity = substr($_GET['city'], 0, 3);
			settype ($idCity, 'integer');
			if ($idCity < 1)	$idCity = 1;
			$City = City::model()->find('id=:id',array(':id'=>$idCity));
			Yii::app()->user->setState('glcity', $City->name);
		}
		*/
		if (isset($_GET['city'])) {
			$s = urldecode($_GET['city']);
			$exists = City::model()->exists('name=:id',array(':id'=>$s));
			if (!$exists) $s = 'Екатеринбург';
			Yii::app()->user->setState('glcity', $s);
			$cookCityName = 'usercity';
			$nCook=new CHttpCookie($cookCityName, $s);
			$nCook->httpOnly=true;
			$nCook->expire = 1879048191; // 0x6FFFFFFF;
			Yii::app()->request->cookies[$cookCityName]=$nCook;
			$ref = Yii::app()->request->urlReferrer;
			$p = strpos($ref, Yii::app()->params->site_base);
			if ($p===false)
					$this->redirect(array('index/view'));
			else	$this->redirect($ref);
			CApplication::end();
		}
		$criteria = new CDbCriteria;
		$criteria->select = 'text_page, title, description, keywords';
		$criteria->condition = 'name=:name_page';
		$criteria->params = array(':name_page'=>'index');
		$Static_page = Statics::model()->find($criteria);

		/* Получаем значения для фильтра Район (Зависит от выбранного города) */
		$criteria = new CDbCriteria;
		$criteria->order = 'name';
		$criteria->condition = 'city=:ct';
		$criteria->params = array(':ct'=>$vals->getState('glcity', ''));
		$list_districts = CHtml::listData( District::model()->findAll($criteria), 'id', 'name');
		array_unshift($list_districts, 'Не важен');
		$this->list_districts = $list_districts;
		// Получим значения сессий для установки значений фильтров
		$this->ses_vals['vraion'] = $vals->getState('ses_raion', 0);
		$this->ses_vals['vsqr1'] = $vals->getState('ses_sqr1', '');
		$this->ses_vals['vsqr2'] = $vals->getState('ses_sqr2', '');
		$this->ses_vals['vprc1'] = $vals->getState('ses_prc1', '');
		$this->ses_vals['vprc2'] = $vals->getState('ses_prc2', '');

		if ($Static_page->title == '')
				$this->pageTitle = 'Главная страница';
		else	$this->pageTitle = $Static_page->title;
		$this->pageDescription = $Static_page->description;
		$this->pageKeywords = $Static_page->keywords;

		$this->render('view', array(
			'text'			=>$Static_page->text_page,
		));

	}

	public function actionSearch() {
		$this->layout = 'serp01';
		$vals = Yii::app()->user;
		die('Index contr action Search');
		$whereString = ''; // Переменная для формирования WHERE-предложения sql-запроса
		$Params = array(); // Массив параметров, передаваемых в запрос

		if (isset($vals->glcity)) { // Город
			$whereString = ' and I.city=:ct';
			$Params[':ct'] = $vals->getState('glcity');
		}
		if (isset($vals->ses_tip_sooruj)) { // Тип сооружения
			$whereString .= ' and I.kind_structure=:ks';
			$Params[':ks'] = $vals->getState('ses_tip_sooruj');
		}
		if (isset($vals->ses_kind)) { // Тип помещения (офис/склад и т.п.
			$whereString .= ' and I.kind=:kd';
			$Params[':kd'] = $vals->getState('ses_kind');
		}
		if (isset($vals->ses_funkcional)) { // Функциональное назначение
			$whereString .= ' and I.functionality=:fn';
			$Params[':fn'] = $vals->getState('ses_funkcional');
		}
		if (isset($vals->ses_klass)) { // Класс (классность) объекта
			$whereString .= ' and I.class=:cl';
			$Params[':cl'] = $vals->getState('ses_klass');
		}
		if (isset($vals->ses_raion)) { // Район
			$whereString .= ' and I.district=:ds';
			$Params[':ds'] = $vals->getState('ses_raion');
		}
		if (isset($vals->ses_ulica)) { // Улица
			$whereString .= ' and I.street=:st';
			$Params[':st'] = $vals->getState('ses_ulica');
		}
		if (isset($vals->ses_etaj1) && ($vals->getState('ses_etaj1')!='')) { // Этаж. Минимально приемлемый
			$whereString .= ' and I.storey >= :et1';
			$Params[':et1'] = $vals->getState('ses_etaj1');
		}
		if (isset($vals->ses_etaj2) && ($vals->getState('ses_etaj2')!='')) { // Этаж. Максимально допустимый
			$whereString .= ' and I.storey <= :et2';
			$Params[':et2'] = $vals->getState('ses_etaj2');
		}
		if (isset($vals->ses_gruz_lift)) { // Грузовой лифт
			$whereString .= ' and I.service_lift=:gl';
			$Params[':gl'] = $vals->getState('ses_gruz_lift');
		}
		if (isset($vals->ses_sqr1) && ($vals->getState('ses_sqr1')!='')) { // Площадь помещения. Минимально приемлемая
			$whereString .= ' and I.total_item_sqr >= :sq1';
			$Params[':sq1'] = $vals->getState('ses_sqr1');
		}
		if (isset($vals->ses_sqr2) && ($vals->getState('ses_sqr2')!='')) { // Площадь помещения. Максимально допустимая
			$whereString .= ' and I.total_item_sqr <= :sq2';
			$Params[':sq2'] = $vals->getState('ses_sqr2');
		}
		if (isset($vals->ses_sosremont)) { // Состояние ремонта
			$whereString .= ' and I.repair=:rp';
			$Params[':rp'] = $vals->getState('ses_sosremont');
		}
		if (isset($vals->ses_parkovka)) { // Наличие парковки
			$whereString .= ' and I.parking=:pk';
			$Params[':pk'] = $vals->getState('ses_parkovka');
		}
		if (isset($vals->ses_prc1) && ($vals->getState('ses_prc1')!='')) { // Арендная ставка. Минимально приемлемая
			$whereString .= ' and I.price >= :pr1';
			$Params[':pr1'] = $vals->getState('ses_prc1');
		}
		if (isset($vals->ses_prc2) && ($vals->getState('ses_prc2')!='')) { // Арендная ставка. Максимально допустимая
			$whereString .= ' and I.price <= :pr2';
			$Params[':pr2'] = $vals->getState('ses_prc2');
		}
		if (isset($vals->ses_est_inet)) { // Наличие Интернет
			$whereString .= ' and I.internet=:nt';
			$Params[':nt'] = $vals->getState('ses_est_inet');
		}
		if (isset($vals->ses_est_telef)) { // Наличие Телефонии
			$whereString .= ' and I.telephony=:tf';
			$Params[':tf'] = $vals->getState('ses_est_telef');
		}
		if (isset($vals->ses_est_voda)) { // Наличие отопления и водоснабжения
			$whereString .= ' and I.heating=:ht';
			$Params[':ht'] = $vals->getState('ses_est_voda');
		}
		if (isset($vals->ses_potolok1) && ($vals->getState('ses_potolok1')!='')) { // Высота потолков. Минимальная
			$whereString .= ' and I.ceiling_height >= :ch1';
			$Params[':ch1'] = $vals->getState('ses_potolok1');
		}
		if (isset($vals->ses_potolok2) && ($vals->getState('ses_potolok2')!='')) { // Высота потолков. Максимальная
			$whereString .= ' and I.ceiling_height <= :ch2';
			$Params[':ch2'] = $vals->getState('ses_potolok2');
		}
		if (isset($vals->ses_kranb)) { // Кран-балка
			$whereString .= ' and I.cathead=:chd';
			$Params[':chd'] = $vals->getState('ses_kranb');
		}
		if (isset($vals->ses_ventil)) { //  Вентиляция
			$whereString .= ' and I.ventilation=:vl';
			$Params[':vl'] = $vals->getState('ses_ventil');
		}
		if (isset($vals->ses_pozhar)) { //  Пожаротушение
			$whereString .= ' and I.firefighting=:ff';
			$Params[':ff'] = $vals->getState('ses_pozhar');
		}
		if (isset($vals->ses_vorota1) && ($vals->getState('ses_vorota1')!='')) { // Высота ворот. Минимальная
			$whereString .= ' and I.gates_height >= :gh1';
			$Params[':gh1'] = $vals->getState('ses_vorota1');
		}
		if (isset($vals->ses_vorota2) && ($vals->getState('ses_vorota2')!='')) { // Высота ворот. Максимальная
			$whereString .= ' and I.gates_height <= :gh2';
			$Params[':gh2'] = $vals->getState('ses_vorota2');
		}
		if (isset($vals->ses_pandus)) { //  Пандус
			$whereString .= ' and I.rampant=:rm';
			$Params[':rm'] = $vals->getState('ses_pandus');
		}
		if (isset($vals->ses_aways)) { //  Автомобильные подъездные пути
			$whereString .= ' and I.autoways=:aw';
			$Params[':aw'] = $vals->getState('ses_aways');
		}
		if (isset($vals->ses_rways)) { //  Железнодорожные подъездные пути
			$whereString .= ' and I.railways=:rw';
			$Params[':rw'] = $vals->getState('ses_rways');
		}

		if ($whereString!='')	$whereString = substr($whereString, 5);
		if (isset($_GET['sort'])){
			$route = '';
			$sortCol = $_GET['sort'];
			switch ($sortCol) {
				case 2 : {$order = 'D.name'; break;}
				case 3 : {$order = 'S.name, I.house'; break;}
				case 4 : {$order = 'I.total_item_sqr'; break;}
				case 5 : {$order = 'I.price'; break;}
				case 6 : {$order = 'I.price*I.total_item_sqr'; break;}
				case 7 : {$order = 'I.created'; break;}
			}
			if (isset($_GET['type'])) {
				if ($sortCol==3)
					$order = 'S.name desc, I.house desc';
				else	$order .= ' desc';
			}			
		} else {
			$sortCol = 7;
			$route = 'search';
			$order = 'I.special desc, I.created desc';
		}

		$isTenant = (!$vals->isGuest && $vals->id['user_role'] == 'tenant');
		if ($isTenant)
			$toNotepad = ', (Select COALESCE(N.id, 0) from notepad N where N.item=I.id and N.tenant='.$vals->id['user_id'].') as "inNotepad"';
		else $toNotepad = '';
		$criteria = new CDbCriteria;
		$criteria->alias = 'I';
		$criteria->select = 'I.id, I.special, I.city, D.name as "district", S.name as "street", I.house, I.total_item_sqr, I.price, I.created, (Select COALESCE(MAX(F.id), 0) from images F where F.id_item=I.id) as "hasFoto"'.$toNotepad;
		$criteria->join = 'join streets S on S.id=I.street join districts D on D.id=I.district';
		//$criteria->condition = $whereString;
		//$criteria->params = $Params;
		$criteria->order = $order;

		$count_of_search = Search1::model()->count($criteria);
		/* Постраничная разбивка */
		$pages = new CPagination1($count_of_search);
		$pages->pageSize = '12';
		$pages->route = $route;
		$pages->applyLimit($criteria);

		$Search_obj = Search1::model()->findAll($criteria);
		/* Сохранение стастистики показов объкта при поиске
		if (!empty($Search_obj)) {
			$ids = array();
			for ($i=0;$i<count($Search_obj);$i++)
				$ids[] = $Search_obj[$i]->id;
			$this->widget('PutStatFindShow', array('items'=>$ids));
		}
		*/

		$this->pageTitle = 'Поиск объекта';
		$this->pageDescription = '';
		$this->pageKeywords = '';

		$this->render('resoult', array(
			'Search_obj'	=>$Search_obj,
			'count_of_search'=>$count_of_search,
			'pages'			=>$pages,
			'sortCol'		=>$sortCol,
			'userTenant'	=>$isTenant // Показывать ли ссылку Добавить в блокнот
		));
	}

	public function actionUnset(){
		$vals = Yii::app()->user;
		unset1 ($vals->ses_kind);
		unset ($vals->ses_tip_sooruj);
		unset ($vals->ses_funkcional);
		unset ($vals->ses_klass);
		unset ($vals->ses_raion);
		unset ($vals->ses_ulica);
		unset ($vals->ses_etaj1);
		unset ($vals->ses_etaj2);
		unset ($vals->ses_gruz_lift);
		unset ($vals->ses_sqr1);
		unset ($vals->ses_sqr2);
		unset ($vals->ses_sosremont);
		unset ($vals->ses_parkovka);
		unset ($vals->ses_prc1);
		unset ($vals->ses_prc2);
		unset ($vals->ses_est_inet);
		unset ($vals->ses_est_telef);
		unset ($vals->ses_est_voda);
		unset ($vals->ses_potolok1);
		unset ($vals->ses_potolok2);
		unset ($vals->ses_kranb);
		unset ($vals->ses_pokrytie);
		unset ($vals->ses_ventil);
		unset ($vals->ses_condic);
		unset ($vals->ses_pozhar);
		unset ($vals->ses_vorota1);
		unset ($vals->ses_vorota2);
		unset ($vals->ses_pandus);
		unset ($vals->ses_aways);
		unset ($vals->ses_rways);
		$this->redirect(array('index/view'));
	}

}?>