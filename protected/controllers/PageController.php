<?php class PageController extends CController{

	public $defaultAction = 'view';
	public $pageDescription;
	public $pageKeywords;
	public $pageClass = 'any';
	public $layout = 'inner02';

	public function actionView(){
	$name_of_page = $_GET['name'];
	$criteria = new CDbCriteria;
	$criteria->select = 'name_ru, text_page, title, description, keywords';
	$criteria->condition = 'name=:name_page';
	$criteria->params = array(':name_page'=>$name_of_page);
	$Static_page = Statics::model()->find($criteria);
	if (empty($Static_page))
		throw new CHttpException(404);

	if ($Static_page->title == '')
			$this->pageTitle = $Static_page->name_ru;
	else	$this->pageTitle = $Static_page->title;
	$this->pageDescription = $Static_page->description;
	$this->pageKeywords = $Static_page->keywords;

	$this->render('view', array(
		'text' =>$Static_page->text_page,
		'title' =>$Static_page->name_ru
	));

	}

}?>