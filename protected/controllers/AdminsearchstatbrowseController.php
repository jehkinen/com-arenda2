<?php class AdminsearchstatbrowseController extends CController{
	public $defaultAction = 'view';
	public $layout = 'control';
	public $pageDescription;
	public $pageKeywords;

	public function actionView(){
		$this->pageTitle = 'Просмотр статистики поисковых запросов';
		$this->pageDescription = '';
		$this->pageKeywords = '';
		$usr = Yii::app()->user;
		$form = new AdminItems_form();
		if (isset($_POST['AdminItems_form']) || (count($_POST)>0)){
			$form->attributes = $_POST['AdminItems_form'];
			if ($form->validate()){
				if ((isset($_POST['AdminItems_form']['city'])) && (!is_numeric($_POST['AdminItems_form']['city'])))
					$usr->setState('afscity', $_POST['AdminItems_form']['city']);
				elseif (isset($usr->afscity))	unset ($usr->afscity);
				if ((isset($_POST['AdminItems_form']['owner'])) && ($_POST['AdminItems_form']['owner']!=-1)) // Искавший посетитель
					$usr->setState('afswho', $_POST['AdminItems_form']['owner']);
				elseif (isset($usr->afswho))	unset ($usr->afswho);
				if ((isset($_POST['AdminItems_form']['kind'])) && ($_POST['AdminItems_form']['kind']!=90)) //  Поисковая вкладка
					$usr->setState('afskind', $_POST['AdminItems_form']['kind']);
				elseif (isset($usr->afskind))	unset ($usr->afskind);
			}
		}

		$whereString = ''; // Переменная для формирования WHERE-предложения sql-запроса
		$Params = array(); // Массив параметров, передаваемых в запрос
		if (isset($usr->afscity)) {
			$whereString .= ' and I.city=:ct';
			$Params[':ct'] = $usr->getState('afscity', '');
		}
		if (isset($usr->afskind)) {
			$whereString .= ' and I.kind=:kd';
			$Params[':kd'] = $usr->getState('afskind', 0);
		}
		if (isset($usr->afswho)) {
			$whereString .= ' and I.who_id=:wn';
			$Params[':wn'] = $usr->getState('afswho', 0);
		}
		if ($whereString!='')	$whereString = substr($whereString, 5);

        # Определяем сортировку для запроса, если нет сортировки то сортируем по id обекта
        $order = '';
		if (isset($_GET['sort'])){		  $order = $_GET['sort'];
		  if (isset($_GET['type'])) $order .= ' desc';		}
		if (empty($order)) $order = 'id';

		$criteria = new CDbCriteria;
		$criteria->alias = 'I';
		$criteria->select = 'I.id, I.created, I.kind, I.city, D.name as "district", I.priceMin, I.priceMax, I.squareMin, I.squareMax, I.who_id, U.username as "whoLogin", U.so_name as "whoSoName", U.name as "whoName"';
		$criteria->join = 'left join districts D on D.id=I.district left join users U on U.id=I.who_id';
		$criteria->condition = $whereString;
		$criteria->params = $Params;
		$criteria->order = 'I.'.$order;

		$qty = FindStat::model()->count($criteria);
		# Постраничная разбивка
		$pages = new CPagination($qty);
		$pages->pageSize = 40;
		$pages->applyLimit($criteria);
		# Получаем текущую страницу (нумерация начинается с 0, поэтому +1)
		$Cpage = $pages->getCurrentPage()+1;

		$Items = FindStat::model()->findAll($criteria);

		$criteria = new CDbCriteria;
		$criteria->order = 'name';
		$arCities = CHtml::listData( City::model()->findAll($criteria), 'name', 'name');
		array_unshift($arCities, 'Любой');

		$criteria = new CDbCriteria;
		$criteria->select = 'id, username, so_name';
		$criteria->condition = 'role=:r';
		$criteria->params = array(':r'=>'owner');
		$criteria->order = 'username';
		$Rows = User::model()->findAll($criteria);
		$arUsers[-1] = 'Любой';
		$arUsers[0] = 'Гость';
		if (!empty($Rows))
			foreach ($Rows as $user)
				$arUsers[$user->id] = $user->username.' ('.$user->so_name.')';

		$arKinds = array(90=>'Любое', 0=>'офис', 1=>'торговое', 2=>'Склад\Производство');

		$this->render('view', array(
			'form'		=>$form,
			'rows'		=>$Items,
			'pages'		=>$pages,
			'foundQty'	=>$qty,
			'Cities'	=>$arCities,
			'city'		=>$usr->getState('afscity', 0),
			'Kinds'		=>$arKinds,
			'kind'		=>$usr->getState('afskind', 90),
			'Users'		=>$arUsers,
			'owner'		=>$usr->getState('afswho', -1),
			'Cpage'		=>$Cpage
			));
	}
	public function actionDetail(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;
		$criteria = new CDbCriteria;
		$criteria->alias = 'T';
		$criteria->select = 'T.id, T.who_id, T.kind, T.city, COALESCE(D.name, "не важен") as "district", T.created, COALESCE(KS.shname, "не важен") as kind_structure, COALESCE(FN.shname, "не важна") as functionality, COALESCE(S.name, "не важна") as street, CASE T.service_lift when 1 then "есть" else "нет" END as service_lift, COALESCE(CL.shname, "не важен") as class, COALESCE(RP.shname, "не важно") as repair, COALESCE(PK.shname, "не важна") as parking, CASE T.internet when 1 then "есть" else "нет" END as internet, CASE T.telephony when 1 then "есть" else "нет" END as telephony, CASE T.heating when 1 then "есть" else "нет" END as heating, CASE T.cathead when 1 then "есть" else "нет" END as cathead, CASE T.firefighting when 1 then "есть" else "нет" END as firefighting, CASE T.rampant when 1 then "есть" else "нет" END as rampant, CASE T.autoways when 1 then "есть" else "нет" END as autoways, CASE T.railways when 1 then "есть" else "нет" END as railways, NULLIF(T.gates_height1, "0") as "gates_height1", NULLIF(T.gates_height2, "0") as "gates_height2", NULLIF(T.ceiling_height1, "0") as "ceiling_height1", NULLIF(T.ceiling_height2, "0") as "ceiling_height2", NULLIF(T.storeyMin, "0") as "storeyMin", NULLIF(T.storeyMax, "0") as "storeyMax", NULLIF(T.squareMin, "0") as "squareMin", NULLIF(T.squareMax, "0") as "squareMax", NULLIF(T.priceMin, "0") as "priceMin", NULLIF(T.priceMax, "0") as "priceMax", U.so_name as "whoSoName", U.name as "whoName", U.otchestvo as "whoOtchestvo", U.phone as "whoPhone"';
		$criteria->join = 'Left join districts D on D.id=T.district left join users U on U.id=T.who_id '.
			'left join streets S on S.id=T.street '.
			'left join kind_structure KS on KS.id=T.kind_structure '.
			'left join functionality FN on FN.id=T.functionality '.
			'left join classes CL on CL.id=T.class '.
			'left join repair RP on RP.id=T.repair '.
			'left join parking PK on PK.id=T.parking ';
		$criteria->condition = 'T.id=:id';
		$criteria->params = array(':id'=>$id);
		$Row = FindStat::model()->find($criteria);

		if (empty($Row))
			throw new CHttpException(404);

		$this->pageTitle = 'Поисковый запрос';
		$this->pageDescription = '';
		$this->pageKeywords = '';

		$this->render('detail', array('info'	=>$Row));

	}

	public function actionClick(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$Row = FindStat::model()->find('id=:id', array(':id'=>$get->id_out));
		if (empty($Row))
			throw new CHttpException(404);
		$vals = Yii::app()->user;
		$vals->setState('ses_kind', $Row->kind);
		if ($Row->district == 0)
				unset ($vals->ses_raion);
		else	$vals->setState('ses_raion', $Row->district);
		if ($Row->kind_structure == 0)
				unset ($vals->ses_tip_sooruj);
		else	$vals->setState('ses_tip_sooruj', $Row->kind_structure);
		if ($Row->functionality == 0)
				unset ($vals->ses_funkcional);
		else	$vals->setState('ses_funkcional', $Row->functionality);
		if ($Row->class == 0)
				unset ($vals->ses_klass);
		else	$vals->setState('ses_klass', $Row->class);
		if ($Row->street == 0)
				unset ($vals->ses_ulica);
		else	$vals->setState('ses_ulica', $Row->street);
		if (empty($Row->storeyMin))
				unset ($vals->ses_etaj1);
		else	$vals->setState('ses_etaj1', $Row->storeyMin);
		if (empty($Row->storeyMax))
				unset ($vals->ses_etaj2);
		else	$vals->setState('ses_etaj2', $Row->storeyMax);
		if (empty($Row->service_lift))
				unset ($vals->ses_gruz_lift);
		else	$vals->setState('ses_gruz_lift', 1);
		if (empty($Row->squareMin))
				unset ($vals->ses_sqr1);
		else	$vals->setState('ses_sqr1', $Row->squareMin);
		if (empty($Row->squareMax))
				unset ($vals->ses_sqr2);
		else	$vals->setState('ses_sqr2', $Row->squareMax);
		if ($Row->repair == 0)
				unset ($vals->ses_sosremont);
		else	$vals->setState('ses_sosremont', $Row->repair);
		if ($Row->parking == 0)
				unset ($vals->ses_parkovka);
		else	$vals->setState('ses_parkovka', $Row->parking);
		if (empty($Row->priceMin))
				unset ($vals->ses_prc1);
		else	$vals->setState('ses_prc1', $Row->priceMin);
		if (empty($Row->priceMax))
				unset ($vals->ses_prc2);
		else	$vals->setState('ses_prc2', $Row->priceMax);
		if (empty($Row->internet))
				unset ($vals->ses_est_inet);
		else	$vals->setState('ses_est_inet', 1);
		if (empty($Row->telephony))
				unset ($vals->ses_est_telef);
		else	$vals->setState('ses_est_telef', 1);
		if (empty($Row->heating))
				unset ($vals->ses_est_voda);
		else	$vals->setState('ses_est_voda', 1);
		if (empty($Row->ceiling_height1))
				unset ($vals->ses_potolok1);
		else	$vals->setState('ses_potolok1', $Row->ceiling_height1);
		if (empty($Row->ceiling_height2))
				unset ($vals->ses_potolok2);
		else	$vals->setState('ses_potolok2', $Row->ceiling_height2);
		if (empty($Row->cathead))
				unset ($vals->ses_kranb);
		else	$vals->setState('ses_kranb', 1);
		if ($Row->ventilation == 0)
				unset ($vals->ses_ventil);
		else	$vals->setState('ses_ventil', $Row->ventilation);
		if (empty($Row->firefighting))
				unset ($vals->ses_pozhar);
		else	$vals->setState('ses_pozhar', 1);
		if (empty($Row->gates_height1))
				unset ($vals->ses_vorota1);
		else	$vals->setState('ses_vorota1', $Row->gates_height1);
		if (empty($Row->gates_height2))
				unset ($vals->ses_vorota2);
		else	$vals->setState('ses_vorota2', $Row->gates_height2);
		if (empty($Row->rampant))
				unset ($vals->ses_pandus);
		else	$vals->setState('ses_pandus', 1);
		if (empty($Row->autoways))
				unset ($vals->ses_aways);
		else	$vals->setState('ses_aways', 1);
		if (empty($Row->railways))
				unset ($vals->ses_rways);
		else	$vals->setState('ses_rways', 1);
		$this->redirect(array('search/table'));
	}

	public function actionUnset(){
		$usr = Yii::app()->user;
		unset($usr->afscity);
		unset($usr->afskind);
		unset($usr->afswho);
		$this->redirect(array('adminsearchstatbrowse/view'));
	}

	public function filters(){
	return array(
		array(
			'application.filters.AccessFilter',
			'role'=>'admin',
		)
	);
	}

}
?>