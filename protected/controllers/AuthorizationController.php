<?php class AuthorizationController extends CController{

	public $pageDescription;
	public $pageKeywords;
	public $defaultAction = 'login';
	public $layout = 'inner02';

	public function actions(){
		return array(
			'captcha'=>array(
				'class'=>'MyCCaptchaAction',
				/*'class'=>'CCaptchaAction',
					'backColor'=> 0x003300,
					'maxLength'=> 3,
					'minLength'=> 3,
					'foreColor'=> 0x66FF66,*/
			),
		);
	}

	public function actionFirstlogin(){

		$form = new User('first');
		$form->username = $_POST['username'];
		$form->password = $_POST['password'];
		if($form->validate('login')){
			if ($form->error === null) {
				$ref = Yii::app()->request->urlReferrer;
				$p = strpos($ref, Yii::app()->params->site_base);
				if ($p===false)
						Yii::app()->request->redirect(Yii::app()->homeUrl);
				else	Yii::app()->request->redirect($ref);
			} else	$this->redirect(array('authorization/login', 'errorcode'=>$form->error));
		}
	}

	public function actionLogin(){

		$model=new LoginForm;

		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];

			if($model->validate() && $model->login())
			{
				Yii::app()->request->redirect(Yii::app()->homeUrl);
			}

		}
		$this->render('login',array('form'=>$model));
	}

//	public function actionLogin(){
//
//		$form = new User('next');
//
//		if (!empty($_POST['User'])){
//			$form->attributes = $_POST['User'];
//			$form->login();
//			//$form->verifyCode = $_POST['User']['verifyCode'];
//			//if($form->validate('login')){
//				if (Yii::app()->user->hasState('ses_want_url'))	{
//						$url = Yii::app()->user->getState('ses_want_url');
//						unset(Yii::app()->user->ses_want_url);
//				} else	$url = Yii::app()->homeUrl;
//				//Yii::app()->request->redirect($url);
//			//}
//		}
//
//		$prompt = 'Попробуйте авторизироваться снова:';
//		$btCaption = 'Повторить попытку';
//		$errCode = 0;
////		if (isset($_GET['errorcode'])){
////			$errCode = $_GET['errorcode'];
////			switch ($errCode){
////				case 100:{	$label_error = 'Неверно набрано имя пользователя'; break; }
////				case 200:{	$label_error = 'Неверный пароль'; break; }
////
////				case 250: { $label_error = 'Аккаунт деактивирован'; break; }
////				case 260: { $label_error = 'Аккаунт заблокирован'; break; }
////				case 270: { $label_error = 'Ваша учетная запись ожидает активации'; break; }
////				case 299: { $label_error = 'Система защиты от подбора паролей включена'; break; }
////				case 300: { $label_error = 'Неизвестная ошибка'; break; }
////			default: $label_error = 'Неверно заданный идентификатор ошибки';
////			}
////		}
////		elseif (Yii::app()->user->hasState('ses_want_url'))	{
////				$label_error = 'Вы обратились к странице, требующей привелегированных прав доступа. Представьтесь, пожалуйста!';
////				$prompt = 'Введите свои учетные данные:';
////				$btCaption = 'Продолжить';
////		} else	$label_error = 'Укажите параметры Вашей учетной записи';
////
//
//		$this->pageTitle = 'Авторизация пользователя';
//		$this->render('login', array(
//			'form'			=>$form,
//			//'label_error'	=>$label_error,
//			///'prompt'		=>$prompt,
//			//'errorCode'		=>$errCode
//		));
//
//	}

	public function actionLogout(){
		Yii::app()->user->logout();
		$ref = Yii::app()->request->urlReferrer;
		$p = strpos($ref, Yii::app()->params->site_base);
		if ($p===false)
				Yii::app()->request->redirect(Yii::app()->homeUrl);
		else	Yii::app()->request->redirect($ref);
	}

}?>