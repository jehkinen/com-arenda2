<?php class ControlController extends CController{

	public $layout = 'control';

	#-------------------------------------------------------------------------
	# Модерирование объектами
	#-------------------------------------------------------------------------

	public function actionIndex(){
		$criteria = new CDbCriteria;
		$criteria->alias = 'I';
		$criteria->select = 'I.id, I.created, I.kind, I.city, COALESCE(S.name, "?") as street, I.house, I.owner_id, U.username as "login", U.so_name as "lastname", U.name as "firstname"';
		$criteria->join = 'left join users U on U.id=I.owner_id left join streets S on S.id=I.street';
		$criteria->condition = 'I.status=3';
		$criteria->order = 'I.created';
		$Rows = ItemDetail::model()->findAll($criteria);

		// Получение статистической информации о Пользователях и Объектах
		// - Пользователи
		$criteria->alias = 'U';
		$criteria->select = 'U.role, COUNT(*) as "id"';
		$criteria->condition = '';
		$criteria->join = '';
		$criteria->order = '';
		$criteria->group = 'U.role';
		$aUsr = User::model()->findAll($criteria);
		$Users = array(); $q = 0;
		if (!empty($aUsr)) {
			foreach ($aUsr as $u) {
				$q+=$u->id;
				$Users[$u->role] = $u->id;
			}
			$Users['total'] = $q;
		}
		// - количество зарегистрированных сегодня
		$criteria->select = 'COUNT(*) as "id"';
		$criteria->condition = 'DATE(U.created)=DATE(NOW())';
//Без этих проверок не грузилась админка
		if( count(User::model()->find($criteria)) > 0){
		$Users['today'] = User::model()->find($criteria)->id;
		}else{
			 $Users['today'] = 0;
		}
		// - количество зарегистрированных вчера
		$criteria->condition = 'DATE(U.created)=DATE(NOW())-1';
		if(count( User::model()->find($criteria))>0){
			$Users['yesterday'] = User::model()->find($criteria)->id;
		} else {
			$Users['yesterday'] = 0;
		}
		// - Объекты
		$criteria->alias = 'I';
		$criteria->select = 'I.kind, COUNT(*) as "id"';
		$criteria->condition = '';
		$criteria->group = 'I.kind';
		$aItm = Search::model()->findAll($criteria);
		$Items = array(); $q = 0;
		if (!empty($aItm)) {
			foreach ($aItm as $i) {
				$q+=$i->id;
				$Items[$i->kind] = $i->id;
			}
			$Items['total'] = $q;
		}
		// - количество зарегистрированных сегодня
		$criteria->select = 'COUNT(*) as "id"';
		$criteria->condition = 'DATE(I.created)=DATE(NOW())';
		if(count( Search::model()->find($criteria))>0){
			$Items['today'] = Search::model()->find($criteria)->id;
		}  else{
			 $Items['today'] = 0;
		}
		// - количество зарегистрированных вчера
		$criteria->condition = 'DATE(I.created)=DATE(NOW())-1';
		if(count( Search::model()->find($criteria))>0){
			$Items['yesterday'] = Search::model()->find($criteria)->id;
		}   else{
			 $Items['yesterday'] = 0;
		}

		$this->render('index', array('rows' =>$Rows, 'aUsers'=>$Users, 'aItems'=>$Items));
	}

	public function actionObj_detail(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		$criteria = new CDbCriteria;
		$criteria->alias = 'I';
		$criteria->select = 'I.id, I.parent, I.created, I.itemType, I.status, I.status_changed, I.special, I.kind, I.owner_id, U.username as "login", U.so_name as "lastname", U.name as "firstname", I.city, COALESCE(D.name, "нет") as district, COALESCE(KS.shname, "нет") as kind_structure, COALESCE(FN.shname, "нет") as functionality, COALESCE(I.name, "без имени") as name, COALESCE(CL.shname, "нет") as class, COALESCE(S.name, "нет") as street, COALESCE(I.house, "нет") as house, COALESCE(I.housing, "нет") as housing, COALESCE(I.storey, "нет") as storey, COALESCE(I.storeys, "нет") as storeys, CASE I.service_lift when 0 then "нет" else "есть" END as service_lift, COALESCE(I.total_item_sqr, "нет") as total_item_sqr, COALESCE(RP.shname, "нет") as repair, COALESCE(PK.shname, "нет") as parking, I.fixed_qty, CASE I.video when 0 then "нет" else "есть" END as video, CASE I.access when 0 then "нет" else "есть" END as access, I.price, COALESCE(PU.shname, "нет") as pay_utilities, CASE I.internet when 0 then "нет" else "есть" END as internet, COALESCE(IP.shname, "нет") as providers, CASE I.telephony when 0 then "нет" else "есть" END as telephony, COALESCE(TC.shname, "нет") as tel_company, COALESCE(CC.shname, "нет") as contr_condition, I.about, COALESCE(I.welfare, "нет") as welfare, CASE I.heating when 0 then "нет" when 1 then "есть" else "не указано" END as heating, COALESCE(I.pallet_capacity, "нет") as pallet_capacity, COALESCE(I.shelf_capacity, "нет") as shelf_capacity, COALESCE(I.ceiling_height, "нет") as ceiling_height, COALESCE(I.work_height, "нет") as work_height, CASE I.cathead when 0 then "нет" when 1 then "есть" else "не указано" END as cathead, COALESCE(FL.shname, "нет") as flooring, COALESCE(I.floor_load, "нет") as floor_load, COALESCE(V.shname, "нет") as ventilation, CASE I.air_condit when 0 then "нет" else "есть" END as air_condit, CASE I.firefighting when 0 then "нет" else "есть" END as firefighting, CASE I.can_reclame when 0 then "нет" else "есть" END as can_reclame, COALESCE(I.el_power, "нет") as el_power, COALESCE(I.gates_qty, "нет") as gates_qty, COALESCE(I.gates_height, "нет") as gates_height, CASE I.rampant when 0 then "нет" when 1 then "есть" else "не указано" END as rampant, CASE I.autoways when 0 then "нет" else "есть" END as autoways, CASE I.railways when 0 then "нет" else "есть" END as railways';
		$criteria->join = 'left join users U on U.id=I.owner_id '.
			'left join districts D on D.id=I.district '.
			'left join streets S on S.id=I.street '.
			'left join kind_structure KS on KS.id=I.kind_structure '.
			'left join functionality FN on FN.id=I.functionality '.
			'left join classes CL on CL.id=I.class '.
			'left join repair RP on RP.id=I.repair '.
			'left join parking PK on PK.id=I.parking '.
			'left join pay_utilities PU on PU.id=I.pay_utilities '.
			'left join net_providers IP on IP.id=I.providers '.
			'left join tel_companies TC on TC.id=I.tel_company '.
			'left join contr_forms CC on CC.id=I.contr_condition '.
			'left join flooring FL on FL.id=I.flooring '.
			'left join ventilation V on V.id=I.ventilation ';
		$criteria->condition = 'I.id=:id';
		$criteria->params = array(':id'=>$id);
		$Item = ItemDetail::model()->find($criteria);
		if (empty($Item))
			throw new CHttpException(404);
		switch ($Item->status) {
			case 0 : {$Item->status = '"деактивирован"'; break;}
			case 1 : {$Item->status = '"показы отменены"'; break;}
			//case 2 : {$Item->status = '"уже (пока) сдан"'; break;} // временно отменен
			case 3 : {$Item->status = '"на модерации"'; break;}
			// case 5 : {$Item->status = '"показы приостановлены"'; break;} // временно отменен
			case 6 : {$Item->status = '"идут показы"'; break;}
		default: {$Item->status = '"Не известен"'; }
		}

		// Фотографии
		$criteria->alias = '';
		$criteria->join = '';
		$criteria->select = '*';
		$criteria->condition = 'id_item=:id';
		$criteria->params = array(':id'=>$Item->id);
		$criteria->order = 'id';
		$Fotos = Images::model()->findAll($criteria);
		// Прикрепленные файлы
		$criteria->condition = 'item=:id';
		$criteria->params = array(':id'=>$Item->id);
		$criteria->order = 'id';
		$Attachs = Attach::model()->findAll($criteria);
		// Жалобы на объект
		$criteria->alias = 'S';
		$criteria->select = 'S.id, S.tenant, S.sended, S.kind, T.username as "tenLogin", T.so_name as "tenLastname", T.name as "tenFirstname"';
		$criteria->join = 'join users T on T.id=S.tenant';
		$criteria->condition = 'S.item=:var';
		$criteria->params = array(':var'=>$Item->id);
		$criteria->order = 'S.id desc';
		$Claims = Claim::model()->findAll($criteria);

		// Выясним статус объекта: помещение или центр
		$Childs = null;
		if ($Item->parent==0) { // Если объект сам никому не принадлежит, проверим, есть ли у него дети
			$criteria->alias = '';
			$criteria->join = '';
			$criteria->select = '*';
			$criteria->condition = 'parent=:id';
			$criteria->params = array(':id'=>$Item->id);
			$criteria->order = 'id';
			$Childs = ItemDetail::model()->findAll($criteria);
		}

		$this->render('item_detail', array(
			'item'	=>$Item,
			'claims'=>$Claims,
			'childs'=>$Childs,
			'aFotos'=>$Fotos,
			'aAttachs'=>$Attachs
		));
	}

	#-------------------------------------------------------------------------
	# Управление статичными страницами
	#-------------------------------------------------------------------------

	public function actionStatic(){
		$criteria = new CDbCriteria;
		$criteria->select = 'name, name_ru';
		$criteria->condition = 'name != :exception';
		$criteria->params = array(':exception'=>'index');
		$criteria->order = 'sort';
		$Static_obj = Statics::model()->findAll($criteria);

		$this->render('static', array('Static_obj' =>$Static_obj));
	}

	public function actionPage_add(){
		$form = new Statics('add');

		if (!empty($_POST['Statics'])){
			$form->attributes = $_POST['Statics'];
			if($form->validate()){
				$criteria = new CDbCriteria;
				$criteria->select = new CDbExpression("MAX(sort) as maximum");
				$max_count = Statics::model()->findAll($criteria);
				if (count($max_count) != 0)
						$form->sort = $max_count[0]['maximum'] + 1;
				else	$form->sort = '1';

				if ($form->save(false))
					$this->redirect(array('control/static'));
			}
		}
		$this->render('page_add', array('form' =>$form));
	}

	public function actionPage_edit(){
		$name_of_page = $_GET['name'];

		$Static_obj = Statics::model()->find('name=:var', array(':var'=>$name_of_page));
		if (empty($Static_obj))
			throw new CHttpException(404);

		$form = new Statics();

		if (!empty($_POST['Statics'])){
			$Static_obj->attributes = $_POST['Statics'];
			if($Static_obj->validate()){
				if ($Static_obj->save(false))
				$this->redirect(array('control/static'));
			}
		}

		$url = $this->createUrl('page/view', array('name'=>$Static_obj->name));

		$this->render('page_edit', array(
			'form'			=>$form,
			'label'			=>$Static_obj->name_ru,
			'title'			=>$Static_obj->title,
			'description'	=>$Static_obj->description,
			'keywords'		=>$Static_obj->keywords,
			'text'			=>$Static_obj->text_page,
			'menu'			=>$Static_obj->menu,
			'url'			=>$url
		));
	}

	public function actionPage_delete(){
		$name_of_page = $_GET['name'];

		if ($name_of_page == 'index')
			$this->redirect(array('control/static'));

		if (isset($_GET['flg'])){
			$Static_del = Statics::model()->find('name=:var', array(':var'=>$name_of_page));
			if (empty($Static_del))
				throw new CHttpException(404);

			if ($Static_del->delete(false))
				$this->redirect(array('control/static'));
		}

		$this->render('page_delete', array('name_of_page' =>$name_of_page));
	}

	#-------------------------------------------------------------------------
	# Управление главным меню
	#-------------------------------------------------------------------------

	public function actionMenu(){
		$criteria = new CDbCriteria;
		$criteria->order = 'sort';
		$criteria->condition = 'menu=:var and name != :exception';
		$criteria->params = array(':var'=>'1', ':exception'=>'index');
		$Static_obj = Statics::model()->findAll($criteria);

		$this->render('menu', array('Static_obj' =>$Static_obj));
	}

	public function actionMenu_up(){
		$name_of_page = $_GET['name'];
		$name_of_old_page = $_GET['name_old'];

		if ($name_of_page == 'index' || $name_of_old_page == 'index' || $name_of_old_page == '0')
			$this->redirect(array('control/menu'));

		$Static_old_obj = Statics::model()->find('name=:var', array(':var'=>$name_of_old_page));
		if (empty($Static_old_obj))
			throw new CHttpException(404);

		$temp_old = $Static_old_obj->sort;

		$Static_obj = Statics::model()->find('name=:var', array(':var'=>$name_of_page));
		if (empty($Static_obj))
			throw new CHttpException(404);

		$temp = $Static_obj->sort;
		$Static_old_obj->sort = $temp;
		$Static_obj->sort = $temp_old;

		if ($Static_old_obj->save() && $Static_obj->save())
			$this->redirect(array('control/menu'));
	}

	public function filters(){
		return array(
			array(
				'application.filters.AccessFilter',
				'role'=>'admin'
			),
		);
	}

}?>