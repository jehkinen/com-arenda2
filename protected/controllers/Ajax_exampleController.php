<?php
  class Ajax_exampleController extends CController{
    public function actionLoad_form(){
      if (!isset($_POST['type']))
	    throw new CHttpException(404);

       switch ($_POST['type']){
         case 'tab1':{           $criteria = new CDbCriteria;
	       $criteria->select = 'id, name';
	       $criteria->order = 'name';
	       $criteria->condition = 'city=:ct';
	       $criteria->params = array(':ct'=>'Екатеринбург');
	       $Districts_obj = District::model()->findAll($criteria);

           $main_form = '<div class="row">
                         <strong><label for="Search_form_raion">Район</label>:</strong><br />
                         <select name="Search_form[raion]" id="Search_form_raion">
                         <option value="0" selected="selected">-Не важен-</option>';
           foreach ($Districts_obj as $object)
           $main_form .= '<option value="'.$object->id.'">'.$object->name.'</option>';
           $main_form .= '</select></div>';
           $main_form .= '<input type="hidden" value="'.$_POST['type'].'" name="type" id="type" />';

           $else_form = '<input name="Name" type="text" value="первая вкладка">';

	       echo json_encode(array($main_form, $else_form));

           break;         }

         case 'tab2':{           $criteria = new CDbCriteria;
           $criteria->select = 'id, name';
           $criteria->order = 'name';
           $criteria->condition = 'city=:ct';
           $criteria->params = array(':ct'=>'Екатеринбург');
           $Streets_obj = Street::model()->findAll($criteria);

           $main_form = '<div class="row">
                         <strong><label for="Search_form_ulica">Улица</label>:</strong><br />
                         <select name="Search_form[ulica]" id="Search_form_ulica">
                         <option value="0" selected="selected">-Любая-</option>';
           foreach ($Streets_obj as $object)
           $main_form .= '<option value="'.$object->id.'">'.$object->name.'</option>';
           $main_form .= '</select></div>';
           $main_form .= '<input type="hidden" value="'.$_POST['type'].'" name="type" id="type" />';

	       $else_form = '<input name="Name" type="text" value="вторая вкладка">';

	       echo json_encode(array($main_form, $else_form));

           break;
         }

         case 'tab3':{
           $Structure_obj = KindStructure::model()->findAll();

           $main_form = '<div class="row">
                         <strong><label for="Search_form_tip_sooruj">Тип сооружения</label>:</strong><br />
                         <select name="Search_form[tip_sooruj]" id="Search_form_tip_sooruj">
                         <option value="0" selected="selected">-Любой-</option>';
           foreach ($Structure_obj as $object)
           $main_form .= '<option value="'.$object->id.'">'.$object->name.'</option>';
           $main_form .= '</select></div>';
           $main_form .= '<input type="hidden" value="'.$_POST['type'].'" name="type" id="type" />';

           $else_form = '<input name="Name" type="text" value="третья вкладка">';

	       echo json_encode(array($main_form, $else_form));

           break;
         }

         case 'tab4':{           $Functionality_obj = Functionality::model()->findAll();

           $main_form = '<div class="row">
                         <strong><label for="Search_form_funkcional">Функциональное назначение</label>:</strong><br />
                         <select name="Search_form[funkcional]" id="Search_form_funkcional">
                         <option value="0" selected="selected">-Любое-</option>';
           foreach ($Functionality_obj as $object)
           $main_form .= '<option value="'.$object->id.'">'.$object->name.'</option>';
           $main_form .= '</select></div>';
           $main_form .= '<input type="hidden" value="'.$_POST['type'].'" name="type" id="type" />';

           $else_form = '<input name="Name" type="text" value="четвертая вкладка">';

	       echo json_encode(array($main_form, $else_form));

           break;
         }

         default:{           throw new CHttpException(404);         }
       }

	}  }
?>