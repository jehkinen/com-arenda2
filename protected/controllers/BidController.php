<?php class BidController extends CController{
/* 
Разобраться, должна ли Система Вентиляции (ventilation) фигурировать в заявках
 */
	public $defaultAction = 'view';
	public $layout = 'inner02';
	public $pageDescription;
	public $pageKeywords;

	public function actionView(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;
		$criteria = new CDbCriteria;
		$criteria->alias = 'T';
		$criteria->select = 'T.id, T.who_id, T.kind, T.city, COALESCE(D.name, "не важен") as "district", T.created, COALESCE(KS.shname, "не важен") as kind_structure, COALESCE(FN.shname, "не важна") as functionality, COALESCE(S.name, "не важна") as street, CASE T.service_lift when 1 then "нужен" else "нет" END as service_lift, COALESCE(CL.shname, "не важен") as class, COALESCE(RP.shname, "не важно") as repair, COALESCE(PK.shname, "не важна") as parking, CASE T.internet when 1 then "нужен" else "нет" END as internet, CASE T.telephony when 1 then "нужен" else "нет" END as telephony, CASE T.heating when 1 then "нужен" else "нет" END as heating, CASE T.cathead when 1 then "нужен" else "нет" END as cathead, CASE T.firefighting when 1 then "нужен" else "нет" END as firefighting, CASE T.rampant when 1 then "нужен" else "нет" END as rampant, CASE T.autoways when 1 then "нужен" else "нет" END as autoways, CASE T.railways when 1 then "нужен" else "нет" END as railways, NULLIF(T.gates_height1, "0") as "gates_height1", NULLIF(T.gates_height2, "0") as "gates_height2", NULLIF(T.ceiling_height1, "0") as "ceiling_height1", NULLIF(T.ceiling_height2, "0") as "ceiling_height2", NULLIF(T.storeyMin, "0") as "storeyMin", NULLIF(T.storeyMax, "0") as "storeyMax", NULLIF(T.squareMin, "0") as "squareMin", NULLIF(T.squareMax, "0") as "squareMax", NULLIF(T.priceMin, "0") as "priceMin", NULLIF(T.priceMax, "0") as "priceMax", U.so_name as "whoSoName", U.name as "whoName", U.otchestvo as "whoOtchestvo", U.phone as "whoPhone"';
		$criteria->join = 'Left join districts D on D.id=T.district join users U on U.id=T.who_id '.
			'left join streets S on S.id=T.street '.
			'left join kind_structure KS on KS.id=T.kind_structure '.
			'left join functionality FN on FN.id=T.functionality '.
			'left join classes CL on CL.id=T.class '.
			'left join repair RP on RP.id=T.repair '.
			'left join parking PK on PK.id=T.parking ';
		$criteria->condition = 'T.id=:id';
		$criteria->params = array(':id'=>$id);
		$Tender = Tenders::model()->find($criteria);

		if (empty($Tender)) 
			throw new CHttpException(404);

		$this->pageTitle = 'Заявки арендаторов';
		$this->pageDescription = '';
		$this->pageKeywords = '';

		$this->render('view', array('info'	=>$Tender));

	}

	public function filters(){
		return array(
			array(
				'application.filters.AccessFilter',
				'role'=>'owner,tenant,admin'
			)
		);
	}

}?>