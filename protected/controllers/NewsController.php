<?php class NewsController extends CController{
	public $layout = 'inner02';
	public $defaultAction = 'all';	public $pageClass = 'any';	public $pageDescription;
	public $pageKeywords;

	public function actionView() {
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$news = News::model()->findByPK($get->id_out);
		if (empty($news))
			throw new CHttpException(404);

		$this->pageTitle = $news->header;
		$this->pageDescription = $news->description;
		$this->pageKeywords = $news->keywords;
		$this->render('view', array('item'	=>$news));
	}

	public function actionAll() {
		$criteria = new CDbCriteria;
		$criteria->order = 'id desc';
		$qty = News::model()->count($criteria);
		# Постраничная разбивка
		$pages = new CPagination($qty);
		$pages->pageSize = 15;
		$pages->applyLimit($criteria);
		$news = News::model()->findAll($criteria);
		if (empty($news))
			throw new CHttpException(404);

		$this->pageTitle = 'Блог проекта';
		$this->pageDescription = '';
		$this->pageKeywords = '';
		$this->render('newstype', array(
			'pages'	=>$pages,
			'rows'	=>$news
		));
	}
}?>