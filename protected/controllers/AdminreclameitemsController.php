<?php class AdminreclameitemsController extends CController{

	public $defaultAction = 'view';
	public $layout = 'control';
	public $pageDescription;
	public $pageKeywords;

	public function actionView(){

		$this->pageTitle = 'Просмотр рекламных кампаний объектов недвижимости';
		$this->pageDescription = '';
		$this->pageKeywords = '';
		$usr = Yii::app()->user;

		$form = new AdminItems_form();
		if (isset($_POST['AdminItems_form']) || (count($_POST)>0)){
			$form->attributes = $_POST['AdminItems_form'];
			if ($form->validate()){
				if ((isset($_POST['AdminItems_form']['city'])) && (!is_numeric($_POST['AdminItems_form']['city'])))
					$usr->setState('aricity', $_POST['AdminItems_form']['city']);
				elseif (isset($usr->aricity))	unset ($usr->aricity);
				if ((isset($_POST['AdminItems_form']['owner'])) && ($_POST['AdminItems_form']['owner']!=0)) // Искавший посетитель
					$usr->setState('ariwho', $_POST['AdminItems_form']['owner']);
				elseif (isset($usr->ariwho))	unset ($usr->ariwho);
			}
		}

		$whereString = ''; // Переменная для формирования WHERE-предложения sql-запроса
		$Params = array(); // Массив параметров, передаваемых в запрос
		if (isset($usr->aricity)) {
			$whereString .= ' and I.city=:ct';
			$Params[':ct'] = $usr->getState('aricity', '');
		}
		if (isset($usr->ariwho)) {
			$whereString .= ' and I.owner_id=:wn';
			$Params[':wn'] = $usr->getState('ariwho', 0);
		}
		if ($whereString!='')	$whereString = substr($whereString, 5);

        # Определяем сортировку для запроса, если нет сортировки то сортируем по id обекта
        $order = '';
		if (isset($_GET['sort'])){
		  $order = $_GET['sort'];
		  if (isset($_GET['type'])) $order .= ' desc';
		}
		if (empty($order)) $order = 'id';

		$criteria = new CDbCriteria;
		$criteria->alias = 'R';
		$criteria->select = 'R.id, R.item, R.created, CASE R.isActive when 1 then "да" else "нет" END as isActive, R.name, CASE R.onMain when 1 then "да" else "нет" END as onMain, CASE R.onSearch when 1 then "да" else "нет" END as onSearch, CASE R.onAny when 1 then "да" else "нет" END as onAny, R.start, R.stop, R.qty, U.id as "who_id", U.username as "whoLogin", U.so_name as "whoSoName", U.name as "whoName", (Select COUNT(*) from reclame_item_stat S where S.camp=R.id) as "totalQty"';
		$criteria->join = 'join items I on I.id=R.item join users U on U.id=I.owner_id';
		$criteria->condition = $whereString;
		$criteria->params = $Params;
		$criteria->order = 'R.'.$order;

		$qty = ReclameItem::model()->count($criteria);
		# Постраничная разбивка
		$pages = new CPagination($qty);
		$pages->pageSize = 20;
		$pages->applyLimit($criteria);
		# Получаем текущую страницу (нумерация начинается с 0, поэтому +1)
		$Cpage = $pages->getCurrentPage()+1;

		$Items = ReclameItem::model()->findAll($criteria);

		$criteria = new CDbCriteria;
		$criteria->order = 'name';
		$arCities = CHtml::listData( City::model()->findAll($criteria), 'name', 'name');
		array_unshift($arCities, 'Любой');

		$criteria = new CDbCriteria;
		$criteria->select = 'id, username, so_name';
		$criteria->condition = 'role=:r';
		$criteria->params = array(':r'=>'owner');
		$criteria->order = 'username';
		$Rows = User::model()->findAll($criteria);
		$arUsers[0] = 'Любой';
		if (!empty($Rows))
			foreach ($Rows as $user)
				$arUsers[$user->id] = $user->username.' ('.$user->so_name.')';

		$this->render('view', array(
			'form'		=>$form,
			'rows'		=>$Items,
			'pages'		=>$pages,
			'foundQty'	=>$qty,
			'Cities'	=>$arCities,
			'city'		=>$usr->getState('aricity', 0),
			'Users'		=>$arUsers,
			'owner'		=>$usr->getState('ariwho', 0),
			'Cpage'		=>$Cpage
			));
	}

	public function actionAdd() {
		$Row = new ReclameItem();
		$oTxt = '';
		if (!empty($_POST['ReclameItem'])){
			$Row->attributes = $_POST['ReclameItem'];
			$dateStart = $Row->start;
			$dateStop = $Row->stop;
			$Row->start = $this->convertDate($Row->start);
			$Row->stop = $this->convertDate($Row->stop);
			$oTxt = $Row->txt;
			$filter = new CHtmlPurifier();
			$Row->txt = $filter->purify($Row->txt);
			if($Row->validate()){
				if ($Row->save(false))
					$this->redirect(array('adminreclameitems/view'));
			} else {
				$Row->start = $dateStart;
				$Row->stop = $dateStop;
			}
		}
		$this->render('add', array('form' =>$Row, 'oTxt' =>$oTxt));
	}

	public function actionEdit() {
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$oRow = ReclameItem::model()->findByPK($get->id_out);
		if (empty($oRow))
			throw new CHttpException(404);
		if (!empty($_POST['ReclameItem'])){
			$oRow->attributes = $_POST['ReclameItem'];
			$oRow->start = $this->convertDate($oRow->start);
			$oRow->stop = $this->convertDate($oRow->stop);
			$filter = new CHtmlPurifier();
			$oRow->txt = $filter->purify($oRow->txt);
			if($oRow->validate()){
				if ($oRow->save(false))
					$this->redirect(array('adminreclameitems/view'));
			}
		}
		$this->render('edit', array('form' =>$oRow,
			'oName'		=>$oRow->name,
			'oTxt'		=>$oRow->txt,
			'oIsActive'	=>$oRow->isActive,
			'oOnMain'	=>$oRow->onMain,
			'oOnSearch'	=>$oRow->onSearch,
			'oOnAny'	=>$oRow->onAny,
			'oStart'	=>$oRow->start,
			'oStop'		=>$oRow->stop,
			'oQty'		=>$oRow->qty
		));
	}

	public function actionDelete() {
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$Row = ReclameItem::model()->findByPK($get->id_out);
		if (empty($Row))
			throw new CHttpException(404);
		if (isset($_GET['flg'])){
			if ($Row->delete(false)) {
				// Удаляем статистику показов Кампании
				$cmd = Yii::app()->db->createCommand('Delete from reclame_item_stat where camp='.$get->id_out.';');
				$cmd->execute();
				$this->redirect(array('adminreclameitems/view'));
			}
		}
		$this->render('delete', array('id' =>$get->id_out));
	}

	public function actionUnset(){
		$usr = Yii::app()->user;
		unset($usr->aricity);
		unset($usr->ariwho);
		$this->redirect(array('adminreclameitems/view'));
	}

	public function filters(){
	return array(
		array(
			'application.filters.AccessFilter',
			'role'=>'admin',
		)
	);
	}

	private function convertDate($dt){
		list ($d, $m, $y) = explode('.', $dt);
		return $y.'-'.$m.'-'.$d;
	}
}
?>