<?php
class AdminusersController extends CController{
	public $defaultAction = 'view';
	public $layout = 'control';
	public $pageDescription;
	public $pageKeywords;

	public function actionView(){
		$this->pageTitle = 'Управление пользователями';
		$this->pageDescription = '';
		$this->pageKeywords = '';
		$usr = Yii::app()->user;
		$form = new AdminUsers_form();
		if (isset($_POST['AdminUsers_form']) || (count($_POST)>0)){
			$form->attributes = $_POST['AdminUsers_form'];
			if ($form->validate()){
				if ((isset($_POST['AdminUsers_form']['status'])) && ($_POST['AdminUsers_form']['status']!=90)) // Статус пользователя
					$usr->setState('austatus', $_POST['AdminUsers_form']['status']);
				elseif (isset($usr->austatus))	unset ($usr->austatus);
				if ((isset($_POST['AdminUsers_form']['role'])) && ($_POST['AdminUsers_form']['role']!=90)) // Роль
					$usr->setState('aurole', $_POST['AdminUsers_form']['role']);
				elseif (isset($usr->aurole))	unset ($usr->aurole);
			}
		}

		$whereString = ''; // Переменная для формирования WHERE-предложения sql-запроса
		$Params = array(); // Массив параметров, передаваемых в запрос
		if (isset($usr->aurole)) {
			$whereString .= ' and U.role=:kd';
			$Params[':kd'] = $usr->getState('aurole', 0);
		}
		if (isset($usr->austatus)) {
			$whereString .= ' and U.state=:st';
			$Params[':st'] = $usr->getState('austatus', 0);
		}
		if ($whereString!='')	$whereString = substr($whereString, 5);

		if (isset($_GET['sort'])){
			switch ($_GET['sort']) {
				case 1 : {$order = 'U.created'; break;}
				case 4 : {$order = 'U.username'; break;}
				case 5 : {$order = 'U.so_name, U.name, U.id'; break;}
				case 8 : {$order = '10'; break;}
				case 9 : {$order = 'I.id'; break;}
				default : $order = 'U.id';
			}
			if (isset($_GET['type'])) {
				if ($_GET['sort']==5)
					$order = 'U.so_name desc, U.name desc, U.id';
				else	$order .= ' desc';
			}
		} else	$order = 'U.id';
		
		$criteria = new CDbCriteria;
		$criteria->alias = 'U';
		$criteria->select = 'U.id, U.created, U.state, U.role, U.username, U.so_name, U.name, U.otchestvo, U.e_mail, CASE U.role when "owner" then (Select COUNT(*) from items I where I.owner_id=U.id) when "tenant" then (Select COUNT(*) from tenders T where T.who_id=U.id) END as act';
		$criteria->condition = $whereString;
		$criteria->params = $Params;
		$criteria->order = $order;

		$qty = AdminUsers::model()->count($criteria);
		# Постраничная разбивка 
		$pages = new CPagination($qty);
		$pages->pageSize = 25;
		$pages->applyLimit($criteria);

		$Items = AdminUsers::model()->findAll($criteria);
		
		$arStatus = array(90=>'Любой', 0=>'удален', 5=>'действующий');
		$arRoles = array(90=>'Все', 'owner'=>'собственник', 'tenant'=>'арендатор');

		$this->render('view', array(
			'form'		=>$form,
			'rows'		=>$Items,
			'pages'		=>$pages,
			'foundQty'	=>$qty,
			'Roles'		=>$arRoles,
			'role'		=>$usr->getState('aurole', 90),
			'Statuses'	=>$arStatus,
			'status'	=>$usr->getState('austatus', 90)
			));
	}
	public function actionDetail(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = AdminUsers::model()->find($criteria);
		if (empty($Row))
			throw new CHttpException(404);
		if ($Row->role=='tenant') {
			$criteria = new CDbCriteria;
			$criteria->alias = 'T';
			$criteria->select = 'T.id, T.kind, T.created, T.city, COALESCE(D.name, "не указан") as district, COALESCE(S.name, "не важна") as street, T.priceMin, T.priceMax, T.squareMin, T.squareMax';
			$criteria->join = 'left join districts D on D.id=T.district left join streets S on S.id=T.street';
			$criteria->condition = 'T.who_id=:var';
			$criteria->params = array(':var'=>$id);
			$Orders = Tenders::model()->findAll($criteria);
			$criteria = new CDbCriteria;
			$criteria->alias = 'N';
			$criteria->select = 'N.id, N.item, I.kind, N.created, I.city, COALESCE(D.name, "не указан") as district, COALESCE(S.name, "не важна") as street';
			$criteria->join = 'join items I on I.id=N.item left join districts D on D.id=I.district left join streets S on S.id=I.street';
			$criteria->condition = 'tenant=:var';
			$criteria->params = array(':var'=>$id);
			$Notes = Notepads::model()->findAll($criteria);			
			$criteria->alias = 'C';
			$criteria->select = 'C.id, C.item, C.sended, C.kind, O.username as "ownLogin", O.so_name as "ownLastname", O.name as "ownFirstname", I.owner_id as "itemOwner", I.name as "itemName", COALESCE(S.name, "") as "itemStreet", COALESCE(I.house, "?") as "itemHouse"';
			$criteria->join = 'join items I on I.id=C.item left join streets S on S.id=I.street join users O on O.id=I.owner_id';
			$criteria->condition = 'C.tenant=:var';
			$criteria->params = array(':var'=>$id);
			$criteria->order = 'C.id desc';
			$Claims = Claim::model()->findAll($criteria);
		} elseif ($Row->role=='owner') {
			$criteria = new CDbCriteria;
			$criteria->alias = 'I';
			$criteria->select = 'I.id, I.created, I.status, I.parent, (Select COALESCE(MAX(C.id), 0) from items C where C.parent=I.id) as "hasChild", I.kind, I.city, D.name as "district", S.name as "street", COALESCE(I.house, "?") as house, COALESCE(I.housing, "?") as housing, (Select COUNT(*) from item_show_stat SS where SS.item=I.id) as "showQty"';
			$criteria->join = 'left join streets S on S.id=I.street left join districts D on D.id=I.district';
			$criteria->condition = 'owner_id=:var';
			$criteria->params = array(':var'=>$id);
			$Items = Search::model()->findAll($criteria);
			$criteria->alias = 'C';
			$criteria->select = 'C.id, C.item, C.sended, C.tenant, C.kind, T.username as "tenLogin", T.so_name as "tenLastname", T.name as "tenFirstname", I.owner_id as "itemOwner", I.name as "itemName", COALESCE(S.name, "") as "itemStreet", COALESCE(I.house, "?") as "itemHouse"';
			$criteria->join = 'join users T on T.id=C.tenant join items I on I.id=C.item left join streets S on S.id=I.street';
			$criteria->condition = 'I.owner_id=:var';
			$criteria->params = array(':var'=>$id);
			$criteria->order = 'C.id desc';
			$Claims = Claim::model()->findAll($criteria);
		}
		$criteria = new CDbCriteria;
		$criteria->condition = 'reciever=:var';
		$criteria->params = array(':var'=>$id);
		$criteria->order = 'sdate desc';
		$LogMess = LogSendMessages::model()->findAll($criteria);

		$this->render('detail', array(
			'row'=>$Row,
			'orders'=>$Orders,
			'items'=>$Items,
			'notes'=>$Notes,
			'claims'=>$Claims,
			'logMess'=>$LogMess
		));
	}

	public function actionDelete(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		if (isset($_GET['flg'])){
			$criteria = new CDbCriteria;
			$criteria->condition = 'id=:var';
			$criteria->params = array(':var'=>$id);
			$Row = AdminUsers::model()->find($criteria);
			if (empty($Row))
				throw new CHttpException(404);

			$Row->delete(false);
			$this->redirect(array('adminusers/view'));
		}

		$this->render('delete', array('id'=>$id));
	}

	public function actionShowmessage(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		$Mess = LogSendMessages::model()->findByPK($id);
		$User = null;
		if (!empty($Mess)) {
			$User = AdminUsers::model()->findByPK($Mess->reciever);
		}

		$this->render('showmessage', array(
			'mess'=>$Mess,
			'user'=>$User
		));
	}

	public function actionSendmessage(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		$User = AdminUsers::model()->findByPK($id);

		$form = new UserSendMessage_form();
		$defSubject = 'Портал Com-Arenda.ru';
		$oldTxt = '';

		if (!empty($_POST['UserSendMessage_form'])){
			$form->attributes = $_POST['UserSendMessage_form'];
			$filter = new CHtmlPurifier();
			$form->subject = $filter->purify($form->subject);
			$form->txt = $filter->purify($form->txt);
			$defSubject = $form->subject;
			$oldTxt = $form->txt;
			if ($form->validate()){
				@require_once('protected/extensions/mailer/class.mailer.php');
				// Отсылаем письмо
				$mail = new Mailer();
				$mail->IsHTML(true);
				$mail->From = Yii::app()->params->admin_mail;
				$mail->FromName = 'Портал Com-Arenda.ru';
				$mail->Subject  = $form->subject;
				$mail->Body = $form->txt;
				$mail->AddAddress($User->e_mail);
				// $mail->AddAddress('dpriest@list.ru'); 
				if ($mail->Send())
					$succ = 1;
				else	$succ = 0;
				$cmd = Yii::app()->db->createCommand('Insert into log_send_messages (kind, success, reciever, address, txt) Values (4, '.$succ.', '.$id.', '."'$User->e_mail', '$form->txt');");
				$cmd->execute();
				$this->redirect(array('detail', 'id'=>$id));
			}
		}
	}

	public function actionSendsms(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		$User = AdminUsers::model()->findByPK($id);

		if (isset($row->info_phone)) $smsTel = $User->info_phone;
		else {
			$smsTel = '';
			$fun = $this->widget('AnalyzePhone', array('phone'=>$User->phone));
			if ($fun->isCellphone)
				$smsTel = $fun->phone;
		}
		if ($smsTel!='') {

			$form = new SendSMS_form();
			$oldTxt = '';

			if (!empty($_POST['SendSMS_form'])){
				$form->attributes = $_POST['SendSMS_form'];
				$filter = new CHtmlPurifier();
				$form->txt = $filter->purify($form->txt);
				$oldTxt = $form->txt;
				if ($form->validate()){
					@require_once('protected/extensions/smspilot/smspilot.php');
					// Отсылаем SMS-сообщение
					if (sms('7'.$smsTel, $form->txt,'Com-Arenda'))
						$succ = 1;
					else	$succ = 0;
					$cmd = Yii::app()->db->createCommand('Insert into log_send_messages (kind, success, reciever, address, txt) Values (5, '.$succ.', '.$id.', '."'$smsTel', '$form->txt');");
					$cmd->execute();
					$this->redirect(array('detail', 'id'=>$id));
				}
			}
		}

		$this->render('sendsms', array(
			'form'		=>$form,
			'oldTxt'	=>$oldTxt,
			'canSend'	=>($smsTel!=''),
			'user'		=>$User
		));
	}

	public function actionUnset(){
		unset(Yii::app()->user->austatus);
		unset(Yii::app()->user->aurole);
		$this->redirect(array('adminusers/view'));
	}

	public function filters(){
	return array(
		array(
			'application.filters.AccessFilter',
			'role'=>'admin',
		)
	);
	}
}
?>