<?php

class AjaxobjectsController extends CController{


    public function actionLoad_parent_item($id){

       $item = Object2::model()->findByPk($id);

       $jsonResponse['city'] = $item->city;
       $jsonResponse['name'] = $item->name;
       $jsonResponse['class'] = $item->class;
       $jsonResponse['district'] = $item->district ;
       $jsonResponse['street'] = $item->street;
       $jsonResponse['house'] = $item->house;

       $jsonResponse['price'] = $item->price;
       $jsonResponse['total_item_sqr'] = $item->total_item_sqr;

       $jsonResponse['kind_structure'] = $item->kind_structure;
       $jsonResponse['storeys'] = $item->storeys;

       $jsonResponse['service_lift'] = $item->service_lift;
       $jsonResponse['repair'] = $item->repair;
       $jsonResponse['parking'] = $item->parking;
       $jsonResponse['pay_utilities'] = $item->pay_utilities;
       $jsonResponse['flooring'] = $item->flooring;
       $jsonResponse['ventilation'] = $item->ventilation;
       $jsonResponse['contr_condition'] = $item->contr_condition;
       $jsonResponse['access'] = $item->access;

       $jsonResponse['video'] = $item->video;
       $jsonResponse['internet'] = $item->internet;

       $jsonResponse['telephony'] = $item->telephony;
       $jsonResponse['welfare'] = $item->welfare;

       $jsonResponse['air_condit'] = $item->air_condit;
       $jsonResponse['firefighting'] = $item->firefighting;
       $jsonResponse['can_reclame'] = $item->can_reclame;

       echo CJSON::encode($jsonResponse);
    }


    public function actionGet_parent_items() {
        ?>
    <h4>Ваши центры недвижимости</h4>
    <div class="oneCol">
        <input id="ytObjects_add_parent" type="hidden" value="" name="Objects_add[parent]" />
        <?php
        if (isset($_POST['id']) && ($_POST['id']!='') && is_numeric($_POST['id'])) {
            $criteria = new CDbCriteria;
            $criteria->alias = 'I';
            $criteria->select = 'I.id, I.city, S.name as street, I.house';
            $criteria->join = 'join streets S on S.id=I.street';
            $criteria->condition = 'owner_id=:oi and itemType=:it and parent=:pr';
            $criteria->params = array(':oi'=>$_POST['id'], ':pr'=>0, 'it'=>1);
            $criteria->order = 'I.city, S.name, I.house, I.id';
            $Items = Search::model()->findAll($criteria);
            if (empty($Items))      echo('Не найдено');
            else {
                $i = 0;
                foreach ($Items as $item) { ?>
                    <input id="Objects_add_parent_<?php echo($i);?>" value="<?php echo($item->id);?>" type="radio" name="Objects_add[parent]" <?php if($i==0) echo('checked="checked"');?>/> <label for="Objects_add_parent_<?php echo($i);?>"><?php echo($item->city);?>, ул. <?php echo($item->street);?> дом <?php echo($item->house);?></label><br/>
                    <?php $i++;
                }
            }
        }
        ?>
    </div>
    <?php
    }


	public function actionGet_parent_items_() {
		if (isset($_POST['id']) && ($_POST['id']!='') && is_numeric($_POST['id'])) {
			$criteria = new CDbCriteria;
			$criteria->alias = 'I';
			$criteria->select = 'I.id, I.city, S.name as street, I.house';
			$criteria->join = 'join streets S on S.id=I.street';
			$criteria->condition = 'owner_id=:oi and itemType=:it and parent=:pr';
			$criteria->params = array(':oi'=>$_POST['id'], ':pr'=>0, 'it'=>1);
			$criteria->order = 'I.city, S.name, I.house, I.id';
			$Items = Search::model()->findAll($criteria);

            $jsonResponse = array();
			if ($Items){
                $list = array();
				foreach ($Items as $item) {
                    $list[$item->id] = "$item->city , ул. $item->street дом $item->house";
				}
                $jsonResponse['items'] = $this->renderPartial('//layouts/blocks/_list', array('list'=>$list, 'param'=>'выберите центр'),true,true);


			}
            else{
                $jsonResponse['items'] = 0;
            }

            echo CJSON::encode($jsonResponse);
		}
        return;
	}

	public function actionChange_state() {
	
		if (!isset($_POST['id']) || !isset($_POST['stat']) || Yii::app()->user->id['user_role'] != 'admin')
			throw new CHttpException(404);

		$item = Search::model()->findByPK($_POST['id']);
		if (empty($item))
			CApplication::end();

		$item->status = $_POST['stat'];
		$item->save(false);
	
	}

	public function actionChange_spec() {
	
		if (!isset($_POST['id']) || !isset($_POST['spec']) || Yii::app()->user->id['user_role'] != 'admin')
			throw new CHttpException(404);

		$item = Search::model()->findByPK(substr($_POST['id'], 2));
		if (empty($item))
			CApplication::end();

		$item->special = $_POST['spec'];
		$item->save(false);
	
	}

}?>