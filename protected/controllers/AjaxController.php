<?php class AjaxController extends CController{

	#-----------------------------------------------
	# Управление блокнотом в кабинете арендатора
	#-----------------------------------------------

	public function actionNotepad_add(){
	// Добавление объекта в блокнот арендатора
		if (!isset($_POST['id']))
			throw new CHttpException(404);
		$idItem = substr($_POST['id'], 3);
		// Проверим, не присутствует ли уже добавляемый объект в блокноте
		$exists = Notepads::model()->exists('item=:it and tenant=:tn', array(':it'=>$idItem, ':tn'=>Yii::app()->user->id['user_id']));
		if (!$exists){
			$NewRec = new Notepads;
			$NewRec->status = 1;
			$NewRec->item = $idItem;
			$NewRec->tenant = Yii::app()->user->id['user_id'];
			$NewRec->save(false);
		}
	}

	public function actionNotepad_tenant_delete(){
		if (!isset($_POST['id']))
			throw new CHttpException(404);
		$rec = Notepads::model()->findByPk($_POST['id']);
		if (!empty($rec) && ($rec->tenant=Yii::app()->user->id['user_id']))
			$rec->delete(false);
	}

	public function actionOrder_tenant_delete(){
		if (!isset($_POST['id']))
			throw new CHttpException(404);
		//$id = substr($_POST['id'], 2);
		$id = $_POST['id'];
		$rec = Tenders::model()->findByPk($id);
		if (!empty($rec) && ($rec->who_id=Yii::app()->user->id['user_id'])) {
			// Удалим связь заявки с собственниками
			$cmd = Yii::app()->db->createCommand('Delete from bid_owners where tender='.$id.';');
			$cmd->execute();
			$rec->delete(false);
		}
	}

	#-----------------------------------------------
	# Управление блокнотом в кабинете собственника
	#-----------------------------------------------

	public function actionNotepad_owner_save(){

		if (!isset($_POST['id']) || !isset($_POST['stat']))
			throw new CHttpException(404);

		$item = Search::model()->findByPK($_POST['id']);
		if (!empty($item) && ($item->status!=0) && ($item->status!=3) && ($item->owner_id=Yii::app()->user->id['user_id'])) {
			if (!empty($item->status_changed)) {
				list($y, $m, $d) = explode('-', $item->status_changed);
				$doInform = (time() > 260000 + mktime(0, 0, 0, $m, $d, $y));
			} else $doInform = true;
			$item->status = $_POST['stat'];
			if ($item->status ==6)
				$item->created = new CDbExpression('NOW()');
			$item->status_changed = new CDbExpression('NOW()');
			$item->save(false);
			if ($doInform)	$this->widget('ChangeItemStatus', array('itemId'=>$item->id));
		}
	}

	public function actionNotepad_owner_delete(){

		if (!isset($_POST['id']))
			throw new CHttpException(404);

		$item = Search::model()->findByPK($_POST['id']);
		if (empty($item) || ($item->owner_id!=Yii::app()->user->id['user_id']))
			CApplication::end();

		// Удалим статистику показов объекта
		$cmd = Yii::app()->db->createCommand('Delete from item_show_stat where item='.$item->id.';');
		$cmd->execute();
		// Удалим объект из блокнтов арендаторов
		$cmd = Yii::app()->db->createCommand('Delete from notepad where item='.$item->id.';');
		$cmd->execute();
		$s = Yii::app()->params->web_root;
		$i = strrpos($s, '/'); $a = strlen($s)-1;
		if ($i != $a)	$s.='/';
		// Удалим фотографии объекта
		$Fotos = Images::model()->findAll('id_item=:ii', array(':ii'=>$item->id));
		if (!empty($Fotos)) {
			$p = $s.'storage/images/';
			foreach ($Fotos as $foto) {
				@unlink ($p.$foto->img);
				@unlink ($p.'_thumbs/'.$foto->name_medium);
				@unlink ($p.'_thumbs/'.$foto->name_small);
			}
			$cmd = Yii::app()->db->createCommand('Delete from images where id_item='.$item->id.';');
			$cmd->execute();
		}
		// Удалим прикрепленные файлы
		$Attachs = Attach::model()->findAll('item=:ii', array(':ii'=>$id));
		if (!empty($Attachs)) {
			$p = $s.'storage/files/objs/';
			foreach ($Attachs as $fl)
				@unlink ($p.$fl->filename);
			$cmd = Yii::app()->db->createCommand('Delete from attachs where item='.$id.';');
			$cmd->execute();
		}
		// Удалим (если есть) файл-архив с описанием объекта
		$p = $s.'storage/files/zipobj/obj'.$id.'.zip';
		if (file_exists($p))
			@unlink ($p);
		// Удалим (если есть) pdf-файл с описанием объекта
		$p = $s.'storage/files/pdfobj/obj'.$id.'.pdf';
		if (file_exists($p))
			@unlink ($p);
		$item->delete(false);
	}

	#-----------------------------------------------
	# Сохранение заявки из формы поиска
	#-----------------------------------------------

	public function actionOrder_add(){
	// Пересено из OrdersController
	// Добавление параметров поиска в качестве заявки
	// Проверим, не присутствует ли уже точно такой же заявки
		$vals = Yii::app()->user;
		$whereString = ''; // Переменная для формирования WHERE-предложения sql-запроса
		$Params = array(); // Массив параметров, передаваемых в запрос
		$NewRec = new Tenders; // Объявим здесь эту переменную в целях оптимизации кода

		if (isset($vals->glcity)) { // Город
			$whereString = ' and city=:ct';
			$Params[':ct'] = $vals->getState('glcity');
			$NewRec->city = $vals->getState('glcity');
		}
		if (isset($vals->ses_tip_sooruj)) { // Тип сооружения
			$whereString .= ' and kind_structure=:ks';
			$Params[':ks'] = $vals->getState('ses_tip_sooruj');
			$NewRec->kind_structure = $vals->getState('ses_tip_sooruj');
		} else $whereString .= ' and kind_structure=0';
		if (isset($vals->ses_kind)) { // Тип помещения (офис/склад и т.п.
			$whereString .= ' and kind=:kd';
			$Params[':kd'] = $vals->getState('ses_kind');
			$NewRec->kind = $vals->getState('ses_kind');
		} else $whereString .= ' and kind=0';
		if (isset($vals->ses_funkcional)) { // Функциональное назначение
			$whereString .= ' and functionality=:fn';
			$Params[':fn'] = $vals->getState('ses_funkcional');
			$NewRec->functionality = $vals->getState('ses_funkcional');
		} else $whereString .= ' and functionality=0';
		if (isset($vals->ses_klass)) { // Класс (классность) объекта
			$whereString .= ' and class=:cl';
			$Params[':cl'] = $vals->getState('ses_klass');
			$NewRec->class = $vals->getState('ses_klass');
		} else $whereString .= ' and class=0';
		if (isset($vals->ses_raion)) { // Район
			$whereString .= ' and district=:ds';
			$Params[':ds'] = $vals->getState('ses_raion');
			$NewRec->district = $vals->getState('ses_raion');
		} else $whereString .= ' and district=0';
		if (isset($vals->ses_ulica)) { // Улица
			$whereString .= ' and street=:st';
			$Params[':st'] = $vals->getState('ses_ulica');
			$NewRec->street = $vals->getState('ses_ulica');
		} else $whereString .= ' and street=0';
		if (isset($vals->ses_etaj1) && ($vals->getState('ses_etaj1')!='')) { // Этаж. Минимально приемлемый
			$whereString .= ' and storeyMin = :et1';
			$Params[':et1'] = $vals->getState('ses_etaj1');
			$NewRec->storeyMin = $vals->getState('ses_etaj1');
		} else $whereString .= ' and storeyMin is null';
		if (isset($vals->ses_etaj2) && ($vals->getState('ses_etaj2')!='')) { // Этаж. Максимально допустимый
			$whereString .= ' and storeyMax = :et2';
			$Params[':et2'] = $vals->getState('ses_etaj2');
			$NewRec->storeyMax = $vals->getState('ses_etaj2');
		} else $whereString .= ' and storeyMax is null';
		if (isset($vals->ses_gruz_lift)) { // Грузовой лифт
			$whereString .= ' and service_lift=:gl';
			$Params[':gl'] = $vals->getState('ses_gruz_lift');
			$NewRec->service_lift = $vals->getState('ses_gruz_lift');
		} else $whereString .= ' and service_lift is null';
		if (isset($vals->ses_sqr1) && ($vals->getState('ses_sqr1')!='')) { // Площадь помещения. Минимально приемлемая
			$whereString .= ' and squareMin = :sq1';
			$Params[':sq1'] = $vals->getState('ses_sqr1');
			$NewRec->squareMin = $vals->getState('ses_sqr1');
		} else $whereString .= ' and squareMin is null';
		if (isset($vals->ses_sqr2) && ($vals->getState('ses_sqr2')!='')) { // Площадь помещения. Максимально допустимая
			$whereString .= ' and squareMax = :sq2';
			$Params[':sq2'] = $vals->getState('ses_sqr2');
			$NewRec->squareMax = $vals->getState('ses_sqr2');
		} else $whereString .= ' and squareMax is null';
		if (isset($vals->ses_sosremont)) { // Состояние ремонта
			$whereString .= ' and repair=:rp';
			$Params[':rp'] = $vals->getState('ses_sosremont');
			$NewRec->repair = $vals->getState('ses_sosremont');
		} else $whereString .= ' and repair=0';
		if (isset($vals->ses_parkovka)) { // Наличие парковки
			$whereString .= ' and parking=:pk';
			$Params[':pk'] = $vals->getState('ses_parkovka');
			$NewRec->parking = $vals->getState('ses_parkovka');
		} else $whereString .= ' and parking=0';
		if (isset($vals->ses_prc1) && ($vals->getState('ses_prc1')!='')) { // Арендная ставка. Минимально приемлемая
			$whereString .= ' and priceMin = :pr1';
			$Params[':pr1'] = $vals->getState('ses_prc1');
			$NewRec->priceMin = $vals->getState('ses_prc1');
		} else $whereString .= ' and priceMin is null';
		if (isset($vals->ses_prc2) && ($vals->getState('ses_prc2')!='')) { // Арендная ставка. Максимально допустимая
			$whereString .= ' and priceMax = :pr2';
			$Params[':pr2'] = $vals->getState('ses_prc2');
			$NewRec->priceMax = $vals->getState('ses_prc2');
		} else $whereString .= ' and priceMax is null';
		if (isset($vals->ses_est_inet)) { // Наличие Интернет
			$whereString .= ' and internet=:nt';
			$Params[':nt'] = $vals->getState('ses_est_inet');
			$NewRec->internet = $vals->getState('ses_est_inet');
		} else $whereString .= ' and internet is null';
		if (isset($vals->ses_est_telef)) { // Наличие Телефонии
			$whereString .= ' and telephony=:tf';
			$Params[':tf'] = $vals->getState('ses_est_telef');
			$NewRec->telephony = $vals->getState('ses_est_telef');
		} else $whereString .= ' and telephony is null';
		if (isset($vals->ses_est_voda)) { // Наличие отопления и водоснабжения
			$whereString .= ' and heating=:ht';
			$Params[':ht'] = $vals->getState('ses_est_voda');
			$NewRec->heating = $vals->getState('ses_est_voda');
		} else $whereString .= ' and heating is null';
		if (isset($vals->ses_potolok1) && ($vals->getState('ses_potolok1')!='')) { // Высота потолков. Минимально приемлемая
			$whereString .= ' and ceiling_height1 = :ch1';
			$Params[':ch1'] = $vals->getState('ses_potolok1');
			$NewRec->ceiling_height1 = $vals->getState('ses_potolok1');
		} $whereString .= ' and ceiling_height1 is null';
		if (isset($vals->ses_potolok2) && ($vals->getState('ses_potolok2')!='')) { // Высота потолков. Максимально приемлемая
			$whereString .= ' and ceiling_height2 = :ch2';
			$Params[':ch2'] = $vals->getState('ses_potolok2');
			$NewRec->ceiling_height2 = $vals->getState('ses_potolok2');
		} $whereString .= ' and ceiling_height2 is null';
		if (isset($vals->ses_kranb)) { // Кран-балка
			$whereString .= ' and cathead=:chd';
			$Params[':chd'] = $vals->getState('ses_kranb');
			$NewRec->cathead = $vals->getState('ses_kranb');
		} $whereString .= ' and cathead is null';
		if (isset($vals->ses_ventil)) { //  Вентиляция
			$whereString .= ' and ventilation=:vl';
			$Params[':vl'] = $vals->getState('ses_ventil');
			$NewRec->ventilation = $vals->getState('ses_ventil');
		} else $whereString .= ' and ventilation=0';
		if (isset($vals->ses_pozhar)) { //  Пожаротушение
			$whereString .= ' and firefighting=:ff';
			$Params[':ff'] = $vals->getState('ses_pozhar');
			$NewRec->firefighting = $vals->getState('ses_pozhar');
		} else $whereString .= ' and firefighting is null';
		if (isset($vals->ses_vorota1) && ($vals->getState('ses_vorota1')!='')) { // Высота ворот. Минимально приемлемая
			$whereString .= ' and gates_height1 = :gh1';
			$Params[':gh1'] = $vals->getState('ses_vorota1');
			$NewRec->gates_height1 = $vals->getState('ses_vorota1');
		} $whereString .= ' and gates_height1 is null';
		if (isset($vals->ses_vorota2) && ($vals->getState('ses_vorota2')!='')) { // Высота ворот. Максимально приемлемая
			$whereString .= ' and gates_height2 = :gh2';
			$Params[':gh2'] = $vals->getState('ses_vorota2');
			$NewRec->gates_height2 = $vals->getState('ses_vorota2');
		} $whereString .= ' and gates_height2 is null';
		if (isset($vals->ses_pandus)) { //  Пандус
			$whereString .= ' and rampant=:rm';
			$Params[':rm'] = $vals->getState('ses_pandus');
			$NewRec->rampant = $vals->getState('ses_pandus');
		} else $whereString .= ' and rampant is null';
		if (isset($vals->ses_aways)) { //  Автомобильные подъездные пути
			$whereString .= ' and autoways=:aw';
			$Params[':aw'] = $vals->getState('ses_aways');
			$NewRec->autoways = $vals->getState('ses_aways');
		} else $whereString .= ' and autoways is null';
		if (isset($vals->ses_rways)) { //  Железнодорожные подъездные пути
			$whereString .= ' and railways=:rw';
			$Params[':rw'] = $vals->getState('ses_rways');
			$NewRec->railways = $vals->getState('ses_rways');
		} else $whereString .= ' and railways is null';

		$whereString = 'who_id=:wi'.$whereString;
		$Params[':wi'] = $vals->id['user_id'];

		$row = Tenders::model()->find($whereString, $Params);
		if (empty($row)){
			// Если таковой записи не существует, добавим заявку
			$NewRec->who_id = $vals->id['user_id'];
			if ($NewRec->save(true)) {
				$user = User::model()->findByPk($vals->id['user_id']);
				if (!empty($user) && ($user->role=='tenant') && ($user->info_order==1)) 
					$this->SaveForOwners($NewRec->id);
				echo 'Ваша заявка размещена<br /><br />Просмотреть свои заявки, а также удалить устаревшие Вы можете на страницах Личного Кабинета';
			} else
				echo 'Сохранить заявку не удалось<br />Сообщите об ошибке в службу технической поддержки';
		} else {
			// Если такая заявка уже есть, то обновим дату и время занесения
			$row->created = new CDbExpression('NOW()');
			$row->save(false);
			echo 'Заявка с выбранными критериями поиска уже существует<br /><br />Просмотреть свои заявки Вы можете на страницах Личного Кабинета';
		}
	}

	#-----------------------------------------------
	# Работа с Объектами
	#-----------------------------------------------

	public function actionSend_claim(){
		if (!isset($_POST['id']) || !isset($_POST['clm']) || (Yii::app()->user->isGuest) || (Yii::app()->user->id['user_role'] != 'tenant'))
			throw new CHttpException(404);
		$idItem = substr($_POST['id'], 3);
		$claim = $_POST['clm'];
		if (!is_numeric($claim) || ($claim>3) || ($claim<0))	$claim = 0;
		$user = Yii::app()->user->id['user_id'];
		// Проверим, не существует ли жалобы арендатора на этот объект в течение часа
		$exists = Claim::model()->exists('item=:it and tenant=:tn and (sended > DATE_SUB(NOW(), INTERVAL 1 HOUR))', array(':it'=>$idItem, ':tn'=>$user));
		if (!$exists){
			$NewRec = new Claim;
			$NewRec->kind = $claim;
			$NewRec->item = $idItem;
			$NewRec->tenant = $user;
			$NewRec->save(false);
		}
	}

	#-----------------------------------------------
	# AJAX валидация
	#-----------------------------------------------

	# доступность логина
	public function actionIslogin(){
		if (!isset($_POST['val'])) throw new CHttpException(404);
		$name = $_POST['val'];
		if ($name == ''){
			$mess = array('Логин не должен быть пустым', 'err');
			echo json_encode($mess);
			CApplication::end();
		}
		if (!preg_match("/^[a-zA-Z0-9_]+$/", $name)){
			$mess = array('Есть недопустимые символы', 'err');
			echo json_encode($mess);
			CApplication::end();
		}
		if (mb_strlen($name, 'UTF-8') < 5){
			$mess = array('Логин слишком короткий', 'err');
			echo json_encode($mess);
			CApplication::end();
		}
		$User_obj = User::model()->find('username=:varible', array(':varible'=>$name));
		if (empty($User_obj)){
			$mess = array('Логин свободный', 'ok');
			echo json_encode($mess);
		}else{
			$mess = array('Логин занят', 'err');
			echo json_encode($mess);
		}
	}

	// Русские поля
	public function actionRegrusfields(){
		if (!isset($_POST['val']))
			throw new CHttpException(404);
		$val = $_POST['val'];
		$req = ($_POST['req'] == 1);
		if ($req && ($val == '')){
			$mess = array('Поле обязательно для заполнения', 'err');
			echo json_encode($mess);
			CApplication::end();
		} 
		$len = mb_strlen($val, 'UTF-8');
		if ($len > 30){
			$mess = array('Превышена допустимая длина. Выражайтесь короче', 'err');
			echo json_encode($mess);
			CApplication::end();
		}
		if (!preg_match("/^[ёа-я -]+$/ui", $val) && ($req || ($len>0))){
			$mess = array('Есть недопустимые символы', 'err');
			echo json_encode($mess);
			CApplication::end();
		}
		$mess = array('', 'ok');
		echo json_encode($mess);
	}
	// Телефон
	public function actionCheckphone(){
		if (!isset($_POST['val']))
			throw new CHttpException(404);
		$val = preg_replace ('/[^0-9]/ui', '', $_POST['val']);
		if ($val{0}=='7')	$val = substr($val, 1);
		$len = mb_strlen($val, 'UTF-8');		
		$s = '+7 ';
		for ($i=0;$i<$len;$i++){
			if ($i==3)	$s.=' ';
			$s.=$val{$i};
		}
		$mess = array($s, 'ok');
		echo json_encode($mess);
	}

	# доступность ящика
	public function actionIsemail(){
		if (!isset($_POST['val'])) throw new CHttpException(404);
		$email_chek = $_POST['val'];
		if ($email_chek == ''){
			$mess = array('Имя почтового ящика не может быть пустым', 'err');
			echo json_encode($mess);
			CApplication::end();
		}
		if (!preg_match("/^[a-zA-Z0-9_@.-]+$/", $email_chek)){
			$mess = array('Есть недопустимые символы', 'err');
			echo json_encode($mess);
			CApplication::end();
		}
		if (!preg_match("/^[a-z0-9](?:[a-z0-9_\.-]*[a-z0-9])*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])*\.)+[a-z]{2,4}$/i", $email_chek)){
			$mess = array('Не соответствует формату e-mail', 'err');
			echo json_encode($mess);
			CApplication::end();
		}
		$User_obj = User::model()->find('e_mail=:varible', array(':varible'=>$email_chek));
		if (empty($User_obj)){
			$mess = array('E-mail свободный', 'ok');
			echo json_encode($mess);
		}else{
			$mess = array('E-mail занят', 'err');
			echo json_encode($mess);
		}
	}

	# проверка пароля
	public function actionChekpass(){
		if (!isset($_POST['val'])) throw new CHttpException(404);
		$pass_chek = $_POST['val'];
		if ($pass_chek == ''){
			$mess = array('Пароль не может быть пустым', 'err');
			echo json_encode($mess);
			CApplication::end();
		}
		if (!preg_match("/^[a-zA-Z0-9_]+$/", $pass_chek)){
			$mess = array('Есть недопустимые символы', 'err');
			if (preg_match("/[ёа-я -]+/ui", $pass_chek))
				$mess[0].=' (русские буквы)';
			echo json_encode($mess);
			CApplication::end();
		}
		$len = mb_strlen($pass_chek, 'UTF-8');
		if ($len < 5){
			$mess = array('Слишком короткий пароль', 'err');
			echo json_encode($mess);
			CApplication::end();
		}
		if ($len > 10){
			$mess = array('Такой длинный пароль скоро забудете!', 'err');
			echo json_encode($mess);
			CApplication::end();
		}
		$mess = array('Пароль подходит', 'ok');
		echo json_encode($mess);
	}

	# равенство 2х строк
	public function actionConfirmpass(){
		if (!isset($_POST['confirm']) || !isset($_POST['pass']))
			throw new CHttpException(404);
		$password = $_POST['pass'];
		if ($password == ''){
			$mess = array('Сначала заполните поле "Пароль"', 'err');
			echo json_encode($mess);
			CApplication::end();
		}
		if ($_POST['confirm'] == $password){
			$mess = array('Пароли совпадают', 'ok');
			echo json_encode($mess);
		}else{
			$mess = array('Пароли не совпадают', 'err');
			echo json_encode($mess);
		}
	}

	# доступность имени статических страниц
	public function actionIsname(){

		if (!isset($_POST['name']))
			throw new CHttpException(404);

		$name_chek = $_POST['name'];

		if ($name_chek == ''){
			$mess = array('имя не должно быть пустым', 'err');
			echo json_encode($mess);
			CApplication::end();
		}

		if (!preg_match("/^[a-zA-Z0-9_]+$/", $name_chek)){
			$mess = array('есть недопустимые символы', 'err');
			echo json_encode($mess);
			CApplication::end();
		}

		$Static_obj = Statics::model()->find('name=:varible', array(':varible'=>$name_chek));
		if (empty($Static_obj)){
			$mess = array('имя свободное', 'ok');
			echo json_encode($mess);
		}else{
			$mess = array('имя занято', 'err');
			echo json_encode($mess);
		}

	}
	
	// Сохранение сопоставления новодобавленной заявки с имеющимися объектами
	function SaveForOwners($bidId) {
		$vals = Yii::app()->user;
		$whereString = 'I.itemType<>1 and I.status=6'; // Переменная для формирования WHERE-предложения sql-запроса
		$Params = array(); // Массив параметров, передаваемых в запрос

		if (isset($vals->glcity)) { // Город
			$whereString .= ' and I.city=:ct';
			$Params[':ct'] = $vals->getState('glcity');
		}
		if (isset($vals->ses_tip_sooruj)) { // Тип сооружения
			$whereString .= ' and I.kind_structure=:ks';
			$Params[':ks'] = $vals->getState('ses_tip_sooruj');
		}
		if (isset($vals->ses_kind)) { // Тип помещения (офис/склад и т.п.
			$whereString .= ' and I.kind=:kd';
			$Params[':kd'] = $vals->getState('ses_kind');
		}
		if (isset($vals->ses_funkcional)) { // Функциональное назначение
			$whereString .= ' and I.functionality=:fn';
			$Params[':fn'] = $vals->getState('ses_funkcional');
		}
		if (isset($vals->ses_klass)) { // Класс (классность) объекта
			$whereString .= ' and I.class=:cl';
			$Params[':cl'] = $vals->getState('ses_klass');
		}
		if (isset($vals->ses_raion)) { // Район
			$whereString .= ' and I.district=:ds';
			$Params[':ds'] = $vals->getState('ses_raion');
		}
		if (isset($vals->ses_ulica)) { // Улица
			$whereString .= ' and I.street=:st';
			$Params[':st'] = $vals->getState('ses_ulica');
		}
		if (isset($vals->ses_etaj1) && ($vals->getState('ses_etaj1')!='')) { // Этаж. Минимально приемлемый
			$whereString .= ' and I.storey >= :et1';
			$Params[':et1'] = $vals->getState('ses_etaj1');
		}
		if (isset($vals->ses_etaj2) && ($vals->getState('ses_etaj2')!='')) { // Этаж. Максимально допустимый
			$whereString .= ' and I.storey <= :et2';
			$Params[':et2'] = $vals->getState('ses_etaj2');
		}
		if (isset($vals->ses_gruz_lift)) { // Грузовой лифт
			$whereString .= ' and I.service_lift=:gl';
			$Params[':gl'] = $vals->getState('ses_gruz_lift');
		}// else $whereString .= ' and I.service_lift=0';
		if (isset($vals->ses_sqr1) && ($vals->getState('ses_sqr1')!='')) { // Площадь помещения. Минимально приемлемая
			$whereString .= ' and I.total_item_sqr >= :sq1';
			$Params[':sq1'] = $vals->getState('ses_sqr1');
		}
		if (isset($vals->ses_sqr2) && ($vals->getState('ses_sqr2')!='')) { // Площадь помещения. Максимально допустимая
			$whereString .= ' and I.total_item_sqr <= :sq2';
			$Params[':sq2'] = $vals->getState('ses_sqr2');
		}
		if (isset($vals->ses_sosremont)) { // Состояние ремонта
			$whereString .= ' and I.repair=:rp';
			$Params[':rp'] = $vals->getState('ses_sosremont');
		}
		if (isset($vals->ses_parkovka)) { // Наличие парковки
			$whereString .= ' and I.parking=:pk';
			$Params[':pk'] = $vals->getState('ses_parkovka');
		}
		if (isset($vals->ses_prc1) && ($vals->getState('ses_prc1')!='')) { // Арендная ставка. Минимально приемлемая
			$whereString .= ' and I.price >= :pr1';
			$Params[':pr1'] = $vals->getState('ses_prc1');
		}
		if (isset($vals->ses_prc2) && ($vals->getState('ses_prc2')!='')) { // Арендная ставка. Максимально допустимая
			$whereString .= ' and I.price <= :pr2';
			$Params[':pr2'] = $vals->getState('ses_prc2');
		}
		if (isset($vals->ses_est_inet)) { // Наличие Интернет
			$whereString .= ' and I.internet=:nt';
			$Params[':nt'] = $vals->getState('ses_est_inet');
		}// else $whereString .= ' and I.internet=0';
		if (isset($vals->ses_est_telef)) { // Наличие Телефонии
			$whereString .= ' and I.telephony=:tf';
			$Params[':tf'] = $vals->getState('ses_est_telef');
		}// else $whereString .= ' and I.telephony=0';
		if (isset($vals->ses_est_voda)) { // Наличие отопления и водоснабжения
			$whereString .= ' and I.heating=:ht';
			$Params[':ht'] = $vals->getState('ses_est_voda');
		}// else $whereString .= ' and I.heating=0';
		if (isset($vals->ses_potolok1) && ($vals->getState('ses_potolok1')!='')) { // Высота потолков. Минимально приемлемая
			$whereString .= ' and ceiling_height1 >= :ch1';
			$Params[':ch1'] = $vals->getState('ses_potolok1');
		}
		if (isset($vals->ses_potolok2) && ($vals->getState('ses_potolok2')!='')) { // Высота потолков. Максимально приемлемая
			$whereString .= ' and ceiling_height2 <= :ch2';
			$Params[':ch2'] = $vals->getState('ses_potolok2');
		}
		if (isset($vals->ses_kranb)) { // Кран-балка
			$whereString .= ' and I.cathead=:chd';
			$Params[':chd'] = $vals->getState('ses_kranb');
		}// else $whereString .= ' and I.cathead=0';
		if (isset($vals->ses_ventil)) { //  Вентиляция
			$whereString .= ' and I.ventilation=:vl';
			$Params[':vl'] = $vals->getState('ses_ventil');
		}
		if (isset($vals->ses_pozhar)) { //  Пожаротушение
			$whereString .= ' and I.firefighting=:ff';
			$Params[':ff'] = $vals->getState('ses_pozhar');
		}// else $whereString .= ' and I.firefighting=0';
		if (isset($vals->ses_vorota1) && ($vals->getState('ses_vorota1')!='')) { // Высота ворот. Минимально приемлемая
			$whereString .= ' and gates_height1 >= :gh1';
			$Params[':gh1'] = $vals->getState('ses_vorota1');
		}
		if (isset($vals->ses_vorota2) && ($vals->getState('ses_vorota2')!='')) { // Высота ворот. Максимально приемлемая
			$whereString .= ' and gates_height2 <= :gh2';
			$Params[':gh2'] = $vals->getState('ses_vorota2');
		}
		if (isset($vals->ses_pandus)) { //  Пандус
			$whereString .= ' and I.rampant=:rm';
			$Params[':rm'] = $vals->getState('ses_pandus');
		}// else $whereString .= ' and I.rampant=0';
		if (isset($vals->ses_aways)) { //  Автомобильные подъездные пути
			$whereString .= ' and I.autoways=:aw';
			$Params[':aw'] = $vals->getState('ses_aways');
		}// else $whereString .= ' and I.autoways=0';
		if (isset($vals->ses_rways)) { //  Железнодорожные подъездные пути
			$whereString .= ' and I.railways=:rw';
			$Params[':rw'] = $vals->getState('ses_rways');
		}// else $whereString .= ' and I.railways=0';

		$criteria = new CDbCriteria;
		$criteria->alias = 'I';
		$criteria->select = 'distinct U.id as "ownerId"';
		$criteria->join = 'join users U on U.id=I.owner_id';
		$criteria->condition = $whereString;
		$criteria->params = $Params;

		$Items = Search::model()->findAll($criteria);
		if (!empty($Items)) {
			require_once('protected/extensions/mailer/class.mailer.php'); // Чтобы не оказалось подключение файла в цикле. Нужно для виджета InformAboutTenders
			require_once('protected/extensions/smspilot/smspilot.php'); 
			$s = 'Insert into bid_owners (tender, owner) values ';
			foreach ($Items as $it) {
				$s .= '('.$bidId.', '.$it->ownerId.'),';
				$this->widget('InformAboutTender', array('bidId'=>$bidId, 'ownId'=>$it->ownerId));
			}
			$cmd = Yii::app()->db->createCommand(substr($s,0,-1).';');
			$cmd->execute();
		}
	}

	public function actionPerson_sendmail(){
		if (!isset($_POST['id']) || !isset($_POST['msg']))
			throw new CHttpException(404);

		$user = User::model()->findByPK($_POST['id']);
		if (!empty($user)) {
			if (empty($user->info_email))
					$mailAddr = $user->e_mail;
			else	$mailAddr = $user->info_email;
			@require_once('protected/extensions/mailer/class.mailer.php');
			$mail = new Mailer();
			$mail->IsHTML(true);
			$mail->From = Yii::app()->params->admin_mail;
			$mail->FromName = 'Портал Com-Arenda.RU';
			$mail->Subject  = 'Письмо со страницы сайта';
			$mail->Body = 'Посетитель сайта <a href="'.Yii::app()->params->site_base.'person/card/id/'.$user->id.'.html">со страницы</a> направил в Ваш адрес сообщение:<br /><br />'.$_POST['msg'].'<br /><hr><br /><span style="font-size:75%">Сообщение могут отправляться неавторизованными посетителями и направляются адресату "как есть". За содержание отправлений администрация сайта ответственности не несет.</span>'.Yii::app()->params->autoscriptum;
			//$mail->AddAddress($mailAddr);
			$mail->AddAddress('dpriest@list.ru'); 
			$mail->Send();
		}
	}

	#-----------------------------------------------
	# AJAX загрузка фотографии
	#-----------------------------------------------
	
	public function actionU_photo_load(){

		ini_set('memory_limit', '128M');
		if (filesize($_FILES['myfile']['tmp_name']) < (1024*1024*6)){ // Ограничение размера фотки в 6 мегабайт
		
			$uploaddir = 'storage/images/personal/temp/';
		
			$rand_part = date('His').rand(1000, 9999);
			$translit_obj = $this->widget('LoadFileName', array('in_text'=>$_FILES['myfile']['name'], 'len'=>100));
			$translit = $translit_obj->out_text;
			$name = substr($translit, 0, strrpos($translit, '.'));
			$ext  = substr($translit, strrpos($translit, '.'));
			$uploadfile = $uploaddir.basename($name.'_'.$rand_part.$ext);
			
			if (move_uploaded_file($_FILES['myfile']['tmp_name'], $uploadfile)){
				$infoIMG = getimagesize($uploadfile);
				$width_out = $infoIMG[0];
				if (($width_out>1024) || ($infoIMG[1]>900)){
					require_once 'protected/extensions/watermark/WideImage.php';
					if ($width_out>1024) $width_out = 1024;
					$qualityIMG = 90;
					if ($infoIMG['mime'] == 'image/png') $qualityIMG = 0.9;
					WideImage::load($uploadfile)->resizeDown(1024, 900, 'inside')->saveToFile($uploadfile, $qualityIMG);
				}
				echo $name.'_'.$rand_part.$ext.'|'.$width_out;
			}
		} 
		else echo false;	
	}

	#-----------------------------------------------
	# AJAX croping
	#-----------------------------------------------
	
	public function actionU_photo_crop(){
	
		ini_set('memory_limit', '128M');
		if (!isset($_POST['file']) || empty($_POST['file']) || !isset($_POST['x1']) || !isset($_POST['y1']) || !isset($_POST['x2']) || !isset($_POST['y2']) || 
			!isset(Yii::app()->user->id['user_id']) || empty(Yii::app()->user->id['user_id']))
		CApplication::end();
		$x1 = $_POST['x1'];
		$y1 = $_POST['y1'];
		$x2 = $_POST['x2'];
		$y2 = $_POST['y2'];
		if (is_numeric($x1) && is_numeric($y1) && is_numeric($x2) && is_numeric($y2)) {
			$x1 = (int)ceil($x1);
			$y1 = (int)ceil($y1);
			$x2 = (int)ceil($x2);
			$y2 = (int)ceil($y2);
			$x1 = abs($x1);
			$y1 = abs($y1);
			$x2 = abs($x2);
			$y2 = abs($y2);
		} else CApplication::end();

		$file = $_POST['file'];
		$file_path = 'storage/images/personal/temp/'.$file;

		if (file_exists($file_path) && realpath($file_path)){
			require_once 'protected/extensions/watermark/WideImage.php';

			$width = abs($x2 - $x1);
			$height = abs($y2 - $y1);

			$infoIMG = getimagesize($file_path);
			$qualityIMG = 90;
			if ($infoIMG['mime'] == 'image/png') $qualityIMG = 0.9;
			WideImage::load($file_path)->crop($x1, $y1, $width, $height)->resize(308, 231)->saveToFile('storage/images/personal/'.$file, $qualityIMG);

			@unlink ($file_path);

			$criteria = new CDbCriteria;
            $criteria->condition = 'id=:var';
            $criteria->params = array(':var'=>Yii::app()->user->id['user_id']);
            $Users_obj = User::model()->find($criteria);
			if (!empty($Users_obj)){
				if ($Users_obj->photo != '') @unlink ('storage/images/personal/'.$Users_obj->photo);
				$Users_obj->photo = $file;
				$Users_obj->save(false);
			}
		}
	}

	#-----------------------------------------------
	# AJAX удаление фотографии
	#-----------------------------------------------
	
	public function actionU_photo_delete(){
	
		if (!isset($_POST['file']) || empty($_POST['file']) ||
			!isset($_POST['type']) || empty($_POST['type']) ||
			!isset(Yii::app()->user->id['user_id']) || empty(Yii::app()->user->id['user_id']))
		CApplication::end();
		
		$type = $_POST['type'];
		
		if ($type == 'user') $basedir = 'storage/images/personal';
		elseif ($type == 'temp') $basedir = 'storage/images/personal/temp';
			else CApplication::end();
		
		$file_to_delete = $_POST['file'];
		$path = realpath($basedir.'/'.$file_to_delete);
		if ($path){
			$ext_allowed = array('jpg','png','jpeg','gif');
			$ext = mb_strtolower(substr(strrchr($file_to_delete, '.'), 1), 'UTF-8');
			if (!in_array($ext, $ext_allowed))
				CApplication::end();
			@unlink($path);
			if ($type == 'user'){
				$criteria = new CDbCriteria;
				$criteria->condition = 'id=:var';
				$criteria->params = array(':var'=>Yii::app()->user->id['user_id']);
				$Users_obj = User::model()->find($criteria);
				if (!empty($Users_obj)){
					$Users_obj->photo = null;
					$Users_obj->save(false);
				}
			}
		}
	}

	
}?>