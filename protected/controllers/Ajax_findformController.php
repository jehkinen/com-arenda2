<?php class Ajax_findformController extends CController{

	public function actionLoad() {
		if (!isset($_POST['type']))
			throw new CHttpException(404);

		$bUrl = Yii::app()->baseUrl;
		$vals = Yii::app()->user;
		$tab = $_POST['type'];

		ini_set('memory_limit', '64M');
		/* Получаем значения для фильтра  Улица  (Зависит от выбранного города)   */
		$criteria = new CDbCriteria;
		$criteria->order = 'name';
		$criteria->condition = 'city=:ct';
		$criteria->params = array(':ct'=>$vals->getState('glcity', ''));
		/* Получаем значения для фильтра  Улица */
		$list_streets = CHtml::listData( Street::model()->findAll($criteria), 'id', 'name');
		array_unshift($list_streets, 'Любая');
		/* Получаем значения для фильтра  Класс */
		$list_classes = CHtml::listData( Classes::model()->findAll(), 'id', 'shname');
		array_unshift($list_classes, 'Любой');
		/* Получаем значения для фильтра  Состояние ремонта */
		$list_repair = CHtml::listData( Repair::model()->findAll(), 'id', 'shname');
		array_unshift($list_repair, 'Любое');
		/* Получаем значения для фильтра  Функциональное назначение */
		$list_functionality = CHtml::listData( Functionality::model()->findAll(), 'id', 'shname');
		array_unshift($list_functionality, 'Любое');
		/* Получаем значения для фильтра Тип сооружения */
		$list_kindStructure = CHtml::listData( KindStructure::model()->findAll(), 'id', 'shname');
		array_unshift($list_kindStructure, 'Не важен');
		
		// В зависимости от выбранной вкладки обнулим лишние поля
		if ($tab=='tab1') { // ОФИС
			unset ($vals->ses_gruz_lift);
			unset ($vals->ses_est_voda);
			unset ($vals->ses_potolok1);
			unset ($vals->ses_potolok2);
			unset ($vals->ses_kranb);
			unset ($vals->ses_ventil);
			unset ($vals->ses_vorota1);
			unset ($vals->ses_vorota2);
			unset ($vals->ses_pandus);
			unset ($vals->ses_aways);
			unset ($vals->ses_rways);
		}
		elseif ($tab=='tab2') { // СКЛАД
			unset ($vals->ses_klass);
			unset ($vals->ses_etaj1);
			unset ($vals->ses_etaj2);
			unset ($vals->ses_sosremont);
			unset ($vals->ses_parkovka);
			unset ($vals->ses_est_inet);
			unset ($vals->ses_est_telef);
			unset ($vals->ses_ventil);
		}
		elseif ($tab=='tab3') { // Торговое
			unset ($vals->ses_gruz_lift);
			unset ($vals->ses_est_voda);
			unset ($vals->ses_kranb);
			unset ($vals->ses_vorota1);
			unset ($vals->ses_vorota2);
			unset ($vals->ses_aways);
			unset ($vals->ses_rways);
		}
		// Получим значения сессий для установки значений фильтров
		$vtip_sooruj = $vals->getState('ses_tip_sooruj', 0);
		$vfunkcional = $vals->getState('ses_funkcional', 0);
		$vklass		= $vals->getState('ses_klass', 0);
		$vulica 	= $vals->getState('ses_ulica', 0);
		$vetaj1 	= $vals->getState('ses_etaj1', '');
		$vetaj2 	= $vals->getState('ses_etaj2', '');
		$vgruz_lift = $vals->getState('ses_gruz_lift', 0);
		$vsosremont = $vals->getState('ses_sosremont', 0);
		$vparkovka 	= $vals->getState('ses_parkovka', 0);
		$vest_inet 	= $vals->getState('ses_est_inet', 0);
		$vest_telef = $vals->getState('ses_est_telef', 0);
		$vest_voda 	= $vals->getState('ses_est_voda', 0);
		$vpotolok1 	= $vals->getState('ses_potolok1', '');
		$vpotolok2 	= $vals->getState('ses_potolok2', '');
		$vkranb 	= $vals->getState('ses_kranb', 0);
		$vventil 	= $vals->getState('ses_ventil', 0);
		$vpozhar 	= $vals->getState('ses_pozhar', 0);
		$vvorota1 	= $vals->getState('ses_vorota1', '');
		$vvorota2 	= $vals->getState('ses_vorota2', '');
		$vpandus 	= $vals->getState('ses_pandus', 0);
		$vaways 	= $vals->getState('ses_aways', 0);
		$vrways 	= $vals->getState('ses_rways', 0);


		
		// СКЛАД
		if ($tab=='tab2') {
			$res = '<div class="data_box_filter">
					<label>Улица <div class="rounded2">
						<select name="ulica">';
				foreach ($list_streets as $key=>$val) {
					$res .= '<option value="'.$key;
					if ($key==$vulica)	$res .= '" selected="selected';
					$res .= '">'.$val.'</option>';
				} $res .='
						</select>
					</div></label>
				</div>
				<div class="data_box_filter">
					<label>Функц. назначение <div class="rounded2">
						<select name="funkcional">';
				foreach ($list_functionality as $key=>$val) {
					$res .= '<option value="'.$key;
					if ($key==$vfunkcional)	$res .= '" selected="selected';
					$res .= '">'.$val.'</option>';
				} $res .='
						</select>
					</div></label>
				</div>';
		} 
		// ОФИС + ТОРГОВОЕ
		else {
			$res = '<div class="data_box_filter">
					<label>Улица <div class="rounded2">
						<select name="ulica">';
				foreach ($list_streets as $key=>$val) {
					$res .= '<option value="'.$key;
					if ($key==$vulica)	$res .= '" selected="selected';
					$res .= '">'.$val.'</option>';
				} $res .='
						</select>
					</div></label>
				</div>
				<div class="data_box_filter">
					<label>Класс <div class="rounded2">
						<select name="klass">';
				foreach ($list_classes as $key=>$val) {
					$res .= '<option value="'.$key;
					if ($key==$vklass)	$res .= '" selected="selected';
					$res .= '">'.$val.'</option>';
				} $res .='
						</select>
					</div></label>
				</div>
				<div class="data_box_filter">Этажи от 
					<div class="rounded2"><input type="text" class="number" name="etaj1" value="'.$vetaj1.'" /></div> до <div class="rounded2"><input type="text" class="number" name="etaj2" value="'.$vetaj2.'" /></div>
				</div>
				<div class="data_box_filter">
					<label>Ремонт <div class="rounded2">
						<select name="sosremont">';
				foreach ($list_repair as $key=>$val) {
					$res .= '<option value="'.$key;
					if ($key==$vsosremont)	$res .= '" selected="selected';
					$res .= '">'.$val.'</option>';
				} $res .='
						</select>
					</div></label>
				</div>				
				<div class="data_box_filter">
					<label>Функц. назначение <div class="rounded2">
						<select name="funkcional">';
				foreach ($list_functionality as $key=>$val) {
					$res .= '<option value="'.$key;
					if ($key==$vfunkcional)	$res .= '" selected="selected';
					$res .= '">'.$val.'</option>';
				} $res .='
						</select>
					</div></label>
				</div>';
		}

		// ОФИС
		if ($tab=='tab1') {
			/* Получаем значения для фильтра  Наличие Парковки */
			$list_parking = CHtml::listData( Parking::model()->findAll(), 'id', 'shname');
			array_unshift($list_parking, 'Не важно');
			$res .= '<div class="data_box_filter">
					<label>Парковка <div class="rounded2">
						<select name="parkovka">';
				foreach ($list_parking as $key=>$val) {
					$res .= '<option value="'.$key;
					if ($key==$vparkovka)	$res .= '" selected="selected';
					$res .= '">'.$val.'</option>';
				} $res .='
						</select>
					</div></label>
				</div>
				<div class="data_box_filter">
					<label>Тип сооружения <div class="rounded2">
						<select name="tip_sooruj">';
				foreach ($list_kindStructure as $key=>$val) {
					$res .= '<option value="'.$key;
					if ($key==$vtip_sooruj)	$res .= '" selected="selected';
					$res .= '">'.$val.'</option>';
				} $res .='
						</select>
					</div></label>
				</div>
				<div class="data_box_filter">
					<label class="checkbox"><input type="checkbox" name="est_telef"'.($vest_telef==1?' checked="checked"':'').' /> Телефония</label>
					<label class="checkbox"><input type="checkbox" name="est_inet"'.($vest_inet==1?' checked="checked"':'').' /> Интернет</label>
					<label class="checkbox"><input type="checkbox" name="pozhar"'.($vpozhar==1?' checked="checked"':'').' /> Система пожаротушения</label>
					<input type="hidden" name="kind" value="0" />
				</div>';
		}
		// СКЛАД
		elseif ($tab=='tab2') {
			$res .= '<div class="data_box_filter">Высота потолков от 
					<div class="rounded2"><input type="text" class="number" name="potolok1" value="'.$vpotolok1.'" /></div> до <div class="rounded2"><input type="text" class="number" name="potolok2" value="'.$vpotolok2.'" /></div>
				</div>
				<div class="data_box_filter">Высота ворот от 
					<div class="rounded2"><input type="text" class="number" name="vorota1" value="'.$vvorota1.'" /></div> до <div class="rounded2"><input type="text" class="number" name="vorota2" value="'.$vvorota2.'" /></div>
				</div>
				<div class="data_box_filter">
					<label>Тип сооружения <div class="rounded2">
						<select name="tip_sooruj">';
				foreach ($list_kindStructure as $key=>$val) {
					$res .= '<option value="'.$key;
					if ($key==$vtip_sooruj)	$res .= '" selected="selected';
					$res .= '">'.$val.'</option>';
				} $res .='
						</select>
					</div></label>
				</div>
				<div class="data_box_filter">
					<label class="checkbox"><input type="checkbox" name="est_voda"'.($vest_voda==1?' checked="checked"':'').' /> Система отопления и водоснабжения</label>
					<label class="checkbox"><input type="checkbox" name="pozhar"'.($vpozhar==1?' checked="checked"':'').' /> Система пожаротушения</label>
					<label class="checkbox"><input type="checkbox" name="pandus"'.($vpandus==1?' checked="checked"':'').' /> Пандус</label>
					<label class="checkbox"><input type="checkbox" name="kranb"'.($vkranb==1?' checked="checked"':'').' /> Кран-балка</label>
					<label class="checkbox"><input type="checkbox" name="gruz_lift"'.($vgruz_lift==1?' checked="checked"':'').' /> Грузовой лифт</label>
				</div>
				<div class="data_box_filter">Подъездные пути:
					<label><input type="checkbox" name="autoways"'.($vaways==1?' checked="checked"':'').' /> Авто</label>
					<label class="checkbox"><input type="checkbox" name="railways"'.($vrways==1?' checked="checked"':'').' /> Железно-дорожные</label>
					<input type="hidden" name="kind" value="2" />
				</div>';
		}
		// ТОРГОВОЕ
		elseif ($tab=='tab3') {
			/* Получаем значения для фильтра  Наличие Парковки */
			$list_parking = CHtml::listData( Parking::model()->findAll(), 'id', 'shname');
			array_unshift($list_parking, 'Не важно');
			/* Получаем значения для фильтра  Системы вентиляции */
			$list_ventil = CHtml::listData( Ventilation::model()->findAll(), 'id', 'shname');
			array_unshift($list_ventil, 'Не важно');
			$res .= '<div class="data_box_filter">
					<label>Парковка <div class="rounded2">
						<select name="parkovka">';
				foreach ($list_parking as $key=>$val) {
					$res .= '<option value="'.$key;
					if ($key==$vparkovka)	$res .= '" selected="selected';
					$res .= '">'.$val.'</option>';
				} $res .='
						</select>
					</div></label>
				</div>
				<div class="data_box_filter">
					<label>Вентиляция <div class="rounded2">
						<select name="ventil">';
				foreach ($list_ventil as $key=>$val) {
					$res .= '<option value="'.$key;
					if ($key==$vventil)	$res .= '" selected="selected';
					$res .= '">'.$val.'</option>';
				} $res .='
						</select>
					</div></label>
				</div>
				<div class="data_box_filter">Высота потолков от 
					<div class="rounded2"><input type="text" class="number" name="potolok1" value="'.$vpotolok1.'" /></div> до <div class="rounded2"><input type="text" class="number" name="potolok2" value="'.$vpotolok2.'" /></div>
				</div>
				<div class="data_box_filter">
					<label>Тип сооружения <div class="rounded2">
						<select name="tip_sooruj">';
				foreach ($list_kindStructure as $key=>$val) {
					$res .= '<option value="'.$key;
					if ($key==$vtip_sooruj)	$res .= '" selected="selected';
					$res .= '">'.$val.'</option>';
				} $res .='
						</select>
					</div></label>
				</div>
				<div class="data_box_filter">
					<label class="checkbox"><input type="checkbox" name="est_telef"'.($vest_telef==1?' checked="checked"':'').' /> Телефония</label>
					<label class="checkbox"><input type="checkbox" name="est_inet"'.($vest_inet==1?' checked="checked"':'').' /> Интернет</label>
					<label class="checkbox"><input type="checkbox" name="pozhar"'.($vpozhar==1?' checked="checked"':'').' /> Система пожаротушения</label>
					<label class="checkbox"><input type="checkbox" name="pandus"'.($vpandus==1?' checked="checked"':'').' /> Пандус</label>
					<input type="hidden" name="kind" value="1" />
				</div>';
		} else $res = '';
		echo $res;
	}

}?>