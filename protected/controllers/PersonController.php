<?php
class PersonController extends CController{
	public $defaultAction = 'card';
	public $layout = 'kabinets02';
	public $pageClass = 'any';
	public $cssFiles = array();
	public $jsFiles = array();
	public $pageDescription;
	public $pageKeywords;

	public function actionCard() {
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$user = User::model()->findByPK($get->id_out);
		if (empty($user))
			throw new CHttpException(404);

		$this->cssFiles[] = 'modal.css';
		$this->jsFiles[] = 'jquery.simplemodal.js';
		$this->jsFiles[] = 'perscard.js';
		$this->pageTitle = 'Карточка пользователя';
		$this->pageDescription = '';
		$this->pageKeywords = '';
		if (empty($user->parag1_name) && empty($user->parag2_name))
				$this->render('card', array('user'	=>$user));
		else		$this->render('card-with-abouts', array('user'	=>$user));
	}

	public function actionObjects() {
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$user = User::model()->findByPK($get->id_out);
		if (empty($user) || $user->role!='owner')
			throw new CHttpException(404);

		$vals = Yii::app()->user;
		$isTenant = (!$vals->isGuest && $vals->id['user_role'] == 'tenant');
		if ($isTenant)
			$toNotepad = ', (Select COALESCE(N.id, 0) from notepad N where N.item=O.id and N.tenant='.$vals->id['user_id'].') as "inNotepad"';
		else $toNotepad = '';
		$criteria = new CDbCriteria;
		$criteria->alias = 'O';
		$criteria->select = 'O.id, O.special, O.city, D.name as "district", S.name as "street", O.house, O.created, O.name, O.total_item_sqr, O.price, (Select F.name_small from images F where F.id_item=O.id order by F.id asc limit 1) as "hasFoto"'.$toNotepad;
		$criteria->join = 'join districts D on D.id=O.district join streets S on S.id=O.street';
		$criteria->condition = 'owner_id=:id_user and O.status=6 and O.itemType<>1';
		$criteria->params = array(':id_user'=>$user->id);
		$criteria->order = 'O.created desc';
		$criteris->together = false;

		$qty = Search::model()->count($criteria);
		/* Постраничная разбивка */
		$pages = new CPagination($qty);
		$pages->pageSize = '25';
		$pages->applyLimit($criteria);

		$Rows = Search::model()->findAll($criteria);
		
		$this->pageTitle = 'Карточка пользователя';
		$this->pageDescription = '';
		$this->pageKeywords = '';
		$this->render('objects', array(
			'user'			=>$user, 
			'items'			=>$Rows,
			'qty'			=>$count_of_search,
			'pages'			=>$pages,
			'userTenant'	=>$isTenant // Показывать ли ссылку Добавить в блокнот
		));
	}
}
?>