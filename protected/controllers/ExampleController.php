<?php
  class ExampleController extends CController{
    public $defaultAction = 'view';
	public $pageDescription;
	public $pageKeywords;

    public function actionView(){
      $form = new Search_form();

      $criteria = new CDbCriteria;
	  $criteria->select = 'text_page, title, description, keywords';
	  $criteria->condition = 'name=:name_page';
	  $criteria->params = array(':name_page'=>'index');
	  $Static_page = Statics::model()->find($criteria);
      $this->pageTitle = '';
	  $this->pageDescription = '';
	  $this->pageKeywords = '';
      $this->render('view', array(
                                  'form' =>$form,
                                  'text' =>$Static_page->text_page,
                                  ));
    }
  }
?>