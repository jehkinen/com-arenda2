<?php
class AdminitemsController extends CController{
	public $defaultAction = 'view';
	public $layout = 'control';
	public $pageDescription;
	public $pageKeywords;

	public function actionView(){
		$this->pageTitle = 'Управление объектами';
		$this->pageDescription = '';
		$this->pageKeywords = '';
		$usr = Yii::app()->user;
		$form = new AdminItems_form();
		if (isset($_POST['AdminItems_form']) || (count($_POST)>0)){
			$form->attributes = $_POST['AdminItems_form'];
			if ($form->validate()){
				if ((isset($_POST['AdminItems_form']['city'])) && (!is_numeric($_POST['AdminItems_form']['city'])))
					$usr->setState('aicity', $_POST['AdminItems_form']['city']);
				elseif (isset($usr->aicity))	unset ($usr->aicity);
				if ((isset($_POST['AdminItems_form']['owner'])) && ($_POST['AdminItems_form']['owner']!=0)) // Владелей объекта
					$usr->setState('aiowner', $_POST['AdminItems_form']['owner']);
				elseif (isset($usr->aiowner))	unset ($usr->aiowner);
				if ((isset($_POST['AdminItems_form']['status'])) && ($_POST['AdminItems_form']['status']!=90)) // Статус объекта
					$usr->setState('aistatus', $_POST['AdminItems_form']['status']);
				elseif (isset($usr->aistatus))	unset ($usr->aistatus);
				if ((isset($_POST['AdminItems_form']['kind'])) && ($_POST['AdminItems_form']['kind']!=90)) // Тип помещения
					$usr->setState('aikind', $_POST['AdminItems_form']['kind']);
				elseif (isset($usr->aikind))	unset ($usr->aikind);
			}
		}

		$whereString = ''; // Переменная для формирования WHERE-предложения sql-запроса
		$Params = array(); // Массив параметров, передаваемых в запрос
		if (isset($usr->aicity)) {
			$whereString .= ' and I.city=:ct';
			$Params[':ct'] = $usr->getState('aicity', '');
		}
		if (isset($usr->aikind)) {
			$whereString .= ' and I.kind=:kd';
			$Params[':kd'] = $usr->getState('aikind', 0);
		}
		if (isset($usr->aistatus)) {
			$whereString .= ' and I.status=:st';
			$Params[':st'] = $usr->getState('aistatus', 0);
		}
		if (isset($usr->aiowner)) {
			$whereString .= ' and I.owner_id=:wn';
			$Params[':wn'] = $usr->getState('aiowner', 0);
		}
		if ($whereString!='')	$whereString = substr($whereString, 5);

        # Определяем сортировку для запроса, если нет сортировки то сортируем по id обекта
        $order = '';
		if (isset($_GET['sort'])){		  $order = $_GET['sort'];
		  if (isset($_GET['type'])) $order .= ' desc';		}
		if (empty($order)) $order = 'id';

		$criteria = new CDbCriteria;
		$criteria->alias = 'I';
		$criteria->select = 'I.id, I.created, I.status, I.itemType, I.special, I.parent, (Select COALESCE(MAX(C.id), 0) from items C where C.parent=I.id) as "hasChild", I.kind, I.city, D.name as "district", S.name as "street", COALESCE(I.house, "?") as house, COALESCE(I.housing, "?") as housing, I.owner_id, U.username as "login", U.so_name as "lastname", U.name as "firstname", (Select COUNT(*) from item_show_stat SS where SS.item=I.id) as "showQty"';
		$criteria->join = 'left join streets S on S.id=I.street left join districts D on D.id=I.district left join users U on U.id=I.owner_id';
		$criteria->condition = $whereString;
		$criteria->params = $Params;
		$criteria->order = $order;

		$qty = Search::model()->count($criteria);
		# Постраничная разбивка
		$pages = new CPagination($qty);
		$pages->pageSize = 10;
		$pages->applyLimit($criteria);
		# Получаем текущую страницу (нумерация начинается с 0, поэтому +1)
		$Cpage = $pages->getCurrentPage()+1;

		$Items = Search::model()->findAll($criteria);

		$criteria = new CDbCriteria;
		$criteria->order = 'name';
		$arCities = CHtml::listData( City::model()->findAll($criteria), 'name', 'name');
		array_unshift($arCities, 'Любой');

		$criteria = new CDbCriteria;
		$criteria->select = 'id, username, so_name';
		$criteria->condition = 'role=:r';
		$criteria->params = array(':r'=>'owner');
		$criteria->order = 'username';
		$Rows = User::model()->findAll($criteria);
		$arUsers[0] = 'Любой';
		if (!empty($Rows))
			foreach ($Rows as $user)
				$arUsers[$user->id] = $user->username.' ('.$user->so_name.')';

		$arStatus = array(90=>'Любой', 0=>'деактивирован', 3=>'на модерации', 6=>'идут показы');
		$arKinds = array(90=>'Любое', 0=>'офис', 1=>'торговое', 2=>'Склад\Производство');

		$this->render('view', array(
			'form'		=>$form,
			'rows'		=>$Items,
			'pages'		=>$pages,
			'foundQty'	=>$qty,
			'Cities'	=>$arCities,
			'city'		=>$usr->getState('aicity', 0),
			'Kinds'		=>$arKinds,
			'kind'		=>$usr->getState('aikind', 90),
			'Statuses'	=>$arStatus,
			'status'	=>$usr->getState('aistatus', 90),
			'Users'		=>$arUsers,
			'owner'		=>$usr->getState('aiowner', 0),
			'Cpage'		=>$Cpage
			));
	}
	public function actionDelete(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		if (isset($_GET['flg'])){
			$Row = Search::model()->findByPK($id);
			if (empty($Row))
				throw new CHttpException(404);

			// Удалим статистику показов объекта
			$cmd = Yii::app()->db->createCommand('Delete from item_show_stat where item='.$Row->id.';');
			$cmd->execute();
			// Удалим объект из блокнтов арендаторов
			$cmd = Yii::app()->db->createCommand('Delete from notepad where item='.$Row->id.';');
			$cmd->execute();
			$s = Yii::app()->params->web_root;
			$i = strrpos($s, '/'); $a = strlen($s)-1;
			if ($i != $a)	$s.='/';
			// Удалим фотографии объекта
			$Fotos = Images::model()->findAll('id_item=:ii', array(':ii'=>$id));
			if (!empty($Fotos)) {
				$p = $s.'storage/images/';
				foreach ($Fotos as $foto) {
					@unlink ($p.$foto->img);
					@unlink ($p.'_thumbs/'.$foto->name_medium);
					@unlink ($p.'_thumbs/'.$foto->name_small);
				}
				$cmd = Yii::app()->db->createCommand('Delete from images where id_item='.$Row->id.';');
				$cmd->execute();
			}
			// Удалим прикрепленные файлы
			$Attachs = Attach::model()->findAll('item=:ii', array(':ii'=>$Row->id));
			if (!empty($Attachs)) {
				$p = $s.'storage/files/objs/';
				foreach ($Attachs as $fl)
					@unlink ($p.$fl->filename);
				$cmd = Yii::app()->db->createCommand('Delete from attachs where item='.$Row->id.';');
				$cmd->execute();
			}
			// Удалим (если есть) файл-архив с описанием объекта
			$p = $s.'storage/files/zipobj/obj'.$Row->id.'.zip';
			if (file_exists($p))
				@unlink ($p);
			// Удалим (если есть) pdf-файл с описанием объекта
			$p = $s.'storage/files/pdfobj/obj'.$Row->id.'.pdf';
			if (file_exists($p))
				@unlink ($p);

			if ($Objects_del->delete(false))
				$this->redirect(array('owner/view'));

			$Row->delete(false);
			$this->redirect(array('adminitems/view'));
		}

		$this->render('delete', array('id'=>$id));
	}

	public function actionUnset(){
		$usr = Yii::app()->user;
		unset($usr->aicity);
		unset($usr->aikind);
		unset($usr->aistatus);
		unset($usr->aiowner);
		$this->redirect(array('adminitems/view'));
	}

	public function filters(){
	return array(
		array(
			'application.filters.AccessFilter',
			'role'=>'admin',
		)
	);
	}
}
?>