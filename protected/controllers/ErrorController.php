<?php
  class ErrorController extends CController{
    public $defaultAction = 'error';
    public $pageDescription;
    public $pageKeywords;

    public function actionError(){
      $this->pageTitle = 'Неизвестная ошибка';
      $this->pageDescription = '';
      $this->pageKeywords = '';
      $this->render('error');    }  }
?>