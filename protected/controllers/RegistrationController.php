<?php 
class RegistrationController extends CController{
	public $defaultAction = 'view';
	public $pageClass = 'any';
	public $layout = 'inner02';
	public $pageDescription;
	public $pageKeywords;
	public $jsFiles = array();
	public $toBodyTag = '';

	public function actions(){
		return array(
			'captcha'=>array(
				'class'=>'MyCCaptchaAction',
				/*'class'=>'CCaptchaAction',
				'backColor'=> 0x003300,
				'maxLength'=> 3,
				'minLength'=> 3,
				'foreColor'=> 0x66FF66,*/
			),
		);
	}

	public function actionView(){
		$form = new Registration();
		$wasRegistration = false;
		$isAdmin = Yii::app()->user->id['user_role'] == 'admin';
		$Attrs[0] = 'организация';
		$Attrs[1] = 'Фамилия...';
		$Attrs[2] = 'Имя...';
		$Attrs[3] = 'Отчество...';
		$Attrs[4] = '+7 ___ _______';
		$Attrs[5] = 'E-mail...';
		$Attrs[6] = 'Логин...';

		if (empty($_POST['Registration'])) {
			$form->role = 'owner';
		} else {			$form->attributes = $_POST['Registration'];
			if ($form->rules == 0)
				$this->redirect(array('registration/view'));
			$form->verifyCode = $_POST['Registration']['verifyCode'];
			if ($Attrs[0]==$form->organization)	$form->organization='';
			if ($Attrs[1]==$form->so_name)		$form->so_name='';
			if ($Attrs[2]==$form->name)			$form->name='';
			if ($Attrs[3]==$form->otchestvo)	$form->otchestvo='';
			if ($Attrs[5]==$form->e_mail)		$form->e_mail='';
			if ($Attrs[4]==$form->phone)		$form->phone='';
			if ($Attrs[6]==$form->username)		$form->username='';
			if ($form->validate()){
				$form->password = md5($form->password);
				$form->username = trim($form->username);
				if ($isAdmin) {
					$form->state = 5; // статус: готов к употреблению
					$form->codeReg = null;
					$form->codeDate = null;
				} else {
					$form->state = 2; // статус: ожидает подтверждения регистрации
					$form->codeReg = $this->genRegCode();
					$form->codeDate = new CDbExpression('NOW()');
				}
				$form->info_moder = 1;
				$form->info_order = 1;
				$form->info_deactivate = 1;
				$form->auto_notepad = 1;
				$form->info_flag_email = 1;
				$form->info_email = $form->e_mail;
				$wg = $this->widget('AnalyzePhone', array('phone'=>$form->phone));
				if ($wg->isCellphone) {
					$form->info_flag_phone = 1;
					$form->info_phone = $wg->phone;
				}
				if ($form->save(false)){
					if (!$isAdmin) { // Если регистрирует пользователя Админ, то письма не отсылаем
						// Получим из базы id-ник нововставленной записи
						$criteria = new CDbCriteria;
						$criteria->condition = 'username=:un and e_mail=:em';
						$criteria->params = array(':un'=>$form->username, ':em'=>$form->e_mail);
						$currentUser = Registration::model()->find($criteria);
						// Отсылаем письмо
						require_once('protected/extensions/mailer/class.mailer.php');
						$mail = new Mailer();
						$mail->IsHTML(true);
						$mail->From = Yii::app()->params->admin_mail;
						$mail->Subject  = 'Портал Com-Arenda.ru. Активация аккаунта';
						$s = 'Здравствуйте, '.$currentUser->so_name.' '.$currentUser->name.' '.$currentUser->otchestvo.'.<br />Ваша учетная запись готова. Для ее активизации, перейдите по ссылке (действительна трое суток):<br /><a href="'.Yii::app()->params->site_base.'registration/activate/code/'.$currentUser->codeReg.'">'.Yii::app()->params->site_base.'registration/activate/code/'.$currentUser->codeReg.'</a><br />Ваш логин: '.$currentUser->username.'<br />Предлагаем посетить <a href="'.Yii::app()->params->site_base.'kabinet/'.$currentUser->role.'.html">Ваш Личный кабинет</a> на нашем сайте, где Вы сможете сделать тонкую настройку Вашего аккаунта, подключить дополнительные возможности, указать информационные контакты и многое другое.';
						$mail->Body = $s.Yii::app()->params->autoscriptum;
						$mail->AddAddress($form->e_mail);  
						// $mail->AddAddress('dpriest@list.ru'); 
						if ($mail->Send())
							$succ = 1;
						else	$succ = 0;
						$cmd = Yii::app()->db->createCommand('Insert into log_send_messages (kind, success, reciever, address, txt) Values (3, '.$succ.', '.$currentUser->id.', '."'$form->e_mail', '$s');");
						$cmd->execute();
					}
					$wasRegistration = true;
				}
			}		}

		$this->pageTitle = 'Регистрация';
		$this->pageDescription = '';
		$this->pageKeywords = '';

		if ($wasRegistration) {
			if ($isAdmin)
					$this->redirect(array('cabinet/view'));
			else	$this->render('activate', array('reg' =>$wasRegistration));
		} else {
			$this->jsFiles[] = 'registration.js';
			//$this->jsFiles[] = 'new.js'; 8557
			//$this->toBodyTag = ' onload="fakeChecks();fakeInputs();"';
			$this->render('view', array('form'	=>$form));
		}
	}
	
	public function actionRestore(){
		$ip = $_SERVER['REMOTE_ADDR'];
		$criteria = new CDbCriteria;
		$criteria->select = 'COUNT(*) as "qty"';
		$criteria->condition = 'ip=:ip and floodtime>DATE_SUB(NOW(), INTERVAL 30 MINUTE) and what=:wt';
		$criteria->params = array(':ip'=>$ip, ':wt'=>'mail');
		$Log = FloodLog::model()->find($criteria);
		if ($Log->qty>2)
			throw new CHttpException(404);
		$form = new Registration();
		$wasPost = false;
		$resCode = 0;
		$umail = '';

		if (!empty($_POST['Registration'])){
			$form->attributes = $_POST['Registration'];
			$filter = new CHtmlPurifier();
			$umail = $filter->purify($form->e_mail);
			$User = Registration::model()->find('e_mail=:em and state>3', array(':em'=>$form->e_mail));
			if (empty($User)) {
				$Flood = new FloodLog();
				$Flood->what = 'mail';
				$Flood->ip = $ip;
				$Flood->txt = substr($umail, 0, 511);
				$Flood->save(false);
				if ($Log->qty==2)
					$resCode = 2;
				else $resCode = 3;
			} else {
				$code = $this->genRegCode();
				$pass = md5(rand(11111, 99999));
				$cmd = Yii::app()->db->createCommand('Update users set password='."'$pass'".' codeReg='."'$code'".', codeDate=NOW(), state=2 where id='.$User->id.';');
				$cmd->execute();
				// Отсылаем письмо
				require_once('protected/extensions/mailer/class.mailer.php');
				$mail = new Mailer();
				$mail->IsHTML(true);
				$mail->From = Yii::app()->params->admin_mail;
				$mail->Subject  = 'Портал Com-Arenda.Ru. Восстановление пароля';
				$s = 'Здравствуйте, '.$User->so_name.' '.$User->name.' '.$User->otchestvo.'.<br />У Вашей учетной записи был сброшен пароль.<br />Ваш логин: '.$User->username.'<br />Новый пароль: '.$pass.'<br />Учетная запись заблокирована. Для продолжения работы ее следует вновь активировать, перейдя по ссылке (действительна трое суток):<br />'.Yii::app()->params->site_base.'registration/activate/code/'.$code.'<br />Рекомендуем в обязательном порядке изменить пароль в <a href="'.Yii::app()->params->site_base.'kabinet/'.$User->role.'.html">Вашем Личном кабинете</a>.<br />Если восстановление пароля было инициировано не Вами, то просим <a href="mailto:'.Yii::app()->params->admin_mail.'">сообщить администрации сайта</a> и приносим свои извинения за причиненные неудобства.';
				$mail->Body = $s.Yii::app()->params->autoscriptum;
				$mail->AddAddress($form->e_mail);  
				// $mail->AddAddress('dpriest@list.ru'); 
				if ($mail->Send())
					$succ = 1;
				else	$succ = 0;
				$cmd = Yii::app()->db->createCommand('Insert into log_send_messages (kind, success, reciever, address, txt) Values (3, '.$succ.', '.$User->id.', '."'$form->e_mail', '$s');");
				$cmd->execute();
				$resCode = 1;
			}
			$wasPost = true;
		}
		$this->pageTitle = 'Восстановление доступа к учетной записи';
		$this->pageDescription = '';
		$this->pageKeywords = '';

		if ($wasPost && ($resCode<3))
			$this->render('activate', array('res'=>$resCode));
		else
			$this->render('restore', array('form' =>$form, 'res'=>$resCode, 'lastmail'=>$umail));
	}
	
	public function actionActivate(){
		if (!isset($_GET['code']) || ($_GET['code']==''))
			throw new CHttpException(404);
		$cmd = Yii::app()->db->createCommand('Update users set codeReg=NULL, codeDate=NULL where codeDate<DATE_SUB(NOW(), INTERVAL 3 DAY);');
		$cmd->execute();
		$filter = new CHtmlPurifier();
		$code = $filter->purify($_GET['code']);
		$User = Registration::model()->find('codeReg=:cr', array(':cr'=>$code));
		$happy = 0;
		if (empty($User)) 
			$happy = 0;
		else {
			$cmd = Yii::app()->db->createCommand('Update users set codeReg=NULL, codeDate=NULL, state=5 where id='.$User->id.';');
			$cmd->execute();
			$happy = 1;
		}

		$this->pageTitle = 'Активация учетной записи';
		$this->pageDescription = '';
		$this->pageKeywords = '';
		$this->render('activate', array(
			'happy' =>$happy
		));
	}

	private function genRegCode () {
		$ch = 'abcdefghijklmnopqrstuvwxyz';
		$s = '';
		for ($j=0;$j<2;$j++) {
			$s .= rand(11, 99);
			for ($i=0;$i<3;$i++)
				$s .= $ch{rand(0, 25)};
			$s .= rand(11, 99);
		}
		return $s;
	}
}?>