<?php
  class NotepadController extends CController{
    public $defaultAction = 'view';
    public $pageDescription;
    public $pageKeywords;

    public function actionView(){		/*    Пока не восстребован
      $criteria = new CDbCriteria;
      $criteria->select = 'text_page, title, description, keywords';
      $criteria->condition = 'name=:name_page';
      $criteria->params = array(':name_page'=>'index');
      $Static_page = Statics::model()->find($criteria);

      $form = new Search_form();


      $this->render('view', array(
			'form'			=>$form,
		    'vpandus'		=> Yii::app()->user->getState('ses_pandus', 0),
		    'vputi'			=> Yii::app()->user->getState('ses_puti', 0)
		));
		*/
    }

	public function actionAdd(){
	// Добавление объекта в блокнот арендатора
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$idItem = $get->id_out;
		// Проверим, не присутствует ли уже добавляемый объект в блокноте
		$exists = Notepads::model()->exists('item=:it and tenant=:tn', array(':it'=>$idItem, ':tn'=>Yii::app()->user->id['user_id']));
		if (!$exists){
			$NewRec = new Notepads;
			$NewRec->status = 1;
			$NewRec->item = $idItem;
			$NewRec->tenant = Yii::app()->user->id['user_id'];
			$NewRec->save(false);
			$this->redirect(array('index/view')); // Уточнить, куда делать редирект
		} else $this->redirect(array('index/view')); // Уточнить, куда делать редирект
	}
	
    public function filters(){
      return array(
        array(
              'application.filters.AccessFilter',
              'role'=>'tenant',
              ),
      );
    }
	
  }
?>