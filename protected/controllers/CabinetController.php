<?php class CabinetController extends CController{
	public $defaultAction = 'view';
	public $layout = 'kabinets02';
	public $pageClass = 'any';
	public $pageDescription;
	public $pageKeywords;

	public function actionView(){
		$user = User::model()->find('id=:id_user', array(':id_user'=>Yii::app()->user->id['user_id']));
		if (empty($user)) // Если нет такого собственника 
			throw new CHttpException(404);

		$isTenant = Yii::app()->user->id['user_role'] == 'tenant';
		if (empty($user->info_email))
			$user->info_email = $user->e_mail;

		if ($isTenant)
				$form_options = new Tenant_options();
		else	$form_options = new Owner_options();
		$form_info = new Owner_info();
		$form_pass = new Owner_pass();

		$settsSaved = $infoSaved = $passSaved = $wasSend = false;

		if (isset($_POST['Owner_options'])){
			$form_options->attributes = $_POST['Owner_options'];
			$form_options->info_phone = $_POST['Owner_options']['tel_part1'].$_POST['Owner_options']['tel_part2'];
			$wasSend = true;
		}
		elseif (isset($_POST['Tenant_options'])){
			$form_options->attributes = $_POST['Tenant_options'];
			$form_options->info_phone = $_POST['Tenant_options']['tel_part1'].$_POST['Tenant_options']['tel_part2'];
			$wasSend = true;
		}
		if ($wasSend){
			$user->info_moder = $form_options->info_moder;
			$user->info_order = $form_options->info_order;
			if ($isTenant) {
				$user->info_deactivate = $form_options->info_deactivate;
				$user->auto_notepad = $form_options->auto_notepad;
			}
			$user->info_flag_phone = $form_options->info_flag_phone;
			$user->info_flag_email = $form_options->info_flag_email;
			if (trim($form_options->info_phone)=='')
				$user->info_phone = null;
			else $user->info_phone = $form_options->info_phone;
			if (trim($form_options->info_email)=='')
				$user->info_email = null;
			else $user->info_email = $form_options->info_email;
			if ($form_options->validate())
				if ($user->save(false)) {
					$settsSaved = true;
					$this->redirect(array('cabinet/view'));
				}
		}

		$Attrs[1] = 'Фамилия *';
		$Attrs[2] = 'Имя *';
		$Attrs[3] = 'Отчество';
		$Attrs[4] = 'Телефон *';
		$Attrs[5] = 'E-mail *';
		$Attrs[7] = 'Новый пароль *';
		$Attrs[8] = 'Повторите новый пароль *';

		if (isset($_POST['Owner_info'])){
			$form_info->attributes = $_POST['Owner_info'];
			if ($Attrs[1]==$form_info->so_name)		$form_info->so_name='';
			if ($Attrs[2]==$form_info->name)		$form_info->name='';
			if ($Attrs[3]==$form_info->otchestvo)	$form_info->otchestvo='';
			if ($Attrs[5]==$form_info->e_mail)		$form_info->e_mail='';
			if ($Attrs[4]==$form_info->phone)		$form_info->phone='';
			$user->name = $form_info->name;
			$user->so_name = $form_info->so_name;
			$user->otchestvo = $form_info->otchestvo;
			$user->e_mail = $form_info->e_mail;
			$user->phone = $form_info->phone;
			$form_info->user_id = $user->id;
			$wasSend = true;
			if ($form_info->validate())
				if ($user->save(false)) {
					$infoSaved = true;
					if (empty($user->otchestvo))
						$user->otchestvo = $Attrs[3];
					$this->redirect(array('cabinet/view'));
				}
		}

		if (isset($_POST['Owner_pass'])){
			$form_pass->attributes = $_POST['Owner_pass'];
			if ($Attrs[7]==$form_pass->password)	$form_pass->password='';
			if ($Attrs[8]==$form_pass->confirm)		$form_pass->confirm='';
			if ($form_pass->validate()){
				$user->password = md5($form_pass->password);
				$wasSend = true;
				if ($user->save(false)) {
					$passSaved = true;
					$form_pass->password = $Attrs[7];
					$form_pass->confirm = $Attrs[8];
					$this->redirect(array('cabinet/view'));
				}
			}
		}


		$this->pageTitle = 'Личный кабинет';
		$this->pageDescription = '';
		$this->pageKeywords = '';

		if (empty($user->info_phone)) {
			$tel1 = '';	$tel2 = '';
		} else {
			$tel1 = substr($user->info_phone, 0, 3);
			$tel2 = substr($user->info_phone, 3);
		}
		if (empty($user->so_name))	$user->so_name = $Attrs[1];
		if (empty($user->name))		$user->name = $Attrs[2];
		if (empty($user->otchestvo))$user->otchestvo = $Attrs[3];
		if (empty($user->e_mail))	$user->e_mail = $Attrs[5];
		if (empty($user->phone))	$user->phone = $Attrs[4];
		$form_pass->password = $Attrs[7];
		$form_pass->confirm = $Attrs[8];

		$this->render('view', array(
			'form_options'	=>$form_options,
			'form_info'		=>$form_info,
			'form_pass'		=>$form_pass,
			'user'			=>$user,
			'tel1'			=>$tel1,
			'tel2'			=>$tel2,
			'Attrs'			=>$Attrs,
			'wasSend'		=>$wasSend,
			'settsSaved'	=>$settsSaved,
			'infoSaved'		=>$infoSaved,
			'passSaved'		=>$passSaved,
			'tenant'		=>$isTenant
		));
	}

	public function actionObjects(){
		if (Yii::app()->user->id['user_role'] == 'tenant') // Только собственники пропускаются дальше
			throw new CHttpException(404);
		$id = Yii::app()->user->id['user_id'];

		$criteria = new CDbCriteria;
		$criteria->alias = 'O';
		$criteria->select = 'O.id, O.kind, O.special, O.city, S.name as "street", O.house, O.housing, O.storey, O.status, O.created, O.name, O.total_item_sqr, O.price, (Select COUNT(*) from item_show_stat SS where SS.item=O.id) as "showQty", (Select F.name_small from images F where F.id_item=O.id order by F.id asc limit 1) as "hasFoto"';
		$criteria->join = 'Left join streets S on S.id=O.street';
		$criteria->condition = 'owner_id=:id_user';
		$criteria->params = array(':id_user'=>$id);
		$criteria->order = 'O.created desc';
		$criteria->together = false;

		$qRows = Search::model()->count($criteria);
		/* Постраничная разбивка */
		$pages = new CPagination($qRows);
		$pages->pageSize = '25';
		$pages->applyLimit($criteria);

		$Rows = Search::model()->findAll($criteria);

		$this->pageTitle = 'Мои объекты. Личный кабинет';
		$this->pageDescription = '';
		$this->pageKeywords = '';

		$this->render('objects', array(
			'Rows'	=>$Rows,
			'qRows'	=>$qRows,
			'pages'	=>$pages
		));
	}

	public function actionOrders(){
		if (Yii::app()->user->id['user_role'] == 'tenant') // Только собственники пропускаются дальше
			throw new CHttpException(404);
		$id = Yii::app()->user->id['user_id'];

		$criteria = new CDbCriteria;
		$criteria->alias = 'T';
		$criteria->select = 'T.id, T.who_id, T.kind, T.city, COALESCE(D.name, "Не важен") as "district", T.created, NULLIF(T.squareMin, "0") as "squareMin", NULLIF(T.squareMax, "0") as "squareMax", NULLIF(T.priceMin, "0") as "priceMin", NULLIF(T.priceMax, "0") as "priceMax", U.so_name as "whoSoName", U.name as "whoName", U.otchestvo as "whoOtchestvo", U.phone as "whoPhone"';
		$criteria->join = 'Left join districts D on D.id=T.district join users U on U.id=T.who_id join bid_owners B on B.tender=T.id';
		$criteria->condition = 'owner=:id_user';
		$criteria->params = array(':id_user'=>$id);
		$criteria->order = 'T.created desc';

		$qRows = Tenders::model()->count($criteria);
		/* Постраничная разбивка */
		$pages = new CPagination($qRows);
		$pages->pageSize = '25';
		$pages->applyLimit($criteria);

		$Rows = Tenders::model()->findAll($criteria);
		if (empty($Rows)) {
			$qObjs = Search::model()->count('owner_id=:id', array(':id'=>$id));
		}

		$this->pageTitle = 'Заявки. Личный кабинет';
		$this->pageDescription = '';
		$this->pageKeywords = '';

		$this->render('orders', array(
			'Rows'	=>$Rows,
			'qRows'	=>$qRows,
			'qObjs'	=>$qObjs,
			'pages'	=>$pages
		));
	}

	public function actionMyorders(){
		if (Yii::app()->user->id['user_role'] == 'owner') // Только арендаторы пропускаются дальше
			throw new CHttpException(404);
		$id = Yii::app()->user->id['user_id'];

		$criteria = new CDbCriteria;
		$criteria->alias = 'T';
		$criteria->select = 'T.id, T.kind, T.city, COALESCE(D.name, "Не важен") as "district", T.created, NULLIF(T.squareMin, "0") as "squareMin", NULLIF(T.squareMax, "0") as "squareMax", NULLIF(T.priceMin, "0") as "priceMin", NULLIF(T.priceMax, "0") as "priceMax"';
		$criteria->join = 'Left join districts D on D.id=T.district';
		$criteria->condition = 'who_id=:id_user';
		$criteria->params = array(':id_user'=>$id);
		$criteria->order = 'T.created desc, T.id';

		$qRows = Tenders::model()->count($criteria);
		/* Постраничная разбивка */
		$pages = new CPagination($qRows);
		$pages->pageSize = '25';
		$pages->applyLimit($criteria);

		$Rows = Tenders::model()->findAll($criteria);

		$this->pageTitle = 'Мои заявки. Личный кабинет';
		$this->pageDescription = '';
		$this->pageKeywords = '';

		$this->render('myorders', array(
			'Rows'	=>$Rows,
			'qRows'	=>$qRows,
			'pages'	=>$pages
		));
	}

	public function actionNotepad(){
		if (Yii::app()->user->id['user_role'] == 'owner') // Только арендаторы пропускаются дальше
			throw new CHttpException(404);
		$id = Yii::app()->user->id['user_id'];

		$criteria = new CDbCriteria;
		$criteria->alias = 'N';
		$criteria->select = 'N.id, N.item, N.created, N.status, I.status as "itemStatus", I.special, I.city, S.name as "street", I.house, I.total_item_sqr, I.price, U.id as "ownId", U.so_name as "ownSoName", U.name as "ownName", U.otchestvo as "ownOtchestvo", (Select F.name_small from images F where F.id_item=N.item order by F.id asc limit 1) as "hasFoto"';
		$criteria->join = 'join items I on I.id=N.item left join streets S on S.id=I.street Left join users U on U.id=I.owner_id';
		$criteria->condition = 'tenant=:id_user and I.itemType<>1';
		$criteria->params = array(':id_user'=>$id);
		$criteria->order = 'N.status asc, N.created desc';

		$qRows = Notepads::model()->count($criteria);
		/* Постраничная разбивка */
		$pages = new CPagination($qRows);
		$pages->pageSize = '25';
		$pages->applyLimit($criteria);

		$Rows = Notepads::model()->findAll($criteria);

		$this->pageTitle = 'Блокнот. Личный кабинет';
		$this->pageDescription = '';
		$this->pageKeywords = '';

		$this->render('notepad', array(
			'Rows'	=>$Rows,
			'qRows'	=>$qRows,
			'pages'	=>$pages
		));
	}

	public function actionMypage(){
		$user = User::model()->find('id=:id_user', array(':id_user'=>Yii::app()->user->id['user_id']));
		if (empty($user)) // Если нет такого собственника 
			throw new CHttpException(404);

		$addcSaved = $paragSaved = $wasSend = false;
		$Attrs[1] = '';
		$Attrs[2] = '';
		$Attrs[3] = '';

		$form_page = new User_page();
		$form_addc = new User_addcontacts();

		if (empty($_POST['User_page'])) {
			if (empty($user->parag1_name))
				$user->parag1_name = 'Обо мне';
			$form_page->parag1_name = $user->parag1_name;
			if (empty($user->parag2_name))
				$user->parag2_name = 'Обращение к клиентам';
			$form_page->parag2_name = $user->parag2_name;
			$vtext1 = $user->parag1_text;
			$vtext2 = $user->parag2_text;
		} else {
			$form_page->attributes = $_POST['User_page'];
			$filter = new CHtmlPurifier();
			$form_page->parag1_name = $filter->purify(trim($form_page->parag1_name));
			$form_page->parag2_name = $filter->purify(trim($form_page->parag2_name));
			$form_page->parag1_text = $filter->purify(trim($form_page->parag1_text));
			$form_page->parag2_text = $filter->purify(trim($form_page->parag2_text));

			$vtext1 = $form_page->parag1_text;
			$vtext2 = $form_page->parag2_text;
			$wasSend = true;
			if ($form_page->validate()){
				if ($form_page->parag1_name=='')
						$user->parag1_name = null;
				else	$user->parag1_name = $form_page->parag1_name;
				if ($form_page->parag2_name=='')
						$user->parag2_name = null;
				else	$user->parag2_name = $form_page->parag2_name;
				if ($form_page->parag1_text=='')
						$user->parag1_text = null;
				else	$user->parag1_text = $form_page->parag1_text;
				if ($form_page->parag2_text=='')
						$user->parag2_text = null;
				else	$user->parag2_text = $form_page->parag2_text;
				if ($user->save(false)){
					$paragSaved = true;
				}
			}
		}

		if (empty($_POST['User_addcontacts'])) {
			if (empty($user->city))
					$form_addc->city = $Attrs[1];
			else	$form_addc->city = $user->city;
			if (empty($user->skype))
					$form_addc->skype = $Attrs[2];
			else	$form_addc->skype = $user->skype;
			if (empty($user->icq))
					$form_addc->icq = $Attrs[3];
			else	$form_addc->icq = $user->icq;
		} else {
			$form_addc->attributes = $_POST['User_addcontacts'];
			if ($Attrs[1]==$form_addc->city)	$form_addc->city='';
			if ($Attrs[2]==$form_addc->skype)	$form_addc->skype='';
			if ($Attrs[3]==$form_addc->icq)		$form_addc->icq='';

			$filter = new CHtmlPurifier();
			$form_addc->city = $filter->purify(trim($form_addc->city));
			$form_addc->skype = $filter->purify(trim($form_addc->skype));
			$form_addc->icq = $filter->purify(trim($form_addc->icq));

			$wasSend = true;
			if ($form_addc->validate()){
				if ($form_addc->city=='')
						$user->city = null;
				else	$user->city = $form_addc->city;
				if ($form_addc->skype=='')
						$user->skype = null;
				else	$user->skype = $form_addc->skype;
				if ($form_addc->icq=='')
						$user->icq = null;
				else	$user->icq = $form_addc->icq;
				if ($user->save(false)){
					$addcSaved = true;
				}
			} else {
				if (empty($form_addc->city))	$form_addc->city=$Attrs[1];
				if (empty($form_addc->skype))	$form_addc->skype=$Attrs[2];
				if (empty($form_addc->icq))		$form_addc->icq=$Attrs[3];
			}
		}

		$this->pageTitle = 'Моя страница. Личный кабинет';
		$this->pageDescription = '';
		$this->pageKeywords = '';

		$this->render('mypage', array(
			'form_page'		=>$form_page,
			'form_addc'		=>$form_addc,
			'vtext1'		=>$vtext1,
			'vtext2'		=>$vtext2,
			'Attrs'			=>$Attrs,
			'wasSend'		=>$wasSend,
			'addcSaved'		=>$addcSaved,
			'paragSaved'	=>$paragSaved,
			'uphoto'		=>$user->photo
		));
	}

	/*
	public function actionSave_photo(){
		if (!empty($_POST) && $_POST['x1']!='' && $_POST['y1']!='' && $_POST['x2']!='' && $_POST['y2']!='' && $_POST['img']){
			include_once 'protected/extensions/watermark/WideImage.php';
			$infoIMG = getimagesize('storage/images/personal/temp/'.$_POST['img']);
			$qualityIMG = 90;
			if ($infoIMG['mime'] == 'image/png') $qualityIMG = 0.9;
			WideImage::load('storage/images/personal/temp/'.$_POST['img'])->crop($_POST['x1'], $_POST['y1'], abs($_POST['x2'] - $_POST['x1']), abs($_POST['y2'] - $_POST['y1']))->resize(90, 120)->saveToFile('storage/images/personal/'.$_POST['img'], $qualityIMG);
			unlink ('storage/images/personal/temp/'.$_POST['img']);
			
			$criteria = new CDbCriteria;
            $criteria->condition = 'id=:var';
            $criteria->params = array(':var'=>Yii::app()->user->id['user_id']);
            $Users_obj = User::model()->find($criteria);
			if (!empty($Users_obj)){
				if ($Users_obj->photo != '') unlink ('storage/images/personal/'.$Users_obj->photo);
				$Users_obj->photo = $_POST['img'];
				$Users_obj->save(false);
			}
		}
		$this->redirect(array('cabinet/mypage'));
	}
	*/

	public function actionFake(){
		$this->render('lc_mypage', array(
		));
	}

	public function filters(){
		return array(
			array(
				'application.filters.AccessFilter',
				'role'=>'owner,tenant'
			)
		);
	}

}?>