<?php 
class SendmailController extends CController{
	public $defaultAction = 'view';
	public $layout = 'inner02';
	public $pageDescription;
	public $pageKeywords;

	public function actions(){
		return array(
			'captcha'=>array(
				'class'=>'MyCCaptchaAction',
				/*'class'=>'CCaptchaAction',
				'backColor'=> 0x003300,
				'maxLength'=> 3,
				'minLength'=> 3,
				'foreColor'=> 0x66FF66,*/
			),
		);
	}

	public function actionView(){
		$form = new SendMail();
		$wasSend = false;
		$txt = '';
		
		if (!empty($_POST['SendMail'])){			$form->attributes = $_POST['SendMail'];
			$txt = $form->messTxt;
			if ($form->validate()){
				$filter = new CHtmlPurifier();
				$form->e_mail = $filter->purify($form->e_mail);
				$form->name = $filter->purify($form->name);
				$form->messTxt = $filter->purify($form->messTxt);
				switch ($form->receiver) {
					case 1 : {$toMail = 'ptil@yandex.ru'; break;}
					case 2 : {$toMail = 'ptil@yandex.ru'; break;}
					default: $toMail = 'ptil@yandex.ru';
				}
				// Отсылаем письмо
				require_once('protected/extensions/mailer/class.mailer.php');
				$mail = new Mailer();
				$mail->From = Yii::app()->params->admin_mail;
				$mail->Subject  = 'Письмо с сайта Com-Arenda.ru';
				$mail->Body = 'Имя отправителя: '.$form->name."\n".'Обратный email: '.$form->e_mail."\n".'Отправлено: '.date('d.m.Y h:i:s')."\n".'Текст письма:'."\n".$form->messTxt;
				$mail->AddAddress($toMail);  
				if ($mail->Send())
					$wasSend = true;
			}
		}

		$aMails = array('0'=>'Приветливый админ', '1'=>'Бухгалтерия', '2'=>'Секретарша');

		$this->pageTitle = 'Форма обратной связи';
		$this->pageDescription = '';
		$this->pageKeywords = '';

		if ($wasSend)
			$this->render('success');
		else
			$this->render('view', array('form' =>$form, 'recMails' =>$aMails, 'text'=>$txt));
	}
}?>