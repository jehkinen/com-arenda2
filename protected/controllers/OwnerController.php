<?php class OwnerController extends CController{
	public $defaultAction = 'view';
	public $layout = 'inner01';
	public $pageDescription;
	public $pageKeywords;

	public function actionView(){
		$user = Owner_user::model()->find('id=:id_user', array(':id_user'=>Yii::app()->user->id['user_id']));
		if (empty($user)) // Если нет такого собственника 
			throw new CHttpException(404);

		/* ~~~~ Получение данных об объектах собственника для второй вкладки ~~~~~~~  */
		$criteria = new CDbCriteria;
		$criteria->alias = 'O';
		$criteria->select = 'O.id, O.city, S.name as "street", O.house, O.housing, O.storey, O.status, O.created, O.name, O.total_item_sqr, O.price, (Select COUNT(*) from item_show_stat SS where SS.item=O.id) as "showQty"';
		$criteria->join = 'Left join streets S on S.id=O.street';
		$criteria->condition = 'owner_id=:id_user';
		$criteria->params = array(':id_user'=>$user->id);
		$criteria->order = 'O.created desc';
		$criteris->together = false;
		$Rows = Search::model()->findAll($criteria);
		/* /// ~~~~ Получение данных об объектах собственника для второй вкладки ~~~~~~~  */
		/* ~~~~ Получение данных об имеющихся заявках для третьей вкладки ~~~~~~~  */
		$criteria = new CDbCriteria;
		$criteria->alias = 'T';
		$criteria->select = 'T.id, T.who_id, T.kind, T.city, COALESCE(D.name, "Не важен") as "district", T.created, '.
		'NULLIF(T.squareMin, "0") as "squareMin", NULLIF(T.squareMax, "0") as "squareMax", '.
		'NULLIF(T.priceMin, "0") as "priceMin", NULLIF(T.priceMax, "0") as "priceMax", '.
		'U.so_name as "whoSoName", U.name as "whoName", U.otchestvo as "whoOtchestvo", U.phone as "whoPhone"';
		$criteria->join = 'Left join districts D on D.id=T.district join users U on U.id=T.who_id '.
			'join bid_owners B on B.tender=T.id';
		$criteria->condition = 'owner=:id_user';
		$criteria->params = array(':id_user'=>$user->id);
		$criteria->order = 'T.created desc';
		$Tends = Tenders::model()->findAll($criteria);
		/* /// ~~~~ Получение данных об имеющихся заявках для третьей вкладки ~~~~~~~  */

		if (empty($user->info_email))
			$user->info_email = $user->e_mail;

		$form_options = new Owner_options();
		$form_info = new Owner_info();
		$form_pass = new Owner_pass();

		if (isset($_POST['Owner_options'])){			$form_options->attributes = $_POST['Owner_options'];
			$form_options->info_phone = $_POST['Owner_options']['tel_part1'].$_POST['Owner_options']['tel_part2'];
			$user->info_moder = $form_options->info_moder;
			$user->info_order = $form_options->info_order;
			$user->info_flag_phone = $form_options->info_flag_phone;
			$user->info_flag_email = $form_options->info_flag_email;
			if (trim($form_options->info_phone)=='')
				$user->info_phone = null;
			else $user->info_phone = $form_options->info_phone;
			if (trim($form_options->info_email)=='')
				$user->info_email = null;
			else $user->info_email = $form_options->info_email;
			if ($form_options->validate())
				if ($user->save(false))
					$this->redirect(array('owner/view'));		}

		if (isset($_POST['Owner_info'])){
			$form_info->attributes = $_POST['Owner_info'];
			$user->name = $form_info->name;
			$user->so_name = $form_info->so_name;
			$user->otchestvo = $form_info->otchestvo;
			$user->e_mail = $form_info->e_mail;
			$user->phone = $form_info->phone;
			$form_info->user_id = $user->id;
			if ($form_info->validate())
				if ($user->save(false))
					$this->redirect(array('owner/view'), false, 302);
		}

		if (isset($_POST['Owner_pass'])){
			$form_pass->attributes = $_POST['Owner_pass'];
			if ($form_pass->validate()){
			$user->password = md5($form_pass->password);
			if ($user->save(false))
				$this->redirect(array('owner/view'));
			}
		}
		$this->pageTitle = 'Кабинет собственника';
		$this->pageDescription = '';
		$this->pageKeywords = '';

		if (empty($user->info_phone)) {
			$tel1 = '';	$tel2 = '';
		} else {
			$tel1 = substr($user->info_phone, 0, 3);
			$tel2 = substr($user->info_phone, 3);
		}

		$this->render('view', array(
			'form_options'	=>$form_options,
			'form_info'		=>$form_info,
			'form_pass'		=>$form_pass,
			'rows'			=>$Rows,
			'tenders'		=>$Tends,
			'user'			=>$user,
			'tel1'			=>$tel1,
			'tel2'			=>$tel2
		));

	}

	public function actionChange(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));

		$Objects_del = Objects::model()->find('id=:id_object', array(':id_object'=>$get->id_out));
		if (empty($Objects_obj))
			throw new CHttpException(404);

		$form = new Objects();

		if (isset($_POST['Objects'])){
			$Objects_obj->status = $_POST['Objects']['status'];
			if ($Objects_obj->save(false))
				$this->redirect(array('owner/view'));
		}

		$listStat = array(0=>'не активен', 1=>'объект активен', 2=>'модерация');
		$form->status = $Objects_obj->status;

		$this->render('change', array(
			'form'		=>$form,
			'listStat'	=>$listStat,
		));	}

	public function actionDelete(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		di('ia');
		$id = $get->id_out;
		if (isset($_GET['flg'])){
			$Objects_del = Search::model()->find('id=:id and owner_id=:oi', array(':id'=>$id, ':oi'=>Yii::app()->user->id['user_id']));
			if (empty($Objects_del))
				throw new CHttpException(404);
			// Удалим статистику показов объекта
			$cmd = Yii::app()->db->createCommand('Delete from item_show_stat where item='.$id.';');
			$cmd->execute();
			// Удалим объект из блокнтов арендаторов
			$cmd = Yii::app()->db->createCommand('Delete from notepad where item='.$id.';');
			$cmd->execute();
			$s = Yii::app()->params->web_root;
			$i = strrpos($s, '/'); $a = strlen($s)-1;
			if ($i != $a)	$s.='/';
			// Удалим фотографии объекта
			$Fotos = Images::model()->findAll('id_item=:ii', array(':ii'=>$id));
			if (!empty($Fotos)) {
				$p = $s.'storage/images/';
				foreach ($Fotos as $foto) {
					@unlink ($p.$foto->img);
					@unlink ($p.'_thumbs/'.$foto->name_medium);
					@unlink ($p.'_thumbs/'.$foto->name_small);
				}
				$cmd = Yii::app()->db->createCommand('Delete from images where id_item='.$id.';');
				$cmd->execute();
			}
			// Удалим прикрепленные файлы
			$Attachs = Attach::model()->findAll('item=:ii', array(':ii'=>$id));
			if (!empty($Attachs)) {
				$p = $s.'storage/files/objs/';
				foreach ($Attachs as $fl)
					@unlink ($p.$fl->filename);
				$cmd = Yii::app()->db->createCommand('Delete from attachs where item='.$id.';');
				$cmd->execute();
			}
			// Удалим (если есть) файл-архив с описанием объекта
			$p = $s.'storage/files/zipobj/obj'.$id.'.zip';
			if (file_exists($p))
				@unlink ($p);
			// Удалим (если есть) pdf-файл с описанием объекта
			$p = $s.'storage/files/pdfobj/obj'.$id.'.pdf';
			if (file_exists($p))
				@unlink ($p);
			if ($Objects_del->delete(false))
				$this->redirect(array('owner/view'));
		}
		$this->render('_delete', array('id_obj' =>$id_obj));
	}

	public function filters(){
		return array(
			array(
				'application.filters.AccessFilter',
				'role'=>'owner'
			)
		);
	}

}?>