<?php
class AdminclaimsController extends CController{
	public $defaultAction = 'view';
	public $layout = 'control';
	public $pageDescription;
	public $pageKeywords;

	public function actionView(){
		$this->pageTitle = 'Просмотр жалоб';
		$this->pageDescription = '';
		$this->pageKeywords = '';
		$usr = Yii::app()->user;
        # Определяем сортировку для запроса, если нет сортировки то сортируем по id обекта
        $order = '';
		if (isset($_GET['sort'])){		  $order = $_GET['sort'];
		  if (isset($_GET['type'])) $order .= ' desc';		}
		if (empty($order)) $order = 'id';

		$criteria = new CDbCriteria;
		$criteria->alias = 'C';
		$criteria->select = 'C.id, C.item, C.tenant, C.sended, C.kind, T.username as "tenLogin", T.so_name as "tenLastname", T.name as "tenFirstname", O.username as "ownLogin", O.so_name as "ownLastname", O.name as "ownFirstname", I.owner_id as "itemOwner", I.name as "itemName", COALESCE(S.name, "") as "itemStreet", COALESCE(I.house, "?") as "itemHouse"';
		$criteria->join = 'join users T on T.id=C.tenant join items I on I.id=C.item left join streets S on S.id=I.street join users O on O.id=I.owner_id';
		$criteria->order = $order;

		$qty = Claim::model()->count($criteria);
		# Постраничная разбивка
		$pages = new CPagination($qty);
		$pages->pageSize = 40;
		$pages->applyLimit($criteria);
		# Получаем текущую страницу (нумерация начинается с 0, поэтому +1)
		$Cpage = $pages->getCurrentPage()+1;

		$Rows = Claim::model()->findAll($criteria);

		$this->render('view', array(
			'rows'		=>$Rows,
			'pages'		=>$pages,
			'foundQty'	=>$qty,
			'Cpage'		=>$Cpage
			));
	}
	public function filters(){
	return array(
		array(
			'application.filters.AccessFilter',
			'role'=>'admin',
		));
	}
}?>