<?php class DirectoriesController extends CController{

	public $layout = 'control';
	public $defaultAction = 'view';

	public function filters(){
		return array(
		array(
				'application.filters.AccessFilter',
				'role'=>'admin',
				),
		);
	}

	#-------------------------------------------------------------------------
	# Управление справочниками
	#-------------------------------------------------------------------------

	public function actionView_all(){
		$vals = Yii::app()->user;
		if (isset($vals->dict_districs_city))	unset($vals->dict_districs_city);
		if (isset($vals->dict_street_city))		unset($vals->dict_street_city);
		if (isset($vals->dict_providers_city))	unset($vals->dict_providers_city);
		if (isset($vals->dict_telcompany_city))	unset($vals->dict_telcompany_city);
		$this->render('view_all');
	}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	Сгруппируем справочники по типам на основании структуры их таблиц.
	Тип 1. Справочники, зависящие от таблицы Области (areas)
		Поля:	id - Идентификатор записи
			name - Наименование
		Таблицы: city (города)

	Тип 10. Справочники, зависящие от таблицы Города (cities)
		Поля:	id - Идентификатор записи
			name - Наименование
			city - Город
		Таблицы: districts (районы), streets (улицы)
	Тип 11. Справочники, зависящие от таблицы Города (cities) и имеющие остальные атрибуты большинства других справочников
		Поля:	id - Идентификатор записи
			city - Город
			name - Наименование
			shname - Краткое наименование, выводимое в combobox`ах
		Таблицы: tel_companies (Телефонные компании), net_providers (Интернет-провайдеры)
	Тип 20. Справочники характеристик объекта без дополнительных полей
		Поля:	id - Идентификатор записи
			name - Наименование
			shname - Краткое наименование, выводимое в combobox`ах
		Таблицы: classes, contr_forms, flooring, functionality, kind_structure, net_providers, parking, pay_utilities, repair, tel_companies, ventilation
	Тип 30. Справочники с одним полем, ествественным ключом
		Поля:	name - Наименование, первичный ключ
		Таблицы: cities (города)

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */



	#*************************************************
	# Справочники тип 1
	#*************************************************

	#-------------------------------------------------------------------------
	# Области городов
	#-------------------------------------------------------------------------

	public function loadArea($id)
	{
		$model=Area::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function actionAreas(){

		$model=new Area('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Area']))
			$model->attributes=$_GET['Area'];

		$this->render('type1',array(
			'model'=>$model,
		));
	}

	public function actionDelete_area($id){
		$this->loadArea($id)->delete();
		$this->redirect(Yii::app()->createUrl("directories/areas"));
	}
	public function actionadd_area()
	{
		$model=new Area;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidationArea($model);

		if(isset($_POST['Area']))
		{
			$model->attributes=$_POST['Area'];
			if($model->save())
				$this->redirect(array('Areas'));
		}

		$this->render('type1_add',array(
			'model'=>$model,
		));
	}


	public function actionEdit_area($id)
	{
		$model=$this->loadArea($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidationArea($model);

		if(isset($_POST['Area']))
		{
			$model->attributes=$_POST['Area'];
			if($model->save())
				$this->redirect(array('Areas'));
		}

		$this->render('type1_edit',array(
			'model'=>$model,
		));
	}
	protected function performAjaxValidationArea($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='Area-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	#*************************************************
	# Справочники тип 10
	#*************************************************

	#-------------------------------------------------------------------------
	# Районы городов
	#-------------------------------------------------------------------------
	public function actionDistricts(){

	/* Получим список городов для фильтра */
		$criteria = new CDbCriteria;
		$criteria->select = 'name';
		$criteria->order = 'name';
		$list_cities = CHtml::listData( City::model()->findAll($criteria), 'name', 'name');
		$selected = '--Выберите город--';
		array_unshift($list_cities, $selected);

	$vals = Yii::app()->user;
	
	$Rows = null;
	$pages = null;
	if (isset($vals->dict_districs_city)) {
	// Если попали в контроллер путем клика по ссылке в пейджере
		$criteria = new CDbCriteria;
		$criteria->condition = 'city=:city_value';
		$criteria->params = array(':city_value'=>$vals->dict_districs_city);
		$criteria->order = 'name';
		$totalQty = District::model()->count($criteria);
		$pages = new CPagination($totalQty);
		$pages->pageSize = 25;
		$pages->applyLimit($criteria);
		$Rows = District::model()->findAll($criteria);
		$selected = $vals->dict_districs_city;
	}
	$this->render('type10', array(
		'Cities'	=>$list_cities,
		'sel_city'	=>$selected,
		'dicName'	=>'Районы городов',
		'className'	=>'districts',
		'linkAdd'	=>'directories/districts_add',
		'nameAction'	=>'select_city',
		'rows'		=>$Rows,
		'pages'		=>$pages
	));
	}

	public function actionDistricts_add(){
		$form = new District();
		if (!empty($_POST['District'])){
		$form->attributes = $_POST['District'];
		if($form->validate()){
			Yii::app()->user->setState('dict_districs_city', $_POST['District']['city']);
			if ($form->save(false))
			$this->redirect(array('directories/districts'));
		}
		}
	/* Получим список городов для фильтра */
		$criteria = new CDbCriteria;
		$criteria->select = 'name';
		$criteria->order = 'name';
		$list_cities = CHtml::listData( City::model()->findAll($criteria), 'name', 'name');
		$def = Yii::app()->user->getState('dict_districs_city', 'none');

		$this->render('type10_add', array(
		'dicName'	=>'новый район',
		'linkCancel'	=>'directories/districts',
		'modelName'	=>'District',
		'Cities'	=>$list_cities,
		'selCity'	=>$def,
		'form'		=>$form
	));
	}

	public function actionDistricts_edit(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = District::model()->find($criteria);
		if (empty($Row))
		throw new CHttpException(404);

		$form = new District();
		if (!empty($_POST['District'])){
		$form->attributes = $_POST['District'];
		$form->city = Yii::app()->user->getState('dict_districs_city', '');
		if($form->validate()){
			$Row->attributes = $form->attributes;
			if ($Row->save(false))
			$this->redirect(array('directories/districts'));
		} else $Row->attributes = $form->attributes;
		}
		Yii::app()->user->setState('dict_districs_city', $Row->city);
		$this->render('type10_edit', array(
		'dicName'	=>'район города',
		'linkCancel'	=>'directories/districts',
		'row'		=>$Row,
		'form'		=>$form
	));
	}

	public function actionDistricts_delete(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		if (isset($_GET['flg'])){
		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = District::model()->find($criteria);
		if (empty($Row))
			throw new CHttpException(404);

		if ($Row->delete(false))
			$this->redirect(array('directories/districts'));
		}

		$this->render('type10_delete', array(
		'linkYes'	=>'directories/districts_delete',
		'linkNo'	=>'directories/districts',
		'id'		=>$id
	));
	}

	#-------------------------------------------------------------------------
	# Улицы
	#-------------------------------------------------------------------------
	public function actionStreet(){

	/* Получим список городов для фильтра */
		$criteria = new CDbCriteria;
		$criteria->select = 'name';
		$criteria->order = 'name';
		$list_cities = CHtml::listData( City::model()->findAll($criteria), 'name', 'name');
		$selected = '--Выберите город--';
		array_unshift($list_cities, $selected);

	$vals = Yii::app()->user;
	
	$Rows = null;
	$pages = null;
	if (isset($vals->dict_street_city)) {
	// Если попали в контроллер путем клика по ссылке в пейджере
		$criteria = new CDbCriteria;
		$criteria->condition = 'city=:city_value';
		$criteria->params = array(':city_value'=>$vals->dict_street_city);
		$criteria->order = 'name';
		$totalQty = Street::model()->count($criteria);
		$pages = new CPagination($totalQty);
		$pages->pageSize = 25;
		$pages->applyLimit($criteria);
		$Rows = Street::model()->findAll($criteria);
		$selected = $vals->dict_street_city;
	}
	$this->render('type10', array(
		'Cities'	=>$list_cities,
		'sel_city'	=>$selected,
		'dicName'	=>'Улицы городов',
		'className'	=>'street',
		'linkAdd'	=>'directories/street_add',
		'nameAction'	=>'select_street',
		'rows'		=>$Rows,
		'pages'		=>$pages
	));

	}

	public function actionStreet_add(){
		$form = new Street();
		if (!empty($_POST['Street'])){
		$form->attributes = $_POST['Street'];
		if($form->validate()){
			Yii::app()->user->setState('dict_street_city', $_POST['Street']['city']);
			if ($form->save(false))
			$this->redirect(array('directories/street'));
		}
		}

		/* Получим список городов для фильтра */
		$criteria = new CDbCriteria;
		$criteria->select = 'name';
		$criteria->order = 'name';
		$list_cities = CHtml::listData( City::model()->findAll($criteria), 'name', 'name');
		$def = Yii::app()->user->getState('dict_street_city', 'none');

		$this->render('type10_add', array(
		'dicName'	=>'новую улицу',
		'linkCancel'	=>'directories/street',
		'modelName'	=>'Street',
		'Cities'	=>$list_cities,
		'selCity'	=>$def,
		'form'		=>$form
	));
	}

	public function actionStreet_edit(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = Street::model()->find($criteria);
		if (empty($Row))
		throw new CHttpException(404);

		$form = new Street();
		if (!empty($_POST['Street'])){
		$form->attributes = $_POST['Street'];
		$form->city = Yii::app()->user->getState('dict_street_city', '');
		if($form->validate()){
			$Row->attributes = $form->attributes;
			if ($Row->save(false))
			$this->redirect(array('directories/street'));
		} else $Row->attributes = $form->attributes;
		}

		Yii::app()->user->setState('dict_street_city', $Row->city);
		$this->render('type10_edit', array(
		'dicName'	=>'улицу',
		'linkCancel'	=>'directories/street',
		'row'		=>$Row,
		'form'		=>$form
	));
	}

	public function actionStreet_delete(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		if (isset($_GET['flg'])){
		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = Street::model()->find($criteria);
		if (empty($Row))
			throw new CHttpException(404);

		if ($Row->delete(false))
			$this->redirect(array('directories/street'));
		}

		$this->render('type10_delete', array(
		'linkYes'	=>'directories/street_delete',
		'linkNo'	=>'directories/street',
		'id'		=>$id
	));
	}

	#*************************************************
	# Справочники тип 11
	#*************************************************

	#-------------------------------------------------------------------------
	# Интернет-провайдеры
	#-------------------------------------------------------------------------
	public function actionNetProvs(){
	/* Получим список городов для фильтра */
		$criteria = new CDbCriteria;
		$criteria->select = 'name';
		$criteria->order = 'name';
		$list_cities = CHtml::listData( City::model()->findAll($criteria), 'name', 'name');
		$selected = '--Выберите город--';
		array_unshift($list_cities, $selected);

	$vals = Yii::app()->user;
	if (isset($vals->dict_providers_city)) {
		$criteria = new CDbCriteria;
		$criteria->condition = 'city=:city_value';
		$criteria->params = array(':city_value'=>$vals->dict_providers_city);
		$criteria->order = 'id';
		$Rows = NetProvider::model()->findAll($criteria);
		$selected = $vals->dict_providers_city;
	} else $Rows = null;
	
	$this->render('type11', array(
		'Cities'	=>$list_cities,
		'sel_city'	=>$selected,
		'dicName'	=>'Интернет-провайдеры',
		'className'	=>'netprovs',
		'linkAdd'	=>'directories/netprovs_add',
		'nameAction'=>'providers',
		'rows'		=>$Rows
	));
	}

	public function actionNetProvs_add(){
		$form = new NetProvider();
		if (!empty($_POST['NetProvider'])){
		$form->attributes = $_POST['NetProvider'];
		if($form->validate()){
			Yii::app()->user->setState('dict_providers_city', $_POST['NetProvider']['city']);
			if ($form->save(false))
			$this->redirect(array('directories/netprovs'));
		}
		}

		/* Получим список городов для фильтра */
		$criteria = new CDbCriteria;
		$criteria->select = 'name';
		$criteria->order = 'name';
		$list_cities = CHtml::listData( City::model()->findAll($criteria), 'name', 'name');
		$def = Yii::app()->user->getState('dict_providers_city', 'none');

		$this->render('type11_add', array(
		'dicName'	=>'нового интернет-провайдера',
		'linkCancel'	=>'directories/netprovs',
		'modelName'	=>'NetProvider',
		'Cities'	=>$list_cities,
		'selCity'	=>$def,
		'form'		=>$form
	));
	}

	public function actionNetProvs_edit(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = NetProvider::model()->find($criteria);
		if (empty($Row))
		throw new CHttpException(404);

		$form = new NetProvider();
		if (!empty($_POST['NetProvider'])){
		$form->attributes = $_POST['NetProvider'];
		$form->city = Yii::app()->user->getState('dict_providers_city', '');
		if($form->validate()){
			$Row->attributes = $form->attributes;
			if ($Row->save(false))
			$this->redirect(array('directories/netprovs'));
		} else $Row->attributes = $form->attributes;
		}
		Yii::app()->user->setState('dict_providers_city', $Row->city);

		$this->render('type11_edit', array(
		'dicName'	=>'данные об интернет-провайдере',
		'linkCancel'	=>'directories/netprovs',
		'row'		=>$Row,
		'form'		=>$form
	));
	}

	public function actionNetProvs_delete(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		if (isset($_GET['flg'])){
		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = NetProvider::model()->find($criteria);
		if (empty($Row))
			throw new CHttpException(404);

		if ($Row->delete(false))
			$this->redirect(array('directories/netprovs'));
		}

		$this->render('type20_delete', array(
		'linkYes'	=>'directories/netprovs_delete',
		'linkNo'	=>'directories/netprovs',
		'id'		=>$id
	));
	}

	#-------------------------------------------------------------------------
	# Телефонные компании
	#-------------------------------------------------------------------------
	public function actionTelComp(){
	/* Получим список городов для фильтра */
		$criteria = new CDbCriteria;
		$criteria->select = 'name';
		$criteria->order = 'name';
		$list_cities = CHtml::listData( City::model()->findAll($criteria), 'name', 'name');
		$selected = '--Выберите город--';
		array_unshift($list_cities, $selected);

	$vals = Yii::app()->user;
	if (isset($vals->dict_telcompany_city)) {
		$criteria = new CDbCriteria;
		$criteria->condition = 'city=:city_value';
		$criteria->params = array(':city_value'=>$vals->dict_telcompany_city);
		$criteria->order = 'id';
		$Rows = TelCompany::model()->findAll($criteria);
		$selected = $vals->dict_telcompany_city;
	} else $Rows = null;
	
	$this->render('type11', array(
		'Cities'	=>$list_cities,
		'sel_city'	=>$selected,
		'dicName'	=>'Телефонные компании',
		'className'	=>'telcomp',
		'linkAdd'	=>'directories/telcomp_add',
		'nameAction'=>'telcompanies',
		'rows'		=>$Rows
	));
	}

	public function actionTelComp_add(){
		$form = new TelCompany();
		if (!empty($_POST['TelCompany'])){
		$form->attributes = $_POST['TelCompany'];
		if($form->validate()){
			Yii::app()->user->setState('dict_telcompany_city', $_POST['TelCompany']['city']);
			if ($form->save(false))
			$this->redirect(array('directories/telcomp'));
		}
		}

		/* Получим список городов для фильтра */
		$criteria = new CDbCriteria;
		$criteria->select = 'name';
		$criteria->order = 'name';
		$list_cities = CHtml::listData( City::model()->findAll($criteria), 'name', 'name');
		$def = Yii::app()->user->getState('dict_telcompany_city', 'none');

		$this->render('type11_add', array(
		'dicName'	=>'новую телефонную компанию',
		'linkCancel'	=>'directories/telcomp',
		'modelName'	=>'TelCompany',
		'Cities'	=>$list_cities,
		'selCity'	=>$def,
		'form'		=>$form
	));
	}

	public function actionTelComp_edit(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = TelCompany::model()->find($criteria);
		if (empty($Row))
		throw new CHttpException(404);

		$form = new TelCompany();
		if (!empty($_POST['TelCompany'])){
		$form->attributes = $_POST['TelCompany'];
		$form->city = Yii::app()->user->getState('dict_telcompany_city', '');
		if($form->validate()){
			$Row->attributes = $form->attributes;
			if ($Row->save(false))
			$this->redirect(array('directories/telcomp'));
		} else $Row->attributes = $form->attributes;
		}
		Yii::app()->user->setState('dict_telcompany_city', $Row->city);

		$this->render('type11_edit', array(
		'dicName'	=>'телефонную компанию',
		'linkCancel'=>'directories/telcomp',
		'row'		=>$Row,
		'form'		=>$form
	));
	}

	public function actionTelComp_delete(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		if (isset($_GET['flg'])){
		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = TelCompany::model()->find($criteria);
		if (empty($Row))
			throw new CHttpException(404);

		if ($Row->delete(false))
			$this->redirect(array('directories/telcomp'));
		}

		$this->render('type20_delete', array(
		'linkYes'	=>'directories/telcomp_delete',
		'linkNo'	=>'directories/telcomp',
		'id'		=>$id
	));
	}


	#*************************************************
	# Справочники тип 20
	#*************************************************

	#-------------------------------------------------------------------------
	# Классы объектов
	#-------------------------------------------------------------------------
	public function actionObjectClasses(){
		$criteria = new CDbCriteria;
		$criteria->order='id';
		$Rows = Classes::model()->findAll($criteria);

		$this->render('type20', array(
		'dicName'	=>'Классы объекта',
		'className'	=>'objectclasses',
		'linkAdd'	=>'directories/objectclasses_add',
		'Rows'		=>$Rows
	));
	}

	public function actionObjectClasses_add(){
		$form = new Classes();
		if (!empty($_POST['Classes'])){
		$form->attributes = $_POST['Classes'];
		if($form->validate()){
			if ($form->save(false))
			$this->redirect(array('directories/objectclasses'));
		}
		}

		$this->render('type20_add', array(
		'dicName'	=>'класс объекта',
		'linkCancel'	=>'directories/objectclasses',
		'form'		=>$form
	));
	}

	public function actionObjectClasses_edit(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = Classes::model()->find($criteria);
		if (empty($Row))
		throw new CHttpException(404);

		$form = new Classes();
		if (!empty($_POST['Classes'])){
		$form->attributes = $_POST['Classes'];
		if($form->validate()){
			$Row->attributes = $form->attributes;
			if ($Row->save(false))
			$this->redirect(array('directories/objectclasses'));
		}
		}

		$this->render('type20_edit', array(
		'dicName'	=>'класс объекта',
		'linkCancel'	=>'directories/objectclasses',
		'row'		=>$Row,
		'form'		=>$form
	));
	}

	public function actionObjectClasses_delete(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		if (isset($_GET['flg'])){
		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = Classes::model()->find($criteria);
		if (empty($Row))
			throw new CHttpException(404);

		if ($Row->delete(false))
			$this->redirect(array('directories/objectclasses'));
		}

		$this->render('type20_delete', array(
		'linkYes'	=>'directories/objectclasses_delete',
		'linkNo'	=>'directories/objectclasses',
		'id'		=>$id
	));
	}

	#-------------------------------------------------------------------------
	# Формы договоров
	#-------------------------------------------------------------------------
	public function actionContractForm(){
		$criteria = new CDbCriteria;
		$criteria->order='id';
		$Rows = ContrForms::model()->findAll($criteria);

		$this->render('type20', array(
		'dicName'	=>'Формы договоров',
		'className'	=>'contractform',
		'linkAdd'	=>'directories/contractform_add',
		'Rows'		=>$Rows
	));
	}

	public function actionContractForm_add(){
		$form = new ContrForms();
		if (!empty($_POST['ContrForms'])){
		$form->attributes = $_POST['ContrForms'];
		if($form->validate()){
			if ($form->save(false))
			$this->redirect(array('directories/contractform'));
		}
		}

		$this->render('type20_add', array(
		'dicName'	=>'новую форму договора',
		'linkCancel'	=>'directories/contractform',
		'form'		=>$form
	));
	}

	public function actionContractForm_edit(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = ContrForms::model()->find($criteria);
		if (empty($Row))
		throw new CHttpException(404);

		$form = new ContrForms();
		if (!empty($_POST['ContrForms'])){
		$form->attributes = $_POST['ContrForms'];
		if($form->validate()){
			$Row->attributes = $form->attributes;
			if ($Row->save(false))
			$this->redirect(array('directories/contractform'));
		}
		}

		$this->render('type20_edit', array(
		'dicName'	=>'форму договора',
		'linkCancel'	=>'directories/contractform',
		'row'		=>$Row,
		'form'		=>$form
	));
	}

	public function actionContractForm_delete(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		if (isset($_GET['flg'])){
		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = ContrForms::model()->find($criteria);
		if (empty($Row))
			throw new CHttpException(404);

		if ($Row->delete(false))
			$this->redirect(array('directories/contractform'));
		}

		$this->render('type20_delete', array(
		'linkYes'	=>'directories/contractform_delete',
		'linkNo'	=>'directories/contractform',
		'id'		=>$id
	));
	}

	#-------------------------------------------------------------------------
	# Виды покрытия пола
	#-------------------------------------------------------------------------
	public function actionFlooring(){
		$criteria = new CDbCriteria;
		$criteria->order='id';
		$Rows = Flooring::model()->findAll($criteria);

		$this->render('type20', array(
		'dicName'	=>'Виды покрытия пола',
		'className'	=>'flooring',
		'linkAdd'	=>'directories/flooring_add',
		'Rows'		=>$Rows
	));
	}

	public function actionFlooring_add(){
		$form = new Flooring();
		if (!empty($_POST['Flooring'])){
		$form->attributes = $_POST['Flooring'];
		if($form->validate()){
			if ($form->save(false))
			$this->redirect(array('directories/flooring'));
		}
		}

		$this->render('type20_add', array(
		'dicName'	=>'новый вид покрытия пола',
		'linkCancel'	=>'directories/flooring',
		'form'		=>$form
	));
	}

	public function actionFlooring_edit(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = Flooring::model()->find($criteria);
		if (empty($Row))
		throw new CHttpException(404);

		$form = new Flooring();
		if (!empty($_POST['Flooring'])){
		$form->attributes = $_POST['Flooring'];
		if($form->validate()){
			$Row->attributes = $form->attributes;
			if ($Row->save(false))
			$this->redirect(array('directories/flooring'));
		}
		}

		$this->render('type20_edit', array(
		'dicName'	=>'вид покрытия пола',
		'linkCancel'	=>'directories/flooring',
		'row'		=>$Row,
		'form'		=>$form
	));
	}

	public function actionFlooring_delete(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		if (isset($_GET['flg'])){
		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = Flooring::model()->find($criteria);
		if (empty($Row))
			throw new CHttpException(404);

		if ($Row->delete(false))
			$this->redirect(array('directories/flooring'));
		}

		$this->render('type20_delete', array(
		'linkYes'	=>'directories/flooring_delete',
		'linkNo'	=>'directories/flooring',
		'id'		=>$id
	));
	}

	#-------------------------------------------------------------------------
	# Функциональное назначение помещений (объектов)
	#-------------------------------------------------------------------------
	public function actionFunctional(){
		$criteria = new CDbCriteria;
		$criteria->order='id';
		$Rows = Functionality::model()->findAll($criteria);

		$this->render('type20', array(
		'dicName'	=>'Функциональное назначение помещений (объектов)',
		'className'	=>'functional',
		'linkAdd'	=>'directories/functional_add',
		'Rows'		=>$Rows
	));
	}

	public function actionFunctional_add(){
		$form = new Functionality();
		if (!empty($_POST['Functionality'])){
		$form->attributes = $_POST['Functionality'];
		if($form->validate()){
			if ($form->save(false))
			$this->redirect(array('directories/functional'));
		}
		}

		$this->render('type20_add', array(
		'dicName'	=>'новое функциональное назначение',
		'linkCancel'	=>'directories/functional',
		'form'		=>$form
	));
	}

	public function actionFunctional_edit(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = Functionality::model()->find($criteria);
		if (empty($Row))
		throw new CHttpException(404);

		$form = new Functionality();
		if (!empty($_POST['Functionality'])){
		$form->attributes = $_POST['Functionality'];
		if($form->validate()){
			$Row->attributes = $form->attributes;
			if ($Row->save(false))
			$this->redirect(array('directories/functional'));
		}
		}

		$this->render('type20_edit', array(
		'dicName'	=>'функциональное назначение',
		'linkCancel'	=>'directories/functional',
		'row'		=>$Row,
		'form'		=>$form
	));
	}

	public function actionFunctional_delete(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		if (isset($_GET['flg'])){
		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = Functionality::model()->find($criteria);
		if (empty($Row))
			throw new CHttpException(404);

		if ($Row->delete(false))
			$this->redirect(array('directories/functional'));
		}

		$this->render('type20_delete', array(
		'linkYes'	=>'directories/functional_delete',
		'linkNo'	=>'directories/functional',
		'id'		=>$id
	));
	}

	#-------------------------------------------------------------------------
	# Типы сооружений
	#-------------------------------------------------------------------------
	public function actionKindsStruc(){
		$criteria = new CDbCriteria;
		$criteria->order='id';
		$Rows = KindStructure::model()->findAll($criteria);

		$this->render('type20', array(
		'dicName'	=>'Типы сооружений',
		'className'	=>'kindsstruc',
		'linkAdd'	=>'directories/kindsstruc_add',
		'Rows'		=>$Rows
	));
	}

	public function actionKindsStruc_add(){
		$form = new KindStructure();
		if (!empty($_POST['KindStructure'])){
		$form->attributes = $_POST['KindStructure'];
		if($form->validate()){
			if ($form->save(false))
			$this->redirect(array('directories/kindsstruc'));
		}
		}

		$this->render('type20_add', array(
		'dicName'	=>'новый тип сооружений',
		'linkCancel'	=>'directories/kindsstruc',
		'form'		=>$form
	));
	}

	public function actionKindsStruc_edit(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = KindStructure::model()->find($criteria);
		if (empty($Row))
		throw new CHttpException(404);

		$form = new KindStructure();
		if (!empty($_POST['KindStructure'])){
		$form->attributes = $_POST['KindStructure'];
		if($form->validate()){
			$Row->attributes = $form->attributes;
			if ($Row->save(false))
			$this->redirect(array('directories/kindsstruc'));
		}
		}

		$this->render('type20_edit', array(
		'dicName'	=>'тип сооружений',
		'linkCancel'	=>'directories/kindsstruc',
		'row'		=>$Row,
		'form'		=>$form
	));
	}

	public function actionKindsStruc_delete(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		if (isset($_GET['flg'])){
		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = KindStructure::model()->find($criteria);
		if (empty($Row))
			throw new CHttpException(404);

		if ($Row->delete(false))
			$this->redirect(array('directories/kindsstruc'));
		}

		$this->render('type20_delete', array(
		'linkYes'	=>'directories/kindsstruc_delete',
		'linkNo'	=>'directories/kindsstruc',
		'id'		=>$id
	));
	}


	#-------------------------------------------------------------------------
	# Виды паркинга
	#-------------------------------------------------------------------------
	public function actionParking(){
		$criteria = new CDbCriteria;
		$criteria->order='id';
		$pages = new CPagination(Parking::model()->count($criteria));
		$pages->pageSize = '3';
		$pages->applyLimit($criteria);

		$Rows = Parking::model()->findAll($criteria);

		$this->render('type20_pages', array(
		'dicName'	=>'Виды паркинга',
		'className'	=>'parking',
		'linkAdd'	=>'directories/parking_add',
		'Rows'		=>$Rows,
		'pages'	=>$pages
	));
	}

	public function actionParking_add(){
		$form = new Parking();
		if (!empty($_POST['Parking'])){
		$form->attributes = $_POST['Parking'];
		if($form->validate()){
			if ($form->save(false))
			$this->redirect(array('directories/parking'));
		}
		}

		$this->render('type20_add', array(
		'dicName'	=>'новый вид паркинга',
		'linkCancel'	=>'directories/parking',
		'form'		=>$form
	));
	}

	public function actionParking_edit(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = Parking::model()->find($criteria);
		if (empty($Row))
		throw new CHttpException(404);

		$form = new Parking();
		if (!empty($_POST['Parking'])){
		$form->attributes = $_POST['Parking'];
		if($form->validate()){
			$Row->attributes = $form->attributes;
			if ($Row->save(false))
			$this->redirect(array('directories/parking'));
		}
		}

		$this->render('type20_edit', array(
		'dicName'	=>'вид паркинга',
		'linkCancel'	=>'directories/parking',
		'row'		=>$Row,
		'form'		=>$form
	));
	}

	public function actionParking_delete(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		if (isset($_GET['flg'])){
		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = Parking::model()->find($criteria);
		if (empty($Row))
			throw new CHttpException(404);

		if ($Row->delete(false))
			$this->redirect(array('directories/parking'));
		}

		$this->render('type20_delete', array(
		'linkYes'	=>'directories/parking_delete',
		'linkNo'	=>'directories/parking',
		'id'		=>$id
	));
	}

	#-------------------------------------------------------------------------
	# Условия платежей по коммунальным услугам
	#-------------------------------------------------------------------------
	public function actionPayUtils(){
		$criteria = new CDbCriteria;
		$criteria->order='id';
		$Rows = PayUtil::model()->findAll($criteria);

		$this->render('type20', array(
		'dicName'	=>'Особенности при платежах коммунальных услуг',
		'className'	=>'payutils',
		'linkAdd'	=>'directories/payutils_add',
		'Rows'		=>$Rows
	));
	}

	public function actionPayUtils_add(){
		$form = new PayUtil();
		if (!empty($_POST['PayUtil'])){
		$form->attributes = $_POST['PayUtil'];
		if($form->validate()){
			if ($form->save(false))
			$this->redirect(array('directories/payutils'));
		}
		}

		$this->render('type20_add', array(
		'dicName'	=>'новую запись',
		'linkCancel'	=>'directories/payutils',
		'form'		=>$form
	));
	}

	public function actionPayUtils_edit(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = PayUtil::model()->find($criteria);
		if (empty($Row))
		throw new CHttpException(404);

		$form = new PayUtil();
		if (!empty($_POST['PayUtil'])){
		$form->attributes = $_POST['PayUtil'];
		if($form->validate()){
			$Row->attributes = $form->attributes;
			if ($Row->save(false))
			$this->redirect(array('directories/payutils'));
		}
		}

		$this->render('type20_edit', array(
		'dicName'	=>'значения',
		'linkCancel'	=>'directories/payutils',
		'row'		=>$Row,
		'form'		=>$form
	));
	}

	public function actionPayUtils_delete(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		if (isset($_GET['flg'])){
		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = PayUtil::model()->find($criteria);
		if (empty($Row))
			throw new CHttpException(404);

		if ($Row->delete(false))
			$this->redirect(array('directories/payutils'));
		}

		$this->render('type20_delete', array(
		'linkYes'	=>'directories/payutils_delete',
		'linkNo'	=>'directories/payutils',
		'id'		=>$id
	));
	}

	#-------------------------------------------------------------------------
	# Состояния ремонта
	#-------------------------------------------------------------------------
	public function actionRepair(){
		$criteria = new CDbCriteria;
		$criteria->order='id';
		$Rows = Repair::model()->findAll($criteria);

		$this->render('type20', array(
		'dicName'	=>'Состояния ремонта',
		'className'	=>'repair',
		'linkAdd'	=>'directories/repair_add',
		'Rows'		=>$Rows
	));
	}

	public function actionRepair_add(){
		$form = new Repair();
		if (!empty($_POST['Repair'])){
		$form->attributes = $_POST['Repair'];
		if($form->validate()){
			if ($form->save(false))
			$this->redirect(array('directories/repair'));
		}
		}

		$this->render('type20_add', array(
		'dicName'	=>'новый вид состояния ремонта',
		'linkCancel'	=>'directories/repair',
		'form'		=>$form
	));
	}

	public function actionRepair_edit(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = Repair::model()->find($criteria);
		if (empty($Row))
		throw new CHttpException(404);

		$form = new Repair();
		if (!empty($_POST['Repair'])){
		$form->attributes = $_POST['Repair'];
		if($form->validate()){
			$Row->attributes = $form->attributes;
			if ($Row->save(false))
			$this->redirect(array('directories/repair'));
		}
		}

		$this->render('type20_edit', array(
		'dicName'	=>'состояние ремонта',
		'linkCancel'	=>'directories/repair',
		'row'		=>$Row,
		'form'		=>$form
	));
	}

	public function actionRepair_delete(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		if (isset($_GET['flg'])){
		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = Repair::model()->find($criteria);
		if (empty($Row))
			throw new CHttpException(404);

		if ($Row->delete(false))
			$this->redirect(array('directories/repair'));
		}

		$this->render('type20_delete', array(
		'linkYes'	=>'directories/repair_delete',
		'linkNo'	=>'directories/repair',
		'id'		=>$id
	));
	}

	#-------------------------------------------------------------------------
	# Системы вентиляции
	#-------------------------------------------------------------------------
	public function actionVentilation(){
		$criteria = new CDbCriteria;
		$criteria->order='id';
		$Rows = Ventilation::model()->findAll($criteria);

		$this->render('type20', array(
		'dicName'	=>'Системы вентиляции',
		'className'	=>'ventilation',
		'linkAdd'	=>'directories/ventilation_add',
		'Rows'		=>$Rows
	));
	}

	public function actionVentilation_add(){
		$form = new Ventilation();
		if (!empty($_POST['Ventilation'])){
		$form->attributes = $_POST['Ventilation'];
		if($form->validate()){
			if ($form->save(false))
			$this->redirect(array('directories/ventilation'));
		}
		}

		$this->render('type20_add', array(
		'dicName'	=>'новую систему вентиляции',
		'linkCancel'	=>'directories/ventilation',
		'form'		=>$form
	));
	}

	public function actionVentilation_edit(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = Ventilation::model()->find($criteria);
		if (empty($Row))
		throw new CHttpException(404);

		$form = new Ventilation();
		if (!empty($_POST['Ventilation'])){
		$form->attributes = $_POST['Ventilation'];
		if($form->validate()){
			$Row->attributes = $form->attributes;
			if ($Row->save(false))
			$this->redirect(array('directories/ventilation'));
		}
		}

		$this->render('type20_edit', array(
		'dicName'	=>'систему вентиляции',
		'linkCancel'	=>'directories/ventilation',
		'row'		=>$Row,
		'form'		=>$form
	));
	}

	public function actionVentilation_delete(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		if (isset($_GET['flg'])){
		$criteria = new CDbCriteria;
		$criteria->condition = 'id=:var';
		$criteria->params = array(':var'=>$id);
		$Row = Ventilation::model()->find($criteria);
		if (empty($Row))
			throw new CHttpException(404);

		if ($Row->delete(false))
			$this->redirect(array('directories/ventilation'));
		}

		$this->render('type20_delete', array(
		'linkYes'	=>'directories/ventilation_delete',
		'linkNo'	=>'directories/ventilation',
		'id'		=>$id
	));
	}


	#*************************************************
	# Справочники тип 30
	#*************************************************

	#-------------------------------------------------------------------------
	# Города
	#-------------------------------------------------------------------------
	public function actionCity(){
		$criteria = new CDbCriteria;
		$criteria->order='name';
		$Rows = City::model()->findAll($criteria);

		$this->render('type30', array(
			'dicName'	=>'Города',
			'className'	=>'city',
			'linkAdd'	=>'directories/city_add',
			'Rows'		=>$Rows
		));
	}

	//Объединил редактирование и созд. города в одну view, поэтому тут переименовал form в model
	public function actionCity_add(){
		$model = new City();
		if (!empty($_POST['City'])){
			$model->attributes = $_POST['City'];
			if($model->validate()){
				if ($model->save(false))
					$this->redirect(array('directories/city'));
			}
		}

		$this->render('type30_add', array(
			'dicName'	=>'город',
			'linkCancel'	=>'directories/city',
			'model'		=>$model
		));
	}

	public function actionCity_edit(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$pk = $get->id_out;

		$criteria = new CDbCriteria;
		$criteria->condition = 'name=:var';
		$criteria->params = array(':var'=>$pk);

		$model = City::model()->find($criteria);
		if (empty($model))
			throw new CHttpException(404);

		if (!empty($_POST['City'])){
			$model->attributes = $_POST['City'];
			if($model->validate()){
				if ($model->save(false))
					$this->redirect(array('directories/city'));
			}
		}

		$this->render('type30_edit', array(
			'dicName'	=>'город',
			'linkCancel'=>'directories/city',
			'model'		=>$model
		));
	}

	public function actionCity_delete(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$pk = $get->id_out;

		if (isset($_GET['flg'])){
			$criteria = new CDbCriteria;
			$criteria->condition = 'name=:var';
			$criteria->params = array(':var'=>$pk);
			$Row = City::model()->find($criteria);
			if (empty($Row))
				throw new CHttpException(404);

			if ($Row->delete(false))
				$this->redirect(array('directories/city'));
		}

		$this->render('type30_delete', array(
			'linkYes'	=>'directories/city_delete',
			'linkNo'	=>'directories/city',
			'key'		=>$pk
		));
	}

}
?>