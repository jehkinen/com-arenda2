<?php class NewsadminController extends CController{

	public $layout = 'control';
	public $defaultAction = 'index';
	
	public function actionIndex(){
		$criteria = new CDbCriteria;
		$criteria->order = 'created desc';
		$qty = News::model()->count($criteria);
		$pages = new CPagination($qty);
		$pages->pageSize = 25;
		$pages->applyLimit($criteria);
		$Rows = News::model()->findAll($criteria);
		$this->render('index', array(
			'rows' =>$Rows,
			'total'	=>$qty,
			'pages'	=>$pages
		));
	}

	public function actionAdd(){
		$fNew = new News();
		$newText = '';
		if (!empty($_POST['News'])){
			$fNew->attributes = $_POST['News'];
			$newText = $fNew->body;
			$fNew->image = CUploadedFile::getInstance($fNew,'image');
			if($fNew->validate()){
				if (!empty($fNew->image)) {
					$translit_obj = $this->widget('LoadFileName', array('in_text'=>$fNew->image->name, 'len'=>200));
					$translit = $translit_obj->out_text;
					$name = substr($translit, 0, strrpos($translit, '.'));
					$ext = substr($translit, strrpos($translit, '.'));
					$random = mt_rand(10000, 99999);
					$fullname = $name.'_'.$random.$ext;
					$smallname = $name.'_'.$random.'_mini'.$ext;
					if ($fNew->image->saveAs('storage/images/news/'.$fullname)){
						$fNew->big_pic = $fullname;
						$fNew->small_pic = $smallname;
						$resizer = $this->widget('ImgResize', array('src'=>'storage/images/news/'.$fullname, 'dest'=>'storage/images/news/'.$smallname, 'width'=>99, 'height'=>3000));
					}
				}
				if ($fNew->save(false))
					$this->redirect(array('newsadmin/index'));
			}
		}
		$this->render('add', array('form' =>$fNew, 'txt'=>$newText));
	}

	public function actionEdit() {
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$oNew = News::model()->findByPK($get->id_out);
		if (empty($oNew))
			throw new CHttpException(404);
		if (!empty($_POST['News'])){
			$oNew->attributes = $_POST['News'];
			$oNew->image = CUploadedFile::getInstance($oNew,'image');
			if($oNew->validate()){
				if (!empty($oNew->image)) {
					$translit_obj = $this->widget('LoadFileName', array('in_text'=>$oNew->image->name, 'len'=>200));
					$translit = $translit_obj->out_text;
					$name = substr($translit, 0, strrpos($translit, '.'));
					$ext = substr($translit, strrpos($translit, '.'));
					$random = mt_rand(10000, 99999);
					$fullname = $name.'_'.$random.$ext;
					$smallname = $name.'_'.$random.'_mini'.$ext;
					if ($oNew->image->saveAs('storage/images/news/'.$fullname)){
						$oNew->big_pic = $fullname;
						$oNew->small_pic = $smallname;
						$resizer = $this->widget('ImgResize', array('src'=>'storage/images/news/'.$fullname, 'dest'=>'storage/images/news/'.$smallname, 'width'=>99, 'height'=>3000));
					}
				}
				if ($oNew->save(false))
					$this->redirect(array('newsadmin/index'));
			}
		}
		$this->render('edit', array('form' =>$oNew,
			'oHeader'	=>$oNew->header,
			'oStick'	=>$oNew->stick,
			'oKeys'		=>$oNew->keywords,
			'oDescr'	=>$oNew->description,
			'oAnonce'	=>$oNew->anounce,
			'oBody'		=>$oNew->body
		));
	}
	
	public function actionFotodelete() {
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$oNew = News::model()->findByPK($get->id_out);
		if (empty($oNew))
			throw new CHttpException(404);
		$s = Yii::app()->params->web_root;
		$i = strrpos($s, '/'); $a = strlen($s)-1;
		if ($i != $a)	$s.='/';
		$s.='storage/images/news/';
		@unlink ($s.$oNew->big_pic);
		@unlink ($s.$oNew->small_pic);
		$oNew->big_pic = new CDbExpression('NULL');
		$oNew->small_pic = new CDbExpression('NULL');
		$oNew->save(false);
		$this->redirect(array('newsadmin/edit/id/'.$oNew->id));
	}

	public function actionDelete() {
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$New = News::model()->findByPK($get->id_out);
		if (empty($New))
			throw new CHttpException(404);
		if (isset($_GET['flg'])){
			if (isset($New->small_pic)) {
				$s = Yii::app()->params->web_root;
				$i = strrpos($s, '/'); $a = strlen($s)-1;
				if ($i != $a)	$s.='/';
				$s.='storage/images/news/';
				@unlink ($s.$New->big_pic);
				@unlink ($s.$New->small_pic);
			}
			if ($New->delete(false))
				$this->redirect(array('newsadmin/index'));
		}
		$this->render('delete', array('id' =>$get->id_out));
	}

	public function filters(){
		return array(
			array(
				'application.filters.AccessFilter',
				'role'=>'admin'
			),
		);
	}

}?>