<?php

class Object2Controller extends CController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	//public $layout='//layouts/column2';
	public $layout = 'index03';
	public $pageDescription;
	public $pageKeywords;
	//public $cssFiles = array('jqtransform');
	//public $jsFiles = array('jquery.jqtransform', 'jquery2');
	public $list_districts;
	public $ses_vals;

	/**
	 * @return array action filters
	 */

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','edit', 'ajaxUpdate'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionajaxUpdate(){

		if(Yii::app()->request->isAjaxRequest){
			if($_GET['city']){
				$city = $_GET['city'];
				$criteria = new CDbCriteria;
				$criteria->order = 'name';
				$criteria->condition = 'city=:c';
				$criteria->params = array(':c'=>$city);
				$districts = CHtml::listData(District::model()->findAll($criteria), 'id', 'name');
				$streets = CHtml::listData(Street::model()->findAll($criteria), 'id', 'name');

                $net_providers =  CHtml::listData(NetProvider::model()->findAll($criteria), 'id', 'shname');
				$tel_providers = CHtml::listData(TelCompany::model()->findAll($criteria), 'id', 'shname');

				$jsonResponse['districts'] = $this->renderPartial('//layouts/blocks/_list', array('list'=>$districts, 'param'=> 'район'), true, true);
				$jsonResponse['streets'] = $this->renderPartial('//layouts/blocks/_list', array('list'=>$streets, 'param'=> 'улицу'), true, true);
			}
			else {
				$jsonResponse['districts'] = '<option value="">Выберите район</option>';
				$jsonResponse['streets'] = '<option value="">Выберите улицу</option>';
			}
			echo CJSON::encode($jsonResponse);
		}
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Object2;
		$model->city = Yii::app()->user->getState('glcity'); //Город из шапки

		// Uncomment the following line if AJAX validation is needed
		if(Yii::app()->getRequest()->getIsAjaxRequest())
		{
			echo CActiveForm::validate( array( $model));
			Yii::app()->end();
		}


		if(isset($_POST['Object2'])){
			ini_set('memory_limit', '64M');
			ini_set('upload_max_filesize', '6M');

			$model->attributes=$_POST['Object2'];

			//Статус. 0-деактивирован, 3-модерируется, 6-показывается
			$model->status = 3;   // При добавлении нового объекта выставляем ему статус "ожидает модерации"
			$model->owner_id = (Yii::app()->user->isGuest)?'0':Yii::app()->user->id['user_id'];   // При добавлении нового объекта выставляем ему статус "ожидает модерации"

			/*Загрузка документов*/
			$model->docums[] = CUploadedFile::getInstance($model,'[1]docums');
			$model->docums[] = CUploadedFile::getInstance($model,'[2]docums');
			$model->docums[] = CUploadedFile::getInstance($model,'[3]docums');

			/*Заполняем имена из массива POST*/
			foreach($model->docums as $i=>$doc){
				if($doc!=NULL){
					$model->docname[] = trim($_POST['Object2'][$i+1]['docname']);
				}
			}

		    if($model->save()){
				if (!empty($model->docums)) {
					$docs = $model->docums;
					$names= $model->docname;

					for ($i=0;$i<3;$i++) {

						if (!empty($docs[$i])) {
							$translit_obj = $this->widget('LoadFileName', array('in_text'=>$docs[$i]->name, 'len'=>200));

							$translit = $translit_obj->out_text;
							$f_name = substr($translit, 0, strrpos($translit, '.'));
							$f_ext = substr($translit, strrpos($translit, '.'));
							$random = mt_rand(10000, 99999);
							$name = $f_name.'_'.$random.$f_ext;
							if ($docs[$i]->saveAs('storage/files/objs/'.$name)) {
								$rec = new Attach;
								$rec->item = $model->id;
								$rec->name = $names[$i];
								$rec->filename = $name;
								$rec->save(false);
							}
						}
					}
				}
		    $this->widget('Clear_session');
			$this->redirect(array('object/adding/id/'.$model->id));
			}

		   else{
			   $this->render('create',array(
				   'model'=>$model,
			   ));
		   }
		}
		else{
			$this->render('create',array(
				'model'=>$model,
			));
		}
	}

	public function actionEdit($id)
	{

		//$this->pageTitle = $news->header;
		///$this->pageDescription = $news->description;
		//$this->pageKeywords = $news->keywords;

		$model=$this->loadModel($id);

		if(Yii::app()->getRequest()->getIsAjaxRequest())
		{
			echo CActiveForm::validate( array( $model));
			Yii::app()->end();
		}



		if(isset($_POST['Object2']))
		{

			$model->attributes=$_POST['Object2'];
			$model->status = 3;   // При добавлении нового объекта выставляем ему статус "ожидает модерации"
			$model->owner_id = (Yii::app()->user->isGuest)?'0':Yii::app()->user->id['user_id'];   // При добавлении нового объекта выставляем ему статус "ожидает модерации"



			if($model->save())
				$this->redirect(array('object/adding/id/'.$model->id));
				//$this->redirect(array('view','id'=>$model->id));
		}
		/*Загрузка фотографий*/
		$criteria = new CDbCriteria;
		$criteria->alias = 'I';
		$criteria->select = 'I.id, I.parent, I.itemType, I.kind, I.owner_id, I.city, COALESCE(I.name, "") as name, COALESCE(S.name, "не указана") as street, COALESCE(I.house, "дом не указан") as house, COALESCE(I.storey, "не указан") as storey, COALESCE(II.name, "<без названия>") as parentName';
		$criteria->join = 'left join streets S on S.id=I.street '.
			'left join items II on II.id=I.parent ';
		$criteria->condition = 'I.id=:id';
		$criteria->params = array(':id'=>$id);
		$row = ItemDetail::model()->find($criteria);

		$criteria = new CDbCriteria;
		$criteria->condition = 'id_item=:ii';
		$criteria->params = array(':ii'=>$id);
		$criteria->order = 'id asc';
		$fotos=array();
		$fotos = Images::model()->findAll($criteria);
		//$this->redirect(array('object/adding/id/'.$form->id));
		$this->render('edit',array(
			'model'=>$model,
			'fotos'	=>	$fotos,
			'row'=>$row,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Object2');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Object2('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Object2']))
			$model->attributes=$_GET['Object2'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Object2::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='object-new-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
