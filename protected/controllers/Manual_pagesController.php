<?php class Manual_pagesController extends CController{

	public $layout = 'inner02';
	public $defaultAction = 'view';
	public $pageClass = 'any';
	public $pageDescription;
	public $pageKeywords;

	public function actionHowwork() {
		$this->pageTitle = 'Как работает com-arenda';
		$this->pageDescription = '';
		$this->pageKeywords = '';
		$this->pageClass = '';
		$this->layout = 'one-column02';
		$this->render('how-work');
	}
	public function actionTest() {

		$this->pageClass = '';
		$this->layout = 'inner02';
		$this->render('test');
	}

	/*страница: site/page/agent.html*/
	public function actionFor_estate_agency() {
		$this->pageTitle = 'Агенствам недвижимости';
		$this->pageDescription = '';
		$this->pageKeywords = '';
		$this->pageClass = '';
		$this->layout = 'one-column02';
		$model = new Partnership;
		$this->render('for_estate_agency', array('model'=>$model));
	}



}?>