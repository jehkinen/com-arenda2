<?php class AjaxdictsController extends CController{
	public function actionSelect_city(){

		if (!isset($_POST['id']) || !isset($_POST['clName']))
			throw new CHttpException(404);

		if ($_POST['id'] == ''){
			echo '';
			CApplication::end();
		}

		$select_city = $_POST['id'];
		$clName = $_POST['clName'];

		$criteria = new CDbCriteria;
		$criteria->condition = 'city=:city_value';
		$criteria->params = array(':city_value'=>$select_city);
		$criteria->order = 'name';

		$totalQty = District::model()->count($criteria);
		$qtyOnPage = 25;
		$showPager = ($totalQty > $qtyOnPage);

		Yii::app()->user->setState('dict_districs_city', $select_city);
		if ($showPager) {
			$pages = new CPagination($totalQty);
			$pages->pageSize = $qtyOnPage;
			$pages->route = 'Directories/districts';
			$pages->applyLimit($criteria);
		}
		$Districts_obj = District::model()->findAll($criteria);

		if (!empty($Districts_obj)){
			echo '<table class="sample">
				<col width="35%" />
				<col width="45%" />
				<col width="10%" />
				<col width="10%" />
					<tr>
					<th>Город</th>
					<th>Наименование</th>
					<th>Ред.</th>
					<th>Уд.</th>
					</tr>';
		foreach ($Districts_obj as $object){
			echo '<tr>
					<td>'.$object->city.'</td>
					<td>'.$object->name.'</td>
					<td class="action">'.CHtml::link('E', array('directories/'.$clName.'_edit', 'id'=>$object->id)).'</td>
					<td class="action">'.CHtml::link('D', array('directories/'.$clName.'_delete', 'id'=>$object->id)).'</td>
				</tr>';
		}
		echo '</table>'."\n";
		if ($showPager) {
			echo '<br />
			<div align="center">'."\n";
			$this->widget('CLinkPager',array(
				'pages'			=>$pages,
				'maxButtonCount' =>5, # максимальное колличество вкладок на странице
				'header'		 =>'<b>Перейти к странице:</b><br>', # заголовок
				'nextPageLabel'  =>'Вперед&gt;', # название кнопок навигаций next и prev
				'prevPageLabel'  =>'&lt;Назад',
			));
			echo "\n".'</div>';
		}
		}else
			echo 'пусто';

	}

	public function actionSelect_street(){

		if (!isset($_POST['id']) || !isset($_POST['clName']))
			throw new CHttpException(404);

		if ($_POST['id'] == ''){
			echo '';
			CApplication::end();
		}

		$select_city = $_POST['id'];
		$clName = $_POST['clName'];

		$criteria = new CDbCriteria;
		$criteria->condition = 'city=:city_value';
		$criteria->params = array(':city_value'=>$select_city);
		$criteria->order = 'name';

		$totalQty = Street::model()->count($criteria);
		$qtyOnPage = 25;
		$showPager = ($totalQty > $qtyOnPage);

		Yii::app()->user->setState('dict_street_city', $select_city);
		if ($showPager) {
			$pages = new CPagination($totalQty);
			$pages->pageSize = $qtyOnPage;
			$pages->route = 'Directories/street';
			$pages->applyLimit($criteria);
		}
		$Street_obj = Street::model()->findAll($criteria);

		if (!empty($Street_obj)){
			echo '<table class="sample">
				<col width="35%" />
				<col width="45%" />
				<col width="10%" />
				<col width="10%" />
					<tr>
					<th>Город</th>
					<th>Наименование</th>
					<th>Ред.</th>
					<th>Уд.</th>
					</tr>';
		foreach ($Street_obj as $object){
			echo '<tr>
					<td>'.$object->city.'</td>
					<td>'.$object->name.'</td>
					<td class="action">'.CHtml::link('E', array('directories/'.$clName.'_edit', 'id'=>$object->id)).'</td>
					<td class="action">'.CHtml::link('D', array('directories/'.$clName.'_delete', 'id'=>$object->id)).'</td>
				</tr>';
		}
		echo '</table>'."\n";
		if ($showPager) {
			echo '<br />
			<div align="center">'."\n";
			$this->widget('CLinkPager',array(
				'pages'				=>$pages,
				'maxButtonCount'	=>5, # максимальное колличество вкладок на странице
				'header'			=>'<b>Перейти к странице:</b><br>', # заголовок
				'nextPageLabel'		=>'Вперед&gt;', # название кнопок навигаций next и prev
				'prevPageLabel'		=>'&lt;Назад',
			));
			echo "\n".'</div>';
		}
		}else
			echo 'пусто';

	}

	public function actionProviders(){		if (!isset($_POST['id']) || !isset($_POST['clName']))
			throw new CHttpException(404);

		if ($_POST['id'] == ''){			echo '';
			CApplication::end();		}

		$select_city = $_POST['id'];
		$clName = $_POST['clName'];

		$criteria = new CDbCriteria;
		$criteria->condition = 'city=:city_value';
		$criteria->params = array(':city_value'=>$select_city);
		$criteria->order = 'name';

		$Provider_obj = NetProvider::model()->findAll($criteria);
		Yii::app()->user->setState('dict_providers_city', $select_city);

		if (!empty($Provider_obj)){			echo '<table class="sample">
					<col width="30%" />
					<col width="40%" />
					<col width="20%" />
					<col width="5%" />
					<col width="5%" />
					  <tr>
						<th>Город</th>
						<th>Наименование</th>
						<th>Название кратко</th>
						<th>Ред.</th>
						<th>Уд.</th>
					  </tr>';
		  foreach ($Provider_obj as $object){			echo '<tr>
					<td>'.$object->city.'</td>
					<td>'.$object->name.'</td>
					<td>'.$object->shname.'</td>
					<td class="action">'.CHtml::link('E', array('directories/'.$clName.'_edit', 'id'=>$object->id)).'</td>
					<td class="action">'.CHtml::link('D', array('directories/'.$clName.'_delete', 'id'=>$object->id)).'</td>
					</tr>';		}
		echo '</table>'."\n";
		}else
			echo 'пусто';
	}

	public function actionTelcompanies(){
		if (!isset($_POST['id']) || !isset($_POST['clName']))
			throw new CHttpException(404);

		if ($_POST['id'] == ''){
			echo '';
			CApplication::end();
		}

		$select_city = $_POST['id'];
		$clName = $_POST['clName'];

		$criteria = new CDbCriteria;
		$criteria->condition = 'city=:city_value';
		$criteria->params = array(':city_value'=>$select_city);
		$criteria->order = 'name';

		$TelCompany_obj = TelCompany::model()->findAll($criteria);
		Yii::app()->user->setState('dict_telcompany_city', $select_city);

		if (!empty($TelCompany_obj)){
			echo '<table class="sample">
					<col width="30%" />
					<col width="40%" />
					<col width="20%" />
					<col width="5%" />
					<col width="5%" />
					<tr>
						<th>Город</th>
						<th>Наименование</th>
						<th>Название кратко</th>
						<th>Ред.</th>
						<th>Уд.</th>
					</tr>';
		  foreach ($TelCompany_obj as $object){
			echo '<tr>
					<td>'.$object->city.'</td>
					<td>'.$object->name.'</td>
					<td>'.$object->shname.'</td>
					<td class="action">'.CHtml::link('E', array('directories/'.$clName.'_edit', 'id'=>$object->id)).'</td>
					<td class="action">'.CHtml::link('D', array('directories/'.$clName.'_delete', 'id'=>$object->id)).'</td>
					</tr>';
		}
		echo '</table>'."\n";
		}else
			echo 'пусто';
	}
}
?>