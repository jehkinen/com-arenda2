<?php class AdminreclamebannersController extends CController{

	public $defaultAction = 'view';
	public $layout = 'control';
	public $pageDescription;
	public $pageKeywords;

	public function actionView(){

		$this->pageTitle = 'Просмотр баннерных рекламных кампаний';
		$this->pageDescription = '';
		$this->pageKeywords = '';
		$usr = Yii::app()->user;

        # Определяем сортировку для запроса, если нет сортировки то сортируем по id обекта
        $order = '';
		if (isset($_GET['sort'])){
		  $order = $_GET['sort'];
		  if (isset($_GET['type'])) $order .= ' desc';
		}
		if (empty($order)) $order = 'id';

		$criteria = new CDbCriteria;
		$criteria->alias = 'B';
		$criteria->select = 'B.id, B.created, CASE B.isActive when 1 then "да" else "нет" END as isActive, B.name, B.width, B.height, CASE B.onMain when 1 then "да" else "нет" END as onMain, CASE B.onSearch when 1 then "да" else "нет" END as onSearch, CASE B.onAny when 1 then "да" else "нет" END as onAny, B.start, B.stop, B.qty, (Select COUNT(*) from reclame_banner_stat S where S.camp=B.id) as "totalQty"';
		$criteria->order = 'B.'.$order;

		$qty = ReclameBanner::model()->count($criteria);
		# Постраничная разбивка
		$pages = new CPagination($qty);
		$pages->pageSize = 20;
		$pages->applyLimit($criteria);
		# Получаем текущую страницу (нумерация начинается с 0, поэтому +1)
		$Cpage = $pages->getCurrentPage()+1;
		$Items = ReclameBanner::model()->findAll($criteria);
		$this->render('view', array(
			'form'		=>$form,
			'rows'		=>$Items,
			'pages'		=>$pages,
			'foundQty'	=>$qty,
			'Cpage'		=>$Cpage
			));
	}

	public function actionAdd() {
		$Row = new ReclameBanner();
		if (!empty($_POST['ReclameBanner'])){
			$Row->attributes = $_POST['ReclameBanner'];
			$dateStart = $Row->start;
			$dateStop = $Row->stop;
			$Row->start = $this->convertDate($Row->start);
			$Row->stop = $this->convertDate($Row->stop);
			$Row->filename = CUploadedFile::getInstance($Row,'filename');
			if($Row->validate()){
				if (!empty($Row->filename)) {
					$translit_obj = $this->widget('LoadFileName', array('in_text'=>$Row->filename->name, 'len'=>100));
					$translit = $translit_obj->out_text;
					$name = substr($translit, 0, strrpos($translit, '.'));
					$ext = substr($translit, strrpos($translit, '.'));
					$random = mt_rand(10000, 99999);
					$fullname = $name.'_'.$random.$ext;
					$Row->filename->saveAs('storage/images/banners/'.$fullname);
					$Row->filename = $fullname;
				}
				if ($Row->save(false))
					$this->redirect(array('adminreclamebanners/view'));
			} else {
				$Row->start = $dateStart;
				$Row->stop = $dateStop;
			}
		}
		$this->render('add', array('form' =>$Row));
	}

	public function actionEdit() {
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$oRow = ReclameBanner::model()->findByPK($get->id_out);
		if (empty($oRow))
			throw new CHttpException(404);
		if (!empty($_POST['ReclameBanner'])){
			$oRow->attributes = $_POST['ReclameBanner'];
			$oRow->start = $this->convertDate($oRow->start);
			$oRow->stop = $this->convertDate($oRow->stop);
			if($oRow->validate()){
				if ($oRow->save(false))
					$this->redirect(array('adminreclamebanners/view'));
			}
		}
		$this->render('edit', array('form' =>$oRow,
			'oName'		=>$oRow->name,
			'oWidth'	=>$oRow->width,
			'oHeight'	=>$oRow->height,
			'banName'	=>$oRow->filename,
			'oIsActive'	=>$oRow->isActive,
			'oOnMain'	=>$oRow->onMain,
			'oOnSearch'	=>$oRow->onSearch,
			'oOnAny'	=>$oRow->onAny,
			'oStart'	=>$oRow->start,
			'oStop'		=>$oRow->stop,
			'oQty'		=>$oRow->qty
		));
	}

	public function actionDelete() {
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$Row = ReclameBanner::model()->findByPK($get->id_out);
		if (empty($Row))
			throw new CHttpException(404);
		if (isset($_GET['flg'])){
			if ($Row->delete(false)) {
				$s = Yii::app()->params->web_root;
				$i = strrpos($s, '/'); $a = strlen($s)-1;
				if ($i != $a)	$s.='/';
				$s.='storage/images/banners/';
				@unlink ($s.$Row->filename);
				// Удаляем статистику показов Кампании
				$cmd = Yii::app()->db->createCommand('Delete from reclame_banner_stat where camp='.$get->id_out.';');
				$cmd->execute();
				$this->redirect(array('adminreclamebanners/view'));
			}
		}
		$this->render('delete', array('id' =>$get->id_out));
	}

	public function filters(){
	return array(
		array(
			'application.filters.AccessFilter',
			'role'=>'admin',
		)
	);
	}

	private function convertDate($dt){
		list ($d, $m, $y) = explode('.', $dt);
		return $y.'-'.$m.'-'.$d;
	}
}
?>