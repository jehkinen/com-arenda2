<?php class TenantController extends CController{

	public $defaultAction = 'view';
	public $layout = 'inner01';
	public $pageDescription;
	public $pageKeywords;

	public function actionView(){
		$user = Owner_user::model()->find('id=:id_user', array(':id_user'=>Yii::app()->user->id['user_id']));
		if (empty($user))
			throw new CHttpException(404);
		
		/* ~~~~ Получение данных о блокноте собственника для второй вкладки ~~~~~~~  */
		$criteria = new CDbCriteria;
		$criteria->alias = 'N';
		$criteria->select = 'N.id, N.item, N.status, I.status as "itemStatus", F.name as "functionality", I.city, S.name as "street", I.house, I.total_item_sqr, I.price, U.id as "ownId", U.so_name as "ownSoName", U.name as "ownName", U.otchestvo as "ownOtchestvo", U.phone as "ownPhone"';
		$criteria->join = 'join items I on I.id=N.item left join functionality F on F.id=I.functionality join streets S on S.id=I.street '.
		'Left join users U on U.id=I.owner_id';
		$criteria->condition = 'tenant=:id_user and I.itemType<>1';
		$criteria->params = array(':id_user'=>$user->id);
		$criteria->order = 'N.status asc, N.created desc';
		$Rows = Notepads::model()->findAll($criteria);
		// Изменим статус объектов в блокноте
		$cmd = Yii::app()->db->createCommand('Update notepad set status=status+1 where tenant='.$user->id.';');
		$cmd->execute();
		$cmd = Yii::app()->db->createCommand('Update notepad set status=2 where status>2;');
		$cmd->execute();
		/* /// ~~~~ Получение данных о блокноте собственника для второй вкладки ~~~~~~~  */
		/* ~~~~ Получение данных об имеющихся заявках для третьей вкладки ~~~~~~~  */
		$criteria = new CDbCriteria;
		$criteria->alias = 'T';
		$criteria->select = 'T.id, T.kind, T.city, COALESCE(D.name, "Не важен") as "district", T.created, '.
		'NULLIF(T.squareMin, "0") as "squareMin", NULLIF(T.squareMax, "0") as "squareMax", '.
		'NULLIF(T.priceMin, "0") as "priceMin", NULLIF(T.priceMax, "0") as "priceMax", '.
		'U.so_name as "whoSoName", U.name as "whoName", U.otchestvo as "whoOtchestvo"';
		$criteria->join = 'Left join districts D on D.id=T.district join users U on U.id=T.who_id';
		$criteria->condition = 'who_id=:id_user';
		$criteria->params = array(':id_user'=>$user->id);
		$criteria->order = 'T.created desc, T.id';
		$Tends = Tenders::model()->findAll($criteria);
		/* /// ~~~~ Получение данных об имеющихся заявках для третьей вкладки ~~~~~~~  */

		if (empty($user->info_email))
			$user->info_email = $user->e_mail;

  		$form_options = new Tenant_options();
		$form_info = new Owner_info();
		$form_pass = new Owner_pass();

		if (isset($_POST['Tenant_options'])){
			$form_options->attributes = $_POST['Tenant_options'];
			$form_options->info_phone = $_POST['Tenant_options']['tel_part1'].$_POST['Tenant_options']['tel_part2'];
			$user->info_moder = $form_options->info_moder;
			$user->info_order = $form_options->info_order;
			$user->info_deactivate = $form_options->info_deactivate;
			$user->auto_notepad = $form_options->auto_notepad;
			$user->info_flag_phone = $form_options->info_flag_phone;
			$user->info_flag_email = $form_options->info_flag_email;
			if (trim($form_options->info_phone)=='')
				$user->info_phone = null;
			else $user->info_phone = $form_options->info_phone;
			if (trim($form_options->info_email)=='')
				$user->info_email = null;
			else $user->info_email = $form_options->info_email;
			if ($form_options->validate())
				if ($user->save(false))
					$this->redirect(array('tenant/view'));
		}

		if (isset($_POST['Owner_info'])){
			$form_info->attributes = $_POST['Owner_info'];
			$user->name = $form_info->name;
			$user->so_name = $form_info->so_name;
			$user->otchestvo = $form_info->otchestvo;
			$user->e_mail = $form_info->e_mail;
			$user->phone = $form_info->phone;
			$form_info->user_id = $user->id;
			if ($form_info->validate())
				if ($user->save(false))
					$this->redirect(array('tenant/view'));
		}

		if (isset($_POST['Owner_pass'])){
			$form_pass->attributes = $_POST['Owner_pass'];
			if ($form_pass->validate()){
			$user->password = md5($form_pass->password);
			if ($user->save(false))
				$this->redirect(array('tenant/view'));
			}
		}
		
		if (empty($user->info_phone)) {
			$tel1 = '';	$tel2 = '';
		} else {
			$tel1 = substr($user->info_phone, 0, 3);
			$tel2 = substr($user->info_phone, 3);
		}

		$this->pageTitle = 'Кабинет арендатора';
		$this->pageDescription = '';
		$this->pageKeywords = '';

		$this->render('view', array(
			'form_options'	=>$form_options,
			'form_info'		=>$form_info,
			'form_pass'		=>$form_pass,
			'user'			=>$user,
			'Items'			=>$Rows,
			'Tends'			=>$Tends
		));

	}

	public function actionDelete_notepad(){		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;
		if (isset($_GET['flg'])){
			$NoteRow = Notepads::model()->find('item=:iid and tenant=:tid', array(':iid'=>$id, ':tid'=>Yii::app()->user->id['user_id']));
			if (empty($NoteRow))
				throw new CHttpException(404);

			if ($NoteRow->delete(false))
				$this->redirect(array('tenant/view'));
		}

		$this->render('_delete', array('id_active' =>$id));	}

	public function actionDelete_order(){
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;

		if (isset($_GET['flg'])){
			$Tender = Tenders::model()->find('id=:iid and who_id=:tid', array(':iid'=>$id, ':tid'=>Yii::app()->user->id['user_id']));
			if (empty($Tender))
				throw new CHttpException(404);

			// Удалим связь заявки с собственниками
			$cmd = Yii::app()->db->createCommand('Delete from bid_owners where tender='.$id.';');
			$cmd->execute();
			if ($Tender->delete(false))
				$this->redirect(array('tenant/view'));
		}

		$this->render('_delete_order', array('id' =>$id));
	}
	
	public function filters(){
		return array(
			array(
				'application.filters.AccessFilter',
				'role'=>'tenant'
			),
		);
	}

}?>