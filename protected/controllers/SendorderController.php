<?php 
class SendorderController extends CController{

	public $defaultAction = 'view';
	public $layout = 'inner02';
	public $pageDescription;
	public $pageKeywords;
	public $jsFiles = array('new.js');
	public $toBodyTag = ' onload="fakeUploads();"';

	public function actions(){
		return array(
			'captcha'=>array(
				'class'=>'MyCCaptchaAction',
				/*'class'=>'CCaptchaAction',
				'backColor'=> 0x003300,
				'maxLength'=> 3,
				'minLength'=> 3,
				'foreColor'=> 0x66FF66,*/
			),
		);
	}

	public function actionView(){

		$form = new SendOrder();
		$wasSend = false;
		$kind = 0; $about = '';
		
		if (!empty($_POST['SendOrder'])){
			ini_set('memory_limit', '72M');
			ini_set('upload_max_filesize', '9M');
			$form->attributes = $_POST['SendOrder'];
			$kind = $form->kind;
			$about = $form->about;
			if ($form->validate()){
				$filter = new CHtmlPurifier();
				$form->e_mail = $filter->purify($form->e_mail);
				$form->name = $filter->purify($form->name);
				$form->phone = $filter->purify($form->phone);

				$form->about = $filter->purify($form->about);
				$form->city = $filter->purify($form->city);
				$form->district = $filter->purify($form->district);
				$form->street = $filter->purify($form->street);
				$form->house = $filter->purify($form->house);
				$form->housing = $filter->purify($form->housing);
				$form->stage = $filter->purify($form->stage);
				$form->staging = $filter->purify($form->staging);
				$form->square = $filter->purify($form->square);
				$form->price = $filter->purify($form->price);

				$form->fotos[] = CUploadedFile::getInstance($form,'[1]fotos');
				$form->fotos[] = CUploadedFile::getInstance($form,'[2]fotos');
				$form->fotos[] = CUploadedFile::getInstance($form,'[3]fotos');
				$form->fotos[] = CUploadedFile::getInstance($form,'[4]fotos');
				$form->fotos[] = CUploadedFile::getInstance($form,'[5]fotos');

				// Отсылаем письмо
				require_once('protected/extensions/mailer/class.mailer.php');
				$mail = new Mailer();
				$mail->From = Yii::app()->params->admin_mail;
				$mail->Subject  = 'Заявка на размещение объекта';
				$Flds[0] = 'Город : '; if (empty($form->city)) $Flds[0].='?'; else $Flds[0] .= $form->city; $Flds[0].="\n";
				$Flds[1] = 'Район : '; if (empty($form->district)) $Flds[1].='?'; else $Flds[1] .= $form->district; $Flds[1].="\n";
				$Flds[2] = 'Улица : '; if (empty($form->street)) $Flds[2].='?'; else $Flds[2] .= $form->street; $Flds[2].="\n";
				$Flds[3] = 'Дом : '; if (empty($form->house)) $Flds[3].='?'; else $Flds[3] .= $form->house; $Flds[3].="\n";
				$Flds[4] = 'Дом : '; if (empty($form->housing)) $Flds[4].='?'; else $Flds[4] .= $form->housing; $Flds[4].="\n";
				$Flds[5] = 'Этаж : '; if (empty($form->stage)) $Flds[5].='?'; else $Flds[5] .= $form->stage; $Flds[5].="\n";
				$Flds[6] = 'Этажность : '; if (empty($form->staging)) $Flds[6].='?'; else $Flds[6] .= $form->staging; $Flds[6].="\n";
				$Flds[7] = 'Площадь : '; if (empty($form->square)) $Flds[7].='?'; else $Flds[7] .= $form->square; $Flds[7].="\n";
				$Flds[8] = 'Цена : '; if (empty($form->price)) $Flds[8].='?'; else $Flds[8] .= $form->price; $Flds[8].="\n";
				$Flds[9] = 'Дополнительно : '; if (empty($form->about)) $Flds[9].='?'; else $Flds[9] .= $form->about; $Flds[9].="\n";
				$txt = 'Тип объекта: ';
				switch ($form->kind){
					case 1: {$txt.='Торговое помещение'; break;}
					case 2: {$txt.='Склад/Производство'; break;}
					default: $txt.='Офис';
				}
				$txt.="\n";
				for ($i=0;$i<10;$i++)
					$txt.= $Flds[$i];
				$mail->Body = 'Имя отправителя: '.$form->name."\n".'Обратный email: '.$form->e_mail."\n".'Контактный телефон: '.$form->phone."\n".'Время подачи заявки: '.date('d.m.Y h:i:s')."\n\n".'Значения полей --'."\n".$txt;
				// Фото
				$Attachs = array();
				if (!empty($form->fotos)){
					foreach ($form->fotos as $file)
						if (!empty($file) && file_exists($file->tempName)) {
							$translit = $this->widget('LoadFileName', array('in_text'=>$file->name, 'len'=>200));
							$fname = 'storage/images/mail_attachs/'.$translit->out_text;
							if ($file->saveAs($fname)){
								$Attachs[] = $fname;
								$mail->AddAttachment ($fname, $file->name);
							}
						}
				}
			   $mail->AddAddress('office@com-arenda.ru');
				if ($mail->Send())
					$wasSend = true;
				// Удалим временно сохраненные файлы, прикрепленные к письму
				if (!empty($Attachs)) {
					$s = Yii::app()->params->web_root;
					$i = strrpos($s, '/'); $a = strlen($s)-1;
					if ($i != $a)	$s.='/';
					foreach ($Attachs as $foto)
						@unlink ($s.$foto);
				}
			}
		}

		$this->pageTitle = 'Размещение объекта';
		$this->pageDescription = '';
		$this->pageKeywords = '';
		//Возьмём город из сессии
		$form->city=Yii::app()->user->getState('glcity');

		if ($wasSend)
			$this->render('success');
		else
			$this->render('view', array('form' =>$form, 'kind' =>$kind, 'about'=>$about));
	}

}?>