<?php class TestController extends CController{
	public $layout = 'inner02';
	public $defaultAction = 'view';
	public $pageClass = 'any';
	public $pageDescription;
	public $pageKeywords;

	public function actionView() {
		$this->pageTitle = $news->header;
		$this->pageDescription = $news->description;
		$this->pageKeywords = $news->keywords;
		$this->render('view');
	}

	public function actionImagesedit() {
		$get = $this->widget('GetChek', array('get_mass'=>$_GET));
		$id = $get->id_out;
		$criteria = new CDbCriteria;
		$criteria->alias = 'I';
		$criteria->select = 'I.id, I.parent, I.itemType, I.kind, I.owner_id, I.city, COALESCE(I.name, "") as name, COALESCE(S.name, "не указана") as street, COALESCE(I.house, "дом не указан") as house, COALESCE(I.storey, "не указан") as storey, COALESCE(II.name, "<без названия>") as parentName';
		$criteria->join = 'left join streets S on S.id=I.street '.
			'left join items II on II.id=I.parent ';
		$criteria->condition = 'I.id=:id';
		$criteria->params = array(':id'=>$id);
		$Row = ItemDetail::model()->find($criteria);
		$vals = Yii::app()->user;
		if (empty($Row) || $vals->isGuest || ($vals->id['user_role'] != 'admin' && ($vals->id['user_id'] != $Row->owner_id)))
			throw new CHttpException(404);
		$criteria = new CDbCriteria;
		$criteria->condition = 'id_item=:ii';
		$criteria->params = array(':ii'=>$id);
		$criteria->order = 'id desc';
		$Fotos = Images::model()->findAll($criteria);
		$this->pageTitle = '';
		$this->pageDescription = '';
		$this->pageKeywords = '';
		$this->render('imagesedit', array('fotos'	=>	$Fotos, 'obj'	=>	$Row)); 
	}

	public function actionUnproportional() {

		$criteria = new CDbCriteria;
		$criteria->alias = 'I';
		$criteria->select = 'M.img as hasFoto, I.id, I.parent, I.itemType, I.kind, I.owner_id, I.city, COALESCE(I.name, "") as name, COALESCE(S.name, "не указана") as street, COALESCE(I.house, "дом не указан") as house, COALESCE(I.storey, "не указан") as storey, COALESCE(II.name, "<без названия>") as parentName';
		$criteria->join = 'left join streets S on S.id=I.street join images M on M.id_item=I.id '.
			'left join items II on II.id=I.parent ';
		$criteria->condition = 'I.id<500';
		$criteria->order = 'I.id';
		$Rows = ItemDetail::model()->findAll($criteria);
		$vals = Yii::app()->user;
		if (empty($Rows) || $vals->isGuest || $vals->id['user_role'] != 'admin')
			throw new CHttpException(404);

		foreach ($Rows as $f) {
			$infoIMG = getimagesize('storage/images/'.$f->hasFoto);
			$dif = $infoIMG[0]/$infoIMG[1];
			$f->phone = (($dif>1.5) || ($dif<0.7));
			$f->login = $infoIMG[0].'x'.$infoIMG[1].' = '.$dif;
		}

		$this->pageTitle = '';
		$this->pageDescription = '';
		$this->pageKeywords = '';
		$this->render('unproportional', array('Objs'	=>	$Rows)); 
	}

	public function actionUsersinfoset() {

		$Rows = User::model()->findAll('role NOT IN(:rl)', array(':rl'=>'admin'));

		foreach ($Rows as $u) {
			$u->info_moder = 1;
			$u->info_order = 1;
			$u->info_deactivate = 1;
			$u->auto_notepad = 1;
			$u->info_flag_email = 1;
			if (!isset($u->info_phone) || empty($u->info_phone)) {
				$wg = $this->widget('AnalyzePhone', array('phone'=>$u->phone));
				if ($wg->isCellphone) {
					$u->info_flag_phone = 1;
					$u->info_phone = $wg->phone;
				} else {
					$u->info_flag_phone = 0;
					unset ($u->info_phone);
				}
			}
			if (!isset($u->info_email) || empty($u->info_email)) {
				$u->info_email = $u->e_mail;
			}
			$u->save(false);
		}

		$this->pageTitle = '';
		$this->pageDescription = '';
		$this->pageKeywords = '';
		$this->render('usersinfoset', array('Users'	=>	$Rows)); 
	}

}?>