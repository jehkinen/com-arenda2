<?php

class Partnership extends CFormModel{
	public $text;
	public $name;
	public $telephone;
	public $email;


	public function rules(){
		return array(
			array('name, telephone, email, text', 'required', 'message'=>'Введите поле "{attribute}".'),
			array('telephone', 'match', 'pattern'=>'/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/', 'message'=>'Введте пожалуйста корректный телефон'),
			array('name', 'match', 'pattern' => '/^[ёа-я0-9. \/-]+$/ui', 'message' => '{attribute} содержит недопустимые символы.'),
			array('email','email', 'message'=>'Введте пожалуйста корректный адрес'),
			array('text', 'safe')
		);
	}

	public function attributeLabels(){
		return array(
			'name' =>'Имя',
			'telephone' =>'Телефон',
			'email' =>'Электронный адрес',
			'text' =>'Текст'
		);
	}

}
?>