<?php class SendOrder extends CFormModel{

	public $name;
	public $phone;
	public $e_mail;
	public $verifyCode;

	public $kind;
	public $about;
	public $city;
	public $district;
	public $street;
	public $house;
	public $housing;
	public $stage;
	public $staging;
	public $square;
	public $price;
	public $fotos;

	public function rules(){
		return array(
			array('name, e_mail, phone, kind, about, city, district, street, house, housing, stage, staging, square, price', 'safe'),
			array('city, district, street, name, phone, e_mail', 'required', 'message'=>'Обязательно укажите {attribute}.'),
			array('e_mail', 'email', 'message'=>'{attribute} имеет недопустимый формат.'),
			array('verifyCode', 'captcha', 'allowEmpty'=>!extension_loaded('gd'), 'message'=>'Код с картинки введен неверно.')
		);
	}

	public function attributeLabels(){
		return array(
			'city'		=>'Город',
			'district'	=>'Район',
			'street'	=>'Улица',
			'name'		=>'Имя',
			'e_mail'	=>'E-mail',
			'phone'		=>'Контактный телефон'
		);
	}

}?>