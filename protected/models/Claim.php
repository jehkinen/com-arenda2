<?php class Claim extends CActiveRecord{

	public $tenLogin;
	public $tenFirstname;
	public $tenLastname;
	public $ownLogin;
	public $ownFirstname;
	public $ownLastname;
	public $itemOwner;
	public $itemName;
	public $itemStreet;
	public $itemHouse;


	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function tableName(){
		return 'claims';
	}

}?>