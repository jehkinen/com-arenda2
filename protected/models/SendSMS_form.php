<?php
class SendSMS_form extends CFormModel{
	public $txt;

	public function rules(){
	return array(
		array('txt', 'required', 'message'=>'Поле "{attribute}" обязательно для заполнения.'),
		array('txt', 'safe')
		);
	}

	public function attributeLabels(){
		return array(
			'txt' =>'Текст сообщения'
		);
	}

	}
?>