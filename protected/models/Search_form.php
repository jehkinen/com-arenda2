<?php
  class Search_form extends CFormModel{	
	public $kind;
	public $sity;
	public $raion;
	public $tip_sooruj;
	public $funkcional;
	public $klass;
	public $ulica;
	public $etaj1;
	public $etaj2;
	public $etaj_label;
	public $gruz_lift;
	public $sqr1;
	public $sqr2;
	public $sqr_label;
	public $sosremont;
	public $parkovka;
	public $prc1;
	public $prc2;
	public $prc_label;
	public $est_inet;
	public $est_telef;
	public $est_voda;
	public $potolok;
	public $kranb;
	public $pokrytie;
	public $ventil;
	public $condic;
	public $pozhar;
	public $vorota;
	public $pandus;
	public $autoways;
	public $railways;

    public function rules(){
      return array(
             array('etaj1, etaj2', 'numerical', 'integerOnly'=>true, 'min'=>0, 'message'=>'поле &laquo;{attribute}&raquo; может быть только целочисленным.', 'tooSmall'=>'поле &laquo;{attribute}&raquo; не может быть отрицательным.'),
             #array('etaj1', 'etaj_logic', 'dop'=>self::STRONG),
             array('sqr1, sqr2, prc1, prc2', 'numerical', 'integerOnly'=>false, 'min'=>0, 'message'=>'поле &laquo;{attribute}&raquo; может быть только числом.', 'tooSmall'=>'поле &laquo;{attribute}&raquo; не может быть отрицательным.')
      );
    }

    /*public function etaj_logic($qwer, $qazw){
      var_dump($qazw['dop']);
      die();
      if ($this->$etaj1 != '' && $this->$etaj2 != ''){        if ($this->$etaj1 > $this->$etaj2){          $this->addError('etaj_label', 'значение &laquo;Этаж. ОТ&raquo; не должен быть больше значения &laquo;Этаж. ДО&raquo;');
          #$this->addError('username', 'Такой юзер уже существует.');        }      }

    }*/
			
	public function attributeLabels(){
		return array(
			'kind'       =>'Помещение',
			'sity'       =>'Город',
			'raion'      =>'Район',
			'tip_sooruj' =>'Тип сооружения',
			'funkcional' =>'Функциональное назначение',
			'klass'      =>'Класс',
			'ulica'      =>'Улица',
			'etaj1'      =>'Этаж. ОТ',
			'etaj2'      =>'Этаж. ДО',
			'etaj_label' =>'Этаж. ОТ и ДО',
			'gruz_lift'  =>'Грузовой лифт',
			'sqr1'       =>'Площадь помещения. ОТ',
			'sqr2'       =>'Площадь помещения. ДО',
			'sqr_label'  =>'Площадь помещения. ОТ и ДО',
			'sosremont'  =>'Состояние ремонта',
			'parkovka'   =>'Паркинг',
			'prc1'       =>'Арендная ставка. ОТ',
			'prc2'       =>'Арендная ставка. ДО',
			'prc_label'  =>'Арендная ставка. ОТ и ДО',
			'est_inet'   =>'Интернет',
			'est_telef'  =>'Телефония',
			'est_voda'   =>'Система отопления и водоснабжения',
			'potolok'    =>'Общая высота потолков',
			'kranb'      =>'Кран-балка',
			'pokrytie'   =>'Покрытие пола',
			'ventil'     =>'Вентиляция',
			'condic'     =>'Кондиционирование',
			'pozhar'     =>'Пожаротушение',
			'vorota'     =>'Высота ворот',
			'pandus'     =>'Пандус, чекбокс',
			'autoways'   =>'Автомобильные подъездные пути',
			'railways'   =>'Железнодорожные подъездные пути'
		);
	}

  }
?>