<?php
class UserSendMessage_form extends CFormModel{
	public $subject;
	public $txt;

	public function rules(){
	return array(
		array('subject, txt', 'required', 'message'=>'Поле "{attribute}" обязательно для заполнения.'),
		array('subject, txt', 'safe')
		);
	}

	public function attributeLabels(){
		return array(
			'subject'	=>'Тема сообщения',
			'txt'		=>'Текст сообщения'
		);
	}

	}
?>