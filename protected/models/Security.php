<?php
  class Security extends CActiveRecord{

    public static function model($className=__CLASS__){
      return parent::model($className);
    }

    public function tableName(){
      return 'security';
    }

	public function rules(){
	return array(
		array('name, shname', 'required', 'message'=>'Введите поле "{attribute}".'),
		array('name, shname', 'match', 'pattern' => '/^[ёа-я0-9. \/-]+$/ui', 'message' => '{attribute} содержит недопустимые символы.'),
		array('name, shname', 'safe')
		);
	}

    public function attributeLabels(){
      return array(
		'name' =>'Название системы безопасности',
		'shname' =>'Название системы безопасности кратко'
		);
    }

  }
?>