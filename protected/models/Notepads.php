<?php class Notepads extends CActiveRecord{

	public $kind;
	public $special;
	public $itemStatus;
	public $functionality;
	public $created;
	public $city;
	public $district;
	public $street;
	public $house;
	public $total_item_sqr;
	public $price;
	public $hasFoto;
	public $ownSoName;
	public $ownName;
	public $ownOtchestvo;
	public $ownId;
	public $ownPhone;
	public $tenInfoDeactivate;
	public $tenInfoMail;
	public $tenInfoFlagMail;
	public $tenInfoPhone;
	public $tenInfoFlagPhone;

	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function tableName(){
		return 'notepad';
	}

}?>