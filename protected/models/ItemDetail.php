<?php class ItemDetail extends CActiveRecord{

	public $login;
	public $firstname;
	public $lastname;
	public $phone;
	public $hasFoto;
	public $districtID;
	public $parentName;
	public $parentKind;
	public $maxPrice;
	public $minPrice;
	public $maxSquare;
	public $minSquare;

	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function tableName(){
		return 'items';
	}

}?>