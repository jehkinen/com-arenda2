<?php class SendMail extends CFormModel{

	public $receiver;
	public $name;
	public $e_mail;
	public $messTxt;
	public $verifyCode;

	public function rules(){
		return array(
			array('receiver, name, e_mail, messTxt', 'safe'),
			array('name, e_mail, messTxt', 'required', 'message'=>'Обязательно укажите {attribute}.'),
			array('e_mail', 'email', 'message'=>'{attribute} имеет недопустимый формат.'),
			array('verifyCode', 'captcha', 'allowEmpty'=>!extension_loaded('gd'), 'message'=>'Код с картинки введен неверно.')
		);
	}

	public function attributeLabels(){
		return array(
			'receiver'	=>'Получатель',
			'name'		=>'Ваше Имя (а лучше и Отчество и Фамилия)',
			'e_mail'	=>'Ваш e-mail для ответа',
			'messTxt'	=>'Текст'
		);
	}

}?>