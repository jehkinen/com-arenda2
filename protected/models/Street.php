<?php
  class Street extends CActiveRecord{

    public static function model($className=__CLASS__){
      return parent::model($className);
    }

    public function tableName(){
      return 'streets';
    }

	public function rules(){
	return array(
		array('name, city', 'required', 'message'=>'Введите поле "{attribute}".'),
		array('name, city', 'match', 'pattern' => '/^[ёа-я0-9. \/-]+$/ui', 'message' => '{attribute} содержит недопустимые символы.'),
		array('name, city', 'safe')
		);
	}

	public function attributeLabels(){
	return array(
		'name' =>'Название улицы',
		'city' =>'Город'
		);
	}

  }
?>