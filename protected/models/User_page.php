<?php class User_page extends CActiveRecord{

	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function tableName(){
		return 'users';
	}
	
	public function rules(){
		return array(
			array('parag1_name, parag2_name, parag1_text, parag2_text', 'safe'),
			array('parag1_name, parag2_name', 'my_length', 'len'=>128, 'mes'=>'Название раздела не должно превышать 128 символов'),
			array('parag1_text, parag2_text', 'my_length', 'len'=>8190, 'mes'=>'Текст раздела не должен содержать свыше 8190 символов'),
			array('parag1_text', 'chek_name', 'name'=>'parag1_name'),
			array('parag2_text', 'chek_name', 'name'=>'parag2_name')
		);
	}

	public function chek_name($attribute,$params){
		if (!empty($this->$attribute) && (empty($this->$params['name'])))
			$this->addError($attribute, 'Если существует текст раздела, то укажите также название раздела');
	}

	public function my_length($attribute,$params) {
		if (strlen($this->$attribute)>$params['len'])
			$this->addError($attribute, $params['mes']);
	}

}?>