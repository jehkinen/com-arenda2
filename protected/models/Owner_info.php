<?php class Owner_info extends CActiveRecord{

	public $user_id;

	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function tableName(){
		return 'users';
	}

	public function rules(){
		return array(
			array('name, so_name, phone', 'required', 'message'=>'строка {attribute} не должна быть пустой.'),
			array('name, so_name, otchestvo', 'match', 'pattern' => '/^[ёа-я -]+$/ui', 'message' => '{attribute} содержит недопустимые символы.'),
			array('e_mail', 'email', 'message'=>'Некорректный формат e-mail.'),
			array('e_mail', 'chek_mail'),
			array('name, so_name, otchestvo, e_mail, phone', 'safe')
		);
	}

	public function chek_mail($e_mail){
		$exists = User::model()->exists('e_mail=:em and not (id=:ui)', array(':em'=>$this->e_mail, ':ui'=>$this->user_id));
		//$User_obj = User::model()->find('e_mail=:em and id<>:ui', array(':em'=>$this->e_mail, ':ui'=>$this->user_id));
		//if (!empty($User_obj))
		if ($exists)
			$this->addError('e_mail', 'Такой e-mail уже существует.');
	}

	public function attributeLabels(){
		return array(
			'name'		=>'Имя',
			'so_name'	=>'Фамилия',
			'otchestvo'	=>'Отчество',
			'e_mail'	=>'E-mail',
			'phone'		=>'Телефон'
		);
	}

}?>