<?php class ReclameBanner extends CActiveRecord{
	public $totalQty;

	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function tableName(){
		return 'reclame_banners';
	}

	public function rules(){
		return array(
			array('name, filename, width, height, isActive, onMain, onSearch, onAny, start, stop, qty', 'required', 'message'=>'Введите {attribute}.'),
			array('name, filename, width, height, isActive, onMain, onSearch, onAny, start, stop, qty', 'safe'),
			array('name', 'match', 'pattern'=>'/^[ёа-яa-z0-9,.()№"_! \/-]+$/ui', 'message'=>'Поле {attribute} содержит недопустимые символы.'),
			array('isActive, onMain, onSearch, onAny', 'validateCheckBox'),
			array('qty', 'numerical', 'integerOnly'=>true, 'min'=>0, 'max'=>65400, 'message'=>'Поле "{attribute}" в недопустимом диапазоне.'),
			array('width, height', 'numerical', 'integerOnly'=>true, 'min'=>30, 'max'=>3000, 'message'=>'Поле "{attribute}" в недопустимом диапазоне.'),
			array('start, stop', 'checkDateAttr')
		);
	}

    public function validateCheckBox($attribute,$params){
		if ($this->$attribute!=0)
			$this->$attribute=1;
	}

	public function checkDateAttr($attribute,$params){
		list ($year, $month, $day) = explode('-', $this->$attribute);
		$flag = ( (is_numeric ($day)) and (is_numeric ($month)) and (is_numeric ($year)) );
		if ($flag) $flag = ( ($year > 1920) and ($year < 2100) );
		if ($flag) $flag = checkdate ($month, $day, $year);
		if (!$flag) {
			$labels = $this->attributeLabels();
			$this->addError($attribute, 'Некорректная дата в поле '.$labels[$attribute]);
		}
	}

	public function attributeLabels(){
		return array(
			'name'		=>'Названия кампании (чтоб отличать)',
			'filename'	=>'Файл-баннер',
			'width'		=>'Ширина баннера',
			'height'	=>'Высота баннера',
			'isActive'	=>'Активна ли кампания (ручное управление)',
			'onMain'	=>'Показывать ли на Главной странице',
			'onSearch'	=>'Показывать ли на страницах с Результатами поиска и Описанием объектов',
			'onAny'		=>'Показывать ли на всех других страницах',
			'start'		=>'Дата Начала показа кампании (формат дат: ДД.ММ.ГГГГ)',
			'stop'		=>'Дата Окончания показа кампании',
			'qty'		=>'Кол-во показов (сколько раз показывать баннер)'
		);
	}

}?>