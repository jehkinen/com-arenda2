<?php
class AdminItems_form extends CFormModel{
	public $kind;
	public $city;
	public $status;
	public $owner;

	public function rules(){
	return array(
		array('city', 'match', 'pattern' => '/^[ёа-я0-9. \/-]+$/ui', 'message' => '{attribute} содержит недопустимые символы.'),
		array('kind, status, owner', 'numerical', 'integerOnly'=>true),
		array('city, kind, status, owner', 'safe')
		);
	}


	public function attributeLabels(){
		return array(
			'kind'		=>'Помещение',
			'city'		=>'Город',
			'status'	=>'Статус',
			'owner'		=>'Владелец'
		);
	}

	}
?>