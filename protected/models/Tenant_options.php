<?php class Tenant_options extends CActiveRecord{

	public $tel_part1;
	public $tel_part2;

	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function tableName(){
		return 'users';
	}

	public function rules(){
		return array(
			array('info_moder, info_order, info_deactivate, auto_notepad, info_flag_phone, info_flag_email', 'validateCheckBox'),
			array('info_phone', 'contactsMustExists'),
			array('info_email', 'email', 'message'=>'Некорректный формат e-mail.'),
			array('info_moder, info_order, info_deactivate, auto_notepad, info_flag_phone, info_flag_email, info_phone, info_email, tel_part1, tel_part2', 'safe'),
		);
	}

	public function validateCheckBox($attribute,$params){
		if ($this->$attribute!=0)
			$this->$attribute=1;
	}

	public function contactsMustExists(){
		if (($this->info_moder || $this->info_deactivate) && !$this->info_flag_phone && !$this->info_flag_email)
			$this->addError('', 'Не отмечен ни один из способов оповещения');
		if ($this->info_flag_phone) {
			if (empty($this->tel_part1))
				$this->addError('tel_part1', 'Не задан код оператора связи для SMS-оповещения');
			if (empty($this->tel_part2))
				$this->addError('tel_part2', 'Не задан номер телефона для SMS-оповещения');
			if (!empty($this->tel_part1) && !empty($this->tel_part2))
				$this->checkMobile();
		}
		if ($this->info_flag_email && empty($this->info_email))
			$this->addError('info_email', 'Не указан e-mail для отправки уведомлений');
	}

    public function checkMobile(){		$s = $this->tel_part1.$this->tel_part2;
		$l = strlen($s);
		if ($l==0) {
			if ($this->info_flag_phone==1)
				$this->addError('', 'Укажите номер телефона (допустимы только мобильные номера) для SMS-оповещения');
		} elseif ($l==10) {
				if (preg_match('/\d{10}/', $s))
					$this->info_phone = $s;
				else $this->addError('', 'Формат номера телефона для SMS-оповещения: 000 0000000');
			} else {
				$this->addError('', 'Недопустимый формат номера телефона для SMS-оповещения');
			}
	}

	public function attributeLabels(){
		return array(
			'info_moder'		=>'Извещать меня о поступлении новых объектов ',
			'info_order'		=>'Показывать мои запросы собственникам',
			'info_deactivate'	=>'Информировать меня при деактивации (сдаче) объекта из блокнота',
			'auto_notepad'		=>'Автоматически копировать подходящие объекты в блокнот',
			'info_flag_phone'	=>'по SMS',
			'info_flag_email'	=>'на e-mail',
			'info_phone'		=> '+7',
			'info_email'		=> ''
		);
	}

}?>