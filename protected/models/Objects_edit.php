<?php class Objects_edit extends CActiveRecord{

	public $image;		// Загрузка фотографии
	public $docums;		// Прикрепленные документы
	public $docname;	// Названия прикрепленных документов

	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function tableName(){
		return 'items';
	}

	public function rules(){
		return array(
			array('price, total_item_sqr, gates_height, floor_load, el_power, ceiling_height, work_height, pallet_capacity, shelf_capacity', 'correctDecDot'),

			array('itemType', 'required', 'message'=>'Выберите тип объекта.'),
			array('itemType', 'checkItemType'),
			array('parent, about', 'safe'),
			array('kind', 'required', 'message'=>'Укажите тип помещения.'),
			array('kind, parent', 'numerical', 'integerOnly'=>true, 'min'=>0, 'message'=>'Поле "{attribute}".'),
			array('city', 'match', 'pattern'=>'/^[ёа-я0-9. \/-]+$/ui', 'message'=>'Поле {attribute} содержит недопустимые символы.'),

			array('street, district, house, name', 'required', 'message'=>'Поле "{attribute}" обязательно для заполнения.'),
			array('storey, kind_structure', 'requireByType', 'message'=>'Поле "{attribute}" обязательно для заполнения.'),
			array('kind_structure', 'numerical', 'integerOnly'=>true, 'min'=>0, 'message'=>'Поле "{attribute}".'),
			array('functionality, street, district, class', 'numerical', 'integerOnly'=>true, 'min'=>1, 'message'=>'Поле "{attribute}".'),
			array('name', 'match', 'pattern'=>'/^[ёа-я0-9a-z." \/-]+$/ui', 'message'=>'Поле {attribute} содержит недопустимые символы.'),
			array('house, housing', 'match', 'pattern'=>'/^[ёа-я0-9\/-]+$/ui', 'message'=>'Поле {attribute} содержит недопустимые символы.'),
			array('storey', 'numerical', 'integerOnly'=>true, 'min'=>-100, 'max'=>200, 'message'=>'Поле "{attribute}" может быть только целочисленным.', 'tooSmall'=>'Поле "{attribute}". Ниже 100? Слишком низко!', 'tooBig'=>'Поле "{attribute}". Выше 200? Слишком высоко!'),
			array('storeys', 'numerical', 'integerOnly'=>true, 'min'=>-100, 'max'=>200, 'message'=>'Поле "{attribute}" может быть только целочисленным.', 'tooSmall'=>'Поле "{attribute}". Ниже 100? Слишком глубоко!', 'tooBig'=>'Поле "{attribute}". Выше 200? Слишком высоко!'),
			array('storeys', 'checkStoreys', 'message'=>'Поле "{attribute}" превышает допустимое значение.'),
			array('storey', 'checkStorey', 'message'=>'Поле "{attribute}" превышает допустимое значение.'),

			array('price, total_item_sqr', 'required', 'message'=>'Поле "{attribute}" обязательно для заполнения.'),
			array('ceiling_height, el_power', 'requireByType', 'message'=>'Поле "{attribute}" обязательно для заполнения.'),
			array('contr_condition, pay_utilities, tel_company, providers', 'numerical', 'integerOnly'=>true, 'min'=>0, 'message'=>'Поле "{attribute}".'),
			array('price, total_item_sqr, gates_height, floor_load, el_power, ceiling_height, work_height', 'numerical', 'integerOnly'=>false, 'min'=>0, 'message'=>'Поле "{attribute}" является числовым.', 'tooSmall'=>'Поле "{attribute}" не может быть отрицательным.'),
			array('service_lift, cathead, telephony, internet', 'validateCheckBox'),
			array('gates_qty', 'numerical', 'integerOnly'=>true, 'min'=>1, 'max'=>50, 'message'=>'Поле "{attribute}" может быть только целочисленным.', 'tooSmall'=>'Поле "{attribute}" не может быть отрицательным.', 'tooBig'=>'Поле "{attribute}". Больше 50-ти? Так много!'),
			//array('total_item_sqr', 'checkSquare', 'message'=>'Поле "{attribute}" превышает допустимое значение.'),

			array('ventilation, flooring', 'requireByType', 'message'=>'Поле "{attribute}" обязательно для заполнения.'),
			array('repair, parking, flooring, ventilation', 'numerical', 'integerOnly'=>true, 'min'=>0, 'message'=>'Поле "{attribute}".'),
			array('fixed_qty', 'numerical', 'integerOnly'=>true, 'min'=>0, 'message'=>'Поле "{attribute}" может быть только целочисленным.', 'tooSmall'=>'Поле "{attribute}" не может быть отрицательным.'),
			array('welfare', 'match', 'pattern'=>'/^[ёа-я0-9,.()№_! \/-]+$/ui', 'message'=>'Поле {attribute} содержит недопустимые символы.'),
			array('access, video, heating, air_condit, firefighting, can_reclame, rampant, autoways, railways', 'validateCheckBox'),
			array('pallet_capacity, shelf_capacity', 'numerical', 'integerOnly'=>false, 'min'=>0, 'message'=>'Поле "{attribute}" является числовым.', 'tooSmall'=>'Поле "{attribute}" не может быть отрицательным.'),

//			array('image', 'files'),
			//array('docums', 'checkDocs')
		);
	}

	public function checkDocs(){
		if(!$this->hasErrors()){
			$docs = $this->docums;
			$names= $this->docname;
			//CVarDumper::Dump($docs, 3, true); die;
			$validExt = array('.txt', '.doc', '.docx', '.rtf', '.xls', '.xlsx', '.ppt', '.pptx', '.pdf', '.odt', '.ods', '.sxw');
			for ($i=0;$i<3;$i++) {
				if (!empty($docs[$i])) {
					if (empty($names[$i])) {
						$this->addError('docname', 'У файла номер '.($i+1).' не указано название');	break;
					}
					$ext = substr($docs[$i]->name, strrpos($docs[$i]->name, '.'));
					$ext = mb_strtolower($ext, 'UTF-8');
					if (!in_array($ext, $validExt)){
						$this->addError('docums', 'Файл номер '.($i+1).' имеет неверный тип. Допустимые форматы: *txt, *doc, *docx, *rtf, *xls, *xlsx, *ppt, *pptx, *pdf, *odt, *ods, *sxw'); break;
					}
					if (preg_match('/[№\/*%@\\\`]+/', $names[$i])) {
						$this->addError('docname', 'Название файла номер '.($i+1).' содержало недопустимые символы');	break;
					}
				}
			}
		}
	}

	public function files(){
		if(!$this->hasErrors()){
			$files = $this->image;
			$need_expansion = array('.jpg','.jpeg','.png','.gif');
			foreach ($files as $file){
				if (!empty($file)){
					$expansion = substr($file->name, strrpos($file->name, '.'));
					$expansion = mb_strtolower($expansion, 'UTF-8');
					if (!in_array($expansion, $need_expansion)){
						$this->addError('image', 'Одна из фотографий имеет неверный тип, допустимые форматы: *jpg, *jpeg, *png, *gif');
						break;
					}
					if ($file->size > 5242880){
						$this->addError('image', 'Размер одной из фотографий больше 5Мб.');
						break;
					}
					$image_size = getimagesize($file->tempName);
					if ($image_size[0] <= 309){
						$this->addError('image', 'Ширина одной из фотографии меньше 310 пикселов.');
						break;
					}
				}
			}
		}
	}

	public function correctDecDot ($attribute,$params) {
		$vo = $this->$attribute;
		$v = str_replace(',', '.', $vo);
		$p = strpos($v, '.');
		if ($p!==false) {
			$p = strpos($v, '.', $p+1);
			if ($p!==false)
				$v = substr($v, 0, $p);
		}
		if ($v!=$vo) {
			$this->$attribute = $v;
		}
	}

    public function validateCheckBox($attribute,$params){
		if ($this->$attribute!=0)
			$this->$attribute=1;
	}

	public function checkItemType () {
		if (($this->itemType==2) && (empty($this->parent)))
			$this->addError('itemType', 'Выберите пункт "Добавить помещение в существующий центр недвижимости" или начните заново');
	}

	public function requireByType ($attribute,$params) {
		if ( ($this->kind<2) && ($attribute=='storey') && ($this->itemType!=1) && ((empty($this->storey))||($this->storey=='')||($this->storey==0)) )
			$this->addError($attribute, 'Укажите обязательно этаж');
		if ( ($this->kind>0) && ($attribute=='kind_structure') && ((empty($this->kind_structure))||($this->kind_structure=='')||($this->kind_structure==0)) )
			$this->addError($attribute, 'Укажите обязательно Тип сооружения');
		if ( ($this->kind>0) && ($attribute=='ceiling_height') && ((empty($this->ceiling_height))||($this->ceiling_height=='')||($this->ceiling_height==0)) )
			$this->addError($attribute, 'Укажите обязательно значение поля "Общая высота потолков"');
		if ( ($this->kind>1) && ($attribute=='el_power') && ((empty($this->el_power))||($this->el_power=='')||($this->el_power==0)) )
			$this->addError($attribute, 'Укажите обязательно значение поля "Эл. мощность"');
		/*
		if ( ($this->kind!=2) && ($attribute=='access') && ((empty($this->access))||($this->access=='')) )
			$this->addError($attribute, 'Обязательно напишите о Пропускной системе');
		*/
		if ( ($this->kind==1) && ($attribute=='ventilation') && ((empty($this->ventilation))||($this->ventilation=='')||($this->ventilation==0)) )
			$this->addError($attribute, 'Укажите обязательно Систему вентиляции');
		if ( ($this->kind>1) && ($this->itemType!=1) && ($attribute=='flooring') && ((empty($this->flooring))||($this->flooring=='')||($this->flooring==0)) )
			$this->addError($attribute, 'Укажите обязательно Покрытие пола');
	}

	public function checkStoreys () { // Этажность
		if (($this->itemType!=2) && ((empty($this->storeys))||($this->storeys=='')||($this->storeys==0)))
			$this->addError('storeys', 'Укажите обязательно этажность. Значение поля не может быть равно нулю.');
	}
	public function checkStorey ($attribute,$params) { // Этаж
		if (isset($this->storeys) && ($this->storeys!='') && ($this->storey>0) && isset($this->storey) && ($this->storey!='') && ($this->storey>0) && ((integer)$this->storey>(integer)$this->storeys))
			$this->addError($attribute, 'Вы указали Этаж больше значения поля "Этажность"');
	}

	/*
	public function checkSquare ($attribute,$params) {
		if (($this->itemType!=1) && ((empty($this->total_item_sqr))||($this->total_item_sqr=='')||($this->total_item_sqr==0))) 
			$this->addError($attribute, 'Укажите обязательно "Общую площадь помещения".');
		if (($this->itemType==1) && ((empty($this->total_obj_sqr))||($this->total_obj_sqr=='')||($this->total_obj_sqr==0))) 
			$this->addError($attribute, 'Укажите обязательно "Общую площадь объекта".');
		if (isset($this->total_obj_sqr) && ($this->total_obj_sqr!='') && ($this->total_obj_sqr>0) && ((float)$this->total_item_sqr>(float)$this->total_obj_sqr))
			$this->addError($attribute, 'Вы указали "Общую площадь помещения" больше значения поля "Общая площадь объекта"');
		if (isset($this->square) && ($this->square!='') && ($this->square>0) && ((float)$this->square>(float)$this->total_item_sqr))
			$this->addError($attribute, 'Вы указали значение "Полезная площадь" больше значения поля "Общая площадь помещения"');
	}
	*/

	public function attributeLabels(){
	return array(
		'parent'		=>'Спасибо, что проверили',
		'kind'			=>'Вид объекта',
		'city'			=>'Город',
		'kind_structure'=>'Тип сооружения',
		'functionality'	=>'Функциональное назначение',
		'class'			=>'Класс',
		'name'			=>'Название',
		'district'		=>'Район',
		'street'		=>'Улица',
		'house'			=>'Номер дома (строения)',
		'housing'		=>'Корпус',
		'storey'		=>'Этаж',
		'storeys'		=>'Этажность',

		'contr_condition'=>'Форма договора',
		'price'			=>'Арендная ставка (цена?)',
		'pay_utilities'	=>'Коммунальные услуги',
		'total_item_sqr'=>'Площадь помещения',
		'service_lift'	=>'Грузовой лифт',
		'internet'		=>'Наличие сети Интернет',
		'providers'		=>'Интернет-провайдер',
		'telephony'		=>'Наличие телефона',
		'tel_company'	=>'Телефонная компания',
		'ceiling_height'=>'Общая высота потолков',
		'work_height'	=>'Рабочая высота помещения',
		'cathead'		=>'Кран-балка',
		'floor_load'	=>'Нагрузка на пол',
		'el_power'		=>'Эл. мощность',
		'gates_qty'		=>'Количество ворот',
		'gates_height'	=>'Высота ворот (в метрах?)',

		'repair'		=>'Состояние ремонта',
		'parking'		=>'Паркинг',
		'fixed_qty'		=>'Количество закрепленных за помещением м/м',
		'video'			=>'Видеонаблюдение',
		'access'		=>'Пропускная система',
		'welfare'		=>'Бытовые помещения',
		'heating'		=>'Система отопления и водоснабжения',
		'pallet_capacity'=>'Емкость паллетного хранения',
		'shelf_capacity'=>'Емкость полочного хранения',
		'flooring'		=>'Покрытие пола',
		'ventilation'	=>'Система вентиляции',
		'air_condit'	=>'Кондиционирование',
		'firefighting'	=>'Система пожаротушения',
		'can_reclame'	=>'Возможность рекламы',
		'rampant'		=>'Пандус',
		'autoways'		=>'автомобильные',
		'railways'		=>'железнодорожные',
		'about'			=>'Дополнительная информация',
		
		'image'         =>'Загрузка фотографий',
		'docums'		=>'Добавление файлов'
		);
	}

}?>