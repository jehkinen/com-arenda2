<?php class Search extends CActiveRecord{

	public $login;
	public $firstname;
	public $lastname;
	public $showQty;
	public $hasChild;
	public $hasFoto;
	public $ownerId;
	public $ownerPhone;
	public $inNotepad;

	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function tableName(){
		return 'items';
	}

}?>