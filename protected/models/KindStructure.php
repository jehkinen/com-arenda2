<?php
  class KindStructure extends CActiveRecord{

    public static function model($className=__CLASS__){
      return parent::model($className);
    }

    public function tableName(){
      return 'kind_structure';
    }

	public function rules(){
	return array(
		array('name, shname', 'required', 'message'=>'Введите поле "{attribute}".'),
		array('name, shname', 'match', 'pattern' => '/^[ёа-я0-9. \/-]+$/ui', 'message' => '{attribute} содержит недопустимые символы.'),
		array('name, shname', 'safe')
		);
	}

    public function attributeLabels(){
      return array(
		'name' =>'Имя типа сооружения',
		'shname' =>'Краткое имя типа сооружения'
		);
    }

  }
?>