<?php class ReclameItem extends CActiveRecord{
	public $whoLogin;
	public $whoName;
	public $whoSoName;
	public $who_id;
	public $totalQty;

	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function tableName(){
		return 'reclame_item';
	}

	public function rules(){
		return array(
			array('item, name, txt, isActive, onMain, onSearch, onAny, start, stop, qty', 'required', 'message'=>'Введите {attribute}.'),
			array('item, name, txt, isActive, onMain, onSearch, onAny, start, stop, qty', 'safe'),
			array('item', 'validateItem', 'on'=>'insert'),
			array('name', 'match', 'pattern'=>'/^[ёа-яa-z0-9,.()№"_! \/-]+$/ui', 'message'=>'Поле {attribute} содержит недопустимые символы.'),
			array('isActive, onMain, onSearch, onAny', 'validateCheckBox'),
			array('item', 'numerical', 'integerOnly'=>true, 'min'=>0, 'message'=>'Поле "{attribute}" непонятно.'),
			array('qty', 'numerical', 'integerOnly'=>true, 'min'=>0, 'max'=>65400, 'message'=>'Поле "{attribute}" в недопустимом диапазоне.'),
			array('start, stop', 'checkDateAttr')
		);
	}

    public function validateCheckBox($attribute,$params){
		if ($this->$attribute!=0)
			$this->$attribute=1;
	}

	public function checkDateAttr($attribute,$params){
		list ($year, $month, $day) = explode('-', $this->$attribute);
		$flag = ( (is_numeric ($day)) and (is_numeric ($month)) and (is_numeric ($year)) );
		if ($flag) $flag = ( ($year > 1920) and ($year < 2100) );
		if ($flag) $flag = checkdate ($month, $day, $year);
		if (!$flag) {
			$labels = $this->attributeLabels();
			$this->addError($attribute, 'Некорректная дата в поле '.$labels[$attribute]);
		}
	}

	public function validateItem(){
		$item = Search::model()->findByPK($this->item);
		if (empty($item)) 
			$this->addError('item', 'Объекта с таким Порядковым Ромером не существует');
		else {
			$fotos = Images::model()->count('id_item=:it', array(':it'=>$this->item));
			if ((empty($fotos)) || ($fotos<3))
				$this->addError('item', 'У Объекта меньше трех фотографий');
		}
	}

	public function attributeLabels(){
		return array(
			'name'		=>'Наименование (надпись над фотками)',
			'txt'		=>'Рекламный текст',
			'item'		=>'Порядковый номер рекламируемого объекта',
			'isActive'	=>'Активна ли кампания (ручное управление)',
			'onMain'	=>'Показывать ли на Главной странице',
			'onSearch'	=>'Показывать ли на страницах с Результатами поиска и Описанием объектов',
			'onAny'		=>'Показывать ли на всех других страницах',
			'start'		=>'Дата Начала показа кампании (формат дат: ДД.ММ.ГГГГ)',
			'stop'		=>'Дата Окончания показа кампании',
			'qty'		=>'Кол-во показов (сколько раз показывать рекламу объекта)'
		);
	}

}?>