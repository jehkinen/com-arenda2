<?php class User_addcontacts extends CActiveRecord{

	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function tableName(){
		return 'users';
	}

	public function rules(){
		return array(
			array('city, skype, icq', 'safe'),
			array('city, skype, icq', 'my_length', 'len'=>31, 'mes'=>'Поле [name] не может превышать [len] символов')
		);
	}

	public function my_length($attribute,$params) {
		if (strlen($this->$attribute)>$params['len']) {
			$labels = $this->attributeLabels();
			$s = str_replace('[name]', $labels[$attribute], $params['mes']);
			$s = str_replace('[len]', $params['len'], $s);
			$this->addError($attribute, $s);
		}
	}

	public function attributeLabels(){
		return array(
			'city'	=> 'Ваш город',
			'skype'	=> 'Skype',
			'icq'	=> 'ICQ'
		);
	}
}?>