<?php class User extends CActiveRecord{

	public $verifyCode;
	#public $cooki;
	public $error;

	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function tableName(){
		return 'users';
	}

	public function rules(){
		return array(
			array('username', 'required', 'message'=>'Введите логин.', 'on'=>'next'),
			array('password', 'required', 'message'=>'Введите пароль.', 'on'=>'next'),
			array('password', 'authenticate_first', 'on'=>'first'),
			array('password', 'authenticate', 'on'=>'next'),
			array('verifyCode', 'captcha', 'allowEmpty'=>!extension_loaded('gd'), 'message'=>'Код с картинки введен неверно.', 'on'=>'next'),
			#array('username', 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,]+$/u', 'message' => 'Логин содержит недопустимые символы.'),
		);
	}

	public function authenticate_first($attribute,$params){
		if(!$this->hasErrors()){
			$identity = new UserIdentity($this->username, $this->password);
			$identity->authenticate();
			switch($identity->errorCode){
				case UserIdentity::ERROR_NONE:{				/*if ($_POST['User']['cooki'] == '1'){					 Yii::app()->user->login($identity,3600*24*7);				}else{					 Yii::app()->user->login($identity,0);				}*/
				Yii::app()->user->login($identity,0);
				break;
				}
				case UserIdentity::ERROR_USERNAME_INVALID:{ $this->error = '100'; break; }
				case UserIdentity::ERROR_PASSWORD_INVALID:{ $this->error = '200'; break; }
				case UserIdentity::ERROR_USER_DELETE : { $this->error = '250'; break; }
				case UserIdentity::ERROR_USER_LOCK :   { $this->error = '260'; break; }
				case UserIdentity::ERROR_NO_ACTIVATE : { $this->error = '270'; break; }
				case UserIdentity::ERROR_FLOOD :			{ $this->error = '299'; break; }
				default:
				$this->error = '300';
			}
		}
	}

	public function authenticate($attribute,$params){
		if(!$this->hasErrors()){
			$identity = new UserIdentity($this->username, $this->password);
			$identity->authenticate();
			switch($identity->errorCode){
				case UserIdentity::ERROR_NONE:{
				/*if ($_POST['User']['cooki'] == '1'){
					 Yii::app()->user->login($identity,3600*24*7);
				}else{
					 Yii::app()->user->login($identity,0);
				}*/
				Yii::app()->user->login($identity,0);
					break;
				}
				case UserIdentity::ERROR_USERNAME_INVALID:{
					$this->addError('username','Пользователь не существует!');
					break;}
				case UserIdentity::ERROR_PASSWORD_INVALID:{
					$this->addError('password','Вы указали неверный пароль!');
					break;}
			}
		}
	}

	public function attributeLabels(){
		return array(
			'password' =>'Пароль:',
			'username' =>'Логин:',
			#'cooki'	=>' запомнить меня',
		);
	}

  }
?>