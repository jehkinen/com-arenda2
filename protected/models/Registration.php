<?php class Registration extends CActiveRecord{
	public $verifyCode;
	public $confirm;
	public $rules;

	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function tableName(){
		return 'users';
	}

	public function rules(){
		return array(
			array('name, so_name, password, confirm, username, e_mail, phone', 'required', 'message'=>'строка {attribute} не должна быть пустой.'),
			array('organization', 'match', 'pattern' => '/^[ёа-я0-9a-zA-Z" -]+$/ui', 'message' => '{attribute} содержит недопустимые символы.'),
			array('name, so_name, otchestvo', 'match', 'pattern' => '/^[ёа-я -]+$/ui', 'message' => '{attribute} содержит недопустимые символы.'),
			array('username', 'match', 'pattern' => '/^[a-zA-Z0-9_]+$/ui', 'message' => '{attribute} содержит недопустимые символы.'),
			#array('password', 'match', 'pattern' => '/^[a-zA-Z0-9_]+$/ui', 'message' => '{attribute} содержит недопустимые символы.'),

			array('e_mail', 'email', 'message'=>'{attribute} имеет недопустимый формат.'),
			array('e_mail', 'chek_mail'),
			array('username, password, confirm', 'my_range_length', 'maxlen'=>10, 'minlen'=>5, 'mes'=>'Поле [name] не может [verb] [len] символов'),			array('username', 'chek_user'),
			array('confirm', 'compare', 'compareAttribute'=>'password', 'message'=>'Пароли не совпадают.'),
			array('verifyCode', 'captcha', 'allowEmpty'=>!extension_loaded('gd'), 'message'=>'Код с картинки введен неверно.'),
			array('username, password, role, name, so_name, otchestvo, e_mail, phone, organization, rules', 'safe'),
		);
	}

	public function chek_user($username){
		$u = trim($username);
		if (preg_match('/administrator/i', $u)) {
			$this->addError('username', 'Такой пользователь уже существует.');
			return;
		}
		$criteria = new CDbCriteria;
		$criteria->condition = 'username=:u';
		$criteria->params = array(':u'=>$u);
		$User_obj = User::model()->find($criteria);
		if (!empty($User_obj))
			$this->addError('username', 'Такой пользователь уже существует.');
	}

	public function chek_mail($e_mail){
		$criteria = new CDbCriteria;
		$criteria->condition = 'e_mail=:varible';
		$criteria->params = array(':varible'=>$this->e_mail);
		$User_obj = User::model()->find($criteria);
		if (!empty($User_obj))
			$this->addError('e_mail', 'Такой e-mail уже существует.');
	}

	public function my_range_length($attribute,$params) {		if (strlen($this->$attribute)>$params['maxlen']) {			$labels = $this->attributeLabels();			$s = str_replace('[name]', $labels[$attribute], $params['mes']);			$s = str_replace('[len]', $params['maxlen'], $s);			$s = str_replace('[verb]', 'превышать', $s);			$this->addError($attribute, $s);		}		if (strlen($this->$attribute)<$params['minlen']) {			$labels = $this->attributeLabels();			$s = str_replace('[name]', $labels[$attribute], $params['mes']);			$s = str_replace('[len]', $params['minlen'], $s);			$s = str_replace('[verb]', 'быть меньше', $s);			$this->addError($attribute, $s);		}	}	public function attributeLabels(){
		return array(
			'username'	=>'логин',
			'password'	=>'пароль',
			'confirm'	=>'повторите пароль',
			'role'		=>'тип участника (собственник или арендатор)',
			'name'		=>'имя',
			'so_name'	=>'фамилия',
			'otchestvo'	=>'отчество',
			'e_mail'	=>'e-mail',
			'phone'		=>'Телефон',
			'organization'	=>'организация'
		);
	}

}?>