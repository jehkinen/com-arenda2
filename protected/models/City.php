<?php
  class City extends CActiveRecord{

    public static function model($className=__CLASS__){
      return parent::model($className);
    }

    public function tableName(){
      return 'cities';
    }

	  public function relations()
	  {
		  // NOTE: you may need to adjust the relation name and the related
		  // class name for the relations automatically generated below.
		  return array(
			  'areas' => array(self::HAS_ONE, 'Areas', 'id'),
		  );
	  }

	public function rules(){
	return array(
		array('name', 'required', 'message'=>'Введите поле "{attribute}".'),
		array('area', 'type', 'type'=>'integer'),
		array('name', 'match', 'pattern' => '/^[ёа-я0-9. \/-]+$/ui', 'message' => '{attribute} содержит недопустимые символы.'),
		array('name', 'safe')
		);
	}

    public function attributeLabels(){
      return array(
		'name' =>'Название города',
		'area' =>'Область',
		);
    }

  }
?>