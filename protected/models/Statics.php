<?php
  class Statics extends CActiveRecord{
    public $maximum;

    public static function model($className=__CLASS__){
      return parent::model($className);
    }

    public function tableName(){
      return 'statics';
    }

    public function rules(){
      return array(
             array('name', 'required', 'message'=>'Введите {attribute}.', 'on'=>'add'),
             array('name', 'match', 'pattern'=>'/^[a-zA-Z0-9_]+$/', 'message' => '{attribute} содержит недопустимые символы.', 'on'=>'add'),
             array('name', 'chek_name', 'on'=>'add'),
             array('name_ru', 'required', 'message'=>'Введите {attribute}.', 'on'=>'add'),
             array('name, name_ru, text_page, title, description, keywords, menu', 'safe'),
      );
    }

    public function chek_name($name){

      $criteria = new CDbCriteria;
      $criteria->condition = 'name=:var';
      $criteria->params = array(':var'=>$this->name);
      $Static_obj = Statics::model()->find($criteria);
      if (!empty($Static_obj))
        $this->addError('name','Такое название уже существует.');

    }

    public function attributeLabels(){
      return array(
                   'name'        =>'Название страницы(англ)',
                   'name_ru'     =>'Название страницы(рус)',
                   'text_page'   =>'Текст',
                   'title'       =>'Title',
                   'description' =>'Description',
                   'keywords'    =>'Keywords',
                   'menu'        =>'Страница на гравном меню',
                   );
    }
  }
?>