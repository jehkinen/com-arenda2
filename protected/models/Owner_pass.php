<?php class Owner_pass extends CActiveRecord{
	public $confirm;

	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function tableName(){
		return 'users';
	}

	public function rules(){
		return array(
			array('password, confirm', 'required', 'message'=>'строка {attribute} не должна быть пустой.'),
			#array('password, confirm', 'match', 'pattern' => '/^[a-zA-Z0-9_]+$/ui', 'message' => '{attribute} содержит недопустимые символы.'),
			array('password, confirm', 'length', 'max'=>10, 'min' =>3, 'message'=>'{attribute} должен содержать от 3 до 10 символов'),
			array('confirm', 'compare', 'compareAttribute'=>'password', 'message'=>'Пароли не совпадают.'),
		);
	}

	public function attributeLabels(){
		return array(
			'password' =>'Новый пароль',
			'confirm'  =>'Повторите новый пароль'
		);
	}

}?>