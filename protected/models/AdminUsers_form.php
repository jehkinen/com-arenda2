<?php
class AdminUsers_form extends CFormModel{
	public $status;
	public $role;

	public function rules(){
	return array(
		array('status', 'numerical', 'integerOnly'=>true),
		array('role, status', 'safe')
		);
	}


	public function attributeLabels(){
		return array(
			'status'	=>'Статус',
			'role'		=>'Роль'
		);
	}

	}
?>