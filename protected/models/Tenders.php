<?php class Tenders extends CActiveRecord{

	public $whoName;
	public $whoSoName;
	public $whoOtchestvo;
	public $whoPhone;

	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function tableName(){
		return 'tenders';
	}

	public function rules(){
	return array(
		array('kind, who_id, district, kind_structure, functionality, class, street, service_lift, repair, parking, internet, telephony, heating, cathead, ventilation, firefighting, rampant, autoways, railways', 'numerical', 'integerOnly'=>true, 'min'=>0, 'message'=>'Поле "{attribute}".', 'on'=>'insert'),
		array('storeyMin, storeyMax', 'numerical', 'integerOnly'=>true, 'message'=>'Поле "{attribute}".', 'on'=>'insert'),
		array('squareMin, squareMax, priceMin, priceMax, ceiling_height1, ceiling_height2, gates_height1, gates_height2', 'numerical', 'integerOnly'=>false, 'min'=>0, 'message'=>'Поле "{attribute}".', 'on'=>'insert'),
		array('city', 'required', 'message'=>'Введите поле "{attribute}".', 'on'=>'insert'),
		array('city', 'match', 'pattern' => '/^[ёа-я0-9. \/-]+$/ui', 'message' => '{attribute} содержит недопустимые символы.', 'on'=>'insert')
		);
	}

}?>