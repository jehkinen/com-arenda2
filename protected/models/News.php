<?php class News extends CActiveRecord{	public $image;
	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function tableName(){
		return 'news';
	}

	public function rules(){
		return array(
			array('header, anounce, body', 'required', 'message'=>'Введите {attribute}.'),
			array('stick, header, anounce, body, keywords, description', 'safe'),			array('image', 'checkImage')
		);
	}

	public function checkImage(){		if(!$this->hasErrors()){			$img = $this->image;			if (!empty($img)){				$validExts = array('.jpg','.jpeg','.png','.gif');				$ext = substr($img->name, strrpos($img->name, '.'));				$ext = mb_strtolower($ext, 'UTF-8');				if (!in_array($ext, $validExts))					$this->addError('image', 'Титульная фотография имеет неверный тип. Допустимые форматы: *jpg, *jpeg, *png, *gif');				if ($img->size > 5242880)					$this->addError('image', 'Размер титульной фотографии больше 5Мб.');				$image_size = getimagesize($img->tempName);				if ($image_size[0] > 510)					$this->addError('image', 'Ширина титульной фотографии больше 510 пикселов.');			}		}	}	public function attributeLabels(){
		return array(
			'stick'		=>'Закрепить?',			'header'	=>'Заголовок',			'anounce'	=>'Анонс',			'body'		=>'Текст',			'keywords'	=>'Ключевые слова (для SEO)',			'description'=>'Описание страницы (для SEO)',			'image'		=>'Титульная картинка'
		);
	}
}?>