<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>index</title>
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/carousel.css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.jcarousel.js"></script>
<script type="text/javascript" src="js/jquery.lightbox_me.js"></script>
<script type="text/javascript" src="js/client.js"></script>
<script type="text/javascript" src="js/new.js"></script>
</head>

<body>
<div class="wrap_for_max_width">
	<div class="top_wrap green_border_bottom">
   		<div class="rounded dotted user_inf">
        	<div><a href="">Выход</a>Екатерина Медведева-Двойная</div>
			<div><a href="">Личный кабинет</a>ООО "Рога и Копыта"</div>
        </div>
    	<a href="" class="logo"><img src="images/logo.png" /></a>
        <div class="city_select_wrap">
            <a class="city_select popup_link" href="#city_select">Екатеринбург</a>
            <div class="popup" id="city_select">
           		<a class="popup_close"></a>
                <ul class="city_select_list">
                    <li><a>Асбест</a></li>
                    <li><a>Берёзовский</a></li>
                    <li><a>Верхняя Пышма</a></li>
                    <li><a>Екатеринбург</a></li>
                    <li><a>Каменск-Уральский</a></li>
                    <li><a>Краснотурьинск</a></li>
                    <li><a>Нижний Тагил</a></li>
                    <li><a>Новоуральск</a></li>
                    <li><a>Первоуральск</a></li>
                    <li><a>Полевской</a></li>
                    <li><a>Ревда</a></li>
                    <li><a>Серов</a></li>
                    <li class="add_city">
                        Нет вашего города?
                        <a>Добавить...</a>
                    </li>
                </ul>	
            </div>
        </div>
        <div class="inf_block">
        	всего объектов<br />
			в аренду 1460
        </div>
        <div class="inf_block">
        	Телефон в Екатеринбурге<br />
			(343) 344-60-27
        </div>
        <div class="menu_wrap">
            <ul class="main_menu rounded">
                <li><a href="">О проекте</a></li>
                <li><a href="">Контакты</a></li>
                <li><a href="">Справка</a></li>
                <li><a href="">Пункт меню</a></li>
                <li><a href="">Еще пункт</a></li>
            </ul>
            
            <a href="" class="green_link_block">Как работает com-arenda?</a>
            <a href="" class="green_link_block">Разместить объект</a>
        </div>
    </div>
    
    <div class="content_wrap">
    	<div class="aside">
            <div class="gray_block">
            	Знаете ли вы, что: Ученые обнаружили следы ранее неизвестной рептилии, обитавшей около 245 миллионов лет назад на территории современной Антарктики.
            </div>
            <div class="gray_block">
            	Почему кошка приносит нам мышей? По мнению кошки, ее хозяева по части ловли мышей являются, говоря школьным языком, настоящими «двоечниками». Но кошка готова оказать нам в этом сложном деле посильную помощь. Сытая кошка делится добычей со своими собратьями.
            </div>
            <div class="gray_block">
            	Член полярной экспедиции Георгия Седова, штурман корабля "Святой Фока" Н. Сахаров, обязан жизнью своей преданной собаке Штурке. Сахаров отморозил руки и добирался до корабля один в сопровождении собаки. Выбиваясь из сил, прошел по льду пятьдесят километров, временами терял сознание, впадая в забытье. Тогда Штурка садился возле хозяина, лаял, теребил его за одежду, пытаясь привести друга в чувства. Сахаров поднимался и продолжал идти.
            </div>
            
        </div>
				<div class="content">
					<div class="story">
						<div>
							<div class="attention02">
								<img class="attention02_img" src="/images/attention01.png"/><h3 class="attention02_h3">
									Заголовок сообщения вторая строка
								</h3><p class="attention02_p">
									Если у вас есть много помещений в одном месте бизнес центр, логистический или торгово-развлекательный центр с множеством помещений различного назначения создайте «центр недвижимости» помещения, добавляемыее в него будут наследовать характеристики
									описанные <a>тут</a>
								</p>
							</div><div class="chapter01">
								<h1>
									Заголовок, болд, 21 пт
								</h1><p>
									Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, что он у себя в постели превратился в страшное насекомое. Лежа на панцирнотвердой спине, он видел, стоило ему приподнять голову, свой коричневый, выпуклый, разделенный дугообразными чешуйками живот, на верхушке которого еле держалось готовое вот-вот окончательно сползти одеяло.
								</p><p>
									Его многочисленные, убого тонкие по сравнению с остальным телом ножки беспомощно копошились у него перед глазами. «Что со мной случилось? » - подумал он. Это не было сном. Его комната, настоящая, разве что слишком маленькая, но обычная комната, мирно покоилась в своих четырех хорошо знакомых стенах.
								</p>
							</div>
						</div><div>
							<div class="note02">
								<p class="note02_p">
									Текст сноски, пояснение к тексту или подсказка<br/><br/><br/><br/><br/>
								</p>
							</div><div class="chapter01">
								<h2>
									Заголовок, болд, 16пт
								</h2><p>
									Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, что он у себя в постели превратился в страшное насекомое. Лежа на панцирнотвердой спине, он видел, стоило ему <a>приподнять</a> голову, свой коричневый, выпуклый, разделенный дугообразными чешуйками живот, на верхушке которого еле держалось <a>готовое</a> вот-вот окончательно сползти одеяло.
								</p>
							</div>
						</div><div>
							<div class="note02">
								<p class="note02_p">
									Текст сноски, пояснение к тексту или подсказка<br/><br/><br/><br/><br/>
								</p>
							</div><div class="chapter01">
								<h3>
									Заголовок, болд, 14пт
								</h3><p>
									Его многочисленные, убого тонкие по сравнению с остальным телом ножки беспомощно копошились у него перед глазами. «Что со мной случилось? » - подумал он. Это не было сном. Его комната, настоящая, разве что слишком маленькая, но обычная комната, мирно покоилась в своих четырех хорошо знакомых стенах.
								</p>
							</div>
						</div><div>
							<div class="chapter01">
								<div class="attention01">
									<img class="attention01_img" src="/images/attention01.png"/><h3 class="attention01_h3">
										Заголовок сообщения вторая строка
									</h3><p class="attention01_p">
										Если у вас есть много помещений в одном месте бизнес центр, логистический или торгово-развлекательный центр с множеством помещений различного назначения создайте «центр недвижимости» помещения, добавляемыее в него будут наследовать характеристики описанные <a>тут</a>
									</p>
								</div>
							</div>
						</div><div>
							<div class="chapter01">
								<p>
									<img src="/images/property_thumb.png" style="float:left;"/>Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, что он у себя в постели превратился в страшное насекомое. Лежа на панцирнотвердой спине, он видел, стоило ему <a>приподнять</a> голову, свой коричневый, выпуклый, разделенный дугообразными чешуйками живот, на верхушке которого еле держалось <a>готовое</a> вот-вот окончательно сползти одеяло. Его многочисленные, убого тонкие по сравнению с остальным телом ножки беспомощно копошились у него перед глазами. «Что со мной случилось? » - подумал он. Это не было сном. Его комната, настоящая, разве что слишком маленькая, но обычная комната, мирно покоилась в своих четырех хорошо знакомых стенах.
								</p>
							</div>
						</div><div>
							<div class="chapter01">
								<div class="note01">
									<h3 class="note01_h3">Текст сноски, пояснение к тексту или подсказка</h3><p class="note01_p">
										Его многочисленные, убого тонкие по сравнению с остальным телом ножки беспомощно копошились у него перед глазами. «Что со мной случилось? » - подумал он. Это не было сном. Его комната, настоящая, разве что слишком маленькая, но обычная комната, мирно покоилась в своих четырех хорошо знакомых стенах.
									</p>
								</div>
							</div>
						</div><div>
							<div class="chapter01">
								<p>
									Его многочисленные, убого тонкие по сравнению с остальным телом ножки беспомощно копошились у него перед глазами. «Что со мной случилось? » - подумал он. Это не было сном. Его комната, настоящая, разве что слишком маленькая, но обычная комната, мирно покоилась в своих четырех хорошо знакомых стенах.
								</p>
							</div>
						</div><div>
							<div class="chapter01">
								<h3>
									Маркированный список
								</h3><ul>
									<li>
										Без ограничений
									</li><li>
										Удобно
									</li><li>
										Оперативно<ul>
											<li>
												Без ограничений
											</li><li>
												Удобно
											</li><li>
												Оперативно<!--<ul>
													<li>
														Без ограничений
													</li><li>
														Удобно
													</li><li>
														Оперативно
													</li>
												</ul>-->
											</li>
										</ul>
									</li>
								</ul>
							</div>
						</div><div>
							<div class="chapter01">
								<h3>
									Нумерованный список
								</h3><ol>
									<li>
										Без ограничений
									</li><li>
										Удобно
									</li><li>
										Оперативно<ol>
											<li>
												Без ограничений
											</li><li>
												Удобно
											</li><li>
												Оперативно<!--<ol>
													<li>
														Без ограничений
													</li><li>
														Удобно
													</li><li>
														Оперативно
													</li>
												</ol>-->
											</li>
										</ol>
									</li>
								</ol>
							</div>
						</div><div>
							<div class="chapter01">
								<table>
									<thead>
										<tr>
											<th>
												Заголовок колонки
											</th><th>
												Заголовок колонки
											</th><th>
												Заголовок колонки
											</th>
										</tr>
									</thead><tbody>
										<tr>
											<td>
												Заголовок строки
											</td><td>
												Заголовок строки
											</td><td>
												Заголовок строки
											</td>
										</tr><tr class="even">
											<td>
												Заголовок строки
											</td><td>
												Заголовок строки
											</td><td>
												Заголовок строки
											</td>
										</tr><tr>
											<td>
												Заголовок строки
											</td><td>
												Заголовок строки
											</td><td>
												Заголовок строки
											</td>
										</tr><tr class="even">
											<td>
												Заголовок строки
											</td><td>
												Заголовок строки
											</td><td>
												Заголовок строки
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div><div>
							<div class="chapter01">
								<div class="form01">
									<h3 class="form01_h3">
										Контактная информация
									</h3><div class="p_0010_0000_0010_0000">
										<div class="input01_lbl w130">Имя</div><input class="input01 w300" type="text" style="vertical-align:middle;"/> <b class="cB83939 vt">*</b>
									</div><div class="p_0010_0000_0010_0000">
										<div class="input01_lbl w130">Контактный телефон</div><input class="input01 w300" type="text" style="vertical-align:middle;"/> <b class="cB83939 vt">*</b>
									</div><div class="p_0010_0000_0010_0000">
										<div class="input01_lbl w130">E-mail</div><input class="input01 w300" type="text" style="vertical-align:middle;"/> <b class="cB83939 vt">*</b>
									</div><div class="p_0010_0000_0010_0000">
										<textarea class="h90 textarea02 w438"></textarea>
									</div><div class="p_0010_0000_0010_0000 tr">
										<input class="button02" type="button" value="Добавить"/>
									</div>
								</div>
							</div>
						</div>
					<div class="green_top_border_block clear_both">
						<div class="also_to_view_wrap">
							<div class="recommend"><span>Рекомендуем к просмотру</span><div class="border"></div></div>
							<ul class="also_to_view">
								<li>
									<a href=""><img src="content_images/slider_1.jpg"/><div>Офис<br/>Центр, 220м<sup>2</sup></div></a>
								</li><li>
									<a href=""><img src="content_images/slider_2.jpg"/><div>Торговая площадь<br/>Академический, 300м<sup>2</sup></div></a>
								</li><li>
									<a href=""><img src="content_images/slider_3.jpg"/><div>Торговая площадь<br/>Завокзальный, 256м<sup>2</sup></div></a>
								</li><li>
									<a href=""><img src="content_images/slider_1.jpg"/><div>Торговая площадь<br/>Завокзальный, 256м<sup>2</sup></div></a>
								</li><li>
									<a href=""><img src="content_images/slider_2.jpg"/><div>Торговая площадь<br/>Академический, 300м<sup>2</sup></div></a>
								</li><li>
									<a href=""><img src="content_images/slider_3.jpg"/><div>Торговая площадь<br/>Завокзальный, 256м<sup>2</sup></div></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
    </div>
</div>
    <div class="footer">
        <div class="wrap_for_max_width">
            <img class="small_logo" src="images/logo_small.png" />
            © 2012 Портал Ком-Аренда<br />
            Контактный телефон: (343) 344-60-27<br />
            <a href="mailto:office@com-arenda.ru">office@com-arenda.ru</a>
        </div>
    </div>


<div id="gallery_popup">
	<a class="gallery_close"></a>
	<div class="gallery_big_image">
    	<img src="content_images/gallery_1_big.jpg" />
    </div>
	<ul id="gallery_carousel" class="jcarousel-skin-tango">
    	<li><a rel="content_images/gallery_1_big.jpg"><img src="content_images/gallery_1.jpg" /></a></li>
		<li><a rel="content_images/gallery_2_big.jpg"><img src="content_images/gallery_2.jpg" /></a></li>
		<li><a rel="content_images/gallery_3_big.jpg"><img src="content_images/gallery_3.jpg" /></a></li>
		<li><a rel="content_images/gallery_4_big.jpg"><img src="content_images/gallery_4.jpg" /></a></li>
        <li><a rel="content_images/gallery_5_big.jpg"><img src="content_images/gallery_5.jpg" /></a></li>
		<li><a rel="content_images/gallery_6_big.jpg"><img src="content_images/gallery_6.jpg" /></a></li>
		<li><a rel="content_images/gallery_7_big.jpg"><img src="content_images/gallery_7.jpg" /></a></li>
    </ul>
</div>


</body>
</html>