
<script>
	$(function(){
        $('#SendOrder_city').attr('readonly', true);

        hide_popup();
        /*При клике на город заполняем значение инпута */
        $('.city_list li').click(function(){
            var city = $(this).text();
            $(this).parent().find('li').removeClass('active');
            $(this).addClass('active');
            $('#SendOrder_city').val(city);
            $('#Popup2Close').click();
        });

        function show_popup(){
            $('#Popup2').show();
            $('#Popup2').css('visibility', 'visible');
            $('#Popup2Fog').show();
        }
        function hide_popup(){
            $('#Popup2').hide();
            $('#Popup2Fog').hide();
        }

        $('#Popup2Close, #Popup2Fog').click(function(){
            hide_popup();
        });

        $('#SendOrder_city').click(function(){
            show_popup();
        });

    });
</script>


<h1 class="h101">Размещение объекта</h1>
<div class="desc01">
    Данная страница предназначена для быстрого размещения Ваших объектов и не требует регистрации на сайте. После размещения модератор сайта свяжется с Вами и уточнит необходимые детали. <?php if (Yii::app()->user->isGuest):?>Напоминаем, что <?php echo CHtml::link('регистрация', array('registration/view'), array('class'=>'link01'));?> позволит Вам пользоваться всем функционалом сайта.<?php endif;?><br />
    Подробнее о всех возможностях сайта Вы можете узнать на странице <a class="link01" href="/page/how_work.html">Как работает com-arenda</a>
</div>


<?php echo CHtml::form('', 'post', array('enctype'=>'multipart/form-data'));?>


<?php if($form->hasErrors()):?>
<div class="form01_grp">
    <div class="form01_blk">
        <div class="errorFormat02">
			<?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:');?>
        </div>
    </div>
</div>
<?php endif;?>
<div class="form01_grp">
    <div class="form01_blk bFFF">
        <h1>Сдаю:</h1>
        <div class="m_0010b"><label class="bOffice checkbox02_lbl" onclick="checkboxBg(event);"><input class="checkbox01 m_0000_0030_0000_0000" name="SendOrder[kind]" type="radio" value="0"<?php echo ($kind==0)?' checked="checked"':'';?> />Офис
        </label><label class="bShop checkbox02_lbl" onclick="checkboxBg(event);"><input class="checkbox01 m_0000_0030_0000_0000" name="SendOrder[kind]" type="radio" value="1"<?php echo ($kind==1)?' checked="checked"':'';?> />Торговое помещение
        </label><label class="bEnterprise checkbox02_lbl" onclick="checkboxBg(event);"><input class="checkbox01 m_0000_0030_0000_0000" name="SendOrder[kind]" type="radio" value="2"<?php echo ($kind==2)?' checked="checked"':'';?> />Склад/Производство
        </label></div>
    </div><div class="note01">
    Выберите тип сдаваемого помещения
</div>
</div>
<div class="form01_grp">
    <div class="form01_blk">
        <h1>Основные характеристики помещения:</h1>
        <div>
            <div class="m_0015b">
                <div class="input01_lbl m_0015r">Город <?php echo CHtml::activeTextField($form, 'city', array('class'=>'input01 w90'));?> <b class="cB83939">*</b>
                </div>
                <div class="input01_lbl m_0015r">Район <?php echo CHtml::activeTextField($form, 'district', array('class'=>'input01 w90'));?> <b class="cB83939">*</b>
                </div>
                <div class="input01_lbl">Улица <?php echo CHtml::activeTextField($form, 'street', array('class'=>'input01 w90'));?> <b class="cB83939">*</b>
                </div>
            </div>
            <div class="m_0015b">
                <div class="input01_lbl m_0015r">Дом <?php echo CHtml::activeTextField($form, 'house', array('maxlength'=>4, 'class'=>'input01 w40'));?>
                </div>
                <div class="input01_lbl m_0015r">Этаж <?php echo CHtml::activeTextField($form, 'staging', array('maxlength'=>3, 'class'=>'input01 w30'));?>
                </div>
                <div class="input01_lbl m_0015r">Этажность <?php echo CHtml::activeTextField($form, 'stage', array('maxlength'=>3, 'class'=>'input01 w30'));?>
                </div>
                <div class="input01_lbl">Корпус/Строение <?php echo CHtml::activeTextField($form, 'housing', array('maxlength'=>7, 'class'=>'input01 w50'));?>
                </div>
            </div>
            <div class="m_0005b">
                <div class="input01_lbl m_0015r">Общая площадь помещения <?php echo CHtml::activeTextField($form, 'square', array('maxlength'=>6, 'class'=>'input01 w40'));?>
                </div>
                <div class="input01_lbl m_0015r">Арендная ставка (руб./м<sup>2</sup>) <?php echo CHtml::activeTextField($form, 'price', array('maxlength'=>8, 'class'=>'input01 w60'));?>
                </div>
            </div>
        </div>
    </div><div class="note01">
    Укажите параметры сдаваемого помещения.
</div>
</div>
<div class="form01_grp">
    <div class="form01_blk">
        <h1>Фотографии</h1>
        <div>
            <div class="fupload01 m_0015b" onmousedown="fakeUploadsDown(event);" onmouseup="fakeUploadsUp(event);" onmousemove="fakeUploadsMove(event);" onmouseout="fakeUploadsOut(event);"><?php echo CHtml::activeFileField($form, '[1]fotos', array('class'=>'upload01', 'size'=>'45'));?></div><br />
            <div class="fupload01 m_0015b" onmousedown="fakeUploadsDown(event);" onmouseup="fakeUploadsUp(event);" onmousemove="fakeUploadsMove(event);" onmouseout="fakeUploadsOut(event);"><?php echo CHtml::activeFileField($form, '[2]fotos', array('class'=>'upload01', 'size'=>'45'));?></div><br />
            <div class="fupload01 m_0015b" onmousedown="fakeUploadsDown(event);" onmouseup="fakeUploadsUp(event);" onmousemove="fakeUploadsMove(event);" onmouseout="fakeUploadsOut(event);"><?php echo CHtml::activeFileField($form, '[3]fotos', array('class'=>'upload01', 'size'=>'45'));?></div><br />
            <div class="fupload01 m_0015b" onmousedown="fakeUploadsDown(event);" onmouseup="fakeUploadsUp(event);" onmousemove="fakeUploadsMove(event);" onmouseout="fakeUploadsOut(event);"><?php echo CHtml::activeFileField($form, '[4]fotos', array('class'=>'upload01', 'size'=>'45'));?></div><br />
            <div class="fupload01 m_0015b" onmousedown="fakeUploadsDown(event);" onmouseup="fakeUploadsUp(event);" onmousemove="fakeUploadsMove(event);" onmouseout="fakeUploadsOut(event);"><?php echo CHtml::activeFileField($form, '[5]fotos', array('class'=>'upload01', 'size'=>'45'));?></div><br />
        </div>
        <h1>Дополнительно</h1>
        <div class="m_0005b"><?php echo CHtml::textArea('SendOrder[about]', $about, array('class'=>'h90 textarea01 w440'));?></div>
    </div>
    <div class="note01">
        Добавьте фотографии и описания вашему объекту.
    </div>
</div>
<div class="form01_grp">
    <div class="form01_blk">
        <h1>Контактная информация</h1>
        <div class="m_0015b">
            <div class="input01_lbl w130">Имя</div>
			<?php echo CHtml::activeTextField($form, 'name', array('class'=>'input01 w240', 'style'=>'vertical-align:middle;'));?> <b class="cB83939">*</b>
        </div>
        <div class="m_0015b">
            <div class="input01_lbl w130">Контактный телефон</div>
			<?php echo CHtml::activeTextField($form, 'phone', array('class'=>'input01 w240', 'style'=>'vertical-align:middle;'));?> <b class="cB83939">*</b>
        </div>
        <div class="m_0005b">
            <div class="input01_lbl w130">E-mail</div>
		<?php echo CHtml::activeTextField($form, 'e_mail', array('class'=>'input01 w240', 'style'=>'vertical-align:middle;'));?> <b class="cB83939">*</b>
        </div>
    </div>
    <div class="note01">
        Укажите контактные данные для связи с вами.
    </div>
</div>
<div class="form01_grp">
    <div class="m_0000_0245_0000_0002 w520">
        <h3 class="dl h301 m_0015_0000_0000_0000">Введите результат вычисления</h3>
        <div class="dl"><?php $this->widget('CCaptcha', array('buttonLabel' => ''));?></div>
        <div class="dl">
			<?php echo CHtml::activeTextField($form, 'verifyCode', array('maxlength'=>5, 'class'=>'input01 m_0015_0000_0000_0000 w60'));?><br/>
            <span><a class="captcha01_rfs" id="yt0" href="#">Обновить</a></span>
        </div>
        <!--  <input class="checkbox01 m0000000200000000" type="checkbox"/> -->
        <!--  <span class="checkbox_label02">Я ознакомился с правилами размещения объявлений</span> -->
		<?php echo CHtml::submitButton('Добавить', array('class'=>'button01 m_0010_0000_0000_0000'));?>
    </div>
</div>


<?php echo CHtml::endForm();?>