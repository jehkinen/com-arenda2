<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/form.css'); ?>
Фильтры: <?php echo CHtml::link('Сбросить', array('index/unset')); ?><br />
<div class="form">
	<?php echo CHtml::form(); ?>
	<?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:'); ?>
	<div class="row buttons">
		<br /><?php echo CHtml::submitButton('Найти', array('id'=>'submit')); ?><br />
	</div>
	<?php echo CHtml::endForm(); ?>
</div><!--form-->
<?php if (is_array($rows)):?>
<hr>Найденно объектов: <?php echo $foundQty; ?><br /><br />
<?php if (!empty($rows)):?>
<style type="text/css">
table.sample {
  border-spacing: 2px;
  border-collapse: collapse;
  width: 100%;
  text-align: left;
}
table.sample th {
  border: 1px solid #000000;
  padding: 5px;
  background-color: #aaaaaa;
  text-align: center;
}
table.sample td {
  border: 1px solid #000000;
  padding: 5px;
}
table.sample td.action {
  text-align: center;
}
</style>
<table class="sample">
	<tr>
		<th>Название</th>
		<th>Название кратко</th>
		<th>Уд.</th>
	</tr>
<?php
		foreach ($rows as $row)
			$this->renderPartial('_row', array('obj' =>$row));
?>
<?php endif; ?>
</table>
<?php endif; ?>
<br />
