<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/form.css'); ?>
<style type="text/css">
table.sample {
  border-spacing: 2px;
  border-collapse: collapse;
  width: 100%;
  text-align: left;
}
table.sample th {
  border: 1px solid #000000;
  padding: 5px;
  background-color: #aaaaaa;
  text-align: center;
}
table.sample td {
  border: 1px solid #000000;
  padding: 5px;
}
table.sample td.action {
  text-align: center;
}
</style>
<p><?php echo CHtml::link('Добавить новость', array('newsadmin/add')); ?></p>
Всего новостей: <?php echo $total; ?><br /><br />
<?php if (!empty($rows)):?>
<table class="sample">
	<tr>
		<th>Дата новости</th>
		<th>Заголовок</th>
		<th>Уд.</th>
	</tr>
<?php
		foreach ($rows as $row)
			$this->renderPartial('_row', array('obj' =>$row));
?>
</table>
<div align="center">
<?php
  $this->widget('CLinkPager',array(
		'pages'          =>$pages,
		'maxButtonCount' =>5, # максимальное колличество вкладок на странице
		'header'         =>'<b>Перейти к странице:</b><br>', # заголовок
		'nextPageLabel'  =>'Вперед&gt;', # название кнопок навигаций next и prev
		'prevPageLabel'  =>'&lt;Назад',
  ));
?>
</div>
<?php endif; ?>
