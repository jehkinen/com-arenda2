<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/form.css'); ?>
<script type="text/javascript" src="<?php echo $this->createAbsoluteUrl('/js/tiny_mce/tiny_mce.js'); ?>"></script>
<script type="text/javascript">
    tinyMCE.init({
        mode:"textareas",
        theme:"advanced",
        plugins : "spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,
        content_css : "http://localhost/site/css/content.css",
        style_formats : [
			{title : 'Red header', block : 'h1',  classes : 'example1'},
		],
        language:"ru"
    });
</script>
Добавить Новость [<?php echo CHtml::link('отмена', array('newsadmin/index')); ?>]
<div class="form">
  <?php echo CHtml::form('', 'post', array('enctype'=>'multipart/form-data')); ?>
  <?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:'); ?>
<div class="row">
  <?php echo CHtml::activeLabel($form, 'header'); ?><br />
  <?php echo CHtml::activeTextField($form, 'header', array('size'=>70, 'maxlength'=>512)); ?>
</div>
<div class="row">
  <?php echo CHtml::activeLabel($form, 'description'); ?><br />
  <?php echo CHtml::activeTextField($form, 'description', array('size'=>70, 'maxlength'=>240)); ?>
</div>
<div class="row">
  <?php echo CHtml::activeLabel($form, 'keywords'); ?><br />
  <?php echo CHtml::activeTextField($form, 'keywords', array('size'=>70, 'maxlength'=>240)); ?>
</div>
<div class="row">
  <?php echo CHtml::activeCheckBox($form, 'stick').' '.CHtml::activeLabel($form, 'stick'); ?>
</div>
<div class="row">
    <?php echo CHtml::activeLabel($form, 'image'); ?><br />
    <?php echo CHtml::activeFileField($form, 'image'); ?>
</div>
<div class="row">
  <?php echo CHtml::activeLabel($form, 'anounce'); ?><br />
  <?php echo CHtml::activeTextField($form, 'anounce', array('size'=>100, 'maxlength'=>201)); ?>
</div>
<div class="row">
  <?php echo CHtml::activeLabel($form, 'body'); ?><br />
  <?php echo CHtml::textArea('News[body]', $txt, array('rows'=>20, 'cols'=>70)); ?>
</div>
<div class="row buttons">
  <?php echo CHtml::submitButton('Сохранить', array('id'=>"submit")); ?>
  <?php echo CHtml::resetButton('Сброс', array('id'=>'reset')); ?>
</div>
<?php echo CHtml::endForm(); ?>
</div><!--form-->
