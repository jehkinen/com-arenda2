<?php $s = ' помещение';
switch ($info->kind) {
	case 1: {$s = 'торговое'.$s; break;}
	case 2: {$s = 'складское\производственное'.$s; break;}
	default: $s = 'офис';
}
/* // ссылка в личный кабинет
$linkName = 'Личный кабинет';
//di(Yii::app()->user->id['user_role']);
switch (Yii::app()->user->id['user_role']) {
	case 'owner': {$link='kabinet/owner.html'; break;}
	case 'tenant': {$link='kabinet/tenant.html'; break;}
	default: {$link = 'admin'; $linkName = 'Админка';}
} */
?>
<div class="c999 m_0005t t10"><?php $get = $this->widget('DateFormat', array('kind'=>5,'dbDate'=>$info->created)); echo $get->date;?></div>
<div class="t21">Заявка (Хочу снять <?php echo $s;?>)</div>
<div class="m_0010t t14">
	Подал(-а): <?php echo CHtml::link($info->whoSoName.' '.$info->whoName.' '.$info->whoOtchestvo, array('person/card', 'id'=>$info->who_id), array('class'=>'c2E3192 link01'));?><br/>
<?php	$s = '';
	if (isset($info->squareMin)) {
		if (isset($info->squareMax))
				$s = $info->squareMin.'-'.$info->squareMax;
		else	$s = 'от '.$info->squareMin;
	} elseif (isset($info->squareMax))
			$s = 'до '.$info->squareMax;
	if ($s=='') $s = '<i>не указана</i>';
	else $s.= ' м<sup>2</sup>';?>
	Площадью: <i class="c666"><?php echo $s;?></i><br/>
<?php $s = '';
	if (isset($info->priceMin)) {
		if (isset($info->priceMax))
				$s = $info->priceMin.'-'.$info->priceMax;
		else	$s = 'от '.$info->priceMin;
	} elseif (isset($info->priceMax))
			$s = 'до '.$info->priceMax;
	if ($s=='') $s = '<i>не указана</i>';
	else $s.= ' руб.';?>
	По цене: <i class="c666"><?php echo $s;?></i><br/>
</div>
<div class="stripe01 m_0020t">
	<span class="stripe01_lbl">Детальная информация</span><div class="stripe01_bdr m_0165l"></div>
</div>
<table class="items_2 m_0010t"><tbody>
	<tr><td class="w50p">Город</td><td class="w50p"><?php echo $info->city;?></td></tr>
	<tr class="even"><td class="w50p">Район</td><td class="w50p"><?php echo $info->district;?></td></tr>
	<tr><td class="w50p">Улица</td><td class="w50p"><?php echo $info->street;?></td></tr>
<?php $s = 'не важен';
	if (isset($info->storeyMin)) {
		if (isset($info->storeyMax))
				$s = $info->storeyMin.' - '.$info->storeyMax;
		else	$s = 'от '.$info->storeyMin;
	} elseif (isset($info->storeyMax))
			$s = 'до '.$info->storeyMax;
?>
	<tr class="even"><td class="w50p">Этаж</td><td class="w50p"><?php echo $s; ?></td></tr></tbody>
</table>
<table class="items_2 m_0030t"><tbody>
	<tr><td class="w50p">Тип сооружения</td><td class="w50p"><?php echo $info->kind_structure;?></td></tr>
	<tr class="even"><td class="w50p">Функциональность</td><td class="w50p"><?php echo $info->functionality;?></td></tr>
	<tr><td class="w50p">Класс объекта</td><td class="w50p"><?php echo $info->class;?></td></tr></tbody>
</table>
<table class="items_2 m_0030t"><tbody>
	<tr><td class="w50p">Состояние ремонта</td><td class="w50p"><?php echo $info->repair;?></td></tr>
	<tr class="even"><td class="w50p">Парковка</td><td class="w50p"><?php echo $info->parking;?></td></tr>
	<tr><td class="w50p">Наличие Интернет</td><td class="w50p"><?php echo $info->internet;?></td></tr>
	<tr class="even"><td class="w50p">Телефония</td><td class="w50p"><?php echo $info->telephony;?></td></tr>
	<tr><td class="w50p">Система отопления и водоснабжения</td><td class="w50p"><?php echo $info->heating;?></td></tr>
	<tr class="even"><td class="w50p">Пожарная сигнализация</td><td class="w50p"><?php echo $info->firefighting;?></td></tr>
	<tr><td class="w50p">Грузовой лифт</td><td class="w50p"><?php echo $info->service_lift;?></td></tr></tbody>
</table>
<table class="items_2 m_0030t"><tbody>
	<tr><td class="w50p">Кран-балка</td><td class="w50p"><?php echo $info->cathead;?></td></tr>
	<tr class="even"><td class="w50p">Система вентиляции</td><td class="w50p"><?php echo $info->ventilation;?></td></tr>
	<tr><td class="w50p">Пандус</td><td class="w50p"><?php echo $info->rampant;?></td></tr>
	<tr class="even"><td class="w50p">Автомобильные подъездные пути</td><td class="w50p"><?php echo $info->autoways;?></td></tr>
	<tr><td class="w50p">Железнодорожные подъездные пути</td><td class="w50p"><?php echo $info->railways;?></td></tr>
	<tr class="even"><td class="w50p">Высота ворот</td><td class="w50p">от: <?php if (isset($info->gates_height1)) echo $info->gates_height1; else echo '?';?> до: <?php if (isset($info->gates_height2)) echo $info->gates_height2; else echo '?';?></td></tr>
	<tr><td class="w50p">Высота потолков</td><td class="w50p">от <?php if (isset($info->ceiling_height1)) echo $info->ceiling_height1; else echo '?';?> до <?php if (isset($info->ceiling_height2)) echo $info->ceiling_height2; else echo '?';?></td></tr></tbody>
</table>
