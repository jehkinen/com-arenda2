<?php
	$bUrl = Yii::app()->baseUrl;
	echo CHtml::cssFile(Yii::app()->baseUrl.'/css/modal.css'); 
	if ($userTenant) echo CHtml::scriptFile($bUrl.'/js/jquery.simplemodal.js');
	switch ($kindTab) {
		case 1 : {$type_ses = 'tab3'; break;}
		case 2 : {$type_ses = 'tab2'; break;}
		default: $type_ses = 'tab1'; /* В эту переменную попадает переменная из сессии, т.е. запоминание активной вкладки */ 
	}
?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#add-elements').hide();
		var isAddElemsShow = false;
	    var type_ses = "<?php echo $type_ses; ?>";
	    $('li.load').removeClass('activ');
	    $('[id='+type_ses+']').addClass('activ');

	    $.ajax({
				url: "/ajax_findform/load",
				data: {type: type_ses},
				dataType: 'json',
				type: 'post',
				success: function(data){
										$('#elements').html(data[0]);
										$('#add-elements').html(data[1]);
										}
		});
<?php 	    // $('form').removeClass('jqtransformdone'); 
		// $('form').jqTransform({imgPath:'images/'}); ?>

		$('a.load').bind('click', function(e){
			$('#add-elements').hide();
			if (isAddElemsShow) {
				isAddElemsShow = false;
				$('#additional').text("Дополнительные параметры");
				$('#add-arr').attr('src','<?php echo $bUrl;?>/images/bottom_str.gif');
			}
			var load_type = $(e.target.parentNode.parentNode).attr('id');
			$.ajax({
					url: "/ajax_findform/load",
					data: {type: load_type},
					dataType: 'json',
					type: 'post',
					success: function(data){
										$('#elements').html(data[0]);
										$('#add-elements').html(data[1]);
											$('li.load').removeClass('activ');
											$(e.target.parentNode.parentNode).addClass('activ');
											}
			});
<?php			// $('form').jqTransform({imgPath:'images/'}); ?>
			return false;
		});

		$('#additional').click(function(){
			var Add_obj = $(this);
			var Img_obj = $('#add-arr');
			if (isAddElemsShow) {
				isAddElemsShow = false;
				Add_obj.text("Дополнительные параметры");
				Img_obj.attr('src','<?php echo $bUrl;?>/images/bottom_str.gif');
			} else {
				isAddElemsShow = true;
				Add_obj.text("Скрыть параметры");
				Img_obj.attr('src','<?php echo $bUrl;?>/images/top_str.gif');
			}
            $('#add-elements').slideToggle(100);
            return false;
        });

<?php if ($userTenant):?>
		$('a.tonotepad').bind('click', function(e){
			var Parent_obj = $(this).parents('td');
			var id_obj = $(this).attr('id');
			e.preventDefault();
			show_modal('#owner-delete', 'ods', '<span>Подтверждение</span>', 'Поместить объект в блокнот?', '30%', function(){
				$.ajax({
					url: "/ajax/notepad_add",
					data: {id: id_obj},
					type: 'post',
					success: function(){
						Parent_obj.html('<img src="<?php echo $bUrl;?>/images/se_yes.gif" alt="" />');
					}
				});
			});
		});
		$('a#orderAdd').bind('click', function(e){
			e.preventDefault();
			$.ajax({
				url: "/ajax/order_add",
				data: {},
				dataType: 'html',
				type: 'post',
				success: function(data){
					$('div#info-window-message').html(data);
				}
			});
			show_modal_info('#info-window', 'ims', '<span>Информация</span>', '30%');
		});
<?php endif;?>
	});
<?php if ($userTenant):?>
function show_modal(box, container, header, message, top, callback){
	$(box).modal({
		closeHTML: '<a href="#" title="Закрыть" class="x-close"><img src="/images/modal/mw-close.jpg" /></a>',
		position: [top,],
		opacity: 30,
		overlayId: 'overlay',
		containerId: container,
		closeClass: 'close',
		onShow: function (dialog){
			var modal = this;

			$('.header', dialog.data[0]).append(header);
			$('.message', dialog.data[0]).append(message);

			$('.yes', dialog.data[0]).click(function () {
				if ($.isFunction(callback)) {
					callback.apply();
				}
				modal.close();
			});
		}
	});
}
function show_modal_info(box, container, header, top){
	$(box).modal({
		closeHTML: '<a href="#" title="Закрыть" class="x-close"><img src="/images/modal/mw-close.jpg" /></a>',
		position: [top,],
		opacity: 30,
		overlayId: 'overlay',
		containerId: container,
		closeClass: 'close',
		onShow: function (dialog){
			$('.header', dialog.data[0]).append(header);
		}
	});
}
<?php endif;?>
</script>
<?php if ($userTenant):?>
<div id="owner-delete">
	<div class="header"></div>
	<div class="message"></div>
	<div class="buttons">
		<div class="yes">Да</div><div class="no close">Нет</div>
	</div>
</div>
<div id="info-window">
	<div class="header"></div>
	<div class="message" id="info-window-message"></div>
	<div class="buttons">
		<div class="yes close">Понятно</div>
	</div>
</div>
<?php endif;?>

<div class="orange_block">
<div class="orange_block_02">
<form action="/search" method="POST">
	<div class="orange_menu">
		<ul>
			<li id="tab1" class="activ load"><div><a class="load" href="#">Офис</a></div></li>
			<li id="tab2" class="load"><div><a class="load" href="#">Склад/производство</a></div></li>
			<li id="tab3" class="load"><div><a class="load" href="#">Торговое помещение</a></div></li>
		</ul><div class="clear"></div>
	</div>
	<div id="elements"></div>
	<div id="add-elements"></div>
	<div class="submit_orange_block">
		<div class="hidden_parametrs"><img id="add-arr" src="<?php echo $bUrl;?>/images/bottom_str.gif" alt="" /> <a id="additional" href="#">Дополнительные параметры</a></div>
		<div class="right_submit">
			<input type="submit" class="gray_submit" name="btReset" value="Новый поиск" />
			<input type="submit" class="green_submit" name="btFind" value="Начать поиск" />
		</div><div class="clear"></div>
	</div>
</form>
<?php if ($userTenant)	echo CHtml::link('Сохранить параметры поиска как заявку', array('orders/add'), array('id'=>'orderAdd')).'<br />'; ?>
</div><!-- / orange_block -->
</div><!-- / orange_block_02 -->



<?php if (!empty($Search_obj)){
$sortImg = '<img class="sort" src="'.$bUrl.'/images/rajon_str.gif" alt="" />';
?>
<script type="text/javascript">
function doClick(idPage) {
	location.href = '/objects/detail/id/'+idPage; return false;
}
function doSort(colNo) {
	location.href = '/search/table/sort/'+colNo; return false;
}
</script>
<div class="rezult_search_title"><h2>Результаты поиска</h2></div>
<div class="result_search_table">
<table cellspacing="0" cellpadding="0">
<tr class="title_tr">
	<td class="td_01">Фото</td>
	<td class="td_02" onClick="doSort(2)"><?php if ($sortCol==2) echo $sortImg;?>Район</td>
	<td class="td_03" onClick="doSort(3)"><?php if ($sortCol==3) echo $sortImg;?>Адрес</td>
	<td class="td_04" onClick="doSort(4)"><?php if ($sortCol==4) echo $sortImg;?>Пл.</td>
	<td class="td_05" onClick="doSort(5)"><?php if ($sortCol==5) echo $sortImg;?>Цена (м<sup>2</sup>)</td>
	<td class="td_06" onClick="doSort(6)"><?php if ($sortCol==6) echo $sortImg;?>Сумма</td>
	<td class="td_07" onClick="doSort(7)"><?php if ($sortCol==7) echo $sortImg;?>Дата</td>
<?php if ($userTenant):?>
	<td class="td_08"><img src="<?php echo $bUrl;?>/images/search_r_ico.gif" alt="" /></td>
<?php endif;?>
</tr>

<?php $i=1; foreach ($Search_obj as $object){
		$b = ++$i % 2;
		$this->renderPartial('_resoult', array(
			'obj' =>$object,
			'odd' =>$b,
			'isUserTenant' =>$userTenant
		));
	} ?>
</table>
<?php }else{ ?>
<div class="rezult_search_title"><h2>Поиск не дал результатов</h2></div>
<?php } ?>
<?php
  $this->widget('CLinkComarendaPager',array(
		'pages'          =>$pages,
		'maxButtonCount' =>5, # максимальное колличество вкладок на странице
		'header'         =>'Стр.:', # заголовок
		'nextPageLabel'  =>'&rarr;',
		'prevPageLabel'  =>'&larr;',
		'firstPageLabel' =>'',
		'lastPageLabel' =>''
  ));
?>
<br /><br />
Найденно объектов: <?php echo $count_of_search; ?><br /><br />
