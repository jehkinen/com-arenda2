<?php /*
if ($odd)	$row_class = 'background_tr';
else		$row_class = '';
if ($row_class != '') $row_class = 'class="'.$row_class.'"'; */
$bUrl = Yii::app()->baseUrl;
?>
<div class="post<?php if($odd) echo ' even';?>">
	<div class="image">
	&nbsp;<?php if(empty($obj->hasFoto)) $s=$bUrl.'/images/noFoto-sm.png'; else $s=$bUrl.'/storage/images/_thumbs/'.$obj->hasFoto; echo CHtml::image($s);?>
<?php if ($obj->special==1):?>
		<img src="<?php echo $bUrl;?>images/hot.png" class="hot tl" />
<?php endif;?>
	</div>
	<div class="adress">
		Ул. <?php echo $obj->street;?>, <?php echo $obj->house;?>, <br />Сдает: собственик <?php echo CHtml::link($obj->firstname.' '.$obj->lastname.'.', array('person/card', 'id'=>$obj->owner_id), array('class'=>'have'));?> <?php $get = $this->widget('DateFormat', array('kind'=>1,'dbDate'=>$obj->created)); echo $get->isEmpty?'':$get->date; ?>
	</div>
	&nbsp;<div class="object_main_info">
		<a href="/objects/detail/id/<?php echo($obj->id);?>"><?php echo number_format($obj->total_item_sqr*$obj->price, 0, '.', ' ');?> руб/мц.<br />
		<?php echo number_format($obj->total_item_sqr, 2, '.', ' ');?> м2,<br />
		<?php echo number_format($obj->price, 0, '.', ' ');?> руб./м2</a>
	</div>
	<div class="object_action">
		<a href="#row<?php echo($obj->id);?>" class="popup_link" class="action_select">Действие</a>
<?php if ($isUserTenant):?>
		<span class="success<?php echo $obj->inNotepad==0?' notAtNote':'';?>" id="span<?php echo($obj->id);?>">в блокноте</span>
<?php endif;?>
	</div>
    <div class="pop_up action-window" id="row<?php echo($obj->id);?>">

<?php if ($isUserTenant && $obj->inNotepad==0):?>
		<a href="#" class="tonotepad" id="obj<?php echo($obj->id);?>">Копировать в блокнот</a><br />
<?php endif;?>
		<?php echo CHtml::link('Скачать одним файлом', array('objects/load', 'id'=>$obj->id));?><br />
		<?php echo CHtml::link('Загрузить как PDF-файл', array('objects/download', 'id'=>$obj->id));?><br />
		<?php echo CHtml::link('Распечатать', array('objects/print', 'id'=>$obj->id));?><br />
<?php if ($isUserTenant):?>
		<a href="#" class="claim" id="ojj<?php echo($obj->id);?>">Пожаловаться</a><br />
<?php endif;?>

	</div>
</div>
