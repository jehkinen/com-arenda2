<?php if (is_array($rows)):?>
Всего жалоб: <?php echo $foundQty; ?><br /><br />
<?php if (!empty($rows)):?>
<style type="text/css">
table.sample {
  border-spacing: 2px;
  border-collapse: collapse;
  width: 100%;
  text-align: left;
}
table.sample th {
  border: 1px solid #000000;
  padding: 5px;
  background-color: #aaaaaa;
  text-align: center;
}
table.sample td {
  border: 1px solid #000000;
  padding: 5px;
}
table.sample td.action {
  text-align: center;
}
</style>
<?php echo CHtml::link('Сбросить сортировку', array('adminclaims/view', 'page'=>$Cpage)); ?>
<table class="sample">
	<tr>
		<th>Подана <?php echo CHtml::link('&uarr;', array('adminclaims/view', 'page'=>$Cpage, 'sort'=>'sended')); ?>&nbsp;<?php echo CHtml::link('&darr;', array('adminclaims/view', 'page'=>$Cpage, 'sort'=>'sended', 'type'=>'desc')); ?>
		</th>
		<th>Кто подал <?php echo CHtml::link('&uarr;', array('adminclaims/view', 'page'=>$Cpage, 'sort'=>'tenLogin')); ?>&nbsp;<?php echo CHtml::link('&darr;', array('adminclaims/view', 'page'=>$Cpage, 'sort'=>'tenLogin', 'type'=>'desc')); ?>
		</th>
		<th>Объект</th>
		<th>Владелец <?php echo CHtml::link('&uarr;', array('adminclaims/view', 'page'=>$Cpage, 'sort'=>'ownLogin')); ?>&nbsp;<?php echo CHtml::link('&darr;', array('adminclaims/view', 'page'=>$Cpage, 'sort'=>'ownLogin', 'type'=>'desc')); ?>
		</th>
		<th>Содержание</th>
	</tr>
<?php
		foreach ($rows as $row)
			$this->renderPartial('_row', array('obj' =>$row));
?>
<?php endif; ?>
</table>
<div align="center">
<?php
  $this->widget('CLinkPager',array(
		'pages'          =>$pages,
		'maxButtonCount' =>5, # максимальное колличество вкладок на странице
		'header'         =>'<b>Перейти к странице:</b><br>', # заголовок
		'nextPageLabel'  =>'Вперед&gt;', # название кнопок навигаций next и prev
		'prevPageLabel'  =>'&lt;Назад',
  ));
?>
</div>
<?php endif; ?>
