<?php @require_once('protected/views/layouts/cookies.php'); $bUrl = Yii::app()->baseUrl; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
	<meta name="verify-reformal" content="02c32194f5d8a18f6d4aa773" />
	<title></title>
	<meta name="description" content="<?php echo $this->pageDescription; ?>" />
	<meta name="keywords" content="<?php echo $this->pageKeywords; ?>" />
	<base href="<?php echo Yii::app()->params->site_base; ?>">
	<?php echo CHtml::cssFile($bUrl.'/css/style-01.css')."\n";?>	
	<!--[if IE]><?php echo CHtml::cssFile($bUrl.'/css/ie.css');?><![endif]-->	
	<?php foreach($this->cssFiles as $f) echo CHtml::cssFile($bUrl.'/css/'.$f.'.css')."\n";?>
	<?php echo CHtml::scriptFile($bUrl.'/js/jquery-1.6.min.js');?>
	<?php foreach($this->jsFiles as $f) echo CHtml::scriptFile($bUrl.'/js/'.$f.'.js')."\n";?>

</head>
<body>
<div class="all">
<?php @require_once('protected/views/layouts/header01.php');?>
<div class="content">
<?php @require_once('protected/views/layouts/leftColumn01.php');?>
<!-- R I G H T  B L O C K -->
<div class="right_block">
<?php echo $content; ?>
</div>
</div>
<!-- E N D  R I G H T  B L O C K -->
<div class="clear"></div>
</div>
</div>
<?php @require_once('protected/views/layouts/footer01.php');?>
</body>
</html>