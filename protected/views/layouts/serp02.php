<?php @require_once('protected/views/layouts/cookies.php'); $bUrl = Yii::app()->baseUrl; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="verify-reformal" content="02c32194f5d8a18f6d4aa773" />
	<title><?php echo $this->pageTitle; ?></title>
	<meta name="description" content="<?php echo $this->pageDescription; ?>" />
	<meta name="keywords" content="<?php echo $this->pageKeywords; ?>" />
	<base href="<?php echo Yii::app()->params->site_base; ?>" />
<?php echo
	CHtml::cssFile($bUrl.'/css/reset.css').
	CHtml::cssFile($bUrl.'/css/style.css').
	CHtml::cssFile($bUrl.'/css/jquery.ui.css').
	CHtml::cssFile($bUrl.'/css/jquery-ui.css').

	CHtml::scriptFile($bUrl.'/js/jquery-1.8.3.js').
	CHtml::scriptFile($bUrl.'/js/jquery-ui.js').
	CHtml::scriptFile($bUrl.'/js/jquery.lightbox_me.js').
	//CHtml::scriptFile($bUrl.'/js/jquery.ui.min.js').
	CHtml::scriptFile($bUrl.'/js/client.js');
?>
</head>

<body>
<?php $this->widget('GetReclameBanner', array('onPage'=>'serp', 'width'=>1000, 'height'=>90)); ?>
<div class="top_bg"></div><div class="wrap_for_max_width">
	<div class="top_wrap">
<?php @require_once('protected/views/layouts/header02.php');?>
	</div><!-- /top_wrap -->
<?php @require_once('protected/views/layouts/filter02.php');?>
	<div class="content_wrap">
<?php @require_once('protected/views/layouts/rightColumn02.php');?>
		<div class="content">
<?php echo $content; ?>
		</div><!-- /content -->
	</div><!-- /content_wrap -->
</div><!-- /wrap_for_max_width -->
<?php @require_once('protected/views/layouts/footer02.php');?>

</body>
</html>