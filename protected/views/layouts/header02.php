<!--Стартовая страница леддинга-->
<?
if(isset($_COOKIE['show_when_start'])){
	if(  $_COOKIE['show_when_start'] != false)
		$this->widget('LandingStartPage');
} else{
	setcookie("show_when_start", 1, time()+3600*24*70); //установим на 70 дней куки
	$this->widget('LandingStartPage');
}
?>

<?php $bUrl = Yii::app()->baseUrl; $cityName = Yii::app()->user->getState('glcity', 'Екатеринбург');
if (Yii::app()->user->isGuest): ?>
			<div class="rounded dotted authorization">
			Личный кабинет <a href="#login_form" class="popup_link">Вход</a> <?php echo CHtml::link('Регистрация', array('registration/view'), array('title'=>'Зарегистрироваться на сайте'));?>
			<div class="popup" id="login_form">
				<a class="popup_close"></a>
				<form method="post" action="<?php echo $this->createUrl('authorization/firstlogin');?>">
					<div><label>Логин <input id="username" name="username" value="" title="Вы можете указать свой e-mail" tabindex="4" type="text" /></label></div>
					<div><label>Пароль <input id="password" name="password" value="" title="Пароль" tabindex="5" type="password" /></label></div>
					<input type="submit" value="Войти" class="green_rounded_button" tabindex="6" />
					<label><input type="checkbox" id="remember" name="remember_me" value="1" tabindex="7" />Запомнить меня</label>
				</form>
				<?php echo CHtml::link('Забыли? Восстановить пароль', array('registration/restore'), array('title'=>'Восстановление утраченного пароля'));?>
			</div>
			</div>
<?php else: ?>
	<div class="rounded dotted user_inf">
		<?php $this->widget('Status'); ?>
	</div>
<?php endif; ?>
		<div class="most_search">
<?php $this->widget('OftenSeek');?>
		</div>
	 	<a href="" class="logo"><img src="images/logo.png" /></a>


		<div class="city_select_wrap">
			<a class="city_select" id="city_select"  title="Выбрать другой город"><?php echo($cityName);?></a>

			<input id="hidden_city_input" type="hidden" />
			<?php $this->widget('AreasForming');?>
            <script>
                $(function(){
	               // $('.b-city-add').hide();
                    $('#city_select').click(function(e){
	                    e.preventDefault();
                        $('#Popup2Fog').show();
                        $('#Popup2').show();
                        $('#Popup2').css('visibility', 'visible');
	                    return false;
                    });

	                $('#hidden_city_input').bind('change', function(){
                        window.location.href = 'index/view/city/'+$(this).val();
	                });

                    /*При клике на город  переадресовываем*/
                    $('.city_list li').click(function(){
                        var city = $(this).text();
                        $(this).parent().find('li').removeClass('active');
                        $(this).addClass('active');
                        window.location.href = 'index/view/city/'+city;
                    });
                });
            </script>
			<div class="popup" id="city_select">
				<a class="popup_close"></a>
				<ul class="city_select_list">
					<?php $this->widget('GetCities', array('sel_name'=>$cityName));?>
				</ul>
			</div>
		</div>
	<div class="inf_block"><?php /* всего в аренду<br /><?php $this->widget('ItemsAtCity'); */?>
		</div>
		<div class="inf_block">
			Телефон в Екатеринбурге<br />
			(343) 344-60-27
		</div>
		<div class="menu_wrap">
			<ul class="main_menu rounded">
<?php $this->widget('MainMenu');?>
			</ul>
			<a href="/how_work.html" class="green_link_block">Как работает com-arenda?</a>
			<?php echo CHtml::link('Разместить объект без регистрации', array('sendorder/view'), array('title'=>'Подать заявку на размещение объекта', 'class'=>'green_link_block'));?>
		</div>
