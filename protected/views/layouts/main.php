<?php
if (!isset(Yii::app()->user->glcity)) {
	$cookie=Yii::app()->request->cookies['usercity'];
	if (isset($cookie))
		Yii::app()->user->setState('glcity', $cookie->value);
	else {
		Yii::app()->user->setState('glcity', 'Екатеринбург');
		$cookie=new CHttpCookie('usercity', 'Екатеринбург');
		$cookie->httpOnly=true;
		$cookie->domain=Yii::app()->params->site_base;
		Yii::app()->request->cookies['usercity']=$cookie;
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
  "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
		<title><?php echo $this->pageTitle; ?></title>
		<meta name="description" content="<?php echo $this->pageDescription; ?>" />
		<meta name="keywords" content="<?php echo $this->pageKeywords; ?>" />
		<base href="<?php echo Yii::app()->params->site_base; ?>">
		<script type="text/javascript" charset="utf-8" src="js/jquery-1.6.min.js"></script>
	</head>
	<body>
		<table width="100%" border="1" cellpadding="10">
			<tr>
				<td height="100" colspan="3" bgcolor="#cccccc">
					<?php if (Yii::app()->user->isGuest): ?>
					<form action="<?php echo $this->createUrl('authorization/firstlogin'); ?>" method="post">
						<input type="text" name="username" value="" size="10" maxlength="10" />
						<input type="password" name="password" value="" size="10" maxlength="10" />
						<input type="submit" value="Вход" /> <?php echo CHtml::link('Забыли пароль?', array('registration/restore'), array('title'=>'Восстановление доступа')); ?>
					</form>
					<?php echo CHtml::link('регистрация', array('registration/view'), array('title'=>'Регистрация на сайте')); ?>
					<?php endif; ?>
				</td>
			</tr>
			<tr>
				<td width="20%" valign="top">
					<?php echo CHtml::link('Разместить объект', array('object/add', 'step'=>'step1')); ?><br /><br />
					<ul>
						<?php $this->widget('MainMenu'); ?>
					</ul>
				</td>
				<td width="60%" height="500" valign="top"><?php echo $content; ?></td>
				<td width="20%" valign="top"><?php $this->widget('Status'); ?></td>
			</tr>
			<tr><td height="100" colspan="3" bgcolor="#cccccc"></td></tr>
		</table>
	</body>
</html>