<?php 
if (!isset(Yii::app()->user->glcity)) {
	$cityName =  'Екатеринбург';
	$cookCityName = 'usercity';
	$cookie=Yii::app()->request->cookies[$cookCityName];
	//setcookie ("TestCookie", $cityName, 0x6FFFFFFF);
	if (isset($cookie)) {
		$cityName = $cookie->value;
	} else {
		$nCook=new CHttpCookie($cookCityName, $cityName);
		$nCook->httpOnly=true;
		$nCook->expire = 1879048191; // 0x6FFFFFFF;
		Yii::app()->request->cookies[$cookCityName]=$nCook;
	}
	Yii::app()->user->setState('glcity', $cityName);
}
?>