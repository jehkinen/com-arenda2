<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
  "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
		<title>Админка</title>
		<base href="<?php echo Yii::app()->params->site_base; ?>">
		<script type="text/javascript" charset="utf-8" src="js/jquery-1.6.min.js"></script>
	</head>
	<body>
		<table width="100%" border="1">
			<tr>
				<td width="30%">
					<ul>
						<li><?php echo CHtml::link('Лента админа: Модерация, ...', array('control/index')); ?></li>
						<li><?php echo CHtml::link('Новости', array('newsadmin/index')); ?></li>
						<li><?php echo CHtml::link('Управление страницами', array('control/static')); ?></li>
						<li><?php echo CHtml::link('Порядок главного меню', array('control/menu')); ?></li>
						<li><?php echo CHtml::link('Справочники', array('directories/view_all')); ?></li>
						<li><?php echo CHtml::link('Управление пользователями', array('adminusers/view')); ?></li>
						<li><?php echo CHtml::link('Управление объектами', array('adminitems/view')); ?></li>
						<li><?php echo CHtml::link('Статистика поисковых запросов', array('adminsearchstatbrowse/view')); ?></li>
						<li><?php echo CHtml::link('Управление рекламой объектов', array('adminreclameitems/view')); ?></li>
						<li><?php echo CHtml::link('Управление баннерной рекламой', array('adminreclamebanners/view')); ?></li>
						<li><?php echo CHtml::link('Просмотр жалоб', array('adminclaims/view')); ?></li>
						<li><?php echo CHtml::link('На сайт', Yii::app()->homeUrl); ?></li>
					</ul>
				</td>
				<td width="70%" height="300"><?php echo $content; ?></td>
			</tr>
		</table>
	</body>
</html>