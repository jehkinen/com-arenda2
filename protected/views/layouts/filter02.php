<?php if (isset(Yii::app()->user->ses_kind)) { // Тип помещения (офис/склад и т.п.
		$kind = Yii::app()->user->getState('ses_kind');
	} else $kind = 0;
	switch ($kind) {
		case 1 : {$type_ses = 'tab3'; break;}
		case 2 : {$type_ses = 'tab2'; break;}
		default: $type_ses = 'tab1'; /* В эту переменную попадает переменная из сессии, т.е. запоминание активной вкладки */ 
	}
	$Range = $this->widget('GetPriceSqruareRange');
	if ($this->ses_vals['vsqr1']=='')	$sqr1 = $Range->minSquare; else $sqr1 = (int)$this->ses_vals['vsqr1'];
	if ($this->ses_vals['vsqr2']=='')	$sqr2 = $Range->maxSquare; else $sqr2 = (int)$this->ses_vals['vsqr2'];	
	if ($this->ses_vals['vprc1']=='')	$prc1 = $Range->minPrice; else $prc1 = (int)$this->ses_vals['vprc1'];
	if ($this->ses_vals['vprc2']=='')	$prc2 = $Range->maxPrice; else $prc2 = (int)$this->ses_vals['vprc2'];
?>
	<script type="text/javascript">
	$(document).ready(function(){
		$(".filter .class_of_bulding a").click(function(){
			$(".filter a.green_link_rounded").toggleClass("green_link_rounded");
			$(this).toggleClass("green_link_rounded");
			return false;
		});

		$(".filter a.more_options").click(function(){
			$(".filter form .addition_options").toggleClass("active");
			if ($(this).hasClass("active"))
				$(this).text("Дополнительные параметры");
			else
				$(this).text("Скрыть");
			$(this).toggleClass("active");
			return false;
		});

		// SLIDER (ползунки)
//Square fields
		$(".Square").slider({
			min: <?php echo $Range->minSquare;?>, max: <?php echo $Range->maxSquare;?>,
			values: [<?php echo $sqr1.','.$sqr2;?>],
			range: true,
			stop: function(event, ui) {
				$("input.minSquare").val($(".Square").slider("values",0));
				$("input.maxSquare").val($(".Square").slider("values",1));
			},
			slide: function(event, ui){
				$("input.minSquare").val($(".Square").slider("values",0));
				$("input.maxSquare").val($(".Square").slider("values",1));
			}
		});
		$("input.minSquare").change(function(){
			var value1=$("input.minSquare").val();
			var value2=$("input.maxSquare").val();
			if(parseInt(value1) > parseInt(value2)){
				value1 = value2;
				$("input.minSquare").val(value1);
			}
			$(".Square").slider("values",0,value1);	
		});	
		$("input.maxSquare").change(function(){
			var value1=$("input.minSquare").val();
			var value2=$("input.maxSquare").val();
			if (value2 > <?php echo $Range->maxSquare;?>) { value2 = <?php echo $Range->maxSquare;?>; $("input.maxSquare").val(<?php echo $Range->maxSquare;?>)}
			if(parseInt(value1) > parseInt(value2)){
				value2 = value1;
				$("input.maxSquare").val(value2);
			}
			$(".Square").slider("values",1,value2);
		});
//Price fields
		$(".Price").slider({
			min: <?php echo $Range->minPrice;?>,	max: <?php echo $Range->maxPrice;?>,
			values: [<?php echo $prc1.','.$prc2;?>],
			range: true,
			stop: function(event, ui) {
				$("input.minPrice").val($(".Price").slider("values",0));
				$("input.maxPrice").val($(".Price").slider("values",1));
			},
			slide: function(event, ui){
				$("input.minPrice").val($(".Price").slider("values",0));
				$("input.maxPrice").val($(".Price").slider("values",1));
			}
		});
		$("input.minPrice").change(function(){
			var value1=$("input.minPrice").val();
			var value2=$("input.maxPrice").val();
			if(parseInt(value1) > parseInt(value2)){
				value1 = value2;
				$("input.minPrice").val(value1);
			}
			$(".Price").slider("values",0,value1);	
		});	
		$("input.maxPrice").change(function(){
			var value1=$("input.minPrice").val();
			var value2=$("input.maxPrice").val();
			if (value2 > <?php echo $Range->maxPrice;?>) { value2 = <?php echo $Range->maxPrice;?>; $("input.maxPrice").val(<?php echo $Range->maxPrice;?>)}
			if(parseInt(value1) > parseInt(value2)){
				value2 = value1;
				$("input.maxPrice").val(value2);
			}
			$(".Price").slider("values",1,value2);
		});

		$('input.number').keypress(function(event){
			var key, keyChar;
			if(!event) var event = window.event;

			if (event.keyCode) key = event.keyCode;
			else if(event.which) key = event.which;

			if(key==null || key==0 || key==8 || key==13 || key==9 || key==46 || key==37 || key==39 ) return true;
			keyChar=String.fromCharCode(key);

			if(!/\d/.test(keyChar))	return false;	
		});
		// \ SLIDER (ползунки)


		var type_ses = "<?php echo $type_ses;?>";
		$('div.class_of_bulding a').removeClass('green_link_rounded');
		$('[id='+type_ses+']').addClass('green_link_rounded');

		$.ajax({
			url: "/ajax_findform/load",
			data: {type: type_ses},
			dataType: 'html',
			type: 'post',
			success: function(data){
				$('div.addition_options').html(data);
			}
		});

		$(".filter .class_of_bulding a").bind('click', function(e){
			var load_type = $(e.target).attr('id');
			$.ajax({
				url: "/ajax_findform/load",
				data: {type: load_type},
				dataType: 'html',
				type: 'post',
				success: function(data){
					$('div.addition_options').html(data);
				}
			});
		});
	});
	</script>
	<div class="filter">
		<div class="rounded">
			<form class="office_form active" action="/search" method="post">
				<div class="class_of_bulding">
					<a href="#" id="tab1" class="green_link_rounded">Офис</a>
					<a href="#" id="tab2">Склад/Производство</a>
					<a href="#" id="tab3">Торговое</a>
				</div>
				<div class="data_box_filter">
				<label>в районе:
					<div class="rounded2">
						<select name="raion">
<?php foreach ($this->list_districts as $key=>$val):?>
							<option value="<?php echo $key; if ($key==$this->ses_vals['vraion']) echo '" selected="selected';?>"><?php echo $val;?></option>
<?php endforeach;?>
						</select>
					</div>
				</label>
				</div>
				<div class="data_box_filter">площадью от 
                    <div class="range">
						<div class="rounded2"><input type="text" name="sqr1" value="<?php echo $this->ses_vals['vsqr1'];?>" class="minSquare number" /></div> до <div class="rounded2"><input type="text" name="sqr2" value="<?php echo $this->ses_vals['vsqr2'];?>" class="maxSquare number" /></div> м<sup>2</sup>
						<div class="slider_filter_wrap"><div class="Square"></div></div>
					</div>
				</div>
				<div class="data_box_filter">цена от 
                    <div class="range">
						<div class="rounded2"><input type="text" name="prc1" value="<?php echo $this->ses_vals['vprc1'];?>" class="minPrice number" /></div> до <div class="rounded2"><input type="text" name="prc2" value="<?php echo $this->ses_vals['vprc2'];?>" class="maxPrice number" /></div> руб./м<sup>2</sup>
						<div class="slider_filter_wrap"><div class="Price"></div></div>
					</div>
				</div>
				<input type="submit" class="filter_button" name="btFind" value="Поиск" />

				<div class="addition_options">
				</div>
			</form>
		</div>
		<a href="#" class="more_options">Дополнительные параметры</a>
	</div>
