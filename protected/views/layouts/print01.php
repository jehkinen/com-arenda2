<?php $bUrl = Yii::app()->baseUrl; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
	<title><?php echo $this->pageTitle; ?></title>
	<?php echo CHtml::cssFile($bUrl.'/css/print.css');?>
</head>
<body onLoad="window.print();">
<div class="all">
<?php @require_once('protected/views/layouts/print-header01.php');?>
<div class="content">
<?php echo $content; ?>
<div class="clear"></div>
</div>
</div>
<?php @require_once('protected/views/layouts/print-footer01.php');?>
</body>
</html>