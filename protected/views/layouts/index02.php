<?php  @require_once('protected/views/layouts/cookies.php'); $bUrl = Yii::app()->baseUrl; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="verify-reformal" content="02c32194f5d8a18f6d4aa773" />
	<title><?php echo $this->pageTitle; ?></title>
	<meta name="description" content="<?php echo $this->pageDescription; ?>" />
	<meta name="keywords" content="<?php echo $this->pageKeywords; ?>" />
	<base href="<?php echo Yii::app()->params->site_base; ?>" />
	<?php
	Yii::app()->clientScript->registerPackage('BaseCss');
	Yii::app()->clientScript->registerPackage('BaseJs');
	Yii::app()->clientScript->registerPackage('jquery-ui');
	Yii::app()->clientScript->registerPackage('jquery.cookie');
	Yii::app()->clientScript->registerPackage('CarouselJs');
	Yii::app()->clientScript->registerPackage('CarouselCss');
	?>
</head>

<body>
<?php $this->widget('GetReclameBanner', array('onPage'=>'main', 'width'=>1000, 'height'=>90)); ?>
<div class="top_bg"></div><div class="wrap_for_max_width">
	<div class="top_wrap">
<?php @require_once('protected/views/layouts/header02.php');?>
	</div>
<?php @require_once('protected/views/layouts/filter02.php');?>

	<div class="carousel_wrap">
		<?php $this->widget('SpecialObjectsGallery');?>
	</div>


	<div class="content_wrap index_page">
		<div class="aside">
			<?php $this->widget('GetReclameItem', array('onPage'=>'main'));?>
			<?php $this->widget('GetReclameBanner', array('onPage'=>'main', 'width'=>240, 'height'=>400));?>
		</div>

		<div class="content">
			<div class="project_partners_wrap">
				<div>
					<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash4/cabs/swflash.cab#version=4,0,0,0" width="240" height="400">
						<param name="movie" value="storage/images/banners/our_bc.swf">
						<param name="play" value="true">
						<param name="loop" value="true">
						<param name="quality" value="high">
						<embed src="storage/images/banners/our_bc.swf" play="true" loop="true" quality="high" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" width="240" height="400"></embed>
					</object>
					<?php $this->widget('GetReclameBanner', array('onPage'=>'main', 'width'=>240, 'height'=>200));?>
				</div>
			</div>
			<div class="motd">
				<h1 class="m10b t21">О проекте</h1>
				Ком-аренда это специализированный портал со специально разработанным функционалом призванным обеспечивать потребности арендаторов и собственников, а так же профессионалов рынка агентств недвижимости и частных брокеров.  Более подробную информацию вы можете получить в разделе «<a href="how_work.html">Как все работает</a>».
			</div>
			<!--<div class="clear_left"></div>-->
			<div class="project_news_wrap">
				<?php $this->widget('NewsOnMain', array('qty'=>3));?>
			</div>
		</div>
	</div>
</div>
<?php @require_once('protected/views/layouts/footer02.php');?>
</body>
</html>