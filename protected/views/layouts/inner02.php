<?php @require_once('protected/views/layouts/cookies.php'); $bUrl = Yii::app()->baseUrl; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="verify-reformal" content="02c32194f5d8a18f6d4aa773" />
	<title><?php echo $this->pageTitle; ?></title>
	<meta name="description" content="<?php echo $this->pageDescription; ?>" />
	<meta name="keywords" content="<?php echo $this->pageKeywords; ?>" />
	<base href="<?php echo Yii::app()->params->site_base; ?>" />
	<?
	Yii::app()->clientScript->registerPackage('Base');
	Yii::app()->clientScript->registerPackage('jquery-ui');
	Yii::app()->clientScript->registerPackage('registration');
	Yii::app()->clientScript->registerPackage('jquery.cookie');
	?>
</head>

<body<?php if (isset($this->toBodyTag)) echo $this->toBodyTag;?>>
<?php if (isset($this->pageClass)) $this->widget('GetReclameBanner', array('onPage'=>$this->pageClass, 'width'=>1000, 'height'=>90)); ?>
<div class="top_bg"></div><div class="wrap_for_max_width">
	<div class="top_wrap green_border_bottom">
<?php @require_once('protected/views/layouts/header02.php');?>
	</div><!-- /top_wrap -->
	<div class="content_wrap">
<?php @require_once('protected/views/layouts/rightColumn02.php');?>
		<div class="content">
<?php echo $content; ?>
		</div><!-- /content -->
	</div><!-- /content_wrap -->
</div><!-- /wrap_for_max_width -->
<?php @require_once('protected/views/layouts/footer02.php');?>

</body>
</html>