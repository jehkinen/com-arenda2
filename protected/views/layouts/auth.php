<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
  "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
		<title>Авторизация</title>
		<base href="<?php echo Yii::app()->params->site_base; ?>">
	</head>
	<body>
		<table width="100%" border="1">
			<tr><td height="100" colspan="3" bgcolor="#cccccc"></td></tr>
			<tr>
				<td width="20%">
					<ul>
						<?php $this->widget('MainMenu'); ?>
					</ul>
				</td>
				<td width="60%" height="300"><?php echo $content; ?></td>
				<td width="20%">&nbsp;</td>
			</tr>
			<tr><td height="100" colspan="3" bgcolor="#cccccc"></td></tr>
		</table>
	</body>
</html>