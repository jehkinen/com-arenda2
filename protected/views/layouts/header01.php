<?php $bUrl = Yii::app()->baseUrl; 
$cityName = Yii::app()->user->getState('glcity', 'Екатеринбург'); 
?>
<script type="text/javascript">
        $(document).ready(function() {
            $(".signin").click(function(e) {
				e.preventDefault();
                $("fieldset#signin_menu").toggle();
				$(".signin").toggleClass("menu-open");
            });
			$("fieldset#signin_menu").mouseup(function() {
				return false
			});
			$(document).mouseup(function(e) {
				if($(e.target).parent("a.signin").length==0) {
					$(".signin").removeClass("menu-open");
					$("fieldset#signin_menu").hide();
				}
			});

            $('a.gorod_a').click(function(){
				var List_obj = $('#city_list');
				var pos_x = $(this).offset().left;
				pos_x = pos_x - 20;
            	List_obj.css({'left':pos_x,'width':'200'});
				List_obj.show();
				return false;
			});
			$('#city_list div.close a').click(function(){
				$('#city_list').hide();
				return false;
			});
        });
</script>
<!-- H E A D E R -->
<div class="top_header">
	<div class="left">
		<ul>
			<li><a href="#">О проекте</a></li>
			<li><a href="#">Справка</a></li>
			<li class="no_back"><a href="#">Контакты</a></li>
		</ul>
	</div>
	<div class="right">
<?php if (Yii::app()->user->isGuest): ?>
	<div id="enter_box">
		<div id="enter_head">
			<img src="<?php echo $bUrl;?>/images/zamok_ico.gif" alt="" /><a href="#" class="signin"><span>Войти с паролем</span></a>
		</div>
		<fieldset id="signin_menu">
			<form method="post" id="signin" action="<?php echo $this->createUrl('authorization/firstlogin'); ?>">
				<p>
					<label for="username">Логин или Е-mail</label>
					<input id="username" name="username" value="" title="username" tabindex="4" type="text">
				</p>
				<p>
					<label for="password">Пароль</label>
					<input id="password" name="password" value="" title="password" tabindex="5" type="password">
				</p>
				<p class="remember">
					<input id="signin_submit" value="Войти" tabindex="6" type="submit">
					<input id="remember" name="remember_me" value="1" tabindex="7" type="checkbox">
					<label for="remember">Запомнить меня</label>
				</p>
			</form>
		</fieldset>
	</div> или <?php echo CHtml::link('зарегистрироваться', array('registration/view'), array('title'=>'Регистрация на сайте')); ?>
<?php else: ?>
	<div>
		<?php $this->widget('Status'); ?>
	</div>
<?php endif; ?>
	</div>
<?php if (!Yii::app()->user->isGuest): ?>
	<?php if (Yii::app()->user->id['user_role'] == 'owner'): ?>
	<div class="butn">
		<?php echo CHtml::link('Добавить объект', array('object/add_new'), array('title'=>'Начать добавление нового объекта')); ?>
	</div>
	<?php elseif (Yii::app()->user->id['user_role'] == 'admin'): ?>
	<div class="butn">
		<?php echo CHtml::link('Вход для админа', array('control/index'), array('title'=>'Вход в админку')); ?>
	</div>
<?php endif; ?>
<?php endif; ?>
<div class="clear"></div>
</div>
<!-- E N D  H E A D E R -->
<div class="left_header">
	<div class="logo">
		<h3>Бета-версия</h3>
		<!-- <h1>Онлайн справочник</h1> -->
		<h2>Аренда коммерческой недвижимости</h2>
		<a href="/"><img src="<?php echo $bUrl;?>/images/logo.png" alt="" /></a>
	</div>
</div>
<div id="city_list">
	<div class="close"><a href=""><img src="<?php echo $bUrl;?>/images/modal/mw-close.jpg" border="0" alt="" /></a></div>
	<table class="list">
		<tr><td class="tl"></td><td class="t"></td><td class="tr"></td></tr>
		<tr>
			<td class="l"></td>
			<td class="c">
<?php $this->widget('GetCities', array('sel_name'=>$cityName));?>
			</td>
			<td class="r"></td>
		</tr>
		<tr><td class="bl"></td><td class="b"></td><td class="br"></td></tr>
	</table>
</div>
<div class="right_header">
	<div class="gorod">Ваш город:<br /><a class="gorod_a" href="#" title="Выбрать другой город"><?php echo($cityName);?></a> <a href="#"><img src="<?php echo $bUrl;?>/images/g_str.gif" alt="" /></a></div>
	<div class="gorod_object">Всего в вашем городе:<span><?php $this->widget('ItemsAtCity');?></span></div>
	<div class="phone">Телефон в Екатеринбурге:<span>(343) 290-59-28</span></div><div class="clear"></div>
	<div class="header_menu">
		<ul>
			<?php $this->widget('MainMenu');?>
		</ul>
	</div>
</div>
<div class="clear"></div>
