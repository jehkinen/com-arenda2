<?php @require_once('protected/views/layouts/cookies.php'); $bUrl = Yii::app()->baseUrl;
	if (isset(Yii::app()->user->ses_kind)) { // Тип помещения (офис/склад и т.п.
		$kind = Yii::app()->user->getState('ses_kind');
	} else $kind = 0;
	switch ($kind) {
		case 1 : {$type_ses = 'tab3'; break;}
		case 2 : {$type_ses = 'tab2'; break;}
		default: $type_ses = 'tab1'; /* В эту переменную попадает переменная из сессии, т.е. запоминание активной вкладки */
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
	<meta name="verify-reformal" content="02c32194f5d8a18f6d4aa773" />
	<title><?php echo $this->pageTitle; ?></title>
	<meta name="description" content="<?php echo $this->pageDescription; ?>" />
	<meta name="keywords" content="<?php echo $this->pageKeywords; ?>" />
	<base href="<?php echo Yii::app()->params->site_base; ?>">
	<?php echo CHtml::cssFile($bUrl.'/css/style.css')."\n";?>
	<!--[if IE]><?php echo CHtml::cssFile($bUrl.'/css/ie.css');?><![endif]-->
	<?php echo CHtml::cssFile($bUrl.'/css/skin.css')."\n";?>
	<?php echo CHtml::scriptFile($bUrl.'/js/jquery-1.6.min.js');?>
	<?php echo CHtml::scriptFile($bUrl.'/js/jquery2.js');?>

	<script type="text/javascript">
		jQuery(document).ready(function() {
		jQuery('#mycarousel').jcarousel ({
			auto:4,
			wrap:"circular",
			visible: 5,
			animation: 800
		});
		});
	</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#add-elements').hide();
		var isAddElemsShow = false;
	    var type_ses = "<?php echo $type_ses; ?>";
	    $('li.load').removeClass('activ');
	    $('[id='+type_ses+']').addClass('activ');

	    $.ajax({
				url: "/ajax_findform/load",
				data: {type: type_ses},
				dataType: 'json',
				type: 'post',
				success: function(data){
										$('#elements').html(data[0]);
										$('#add-elements').html(data[1]);
										}
		});
<?php 	    // $('form').removeClass('jqtransformdone');
		// $('form').jqTransform({imgPath:'images/'}); ?>

		$('a.load').bind('click', function(e){
			$('#add-elements').hide();
			if (isAddElemsShow) {
				isAddElemsShow = false;
				$('#additional').text("Дополнительные параметры");
				$('#add-arr').attr('src','<?php echo $bUrl;?>/images/bottom_str.gif');
			}
			var load_type = $(e.target.parentNode.parentNode).attr('id');
			$.ajax({
					url: "/ajax_findform/load",
					data: {type: load_type},
					dataType: 'json',
					type: 'post',
					success: function(data){
										$('#elements').html(data[0]);
										$('#add-elements').html(data[1]);
											$('li.load').removeClass('activ');
											$(e.target.parentNode.parentNode).addClass('activ');
											}
			});
<?php			// $('form').jqTransform({imgPath:'images/'}); ?>
			return false;
		});

		$('#additional').click(function(){
			var Add_obj = $(this);
			var Img_obj = $('#add-arr');
			if (isAddElemsShow) {
				isAddElemsShow = false;
				Add_obj.text("Дополнительные параметры");
				Img_obj.attr('src','<?php echo $bUrl;?>/images/bottom_str.gif');
			} else {
				isAddElemsShow = true;
				Add_obj.text("Скрыть параметры");
				Img_obj.attr('src','<?php echo $bUrl;?>/images/top_str.gif');
			}
            $('#add-elements').slideToggle(100);
            return false;
        });
	});
</script>
</head>
<body>
<div class="all">
<?php @require_once('protected/views/layouts/header01.php');?>
<div class="content">
<!-- L E F T  B L O C K -->
<div class="left_block">
	<div class="left_o_centr">
		<div class="title"><a href="#">Офисный центр "Олимп"</a></div>
		<div class="foto_center">
			<div class="left"><img src="images/centr_img.jpg" alt="" /></div>
			<div class="right"><a href="#"><img src="images/centr_img_mini.jpg" alt="" /></a><a href="#"><img src="images/centr_img_mini.jpg" alt="" /></a><a href="#"><img src="images/centr_img_mini.jpg" alt="" /></a></div>
			<div class="clear"></div>
		</div>
		<h3>Екатеринбург, Ленина, 15</h3>
		<i>Аренда офиса класса "А"</i><br />200 м <sup>2</sup> / 10 000 р.<br />
		Краткое описание офисов. Как много нам  открытий чудных готовит просвещенья...
	</div>
</div>
<!-- E N D  L E F T  B L O C K -->
<!-- R I G H T  B L O C K -->
<div class="right_block">
<div class="orange_block">
<div class="orange_block_02">
<form action="/search" method="POST">
	<div class="orange_menu">
		<ul>
			<li id="tab1" class="activ load"><div><a class="load" href="#">Офис</a></div></li>
			<li id="tab2" class="load"><div><a class="load" href="#">Склад/производство</a></div></li>
			<li id="tab3" class="load"><div><a class="load" href="#">Торговое помещение</a></div></li>
		</ul><div class="clear"></div>
	</div>
	<div id="elements"></div>
	<div id="add-elements"></div>
	<div class="submit_orange_block">
		<div class="hidden_parametrs"><img id="add-arr" src="<?php echo $bUrl;?>/images/bottom_str.gif" alt="" /> <a id="additional" href="#">Дополнительные параметры</a></div>
		<div class="right_submit">
			<input type="submit" class="gray_submit" name="btReset" value="Новый поиск" />
			<input type="submit" class="green_submit" name="btFind" value="Начать поиск" />
		</div><div class="clear"></div>
	</div>
</form>
</div><!-- / orange_block -->
</div><!-- / orange_block_02 -->

</div><!-- / right_block -->
<!-- E N D  R I G H T  B L O C K -->
<div class="clear"></div>
<div class="gor_predlog">
	<div class="title">
		<ul>
			<li><b>Горячие предложения:</b></li>
			<li><a href="#">Офисы</a></li>
			<li><a href="#">Склады</a></li>
			<li><a href="#">Офис+склад/производство</a></li>
			<li><a href="#">Торговые помещения</a></li>
		</ul>
	</div>
	<div class="gallery" id="wrap">
		<div class="jcarousel-skin-tango"><div style="position: relative; display: block;" class="jcarousel-container jcarousel-container-horizontal">
			<div style="position: relative;" class="jcarousel-clip jcarousel-clip-horizontal">
				<ul style="overflow: hidden; position: relative; top: 0px; margin: 0px; padding: 0px; left: 0px; width: 2620px;" id="mycarousel" class="jcarousel-list jcarousel-list-horizontal">
					<li jcarouselindex="1" style="float: left; list-style: none outside none; width: 242px;" class="jcarousel-item jcarousel-item-horizontal jcarousel-item-1 jcarousel-item-1-horizontal">
						<div class="gallery_bl">
							<a href="#"><img src="images/gallery_bl.jpg" alt="" /></a>
							<p>Екатеринбург, Ленина, 15 Аренда офиса класса "А"</p>	220 м<sup>2</sup> / 40 000 р.
						</div>
					</li>
					<li jcarouselindex="2" style="float: left; list-style: none outside none; width: 242px;" class="jcarousel-item jcarousel-item-horizontal jcarousel-item-1 jcarousel-item-1-horizontal">
						<div class="gallery_bl">
							<a href="#"><img src="images/gallery_bl.jpg" alt="" /></a>
							<p>Екатеринбург, Ленина, 15 Аренда офиса класса "А"</p>	220 м<sup>2</sup> / 40 000 р.
						</div>
					</li>
					<li jcarouselindex="3" style="float: left; list-style: none outside none; width: 242px;" class="jcarousel-item jcarousel-item-horizontal jcarousel-item-1 jcarousel-item-1-horizontal">
						<div class="gallery_bl">
							<a href="#"><img src="images/gallery_bl.jpg" alt="" /></a>
							<p>Екатеринбург, Ленина, 15 Аренда офиса класса "А"</p>	220 м<sup>2</sup> / 40 000 р.
						</div>
					</li>
					<li jcarouselindex="4" style="float: left; list-style: none outside none; width: 242px;" class="jcarousel-item jcarousel-item-horizontal jcarousel-item-1 jcarousel-item-1-horizontal">
						<div class="gallery_bl">
							<a href="#"><img src="images/gallery_bl.jpg" alt="" /></a>
							<p>Екатеринбург, Ленина, 15 Аренда офиса класса "А"</p>	220 м<sup>2</sup> / 40 000 р.
						</div>
					</li>
					<li jcarouselindex="5" style="float: left; list-style: none outside none; width: 242px;" class="jcarousel-item jcarousel-item-horizontal jcarousel-item-1 jcarousel-item-1-horizontal">
						<div class="gallery_bl">
							<a href="#"><img src="images/gallery_bl.jpg" alt="" /></a>
							<p>Екатеринбург, Ленина, 15 Аренда офиса класса "А"</p>	220 м<sup>2</sup> / 40 000 р.
						</div>
					</li>
					<li jcarouselindex="6" style="float: left; list-style: none outside none; width: 242px;" class="jcarousel-item jcarousel-item-horizontal jcarousel-item-1 jcarousel-item-1-horizontal">
						<div class="gallery_bl">
							<a href="#"><img src="images/gallery_bl.jpg" alt="" /></a>
							<p>Екатеринбург, Ленина, 15 Аренда офиса класса "А"</p>	220 м<sup>2</sup> / 40 000 р.
						</div>
					</li>
					<li jcarouselindex="7" style="float: left; list-style: none outside none; width: 242px;" class="jcarousel-item jcarousel-item-horizontal jcarousel-item-1 jcarousel-item-1-horizontal">
						<div class="gallery_bl">
							<a href="#"><img src="images/gallery_bl.jpg" alt="" /></a>
							<p>Екатеринбург, Ленина, 15 Аренда офиса класса "А"</p>	220 м<sup>2</sup> / 40 000 р.
						</div>
					</li>
					<li jcarouselindex="8" style="float: left; list-style: none outside none; width: 242px;" class="jcarousel-item jcarousel-item-horizontal jcarousel-item-1 jcarousel-item-1-horizontal">
						<div class="gallery_bl">
							<a href="#"><img src="images/gallery_bl.jpg" alt="" /></a>
							<p>Екатеринбург, Ленина, 15 Аренда офиса класса "А"</p>	220 м<sup>2</sup> / 40 000 р.
						</div>
					</li>
					<li jcarouselindex="9" style="float: left; list-style: none outside none; width: 242px;" class="jcarousel-item jcarousel-item-horizontal jcarousel-item-1 jcarousel-item-1-horizontal">
						<div class="gallery_bl">
							<a href="#"><img src="images/gallery_bl.jpg" alt="" /></a>
							<p>Екатеринбург, Ленина, 15 Аренда офиса класса "А"</p>	220 м<sup>2</sup> / 40 000 р.
						</div>
					</li>
					<li jcarouselindex="10" style="float: left; list-style: none outside none; width: 242px;" class="jcarousel-item jcarousel-item-horizontal jcarousel-item-1 jcarousel-item-1-horizontal">
						<div class="gallery_bl">
							<a href="#"><img src="images/gallery_bl.jpg" alt="" /></a>
							<p>Екатеринбург, Ленина, 15 Аренда офиса класса "А"</p>	220 м<sup>2</sup> / 40 000 р.
						</div>
					</li>
				</ul>
			</div>
			<div disabled="true" style="display: block;" class="jcarousel-prev jcarousel-prev-horizontal jcarousel-prev-disabled jcarousel-prev-disabled-horizontal">
			</div>
			<div disabled="false" style="display: block;" class="jcarousel-next jcarousel-next-horizontal">
			</div>
		</div>
	</div>
</div>
</div><!-- / gor_predlog -->


<!-- L E F T  B L O C K -->
<div class="left_block">
<?php $this->widget('LeftNews', array('qty'=>3));?>
</div>
<!-- E N D  L E F T  B L O C K -->


<!-- R I G H T  B L O C K -->
<div class="right_block">
	<div class="content_text">
		<div class="title_site"><a href="#">О проекте</a></div>
			<?php echo $content; ?>
<?php /* $this->widget('GetReclameItem', array()); */?>
<?php /* $this->widget('GetReclameBanner', array()); */?>
	</div>
</div>
<!-- E N D  R I G H T  B L O C K -->
<div class="clear"></div>


</div>
</div>
<?php @require_once('protected/views/layouts/footer01.php');?>
</body>
</html>