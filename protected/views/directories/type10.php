<script type="text/javascript">
	$(document).ready(function(){
		$('select#city').change(function(){
			var select_city = $("select option:selected").val();
			$.ajax({
				url: "<?php echo $this->createUrl('ajaxdicts/'.$nameAction); ?>",
				data: {id: select_city, clName: "<?php echo $className; ?>"},
				dataType: 'html',
				type: 'post',
				success: function(data){
					$('div#show-box').html(data);
				}
			});
		});
	});
</script>
<style type="text/css">
table.sample {
  border-spacing: 2px;
  border-collapse: collapse;
  width: 80%;
  text-align: left;
}
table.sample th {
  border: 1px solid #000000;
  padding: 5px;
  background-color: #aaaaaa;
  text-align: center;
}
table.sample td {
  border: 1px solid #000000;
  padding: 5px;
}
table.sample td.action {
  text-align: center;
}

/* Из файла page.css - стилизация пейджера */
ul.yiiPager {
    border: 0 none;
    display: inline;
    font-size: 11px;
    line-height: 100%;
    margin: 0;
    padding: 0;
}
ul.yiiPager li {
    display: inline;
}
ul.yiiPager a:link, ul.yiiPager a:visited {
    border: 1px solid #9AAFE5;
    color: #0E509E;
    font-weight: bold;
    padding: 1px 6px;
    text-decoration: none;
}
ul.yiiPager .page a {
    font-weight: normal;
}
ul.yiiPager a:hover {
    border: 1px solid #0E509E;
}
ul.yiiPager .selected a {
    background: none repeat scroll 0 0 #2E6AB1;
    color: #FFFFFF;
    font-weight: bold;
}
ul.yiiPager .hidden a {
    border: 1px solid #DEDEDE;
    color: #888888;
}

</style>
<p>
	<?php echo $dicName; ?>: [<?php echo CHtml::link('отмена', array('directories/view_all')); ?>]<br />
	<?php echo CHtml::link('Добавить', array($linkAdd)); ?>
</p>
<?php echo CHtml::dropDownList('city', $sel_city, $Cities); ?><br /><br />
<div id="show-box"><?php
if (!empty($rows)):?>
<table class="sample">
	<col width="35%" />
	<col width="45%" />
	<col width="10%" />
	<col width="10%" />
	<tr>
		<th>Город</th>
		<th>Наименование</th>
		<th>Ред.</th>
		<th>Уд.</th>
	</tr>
<?php
	foreach ($rows as $object){
		$this->renderPartial('_type10', array(
			'clName'	=>$className,
			'id'		=>$object->id,
			'city'		=>$object->city,
			'name'		=>$object->name
		));
	}?>
</table>
<br />
<div align="center">
<?php
	$this->widget('CLinkPager',array(
		'pages'          =>$pages,
		'maxButtonCount' =>5, # максимальное колличество вкладок на странице
		'header'         =>'<b>Перейти к странице:</b><br>', # заголовок
		'nextPageLabel'  =>'Вперед&gt;', # название кнопок навигаций next и prev
		'prevPageLabel'  =>'&lt;Назад',
	));
?>
</div>
<?php endif;?></div>
