<style type="text/css">
table.sample {
  border-spacing: 2px;
  border-collapse: collapse;
  width: 80%;
  text-align: center;
}
table.sample th {
  border: 1px solid #000000;
  padding: 5px;
  background-color: #aaaaaa;
}
table.sample td {
  border: 1px solid #000000;
  padding: 5px;
}
table.sample td.name {  width: 100%;
  text-align: left;}
</style>
<p>
	Качество постройки: [<?php echo CHtml::link('отмена', array('directories/view_all')); ?>]<br />
	<?php echo CHtml::link('Добавить', array('directories/quality_add')); ?>
</p>
<table class="sample">
	<tr>
		<th>Name</th>
		<th>Edit</th>
		<th>Delete</th>
	</tr>
<?php
  if (!empty($Quality_obj)){
    foreach ($Quality_obj as $object){
      $this->renderPartial('_quality', array(
                                             'id'   =>$object->id_tb1,
                                             'name' =>$object->name,
                                             ));
    }?>
</table>
<?php }else{ ?>
	<tr>
		<td colspan="3">Варианты отсутствуют.</td>
	</tr>
</table>
<?php } ?>
