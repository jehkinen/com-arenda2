<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/form.css'); ?>
Добавить <?php echo $dicName;?> [<?php echo CHtml::link('отмена', array($linkCancel)); ?>]
<div class="form">
  <?php echo CHtml::form(); ?>
  <?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:'); ?>
<div class="row">
  <?php echo CHtml::activeLabel($form, 'city'); ?><br />
  <?php echo CHtml::dropDownList($modelName.'[city]', $selCity, $Cities); ?>
</div>
<div class="row">
  <?php echo CHtml::activeLabel($form, 'name'); ?><br />
  <?php echo CHtml::activeTextField($form, 'name', array('size'=>31, 'maxlength'=>31)); ?>
</div>
<div class="row buttons">
  <?php echo CHtml::submitButton('Сохранить', array('id'=>"submit")); ?>
</div>
<?php echo CHtml::endForm(); ?>
</div><!--form-->
