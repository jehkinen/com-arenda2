<script type="text/javascript">
	$(document).ready(function(){
		$('select#city').change(function(){
			var select_city = $("select option:selected").val();
			$.ajax({
				url: "<?php echo $this->createUrl('ajaxdicts/'.$nameAction); ?>",
				data: {id: select_city, clName: "<?php echo $className; ?>"},
				dataType: 'html',
				type: 'post',
				success: function(data){
					$('div#show-box').html(data);
				}
			});
		});
	});
</script>
<style type="text/css">
table.sample {
  border-spacing: 2px;
  border-collapse: collapse;
  width: 80%;
  text-align: left;
}
table.sample th {
  border: 1px solid #000000;
  padding: 5px;
  background-color: #aaaaaa;
  text-align: center;
}
table.sample td {
  border: 1px solid #000000;
  padding: 5px;
}
table.sample td.action {
  text-align: center;
}

</style>
<p>
	<?php echo $dicName; ?>: [<?php echo CHtml::link('отмена', array('directories/view_all')); ?>]<br />
	<?php echo CHtml::link('Добавить', array($linkAdd)); ?>
</p>
<?php echo CHtml::dropDownList('city', $sel_city, $Cities); ?><br /><br />
<div id="show-box"><?php
if (!empty($rows)):?>
<table class="sample">
	<col width="30%" />
	<col width="40%" />
	<col width="20%" />
	<col width="5%" />
	<col width="5%" />
	<tr>
		<th>Город</th>
		<th>Наименование</th>
		<th>Название кратко</th>
		<th>Ред.</th>
		<th>Уд.</th>
	</tr>
<?php
	foreach ($rows as $object){
		$this->renderPartial('_type11', array(
			'clName'	=>$className,
			'id'		=>$object->id,
			'city'		=>$object->city,
			'name'		=>$object->name,
			'shname'	=>$object->shname
		));
	}?>
</table>
<br />
<?php endif;?></div>
