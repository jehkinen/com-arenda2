<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/form.css'); ?>
Изменть вариант качества [<?php echo CHtml::link('отмена', array('directories/quality')); ?>]
<div class="form">
  <?php echo CHtml::form(); ?>
  <?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:'); ?>
<div class="row">
  <?php echo CHtml::activeLabel($form, 'name'); ?><br />
  <?php echo CHtml::activeTextField($form, 'name', array('value'=>$name, 'size'=>30, 'maxlength'=>30)); ?>
  <span id="nameSpan"></span>
</div>
<div class="row buttons">
  <?php echo CHtml::submitButton('Сохранить', array('id'=>"submit")); ?>
</div>
<?php echo CHtml::endForm(); ?>
</div><!--form-->
