<br />
<?php echo CHtml::link('Области', array('directories/areas')); ?><br />
<?php echo CHtml::link('Города', array('directories/city')); ?><br />
<?php echo CHtml::link('Районы', array('directories/districts')); ?><br />
<?php echo CHtml::link('Улицы', array('directories/street')); ?><br />
<br />
<?php echo CHtml::link('Интернет-провайдеры', array('directories/netprovs')); ?><br />
<?php echo CHtml::link('Телефонные компании', array('directories/telcomp')); ?><br />
<br />
<?php echo CHtml::link('Типы сооружений', array('directories/kindsstruc')); ?><br />
<?php echo CHtml::link('Классы объектов', array('directories/objectclasses')); ?><br />
<?php echo CHtml::link('Функциональное назначение', array('directories/functional')); ?><br />
<br />
<?php echo CHtml::link('Формы договоров', array('directories/contractform')); ?><br />
<?php echo CHtml::link('Условия ком.платежей', array('directories/payutils')); ?><br />
<br />
<?php echo CHtml::link('Виды покрытия пола', array('directories/flooring')); ?><br />
<?php echo CHtml::link('Виды паркинга', array('directories/parking')); ?><br />
<?php echo CHtml::link('Состояние ремонта', array('directories/repair')); ?><br />
<?php echo CHtml::link('Системы вентиляции', array('directories/ventilation')); ?><br />
<br />