<div class="form">
	<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cities2-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model, 'Исправьте, пожалуйста, следующие ошибки:'); ?>

    <div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name'); ?>
		<?php echo $form->error($model,'name'); ?>
    </div>

    <div class="row">
		<?php echo $form->labelEx($model,'area'); ?>
		<?php echo $form->DropDownList($model,'area', Chtml::listData(Area::model()->with('cities')->findAll(), 'id', 'name')); ?>
		<?php echo $form->error($model,'area'); ?>
    </div>

    <div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array('id'=>"submit")); ?>
    </div>

	<?php $this->endWidget(); ?>
</div>