<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/form.css'); ?>
Добавить <?php echo $dicName;?> [<?php echo CHtml::link('отмена', array($linkCancel)); ?>]
<div class="form">
  <?php echo CHtml::form(); ?>
  <?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:'); ?>
<div class="row">
  <?php echo CHtml::activeLabel($form, 'name'); ?><br />
  <?php echo CHtml::activeTextField($form, 'name', array('size'=>47, 'maxlength'=>47)); ?>
</div>
<div class="row">
  <?php echo CHtml::activeLabel($form, 'shname'); ?><br />
  <?php echo CHtml::activeTextField($form, 'shname', array('size'=>47, 'maxlength'=>23)); ?>
</div>
<div class="row buttons">
  <?php echo CHtml::submitButton('Сохранить', array('id'=>"submit")); ?>
</div>
<?php echo CHtml::endForm(); ?>
</div><!--form-->
