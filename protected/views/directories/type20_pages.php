<style type="text/css">
table.sample {
  border-spacing: 2px;
  border-collapse: collapse;
  width: 80%;
  text-align: left;
}
table.sample th {
  border: 1px solid #000000;
  padding: 5px;
  background-color: #aaaaaa;
  text-align: center;
}
table.sample td {
  border: 1px solid #000000;
  padding: 5px;
}
table.sample td.action {
  text-align: center;
}
</style>
<p>
	<?php echo $dicName; ?>: [<?php echo CHtml::link('отмена', array('directories/view_all')); ?>]<br />
	<?php echo CHtml::link('Добавить', array($linkAdd)); ?>
</p>
<table class="sample">
	<col width="45%" />
	<col width="35%" />
	<col width="10%" />
	<col width="10%" />
	<tr>
		<th>Название</th>
		<th>Название кратко</th>
		<th>Ред.</th>
		<th>Уд.</th>
	</tr>
<?php
  if (!empty($Rows)){
    foreach ($Rows as $object){
      $this->renderPartial('_type20', array(
		'clName'	=>$className,
		'id'		=>$object->id,
		'name'		=>$object->name,
		'shname'	=>$object->shname
	));
    }?>
</table>
<!-- Добавляем спец. виджет яшы чтоб разбить постранично -->
<br />
<div align="center">
<?php
  $this->widget('CLinkPager',array(
                                   'pages'          =>$pages,
                                   'maxButtonCount' =>5, # максимальное колличество вкладок на странице
                                   'header'         =>'<b>Перейти к странице:</b><br>', # заголовок
                                   'nextPageLabel'  =>'Вперед&gt;', # название кнопок навигаций next и prev
                                   'prevPageLabel'  =>'&lt;Назад',
  ));
?>
</div>
<!-- конец виджета для разбивки постранично -->
<?php }else{ ?>
	<tr>
		<td colspan="4">Справочник пуст.</td>
	</tr>
</table>
<?php } ?>
