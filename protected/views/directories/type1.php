<style>
	.admin-page{
		padding-left: 20px;
	}
		.admin-page h1{
			font-size: 20px;
		}

    #areas-grid{
        padding:20px 0 !important;
    }
    #areas-grid .button-column .view{
        display: none;
    }
    #areas-grid #areas-grid_c0{
        width: 50px;
    }
    #areas-grid{
        padding: 0 30px;
        width: 90%;
    }

</style>

<div class="admin-page">

<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('areas-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Области</h1>

<? echo  CHtml::link('Добавить область', Yii::app()->createUrl("directories/add_area"), array('class'=>'add_button')); ?>
[<?php echo CHtml::link('отмена', array('directories/view_all')); ?>]<br />

<?/*php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); */?>
<!-- search-form -->



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'areas-grid',
	'dataProvider'=>$model->search(),
	//'summaryText'=>'Всего найдено : {count}',
	'summaryText'=>'',
	//'filter'=>$model,
	'ajaxUpdate'=>'false',
	'columns'=>array(
		'id',
		'name',
		array
		(
		'class'=>'CButtonColumn',
			'deleteConfirmation'=>"js:'Область '+$(this).parent().parent().children(':first-child').next().text()+' будет удалена! Продолжить?'",
		'template'=>'{edit}{delete}',
		'buttons'=>array
		(
			'delete' => array
			(
				'label'=>'x',
				//'imageUrl'=>Yii::app()->request->baseUrl.'/images/email.png',
				'url'=>'Yii::app()->createUrl("directories/delete_area", array("id"=>$data->id))',
			),
			'edit' => array
			(
				'label'=>'редактировать',
				//'imageUrl'=>Yii::app()->request->baseUrl.'/images/email.png',
				'url'=>'Yii::app()->createUrl("directories/edit_area", array("id"=>$data->id))',
			),

		),
	),
	),
)); ?>
</div>