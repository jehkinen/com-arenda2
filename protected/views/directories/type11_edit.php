<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/form.css'); ?>
Изменить <?php echo $dicName;?> [<?php echo CHtml::link('отмена', array($linkCancel)); ?>]
<div class="form">
  <?php echo CHtml::form(); ?>
  <?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:'); ?>
<div class="row">
  <?php echo CHtml::activeLabel($form, 'city'); ?><br />
  <?php echo CHtml::activeTextField($form, 'city', array('value'=>$row->city, 'size'=>31, 'maxlength'=>31, 'disabled'=>'diasbled')); ?>
</div>
<div class="row">
  <?php echo CHtml::activeLabel($form, 'name'); ?><br />
  <?php echo CHtml::activeTextField($form, 'name', array('value'=>$row->name, 'size'=>47, 'maxlength'=>47)); ?>
</div>
<div class="row">
  <?php echo CHtml::activeLabel($form, 'shname'); ?><br />
  <?php echo CHtml::activeTextField($form, 'shname', array('value'=>$row->shname, 'size'=>47, 'maxlength'=>23)); ?>
</div>
<div class="row buttons">
  <?php echo CHtml::submitButton('Сохранить', array('id'=>"submit")); ?>
</div>
<?php echo CHtml::endForm(); ?>
</div><!--form-->
