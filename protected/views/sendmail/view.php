<div class="story">
	<h1>Мы ценим Ваши отзывы и предложения</h1>
	<p>Вот тут какой-нибудь текст о том, что если вдруг у кого-то появились предложения, мысли, идеи... Или обнаружилось желание посодействовать расширению географии сайта.</p>
	<?php echo CHtml::form('', 'post', array('class'=>'form01 m_0000_0245_0015_0000'));?>
	<h2>Форма обратной связи</h2>
	<div class="errorFormat02">
		<?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:');?>
	</div>
	<h3 class="form01_h3">Контактные данные</h3>
	<div class="p_0010_0000_0010_0000">
		<div class="input01_lbl w130">Получатель</div><?php echo CHtml::dropDownList('SendMail[receiver]', 1, $recMails, array('class'=>'dropbox01 w306'));?>
	</div>
	<div class="p_0010_0000_0010_0000">
		<div class="input01_lbl w130">Ваше Имя:</div><?php echo CHtml::activeTextField($form, 'name', array('maxlength'=>30, 'class'=>'input01 w300'));?> <b class="cB83939 vt">*</b>
	</div>
	<div class="p_0010_0000_0010_0000">
		<div class="input01_lbl w130">Ваше e-mail:</div><?php echo CHtml::activeTextField($form, 'e_mail', array('maxlength'=>30, 'class'=>'input01 w300'));?> <b class="cB83939 vt">*</b>
	</div>
	<h3 class="form01_h3">Сообщение</h3>
	<div class="p_0010_0000_0010_0000">
		Текст <b class="cB83939 vt">*</b><br /><?php echo CHtml::textArea('SendMail[messTxt]', $text, array('class'=>'h90 textarea02 w438'));?>
	</div>
	<h3 class="dib h301 m_0015_0000_0000_0000">Введите результат вычисления</h3>
	<div class="dib"><?php $this->widget('CCaptcha', array('buttonLabel' => ''));?></div>
	<div class="dib">
		<?php echo CHtml::activeTextField($form, 'verifyCode', array('maxlength'=>5, 'class'=>'input01 m_0015_0000_0000_0000 w60'));?><br/>
		<span><a class="captcha01_rfs" id="yt0" href="#">Обновить</a></span>
	</div>
	<div class="p_0010_0000_0010_0000 tr">
		<?php echo CHtml::submitButton('Отправить', array('id'=>'submit', 'class'=>'button02'));?>
	</div>
	<?php echo CHtml::endForm();?>
</div><!--story-->
 