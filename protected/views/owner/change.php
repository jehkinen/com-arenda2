Изменение статус объекта [<?php echo CHtml::link('отмена', array('owner/view')); ?>]
<div class="form">
  <?php echo CHtml::form(); ?>
  <?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:'); ?>
  <div class="row">
    <fieldset style="border:1;">
		<legend>Статус объекта</legend>
    	<?php echo CHtml::activeRadioButtonList($form, 'status', $listStat); ?>
    </fieldset>
  </div>
  <div class="row">
    <br /><?php echo CHtml::submitButton('Сохранить', array('id'=>'submit')); ?>
  </div>
  <?php echo CHtml::endForm(); ?>
</div><!--form-->
