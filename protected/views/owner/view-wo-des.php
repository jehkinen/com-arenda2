<style type="text/css">
div.pane{
	border: 2px solid #aaaaaa !important;
	padding: 5px 10px !important;
	margin-bottom: 10px;
}
</style>
<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/tabs.css'); ?>
<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/form.css'); ?>
<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/modal.css'); ?>
<script type="text/javascript" src="js/tabs.js"></script>
<div class="tabs">
<!-- Это сами вкладки -->
	<ul class="tabNavigation">
		<li><a class="" href="<?php echo $this->createUrl('tenant/view'); ?>#first">Настройки</a></li>
		<li><a class="" href="<?php echo $this->createUrl('tenant/view'); ?>#second">Мои объекты</a></li>
		<li><a class="" href="<?php echo $this->createUrl('tenant/view'); ?>#third">Заявки</a></li>
		<li><a class="" href="<?php echo $this->createUrl('tenant/view'); ?>#fourth">Баланс</a></li>
	</ul>
<!-- Это контейнеры содержимого -->
	<div id="first">
		<p>
			Опции
			<div class="form">
				<?php echo CHtml::form(); ?>
				<?php echo CHtml::errorSummary($form_options, 'Исправьте, пожалуйста, следующие ошибки:'); ?>
					<div class="row">
						<?php echo CHtml::activeCheckBox($form_options, 'info_moder', array('checked'=>$user->info_moder)); ?>&nbsp;
						<?php echo CHtml::activeLabel($form_options, 'info_moder'); ?>
					</div>
					<div class="row">
						<?php echo CHtml::activeCheckBox($form_options, 'info_order', array('checked'=>$user->info_order)); ?>&nbsp;
						<?php echo CHtml::activeLabel($form_options, 'info_order'); ?>
					</div>
					<fieldset><legend>Контакты для отправки уведомлений</legend>
					<div class="row">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<?php echo CHtml::activeCheckBox($form_options, 'info_flag_phone', array('checked'=>$user->info_flag_phone)); ?>&nbsp;
						<?php echo CHtml::activeLabel($form_options, 'info_flag_phone'); ?>&nbsp;
						<?php echo CHtml::activeLabel($form_options, 'info_phone'); ?>
						<?php echo CHtml::activeTextField($form_options, 'tel_part1', array('value'=>$tel1, 'size'=>2, 'maxlength'=>3)) ?>&nbsp;
						<?php echo CHtml::activeTextField($form_options, 'tel_part2', array('value'=>$tel2, 'size'=>7, 'maxlength'=>7)); ?>
					</div>
					<div class="row">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<?php echo CHtml::activeCheckBox($form_options, 'info_flag_email', array('checked'=>$user->info_flag_email)); ?>&nbsp;
						<?php echo CHtml::activeLabel($form_options, 'info_flag_email'); ?>&nbsp;
						<?php echo CHtml::activeLabel($form_options, 'info_email'); ?>&nbsp;
						<?php echo CHtml::activeTextField($form_options, 'info_email', array('value'=>$user->info_email, 'size'=>23, 'maxlength'=>39)); ?>
					</div>
					</fieldset>
					<div class="row buttons">
						<?php echo CHtml::submitButton('Сохранить', array('id'=>'submit')); ?>
					</div>
				<?php echo CHtml::endForm(); ?>
			</div><!--form-->

			<hr />Изменить данные о себе

			<div class="form">
				<?php echo CHtml::form(); ?>
				<?php echo CHtml::errorSummary($form_info, 'Исправьте, пожалуйста, следующие ошибки:'); ?>
					<div class="row">
						<?php echo CHtml::activeLabel($form_info, 'name'); ?> *<br />
						<?php echo CHtml::activeTextField($form_info, 'name', array('value'=>$user->name, 'size'=>30, 'maxlength'=>30)); ?>
					</div>
					<div class="row">
						<?php echo CHtml::activeLabel($form_info, 'so_name'); ?> *<br />
						<?php echo CHtml::activeTextField($form_info, 'so_name', array('value'=>$user->so_name, 'size'=>30, 'maxlength'=>30)); ?>
					</div>
					<div class="row">
						<?php echo CHtml::activeLabel($form_info, 'otchestvo'); ?><br />
						<?php echo CHtml::activeTextField($form_info, 'otchestvo', array('value'=>$user->otchestvo, 'size'=>30, 'maxlength'=>30)); ?>
					</div>
					<div class="row">
						<?php echo CHtml::activeLabel($form_info, 'e_mail'); ?> *<br />
						<?php echo CHtml::activeTextField($form_info, 'e_mail', array('value'=>$user->e_mail, 'size'=>30, 'maxlength'=>30)); ?>
					</div>
					<div class="row">
						<?php echo CHtml::activeLabel($form_info, 'phone'); ?> * (виден посетителям)<br />
						<?php echo CHtml::activeTextField($form_info, 'phone', array('value'=>$user->phone, 'size'=>30, 'maxlength'=>15)); ?>
					</div>
					<div class="row buttons">
						<?php echo CHtml::submitButton('Сохранить', array('id'=>'submit')); ?>
					</div>
				<?php echo CHtml::endForm(); ?>
			</div><!--form-->

			<hr />Сменить пароль доступа

			<div class="form">
				<?php echo CHtml::form(); ?>
				<?php echo CHtml::errorSummary($form_pass, 'Исправьте, пожалуйста, следующие ошибки:'); ?>
					<div class="row">
						<?php echo CHtml::activeLabel($form_pass, 'password'); ?><br />
						<?php echo CHtml::activePasswordField($form_pass, 'password', array('size'=>30, 'maxlength'=>10)); ?>
					</div>
					<div class="row">
						<?php echo CHtml::activeLabel($form_pass, 'confirm'); ?><br />
						<?php echo CHtml::activePasswordField($form_pass, 'confirm', array('size'=>30, 'maxlength'=>10)); ?>
					</div>
					<div class="row buttons">
						<?php echo CHtml::submitButton('Сохранить', array('id'=>'submit')); ?>
					</div>
				<?php echo CHtml::endForm(); ?>
			</div><!--form-->
		</p>
	</div>
	<div id="second">
		<p>
<?php if (empty($rows)){?>
У Вас объектов нет.
<?php }else{
foreach ($rows as $row){
	$DataParser_obj = $this->widget('DataParser', array('data_db'=>$row->created));
	$this->renderPartial('_notepad', array(
		'row'	=>$row,
		'day'	=>$DataParser_obj->day,
		'month'	=>$DataParser_obj->month,
		'year'	=>$DataParser_obj->year
	));
}}?>
		</p>
	</div>
	<div id="third">
<?php if (empty($tenders)){?>
Нет заявок, подходящих к Вашим объектам
<?php }else{
foreach ($tenders as $row)
	$this->renderPartial('_tenders', array('row' =>$row));
}?>
	</div>
	<div id="fourth">
		<p>Сколько и кому я должен, когда и какие были произведены платежи.</p>
	</div>
</div>
<div id="owner-change">
	<div class="header"></div>
	<div class="message">
		<input type="radio" name="stat" value="1" alt="не предоставляется" id="rb0" /><label for="rb0">отменить показы</label><br />
		<input type="radio" name="stat" value="6" alt="идут показы" id="rb3" /><label for="rb3">возобновить показы</label>
	</div>
	<div class="buttons">
		<div class="yes">Сохранить</div><div class="no close">Отмена</div>
	</div>
</div>
<div id="owner-delete">
	<div class="header"></div>
	<div class="message"></div>
	<div class="buttons">
		<div class="yes">Да</div><div class="no close">Нет</div>
	</div>
</div>
<div style="display: none;">
	<img src="images/modal/header.gif" alt="" />
	<img src="images/modal/button.gif" alt="" />
</div>
<script type="text/javascript" src="js/jquery.simplemodal.js"></script>
<script type="text/javascript" src="js/owner.js"></script>
