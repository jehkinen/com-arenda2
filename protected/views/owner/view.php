<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/tabs.css'); ?>
<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/form.css'); ?>
<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/modal.css'); ?>
<script type="text/javascript" src="js/tabs.js"></script>

<div class="breadcrumbs">
	<a href="/">Главная</a> &rarr; Личный кабинет
</div><!-- /breadcrumbs -->

<div class="cabinet">
	<div class="cabNavShadow">
		<div class="cabNav">
		<div class="cabNavIn">
			<a href="<?php echo $this->createUrl('owner/view'); ?>#first" class="active"><i><u>Настройки</u></i></a>
			<a href="<?php echo $this->createUrl('owner/view'); ?>#second"><i><u>Мои объекты</u></i></a>
			<a href="<?php echo $this->createUrl('owner/view'); ?>#third"><i><u>Заявки</u></i></a>
		</div><!-- /cabNavIn -->
		</div><!-- /cabNav -->
	</div><!-- /cabNavShadow -->
	<div class="cabTabs">
	<div id="first"><!-- FIRST tab -->

	<div class="cabBlock">
		<h3>Настройка уведомлений</h3>
			<?php echo CHtml::form(); ?>
			<?php echo CHtml::errorSummary($form_options, 'Исправьте, пожалуйста, следующие ошибки:'); ?>
			<fieldset >
              	<?php echo CHtml::activeCheckBox($form_options, 'info_moder', array('checked'=>$user->info_moder, 'class'=>'chkbx')); ?> <?php echo CHtml::activeLabel($form_options, 'info_moder', array('class'=>'chkbxSpan')); ?><br />
              	<?php echo CHtml::activeCheckBox($form_options, 'info_order', array('checked'=>$user->info_order, 'class'=>'chkbx')); ?> <?php echo CHtml::activeLabel($form_options, 'info_order', array('class'=>'chkbxSpan')); ?><br />
				<br /><br />
				<div class="bordered clearafter">
					<h4>Контакты для отправки уведомлений</h4><br />
					<div class="Lcol">
						<p><?php echo CHtml::activeCheckBox($form_options, 'info_flag_phone', array('checked'=>$user->info_flag_phone)); ?> <?php echo CHtml::activeLabel($form_options, 'info_flag_phone', array('class'=>'chkbxSpan chkbx')); ?></p>
						<p><?php echo CHtml::activeCheckBox($form_options, 'info_flag_email', array('checked'=>$user->info_flag_email)); ?> <?php echo CHtml::activeLabel($form_options, 'info_flag_email', array('class'=>'chkbxSpan chkbx')); ?></p>
					</div><!-- /Lcol -->
					<div class="Rcol">
						<span class="plus7">+7</span> <?php echo CHtml::activeTextField($form_options, 'tel_part1', array('value'=>$tel1, 'size'=>3, 'maxlength'=>3, 'class'=>'num3')) ?> <?php echo CHtml::activeTextField($form_options, 'tel_part2', array('value'=>$tel2, 'size'=>3, 'maxlength'=>7, 'class'=>'num7')); ?><br />
						<?php echo CHtml::activeTextField($form_options, 'info_email', array('value'=>$user->info_email, 'size'=>3, 'maxlength'=>39, 'class'=>'mail')); ?>
					</div><!-- /Rcol -->
				</div><!-- /bordered -->
				<?php echo CHtml::submitButton('Сохранить настройки', array('id'=>'submit', 'class'=>'button long')); ?>
			</fieldset>
			<?php echo CHtml::endForm(); ?>	
	</div><!-- /cabBlock -->
	<div class="cabBlock">
		<h3>Личные данные</h3>
		<?php echo CHtml::form(); ?>
		<?php echo CHtml::errorSummary($form_info, 'Исправьте, пожалуйста, следующие ошибки:'); ?>
		<fieldset >
			<label for="Owner_info_name">Имя <span class="req">*</span></label>
			<?php echo CHtml::activeTextField($form_info, 'name', array('value'=>$user->name, 'size'=>20, 'maxlength'=>30, 'class'=>'inputbox')); ?>

			<label for="Owner_info_so_name">Фамилия <span class="req">*</span></label>
			<?php echo CHtml::activeTextField($form_info, 'so_name', array('value'=>$user->so_name, 'size'=>20, 'maxlength'=>30, 'class'=>'inputbox')); ?>

			<?php echo CHtml::activeLabel($form_info, 'otchestvo'); ?>
			<?php echo CHtml::activeTextField($form_info, 'otchestvo', array('value'=>$user->otchestvo, 'size'=>20, 'maxlength'=>30, 'class'=>'inputbox')); ?>

			<h5>Контактные данные</h5>

			<label for="Owner_info_e_mail">E-mail <span class="req">*</span></label>
			<?php echo CHtml::activeTextField($form_info, 'e_mail', array('value'=>$user->e_mail, 'size'=>20, 'maxlength'=>30, 'class'=>'inputbox')); ?>

			<label for="Owner_info_phone">Телефон <span class="req">*</span> (виден посетителям)</label>
			<?php echo CHtml::activeTextField($form_info, 'phone', array('value'=>$user->phone, 'size'=>20, 'maxlength'=>15, 'class'=>'inputbox')); ?>
			<br />
			<?php echo CHtml::submitButton('Сохранить данные', array('id'=>'submit', 'class'=>'button')); ?>
		</fieldset>
		<?php echo CHtml::endForm(); ?>
	</div><!-- /cabBlock -->
	
	<div class="cabBlock">
		<h3>Сменить пароль доступа</h3>
		<?php echo CHtml::form(); ?>
		<?php echo CHtml::errorSummary($form_pass, 'Исправьте, пожалуйста, следующие ошибки:'); ?>
		<fieldset >
			<label for="Owner_pass_password">Новый пароль <span class="req">*</span></label>
			<?php echo CHtml::activePasswordField($form_pass, 'password', array('size'=>20, 'maxlength'=>10, 'class'=>'inputbox')); ?>

			<label for="Owner_pass_confirm">Повторите новый пароль <span class="req">*</span></label>
			<?php echo CHtml::activePasswordField($form_pass, 'confirm', array('size'=>20, 'maxlength'=>10, 'class'=>'inputbox')); ?>
<!--			<span class="passOK">Пароли совпадают</span> -->
			<br />
			<?php echo CHtml::submitButton('Сохранить пароль', array('id'=>'submit', 'class'=>'button')); ?>
		</fieldset>
		<?php echo CHtml::endForm(); ?>

	</div><!-- /cabBlock -->
	</div><!-- /FIRST tab -->
	<div id="second"><!-- SECOND tab -->
	<div class="cabBlock">
		<h3>Список объектов</h3>
<?php
  if (!empty($rows)){
foreach ($rows as $row){
	$DataParser_obj = $this->widget('DataParser', array('data_db'=>$row->created));
	$this->renderPartial('_notepad', array(
		'row'	=>$row,
		'day'	=>$DataParser_obj->day,
		'month'	=>$DataParser_obj->month,
		'year'	=>$DataParser_obj->year
	));
    }
  }	else {?>
	<div class="empty-block">
		<h4>У Вас объектов нет</h4>
		<p>Вы можете разместить объекты в нашей Базе Данных, перейдя по ссылке <?php echo CHtml::link('Разместить объект', array('object/add/step/step1')); ?></p>
	</div>
<?php }?>
	</div><!-- /cabBlock -->
	</div><!-- /SECOND tab -->
	<div id="third"><!-- THIRD tab -->
	<div class="cabBlock">
		<h3>Перечень заявок</h3>
<?php
  if (!empty($tenders)){
    foreach ($tenders as $row){
      $this->renderPartial('_tenders', array(
			'row'   =>$row
		));
    }
  }	else {?>
	<div class="empty-block">
		<h4>Заявок нет</h4>
		<p>В нашей Базе данных нет заявок арендодателей, удовлетворяющих Вашим объектам</p>
	</div>
<?php }?>
	</div><!-- /cabBlock -->
	</div><!-- /THIRD tab -->
	
	</div><!-- /cabTabs -->
</div><!-- /cabinet -->


<div id="owner-change">
	<div class="header"></div>
	<div class="message">
		<input type="radio" name="stat" value="1" alt="не предоставляется" id="rb0" /><label for="rb0">отменить показы</label><br />
		<input type="radio" name="stat" value="6" alt="идут показы" id="rb3" /><label for="rb3">возобновить показы</label>
	</div>
	<div class="buttons">
		<div class="yes">Принять</div><div class="no close">Отмена</div>
	</div>
</div>
<div id="owner-delete">
	<div class="header"></div>
	<div class="message"></div>
	<div class="buttons">
		<div class="yes">Да</div><div class="no close">Нет</div>
	</div>
</div>
<script type="text/javascript" src="js/jquery.simplemodal.js"></script>
<script type="text/javascript" src="js/owner.js"></script>
