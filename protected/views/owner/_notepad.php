<?php 
$borderColor='clrRed';
switch ($row->status) {
	case 0: {$stat = 'заблокирован администратором'; break;}
	case 1: {$stat = 'показы отменены'; break;}
	//case 2: {$stat = 'сдан'; break;}
	case 3: {$stat = 'ожидает модерации'; $borderColor='clrGray'; break;}
	//case 5: {$stat = 'показы приостановлены'; break;}
	case 6: {$stat = 'идут показы'; $borderColor='clrGreen'; break;}
	default: {$stat = 'Не известен'; }
}
?>
		<div id="id-<?php echo $row->id; ?>" class="infoBlock <?php echo $borderColor ?>">
			<div class="rightCol">
				<span class="date"><?php echo $day; ?> <?php echo $month; ?> <?php echo $year; ?></span><br /><i>дата подачи</i>
				<p><?php echo $row->showQty; ?></p><i>показов</i>
			</div>
			<div class="leftCol">
				<?php echo CHtml::link($row->name, array('objects/detail', 'id'=>$row->id), array('target'=>'_blank', 'class'=>'toObj titl')); ?><br /> 
				<b>Адрес:</b> 
<?php if (isset($row->city)) echo ('г. '.$row->city); ?> ул.<?php echo $row->street.';';
if (isset($row->house)) echo (' дом '.$row->house);
if (isset($row->housing)) echo (' корп. '.$row->housing);
if (isset($row->storey)) echo (' Этаж - '.$row->storey);
?>
				<br /><b>Площадь:</b> <?php echo $row->total_item_sqr; ?> м<sup>2</sup> <b>Цена:</b> <?php echo $row->price; ?> р.
				<br /><br />
				<div class="statusBlock">
					<span class="<?php echo $borderColor ?>">Статус объекта:</span><br /><b class="<?php echo $borderColor ?>"><?php echo $stat; ?></b>
					<div class="actions">
						<?php echo CHtml::link('Редактор', array('object/edit', 'id'=>$row->id), array('class'=>'more')); ?>
						<?php echo CHtml::link('Изменить', array('owner/change', 'id'=>$row->id), array('class'=>'change more')); ?>
						<?php echo CHtml::link('Удалить', array('owner/delete', 'id'=>$row->id), array('class'=>'delete del')); ?>
					</div>
				</div>
			</div>
		</div>
