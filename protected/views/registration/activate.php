<div class="story">
<?php if (isset($happy)):?>
	<?php if ($happy==0){?>
	<h2>Активация не возможна</h2>
	<p>Возможно ссылка устарела.<br />Обратитесь в <a href="mailto:<?php echo Yii::app()->params->admin_mail;?>">службу тех.поддержки</a>, указав Ваш Логин и e_mail.</p>
	<?php } else {?>
	<p>Ваш аккаунт успешно активирован.<br /><?php echo CHtml::link('Авторизовавшись', array('authorization/login'), array('title'=>'Страница авторизации'));?>, Вы можете приступить к работе на нашем портале.</p>
	<?php } ?>
<?php elseif(isset($reg)):?>
	<p>На указанный Вами адрес электронной почты отправлено письмо.<br />Для активации созданной учетной записи, перейдите по ссылке, указанной в письме.</p>
<?php elseif(isset($res)):?>
	<?php if ($res==1){?>
	<p>На указанный Вами адрес электронной почты отправлено письмо.<br />Вам следует вновь активировать Вашу учетную запись, перейдя по ссылке, указанной в письме.</p>
	<?php } else {?>
	<p>Вы трижды не смогли верно указать свой e_mail.<br />Система защиты от подбора предлагает Вам вернуться к попыткам некоторое время спустя.</p>
	<?php } ?>
<?php endif;?>
</div>