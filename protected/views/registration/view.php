<?php 
switch($form->role) {
	case 'tenant' : {$s = '0'; break;}
	case 'owner' : {$s = '1'; break;}
	default: $s = '1';
}
?>
<script type="text/javascript">
$(document).ready(function() {
	$('label.switch01_lbl').children('div').removeClass('fswitch01_ce').addClass('fswitch01_ue');
	<?php /*$("#la<?php echo $s;?>").children('div').removeClass('fswitch01_ue').addClass('fswitch01_ce'); */?>
});
</script>

<h1 class="h101 m_0043t">Регистрация</h1>
<div class="formREG_wrp">
	<?php echo CHtml::form('', 'post', array('class'=>'m0 m20t')); $Errs = $form->errors;?>
		<div class="form01_grp">
			<div class="form02_blk">
				<table class="table02 m30b">
					<thead>
						<tr><th>Функционал</th><th>Арендатору</th><th>Собственнику</th><th>Профессионалам</th></tr>
					</thead>
					<tbody>
						<tr><td>Центр недвижимости - много помещений в одном</td>
							<td><div class="bX"></div></td><td><div class="bV"></div></td><td><div class="bV"></div></td>
						</tr>
						<tr><td>SMS и E-mail уведомления о поступивших объектах</td>
							<td><div class="bV"></div></td><td><div class="bV"></div></td><td><div class="bV"></div></td>
						</tr>
						<tr><td>Размещение объектов</td>
							<td><div class="bX"></div></td><td><div class="bV"></div></td><td><div class="bV"></div></td>
						</tr>
						<tr><td>Блокнот для хранения отмеченных объектов</td>
							<td><div class="bV"></div></td><td><div class="bX"></div></td><td><div class="bV"></div></td>
						</tr>
						<tr><td>Поиск помещений</td>
							<td><div class="bV"></div></td><td><div class="bV"></div></td><td><div class="bV"></div></td>
						</tr>
						<tr><td>Статистика и аналитика</td>
							<td><div class="bX"></div></td><td><div class="bX"></div></td><td><div class="bV"></div></td>
						</tr>
						<tr><td>Персональная страница</td>
							<td><div class="bX"></div></td><td><div class="bX"></div></td><td><div class="bV"></div></td>
						</tr>
					</tbody>
				</table>
				<div class="m10b">
					<h2 class="dl m5r w100">Выберите статус:</h2>
					<label id="la0" class="switch01_lbl vt"><div class="fswitch01_ce"></div>Арендатор</label>
					<label id="la1" class="switch01_lbl vt"><div class="fswitch01_ue"></div>Собственник</label>
					<label id="la2" class="switch01_lbl c999 vt"><div class="fswitch01_ud"></div>Профессионал (<span class="cC71D1D">скоро!</span>)</label>
					<?php echo CHtml::activeHiddenField($form, 'role');?>
				</div>
				<div class="m15b" id="container">
					<h1 class="m10b">Регистрационные данные:</h1>
					<div class="m15b">
						<div class="input02_lbl w48_9p w340n w365m">Логин
							<div class="dl lr">
<?php if (isset($Errs['username'][0])) {$d='e';$lb='er';$s=$Errs['username'][0];} else {$d='b';$lb='te';$s='Ваш логин должен быть не менее 5 символов';}?>
								<div class="input02_r<?php echo $d;?>0"></div><div class="input02_r<?php echo $d;?>1"><?php echo CHtml::activeTextField($form, 'username', array('maxlength'=>10, 'tabindex'=>11, 'class'=>'input02 lr w210'));?></div><div class="input02_r<?php echo $d;?>2"></div><b class="i">*</b>
							</div>
							<div id="llogin" class="input02_n<?php echo $lb;?>"><?php echo $s;?></div>
						</div><div class="dl w7_15p"></div><div class="input02_lbl w43_9p w335m w305n">Телефон
							<div class="dl lr">
<?php if (isset($Errs['phone'][0])) {$d='e';$lb='er';$s=$Errs['phone'][0];} else {$d='b';$lb='te';$s='Вводите номер телефона с кодом города (только цифры)';}?>
								<div class="input02_r<?php echo $d;?>0"></div><div class="input02_r<?php echo $d;?>1"><?php echo CHtml::activeTextField($form, 'phone', array('maxlength'=>14, 'tabindex'=>12, 'class'=>'input02 lr w210'));?></div><div class="input02_r<?php echo $d;?>2"></div><b class="i">*</b>
							</div>
							<div id="lphone" class="input02_n<?php echo $lb;?>"><?php echo $s;?></div>
						</div>
					</div>
					<div class="m15b">
						<div class="input02_lbl w48_9p w340n w365m">Пароль
							<div class="dl lr">
<?php if (isset($Errs['password'][0])) {$d='e';$lb='er';$s=$Errs['password'][0];} else {$d='b';$lb='te';$s='Ваш пароль должен быть не менее 5 символов';}?>
								<div class="input02_r<?php echo $d;?>0"></div><div class="input02_r<?php echo $d;?>1"><?php echo CHtml::activePasswordField($form, 'password', array('maxlength'=>10, 'tabindex'=>11, 'class'=>'passinp input02 lr w210'));?></div><div class="input02_r<?php echo $d;?>2"></div><b class="i">*</b>
							</div>
							<div id="lpass" class="input02_n<?php echo $lb;?>"><?php echo $s;?></div>
						</div><div class="dl w7_15p"></div><div class="input02_lbl w43_9p w335m w305n">Ваша почта
							<div class="dl lr">
<?php if (isset($Errs['e_mail'][0])) {$d='e';$lb='er';$s=$Errs['e_mail'][0];} else {$d='b';$lb='te';$s='Кириллические почтовые ящики не поддерживаются';}?>
								<div class="input02_r<?php echo $d;?>0"></div><div class="input02_r<?php echo $d;?>1"><?php echo CHtml::activeTextField($form, 'e_mail', array('maxlength'=>30, 'tabindex'=>12, 'class'=>'input02 lr w210'));?></div><div class="input02_r<?php echo $d;?>2"></div><b class="i">*</b>
							</div>
							<div id="lmail" class="input02_n<?php echo $lb;?>"><?php echo $s;?></div>
						</div>
					</div>
					<div class="m15b">
						<div class="input02_lbl w48_9p w340n w365m">Повторите пароль
							<div class="dl lr">
<?php if (isset($Errs['confirm'][0])) {$d='e';$lb='er';$s=$Errs['confirm'][0];} else {$d='b';$lb='te';$s='Введите пароль еще раз';}?>
								<div class="input02_r<?php echo $d;?>0"></div><div class="input02_r<?php echo $d;?>1"><?php echo CHtml::activePasswordField($form, 'confirm', array('maxlength'=>10, 'tabindex'=>11, 'class'=>'passinp input02 lr w210'));?></div><div class="input02_r<?php echo $d;?>2"></div><b class="i">*</b>
							</div>
							<div id="lconf" class="input02_n<?php echo $lb;?>"><?php echo $s;?></div>
						</div>
					</div>
					<div class="m15b">
						<div class="input02_lbl w48_9p w340n w365m">Ваше имя
							<div class="dl lr">
<?php if (isset($Errs['name'][0])) {$d='e';$lb='er';$s=$Errs['name'][0];} else {$d='b';$lb='te';$s='Введите текст кириллицей (русские буквы)';}?>
								<div class="input02_r<?php echo $d;?>0"></div><div class="input02_r<?php echo $d;?>1"><?php echo CHtml::activeTextField($form, 'name', array('maxlength'=>30, 'tabindex'=>11, 'class'=>'input02 lr w210'));?></div><div class="input02_r<?php echo $d;?>2"></div><b class="i">*</b>
							</div>
							<div id="lname" class="input02_n<?php echo $lb;?>"><?php echo $s;?></div>
						</div>
					</div>
					<div class="m15b">
						<div class="input02_lbl w48_9p w340n w365m">Ваша фамилия
							<div class="dl lr">
<?php if (isset($Errs['so_name'][0])) {$d='e';$lb='er';$s=$Errs['so_name'][0];} else {$d='b';$lb='te';$s='Введите текст кириллицей';}?>
								<div class="input02_r<?php echo $d;?>0"></div><div class="input02_r<?php echo $d;?>1"><?php echo CHtml::activeTextField($form, 'so_name', array('maxlength'=>30, 'tabindex'=>11, 'class'=>'input02 lr w210'));?></div><div class="input02_r<?php echo $d;?>2"></div><b class="i">*</b>
							</div>
							<div id="lsoname" class="input02_n<?php echo $lb;?>"><?php echo $s;?></div>
						</div>
					</div>
					<div class="m15b">
						<div class="input02_lbl w48_9p w340n w365m">Ваше отчество
							<div class="dl lr">
<?php if (isset($Errs['otchestvo'][0])) {$d='e';$lb='er';$s=$Errs['otchestvo'][0];} else {$d='b';$lb='te';$s='Введите текст кириллицей';}?>
								<div class="input02_r<?php echo $d;?>0"></div><div class="input02_r<?php echo $d;?>1"><?php echo CHtml::activeTextField($form, 'otchestvo', array('maxlength'=>30, 'tabindex'=>11, 'class'=>'input02 lr w210'));?></div><div class="input02_r<?php echo $d;?>2"></div><b class="i">&nbsp;</b>
							</div>
							<div id="lotch" class="input02_n<?php echo $lb;?>"><?php echo $s;?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="form01_grp">
			<div class="bFFF form01_blk m0r p0l">
				<div class="m15b">
					<h2 class="captcha01_hdr w240">Введите результат вычисления</h2>
					<div class="captcha01">
						<?php $this->widget('CCaptcha', array('buttonLabel' => ''));?><a href="#" id="yt0"></a>
					</div>
					<div class="captcha01_vrf">
<?php if (isset($Errs['verifyCode'][0])) {$d='e';$lb='er';} else {$d='b';$lb='te';}?>
						<div class="captcha01_txt"><div class="input02_r<?php echo $d;?>0"></div><div class="input02_r<?php echo $d;?>1"><?php echo CHtml::activeTextField($form, 'verifyCode', array('maxlength'=>5, 'class'=>'input02 w60', 'tabindex'=>19));?></div><div class="input02_r<?php echo $d;?>2"></div>
						</div>
						<div><a class="captcha01_rfs" id="yt0" href="#" tabindex="20">обновить</a></div>
					</div>
				</div>
				<div>
					<label class="check01_lbl"><div class="fcheck01_ue"></div><?php echo CHtml::checkBox('Registration[rules]', false, array('class'=>'check01 dn', 'tabindex'=>21));?>Я прочитал и полностью согласен с <a href="/site/page/rules.html" target="_blank">правилами сайта</a></label>
					<?php echo CHtml::submitButton('Регистрация', array('id'=>'submit', 'class'=>'button01', 'name'=>'yt1', 'tabindex'=>22));?>
				</div>
			</div>
		</div>
	<?php echo CHtml::endForm();?>
</div>
