			<div class="content m_0000r">
				<div class="p0033b tc vt"><div class="m_0050t header1HWW vt">Как работает Ком-Аренда</div><span class="c999 t18 vt">Размещайте объекты, управляйте ими и будьте в курсе об их состоянии</span></div><div class="tableHWW">
					<div class="tableHWW_r01">
						<a class="tableHWW_c00" href="/how_work.html#unlimited">
							<div class="iconHWW _01"></div>Без ограничений
						</a><a class="tableHWW_c01" href="how_work.html#easy">
							<div class="iconHWW _02"></div>Просто
						</a><a class="tableHWW_c01" href="how_work.html#quickly">
							<div class="iconHWW _03"></div>Оперативно
						</a><a class="tableHWW_c01" href="how_work.html#profile">
							<div class="iconHWW _04"></div>Личный кабинет
						</a><a class="tableHWW_c01" href="how_work.html#news">
							<div class="iconHWW _05"></div>Новости
						</a>
					</div><div class="tableHWW_r02">
						<a class="tableHWW_c00" href="how_work.html#details">
							<div class="iconHWW _06"></div>Детали
						</a><a class="tableHWW_c01" href="how_work.html#conveniently">
							<div class="iconHWW _07"></div>Удобно
						</a><a class="tableHWW_c01" href="how_work.html#keep_up">
							<div class="iconHWW _08"></div>Будьте в курсе
						</a><a class="tableHWW_c01" href="how_work.html#notepad">
							<div class="iconHWW _09"></div>Блокнот
						</a><span class="tableHWW_c02">
							<div class="iconHWW"></div>&nbsp;
						</span>
					</div>
				</div><div class="m_autoh w925">
					<h2 class="header2HWW p0034t r0" id="unlimited"><div class="dl iconHWW _01 m_0010r"></div>Без ограничений. Теперь не надо регистрироваться.</h2>
					<p class="paragraphHWW">
						Данная страница предназначена для быстрого размещения ваших объектов и не требует регистрации на сайте. После размещения модератор сайта свяжется с вами и уточнит необходимые детали. Напоминаем, что регистрация позволит вам пользоваться всем функционалом сайта.
					</p><img class="imgHWW" src="images/how_we_work_img01.jpg"/>
					<h2 class="header2HWW" id="easy"><div class="dl iconHWW _02 m_0010r"></div>Просто. Максимальный объем параметров.</h2>
					<p class="paragraphHWW">
						Если вы являетесь собственником бизнес-центра или другого объекта, содержащего много помещений, то вам не придется постоянно создавать однотипные объявления и многократно указывать повторяющиеся параметры, такие как город, улица, район и т.д. Создав объект-контейнер (например, деловой центр) и один раз указав основные характеристики, вы будете просто добавлять в него помещения, указывая минимальный набор параметров. 
					</p><img class="imgHWW" src="images/how_we_work_img02.jpg"/>
					<h2 class="header2HWW" id="quickly"><div class="dl iconHWW _03 m_0010r"></div>Оперативно. Бесплатные SMS и e-mail.</h2>
					<p class="paragraphHWW">
						SMS и е-mail уведомления с портала абсолютно бесплатны и позволяют вам оперативно реагировать на ситуацию. Если арендатор оставит заявку на помещение, подобное вашему &ndash; вы узнаете об этом.
					</p><img class="imgHWW" src="images/how_we_work_img03.jpg"/>
					<h2 class="header2HWW" id="profile"><div class="dl iconHWW _04 m_0010r"></div>Личный кабинет. Управляйте объектами.</h2>
					<p class="paragraphHWW">
						Позволяет хранить неограниченное количество объектов. Вы можете управлять объектами, включая и выключая показы, а также смотреть статистику по объектам.
					</p><img class="imgHWW" src="images/how_we_work_img04.jpg"/>
					<h2 class="header2HWW" id="news"><div class="dl iconHWW _05 m_0010r"></div>Новости. Размещение вашей информации.</h2>
					<p class="paragraphHWW">
						Мы размещаем новости о деятельности наших партнеров. Если у вас есть информационный повод &ndash; закончившаяся или начавшаяся реконструкция, снижение цены, ввод новых площадей в эксплуатацию и т.д. &ndash; мы будем оперативно и бесплатно размещать ваши новости в специальном раздле &laquo;новости партнеров&raquo; у нас на портале.
					</p><img class="imgHWW" src="images/how_we_work_img05.jpg"/>
					<h2 class="header2HWW" id="details"><div class="dl iconHWW _06 m_0010r"></div>Детали. Облегченный поиск объектов.</h2>
					<p class="paragraphHWW">
						Мы упрощаем ваш поиск &ndash; каждое объвление описано максимально подробно, включая такие опции, как покрытие пола, пандусы и подъездные пути. Воспользовавшись фильтром, вы сможете сузить рамки поиска и быстрее найти нужный вам объект.
					</p><img class="imgHWW" src="images/how_we_work_img06.jpg"/>
					<h2 class="header2HWW" id="conveniently"><div class="dl iconHWW _07 m_0010r"></div>Удобно. Информация в удобном для вас формате.</h2>
					<p class="paragraphHWW">
						Вы можете скачивать и распечатывать данные по объектам в виде PDF-файла со всеми фотографиями и описаниями. Создавайте свой альбом и отсылайте на печать все фото вместе с информацией.
					</p><img class="imgHWW" src="images/how_we_work_img07.jpg"/>
					<h2 class="header2HWW" id="keep_up"><div class="dl iconHWW _08 m_0010r"></div>Будьте в курсе. Уведомления о подходящем вам объекте.</h2>
					<p class="paragraphHWW">
						SMS и e-mail уведомления с портала абсолютно бесплатны и позволяют вам оперативно реагировать на ситуацию. Сохраните критерии поиска в виде:
					</p><img class="imgHWW" src="images/how_we_work_img08.jpg"/>
					<h2 class="header2HWW" id="notepad"><div class="dl iconHWW _09 m_0010r"></div>Блокнот. Запишите и следите за статусом.</h2>
					<p class="paragraphHWW">
						Копируйте выбранные объекты в блокнот и будьте в курсе статуса объекта. Если помещение перестает быть актуальным, вы узнаете об этом через SMS.
					</p><img class="imgHWW" src="images/how_we_work_img09.jpg"/>
				</div>
			</div>
