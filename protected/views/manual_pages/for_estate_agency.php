<div class="content">
    <h1 class="header12 M60T">Мы ищем партнеров</h1>
    <p class="desc2">Среди агентств недвижимости для взаимовыгодного сотрудничества во всех крупных и средних городах России (Москва, Санкт-Петербург, Екатеринбург, Новосибирск и т.д.)</p>
    <div class="partners_landing_grp">
        <div class="partners_landing_img1"></div>
        <div class="M125L">
            <h3 class="header32">Размещение ваших объектов на главной странице сайта</h3>
            <p class="F12">Объекты, размещенные вашими партнерами, автоматически попадают на главную страницу в &laquo;ленту&raquo;.</p>
        </div>
    </div>
    <div class="partners_landing_grp">
        <div class="partners_landing_img2"></div>
        <div class="M125L">
            <h3 class="header32">Получение заявок от арендаторов</h3>
            <p class="F12">Заявки на подбор помещени о арендаторов в вашем городе отправляются прямо к вам. Баннерная реклама компаний-партнеров и их объектов. Специальный модуль может рекламировать ваши объекты на главной странице и в результатах поиска. Также мы размещаем медийные баннеры стандартных размеров на портале.</p>
        </div>
    </div>
    <div class="partners_landing_grp">
        <div class="partners_landing_img3"></div>
        <div class="M125L">
            <h3 class="header32">Ускоренная модерация</h3>
            <p class="F12">Мы доверяем своим партнерам! Объявления, поступившие от наших партнеров, сразу появляются на сайте.</p>
        </div>
    </div>
    <div class="partners_landing_grp">
        <div class="partners_landing_img4"></div>
        <div class="M125L">
            <h3 class="header32">Размещение новостей в рубрике &laquo;новости партнеров&raquo;</h3>
            <p class="F12">Мы размещаем новости о деятельности наших партнеров. Если у вас есть информационный повод, мы будем оперативно и бесплатно размещать ваши новости в специальном разделе &laquo;новости партнеров&raquo; у нас на портале.</p>
        </div>
    </div>
    <div class="partners_landing_grp">
        <div class="partners_landing_img5"></div>
        <div class="M125L">
            <h3 class="header32">Персональные страницы &laquo;клуб профессионалов&raquo;</h3>
            <p class="F12">Персональные страницы команий-партнеров с фотографиями сотрудников, рекламными текстами, ссылками на официальные сайты и выводом всех объектов компании.</p>
        </div>
    </div>
    <div class="partners_landing_grp">
        <div class="partners_landing_img6"></div>
        <div class="M125L">
            <h3 class="header32">Квалифицированную и бесплатную поддержку нашим специалистом</h3>
            <p class="F12">Если у вас большое количество объектов, то мы примем информацию в любом электронном виде и разместим их самостоятельно.</p>
        </div>
    </div>
    <div class="M5V">
        <span class="spoiler1_btn">С чего начать?</span>
    </div>

	<script>
		$(function(){
            $('#Spoiler1').hide();
			$('.spoiler1_btn').click(function(){
			  	if($('#Spoiler1').is(':hidden')){
                     $(this).text('Cкрыть');
					 $('#Spoiler1').show();
				}
				else{
                    $(this).text('С чего начать?');
                    $('#Spoiler1').hide();
				}
			});
		});
	</script>

    <ol class="spoiler1" id="Spoiler1">
        <li><a href="/registration.html">Зарегистрируйтесь</a> на сайте.</li>
        <li>Начинайте выкладывать все свои объекты, чем больше тем лучше.</li>
        <li>Вы можете разместить у нас флеш-баннер размерами 1000*90 или 240*400.</li>
        <li>Перешлите нам свой логотип для раздела Партнеры.</li>
        <li>Назначьте ответственного сотрудника для связи с нами.</li>
    </ol>

    <div class="M25T">
	    <?php $form=$this->beginWidget('CActiveForm', array(
	    'id'=>'Partnership-form',
	    'action'=>'Ajaxadmins/sendPartersOffers',
	    'enableAjaxValidation'=>true,
	    'clientOptions' => array(
		    'validateOnSubmit' => true, //При отправке
		    'validateOnChange' => true,  //При изменении
	      ),
        ));
	    ?>
	    <?php echo $form->errorSummary($model,'Пожалуйста, исправьте следующие ошибки'); ?>
        <div class="form01_grp">
            <div class="form3_blk">
                <div class="form3_set">
                    <h2 class="header22">Хочу быть партнером</h2>
                    <p class="M25G">Заполните и отправьте форму, чтобы узнать подробнее.</p>
                    <div class="input02_lbl W505">

	                    <?php echo $form->labelEx($model, 'name');?>
                        <div class="input_inner LR">
		                    <div class="input02_re1">
	                            <div class="corner input02_re0"></div>
	                            <div class="corner input02_re2"></div>
		                        <?php echo $form->textField($model,'name', array('class'=>'input02 LR W355')); ?>
	                        </div>
                        </div>
                        <div class="input02_ner W360"><?php echo $form->error($model, 'name'); ?></div>
                    </div><br/>

                    <div class="input02_lbl W505">
	                    <?php echo $form->labelEx($model, 'telephone');?>
                        <div class="LR">
                            <div class="input02_re1">
                                <div class="corner input02_re0"></div>
                                <div class="corner input02_re2"></div>
		                        <?php echo $form->textField($model,'telephone', array('class'=>'input02 LR W355')); ?>
                            </div>
                        </div>
                        <div class="input02_ner W360"><?php echo $form->error($model, 'telephone');?></div>
                    </div><br/>
                    <div class="input02_lbl W505">
	                    <?php echo $form->labelEx($model, 'email');?>
                        <div class="LR">
                            <div class="input02_re1">
                                <div class="corner input02_re0"></div>
                                <div class="corner input02_re2"></div>
			                    <?php echo $form->textField($model,'email', array('class'=>'input02 LR W355')); ?>
                            </div>
                        </div>
                        <div class="input02_ner W360"><?php echo $form->error($model, 'email');?></div>
                    </div><br/>
                    <div class="input02_lbl M5T W503">
	                    <?php echo $form->labelEx($model, 'text');?>
	                    <?php echo $form->textArea($model,'text', array('class'=>'textarea01 E90 LR W350')); ?>
                        <div class="input02_ner W360"><?php echo $form->error($model, 'text');?></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="M15G">
	        <?php echo CHtml::submitButton('Отправить', array('class'=>'button01'));  ?>
        </div>

	 <?php $this->endWidget(); ?>
	</div>
</div><!-- /content -->