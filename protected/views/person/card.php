<?php $bUrl = Yii::app()->baseUrl; $usName = $user->so_name.' '.$user->name.' '.$user->otchestvo;?>
<div id="mwindow">
	<div class="header"></div>
	<div class="message">
		<textarea name="disallow"></textarea>
	</div>
	<div class="buttons">
		<div class="yes">Отправить</div><div class="no close">Отмена</div>
	</div>
</div>
	<div class="clientNav">
		<span class="clientNavTitle"><?php echo $usName;?></span>
		<span class="clientNavIn"><span>Обо мне</span><?php if ($user->role=='owner') echo CHtml::link('Объекты', array('person/objects', 'id'=>$user->id)); ?>
		</span>
	</div>
	<div class="content_wrap">
<?php @require_once('protected/views/layouts/rightColumn02.php');?>
		<div class="content">
		<div class="about">
			<div class="about_definition">
<?php if(empty($user->photo)):?>
				<img src="/images/client_sob_nophoto.jpg" />
<?php else:?>
				<img src="/storage/images/personal/<?php echo $user->photo;?>" alt="<?php echo $usName;?>" />
<?php endif;?>
				<div class="about_definition_contacts about_definition_contacts_nophoto">
					<?php echo $usName;?><br/>
<?php if(!empty($user->city)):?>
					г. <?php echo $user->city;?><br />
<?php endif;?>
					тел. <?php echo $user->phone;?><br />
<?php if(!empty($user->icq)):?>
					ICQ <?php echo $user->icq;?><br />
<?php endif;?>
<?php if(!empty($user->skype)):?>
					Skype: <?php echo $user->skype;?><br />
<?php endif;?>
					<br /><span class="about_definition_pm about_definition_pm_nophoto mw-button" id="<?php echo $user->id;?>">написать письмо</span>
				</div>
			</div>
		</div><!-- /about -->
		</div><!-- /content -->
	</div><!-- /content_wrap -->

