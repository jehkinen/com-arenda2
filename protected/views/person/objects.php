<?php $bUrl = Yii::app()->baseUrl;
	echo CHtml::cssFile(Yii::app()->baseUrl.'/css/modal.css'); 
	if ($userTenant) echo CHtml::scriptFile($bUrl.'/js/jquery.simplemodal.js');
?>
<script type="text/javascript">
	$(document).ready(function(){

<?php if ($userTenant):?>
		$('span.notAtNote').hide();
		$('a.tonotepad').bind('click', function(e){
			<?php /* var Parent_obj = $(this).parents('td'); */ ?>
			var id_obj = $(e.target).attr('id');
			var id = id_obj.slice(3);
			$('div#row'+id).hide();
			e.preventDefault();
			show_modal('#owner-delete', 'ods', '<span>Подтверждение</span>', 'Поместить объект в блокнот?', '30%', function(){
				$.ajax({
					url: "/ajax/notepad_add",
					data: {id: id_obj},
					type: 'post',
					success: function(){
						$(e.target).hide(); 
						$('span#span'+id).show();
					}
				});
			});
		});
		$('a.claim').bind('click', function(e){
			var obj_id = $(e.target).attr('id');
			var id = obj_id.slice(3);
			$('div#row'+id).hide();
			$('div.lb_overlay').hide();
			e.preventDefault();
			show_modal('#mwindow', 'ocs', '<span>Жалоба</span>', '', '20%', function(){
				var selOption = $('option:selected');
				var claim = selOption.val();
				if (typeof(claim) != 'undefined'){
					$.ajax({
						url: "/ajax/send_claim",
						data: {id: obj_id, clm: claim},
						type: 'post',
						success: function(data){}
					});
				}
			});
		});
		$('a#orderAdd').bind('click', function(e){
			e.preventDefault();
			$.ajax({
				url: "/ajax/order_add",
				data: {},
				dataType: 'html',
				type: 'post',
				success: function(data){
					$('div#info-window-message').html(data);
				}
			});
			show_modal_info('#info-window', 'ims', '<span>Информация</span>', '30%');
		});
<?php endif;?>
	});
<?php if ($userTenant):?>
function show_modal(box, container, header, message, top, callback){
	$(box).modal({
		closeHTML: '<a href="#" title="Закрыть" class="x-close"><img src="/images/modal/mw-close.jpg" /></a>',
		position: [top,],
		opacity: 30,
		overlayId: 'overlay',
		containerId: container,
		closeClass: 'close',
		onShow: function (dialog){
			var modal = this;

			$('.header', dialog.data[0]).append(header);
			$('.message', dialog.data[0]).append(message);

			$('.yes', dialog.data[0]).click(function () {
				if ($.isFunction(callback)) {
					callback.apply();
				}
				modal.close();
			});
		}
	});
}
function show_modal_info(box, container, header, top){
	$(box).modal({
		closeHTML: '<a href="#" title="Закрыть" class="x-close"><img src="/images/modal/mw-close.jpg" /></a>',
		position: [top,],
		opacity: 30,
		overlayId: 'overlay',
		containerId: container,
		closeClass: 'close',
		onShow: function (dialog){
			$('.header', dialog.data[0]).append(header);
		}
	});
}
<?php endif;?>
</script>
<?php if ($userTenant):?>
<div id="owner-delete">
	<div class="header"></div>
	<div class="message"></div>
	<div class="buttons">
		<div class="yes">Да</div><div class="no close">Нет</div>
	</div>
</div>
<div id="info-window">
	<div class="header"></div>
	<div class="message" id="info-window-message"></div>
	<div class="buttons">
		<div class="yes close">Понятно</div>
	</div>
</div>
<div id="mwindow">
	<div class="header"></div>
	<div class="message">
		Мной обнаружено:<br />
		<select name="claim">
			<option value="1" selected="selected">Неверная информация об объекте</option>
			<option value="2">Объявление неактульно (сдано)</option>
			<option value="3">Неадекватное поведение</option>
		</select>
	</div>
	<div class="buttons">
		<div class="yes">Подать</div><div class="no close">Отмена</div>
	</div>
</div>
<?php endif;?>




	<div class="clientNav">
		<span class="clientNavTitle"><?php echo $user->so_name.' '.$user->name.' '.$user->otchestvo;?></span>
		<span class="clientNavIn"><?php echo CHtml::link('Обо мне', array('person/card', 'id'=>$user->id));?><span>Объекты</span>
		</span>
	</div>
	<div class="content_wrap">
<?php @require_once('protected/views/layouts/rightColumn02.php');?>
		<div class="content">
			<div class="dealNav">
				<span>Аренда</span><a href="">Продажа</a>
			</div>
			<div class="object_table" id="search_result">
<?php if (empty($items)):?>
<div class="story">
	<h2>Объектов нет</h2>
</div>
<?php else:?>
<?php $i=1; foreach ($items as $object){
		$b = ++$i % 2;
		$this->renderPartial('_object', array(
			'obj' =>$object,
			'odd' =>$b,
			'isUserTenant' =>$userTenant
		));
	} ?>
<?php endif;?>
			</div><!-- / object_table -->
<?php $this->widget('CLinkComarendaPager',array(
		'pages'				=>$pages,
		'maxButtonCount'	=>5, # максимальное колличество вкладок на странице
		'header'			=>'', # заголовок
		'nextPageLabel'		=>'',
		'prevPageLabel'		=>'',
		'firstPageLabel'	=>'',
		'lastPageLabel'		=>''
	));
?>
		</div><!-- /content -->
	</div><!-- /content_wrap -->

