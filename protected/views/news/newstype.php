<div class="t21">Блог проекта</div>
<?php foreach($rows as $i):?>
<div class="c999 m_0015t t10"><?php $get = $this->widget('DateFormat', array('kind'=>1,'dbDate'=>$i->created)); echo $get->date;?></div>
<div class="t16"><?php echo $i->header;?></div>
<div class="m_0015b m_0005t t12"><?php 
	$b = strpos($i->body, '<br');
	if ($b===false)	$b = 999999;
	$p = strpos($i->body, '</p>');
	if (($p===false)||($p>$b))
		$p = $b;
	$i->body = substr($i->body, 0, $p);
	if (isset($i->small_pic)) 
		echo CHtml::image(Yii::app()->baseUrl.'/storage/images/news/'.$i->small_pic, '');
	echo($i->body.' '.CHtml::link('Читать&nbsp;</nobr>дальше', array('news/view', 'id'=>$i->id), array('class'=>'more')).'</p>');
?></div>	
<?php endforeach;?>
<?php $this->widget('CLinkComarendaPager',array(
		'pages'				=>$pages,
		'maxButtonCount'	=>1, # максимальное колличество вкладок на странице
		'header'			=>'', # заголовок
		'nextPageLabel'		=>'&rarr;',
		'prevPageLabel'		=>'&larr;',
		'firstPageLabel'	=>'',
		'lastPageLabel'		=>''
	));
?>
