<link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/vader/jquery-ui.css" rel="stylesheet" />
<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/jcrop/jquery.Jcrop.min.css'); ?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo $this->createAbsoluteUrl('/js/ajaxupload.3.6.js'); ?>"></script>
<script type="text/javascript" src="<?php echo $this->createAbsoluteUrl('/js/jquery.Jcrop.min.js'); ?>"></script>


<script type="text/javascript">
	$(document).ready(function(){
		function MyCallAction (e) {
			e.preventDefault();
			alert(this.id + ' ' + e.data.text);
		}
		function RemoveBlock (e) {
			e.preventDefault();
			var lnk = $(this);
			var s = $(this).attr('id').substr(5);
			var fName = $('img#iitem'+s).attr('src');
			$.ajax({
				url: 'ajaxfoto/delete/',
				data: {	file: fName	},
				type: 'post',
				success: function(){
							$(lnk).parent().remove();
							counter--;
				}
			});
		}
		function showCoords(c){
			$('#x1').val(Math.round(c.x));
			$('#y1').val(Math.round(c.y));
			$('#x2').val(Math.round(c.x2));
			$('#y2').val(Math.round(c.y2));
		}
		function bindDownloader (img, container) {
			var bt = new AjaxUpload(img, {
						action: 'ajaxfoto/upload/',
						name: 'myfile',
						onSubmit : function(file, ext) {
							if (ext && /^(jpg|png|jpeg|gif|JPG|PNG|JPEG|GIF)$/.test(ext)) {
								this.disable();
								<?php /*info.removeClass().addClass('loading').text('Загрузка'); */?>
							} else {
								alert('Неверный тип картинки, допускается только: jpg, jpeg, png, gif');
								return false;
							}
						},
						onComplete: function(file, response) {
							var action = this;
							doClose = true;
							if (response != false){
								var data = response.split('|');
								var file_name = data[0];
								$('#load-here').append(
									jQuery('<img />')
										.attr({
											src: 'storage/images/personal/temp/' + file_name,
											id:  'crop-image',
										})
								);
								<?php /*info.removeClass().addClass('resize').text('Редактирование'); */?>
								$('#crop-box').dialog({
									modal: true,
									width: (parseInt(data[1])+34) + 'px',
									title: 'Редактирование картинки',
									close: function() {
											action.enable();
											$('#crop-box').hide();
											$('#load-here').empty();
											if (doClose) {
												<?php /*info.removeClass().addClass('cancel').text('Отменено пользователем'); */?>
												$.ajax({
													url: 'ajaxfoto/cancel/',
													data: {
														file: file_name,
														type: 'temp'
													},
													type: 'post'
												});
											}
									},
									resizable: false,
									position:['middle', 100]
								});
								$('#crop-image').Jcrop({
									setSelect: [10, 10, 318, 241],
									minSize: [308, 231], 
									aspectRatio: 4/3,
									allowSelect: false,
									bgColor: '#ffffff',
									bgOpacity: .5,
									onChange: showCoords,
									onSelect: showCoords
								});
								$('#save').click(function() {
										$.ajax({
											url: 'ajaxfoto/crop/',
											data: {
												file: file_name,
												x1: $('#x1').val(),
												y1: $('#y1').val(),
												x2: $('#x2').val(),
												y2: $('#y2').val()
											},
											type: 'post',
											success: function(){
													$('#divAdelPhoto').removeClass('hide');
													img.attr({
														src: 'storage/images/personal/' + file_name,
														alt: file_name,
														title: '<?php echo $title_with;?>'
													})
											}
										});
										doClose = false;
										<?php /*info.removeClass().addClass('chek').text('Загруженно: ' + file); */?>
										$('#crop-box').dialog("close"); 
								});
							}else{
								<?php /*info.removeClass().text(''); */?>
								alert('Размер картинки слишком большой, разрешенно не более 6Мб');
								action.enable();
							}
						}
					});
		}
		
		var cont = $('div#container');
		var counter = 0;
		$('a#alButton').bind('click', function(e){
			e.preventDefault();
			if (counter<3) {
				// Создание элементов
				var el = $('div#elem').clone();
				var idVal = "item" + counter;
				el.attr("id", idVal);
				el.css("display", "block");
				el.prependTo(cont);
				var lnk = $("#" + idVal + " > a:first");
				var img = $("#" + idVal + " > img:first");
				bindDownloader(img, idVal);
				lnk.attr({id:"aitem" + counter, href:"#"});
				lnk.bind('click', {}, RemoveBlock);
				img.attr({id:"iitem" + counter});
				
				<?php /*
				var lnkDel = lnk.next().next();
				lnkDel.html('Delete ' + counter);
				lnkDel.attr({id:"adelitem" + counter, href:"#"});
				lnkDel.bind('click', {}, RemoveBlock);
				*/?>
				counter++;
			}
		});
	});
</script>



<h1>Работа с фотографиями объекта <?php echo $item->header;?></h1><br /><br />
<a href="#" id="alButton">Добавить еще фотку</a><br />
<div id="container" style="border:1px solid green; margin: 10px; float:left"></div>
<div style="clear:left"></div>

	
<div style="border:1px solid red; padding:15px; margin:10px; float:left; display:none;" id="elem">
	<img src="images/add_photo.jpg" title="Нажмите, чтобы загрузить" /><br /><a href="/">Удалить фото</a>
</div>
		<!-- Hidden dialog -->
		<div style="display:none;" id="crop-box">
			<div id="dialog">
				<div id="load-here"></div>
				<button id="save" style="margin-top:10px;">Cохранить</button>
			</div>
		</div>
		<!-- /Hidden dialog -->
		<input type="hidden" id="x1" name="x1" />
		<input type="hidden" id="y1" name="y1" />
		<input type="hidden" id="x2" name="x2" />
		<input type="hidden" id="y2" name="y2" />
