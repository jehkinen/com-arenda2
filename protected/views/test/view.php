
<script type="text/javascript">
	$(document).ready(function(){
		function MyCallAction (e) {
			e.preventDefault();
			alert(this.id + ' ' + e.data.text);
		}
		function RemoveBlock (e) {
			e.preventDefault();
			$(this).parent().remove();
		}
		
		var cont = $('div#container');
		var counter = 0;
		$('a#alButton').bind('click', function(e){
			e.preventDefault();
			if (counter<3) {
				// Создание элементов
				var el = $('div#elem').clone();
				var idVal = "item" + counter;
				el.attr("id", idVal);
				el.css("display", "block");
				el.prependTo(cont);
				var lnk = $("#" + idVal + " > a:first");
				lnk.html('i was founded ' + counter);
				lnk.attr({id:"aitem" + counter, href:"#"});
				lnk.bind('click', {text:"string"}, MyCallAction);
				var lnkDel = lnk.next().next();
				lnkDel.html('Delete ' + counter);
				lnkDel.attr({id:"adelitem" + counter, href:"#"});
				lnkDel.bind('click', {}, RemoveBlock);
				
<?php /*				

				//var lnk = $("#" + idVal + " > a");
				//var lnk1 = lnk[0];
				//var lnk2 = lnk[1];



				var lnk = el.children()[0]; // $('div#item'+counter+' > a');
				lnk.innerHTML = 'i was founded ' + counter;
				lnk.id = "aitem" + counter;
				lnk.href = "#";
				el.prependTo(cont);
				var lnk2 = $("#aitem" + counter);
				lnk2.bind('click', {text:"string"}, MyCallAction);
				
*/?>				
				
				
				counter++;
			}
		});
		<?php if (!$atNotepad) { ?>$('span#spanInNotepad').hide();<?php } ?>
		$('a.tonotepad').bind('click', function(e){
			var id_obj = $(e.target).attr('id');
			e.preventDefault();
			show_modal('#owner-delete', 'ods', '<span>Блокнот</span>', 'Скопировать объект в блокнот?', '30%', function(){
				$.ajax({
					url: "/ajax/notepad_add",
					data: {id: id_obj},
					type: 'post',
					success: function(){
						$('span#spanInNotepad').show();
						$(e.target).parent().hide();
					}
				});
			});
		});
		$('a.claim').bind('click', function(e){
			var obj_id = $(e.target).attr('id');
			e.preventDefault();
			show_modal('#mwindow', 'ocs', '<span>Жалоба</span>', '', '20%', function(){
				var selOption = $('option:selected');
				var claim = selOption.val();
				if (typeof(claim) != 'undefined'){
					$.ajax({
						url: "/ajax/send_claim",
						data: {id: obj_id, clm: claim},
						type: 'post',
						success: function(data){}
					});
				}
			});
		});
	});
	

function show_modal(box, container, header, message, top, callback){
	$(box).modal({
		closeHTML: '<a href="#" title="Закрыть" class="x-close"><img src="/images/modal/mw-close.jpg" /></a>',
		position: [top,],
		opacity: 30,
		overlayId: 'overlay',
		containerId: container,
		closeClass: 'close',
		onShow: function (dialog){
			var modal = this;

			$('.header', dialog.data[0]).append(header);
			$('.message', dialog.data[0]).append(message);

			$('.yes', dialog.data[0]).click(function () {
				if ($.isFunction(callback)) {
					callback.apply();
				}
				modal.close();
			});
		}
	});
}
</script>



Тестовая вьюшка <br />
<a href="#" id="alButton">Push me</a><br />
<div id="container" style="border:1px solid green; margin: 10px; float:left"></div>
<div style="clear:left"></div>

	<h1><?php echo $item->header;?></h1>
	
<div style="border:1px solid red; padding:15px; margin:10px; float:left; display:none;" id="elem"><a href="/">Link number 1</a><br /><a href="/">Delete</a></div>
