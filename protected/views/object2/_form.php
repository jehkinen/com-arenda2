<?
/*Если у нас новая запись, то нам нужно получить id-шник будущей вставки: В частности это нужно для загрузчика фотограий*/
	if($model->isNewRecord){
		$connection=Yii::app()->db;
		$sql =  'SHOW TABLE STATUS LIKE "'.$model->tableName().'"';
		$command=$connection->createCommand($sql);
		$dataReader=$command->query();
		$rows=$dataReader->readAll();
		$model->id = $rows[0]['Auto_increment'];
	}
?>

<script type="text/javascript">
$(function(){
    var pref = 'Object2';
    var pref_ = '#'+pref+'_';
	var is_new_model = '<?echo (int) $model->isNewRecord;?>';

    init_form();

    /*если у нас в ajax вернулась 1 для этого чекбокса, то отмечаем его*/
    function set_param_for_checkbox(param , el){
        if (param==1) {
	        el.attr('checked' , true);
	        el.trigger('change');
	        el.parent().trigger('click');
        }
        else{
	        el.attr('checked' , false);
	        el.change();
	        el.parent().trigger('click');
        };
    }


  	function bind_select_city(bindIt){
		  var city_select =  $(pref_+'city');
		  if(bindIt){
              city_select.click(function(){
	              $('#Popup2Fog').show();
	              $('#Popup2').css('visibility', 'visible');
	              $('#Popup2').show();
	          });
          }else{
              city_select.unbind('click');
		  }
		  return false;
	  }


    function init_form() {

        $(pref_+'city').attr('readonly', 'true');

        bind_select_city(true);

	    /*Очистим эти поля
	    $(pref_+'total_item_sqr').val('');
	    $(pref_+'price').val('');
	    $(pref_+'fixed_qty').val('');
	   	*/

	    //Прячем блоки с ценой, кнопку добавления нового объекта и сущ. центры
        $('.itogo').hide();
        $('#addNewObject').hide();
        $('#rootitems_inner').hide();

	    $('#addNewObject').click(function(){
		    $('#object2-form').submit();
	    });

	    //Если чекбокс с правилами отмечен, то показываем кнопку Добавить

        $('#accept_rules').bind('change', function(){
            var related_el =   $('#addNewObject');
            var $obj = $(this);

            if( $obj.is(':checked')){
                related_el.show();
                related_el.children('select').attr('disabled', false );
            }
            else {
                related_el.hide();
                related_el.children('select').attr('disabled', true );
            }
            return false;
        });


	   //Если мы создаём объект, а не редактируем, то делаем запрос на существующие центры ( в которые можно добавить)
	   if(is_new_model == 1 ){
	        $.ajax({
	            url: "<?php echo $this->createUrl('ajaxobjects/get_parent_items_'); ?>",
	            data: {id: <?php echo (Yii::app()->user->isGuest)?'0':Yii::app()->user->id['user_id']; ?>},
	            dataType: 'json',
	            type: 'post',
                success:  function(data){
	                if(data.items !== 0){
		                //Если у вернулись в ответе центры, в которые можно добавить
		                //Присоединяем их к списку центров
	                    $('#rootitems').html(data.items);
	                }
	                else {
		                //Скрываем сам пункт "добавить в центр"
	                    $('#bBcproperty').hide();
	                }
	            }
	        });
       } else{
		   $('.b-add-docs').hide();

	   }
    }

    /*Сделать select только для чтения, дублировав его значение в скрытый input*/
    function readOnlySelect(el, state, name){
        var select = $(el);

        switch(state){
            case false:
                select.attr('disabled', false);
                select.next().remove();
                break;
            case true:
                var input_val = select.val();
                select.attr('disabled', true);
                $('<input/>', {
                    id: select.attr('id'),
                    name: name,
                    value: input_val,
                    type: 'hidden'
                }).insertAfter(select);
                break;
        }
    }
    /*Очистка формы: убираем блокировку со всех элементов и сбрасываем значения*/
    function clear_form(){

        $('#object2-form').each (function(){
            this.reset();

            $(this).find('.fcheck01').each(function () {
                var $this = $(this);
                if($this.hasClass('ce')){
                    $this.removeClass('ce');
                    $this.addClass('ue');
                }
            });

            $(this).find('select, input ').each(function () {
                var $this = $(this);
                if($this.is('[disabled]')){
                    readOnlySelect( '#'+$this.attr('id'), false);
                }
                $this.removeAttr('readonly');
                $(pref_+'city').attr('readonly', 'true');
            })
        });
    }
    function changeStateForm(){
        var itemType = parseInt($(pref_+'itemType').val()); //тип объекта
        var kind = parseInt($(pref_+'kind').val());

        /*По умолчанию скрываем список центров, в которые можно добавить*/
        $('#rootitems_inner').hide();
        clear_form();

	    switch(itemType){
            /*Отдельное помещенеие*/
		    case 0:
				$('#name_inner').hide();  // скрываем имя
                $('.b-object-wizard').show();
                $(pref_+'storey').attr('disabled', false);
                $('#storey_inner').show();
                $(pref_+'itemType').val(0);
            break;

            /*создать центр недвижимости*/
		    case 1:
                $('#name_inner').show();
                $('.b-object-wizard').show();
                $(pref_+'storey').attr('disabled', true);
                $('#storey_inner').hide();
                $('#total_item_sqr').parent().show();
                $('#total_item_sqr').attr('disabled',false);
                $(pref_+'itemType').val(1);
            break;

            /*добавить в существующий центр*/
		    case 2:
                $('.b-object-wizard').hide(); //скрываем все поля, пока не выбран центр
                $('#name_inner').show();
                $(pref_+'storey').attr('disabled', false); //этажность
                $(pref_+'storey').show();
                $('#rootitems_inner').show();
                $(pref_+'itemType').val(2);
            break;

		    default:
                //$('.b-object-wizard').hide();
                clear_form();
			break;
	    }
    }

   function set_price(){
	    var input_price = $(pref_+"price").val();
	    var price_val = parseFloat(input_price.replace(',', '.'));

	    var total_price ="";
	    if($(".only-object").is(":hidden")){
	        var total_item_sqr = parseFloat($(pref_+"total_item_sqr").val().replace(",", "."));
	        var total_price = price_val*total_item_sqr;

	    }
	    else if( $('.auto-price').is(':hidden')){
	        total_price = price_val;
	    }


	    $(".itogo span.CB0C11D").text(total_price.toFixed(2));
	    $(".itogo").show();

	    var percent = parseFloat($("#Object2_agent_comission").val());
	    if(isNaN(percent)) return false;
	    var comission = (total_price/100)*percent;
	    if(comission!=undefined && !isNaN(comission))
	        $(".b-agent-price span").text(comission.toFixed(2));
	}


    $('#price label').toggle( function() {
        $(this).children('.only-object').show();
        $(this).children('.auto-price').hide();
        set_price();
    }, function() {
        $(this).children('.auto-price').show();
        $(this).children('.only-object').hide();
        set_price();
    });


	if( is_new_model != 1) {
		<!--Если редактирование, то просто назначаем активный таб-->
	            $('.b-object-wizard').show();
	            $('#rootitems_inner').hide();

		        $('.tab3_set .tab3').each(function(){
		            $(this).removeClass('act');
		        });

		        switch(parseInt('<?echo $model->itemType?>')){
		            case 0:
		                $('#ofice').addClass('act');
	                    $('#b-general-feauters').show(); //блок общие характеристики
	                    $('#b-work-feauters').hide(); //блок рабочие характеристики
	                    $('#b-path').hide();          //блок подъездные пути
	                    $('#welfare_inner').hide();   // чекбокс бытовые помещения
	                    $('#class_inner').hide();
		                break;
		            case 1:
		                $('#salesroom').addClass('act');
	                    $('#b-general-feauters').show(); //блок общие характеристики
	                    $('#b-work-feauters').hide(); //блок рабочие характеристики
	                    $('#welfare_inner').show();  // чекбокс бытовые помещения
	                    $('#class_inner').show();
		                break;
		            case 2:
		                $('#stock_prod').addClass('act');
	                    $('#b-work-feauters').show(); //блок рабочие характеристики
	                    $('#b-general-feauters').hide(); //блок общие характеристики
	                    $('#class_inner').hide();
		                break;
		        }
	}
	else{
	/*Если новая запись, то вешаем обработчкики */

        /*Вешаем обработчики*/
        $(pref_+'kind').change(function(){
            changeStateForm();
        }).change();

        $(pref_+'itemType').change(function(){
            changeStateForm();
        });

        /*Если добавляем в существующий центр недвижимости и он выбран*/
        $('#rootitems').change(function(){
            if($(this).val()){
                $.ajax({
                    url: "<?php echo $this->createUrl('ajaxobjects/Load_parent_item'); ?>",
                    data: {id: $(this).val()},
                    dataType: 'json',
                    type: 'get',

                    success: function(data){

                        $(pref_+'city').val(data.city);
                        $(pref_+'city').change();

                        $('#temp_district').val(data.district); //район
                        $('#temp_street').val(data.street);     //улица

                        $(pref_+'name').val(data.name);
                        $(pref_+'name').attr('readonly', true);

                        $(pref_+'kind_structure').val(data.kind_structure); //
                        $(pref_+'storeys').val(data.storeys);
                        $(pref_+'total_item_sqr').val(data.total_item_sqr);
                        $(pref_+'price').val(data.price);
                        $(pref_+'house').val(data.house);
                        $(pref_+'repair').val(data.repair);
                        $(pref_+'parking').val(data.parking);
                        $(pref_+'pay_utilities').val(data.pay_utilities);
                        $(pref_+'flooring').val(data.flooring);
                        $(pref_+'ventilation').val(data.ventilation);
                        $(pref_+'contr_condition').val(data.contr_condition);
                        $(pref_+'housing').val(data.contr_condition);

                        //readOnlySelect(pref_+'city',true, pref+'[city]');
                        readOnlySelect(pref_+'district',true, pref+'[district]');
                        readOnlySelect(pref_+'street',true,  pref+'[street]');
                        //readOnlySelect(pref_+'kind_structure',true,  pref+'[kind_structure]');

                        $(pref_+'class').val(data.class);
                        readOnlySelect(pref_+'class',true,  pref+'[class]');

                        /*setting values for checkbox fields*/
                        set_param_for_checkbox(data.service_lift, $(pref_+'service_lift')) ;
                        set_param_for_checkbox(data.video, $(pref_+'video')) ;
                        set_param_for_checkbox(data.railways, $(pref_+'railways')) ;
                        set_param_for_checkbox(data.access, $(pref_+'access')) ;

                        /*интернет, провайдер*/
                        set_param_for_checkbox(data.internet, $(pref_+'internet')) ;

                        /*телефон, комания*/
                        set_param_for_checkbox(data.telephony, $(pref_+'telephony')) ;
                        set_param_for_checkbox(data.can_reclame, $(pref_+'can_reclame')) ;

                        set_param_for_checkbox(data.air_condit, $(pref_+'air_condit')) ;
                        set_param_for_checkbox(data.firefighting, $(pref_+'firefighting')) ;


                        var ro = 'readonly';
                        $(pref_+'house').attr(ro,'true');
                        $(pref_+'created').attr(ro,'true');
                        $(pref_+'storeys').attr(ro,'true');

                        $('.b-object-wizard').show();
                    }
                });

            }

            else{
                clear_form();
                $('.b-object-wizard').hide();
            }
        });



        /*Переключение активного таба типа объекта*/
        var item_type_val = 0;
        $('.switch3_grp .switch3_lbl').click(function() {
            var radio =   $(this).children('.fswitch01');
            var type_id = $(this).attr('id');


            switch(type_id){
                case 'bProperty':
                    item_type_val = 0;
                    bind_select_city(true);
                    $('#parent_field').val(0);
                    break;
                case 'bBusinesscenter':
                    item_type_val = 1;
                    $('#parent_field').val(0);
                    bind_select_city(true);
                    break;
	            case 'bBcproperty':
                    item_type_val = 2;
                    $('#parent_field').val(1);
                    bind_select_city(false); //Снимаем выбор города в попапе, т.к. у нас
                    break;
                default:
                    item_type_val=0;
                    $('#parent_field').val(0);
                    break;
            }

            $(pref_+'itemType').val(item_type_val);
            $(pref_+'itemType').trigger('change');

            if(!radio.hasClass('ce')) {
                $('.switch3_grp .switch3_lbl .fswitch01').each(function(){
                    $(this).removeClass('ce');
                    $(this).addClass('ue');
                });

                $('.switch3_grp .switch3_lbl').each(function(){
                    $(this).removeClass('active');
                });

                $(this).addClass('active');
                radio.addClass('ce');
                radio.removeClass('ue');
            }
        });

        /*Переключение активного таба типа помещения*/
        $('.tab3').each(function() {
            $(this).bind('click', function(){
                if(item_type_val ==2){
                    clear_form();
                    $('.b-object-wizard').hide();
                }

                var type_id = $(this).attr('id');
                var type = 0;

                switch(type_id){
                    case 'ofice': //офис
                        type = 0;
                        $('#b-general-feauters').show(); //блок общие характеристики
                        $('#b-work-feauters').hide(); //блок рабочие характеристики
                        $('#b-path').hide();          //блок подъездные пути
                        $('#welfare_inner').hide();   // чекбокс бытовые помещения
                        $('#class_inner').hide();
                        break;

                    case 'salesroom': //торговое помещение
                        type = 1;
                        $('#b-general-feauters').show(); //блок общие характеристики
                        $('#b-work-feauters').hide(); //блок рабочие характеристики
                        $('#welfare_inner').show();  // чекбокс бытовые помещения
                        $('#class_inner').show();
                        break;

                    case 'stock_prod':  //склад или производство
                        type = 2;
                        $('#b-work-feauters').show(); //блок рабочие характеристики
                        $('#b-general-feauters').hide(); //блок общие характеристики
                        $('#class_inner').hide();
                        break;

                    default:
                        type = 0;
                        break;
                }


                $(pref_+'kind').val(type);

                //$(pref_+'kind').trigger('change');

                if(!$(this).hasClass('act')) {
                    $('.tab3_set .tab3').each(function(){
                        $(this).removeClass('act');
                    });

                    $(this).addClass('act');

                }
                return false;
            });
        });
        $('#ofice').click();    //При создании нового объекта по умолч. выбран офис
    }
});
</script>


<div class="content_wide">
	<h1 class="header1"><?if( $model->isNewRecord) echo 'Размещение объекта'; else echo 'Редактирование объекта';?> </h1>
	<!-- заменить потом везде Object2 -->
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'object2-form',
		'enableAjaxValidation'=>true,
		'clientOptions' => array(
			'validateOnSubmit' => true, //При отправке
			'validateOnChange' => true,  //При изменении
			//После валидации
			'afterValidateAttribute'=>'js:function(form, attribute, data, hasError){
				//Если в общ. площади или арендной ставке есть ошибки: скрываем поле с суммой
				if (data.Object2_price !== undefined || data.Object2_total_item_sqr !== undefined){
					$(".itogo span.CB0C11D").empty();
			        $(".itogo").hide();
				}
				else{
					//Считаем сумму аренды, если ошибок нет и "за объект" не активна
					var price = parseFloat($("#Object2_price").val().replace(",", "."));
					var total_price ="";
					if($(".only-object").is(":hidden")){
						var total_item_sqr = parseFloat($("#Object2_total_item_sqr").val().replace(",", "."));

						var total_price = price*total_item_sqr.toFixed(2);

					}else{
			           total_price = price;
					}

					$(".itogo span.CB0C11D").text(total_price);
				    $(".itogo").show();
					var percent = parseFloat($("#Object2_agent_comission").val());
                    if(isNaN(percent)) return false;
                    var comission = (total_price/100)*percent;
					if(comission!=undefined && !isNaN(comission))
						$(".b-agent-price span").text(comission.toFixed(2));
				}
			 }'
		),
		'htmlOptions'=>array(
			'class'=>'cool-object-wizard_form',
			'enctype'=>'multipart/form-data',
		),
	));
	?>

	<?//php echo $form->errorSummary($model); ?>

	<!--Группа скрытых полей для временной записи атрибутов-->
	<input id="temp_district" type="hidden" value="<?if($model->isNewRecord) echo $model->district;?>"/>
	<input id="temp_street" type="hidden" value="<?if($model->isNewRecord) echo $model->street;?>"/>
	<input id="temp_class" type="hidden" value=""/>

	<?if( $model->isNewRecord){?>
	<div class="switch3_grp">
	    <label class="switch3_lbl" id="bProperty">
	        <div class="fswitch01 ce M35R"></div>
	        Отдельное помещение
	    </label>

	    <label class="switch3_lbl" id="bBusinesscenter">
	        <div class="fswitch01 ue M35R"></div>
	        Создать центр
	    </label>

	    <label class="switch3_lbl" id='bBcproperty'>
	        <div class="fswitch01 ue M35R"></div>
	        Добавить в центр
	    </label>
	</div>
   <?}?>

	<!--Тип. 0-Простой объект, 1-Объект-контейнер, 2-Вложенный объект-->
	<?php echo $form->DropDownList($model, 'itemType',   array( 0=>'Отдельное помещение', 1=>'Много помещений в одном месте (создать центр недвижимости)', 2=>'Добавить помещение в существующий центр недвижимости'), array('class'=>'dn'))?>
	<!--Родитель: Наследуемые жёстко параметры: city, district, street, storeys, house, kind_structure -->
	<?php echo $form->HiddenField($model,'parent',array('size'=>10,'maxlength'=>10, 'id'=>'parent_field')); ?>
	<!-- Тип помещения -->
	<?php echo $form->DropDownList($model, 'kind',  array(0=>'офис', 1=>'торговое', 2=>'Склад\Производство'), array('class'=>'dn'));?>



<div class="form01_grp M65T">
	<div class="form3_blk">
		<div class="tab3_set">
		  <div class="tab3 act" id="ofice">Офис</div>
		  <div class="tab3" id="salesroom">Торговое помещение</div>
		  <div class="tab3" id="stock_prod">Склад/Производство</div>
		</div>

	<div class="form3_set">
	        <h2 class="header2">Основные характеристики помещения</h2>
            <!--сущ. Центры недвижимости  --->
		    <div class="b-object-wizard-head"  id="rootitems_inner">
			    <div class="b-fieldset-line">
		            <div class="b-fieldset">
				      <label class="label_input">Cуществующий центр</label>
			            <div class="b-fieldset__input-inner select-input">
				            <select id='rootitems' class=""></select>
			                <div class="clear"></div>
			            </div>
		            </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
		    </div>
	        <div class="clear"></div>

			<div class="b-object-wizard">
				<div class='b-main-field__inner'>
					<? if($model->isNewRecord ||  (!$model->isNewRecord && $model->parent != 1)){?>
						<?if($model->itemType==1 || $model->isNewRecord ){?>
							<div class="b-fieldset-line" id="name_inner">
								<div class="b-fieldset">
								    <!--Имя (Название) --->
									<?php echo $form->labelEx($model,'name'); ?>
						                <div class="b-fieldset__input-inner">
											<?php echo $form->textField($model,'name',array('size'=>47,'maxlength'=>47, 'class'=>'')); ?>
			                                <ins class="star">*</ins>
						                </div>

									<?php echo $form->error($model,'name'); ?>
									<div class="clear"></div>
		                        </div>
		                        <div class="clear"></div>
							</div>
						<?}?>

					<div class="b-fieldset-line">
		                <!--Город, при выборе города загружаются связанные поля, такие как:district, street, providers, tel_company -->
                        <div class="b-fieldset city">
	                        <?echo $form->labelEx($model,'city');?>
	                        <div class="b-fieldset__input-inner ">
		                        <?
		                        echo $form->textField($model, 'city',	array(
			                        'ajax'=> array(
				                        'url'=>'object2/ajaxUpdate',
				                        'data'=>array('city'=>'js: $(this).val()'),
				                        'dataType'=>'json',
				                        'type'=>'get',
				                        'success'=>'js: function(data) {
														$("#districts select").html(data.districts);
														$("#streets select").html(data.streets);
													}',

				                        'complete'=> 'function() {

												    if($("#temp_district").val()) {
												        $("#districts select").val($("#temp_district").val());
												        $("#districts input").val($("#temp_district").val());

												        $("#temp_district").val("");
												        $("#districts select").attr("readonly","true");
												    }
												     if($("#temp_street").val()) {
												        $("#streets select").val($("#temp_street").val());
												        $("#streets input").val($("#temp_street").val());
												        $("#temp_street").val("");
												        $("#streets select").attr("readonly","true");
												     }
											    }'),
			                        'empty' =>'Выберите город',
			                        'readonly'=>'true',
			                        'class' => '',
		                        ));
		                        ?>
	                            <ins class="star">*</ins>
	                        </div>
	                        <?php echo $form->error($model,'city'); ?>
                        </div>

					    <!--район-->
						<div class="b-fieldset right">
							<?php echo $form->labelEx($model,'district'); ?>
						    <div class="b-fieldset__input-inner select-input" id="districts">
								<?php echo $form->DropDownList($model, 'district',  array(), array('empty'=>'Выберите город'))?>
				                 <ins class="star">*</ins>
						    </div>
                            <div class="input02_ner W160"> <?php echo $form->error($model,'district'); ?></div>
                        </div>
						<div class="clearfix"></div>
					</div>

				    <!--Улица, дом, корпус-->
					<div class="b-fieldset-line">

					    <!--улица -->
                        <div class="b-fieldset" id="steet_inner">
							<?php echo $form->labelEx($model,'street'); ?>
                            <div class="b-fieldset__input-inner select-input" id="streets">
								<?php echo $form->dropDownList($model, 'street', array(), array('empty'=>'Выберите город', 'class'=>'W125')); ?>
                                <ins class="star">*</ins>
					        </div>
					        <?php echo $form->error($model,'street'); ?>
					    </div>
						<?
						//При редактировании и создании нового объекта нужно загрузить связанные с городом поля
							Yii::app()->clientScript->registerScript(
								'triggerCityChange',
								'$(function(){
									$("#Object2_city").change();
								});'
							);
						?>
					    <!--Дом -->
                        <div class="b-fieldset" id="house">
							<?php echo $form->labelEx($model,'house'); ?>
			                <div class="b-fieldset__input-inner">
								<?php echo $form->textField($model,'house', array('size'=>15,'maxlength'=>15, 'class'=>'input02 W45')); ?>
                                <ins class="star">*</ins>
			                </div>
				            <?php echo $form->error($model,'house'); ?>
					    </div>
                        <!--Корпус-->
                        <div class="b-fieldset" id="housing">
							<?php echo $form->labelEx($model,'housing'); ?>
                            <div class="b-fieldset__input-inner">
								<?php echo $form->textField($model,'housing', array('size'=>15,'maxlength'=>15, 'class'=>'input02 W45') ); ?>
				            </div>
				            <?php echo $form->error($model,'housing'); ?>
					    </div>
						<div class="clear"></div>
					</div>
					<?}?>
					<div class="b-fieldset-line">

					    <? if(!$model->isNewRecord && $model->parent == 1){?>
                        <div class="b-fieldset" id="housing">
							<?php echo $form->labelEx($model,'housing'); ?>
                            <div class="b-fieldset__input-inner">
								<?php echo $form->textField($model,'housing', array('size'=>15,'maxlength'=>15, 'class'=>'input02 W45') ); ?>
                            </div>
							<?php echo $form->error($model,'housing'); ?>
                        </div>
						<?}?>
						<? if($model->isNewRecord || (!$model->isNewRecord && $model->parent != 1)){?>
					    <!--Этаж-- показывать, если itemType!=1  -->
                        <div class="b-fieldset" id="storey_inner">
							<?php echo $form->labelEx($model,'storey'); ?>
                            <div class="b-fieldset__input-inner">
								<?php echo $form->textField($model,'storey', array('class'=>'input02 LR W45')); ?>
                                <ins class="star">*</ins>
					        </div>
					        <?php echo $form->error($model,'storey'); ?>
					    </div>
						<?}?>
					    <!-- Этажность  показывать, если itemType!=2 -->
                        <div class="b-fieldset" id="storeys_inner">
								<?php echo $form->labelEx($model,'storeys'); ?>
                             <div class="b-fieldset__input-inner">
								<?php echo $form->textField($model,'storeys',  array('class'=>'input02 LR W45')); ?>
							 </div>

					        <?php echo $form->error($model,'storeys'); ?>
					    </div>

                        <!--класс-->
                        <!--div class="b-fieldset right"  id="class_inner">
							<?//php echo $form->labelEx($model,'class'); ?>
                            <div class="b-fieldset__input-inner select-input">
								<?//php echo $form->dropDownList($model, 'class',  CHtml::listData(Classes::model()->findAll(), 'id', 'shname'), array( 'empty'=>'---', 'class'=>'W50')); ?>
                                <ins class="star">*</ins>
                            </div>
							<?//php echo $form->error($model,'class'); ?>
                        </div-->
                        <div class="clear"></div>
					</div>


                    <div class="b-fieldset-line">
                        <!--Общая площадь помещения--, показывать itemType=1 -->
                        <div class="b-fieldset" id="total_item_sqr">
		                    <?php echo $form->labelEx($model,'total_item_sqr'); ?>
                            <div class="b-fieldset__input-inner">
			                    <?php echo $form->textField($model,'total_item_sqr', array('class'=>'input02 LR W45','maxlength'=>8)); ?>
                                <ins class="star">*</ins>
                            </div>
		                    <?php echo $form->error($model,'total_item_sqr'); ?>
                        </div>

                        <!--Арендная ставка -->
                        <div class="b-fieldset" id="price" >
                            <label >
                                <span class="auto-price">Арендная ставка (руб./м<sup>2</sup>)</span>
                                <span class="only-object" style="display: none;">За объект</span>
                            </label>

                            <div class="b-fieldset__input-inner">
								<?php echo $form->textField($model,'price', array('class'=>'W50','maxlength'=>8,));?>
                                <ins class="star">*</ins>
                            </div>
							<?php echo $form->error($model,'price'); ?>

                        </div>
                        <div class="clear"></div>
					</div>


	                <div class="b-fieldset-line"  id="price_line">
	                    <!--Комиссия агента -->
                        <div class="b-fieldset" id="agent_comission_inner" >
	                        <label >Комиссия агента</label>
		                     <?
		                    for($i=1; $i<=5; $i++)  $percentage[10*$i] = 10*$i.'%'; ?>
                            <div class="b-fieldset__input-inner select-input">
	                            <? echo $form->dropDownList($model, 'agent_comission',  $percentage, array('empty'=>'--'));  ?>
                            </div>
	                        <? echo $form->error($model,'agent_comission'); ?>
	                    </div>
                        <div class="b-agent-price"><span>0</span> руб. </div>
                        <div class="itogo">Итого: <span class="CB0C11D"></span> руб./мес.</div>
		                <div class="clear"></div>

	                </div>
                    <div class="clear"></div>
				</div>
				<div class="note3">
					Если у вас недостаточно времени заполните только этот блок. Наш модератор свяжется с вами по телефону и уточнит необходимые детали. Имейте в виду, что на модерацию «быстрых объявлений требуется дополнительное время
				</div>
				<div class="clear"></div>
                <div class="separator3"></div>
					<!-- Дополнительные параметры start-->
                <div class="b-additional_params">
	                <!--Блок Общие характеристики -->
                    <div class="params_row" id="b-general-feauters">
                        <h1>Общие характеристики</h1>

		                <!--Тип сооружения-->
                         <div class="b-fieldset"  id="kind_structure_inner">
			                 <?php echo $form->labelEx($model,'kind_structure'); ?>
                             <div class="b-fieldset__input-inner select-input">
	                             <?php echo $form->dropDownList($model, 'kind_structure',  CHtml::listData(KindStructure::model()->findAll(), 'id', 'shname'), array('empty'=> 'Не указан', 'class'=>'')); ?>
                             </div>
                             <div class="input02_ner W160"> <?php echo $form->error($model,'kind_structure'); ?></div>
                         </div>
                        <div

                        <!--Состояние ремонта-- ($kind<2) && ($item_type!=1 -->
                        <div class="b-fieldset"  id="repair_inner">
		                    <?php echo $form->labelEx($model,'repair'); ?>
                            <div class="b-fieldset__input-inner select-input">
			                    <?php echo $form->dropDownList($model, 'repair',  CHtml::listData(Repair::model()->findAll(), 'id', 'shname'), array('empty'=> 'Не указан', 'class'=>'')); ?>
                            </div>
                            <div class="input02_ner W160"> <?php echo $form->error($model,'repair'); ?></div>
                        </div>

                       <!--Комунальные услуги -->
                        <div class="b-fieldset">
		                    <?php echo $form->labelEx($model,'pay_utilities'); ?>
                            <div class="b-fieldset__input-inner select-input">
			                    <?php echo $form->dropDownList($model, 'pay_utilities',  CHtml::listData(PayUtil::model()->findAll(), 'id', 'shname'), array('empty'=> 'Не указан', 'class'=>'')); ?>
                            </div>
                            <div class="input02_ner W160"> <?php echo $form->error($model,'pay_utilities'); ?></div>
                        </div>


                        <!--Возможность рекламы--$hasparent==0-->
                        <label class="check3_lbl">
                            <div class="fcheck01 ue">
			                    <?php echo $form->checkBox($model,'can_reclame', array('class'=>'dn')); ?>
                            </div>
		                    <?php echo $form->labelEx($model,'can_reclame'); ?>
		                    <?php echo $form->error($model,'can_reclame'); ?>
                        </label>
                        <!--Коммуникации start-->
                        <div id="b-communication" class="sub-block">
                            <h1>Коммуникации</h1>
                            <!--Интернет-->
                            <label class="check3_lbl">
                                <div class="fcheck01 ue">
				                    <? echo $form->CheckBox($model, 'internet', array('class' => 'dn')) ?>
                                </div>
			                    <?php echo $form->labelEx($model,'internet'); ?>
			                    <?php echo $form->error($model,'internet'); ?>
                            </label>

                            <!--Телефон-->
                            <label class="check3_lbl">
                                <div class="fcheck01 ue">
				                    <? echo $form->CheckBox($model, 'telephony', array('class' => 'dn')) ?>
                                </div>
			                    <?php echo $form->labelEx($model,'telephony'); ?>
			                    <?php echo $form->error($model,'telephony'); ?>
                            </label>
                        </div>
                        <!--Коммуникации end-->
	                </div>



	                 </div>
	                <!--Блок Общие характеристики end -->

	                <!--Рабочие характеристики-->
	                <div class="b-work-feauters params_row" id="b-work-feauters">
	                <h1>Рабочие характеристики</h1>

                    <!--Кран-балка-->
                    <label class="check3_lbl">
                        <div class="fcheck01 ue">
				            <?echo $form->CheckBox($model, 'cathead', array('class' => 'dn')) ?>
                        </div>
			            <?php echo $form->labelEx($model,'cathead'); ?>
			            <?php echo $form->error($model,'cathead'); ?>
                    </label>

                    <!--Лифт--$kind!=1 -->
                    <label class="check3_lbl">
                        <div class="fcheck01 ue">
				            <?echo $form->CheckBox($model, 'service_lift', array('class' => 'dn')) ?>
                        </div>
			            <?php echo $form->labelEx($model,'service_lift'); ?>
			            <?php echo $form->error($model,'service_lift'); ?>
                    </label>

                    <!--Система вентиляции-- $kind==1 req -->
                    <div class="b-fieldset"  id="ventilation_inner">
		                <?php echo $form->labelEx($model,'ventilation'); ?>
                        <div class="b-fieldset__input-inner select-input">
			                <?php echo $form->dropDownList($model, 'ventilation',  CHtml::listData(Ventilation::model()->findAll(), 'id', 'shname'), array('empty'=> 'Не указан', 'class'=>'')); ?>
                        </div>
                        <?php echo $form->error($model,'ventilation'); ?>
                    </div>

                    <!--Система отопления и водоснабжения-$hasparent==0) && ($kind>1 -->
	                <label class="check3_lbl">
	                    <div class="fcheck01 ue">
							<?echo $form->CheckBox($model, 'heating', array('class' => 'dn')) ?>
	                    </div>
						<?php echo $form->labelEx($model,'heating'); ?>
						<?php echo $form->error($model,'heating'); ?>
	                </label>

                    <div class="b-fieldset">
		                <?php echo $form->labelEx($model,'ceiling_height'); ?>
                        <div class="b-fieldset__input-inner">
			                <?php echo $form->textField($model,'ceiling_height', array('class'=>'input02 LR w30')); ?>
                        </div>
		                <?php echo $form->error($model,'ceiling_height'); ?>
	                    <div class="clear"></div>
                    </div>

	                <!--Рабочая высота помещения-->
                    <div class="b-fieldset">
		                <?php echo $form->labelEx($model,'work_height'); ?>
                        <div class="b-fieldset__input-inner">
			                <?php echo $form->textField($model,'work_height', array('class'=>'input02 LR w30')); ?>
                        </div>
		                <?php echo $form->error($model,'work_height'); ?>
                        <div class="clear"></div>
                    </div>

                        <!--Покрытие пола--($kind!=1) && ($item_type!=1) -->
                    <div class="b-fieldset"  >
		                <?php echo $form->labelEx($model,'flooring'); ?>
                        <div class="b-fieldset__input-inner select-input">
			                <?php echo $form->dropDownList($model, 'flooring',  CHtml::listData(Flooring::model()->findAll(), 'id', 'shname'), array('empty'=> 'Не указан', 'class'=>'')); ?>
                        </div>
		                <?php echo $form->error($model,'flooring'); ?>
                        <div class="clear"></div>
                    </div>

	                <!--Электр.мощность-->
	                <div class="b-fieldset" id="el_power_inner">
						<?php echo $form->labelEx($model,'el_power'); ?>
                        <div class="b-fieldset__input-inner">
							<?php echo $form->textField($model,'el_power', array('class'=>'input02 LR w30')); ?>
	                    </div>
						<?php echo $form->error($model,'el_power'); ?>
                        <div class="clear"></div>
	                </div>

	                <!--Количество ворот-->
                    <div class="b-fieldset">
		                <?php echo $form->labelEx($model,'gates_qty'); ?>
                        <div class="b-fieldset__input-inner">
			                <?php echo $form->textField($model,'gates_qty', array('class'=>'input02 LR w30')); ?>
                        </div>
		                <?php echo $form->error($model,'gates_qty'); ?>
                        <div class="clear"></div>
                    </div>

	                <!--Высота ворот-->
                    <div class="b-fieldset">
		                <?php echo $form->labelEx($model,'gates_height'); ?>
                        <div class="b-fieldset__input-inner">
			                <?php echo $form->textField($model,'gates_height', array('class'=>'input02 LR w30')); ?>
                        </div>
		                <?php echo $form->error($model,'gates_height'); ?>
                        <div class="clear"></div>
                    </div>
	            </div>
	                 <!--Рабочие характеристики-- end>

		             <!--Вторая колонка start-->
	                <div class="params_row">
                        <!--Блок Комфорт-->
                        <div id="b-comfort" class="b-comfort sub-block">
                             <h1>Комфорт</h1>

	                        <!-- Бытовые помещения -->
	                        <label class="check3_lbl">
	                            <div class="fcheck01 ue">
						            <?echo $form->CheckBox($model, 'air_condit', array('class' => 'dn')) ?>
	                            </div>
					            <?php echo $form->labelEx($model,'air_condit'); ?>
					            <?php echo $form->error($model,'air_condit'); ?>
	                        </label>

                            <!--Паркинг--$hasparent==0 -->
                            <div class="b-fieldset">
		                        <?php echo $form->labelEx($model,'parking',array('style'=>'width:45px')); ?>
                                <div class="b-fieldset__input-inner select-input">
			                        <?php echo $form->dropDownList($model, 'parking',  CHtml::listData(Parking::model()->findAll(), 'id', 'shname'), array('empty'=> 'Не указан', 'class'=>'')); ?>
                                </div>
                                <div class="input02_ner W160"> <?php echo $form->error($model,'repair'); ?></div>
                            </div>


                            <!-- Бытовые помещения -->
	                        <label class="check3_lbl" id="welfare_inner">
	                            <div class="fcheck01 ue">
						            <?echo $form->CheckBox($model, 'welfare', array('class' => 'dn')) ?>
	                            </div>
					            <?php echo $form->labelEx($model,'welfare'); ?>
					            <?php echo $form->error($model,'welfare'); ?>
	                        </label>
	                    </div>
                        <!--Блок Комфорт-- end-->



                        <!--Блок безопасность start-->
		                <div id="b-security" class="sub-block">
		                    <h1>Безопасность</h1>
	                        <label class="check3_lbl">
	                            <div class="fcheck01 ue">
					                <?echo $form->CheckBox($model, 'video', array('class' => 'dn')) ?>
	                            </div>
				                <?php echo $form->labelEx($model,'video'); ?>
				                <?php echo $form->error($model,'video'); ?>
	                        </label>
			                <label class="check3_lbl">
	                            <div class="fcheck01 ue">
					                <?echo $form->CheckBox($model, 'firefighting', array('class' => 'dn')) ?>
	                            </div>
				                <?php echo $form->labelEx($model,'firefighting'); ?>
				                <?php echo $form->error($model,'firefighting'); ?>
	                        </label>
			                <label class="check3_lbl">
	                            <div class="fcheck01 ue">
					                <?echo $form->CheckBox($model, 'access', array('class' => 'dn')) ?>
	                            </div>
				                <?php echo $form->labelEx($model,'access'); ?>
				                <?php echo $form->error($model,'access'); ?>
	                        </label>
		                </div>
                        <!--Блок безопасность end-->

                        <!--Блок пути start-->
			            <div id="b-path" class="sub-block">
		                    <h1>Подъездные пути</h1>
	                        <!--Пандус-->
	                        <label class="check3_lbl">
	                            <div class="fcheck01 ue">
						            <?echo $form->CheckBox($model, 'rampant', array('class' => 'dn')) ?>
	                            </div>
					            <?php echo $form->labelEx($model,'rampant'); ?>
					            <?php echo $form->error($model,'rampant'); ?>
	                        </label>

	                        <!--Автомобильные подъездные пути-->
	                        <label class="check3_lbl">
	                            <div class="fcheck01 ue">
						            <?echo $form->CheckBox($model, 'autoways', array('class' => 'dn')) ?>
	                            </div>
					            <?php echo $form->labelEx($model,'autoways'); ?>
					            <?php echo $form->error($model,'autoways'); ?>
	                        </label>

	                        <!--Ж/Д подъездные пути-->
	                        <label class="check3_lbl">
	                            <div class="fcheck01 ue">
						            <?echo $form->CheckBox($model, 'railways', array('class' => 'dn')) ?>
	                            </div>
					            <?php echo $form->labelEx($model,'railways'); ?>
					            <?php echo $form->error($model,'railways'); ?>
	                        </label>
			            </div>
                        <!--Блок пути end-->
	                </div>
                    <!--Вторая колонка end-->

	                <!-- 3 колонка start-->
	                <div class="note3" >
                        Имейте в виду, что на модерацию «быстрых объявлений требуется дополнительное время
	                </div>
                    <!-- 3 колонка end-->
					<div class="clear"></div>
			</div>

                <div class="separator3"></div>

			 <!--Добавление фотографий-->
                 <h1 class="additional-doc-title">Фотографии/документы</h1>
                  <!--link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/vader/jquery-ui.css" rel="stylesheet" /-->
				<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/jcrop/jquery.Jcrop.min.css'); ?>
                <script type="text/javascript" src="<?php echo $this->createAbsoluteUrl('/js/ajaxupload.3.6.js'); ?>"></script>
                <script type="text/javascript" src="<?php echo $this->createAbsoluteUrl('/js/jquery.Jcrop.min.js'); ?>"></script>

				<?php if (!empty($fotos))	$qty = count($fotos); else $qty = 0;
				/*switch ($obj->kind) {
					case 1 : {$lbKind = 'Торговое'; $lbParent = 'Торговый центр'; break;}
					case 2 : {$lbKind = 'Склад / Производственное'; $lbParent = 'Центр Недвижимости'; break;}
					default: {$lbKind = 'Офисное'; $lbParent = 'Офисный центр';}
				}*/
				$titleBlank = 'Нажмите, чтобы загрузить';
				$titleFoto = 'Нажмите, чтобы заменить фотографию';
				$fileBlank = 'images/add_photo113.jpg';
				?>

                <script type="text/javascript">
                    $(document).ready(function(){
                        var glass = $('div#glass');
                        var indic = $('div#indic');
                        function ShowGlass() {
                            var w = $(document).width();
                            var h = $(document).height();
                            glass.attr({'width': w,'height': h});
                            glass.show();
                            indic.css({top:h/4, left:w*0.4});
                            indic.show();
                        }
                        function HideGlass() {
                            glass.hide();
                            indic.hide();
                        }
                        HideGlass();
                        function RemoveBlock (e) {
                            e.preventDefault();
                            var s = $(this).attr('id').substr(5);
                            var img = $('img#iitem'+s);
                            var fName = $(img).attr('src');
                            $.ajax({
                                url: 'ajaxfoto/delete/',
                                data: {	file: fName	},
                                type: 'post',
                                success: function(){
                                    $(img).attr({
                                        src: '<?php echo $fileBlank;?>',
                                        title: '<?php echo $titleBlank;?>'
                                    });
                                }
                            });
                        }
                        function showCoords(c){
                            $('#x1').val(Math.round(c.x));
                            $('#y1').val(Math.round(c.y));
                            $('#x2').val(Math.round(c.x2));
                            $('#y2').val(Math.round(c.y2));
                        }
                        function bindDownloader (img) {
                            var bt = new AjaxUpload(img, {
                                action: 'ajaxfoto/upload/',
                                name: 'myfile',
                                onSubmit : function(file, ext) {
                                    ShowGlass();
                                    if (ext && /^(jpg|png|jpeg|gif|JPG|PNG|JPEG|GIF)$/.test(ext)) {
                                        this.disable();
                                    } else {
                                        HideGlass();
                                        alert('Неверный тип картинки, допускается только: jpg, jpeg, png, gif');
                                        return false;
                                    }
                                },
                                onComplete: function(file, response) {
                                    HideGlass();
                                    var action = this;
                                    doClose = true;
                                    if (response != false){
                                        var data = response.split('|');
                                        var file_name = data[0];
                                        $('#load-here').append(
                                                jQuery('<img />')
                                                        .attr({
                                                            src: 'storage/images/temp/' + file_name,
                                                            id:  'crop-image'
                                                        })
                                        );
                                        $('#crop-box').dialog({
                                            modal: true,
                                            width: (parseInt(data[1])+34) + 'px',
                                            title: 'Редактирование картинки',
                                            close: function() {
                                                ShowGlass();
                                                action.enable();
                                                $('#crop-box').hide();
                                                $('#load-here').empty();
                                                if (doClose) {
                                                    $.ajax({
                                                        url: 'ajaxfoto/cancel/',
                                                        data: {	file: file_name	},
                                                        type: 'post'
                                                    });
                                                    HideGlass();
                                                }
                                            },
                                            resizable: false,
                                            position:['middle', 100]
                                        });
                                        $('#crop-image').Jcrop({
                                            setSelect: [10, 10, 410, 310],
                                            minSize: [400, 300],
                                            aspectRatio: 4/3,
                                            allowSelect: false,
                                            bgColor: '#ffffff',
                                            bgOpacity: .5,
                                            onChange: showCoords,
                                            onSelect: showCoords
                                        });
                                        $('#save').click(function() {
                                            if (file_name != '') {
                                                $.ajax({
                                                    url: 'ajaxfoto/crop/',
                                                    data: {
                                                        file: file_name,
                                                        old:$(img).attr('src'),
                                                        oid:<?php echo ($model->isNewRecord) ? $model->id : $obj->id;?>,
                                                        x1: $('#x1').val(),
                                                        y1: $('#y1').val(),
                                                        x2: $('#x2').val(),
                                                        y2: $('#y2').val()
                                                    },
                                                    dataType: 'html',
                                                    type: 'post',
                                                    success: function(data){
                                                        $('#divAdelPhoto').removeClass('hide');
                                                        img.attr({
                                                            src: 'storage/images/_thumbs/' + data,
                                                            alt: data,
                                                            title: '<?php echo $titleFoto;?>'
                                                        });
                                                        file_name = '';
                                                        HideGlass();
                                                    }
                                                });
                                                doClose = false;
                                                $('#crop-box').dialog("close");
                                            }
                                        });
                                    }else{
                                        alert('Размер картинки слишком большой, разрешенно не более 6Мб');
                                        action.enable();
                                    }
                                }
                            });
                        }

                        for (i=0; i<5; i++) {
                            var lnk = $("#aitem" + i);
                            var img = $("#iitem" + i);
                            bindDownloader(img);
                            lnk.bind('click', {}, RemoveBlock);
                        }
                    });
                </script>


                <div class="b-photos-add" id="container" style="margin-left:-10px;">
					<?php for($i=0; $i<5; $i++){?>
                    <div class="item-container" id="item<?php echo $i;?>">
                        <img id="iitem<?php echo $i;?>" <?php if ($i<$qty):?>src="storage/images/_thumbs/<?php echo $fotos[$i]->name_gallery;?>" title="<?php echo $titleFoto;?>"<?php else:?>src="<?php echo $fileBlank;?>" title="<?php echo $titleBlank;?>"<?php endif;?> />
                        <a class="item-del-link" id="aitem<?php echo $i;?>" href="#">Удалить</a>
                        <div class="clear"></div>
                    </div>
					<?php } ?>

                        <div class="clear"></div>
                    </div>
                    <div class="note3">Загрузите ваши фотографии</div>
                      <div class="clear"></div>


                  <!--Добавление документов-->
                  <style type="text/css">
                      #doc2, #doc3
                      {display: none;}
                      #addBox, #addDocBox{
                          margin: 10px 0;
                      }
                      div.master div.bordered {width:550px;}
                  </style>
                  <script type="text/javascript">
                      $(document).ready(function(){
                          var addDocBox_obj = $('#addDocBox');
                          addDocBox_obj.html('<a id="addDoc" href="#">[+] прикрепить еще файл</a>');
                          $('#addDoc').click(function(){
                              var NextDoc_obj = $('#documents').find('div:hidden:first');
                              var id = NextDoc_obj.attr('id');
                              if (id == 'doc3')
                                  addDocBox_obj.html('');
                              NextDoc_obj.show();
                              return false;
                          });
                      })
	                  </script>

                    <div class="b-add-docs bordered" style="display: none;">
	                      <div id="documents">
		                      <?php echo $form->error($model,'docname'); ?>
		                      <?php echo $form->error($model,'docums'); ?>
	                          <div class="b-load-docs_inner" id="doc1">
		                             <div class="b-load-docs"><img src="/images/doc_icon.jpg"/></div>
		                            <?php echo $form->TextField($model, '[1]docname', array('value'=>'', 'size'=>20, 'maxlength'=>23)); ?>
		                            <?php echo $form->FileField($model, '[1]docums', array('class'=>'select_file')); ?>
	                          </div>
                              <div class="b-load-docs_inner" id="doc2">
                                  <div class="b-load-docs"><img src="/images/doc_icon.jpg"/></div>
	                              <?php echo $form->TextField($model, '[2]docname', array('value'=>'', 'size'=>20, 'maxlength'=>23)); ?>
			                       <?php echo $form->FileField($model, '[2]docums', array('class'=>'select_file')); ?>
                              </div>
                              <div class="b-load-docs_inner" id="doc3">
                                  <div class="b-load-docs"><img src="/images/doc_icon.jpg"/></div>
	                              <?php echo $form->TextField($model, '[3]docname', array('value'=>'', 'size'=>20, 'maxlength'=>23)); ?>
	                              <?php echo $form->FileField($model, '[3]docums', array('class'=>'select_file')); ?>
                              </div>
	                          <div id="addDocBox"></div>
	                      </div>
	                      <div class="clear"></div>
	                </div>

                    <!--div style="font-size:14px;"><u>Ограничение:</u> размер фотографий <b>не может</b> превышать 6 мегабайт</div-->

                    <!-- Hidden dialog -->
                    <div style="display:none;" id="crop-box">
                        <div id="dialog">
                            <div id="load-here"></div>
                            <button id="save" style="margin-top:10px;">Cохранить</button>
                        </div>
                    </div>
                    <!-- /Hidden dialog -->
                    <div id="glass" style="position: absolute; top:0px; left:0px; right:0px; bottom:0px; opacity: 0.1; z-index:10005; background-color: #BED400; overflow: hidden;"></div>
                    <div id="indic" style="position: absolute; top:0px; left:0px; font-size:16px; padding-left:25px; background: url('/images/indicator.gif') no-repeat 0 0">Обработка...</div>

                    <input type="hidden" id="x1" name="x1" />
                    <input type="hidden" id="y1" name="y1" />
                    <input type="hidden" id="x2" name="x2" />
                    <input type="hidden" id="y2" name="y2" />

            <div class="separator3"></div>
                    <!--Описание, Дополнительная информация-->
                    <h2 class="header2"><?php echo $form->labelEx($model,'about'); ?></h2>
                    <div class="M20T b-text-area">
						<?php echo $form->textArea($model,'about', array('class'=>'h90 textarea01 w440')); ?>
                        <div class="note3">
                            Текстовая информация.
                        </div>
                        <div class="clear"></div>
                    </div>
					<?php echo $form->error($model,'about'); ?>
                    <div class="clear"></div>
        </div>

	</div>
	<?php $this->endWidget(); ?>
</div>


	<div class="form3_set">
		<div class="P5L">
	        <div class="M15B">
	            <label class="check01_lbl" style="float:left;">
	                <div class="fcheck01 ue"></div>
	                <input class="dn" id="accept_rules" checked="false" type="checkbox"  tabindex="21">
	                Я ознакомился с <a href="/site/page/rules.html" target="_blank">правилами</a> размещения объявлений</label>
	            </label>
				<?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array('class'=>'button01', 'id'=>'addNewObject', )); ?>
	            <div class="clear"></div>
	        </div>
		</div>
	</div>



</div>
</div>














