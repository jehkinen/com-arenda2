<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/form.css'); ?>
Фильтры: <?php echo CHtml::link('Сбросить', array('adminitems/unset')); ?><br />
<div class="form">
	<?php echo CHtml::form(); ?>
	<?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:'); ?>
	<strong><?php echo CHtml::activeLabel($form, 'city'); ?>:</strong> <?php echo CHtml::dropDownList('AdminItems_form[city]', $city, $Cities); ?>
	<strong><?php echo CHtml::activeLabel($form, 'kind'); ?>:</strong> <?php echo CHtml::dropDownList('AdminItems_form[kind]', $kind, $Kinds); ?>
	<strong><?php echo CHtml::activeLabel($form, 'status'); ?>:</strong> <?php echo CHtml::dropDownList('AdminItems_form[status]', $status, $Statuses); ?>
	<strong><?php echo CHtml::activeLabel($form, 'owner'); ?>:</strong> <?php echo CHtml::dropDownList('AdminItems_form[owner]', $owner, $Users); ?>
	<div class="row buttons">
		<br /><?php echo CHtml::submitButton('Найти', array('id'=>'submit')); ?><br />
	</div>
	<?php echo CHtml::endForm(); ?>
</div><!--form-->
<?php if (is_array($rows)):?>
<hr>Найдено объектов: <?php echo $foundQty; ?><br /><br />
<?php if (!empty($rows)):?>
<style type="text/css">
table.sample {
  border-spacing: 2px;
  border-collapse: collapse;
  width: 100%;
  text-align: left;
}
table.sample th {
  border: 1px solid #000000;
  padding: 5px;
  background-color: #aaaaaa;
  text-align: center;
}
table.sample td {
  border: 1px solid #000000;
  padding: 5px;
}
table.sample td.action {
  text-align: center;
}
</style>
<script type="text/javascript" src="js/jquery.simplemodal.js"></script>
<script type="text/javascript" src="js/adminitems.js"></script>
<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/modal.css'); ?>
<div id="mwindow">
	<div class="header"></div>
	<div class="message">
		<input type="radio" name="stat" value="0" alt="деактивирован" id="rb0" /><label for="rb0">заблокировать</label><br />
		<input type="radio" name="stat" value="1" alt="приостановлен" id="rb2" /><label for="rb2">остановить показы</label><br />
		<input type="radio" name="stat" value="6" alt="идут показы" id="rb3" /><label for="rb3">возобновить показы</label>
	</div>
	<div class="buttons">
		<div class="yes">Сохранить</div><div class="no close">Отмена</div>
	</div>
</div>
<div id="admin-allow">
	<div class="header"></div>
	<div class="message">
		<input type="radio" name="spec" value="0" alt="НЕТ" id="rbs0" /><label for="rbs0">снять</label><br />
		<input type="radio" name="spec" value="1" alt="ДА" id="rbs1" /><label for="rbs1">установить</label><br />
	</div>
	<div class="buttons">
		<div class="yes">Сохранить</div><div class="no close">Отмена</div>
	</div>
</div>
<?php echo CHtml::link('Сбросить сортировку', array('adminitems/view', 'page'=>$Cpage)); ?>
<table class="sample">
	<tr>
		<th>Создан <?php echo CHtml::link('&uarr;', array('adminitems/view', 'page'=>$Cpage, 'sort'=>'created')); ?>&nbsp;<?php echo CHtml::link('&darr;', array('adminitems/view', 'page'=>$Cpage, 'sort'=>'created', 'type'=>'desc')); ?>
		</th>
		<th>Статус <?php echo CHtml::link('&uarr;', array('adminitems/view', 'page'=>$Cpage, 'sort'=>'status')); ?>&nbsp;<?php echo CHtml::link('&darr;', array('adminitems/view', 'page'=>$Cpage, 'sort'=>'status', 'type'=>'desc')); ?>
		</th>
		<th>Объект</th>
		<th>Тип помещ. <?php echo CHtml::link('&uarr;', array('adminitems/view', 'page'=>$Cpage, 'sort'=>'kind')); ?>&nbsp;<?php echo CHtml::link('&darr;', array('adminitems/view', 'page'=>$Cpage, 'sort'=>'kind', 'type'=>'desc')); ?>
		</th>
		<th>Владелец <?php echo CHtml::link('&uarr;', array('adminitems/view', 'page'=>$Cpage, 'sort'=>'login')); ?>&nbsp;<?php echo CHtml::link('&darr;', array('adminitems/view', 'page'=>$Cpage, 'sort'=>'login', 'type'=>'desc')); ?>
		</th>
		<th>Адрес</th>
		<th>Показы</th>
		<th>Уд.</th>
	</tr>
<?php
		foreach ($rows as $row)
			$this->renderPartial('_row', array('obj' =>$row));
?>
<?php endif; ?>
</table>
<div align="center">
<?php
  $this->widget('CLinkPager',array(
		'pages'          =>$pages,
		'maxButtonCount' =>5, # максимальное колличество вкладок на странице
		'header'         =>'<b>Перейти к странице:</b><br>', # заголовок
		'nextPageLabel'  =>'Вперед&gt;', # название кнопок навигаций next и prev
		'prevPageLabel'  =>'&lt;Назад',
  ));
?>
</div>
<?php endif; ?>
