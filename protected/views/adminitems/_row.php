<?php switch ($obj->kind) {
	case 1 : {$type = 'торговое'; break;}
	case 2 : {$type = 'склад\произв'; break;}
	default : $type = 'офис'; 
}
switch ($obj->status) {
	case 0 : {$status='деактивирован'; break;}
	case 1 : {$status = 'отменен'; break;}
	//case 2 : {$status = 'сдан'; break;}
	case 3 : {$status='на модерации'; break;}
	//case 5 : {$status = 'приостановлен'; break;}
	case 6 : {$status='идут показы'; break;}
	default: {$status = 'Не известен'; }
}
switch ($obj->itemType) {
	case 1 : {$iType='центр'; break;}
	case 2 : {$iType=CHtml::link('помещение', array('control/obj_detail', 'id'=>$obj->parent)); break;}
	default: {$iType='обычный'; }
}
?>
<tr>
	<td><?php $get = $this->widget('DateFormat', array('kind'=>5,'dbDate'=>$obj->created)); echo $get->date; ?></td>
	<td><?php echo CHtml::link('<span id="'.$obj->id.'">'.$status.'</span>', array('#'), array('class'=>'change')); ?><br /><?php echo CHtml::link('Спец', array('#'), array('class'=>'chspec', 'id'=>'ln'.$obj->id)); ?>: <span id="sp<?php echo $obj->id?>"><?php echo $obj->special?'ДА':'НЕТ';?></span></td>
	<td><?php echo $iType; ?></td>
	<td><?php echo $type; ?></td>
	<td><?php if (isset($obj->login)) echo CHtml::link($obj->login.' ('.$obj->firstname.' '.$obj->lastname.')', array('adminusers/detail', 'id'=>$obj->owner_id)); else echo 'Аноним'; ?></td>
	<td><?php echo CHtml::link($obj->city.': '.$obj->district.' / '.$obj->street.', '.$obj->house.'('.$obj->housing.')', array('control/obj_detail', 'id'=>$obj->id)); ?></td>
	<td><?php echo $obj->showQty; ?></td>
	<td class="action"><?php echo CHtml::link('D', array('adminitems/delete', 'id'=>$obj->id)); ?></td>
</tr>
