<?php $s = ' помещение';
switch ($info->kind) {
	case 1: {$s = 'торговое'.$s; break;}
	case 2: {$s = 'складское\производственное'.$s; break;}
	default: $s = 'офис';
}
?>
<a href="javascript: history.go(-1);">&larr; Назад</a> | <?php echo CHtml::link('список поисковых запросов', array('adminsearchstatbrowse/view')); ?>
<h1>Поисковый запрос</h1>
<b>Посетитель: </b><?php if ($info->who_id==0) echo 'Гость'; else echo CHtml::link($info->whoSoName.' '.$info->whoName.' '.$info->whoOtchestvo, array('person/card', 'id'=>$info->who_id)); ?><br />
<b>Время поиска: </b><?php $get = $this->widget('DateFormat', array('kind'=>5,'dbDate'=>$info->created)); echo $get->date; ?><br />
<b>Вкладка: </b><?php echo $s;?><br />
<?php	$s = '';
	if (isset($info->squareMin)) {
		if (isset($info->squareMax))
				$s = $info->squareMin.'-'.$info->squareMax;
		else	$s = 'от '.$info->squareMin;
	} elseif (isset($info->squareMax))
			$s = 'до '.$info->squareMax;
	if ($s=='') $s = '<i>не указана</i>';
	else $s.= ' м<sup>2</sup>';?>
<b>Площадью:</b> <?php echo $s; ?><br />
<?php $s = '';
	if (isset($info->priceMin)) {
		if (isset($info->priceMax))
				$s = $info->priceMin.'-'.$info->priceMax;
		else	$s = 'от '.$info->priceMin;
	} elseif (isset($info->priceMax))
			$s = 'до '.$info->priceMax;
	if ($s=='') $s = '<i>не указана</i>';
	else $s.= ' руб.';?>
<b>По цене:</b> <?php echo $s; ?>
<style type="text/css">
table.sample {
  border-spacing: 2px;
  border-collapse: collapse;
  width: 50%;
  text-align: left;
}
table.sample td {
  border: 1px solid #EEEEEE;
  padding: 5px;
}
table.sample td.td_01 {
  width: 60%;
}
</style>
<table class="sample">
	<tr><td class="td_01">Город</td><td><?php echo $info->city;?></td></tr>
	<tr class="back_tr"><td class="td_01">Район</td><td><?php echo $info->district;?></td></tr>
	<tr><td class="td_01">Улица</td><td><?php echo $info->street;?></td></tr>
<?php $s = 'не важен';
	if (isset($info->storeyMin)) {
		if (isset($info->storeyMax))
				$s = $info->storeyMin.' - '.$info->storeyMax;
		else	$s = 'от '.$info->storeyMin;
	} elseif (isset($info->storeyMax))
			$s = 'до '.$info->storeyMax;
?>
	<tr class="back_tr"><td class="td_01">Этаж</td><td><?php echo $s; ?></td></tr>

	<tr><td class="td_01">Тип сооружения</td><td><?php echo $info->kind_structure;?></td></tr>
	<tr class="back_tr"><td class="td_01">Функциональность</td><td><?php echo $info->functionality;?></td></tr>
	<tr><td class="td_01">Класс объекта</td><td><?php echo $info->class;?></td></tr>

	<tr><td class="td_01">Состояние ремонта</td><td><?php echo $info->repair;?></td></tr>
	<tr class="back_tr"><td class="td_01">Парковка</td><td><?php echo $info->parking;?></td></tr>
	<tr><td class="td_01">Наличие Интернет</td><td><?php echo $info->internet;?></td></tr>
	<tr class="back_tr"><td class="td_01">Телефония</td><td><?php echo $info->telephony;?></td></tr>
	<tr><td class="td_01">Система отопления и водоснабжения</td><td><?php echo $info->heating;?></td></tr>
	<tr class="back_tr"><td class="td_01">Пожарная сигнализация</td><td><?php echo $info->firefighting;?></td></tr>
	<tr><td class="td_01">Грузовой лифт</td><td><?php echo $info->service_lift;?></td></tr>

	<tr><td class="td_01">Кран-балка</td><td><?php echo $info->cathead;?></td></tr>
	<tr class="back_tr"><td class="td_01">Система вентиляции</td><td><?php echo $info->ventilation;?></td></tr>
	<tr><td class="td_01">Пандус</td><td><?php echo $info->rampant;?></td></tr>
	<tr class="back_tr"><td class="td_01">Автомобильные подъездные пути</td><td><?php echo $info->autoways;?></td></tr>
	<tr><td class="td_01">Железнодорожные подъездные пути</td><td><?php echo $info->railways;?></td></tr>
	<tr class="back_tr"><td class="td_01">Высота ворот</td><td>от: <?php if (isset($info->gates_height1)) echo $info->gates_height1; else echo '?';?> до: <?php if (isset($info->gates_height2)) echo $info->gates_height2; else echo '?';?></td></tr>
	<tr><td class="td_01">Высота потолков</td><td>от <?php if (isset($info->ceiling_height1)) echo $info->ceiling_height1; else echo '?';?> до <?php if (isset($info->ceiling_height2)) echo $info->ceiling_height2; else echo '?';?></td></tr>
</table>
