<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/form.css'); ?>
Фильтры: <?php echo CHtml::link('Сбросить', array('adminsearchstatbrowse/unset')); ?><br />
<div class="form">
	<?php echo CHtml::form(); ?>
	<?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:'); ?>
	<strong><?php echo CHtml::activeLabel($form, 'city'); ?>:</strong> <?php echo CHtml::dropDownList('AdminItems_form[city]', $city, $Cities); ?>
	<strong><?php echo CHtml::activeLabel($form, 'kind'); ?>:</strong> <?php echo CHtml::dropDownList('AdminItems_form[kind]', $kind, $Kinds); ?>
	<strong><?php echo CHtml::activeLabel($form, 'owner'); ?>:</strong> <?php echo CHtml::dropDownList('AdminItems_form[owner]', $owner, $Users); ?>
	<div class="row buttons">
		<br /><?php echo CHtml::submitButton('Найти', array('id'=>'submit')); ?><br />
	</div>
	<?php echo CHtml::endForm(); ?>
</div><!--form-->
<?php if (is_array($rows)):?>
<hr>Удовлетворяет фильтру: <?php echo $foundQty; ?> строк<br /><br />
<?php if (!empty($rows)):?>
<style type="text/css">
table.sample {
  border-spacing: 2px;
  border-collapse: collapse;
  width: 100%;
  text-align: left;
}
table.sample th {
  border: 1px solid #000000;
  padding: 5px;
  background-color: #aaaaaa;
  text-align: center;
}
table.sample td {
  border: 1px solid #000000;
  padding: 5px;
}
table.sample td.action {
  text-align: center;
}
</style>
<?php echo CHtml::link('Сбросить сортировку', array('adminsearchstatbrowse/view', 'page'=>$Cpage)); ?>
<table class="sample">
	<tr>
		<th rowspan="2">Создан <?php echo CHtml::link('&uarr;', array('adminsearchstatbrowse/view', 'page'=>$Cpage, 'sort'=>'created')); ?>&nbsp;<?php echo CHtml::link('&darr;', array('adminsearchstatbrowse/view', 'page'=>$Cpage, 'sort'=>'created', 'type'=>'desc')); ?>
		</th>
		<th rowspan="2">Вкладка <?php echo CHtml::link('&uarr;', array('adminsearchstatbrowse/view', 'page'=>$Cpage, 'sort'=>'kind')); ?>&nbsp;<?php echo CHtml::link('&darr;', array('adminsearchstatbrowse/view', 'page'=>$Cpage, 'sort'=>'kind', 'type'=>'desc')); ?>
		</th>
		<th rowspan="2">Город, район</th>
		<th colspan="2">Цена</th>
		<th colspan="2">Площадь</th>
		<th rowspan="2">Юзер</th>
	</tr>
	<tr>
		<th>от</th>
		<th>до</th>
		<th>от</th>
		<th>до</th>
	</tr>
<?php
	foreach ($rows as $row)
		$this->renderPartial('_row', array('obj' =>$row));
?>
<?php endif; ?>
</table>
<div align="center">
<?php
  $this->widget('CLinkPager',array(
		'pages'          =>$pages,
		'maxButtonCount' =>5, # максимальное колличество вкладок на странице
		'header'         =>'<b>Перейти к странице:</b><br>', # заголовок
		'nextPageLabel'  =>'Вперед&gt;', # название кнопок навигаций next и prev
		'prevPageLabel'  =>'&lt;Назад',
  ));
?>
</div>
<?php endif; ?>
