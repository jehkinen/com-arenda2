<?php $this->widget('CabinetMenu', array('active'=>'setts'));?>
<div class="content_wrap">
<?php @require_once('protected/views/layouts/rightColumn02.php');?>
	<div class="content">
<?php echo CHtml::form('', 'post', array('class'=>'lc_form'));?>
		<div class="easy_notify">
			<div>
				<h1>Очень удобно! Совсем не больно и абсолютно бесплатно!</h1>
				<p>SMS и e-mail уведомления -- Помогут вам оперативно реагировать на ситуацию. Например, при поступлении объекта по вашей заявке, вам придет SMS или (и) письмо на электронную почту.</p>
				<p>При деактивации (удалении) объекта из вашего блокнота вам также придет уведомление.</p>
				<p>В любое время вы можете отключить уведомления.</p>
			</div>
		</div>
		<div class="lc_form_group">
			<h1>Настройка уведомлений</h1><br/>
<?php if($tenant):?>
				<?php echo CHtml::activeCheckBox($form_options, 'info_order', array('checked'=>$user->info_order, 'class'=>'checkbox'));?> <?php echo CHtml::activeLabel($form_options, 'info_order');?><br />
				<?php echo CHtml::activeCheckBox($form_options, 'auto_notepad', array('checked'=>$user->auto_notepad, 'class'=>'checkbox'));?> <?php echo CHtml::activeLabel($form_options, 'auto_notepad');?><br />
				<?php echo CHtml::activeCheckBox($form_options, 'info_moder', array('checked'=>$user->info_moder, 'class'=>'checkbox'));?> <?php echo CHtml::activeLabel($form_options, 'info_moder');?><br />
				<?php echo CHtml::activeCheckBox($form_options, 'info_deactivate', array('checked'=>$user->info_deactivate, 'class'=>'checkbox'));?> <?php echo CHtml::activeLabel($form_options, 'info_deactivate');?><br />
<?php else:?>
				<?php echo CHtml::activeCheckBox($form_options, 'info_moder', array('checked'=>$user->info_moder, 'class'=>'checkbox'));?> <?php echo CHtml::activeLabel($form_options, 'info_moder');?><br />
				<?php echo CHtml::activeCheckBox($form_options, 'info_order', array('checked'=>$user->info_order, 'class'=>'checkbox'));?> <?php echo CHtml::activeLabel($form_options, 'info_order');?><br />
<?php endif;?>
			<h2>Контакты для отправки уведомлений</h2><br/>
				<?php echo CHtml::activeCheckBox($form_options, 'info_flag_phone', array('checked'=>$user->info_flag_phone, 'class'=>'checkbox'));?> <?php echo CHtml::activeLabel($form_options, 'info_flag_phone');?>&nbsp;&nbsp;&nbsp;
				<?php echo CHtml::activeCheckBox($form_options, 'info_flag_email', array('checked'=>$user->info_flag_email, 'class'=>'checkbox'));?> <?php echo CHtml::activeLabel($form_options, 'info_flag_email');?><br />
				<div class="verify_fields">
					SMS на номер +7&nbsp;<?php echo CHtml::activeTextField($form_options, 'tel_part1', array('value'=>$tel1, 'size'=>3, 'maxlength'=>3)).CHtml::activeTextField($form_options, 'tel_part2', array('value'=>$tel2, 'size'=>10, 'maxlength'=>7));?><br/>
					e-mail&nbsp;&nbsp;<?php echo CHtml::activeTextField($form_options, 'info_email', array('value'=>$user->info_email, 'size'=>25, 'maxlength'=>39));?>
				</div>
				<div class="verify_notes hide">
					<span class="ok">поле заполнено верно</span><br/><span class="err">ошибка</span>
				</div>
				<div class="verify_errors<?php if (!$wasSend || $settsSaved) echo ' hide';?>"><?php echo CHtml::errorSummary($form_options, '');?></div><br />
				<?php echo CHtml::submitButton('Сохранить', array('class'=>'green_link_rounded'));?><span class="<?php if (!$settsSaved) echo 'hide';?>">Данные сохранены</span>
		</div>
<?php echo CHtml::endForm();?>
<?php echo CHtml::form('', 'post', array('class'=>'lc_form')); 
$blur = 'if (this.value == '."''".') {this.value = '."'atr';}"; $focus = 'if (this.value == '."'atr'".') {this.value = '."'';}";
?>
		<div class="lc_form_group">
			<h1>Контактные данные</h1><i>-&nbsp; видны посетителям сайта</i><br/>
			<div class="verify_fields">
				<?php $s1=str_replace('atr', $Attrs[2], $blur); $s2=str_replace('atr', $Attrs[2], $focus); echo CHtml::activeTextField($form_info, 'name', array('value'=>$user->name, 'size'=>28, 'maxlength'=>30, 'onblur'=>$s1, 'onfocus'=>$s2));?><br/>
				<?php $s1=str_replace('atr', $Attrs[1], $blur); $s2=str_replace('atr', $Attrs[1], $focus); echo CHtml::activeTextField($form_info, 'so_name', array('value'=>$user->so_name, 'size'=>28, 'maxlength'=>30, 'onblur'=>$s1, 'onfocus'=>$s2));?><br/>
				<?php $s1=str_replace('atr', $Attrs[3], $blur); $s2=str_replace('atr', $Attrs[3], $focus); echo CHtml::activeTextField($form_info, 'otchestvo', array('value'=>$user->otchestvo, 'size'=>28, 'maxlength'=>30, 'onblur'=>$s1, 'onfocus'=>$s2));?><br/>
				<?php $s1=str_replace('atr', $Attrs[5], $blur); $s2=str_replace('atr', $Attrs[5], $focus); echo CHtml::activeTextField($form_info, 'e_mail', array('value'=>$user->e_mail, 'size'=>28, 'maxlength'=>30, 'onblur'=>$s1, 'onfocus'=>$s2));?><br/>
				<?php $s1=str_replace('atr', $Attrs[4], $blur); $s2=str_replace('atr', $Attrs[4], $focus); echo CHtml::activeTextField($form_info, 'phone', array('value'=>$user->phone, 'size'=>28, 'maxlength'=>15, 'onblur'=>$s1, 'onfocus'=>$s2));?>
			</div>
			<div class="verify_notes hide">
				<span class="ok">поле заполнено верно</span><br/>
				<span class="err">ошибка</span><br/>
				<span class="ok">поле заполнено верно</span><br/>
				<span class="err">ошибка</span><br/>
				<span class="ok">поле заполнено верно</span>
			</div>
			<div class="verify_errors<?php if (!$wasSend || $infoSaved) echo ' hide';?>"><?php echo CHtml::errorSummary($form_info, '');?></div><br />
			<?php echo CHtml::submitButton('Сохранить', array('class'=>'green_link_rounded'));?><span class="<?php if (!$infoSaved) echo 'hide';?>">Данные сохранены</span>
		</div>
<?php echo CHtml::endForm();?>
<?php echo CHtml::form('', 'post', array('class'=>'lc_form'));?>
		<div class="lc_form_group">
			<h1>Сменить пароль доступа</h1><br/>
			<div class="verify_fields">
				<?php echo CHtml::activePasswordField($form_pass, 'password', array('size'=>28, 'maxlength'=>10, 'onblur'=>'if (this.value == "") {this.value = "'.$Attrs[7].'"; this.type = "text";}', 'onfocus'=>'if (this.value == "'.$Attrs[7].'") {this.value = "";this.type = "password";}'));?><br/>
				<?php echo CHtml::activePasswordField($form_pass, 'confirm', array('size'=>28, 'maxlength'=>10, 'onblur'=>'if (this.value == "") {this.value = "'.$Attrs[8].'";this.type="text";}', 'onfocus'=>'if (this.value == "'.$Attrs[8].'") {this.value = "";this.type="password";}'));?>
			</div>
			<div class="verify_notes hide">
				<span class="ok">поле заполнено верно</span><br/><span class="err">ошибка</span>
			</div>
			<div class="verify_errors<?php if (!$wasSend || $passSaved) echo ' hide';?>"><?php echo CHtml::errorSummary($form_pass, '');?></div><br />
			<?php echo CHtml::submitButton('Сохранить', array('class'=>'green_link_rounded'));?><span class="<?php if (!$passSaved) echo 'hide';?>">Данные сохранены</span>
		</div>
<?php echo CHtml::endForm();?>
	</div><!-- /content -->
</div><!-- /content_wrap -->
