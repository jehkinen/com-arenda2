<?php $this->widget('CabinetMenu', array('active'=>'page'));?>
<div class="content_wrap">
<?php @require_once('protected/views/layouts/rightColumn02.php');?>
	<div class="content">
<?php echo CHtml::form('', 'post', array('class'=>'lc_form')); ?>
		<div class="easy_notify">
			<div>
				<h1>Очень удобно! Совсем не больно и абсолютно бесплатно!</h1>
				<p>SMS и e-mail уведомления -- Помогут вам оперативно реагировать на ситуацию. Например, при поступлении объекта по вашей заявке, вам придет SMS или (и) письмо на электронную почту.</p>
				<p>При деактивации (удалении) объекта из вашего блокнота вам также придет уведомление.</p>
				<p>В любое время вы можете отключить уведомления.</p>
			</div>
		</div>
		<div class="lc_form_group">
			<!--
			<div class="mypage_photo">
			<img src="images/client_sob_photo.jpg"/><br/>
			<a href>Удалить</a>
			</div>&nbsp;<input type="file"/><br/>
			-->
			<h1>Название раздела</h1>
			<?php echo CHtml::activeTextField($form_page, 'parag1_name', array('size'=>25, 'maxlength'=>128));?>
			<?php echo CHtml::textArea('User_page[parag1_text]', $vtext1, array('rows'=>8)); ?>
			<h1>Название раздела</h1>
			<?php echo CHtml::activeTextField($form_page, 'parag2_name', array('size'=>25, 'maxlength'=>128));?>
			<?php echo CHtml::textArea('User_page[parag2_text]', $vtext2, array('rows'=>8)); ?>
			<div class="<?php if (!$wasSend || $paragSaved) echo ' hide';?>"><?php echo CHtml::errorSummary($form_page, 'Исправьте пожалуйста следующие ошибки:');?></div><br />
			<?php echo CHtml::submitButton('Сохранить', array('class'=>'green_link_rounded'));?><span class="<?php if (!$paragSaved) echo 'hide';?>">Данные сохранены</span>
		</div>
<?php echo CHtml::endForm(); ?>
<?php echo CHtml::form('', 'post', array('class'=>'lc_form')); 
$blur = 'if (this.value == '."''".') {this.value = '."'atr';}"; $focus = 'if (this.value == '."'atr'".') {this.value = '."'';}";
?>
		<div class="lc_form_group">
			<h1>Дополнительные контакты</h1><br/>
			<?php $s1=str_replace('atr', $Attrs[1], $blur); $s2=str_replace('atr', $Attrs[1], $focus); echo CHtml::activeTextField($form_addc, 'city', array('size'=>25, 'maxlength'=>31, 'onblur'=>$s1, 'onfocus'=>$s2));?><br />
			<?php $s1=str_replace('atr', $Attrs[2], $blur); $s2=str_replace('atr', $Attrs[2], $focus); echo CHtml::activeTextField($form_addc, 'skype', array('size'=>25, 'maxlength'=>31, 'onblur'=>$s1, 'onfocus'=>$s2));?><br />
			<?php $s1=str_replace('atr', $Attrs[3], $blur); $s2=str_replace('atr', $Attrs[3], $focus); echo CHtml::activeTextField($form_addc, 'icq', array('size'=>25, 'maxlength'=>31, 'onblur'=>$s1, 'onfocus'=>$s2));?><br />
			<div class="<?php if (!$wasSend || $addcSaved) echo ' hide';?>"><?php echo CHtml::errorSummary($form_addc, 'Исправьте пожалуйста следующие ошибки:');?></div><br />
			<?php echo CHtml::submitButton('Сохранить', array('class'=>'green_link_rounded'));?><span class="<?php if (!$addcSaved) echo 'hide';?>">Данные сохранены</span>
		</div>
<?php echo CHtml::endForm(); ?>
	</div><!-- /content -->
</div><!-- /content_wrap -->
