<link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/vader/jquery-ui.css" rel="stylesheet" />
<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/jcrop/jquery.Jcrop.min.css'); ?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo $this->createAbsoluteUrl('/js/ajaxupload.3.6.js'); ?>"></script>
<script type="text/javascript" src="<?php echo $this->createAbsoluteUrl('/js/jquery.Jcrop.min.js'); ?>"></script>
<script type="text/javascript">
$(document).ready(function() {
	function showCoords(c){
		$('#x1').val(Math.round(c.x));
		$('#y1').val(Math.round(c.y));
		$('#x2').val(Math.round(c.x2));
		$('#y2').val(Math.round(c.y2));
	};
	
<?php /*
	var button = $('#loadImg');
*/ ?>
	var button = $('#loadImg');
	var info   = $('#str');
	var ibox = $('#img_box');

	new AjaxUpload(button, {
		action: 'ajax/u_photo_load/',
		name: 'myfile',
		onSubmit : function(file, ext) {
			if (ext && /^(jpg|png|jpeg|gif|JPG|PNG|JPEG|GIF)$/.test(ext)) {
				this.disable();
<?php /*
				button.addClass('disable'); 
*/ ?>
				info.removeClass().addClass('loading').text('Загрузка');
			} else {
				alert('Неверный тип картинки, допускается только: jpg, jpeg, png, gif');
				return false;
			}
		},
		onComplete: function(file, response) {
			var action = this;
			if (response != false){
				var data = response.split('|');
				var file_name = data[0];
				$('#load-here').append(
					jQuery('<img />')
						.attr({
							src: 'storage/images/personal/temp/' + file_name,
							id:  'crop-image',
						})
				);
				info.removeClass().addClass('resize').text('Редактирование');
				$('#crop-box').dialog({
					modal: true,
					width: (parseInt(data[1])+34) + 'px',
					title: 'Редактирование картинки',
					close: function() {
						info.removeClass().addClass('cancel').text('Отменено пользователем');
						action.enable();
<?php /*
						//button.removeClass('disable');
*/ ?>
						$('#crop-box').hide();
						$('#load-here').empty();
						$.ajax({
							url: 'ajax/u_photo_delete/',
							data: {
								file: file_name,
								type: 'temp'
							},
							type: 'post'
						});
					},
					resizable: false,
					position:['middle', 100]
				});
				$('#crop-image').Jcrop({
					setSelect: [10, 10, 318, 241],
					minSize: [308, 231], 
					aspectRatio: 4/3,
					allowSelect: false,
					bgColor: '#ffffff',
					bgOpacity: .5,
					onChange: showCoords,
					onSelect: showCoords
				});
				$('#save').click(function() {
					$('#crop-box').remove();
					info.removeClass().addClass('chek').text('Загруженно: ' + file);
					$.ajax({
						url: 'ajax/u_photo_crop/',
						data: {
							file: file_name,
							x1: $('#x1').val(),
							y1: $('#y1').val(),
							x2: $('#x2').val(),
							y2: $('#y2').val()
						},
						type: 'post',
						success: function(){
							ibox.empty()
							.append(
								jQuery('<img />')
									.attr({
										src: 'storage/images/personal/' + file_name,
										alt: file_name,
										id:	u_img
									})
									
							)
							.append('<br />')
							.append(
								jQuery('<a />')
									.attr({
										id: 'u_img_delete',
										href: 'javascript://'
									})
									.text('удалить')
									.bind('click', function(e) {
										e.preventDefault();
										$.ajax({
											url: 'ajax/u_photo_delete/',
											data: {
												file: file_name,
												type: 'user'
											},
											type: 'post',
											success: function(){
												info.removeClass().text('Фотография удалена');
<?php /*
												//ibox.empty().text('Фотография удалена');
*/ ?>
												ibox.empty()
												.append(
													jQuery('<img />')
														.attr({
															src: 'images/add_photo.jpg',
															id:	loadImg
														})
														.addClass('button')
												)
											}
										});
										return false;
									})
							);
						}
					});
				});
			}else{
				info.removeClass().text('');
				alert('Размер картинки слишком большой, разрешенно не более 6Мб');
				action.enable();
<?php /*
				//button.removeClass('disable');
*/ ?>
			}
		}
	});

	$('#u_img_delete').click(function() {
		var file_name = $('#u_img').attr('alt');
		$.ajax({
			url: 'ajax/u_photo_delete/',
			data: {
				file: file_name,
				type: 'user'
			},
			type: 'post',
			success: function(){
<?php /*
				//button.text('Загрузить');
*/ ?>
				info.removeClass().text('Фотография удалена');
<?php /*
				//ibox.empty().text('Фотография удалена');
*/ ?>
				ibox.empty()
				.append(
					jQuery('<img />')
						.attr({
							src: 'images/add_photo.jpg',
							id:	loadImg
						})
						.addClass('button')
					)
			}
		});
		return false;
	});
});
</script>

<!-- / -->


<?php $this->widget('CabinetMenu', array('active'=>'page'));?>
<div class="content_wrap">
<?php @require_once('protected/views/layouts/rightColumn02.php');?>
	<div class="content">
<?php echo CHtml::form('', 'post', array('class'=>'')); ?>
	<div class="form01_grp">
		<div class="form01_blk">
			<h1>Информация</h1>
			<div id="str"></div>
			<div id="img_box">
				<?php if ($uphoto == ''): ?>
					<div id="loadImg" class="button"><img src="images/add_photo.jpg"/></div>
				<?php endif; ?>
				<?php if ($uphoto != ''): ?>
					<img id="u_img" src="storage/images/personal/<?php echo $uphoto; ?>" alt="<?php echo $uphoto; ?>" /><br />
					<a id="u_img_delete" href="javascript://">удалить</a>
				<?php endif; ?>
			</div>
			<input type="hidden" id="x1" name="x1" />
			<input type="hidden" id="y1" name="y1" />
			<input type="hidden" id="x2" name="x2" />
			<input type="hidden" id="y2" name="y2" />
			<div class="m_0010b m_0010t">
				<div class="input01_lbl w120">Название раздела</div><?php echo CHtml::activeTextField($form_page, 'parag1_name', array('class'=>'input01 tb w325', 'size'=>25, 'maxlength'=>128));?>
			</div><?php echo CHtml::textArea('User_page[parag1_text]', $vtext1, array('class'=>'h90 m_0015b textarea01 w440', 'rows'=>8)); ?>
			<div class="m_0010b">
				<div class="input01_lbl w120">Название раздела</div><?php echo CHtml::activeTextField($form_page, 'parag2_name', array('class'=>'input01 tb w325', 'size'=>25, 'maxlength'=>128));?>
			</div><?php echo CHtml::textArea('User_page[parag2_text]', $vtext2, array('class'=>'h90 m_0005b textarea01 w440', 'rows'=>8)); ?>
			<div class="<?php if (!$wasSend || $paragSaved) echo ' hide';?>"><?php echo CHtml::errorSummary($form_page, 'Исправьте пожалуйста следующие ошибки:');?></div><br />
		</div><div class="note01">
			<h1>Очень удобно! Совсем не больно и абсолютно бесплатно!</h1>
			<p>SMS и e-mail уведомления -- Помогут вам оперативно реагировать на ситуацию. Например, при поступлении объекта по вашей заявке, вам придет SMS или (и) письмо на электронную почту.</p>
			<p>При деактивации (удалении) объекта из вашего блокнота вам также придет уведомление.</p>
			<p>В любое время вы можете отключить уведомления.</p>
		</div>
	</div><div class="form01_grp">
		<div class="form01_blk">
			<h1>Дополнительные контакты</h1>
			<div class="m_0015b">
				<div class="input01_lbl w120">Город</div><?php echo CHtml::activeTextField($form_addc, 'city', array('class'=>'input01 w325', 'size'=>25, 'maxlength'=>31));?>
			</div><div class="m_0015b">
				<div class="input01_lbl w120">Skype</div><?php echo CHtml::activeTextField($form_addc, 'skype', array('class'=>'input01 w325', 'size'=>25, 'maxlength'=>31));?>
			</div><div class="m_0005b">
				<div class="input01_lbl w120">ICQ</div><?php echo CHtml::activeTextField($form_addc, 'icq', array('class'=>'input01 w325', 'size'=>25, 'maxlength'=>31));?>
			</div>
		</div><div class="note01">
			<p class="<?php if (!$addcSaved) echo 'hide';else echo 'done';?>">Данные сохранены</p>
			<p class="<?php if (!$wasSend || $addcSaved) echo ' hide';?>"><?php echo CHtml::errorSummary($form_addc, 'Исправьте пожалуйста следующие ошибки:');?></p>
			<p class="<?php if ($addcSaved) echo 'hide';?>">Укажите контактные данные для связи с вами.</p>
		</div>
	</div><div class="form01_grp">
		<div class="form01_blk bFFF">
		<label class="checkbox01_lbl dl m_0010t w350">
			<input class="checkbox01" name="AgreeRules" type="checkbox" checked=true /> Я хочу создать свою персональную страницу на портале, &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;прочитал и полностью согласен с <a href="/page/rules.html">правилами</a>
		</label><?php echo CHtml::submitButton('Создать', array('class'=>'button02 m_0015t'));?>
	</div></div>
<?php echo CHtml::endForm(); ?>

		

		<!-- Hidden dialog -->
		<div style="display:none;" id="crop-box">
			<div id="dialog">
				<div id="load-here"></div>
				<button id="save" style="margin-top:10px;">Cохранить</button>
			</div>
		</div>
		<!-- /Hidden dialog -->
		
		
		
	</div><!-- /content -->
</div><!-- /content_wrap -->
