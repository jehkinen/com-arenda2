<?php echo 
	CHtml::cssFile(Yii::app()->baseUrl.'/css/modal.css').
	CHtml::scriptFile($bUrl.'/js/jquery.simplemodal.js').
	CHtml::scriptFile($bUrl.'/js/tenant.js');
?>
<div id="owner-delete">
	<div class="header"></div>
	<div class="message"></div>
	<div class="buttons">
		<div class="yes">Да</div><div class="no close">Нет</div>
	</div>
</div>
<?php $this->widget('CabinetMenu', array('active'=>'notepad'));?>
		<div class="content_wrap">
<?php @require_once('protected/views/layouts/rightColumn02.php');?>
		<div class="content">
			<div class="object_table" id="search_result">
<?php if (empty($Rows)):?>
<div class="story">
	<h2>Блокнот пуст</h2>
	<p>Вы можете поместить объекты в блокнот кликнув по ссылке "Действие" на странице с результатами поиска или на странице с детальным описанием объекта.</p>
</div>
<?php else:?>
<?php $i=1; foreach ($Rows as $r){
		$b = ++$i % 2;
		$this->renderPartial('_notepad', array(
			'row' =>$r,
			'odd' =>$b
		));
	} ?>
<?php endif;?>
			</div>
<?php $this->widget('CLinkComarendaPager',array(
		'pages'				=>$pages,
		'maxButtonCount'	=>5, # максимальное колличество вкладок на странице
		'header'			=>'', # заголовок
		'nextPageLabel'		=>'',
		'prevPageLabel'		=>'',
		'firstPageLabel'	=>'',
		'lastPageLabel'		=>''
	));
?>
		</div><!-- /content -->
	</div><!-- /content_wrap -->
