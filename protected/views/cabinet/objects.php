<?php
	$bUrl = Yii::app()->baseUrl;
	echo
	CHtml::cssFile($bUrl.'/css/modal.css').
	CHtml::scriptFile($bUrl.'/js/jquery.simplemodal.js').
	CHtml::scriptFile($bUrl.'/js/owner.js');
?>
<div id="owner-change">
	<div class="header"></div>
	<div class="message">
		<input type="radio" name="stat" value="1" alt="показы отменены" id="rb0" /><label for="rb0">отменить показы</label><br />
		<input type="radio" name="stat" value="6" alt="идут показы" id="rb3" /><label for="rb3">возобновить показы</label>
	</div>
	<div class="buttons">
		<div class="yes">Принять</div><div class="no close">Отмена</div>
	</div>
</div>
<div id="owner-delete">
	<div class="header"></div>
	<div class="message"></div>
	<div class="buttons">
		<div class="yes">Да</div><div class="no close">Нет</div>
	</div>
</div>

<?php $this->widget('CabinetMenu', array('active'=>'objects'));?>
		<div class="content_wrap">
<?php @require_once('protected/views/layouts/rightColumn02.php');?>
		<div class="content">
			 <?//php echo CHtml::link('разместить объекты в нашей Базе Данных', array('object/add/step/step1')); ?>
            Вы можете <?php echo CHtml::link('разместить объекты в нашей Базе Данных', array('object2/create')); ?>
			<div class="object_table" id="search_result">
<?php if (empty($Rows)):?>
<div class="story">
	<h2>У Вас объектов нет</h2>
	<?//php echo CHtml::link('разместить объекты в нашей Базе Данных', array('object/add/step/step1')); ?></p>
	<p>Вы можете <?php echo CHtml::link('разместить объекты в нашей Базе Данных', array('object2/create')); ?></p>
</div>
<?php else:?>
<?php $i=1; foreach ($Rows as $object){
		$b = ++$i % 2;
		$this->renderPartial('_objects', array(
			'obj' =>$object,
			'odd' =>$b
		));
	} ?>
<?php endif;?>
			</div>
<?php $this->widget('CLinkComarendaPager',array(
		'pages'				=>$pages,
		'maxButtonCount'	=>5, # максимальное колличество вкладок на странице
		'header'			=>'', # заголовок
		'nextPageLabel'		=>'',
		'prevPageLabel'		=>'',
		'firstPageLabel'	=>'',
		'lastPageLabel'		=>''
	));
?>
		</div><!-- /content -->
	</div><!-- /content_wrap -->
