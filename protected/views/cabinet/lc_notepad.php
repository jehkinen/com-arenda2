<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>index</title>
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/carousel.css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.jcarousel.js"></script>
<script type="text/javascript" src="js/jquery.lightbox_me.js"></script>
<script type="text/javascript" src="js/client.js"></script>
<style>
#action_list_1{
    position:absolute;
	padding:5px;
	right:0px;
	top:4px;
	text-align:left;
	font-size:14px;
}
#action_list_2{
    position:absolute;
	padding:5px;
	right:0px;
	top:4px;
	text-align:left;
	font-size:14px;
}
#action_list_3{
    position:absolute;
	padding:5px;
	right:0px;
	top:4px;
	text-align:left;
	font-size:14px;
}
#action_list_4{
    position:absolute;
	padding:5px;
	right:0px;
	top:4px;
	text-align:left;
	font-size:14px;
}
#action_list_5{
    position:absolute;
	padding:5px;
	right:0px;
	top:4px;
	text-align:left;
	font-size:14px;
}
#action_list_6{
    position:absolute;
	padding:5px;
	right:0px;
	top:4px;
	text-align:left;
	font-size:14px;
}
</style>
</head>

<body>
<div class="top_bg"></div><div class="wrap_for_max_width">
	<div class="top_wrap">
   		<div class="rounded dotted user_inf">
        	<div><a href="">Выход</a>Екатерина Медведева-Двойная</div>
			<div><a href="">Личный кабинет</a>ООО "Рога и Копыта"</div>
        </div>
        <div class="most_search">
        	Часто ищут:
			<ul>
            	<li><a href="">офисы 15-30 м2</a></li>
                <li><a href="">офисы 31-50 м2</a></li>
                <li><a href="">торговые помещения в центре</a></li>
            </ul>
        </div>
    	<a href="" class="logo"><img src="images/logo.png" /></a>
        <div class="city_select_wrap">
            <a class="city_select popup_link" href="#city_select">Екатеринбург</a>
            <div class="popup" id="city_select">
           		<a class="popup_close"></a>
                <ul class="city_select_list">
                    <li><a>Асбест</a></li>
                    <li><a>Берёзовский</a></li>
                    <li><a>Верхняя Пышма</a></li>
                    <li><a>Екатеринбург</a></li>
                    <li><a>Каменск-Уральский</a></li>
                    <li><a>Краснотурьинск</a></li>
                    <li><a>Нижний Тагил</a></li>
                    <li><a>Новоуральск</a></li>
                    <li><a>Первоуральск</a></li>
                    <li><a>Полевской</a></li>
                    <li><a>Ревда</a></li>
                    <li><a>Серов</a></li>
                    <li class="add_city">
                        Нет вашего города?
                        <a>Добавить...</a>
                    </li>
                </ul>	
            </div>
        </div>
        <div class="inf_block">
        	всего объектов<br />
			в аренду 1460
        </div>
        <div class="inf_block">
        	Телефон в Екатеринбурге<br />
			(343) 344-60-27
        </div>
        <div class="menu_wrap">
            <ul class="main_menu rounded">
                <li><a href="">О проекте</a></li>
                <li><a href="">Контакты</a></li>
                <li><a href="">Справка</a></li>
                <li><a href="">Пункт меню</a></li>
                <li><a href="">Еще пункт</a></li>
            </ul>
            
            <a href="" class="green_link_block">Как работает com-arenda?</a>
            <a href="" class="green_link_block">Разместить объект</a>
        </div>
			</div><div class="clientNav">
				<span class="clientNavTitle">Личный кабинет</span><span class="clientNavIn">
					<span>Блокнот</span><a class="" href="lc_profi_myorders-1.30.00.html" >Мои заявки</a><a class="" href="lc_profi_mypage-1.30.00.html" >Моя страница</a><a class="" href="lc_profi_settings-1.20.00.html" >Настройки</a>
				</span>
			</div><div class="content_wrap">
    	<div class="aside">
            <div class="gray_block">
            	Знаете ли вы, что: Ученые обнаружили следы ранее неизвестной рептилии, обитавшей около 245 миллионов лет назад на территории современной Антарктики.
            </div>
            <div class="gray_block">
            	Почему кошка приносит нам мышей? По мнению кошки, ее хозяева по части ловли мышей являются, говоря школьным языком, настоящими «двоечниками». Но кошка готова оказать нам в этом сложном деле посильную помощь. Сытая кошка делится добычей со своими собратьями.
            </div>
            <div class="gray_block">
            	Член полярной экспедиции Георгия Седова, штурман корабля "Святой Фока" Н. Сахаров, обязан жизнью своей преданной собаке Штурке. Сахаров отморозил руки и добирался до корабля один в сопровождении собаки. Выбиваясь из сил, прошел по льду пятьдесят километров, временами терял сознание, впадая в забытье. Тогда Штурка садился возле хозяина, лаял, теребил его за одежду, пытаясь привести друга в чувства. Сахаров поднимался и продолжал идти.
            </div>
            
        </div>
				<div class="content">
					<div class="object_table">
						<div class="post">
							<div class="image">
								&nbsp;<img src="content_images/search_result.jpg" /><img src="images/hot.png" class="hot tl" />
							</div><span class="object_type">Офис</span><span class="object_date">19.05.2012</span><div class="adress">
								Екатеринбург, Вторчермет, Ул. Радищева, 228,<br/>Сдает: собственик <a href="" class="have">Дьяченко О</a>
							</div>&nbsp;<div class="object_main_info">
								<a href="">112 500 руб/мц.<br/>173 м2,<br/>650 руб./м2</a>
							</div><div class="object_action">
								<a href="#action_list_1" class="popup_link">Действие</a>
							</div><div class="pop_up" id="action_list_1">
								<a href="#">Удалить из блокнота</a><br />
								<a href="#">Скачать карточку</a><br />
								<a href="#">Распечатать</a><br />
								<a href="#">Пожаловаться</a><br />
							</div><div class="object_status active">Активно</div>
						</div><div class="post even">
							<div class="image">
								&nbsp;<img src="content_images/search_result.jpg" /><img src="images/hot.png" class="hot tl" />
							</div><span class="object_type">Складское помещение</span><span class="object_date">19.05.2012</span><div class="adress">
								Екатеринбург, Вторчермет, Ул. Радищева, 228,<br/>Сдает: собственик <a href="" class="have">Дьяченко О</a>
							</div>&nbsp;<div class="object_main_info">
								<a href="">112 500 руб/мц.<br/>173 м2,<br/>650 руб./м2</a>
							</div><div class="object_action">
								<a href="#action_list_2" class="popup_link">Действие</a>
							</div><div class="pop_up" id="action_list_2">
								<a href="#">Удалить из блокнота</a><br />
								<a href="#">Скачать карточку</a><br />
								<a href="#">Распечатать</a><br />
								<a href="#">Пожаловаться</a><br />
							</div><div class="object_status moderating">На модерации</div>
						</div><div class="post">
							<div class="image">
								&nbsp;<img src="content_images/search_result.jpg" /><img src="images/hot.png" class="hot tl" />
							</div><span class="object_type">Торговое помещение</span><span class="object_date">19.05.2012</span><div class="adress">
								Екатеринбург, Вторчермет, Ул. Радищева, 228,<br/>Сдает: собственик <a href="" class="have">Дьяченко О</a>
							</div>&nbsp;<div class="object_main_info">
								<a href="">112 500 руб/мц.<br/>173 м2,<br/>650 руб./м2</a>
							</div><div class="object_action">
								<a href="#action_list_3" class="popup_link">Действие</a>
							</div><div class="pop_up" id="action_list_3">
								<a href="#">Удалить из блокнота</a><br />
								<a href="#">Скачать карточку</a><br />
								<a href="#">Распечатать</a><br />
								<a href="#">Пожаловаться</a><br />
							</div><div class="object_status inactive">Неактивно</div><div class="fog"></div>
						</div><div class="post even">
							<div class="image">
								&nbsp;<img src="content_images/search_result.jpg" /><img src="images/hot.png" class="hot tl" />
							</div><span class="object_type">Офис</span><span class="object_date">19.05.2012</span><div class="adress">
								Екатеринбург, Вторчермет, Ул. Радищева, 228,<br/>Сдает: собственик <a href="" class="have">Дьяченко О</a>
							</div>&nbsp;<div class="object_main_info">
								<a href="">112 500 руб/мц.<br/>173 м2,<br/>650 руб./м2</a>
							</div><div class="object_action">
								<a href="#action_list_4" class="popup_link">Действие</a>
							</div><div class="pop_up" id="action_list_4">
								<a href="#">Удалить из блокнота</a><br />
								<a href="#">Скачать карточку</a><br />
								<a href="#">Распечатать</a><br />
								<a href="#">Пожаловаться</a><br />
							</div><div class="object_status active">Активно</div>
						</div><div class="post">
							<div class="image">
								&nbsp;<img src="content_images/search_result.jpg" /><img src="images/hot.png" class="hot tl" />
							</div><span class="object_type">Складское помещение</span><span class="object_date">19.05.2012</span><div class="adress">
								Екатеринбург, Вторчермет, Ул. Радищева, 228,<br/>Сдает: собственик <a href="" class="have">Дьяченко О</a>
							</div>&nbsp;<div class="object_main_info">
								<a href="">112 500 руб/мц.<br/>173 м2,<br/>650 руб./м2</a>
							</div><div class="object_action">
								<a href="#action_list_5" class="popup_link">Действие</a>
							</div><div class="pop_up" id="action_list_5">
								<a href="#">Удалить из блокнота</a><br />
								<a href="#">Скачать карточку</a><br />
								<a href="#">Распечатать</a><br />
								<a href="#">Пожаловаться</a><br />
							</div><div class="object_status moderating">На модерации</div>
						</div><div class="post even">
							<div class="image">
								&nbsp;<img src="content_images/search_result.jpg" /><img src="images/hot.png" class="hot tl" />
							</div><span class="object_type">Торговое помещение</span><span class="object_date">19.05.2012</span><div class="adress">
								Екатеринбург, Вторчермет, Ул. Радищева, 228,<br/>Сдает: собственик <a href="" class="have">Дьяченко О</a>
							</div>&nbsp;<div class="object_main_info">
								<a href="">112 500 руб/мц.<br/>173 м2,<br/>650 руб./м2</a>
							</div><div class="object_action">
								<a href="#action_list_6" class="popup_link">Действие</a>
							</div><div class="pop_up" id="action_list_6">
								<a href="#">Удалить из блокнота</a><br />
								<a href="#">Скачать карточку</a><br />
								<a href="#">Распечатать</a><br />
								<a href="#">Пожаловаться</a><br />
							</div><div class="object_status inactive">Неактивно</div><div class="fog"></div>
						</div>
					</div>
            
            <ul class="pagination">
            	<li><a href="" class="prev">&nbsp;</a></li>
            	<li><a href="">1</a></li>
                <li><a href="">2</a></li>
                <li><a href="">3</a></li>
                <li><a href="" class="active">4</a></li>
                <li><a href="">5</a></li>
                <li><a href="">6</a></li>
                <li><a href="" class="next">&nbsp;</a></li>
            </ul>
            
            <div class="also_to_view_wrap">
                Возможно вас заинтересуют следующие предложения:
                <ul class="also_to_view">
                	<li>
                        <a href="">
                            <img src="content_images/slider_1.jpg" />
                            <div>Офис<br />Центр, 220м<sup>2</sup></div>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <img src="content_images/slider_2.jpg" />
                            <div>Торговая площадь<br />Академический, 300м<sup>2</sup></div>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <img src="content_images/slider_3.jpg" />
                            <div>Торговая площадь<br />Завокзальный, 256м<sup>2</sup></div>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <img src="content_images/slider_1.jpg" />
                            <div>Торговая площадь<br />Завокзальный, 256м<sup>2</sup></div>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <img src="content_images/slider_2.jpg" />
                            <div>Торговая площадь<br />Академический, 300м<sup>2</sup></div>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <img src="content_images/slider_3.jpg" />
                            <div>Торговая площадь<br />Завокзальный, 256м<sup>2</sup></div>
                        </a>
                    </li>
                </ul>
                </div>
            
        </div>
    </div>
</div>
    <div class="footer">
        <div class="wrap_for_max_width">
            <img class="small_logo" src="images/logo_small.png" />
            © 2012 Портал Ком-Аренда<br />
            Контактный телефон: (343) 344-60-27<br />
            <a href="mailto:office@com-arenda.ru">office@com-arenda.ru</a>
        </div>
    </div>

</body>
</html>