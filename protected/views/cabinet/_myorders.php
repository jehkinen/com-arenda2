<?php 
switch ($row->kind) {
	case 1:	{$t = 'Торговое помещение'; break;}
	case 2:	{$t = 'Склад/производство'; break;}
	default: $t = 'Офис';
}
	$sq = '';
	if (isset($row->squareMin)) {
		if (isset($row->squareMax))
				$sq = $row->squareMin.'-'.$row->squareMax;
		else	$sq = 'от '.$row->squareMin;
	} elseif (isset($row->squareMax))
			$sq = 'до '.$row->squareMax;
	if ($sq!='')
		$sq .= ' м2';
	else $sq = 'не указана';
	$s = '';
	if (isset($row->priceMin)) {
		if (isset($row->priceMax))
				$s = $row->priceMin.'-'.$row->priceMax;
		else	$s = 'от '.$row->priceMin;
	} elseif (isset($row->priceMax))
			$s = 'до '.$row->priceMax;
	if ($s!='')
		$s .= ' руб.';
	else $s = 'не указана';
?>
<div class="post" id="dop<?php echo($row->id);?>">
	<div class="myorders">
		<div class="myorders_title">Заявка на: <?php echo $t;?> от <?php $get = $this->widget('DataParser', array('data_db'=>$row->created)); echo $get->day.' '.$get->month.' '.$get->year; ?></div>
		<div class="myorders_info">В районе: <?php echo $row->district; if (!empty($row->city)):?> (город <?php echo $row->city; ?>)<?php endif;?> Площадь <?php echo $sq;?> Цена <?php echo $s;?>
		</div>
	</div>
	<div class="object_action">
		<a href="#row<?php echo($row->id);?>" class="popup_link" class="action_select">Действие</a>
	</div>
    <div class="pop_up action-window" id="row<?php echo($row->id);?>">
		<?php echo CHtml::link('Смотреть заявку', array('bid/view', 'id'=>$row->id));?><br />
		<a href="#" class="delorder" id="oaj<?php echo($row->id);?>">Удалить</a><br />
	</div>
</div>
