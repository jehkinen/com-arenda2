<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>index</title>
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/carousel.css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.jcarousel.js"></script>
<script type="text/javascript" src="js/jquery.lightbox_me.js"></script>
<script type="text/javascript" src="js/client.js"></script>
<style>
#action_list_1{
    position:absolute;
	padding:5px;
	right:0px;
	top:4px;
	text-align:left;
	font-size:14px;
}
#action_list_2{
    position:absolute;
	padding:5px;
	right:0px;
	top:4px;
	text-align:left;
	font-size:14px;
}
#action_list_3{
    position:absolute;
	padding:5px;
	right:0px;
	top:4px;
	text-align:left;
	font-size:14px;
}
#action_list_4{
    position:absolute;
	padding:5px;
	right:0px;
	top:4px;
	text-align:left;
	font-size:14px;
}
#action_list_5{
    position:absolute;
	padding:5px;
	right:0px;
	top:4px;
	text-align:left;
	font-size:14px;
}
#action_list_6{
    position:absolute;
	padding:5px;
	right:0px;
	top:4px;
	text-align:left;
	font-size:14px;
}
</style>
</head>

<body>
<div class="top_bg"></div><div class="wrap_for_max_width">
	<div class="top_wrap">
   		<div class="rounded dotted user_inf">
        	<div><a href="">Выход</a>Екатерина Медведева-Двойная</div>
			<div><a href="">Личный кабинет</a>ООО "Рога и Копыта"</div>
        </div>
        <div class="most_search">
        	Часто ищут:
			<ul>
            	<li><a href="">офисы 15-30 м2</a></li>
                <li><a href="">офисы 31-50 м2</a></li>
                <li><a href="">торговые помещения в центре</a></li>
            </ul>
        </div>
    	<a href="" class="logo"><img src="images/logo.png" /></a>
        <div class="city_select_wrap">
            <a class="city_select popup_link" href="#city_select">Екатеринбург</a>
            <div class="popup" id="city_select">
           		<a class="popup_close"></a>
                <ul class="city_select_list">
                    <li><a>Асбест</a></li>
                    <li><a>Берёзовский</a></li>
                    <li><a>Верхняя Пышма</a></li>
                    <li><a>Екатеринбург</a></li>
                    <li><a>Каменск-Уральский</a></li>
                    <li><a>Краснотурьинск</a></li>
                    <li><a>Нижний Тагил</a></li>
                    <li><a>Новоуральск</a></li>
                    <li><a>Первоуральск</a></li>
                    <li><a>Полевской</a></li>
                    <li><a>Ревда</a></li>
                    <li><a>Серов</a></li>
                    <li class="add_city">
                        Нет вашего города?
                        <a>Добавить...</a>
                    </li>
                </ul>	
            </div>
        </div>
        <div class="inf_block">
        	всего объектов<br />
			в аренду 1460
        </div>
        <div class="inf_block">
        	Телефон в Екатеринбурге<br />
			(343) 344-60-27
        </div>
        <div class="menu_wrap">
            <ul class="main_menu rounded">
                <li><a href="">О проекте</a></li>
                <li><a href="">Контакты</a></li>
                <li><a href="">Справка</a></li>
                <li><a href="">Пункт меню</a></li>
                <li><a href="">Еще пункт</a></li>
            </ul>
            
            <a href="" class="green_link_block">Как работает com-arenda?</a>
            <a href="" class="green_link_block">Разместить объект</a>
        </div>
			</div><div class="clientNav">
				<span class="clientNavTitle">Личный кабинет</span><span class="clientNavIn">
					<a class="" href="lc_profi_notepad-1.20.00.html" >Блокнот</a><a class="" href="lc_profi_myorders-1.30.00.html" >Мои заявки</a><span>Моя страница</span><a class="" href="lc_profi_settings-1.20.00.html" >Настройки</a>
				</span>
			</div><div class="content_wrap">
    	<div class="aside">
            <div class="gray_block">
            	Знаете ли вы, что: Ученые обнаружили следы ранее неизвестной рептилии, обитавшей около 245 миллионов лет назад на территории современной Антарктики.
            </div>
            <div class="gray_block">
            	Почему кошка приносит нам мышей? По мнению кошки, ее хозяева по части ловли мышей являются, говоря школьным языком, настоящими «двоечниками». Но кошка готова оказать нам в этом сложном деле посильную помощь. Сытая кошка делится добычей со своими собратьями.
            </div>
            <div class="gray_block">
            	Член полярной экспедиции Георгия Седова, штурман корабля "Святой Фока" Н. Сахаров, обязан жизнью своей преданной собаке Штурке. Сахаров отморозил руки и добирался до корабля один в сопровождении собаки. Выбиваясь из сил, прошел по льду пятьдесят километров, временами терял сознание, впадая в забытье. Тогда Штурка садился возле хозяина, лаял, теребил его за одежду, пытаясь привести друга в чувства. Сахаров поднимался и продолжал идти.
            </div>
            
				</div><div class="content">
					<form class="lc_form">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox"/>Я хочу создать свою персональную страницу на портале, прочитал и полностью согласен с <a href="">правилами</a>.
						<div class="easy_notify"><div>
							<h1>
								Очень удобно! Совсем не больно и абсолютно бесплатно!
							</h1><p>
								SMS и e-mail уведомления -- Помогут вам оперативно реагировать на ситуацию, например при поступлении объекта по вашей заявке вам придет SMS или (и) письмо на электронную почту.
							</p><p>
								При деактивации (удалении) объекта из вашего блокнота вам также придет уведомление.
							</p><p>
								Влюбое время вы можете отключить уведомления.
							</p>
							</div>
						</div><div class="lc_form_group">
							<div class="mypage_photo">
								<img src="images/client_sob_photo.jpg"/><br/>
								<a href>Удалить</a>
							</div>&nbsp;<input type="file"/><br/>
								<h1>Название раздела</h1><input size="25" type="text" value="Обо мне"/><textarea rows="8">
								</textarea><h1>Название раздела</h1><input size="25" type="text" value="Обращение к клиентам"/><textarea rows="8">
								</textarea><input class="green_link_rounded" type="button" value="Сохранить"/><span>Данные сохранены</span>
						</div><div class="lc_form_group">
							<h1>Дополнительные контакты</h1><br/>
							<input size="25" type="text" value="Дополнительный телефон"/><br/><input size="25" type="text" value="Skype"/><br/><input size="25" type="text" value="ICQ"/><br/>
							<input class="green_link_rounded" type="button" value="Сохранить"/><span>Данные сохранены</span>
						</div>
					</form>
            
            <div class="also_to_view_wrap">
                Возможно вас заинтересуют следующие предложения:
                <ul class="also_to_view">
                	<li>
                        <a href="">
                            <img src="content_images/slider_1.jpg" />
                            <div>Офис<br />Центр, 220м<sup>2</sup></div>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <img src="content_images/slider_2.jpg" />
                            <div>Торговая площадь<br />Академический, 300м<sup>2</sup></div>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <img src="content_images/slider_3.jpg" />
                            <div>Торговая площадь<br />Завокзальный, 256м<sup>2</sup></div>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <img src="content_images/slider_1.jpg" />
                            <div>Торговая площадь<br />Завокзальный, 256м<sup>2</sup></div>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <img src="content_images/slider_2.jpg" />
                            <div>Торговая площадь<br />Академический, 300м<sup>2</sup></div>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <img src="content_images/slider_3.jpg" />
                            <div>Торговая площадь<br />Завокзальный, 256м<sup>2</sup></div>
                        </a>
                    </li>
                </ul>
                </div>
            
        </div>
    </div>
</div>
    <div class="footer">
        <div class="wrap_for_max_width">
            <img class="small_logo" src="images/logo_small.png" />
            © 2012 Портал Ком-Аренда<br />
            Контактный телефон: (343) 344-60-27<br />
            <a href="mailto:office@com-arenda.ru">office@com-arenda.ru</a>
        </div>
    </div>

</body>
</html>