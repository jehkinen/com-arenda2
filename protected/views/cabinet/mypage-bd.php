<link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/vader/jquery-ui.css" rel="stylesheet" />
<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/jcrop/jquery.Jcrop.min.css'); ?>
<style type="text/css">
div.button {
	height: 20px;
	width: 120px;
	border: 1px solid #440000;
	background-color: #aa0000;
	font-size: 14px;
	color: #ffffff;
	text-align: center;
	padding-top: 6px;
}
div.button.hover {
	border: 1px solid #660000;
	background-color: #cc0000;
}
div.button.disable {
	border: 1px solid #aaaaaa;
	background-color: #ffffff;
	color: #aaaaaa;
}
#loadImg {
	float: left;
}
#str {
	float: left;
	margin: 5px 0 0 5px;
	padding: 1px 0 3px 20px;
	font-size: 14px;
}
#img_box img {
	margin-top: 10px;
}
div.loading {
	background: url(images/indicator.gif) no-repeat left 0;
}
div.resize {
	background: url(images/resize.png) no-repeat left 0 !important;
}
div.chek {
	background: url(images/ok.png) no-repeat left 0 !important;
}
div.cancel {
	background: url(images/cancel.png) no-repeat left 0 !important;
}
</style>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo $this->createAbsoluteUrl('/js/ajaxupload.3.6.js'); ?>"></script>
<script type="text/javascript" src="<?php echo $this->createAbsoluteUrl('/js/jquery.Jcrop.min.js'); ?>"></script>
<script type="text/javascript">
$(document).ready(function() {
	function showCoords(c){
		$('#x1').val(Math.round(c.x));
		$('#y1').val(Math.round(c.y));
		$('#x2').val(Math.round(c.x2));
		$('#y2').val(Math.round(c.y2));
	};
	
	var button = $('#loadImg');
	var info   = $('#str');
	var ibox = $('#img_box');
	
	new AjaxUpload(button, {
		action: 'ajax/u_photo_load/',
		name: 'myfile',
		onSubmit : function(file, ext) {
			if (ext && /^(jpg|png|jpeg|gif|JPG|PNG|JPEG|GIF)$/.test(ext)) {
				this.disable();
				button.addClass('disable');
				info.removeClass().addClass('loading').text('Загрузка');
			} else {
				alert('Неверный тип картинки, допускается только: jpg, jpeg, png, gif');
				return false;
			}
		},
		onComplete: function(file, response) {
			var action = this;
			if (response != false){
				var data = response.split('|');
				var file_name = data[0];
				$('#load-here').append(
					jQuery('<img />')
						.attr({
							src: 'storage/images/personal/temp/' + file_name,
							id:  'crop-image',
						})
				);
				info.removeClass().addClass('resize').text('Редактирование');
				$('#crop-box').dialog({
					modal: true,
					width: (parseInt(data[1])+34) + 'px',
					title: 'Редактирование картинки',
					close: function() {
						info.removeClass().addClass('cancel').text('Отменено пользователем');
						action.enable();
						button.removeClass('disable');
						$('#crop-box').hide();
						$('#load-here').empty();
						$.ajax({
							url: 'ajax/u_photo_delete/',
							data: {
								file: file_name,
								type: 'temp'
							},
							type: 'post'
						});
					},
					resizable: false,
					position:['middle', 100]
				});
				$('#crop-image').Jcrop({
					setSelect: [10, 10, 318, 241],
					minSize: [308, 231], 
					aspectRatio: 4/3,
					allowSelect: false,
					bgColor: '#ffffff',
					bgOpacity: .5,
					onChange: showCoords,
					onSelect: showCoords
				});
				$('#save').click(function() {
					$('#crop-box').remove();
					info.removeClass().addClass('chek').text('Загруженно: ' + file);
					$.ajax({
						url: 'ajax/u_photo_crop/',
						data: {
							file: file_name,
							x1: $('#x1').val(),
							y1: $('#y1').val(),
							x2: $('#x2').val(),
							y2: $('#y2').val()
						},
						type: 'post',
						success: function(){
							ibox.empty()
							.append(
								jQuery('<img />')
									.attr({
										src: 'storage/images/personal/' + file_name
									})
									
							)
							.append('<br />')
							.append(
								jQuery('<a />')
									.attr({
										id: 'u_img_delete',
										href: 'javascript://'
									})
									.text('удалить')
									.bind('click', function(e) {
										e.preventDefault();
										$.ajax({
											url: 'ajax/u_photo_delete/',
											data: {
												file: file_name,
												type: 'user'
											},
											type: 'post',
											success: function(){
												info.removeClass().text('');
												ibox.empty().text('Фотография удалена');
											}
										});
										return false;
									})
							);
						}
					});
				});
			}else{
				info.removeClass().text('');
				alert('Размер картинки слишком большой, разрешенно не более 6Мб');
				action.enable();
				button.removeClass('disable');
			}
		}
	});
	
	$('#u_img_delete').click(function() {
		var file_name = $('#u_img').attr('alt');
		$.ajax({
			url: 'ajax/u_photo_delete/',
			data: {
				file: file_name,
				type: 'user'
			},
			type: 'post',
			success: function(){
				button.text('Загрузить');
				info.removeClass().text('');
				ibox.empty().text('Фотография удалена');
			}
		});
		return false;
	});
});
</script>

<!-- / -->

<?php $this->widget('CabinetMenu', array('active'=>'page'));?>
<div class="content_wrap">
<?php @require_once('protected/views/layouts/rightColumn02.php');?>
	<div class="content">
<?php echo CHtml::form('', 'post', array('class'=>'lc_form')); ?>
		<div class="easy_notify">
			<div>
				<h1>Очень удобно! Совсем не больно и абсолютно бесплатно!</h1>
				<p>SMS и e-mail уведомления -- Помогут вам оперативно реагировать на ситуацию. Например, при поступлении объекта по вашей заявке, вам придет SMS или (и) письмо на электронную почту.</p>
				<p>При деактивации (удалении) объекта из вашего блокнота вам также придет уведомление.</p>
				<p>В любое время вы можете отключить уведомления.</p>
			</div>
		</div>
		<div class="lc_form_group">
			<!--
			<div class="mypage_photo">
			<img src="images/client_sob_photo.jpg"/><br/>
			<a href>Удалить</a>
			</div>&nbsp;<input type="file"/><br/>
			-->
			<h1>Название раздела</h1>
			<?php echo CHtml::activeTextField($form_page, 'parag1_name', array('size'=>25, 'maxlength'=>128));?>
			<?php echo CHtml::textArea('User_page[parag1_text]', $vtext1, array('rows'=>8)); ?>
			<h1>Название раздела</h1>
			<?php echo CHtml::activeTextField($form_page, 'parag2_name', array('size'=>25, 'maxlength'=>128));?>
			<?php echo CHtml::textArea('User_page[parag2_text]', $vtext2, array('rows'=>8)); ?>
			<div class="<?php if (!$wasSend || $paragSaved) echo ' hide';?>"><?php echo CHtml::errorSummary($form_page, 'Исправьте пожалуйста следующие ошибки:');?></div><br />
			<?php echo CHtml::submitButton('Сохранить', array('class'=>'green_link_rounded'));?><span class="<?php if (!$paragSaved) echo 'hide';?>">Данные сохранены</span>
		</div>
<?php echo CHtml::endForm(); ?>
<?php echo CHtml::form('', 'post', array('class'=>'lc_form')); 
$blur = 'if (this.value == '."''".') {this.value = '."'atr';}"; $focus = 'if (this.value == '."'atr'".') {this.value = '."'';}";
?>
		<div class="lc_form_group">
			<h1>Дополнительные контакты</h1><br/>
			<?php $s1=str_replace('atr', $Attrs[1], $blur); $s2=str_replace('atr', $Attrs[1], $focus); echo CHtml::activeTextField($form_addc, 'city', array('size'=>25, 'maxlength'=>31, 'onblur'=>$s1, 'onfocus'=>$s2));?><br />
			<?php $s1=str_replace('atr', $Attrs[2], $blur); $s2=str_replace('atr', $Attrs[2], $focus); echo CHtml::activeTextField($form_addc, 'skype', array('size'=>25, 'maxlength'=>31, 'onblur'=>$s1, 'onfocus'=>$s2));?><br />
			<?php $s1=str_replace('atr', $Attrs[3], $blur); $s2=str_replace('atr', $Attrs[3], $focus); echo CHtml::activeTextField($form_addc, 'icq', array('size'=>25, 'maxlength'=>31, 'onblur'=>$s1, 'onfocus'=>$s2));?><br />
			<div class="<?php if (!$wasSend || $addcSaved) echo ' hide';?>"><?php echo CHtml::errorSummary($form_addc, 'Исправьте пожалуйста следующие ошибки:');?></div><br />
			<?php echo CHtml::submitButton('Сохранить', array('class'=>'green_link_rounded'));?><span class="<?php if (!$addcSaved) echo 'hide';?>">Данные сохранены</span>
		</div>
<?php echo CHtml::endForm(); ?>



	<form class="lc_form">
		<div class="lc_form_group">
			<h1>Фотография пользователя</h1><br/>
			<div id="loadImg" class="button"><?php echo ($uphoto == '') ? 'Загрузить' : 'Изменить'; ?></div>
			<div id="str"></div>
			<div style="clear:both;"></div>
			<div id="img_box">
<?php if ($uphoto != ''): ?>
				<img id="u_img" src="storage/images/personal/<?php echo $uphoto; ?>" alt="<?php echo $uphoto; ?>" /><br />
				<a id="u_img_delete" href="javascript://">удалить</a>
<?php endif; ?>
			</div>
			<input type="hidden" id="x1" name="x1" />
			<input type="hidden" id="y1" name="y1" />
			<input type="hidden" id="x2" name="x2" />
			<input type="hidden" id="y2" name="y2" />
		</div>
	</form>
		<!-- Hidden dialog -->
		<div style="display:none;" id="crop-box">
			<div id="dialog">
				<div id="load-here"></div>
				<button id="save" style="margin-top:10px;">Cохранить</button>
			</div>
		</div>
		<!-- /Hidden dialog -->
	</div><!-- /content -->
</div><!-- /content_wrap -->
