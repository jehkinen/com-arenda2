<?php $bUrl = Yii::app()->baseUrl;
switch ($row->kind) {
	case 1:	{$t = 'Торговое помещение '; break;}
	case 2:	{$t = 'Склад/производство '; break;}
	default: $t = 'Офис ';
}
switch ($row->itemStatus) {
	case 0: {$stat = 'заблокирован администратором'; $statClass = ' inactive'; break;}
	case 1: {$stat = 'показы отменены'; $statClass = ' inactive'; break;}
	//case 2: {$stat = 'сдан'; break;}
	case 3: {$stat = 'ожидает модерации'; $statClass = ' moderating'; break;}
	//case 5: {$stat = 'показы приостановлены'; break;}
	case 6: {$stat = 'идут показы'; $statClass = ' active'; break;}
	default: {$stat = 'не известен статус'; $statClass = ' moderating'; }
}
?>
<div class="post" id="dop<?php echo($row->id);?>">
	<div class="image">
	&nbsp;<?php if(empty($row->hasFoto)) $s=$bUrl.'/images/noFoto-sm.png'; else $s=$bUrl.'/storage/images/_thumbs/'.$row->hasFoto; echo CHtml::image($s);?>
<?php if ($row->special==1):?>
		<img src="<?php echo $bUrl;?>images/hot.png" class="hot tl" />
<?php endif;?>
	</div>
	<span class="object_type"><?php echo $t;?></span><span class="object_date"><?php $get = $this->widget('DateFormat', array('kind'=>1,'dbDate'=>$row->created)); echo $get->isEmpty?'':$get->date; ?></span>
	<div class="adress">
		<?php echo $row->city;?>. Ул. <?php echo $row->street;?>, <?php echo $row->house;?><br />
		Сдает: собственик <?php echo CHtml::link($row->ownSoName.' '.$row->ownName.'.', array('person/card', 'id'=>$row->ownId), array('class'=>'have'));?>
	</div>
	&nbsp;<div class="object_main_info">
		<a href="/objects/detail/id/<?php echo($row->item);?>"><?php echo number_format($row->total_item_sqr*$row->price, 0, '.', ' ');?> руб/мц.<br />
		<?php echo number_format($row->total_item_sqr, 2, '.', ' ');?> м2,<br />
		<?php echo number_format($row->price, 0, '.', ' ');?> руб./м2</a>
	</div>
	<div class="object_action">
		<a href="#row<?php echo($row->id);?>" class="popup_link" class="action_select">Действие</a>
	</div>
    <div class="pop_up action-window" id="row<?php echo($row->id);?>">
		<a href="#" class="delete" id="oaj<?php echo($row->id);?>">Вычеркнуть запись</a><br />
	</div>
	<div class="object_status<?php echo($statClass);?>" id="dos<?php echo($row->id);?>"><?php echo($stat);?></div>
</div>
