<?php $this->widget('CabinetMenu', array('active'=>'orders'));?>
		<div class="content_wrap">
<?php @require_once('protected/views/layouts/rightColumn02.php');?>
		<div class="content">
			<div class="object_table">
<?php if (empty($Rows)):?>
<div class="story">
	<h2>Нет заявок арендаторов</h2>
<?php if ($qObjs==0):?>
	<p>У Вас не размещено ни одного объекта. Вы можете <?php echo CHtml::link('разместить объекты в нашей Базе Данных', array('object/add/step/step1')); ?></p>
<?php else:?>
	<p>Размещенные Вами объекты недвижимости не совпадают по критериям с имеющимися заявками арендаторов.</p>
<?php endif;?>
</div>
<?php else:?>
<?php $i=1; foreach ($Rows as $r){
		$b = ++$i % 2;
		$this->renderPartial('_orders', array(
			'row' =>$r,
			'odd' =>$b
		));
	} ?>
<?php endif;?>
			</div><!-- /object_table -->
<?php $this->widget('CLinkComarendaPager',array(
		'pages'				=>$pages,
		'maxButtonCount'	=>5, # максимальное колличество вкладок на странице
		'header'			=>'', # заголовок
		'nextPageLabel'		=>'',
		'prevPageLabel'		=>'',
		'firstPageLabel'	=>'',
		'lastPageLabel'		=>''
	));
?>
		</div><!-- /content -->
	</div><!-- /content_wrap -->
