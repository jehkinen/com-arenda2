<?php $bUrl = Yii::app()->baseUrl;
switch ($obj->kind) {
	case 1:	{$t = 'Торговое помещение '; break;}
	case 2:	{$t = 'Склад/производство '; break;}
	default: $t = 'Офис ';
}
switch ($obj->status) {
	case 0: {$stat = 'заблокирован администратором'; $statClass = ' inactive'; break;}
	case 1: {$stat = 'показы отменены'; $statClass = ' inactive'; break;}
	//case 2: {$stat = 'сдан'; break;}
	case 3: {$stat = 'ожидает модерации'; $statClass = ' moderating'; break;}
	//case 5: {$stat = 'показы приостановлены'; break;}
	case 6: {$stat = 'идут показы'; $statClass = ' active'; break;}
	default: {$stat = 'не известен статус'; $statClass = ' moderating'; }
}
?>
<div class="post" id="dop<?php echo($obj->id);?>">
	<div class="image">
	&nbsp;<?php if(empty($obj->hasFoto)) $s=$bUrl.'/images/noFoto-sm.png'; else $s=$bUrl.'/storage/images/_thumbs/'.$obj->hasFoto; echo CHtml::image($s);?>
<?php if ($obj->special==1):?>
		<img src="<?php echo $bUrl;?>images/hot.png" class="hot tl" />
<?php endif;?>
	</div>
	<span class="object_type"><?php echo $t;?></span><span class="object_date"><?php $get = $this->widget('DateFormat', array('kind'=>1,'dbDate'=>$obj->created)); echo $get->isEmpty?'':$get->date; ?></span>
	<div class="adress">
		<?php echo '&laquo;'.$obj->name.'&raquo;. Показов: '.$obj->showQty;?><br />
		<?php echo $obj->city;?>. Ул. <?php echo $obj->street;?>, <?php echo $obj->house;?>
	</div>
	&nbsp;<div class="object_main_info">
		<a href="/objects/detail/id/<?php echo($obj->id);?>"><?php echo number_format($obj->total_item_sqr*$obj->price, 0, '.', ' ');?> руб/мц.<br />
		<?php echo number_format($obj->total_item_sqr, 2, '.', ' ');?> м2,<br />
		<?php echo number_format($obj->price, 0, '.', ' ');?> руб./м2</a>
	</div>
	<div class="object_action">
		<a href="#row<?php echo($obj->id);?>" class="popup_link" class="action_select">Действие</a>
	</div>
    <div class="pop_up action-window" id="row<?php echo($obj->id);?>">
		<?php echo CHtml::link('Редактировать', array('object2/edit', 'id'=>$obj->id)); ?><br />
		<a href="#" class="change" id="obj<?php echo($obj->id);?>">Изменить</a><br />
		<a href="#" class="delete del" id="oaj<?php echo($obj->id);?>">Удалить</a><br />
	</div>
	<div class="object_status<?php echo($statClass);?>" id="dos<?php echo($obj->id);?>"><?php echo($stat);?></div>
</div>
