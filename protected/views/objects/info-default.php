<?php $bUrl = Yii::app()->baseUrl; ?>
<div class="podrobnee_table">
<table cellspacing="0" cellpadding="0">
	<tr><td class="td_01">Общая площадь помещения</td><td><?php echo $item->total_item_sqr;?> м<sup>2</sup></td></tr>
</table>
</div>
<div class="podrobnee_table">
<table cellspacing="0" cellpadding="0">
	<tr><td class="td_01">Состояние ремонта</td><td><?php echo $item->repair;?></td></tr>
	<tr class="back_tr"><td class="td_01">Парковка</td><td><?php echo $item->parking;?></td></tr>
	<tr><td class="td_01">Количество закрепленных машиномест</td><td><?php echo $item->fixed_qty;?></td></tr>
	<tr class="back_tr"><td class="td_01">Грузовой лифт</td>
<?php if($item->service_lift==1):?>
		<td><div class="yes"><img src="<?php echo $bUrl;?>/images/ico_mc_01.png" alt="" /> есть</div></td>
<?php else:?>
		<td><div class="no"><img src="<?php echo $bUrl;?>/images/no.png" alt="" /> нет</div></td>
<?php endif;?>
	</tr>
</table>
</div>
<div class="podrobnee_table">
<table cellspacing="0" cellpadding="0"><caption>Система безопасности</caption>
	<tr><td class="td_01">Видеонаблюдение</td>
<?php if($item->video==1):?>
		<td><div class="yes"><img src="<?php echo $bUrl;?>/images/ico_mc_01.png" alt="" /> есть</div></td>
<?php else:?>
		<td><div class="no"><img src="<?php echo $bUrl;?>/images/no.png" alt="" /> нет</div></td>
<?php endif;?>
	</tr>
	<tr class="back_tr"><td class="td_01">Пропускная система</td>
<?php if($item->access==1):?>
		<td><div class="yes"><img src="<?php echo $bUrl;?>/images/ico_mc_01.png" alt="" /> есть</div></td>
<?php else:?>
		<td><div class="no"><img src="<?php echo $bUrl;?>/images/no.png" alt="" /> нет</div></td>
<?php endif;?>
	</tr>
	<tr><td class="td_01">Система пожаротушения</td>
<?php if($item->firefighting==1):?>
		<td><div class="yes"><img src="<?php echo $bUrl;?>/images/ico_mc_01.png" alt="" /> есть</div></td>
<?php else:?>
		<td><div class="no"><img src="<?php echo $bUrl;?>/images/no.png" alt="" /> нет</div></td>
<?php endif;?>
	</tr>
</table>
</div>
<div class="podrobnee_table">
<table cellspacing="0" cellpadding="0">
	<tr><td class="td_01">Интернет</td>
<?php if($item->internet==1):?>
		<td><div class="yes"><img src="<?php echo $bUrl;?>/images/ico_mc_01.png" alt="" /> есть</div></td>
<?php else:?>
		<td><div class="no"><img src="<?php echo $bUrl;?>/images/no.png" alt="" /> нет</div></td>
<?php endif;?>
	</tr>
	<tr class="back_tr"><td class="td_01">Провайдер</td><td><?php echo $item->providers;?></td></tr>
	<tr><td class="td_01">Телефония</td>
<?php if($item->telephony==1):?>
		<td><div class="yes"><img src="<?php echo $bUrl;?>/images/ico_mc_01.png" alt="" /> есть</div></td>
<?php else:?>
		<td><div class="no"><img src="<?php echo $bUrl;?>/images/no.png" alt="" /> нет</div></td>
<?php endif;?>
	</tr>
	<tr class="back_tr"><td class="td_01">Телефонная компания</td><td><?php echo $item->tel_company;?></td></tr>
</table>
</div>
<div class="podrobnee_table">
<table cellspacing="0" cellpadding="0">
	<tr><td class="td_01">Форма договора</td><td><?php echo $item->contr_condition;?></td></tr>
	<tr class="back_tr"><td class="td_01">Бытовые помещения</td><td><?php echo $item->welfare;?></td></tr>
	<tr><td class="td_01">Коммунальные услуги</td><td><?php echo $item->pay_utilities;?></td></tr>
	<tr class="back_tr"><td class="td_01">Системы отопления и водоснабжения</td>
<?php if($item->heating==1):?>
		<td><div class="yes"><img src="<?php echo $bUrl;?>/images/ico_mc_01.png" alt="" /> есть</div></td>
<?php else:?>
		<td><div class="no"><img src="<?php echo $bUrl;?>/images/no.png" alt="" /> нет</div></td>
<?php endif;?>
	</tr>
	<tr><td class="td_01">Возможность рекламы</td>
<?php if($item->can_reclame==1):?>
		<td><div class="yes"><img src="<?php echo $bUrl;?>/images/ico_mc_01.png" alt="" /> есть</div></td>
<?php else:?>
		<td><div class="no"><img src="<?php echo $bUrl;?>/images/no.png" alt="" /> нет</div></td>
<?php endif;?>
	</tr>
</table>
</div>
<div class="podrobnee_table">
<table cellspacing="0" cellpadding="0">
	<tr><td class="td_01">Емкость паллетного хранения</td><td><?php echo $item->pallet_capacity;?></td></tr>
	<tr class="back_tr"><td class="td_01">Емкость полочного хранения</td><td><?php echo $item->shelf_capacity;?></td></tr>
	<tr><td class="td_01">Высота потолков</td><td><?php echo $item->ceiling_height;?></td></tr>
	<tr class="back_tr"><td class="td_01">Рабочая высота помещения</td><td><?php echo $item->work_height;?></td></tr>
</table>
</div>
<div class="podrobnee_table">
<table cellspacing="0" cellpadding="0">
	<tr><td class="td_01">Кран-балка</td>
<?php if($item->cathead==1):?>
		<td><div class="yes"><img src="<?php echo $bUrl;?>/images/ico_mc_01.png" alt="" /> есть</div></td>
<?php else:?>
		<td><div class="no"><img src="<?php echo $bUrl;?>/images/no.png" alt="" /> нет</div></td>
<?php endif;?>
	</tr>
	<tr class="back_tr"><td class="td_01">Покрытие пола</td><td><?php echo $item->flooring;?></td></tr>
	<tr><td class="td_01">Нагрузка на пол</td><td><?php echo $item->floor_load;?></td></tr>
	<tr class="back_tr"><td class="td_01">Система вентиляции</td><td><?php echo $item->ventilation;?></td></tr>
	<tr><td class="td_01">Кондиционирование</td>
<?php if($item->air_condit==1):?>
		<td><div class="yes"><img src="<?php echo $bUrl;?>/images/ico_mc_01.png" alt="" /> есть</div></td>
<?php else:?>
		<td><div class="no"><img src="<?php echo $bUrl;?>/images/no.png" alt="" /> нет</div></td>
<?php endif;?>
	</tr>
</table>
</div>
<div class="podrobnee_table">
<table cellspacing="0" cellpadding="0">
	<tr><td class="td_01">Электр.мощность</td><td><?php echo $item->el_power;?></td></tr>
	<tr class="back_tr"><td class="td_01">Количество ворот</td><td><?php echo $item->gates_qty;?></td></tr>
	<tr><td class="td_01">Высота ворот</td><td><?php echo $item->gates_height;?></td></tr>
	<tr class="back_tr"><td class="td_01">Пандус</td>
<?php if($item->rampant==1):?>
		<td><div class="yes"><img src="<?php echo $bUrl;?>/images/ico_mc_01.png" alt="" /> есть</div></td>
<?php else:?>
		<td><div class="no"><img src="<?php echo $bUrl;?>/images/no.png" alt="" /> нет</div></td>
<?php endif;?>
	</tr>
	<tr><td class="td_01">Автомобильные подъездные пути</td>
<?php if($item->autoways==1):?>
		<td><div class="yes"><img src="<?php echo $bUrl;?>/images/ico_mc_01.png" alt="" /> есть</div></td>
<?php else:?>
		<td><div class="no"><img src="<?php echo $bUrl;?>/images/no.png" alt="" /> нет</div></td>
<?php endif;?>
	</tr>
	<tr class="back_tr"><td class="td_01">Железнодорожные подъездные пути</td>
<?php if($item->railways==1):?>
		<td><div class="yes"><img src="<?php echo $bUrl;?>/images/ico_mc_01.png" alt="" /> есть</div></td>
<?php else:?>
		<td><div class="no"><img src="<?php echo $bUrl;?>/images/no.png" alt="" /> нет</div></td>
<?php endif;?>
	</tr>
</table>
</div>
<div class="podrobnee_table">
<table cellspacing="0" cellpadding="0">
	<tr><td class="td_01">Дополнительная информация</td><td><?php echo $item->about;?></td></tr>
<tr class="back_tr">
	<td class="td_01">Форма договора</td>
	<td><img src="<?php echo $bUrl;?>/images/ico_mc_04.png" alt="" /> <a href="#">ссылка на документ</a></td>
</tr>
</table>
</div>
