<?php $bUrl = Yii::app()->baseUrl;
	$noFoto=(empty($aFotos));
?>
<?php if(!$noFoto):?>
<div id="gallery_popup">
	<a class="gallery_close"></a>
	<div class="gallery_big_image">
		<img src="<?php echo $bUrl;?>/storage/images/<?php echo $aFotos[0]->img;?>" />
	</div>
	<ul id="gallery_carousel" class="jcarousel-skin-tango">
<?php foreach ($aFotos as $f):?>
		<li><a rel="<?php echo $bUrl;?>/storage/images/<?php echo $f->img;?>"><img src="<?php echo $bUrl;?>/storage/images/_thumbs/<?php echo $f->name_small;?>" /></a></li>
<?php endforeach;?>
	</ul>
</div>
<?php endif;?>

<a href="javascript: history.go(-1);" class="history_back">Вернуться на предыдущую страницу</a>
<div class="left_col_56perc">
	<div class="gallery_block_wrap">
<?php if($noFoto):?>
		<img src="<?php echo $bUrl;?>/images/noFoto-med.png" style="width:100%" />
<?php else:?>
		<a href="<?php echo $bUrl;?>/storage/images/<?php echo $aFotos[0]->img;?>"><img src="<?php echo $bUrl;?>/storage/images/<?php echo $aFotos[0]->img;?>" style="width:100%" /></a>
		<ul class="gallery_block" id="object_card_gallery">
			<li><img class="arrow_thumbs" src="<?php echo $bUrl;?>/images/arr_thumbs_left.png" /></li>
<?php foreach ($aFotos as $f):?>
			<li><a href="<?php echo $bUrl;?>/storage/images/<?php echo $f->img;?>"><img src="<?php echo $bUrl;?>/storage/images/_thumbs/<?php echo $f->name_small;?>" /></a></li>
<?php endforeach;?>
			<li><img class="arrow_thumbs" src="<?php echo $bUrl;?>/images/arr_thumbs_right.png" /></li>
		</ul>
<?php endif;?>
	</div>
<?php if (Yii::app()->params->site_base=='http://com-arenda.ru/'): ?>
	<div class="green_top_border_block">
		<h2>На карте:</h2>
		<div class="map" id="YMapsID"></div>
	</div>
<script src="http://api-maps.yandex.ru/1.1/index.xml?key=ANYOu08BAAAABkHhIAIAuQ8eFZCpqMjUYtyGKCOCmVOdg2wAAAAAAAAAAABRdlW-DED3gC4r4Bvi6P3MejBulQ=="
	type="text/javascript"></script>
<script type="text/javascript">
// Создание обработчика для события window.onLoad
YMaps.jQuery(function () {
	// Создание экземпляра карты и его привязка к созданному контейнеру
	var map = new YMaps.Map(YMaps.jQuery("#YMapsID")[0]);
	// Создание объекта геокодера
	var geocoder = new YMaps.Geocoder("<?php echo $obj->city.', улица '.$obj->street.', '.$obj->house;?>");
	// По завершению геокодирования инициализируем карту первым результатом
	YMaps.Events.observe(geocoder, geocoder.Events.Load, function (geocoder) {
		if (geocoder.length()) {
			map.setBounds(geocoder.get(0).getBounds());
		}
	});
	map.enableScrollZoom();
	var zoom = new YMaps.Zoom({
		customTips: [
			{ index: 10, value: "город" },
			{ index: 14, value: "улица" },
			{ index: 16, value: "дом" }
		]
	});
	map.addControl(zoom);
	map.addControl(new YMaps.TypeControl());
	map.addControl(new YMaps.ToolBar());
	map.addControl(new YMaps.ScaleLine());
	map.addOverlay(geocoder);
	});
</script>
<?php endif; ?>	

<?php if(!empty($items)):?>
	<div class="green_top_border_block">
		<h3>Свободные площади</h3>
		<ul class="free_space">
<?php foreach ($items as $i):?>
			<li><a href="objects/detail/id/<?php echo $i->id;?>"><?php if(empty($i->hasFoto)) $s=$bUrl.'/images/noFoto-sm.png'; else $s=$bUrl.'/storage/images/_thumbs/'.$i->hasFoto; echo CHtml::image($s);?></a>
				<div class="text">
<?php	switch ($i->kind) {
		case 1 : {$s = 'Торговая площадь'; break;}
		case 2 : {$s = 'Склад/производ.'; break;}
		default: {$s = 'Офис'; break;}
	}	echo $s;?><br />
					<?php echo $i->total_item_sqr;?> м2  <?php echo $i->price;?> руб./м2<br />
					<b><?php echo $i->total_item_sqr*$i->price;?> руб./мц.</b>
				</div>
			</li>
<?php endforeach;?>
		</ul>
	</div>
<?php endif;?>
</div><!-- /left_col_56perc -->

<div class="right_col_42perc">
	<div class="object_main_inf">
<?php	switch ($obj->kind) {
		case 1 : {$s = 'Торговый центр '; break;}
		case 2 : {$s = 'Центр Недвижимости '; break;}
		default: {$s = 'Офисный центр '; break;}
	}
?>
		<div class="object_main_inf_type"><?php echo $s;?></div>
		<div class="object_main_inf_class"><?php echo 'класса "'.$obj->class.'"';?></div>
		<div class="object_main_inf_price"><?php echo '&laquo;'.$obj->name.'&raquo;';?></div>
		<br/>
		<div class="object_main_inf_h">Город<br />Район<br/>Адрес<br/>Представитель<br/>Телефон</div>
		<div class="object_main_inf_d">
			<?php echo $obj->city.'<br />'.$obj->district.'<br />ул. '.$obj->street.', '.$obj->house.'<br />'.CHtml::link($obj->firstname.' '.$obj->lastname.'.', array('person/card', 'id'=>$obj->owner_id)).'<br/>'.$obj->phone;?>
		</div>
	</div>

<?php if(!empty($obj->about)):?>
	<div class="green_top_border_block">
		<h3>Дополнительно</h3><?php echo $obj->about;?>
	</div>
<?php endif;?>

</div><!-- /right_col_42perc -->
