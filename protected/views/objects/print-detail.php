<?php
	$bUrl = Yii::app()->baseUrl;
	$noFoto=(empty($aFotos));
	switch ($item->kind) {
		case 1 : {$lbClass = 'торгового помещения'; break;}
		case 2 : {$lbClass = 'склада/производственного помещения'; break;}
		default: $lbClass = 'офиса';
	}
	switch ($item->itemType) {
		case 0 : { // Простой объект
			switch ($item->kind) {
				case 1 : {$lbTitle = 'Торговое помещение'; break;}
				case 2 : {$lbTitle = 'Склад или производственное помещение'; break;}
				default: $lbTitle = 'Офисное помещение';
			}
			break;
			$lbTitle.=' "'.$item->name.'"';
		} 
		case 1 : { // Объект-контейнер
			switch ($item->kind) {
				case 1 : {$lbTitle = 'Торговый центр'; break;}
				case 2 : {$lbTitle = 'Складской или производственный центр'; break;}
				default: $lbTitle = 'Офисный центр';
			}
			$lbTitle.=' "'.$item->name.'"';
			break;
		}
		case 2 : { // Вложенный объект
			switch ($item->kind) {
				case 1 : {$lbTitle = 'Помещение '.$item->name.' в торговом центре'; break;}
				case 2 : {$lbTitle = 'Помещение '.$item->name.' в складском или производственном центре'; break;}
				default: $lbTitle = 'Офис '.$item->name.' в офисном центре';
			}
			$lbTitle.=' "'.$item->parentName.'"';
			break;
		}
	}
	/*
	$ref = Yii::app()->request->urlReferrer;
	CVarDumper::Dump(Yii::app()->request->urlReferrer, 3, true); die;
	*/
?>
<p class="back-url"><?php echo CHtml::link($back_url, array('objects/detail', 'id'=>$item->id));?></p>
<div class="<?php echo (($item->itemType==1)?'green_title':'orange_title');?>"><?php echo $lbTitle;?></div>
<div class="foto_card"<?php if ($noFoto) echo 'style="display:none;"';?>>
<?php if (!$noFoto):?>
<img src="<?php echo $bUrl;?>/storage/images/_thumbs/<?php echo $aFotos[0]->name_medium;?>" alt="<?php echo $aFotos[0]->img;?>" />
	<div class="card_mini_gall">
<?php foreach ($aFotos as $f): ?>
		<img src="<?php echo $bUrl;?>/storage/images/_thumbs/<?php echo $f->name_small;?>" alt="" />
<?php endforeach;?>
	</div>
<?php endif;?>
</div>
<div class="text_card">
		<div class="text_card_info">
		<h3><?php echo $item->city;?>, <?php echo $item->street;?>, <?php echo $item->house;?></h3>
		<i>Аренда <?php echo $lbClass;?> класса "<?php echo $item->class;?>"</i><br /><?php echo (float)$item->total_item_sqr;?> м<sup>2</sup> / <?php echo (float)$item->price;?> р.<br />
		<?php echo $item->about;?>
		</div>
	<div class="phone_card">Телефон: <span><?php echo $item->phone;?></span></div>
	<div class="name_autor_card">
		<?php echo CHtml::link($item->firstname.' '.$item->lastname.'.', array('person/card', 'id'=>$item->owner_id));?>
	</div>
<?php if (!empty($aFiles)):?>
	<div class="file_table">
		<h4>Прикрепленные файлы:</h4>
		<div class="file_table_background">
			<div class="file_table_background_02">
				<table cellspacing="0" cellpadding="0">
<?php foreach($aFiles as $f):?>
					<tr><td class="td_01"><?php echo $f->name;?></td><td><?php echo CHtml::image($bUrl.'/images/ico_mc_04.png');?> <?php echo CHtml::link('скачать', array('/storage/files/objs/'.$f->filename));?></td></tr>
<?php endforeach;?>
				</table>
			</div>
		</div>
	</div>
<?php endif;?>
</div>
<br /><br />
<div class="podrobnee_card">
<h3>Подробно</h3>
<div class="podrobnee_table">
<table cellspacing="0" cellpadding="0">
	<tr><td class="td_01">Функциональное назначение</td><td><?php echo $item->functionality;?></td></tr>
	<tr class="back_tr"><td class="td_01">Район</td><td><?php echo $item->district;?></td></tr>
	<tr><td class="td_01">Улица</td><td><?php echo $item->street;?></td></tr>
	<tr class="back_tr"><td class="td_01">Дом</td><td><?php echo $item->house;?></td></tr>
	<tr><td class="td_01">Корпус</td><td><?php echo $item->housing;?></td></tr>
</table>
</div>
<div class="podrobnee_table">
<table cellspacing="0" cellpadding="0">
	<tr><td class="td_01">Тип сооружения</td><td><?php echo $item->kind_structure;?></td></tr>
	<tr class="back_tr"><td class="td_01">Название</td><td><?php echo $item->name;?></td></tr>
	<tr><td class="td_01">Класс</td><td><?php echo $item->class;?></td></tr>
	<tr class="back_tr"><td class="td_01">Этаж</td><td><?php echo $item->storey;?></td></tr>
	<tr><td class="td_01">Этажность</td><td><?php echo $item->storeys;?></td></tr>
</table>
</div>
<?php
switch ($item->kind) {
	case 0 : {$s='info-ofice'; break;}
	case 1 : {$s='info-torg'; break;}
	case 2 : {$s='info-sklad'; break;}
	default:  $s='info-default';
}
$this->renderPartial($s, array('item' =>$item));
?>
</div>
<br /><br /><br /><br />
<?php $this->widget('SimilarObjects', array('selfId'=>$item->id, 'kind'=>$item->kind, 'district'=>$item->districtID, 'price'=>$item->price, 'square'=>$item->total_item_sqr));?>