				<tr><td class="t">Покрытие пола</td><td><?php echo $item->flooring;?></td></tr>
				<tr class="even"><td class="t">Высота потолков</td><td><?php echo $item->ceiling_height;?></td></tr>
				<tr><td class="t">Рабочая высота помещения</td><td><?php echo $item->work_height;?></td></tr>
				<tr class="even"><td class="t">Нагрузка на пол</td><td><?php echo $item->floor_load;?></td></tr>
				<tr><td class="t">Электр.мощность</td><td><?php echo $item->el_power;?></td></tr>
				<tr class="even"><td class="t">Грузовой лифт</td><?php if($item->service_lift==1){?><td class="check">есть</td><?php } else { ?><td>нет</td><?php } ?></tr>
				<tr><td class="t">Бытовые помещения</td><td><?php echo $item->welfare;?></td></tr>
				<tr class="even"><td class="t">Пандус</td><?php if($item->rampant==1){?><td class="check">есть</td><?php } else { ?><td>нет</td><?php } ?></tr>
				<tr><td class="t">Видеонаблюдение</td><?php if($item->video==1){?><td class="check">есть</td><?php } else { ?><td>нет</td><?php } ?></tr>
				<tr class="even"><td class="t">Система пожаротушения</td><?php if($item->firefighting==1){?><td class="check">есть</td><?php } else { ?><td>нет</td><?php } ?></tr>
				<tr><td class="t">Система вентиляции</td><td><?php echo $item->ventilation;?></td></tr>
				<tr class="even"><td class="t">Кондиционирование</td><?php if($item->air_condit==1){?><td class="check">есть</td><?php } else { ?><td>нет</td><?php } ?></tr>
				<tr><td class="t">Форма договора</td><td><?php echo $item->contr_condition;?></td></tr>
				<tr class="even"><td class="t">Интернет</td><?php if($item->internet==1){?><td class="check">есть</td><?php } else { ?><td>нет</td><?php } ?></tr>
				<tr><td class="t">Провайдер</td><td><?php echo $item->providers;?></td></tr>
				<tr class="even"><td class="t">Телефония</td><?php if($item->telephony==1){?><td class="check">есть</td><?php } else { ?><td>нет</td><?php } ?></tr>
				<tr><td class="t">Телефонная компания</td><td><?php echo $item->tel_company;?></td></tr>
				<tr class="even"><td class="t">Парковка</td><td><?php echo $item->parking;?></td></tr>
				<tr><td class="t">Количество закрепленных машиномест</td><td><?php echo $item->fixed_qty;?></td></tr>
				<tr class="even"><td class="t">Возможность размещения рекламы</td><?php if($item->can_reclame==1){?><td class="check">есть</td><?php } else { ?><td>нет</td><?php } ?></tr>
