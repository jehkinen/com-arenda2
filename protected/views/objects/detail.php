<?php
	$bUrl = Yii::app()->baseUrl;
	if ($userTenant) {
		echo CHtml::cssFile($bUrl.'/css/modal.css'); 
		echo CHtml::scriptFile($bUrl.'/js/jquery.simplemodal.js');
	}
	$noFoto=(empty($aFotos));
	switch ($item->kind) {
		case 1 : {$lbKind = 'Торговое'; $lbClass = 'торгового помещения'; $lbParent = 'Торговый центр'; break;}
		case 2 : {$lbKind = 'Склад / Производственное'; $lbClass = 'складского/производственного помещения'; $lbParent = 'Центр Недвижимости'; break;}
		default: {$lbKind = 'Офисное'; $lbClass = 'офиса класса &laquo;'.$item->class.'&raquo;';$lbParent = 'Офисный центр';}
	}
	/*
	$ref = Yii::app()->request->urlReferrer;
	CVarDumper::Dump(Yii::app()->request->urlReferrer, 3, true); die;
	*/
?>
<?php if ($userTenant):?>
<script type="text/javascript">
	$(document).ready(function(){

		<?php if (!$atNotepad) { ?>$('span#spanInNotepad').hide();<?php } ?>
		$('a.tonotepad').bind('click', function(e){
			var id_obj = $(e.target).attr('id');
			e.preventDefault();
			show_modal('#owner-delete', 'ods', '<span>Блокнот</span>', 'Скопировать объект в блокнот?', '30%', function(){
				$.ajax({
					url: "/ajax/notepad_add",
					data: {id: id_obj},
					type: 'post',
					success: function(){
						$('span#spanInNotepad').show();
						$(e.target).parent().hide();
					}
				});
			});
		});
		$('a.claim').bind('click', function(e){
			var obj_id = $(e.target).attr('id');
			e.preventDefault();
			show_modal('#mwindow', 'ocs', '<span>Жалоба</span>', '', '20%', function(){
				var selOption = $('option:selected');
				var claim = selOption.val();
				if (typeof(claim) != 'undefined'){
					$.ajax({
						url: "/ajax/send_claim",
						data: {id: obj_id, clm: claim},
						type: 'post',
						success: function(data){}
					});
				}
			});
		});
	});
	
function show_modal(box, container, header, message, top, callback){
	$(box).modal({
		closeHTML: '<a href="#" title="Закрыть" class="x-close"><img src="/images/modal/mw-close.jpg" /></a>',
		position: [top,],
		opacity: 30,
		overlayId: 'overlay',
		containerId: container,
		closeClass: 'close',
		onShow: function (dialog){
			var modal = this;

			$('.header', dialog.data[0]).append(header);
			$('.message', dialog.data[0]).append(message);

			$('.yes', dialog.data[0]).click(function () {
				if ($.isFunction(callback)) {
					callback.apply();
				}
				modal.close();
			});
		}
	});
}
</script>
<div id="owner-delete">
	<div class="header"></div>
	<div class="message"></div>
	<div class="buttons">
		<div class="yes">Да</div><div class="no close">Нет</div>
	</div>
</div>
<div id="mwindow">
	<div class="header"></div>
	<div class="message">
		Мной обнаружено:<br />
		<select name="claim">
			<option value="1" selected="selected">Неверная информация об объекте</option>
			<option value="2">Объявление неактульно (сдано)</option>
			<option value="3">Неадекватное поведение</option>
		</select>
	</div>
	<div class="buttons">
		<div class="yes">Подать</div><div class="no close">Отмена</div>
	</div>
</div>
<?php endif;?>


<?php if(!$noFoto):?>
<div id="gallery_popup">
	<a class="gallery_close"></a>
	<div class="gallery_big_image">
		<img src="<?php echo $bUrl;?>/storage/images/<?php echo $aFotos[0]->img;?>" />
	</div>
	<ul id="gallery_carousel" class="jcarousel-skin-tango">
<?php foreach ($aFotos as $f):?>
		<li><a rel="<?php echo $bUrl;?>/storage/images/<?php echo $f->img;?>"><img src="<?php echo $bUrl;?>/storage/images/_thumbs/<?php echo $f->name_small;?>" /></a></li>
<?php endforeach;?>
	</ul>
</div>
<?php endif;?>

<a href="javascript: history.go(-1);" class="history_back">Вернуться на предыдущую страницу</a>
<div class="left_col_56perc">
	<div class="gallery_block_wrap">
<?php if($noFoto):?>
		<img src="<?php echo $bUrl;?>/images/noFoto-med.png" style="width:100%" />
<?php else:?>
		<a href="<?php echo $bUrl;?>/storage/images/<?php echo $aFotos[0]->img;?>"><img src="<?php echo $bUrl;?>/storage/images/<?php echo $aFotos[0]->img;?>" style="width:100%" /></a>
		<ul class="gallery_block" id="object_card_gallery">
			<!--<li><img class="arrow_thumbs" src="<?php echo $bUrl;?>/images/arr_thumbs_left.png" /></li>-->
<?php foreach ($aFotos as $f):?>
			<li><a href="<?php echo $bUrl;?>/storage/images/<?php echo $f->img;?>"><img src="<?php echo $bUrl;?>/storage/images/_thumbs/<?php echo $f->name_small;?>" /></a></li>
<?php endforeach;?>
			<!--<li><img class="arrow_thumbs" src="<?php echo $bUrl;?>/images/arr_thumbs_right.png" /></li>-->
		</ul>
<?php endif;?>
	</div>

<?php if(!empty($item->about)):?>
	<div class="green_top_border_block">
		<h2>Дополнительно</h2><?php echo $item->about;?>
	</div>
<?php endif;?>

<?php if (Yii::app()->params->site_base=='http://com-arenda.ru/'): ?>
	<div class="green_top_border_block">
		<h2>На карте:</h2>
		<div class="map" id="YMapsID"></div>
	</div>
<script src="http://api-maps.yandex.ru/1.1/index.xml?key=ANYOu08BAAAABkHhIAIAuQ8eFZCpqMjUYtyGKCOCmVOdg2wAAAAAAAAAAABRdlW-DED3gC4r4Bvi6P3MejBulQ=="
	type="text/javascript"></script>
<script type="text/javascript">
// Создание обработчика для события window.onLoad
YMaps.jQuery(function () {
	// Создание экземпляра карты и его привязка к созданному контейнеру
	var map = new YMaps.Map(YMaps.jQuery("#YMapsID")[0]);
	// Создание объекта геокодера
	var geocoder = new YMaps.Geocoder("<?php echo $item->city.', улица '.$item->street.', '.$item->house;?>");
	// По завершению геокодирования инициализируем карту первым результатом
	YMaps.Events.observe(geocoder, geocoder.Events.Load, function (geocoder) {
		if (geocoder.length()) {
			map.setBounds(geocoder.get(0).getBounds());
		}
	});
	map.enableScrollZoom();
	var zoom = new YMaps.Zoom({
		customTips: [
			{ index: 10, value: "город" },
			{ index: 14, value: "улица" },
			{ index: 16, value: "дом" }
		]
	});
	map.addControl(zoom);
	<?php /*
	map.addControl(new YMaps.TypeControl());
	map.addControl(new YMaps.ToolBar());
	*/ ?>
	map.addControl(new YMaps.ScaleLine());
	map.addOverlay(geocoder);
	});
</script>
<?php endif; ?>	

<?php if(!empty($aFiles)):?>
	<div class="green_top_border_block">
		<h2>Документы</h2>
		<div class="download_list_files">
<?php $k = count($aFiles); for($i=0;$i<$k;$i++):?>
			<a class="docs_any" href="<?php echo $bUrl;?>/storage/files/objs/<?php echo $aFiles[$i]->filename;?>">Скачать</a><?php if ($i<$k-1) echo '<br />';?>
<?php endfor;?>
		</div>
		<div class="download_list_title">
<?php foreach ($aFiles as $f):?>
			<div><div class="download_list_title_lbl"><?php echo $f->name;?></div><div class="download_list_title_dots">&nbsp;</div></div>
<?php endforeach;?>
		</div>
	</div>
<?php endif;?>

</div><!-- /left_col_56perc -->

<div class="right_col_42perc">
	<div class="object_main_inf">
		<div class="object_main_inf_date"><?php $get = $this->widget('DateFormat', array('kind'=>1,'dbDate'=>$item->created)); echo $get->isEmpty?'':$get->date;?></div>
		<div class="object_main_inf_type"><?php echo $lbKind;?> помещение</div>
		<div class="object_main_inf_class">Аренда <?php echo $lbClass;?></div>
		<div class="object_main_inf_price"><?php echo (float)$item->total_item_sqr*$item->price;?> руб.</div>
		<br/>
		<div class="object_main_inf_h">Город<br />Район<br/>Адрес<br/><?php if ($item->itemType==2):?>Помещение<br/><?php endif;?>Цена за м<sup>2</sup><br/>Площадь<br/>Сдает<br/>Контактное лицо<br/>Телефон</div>
		<div class="object_main_inf_d">
			<?php echo $item->city.'<br />'.$item->district.'<br />ул. '.$item->street.', '.$item->house.'<br />';
if ($item->itemType==2) echo $lbParent.' '.CHtml::link('"'.$item->parentName.'"', array('objects/contain', 'id'=>$item->parent)).'<br />';
echo (float)$item->price.' руб.<br/>'.(float)$item->total_item_sqr.' м2.<br/>собственник<br/>'.CHtml::link($item->firstname.' '.$item->lastname.'.', array('person/card', 'id'=>$item->owner_id)).'<br/>'.$item->phone;?>
		</div>
	</div>

	<ul class="object_actions">
		<li class="object_actions_zip"><?php echo CHtml::link('Скачать', array('objects/load', 'id'=>$id_obj));?><span class="subt"> (ZIP-файл)</span></li>
		<li class="object_actions_pdf"><?php echo CHtml::link('Скачать', array('objects/download', 'id'=>$id_obj));?><span class="subt"> (PDF-файл)</span></li>
		<li class="object_actions_print"><?php echo CHtml::link('Распечатать', array('objects/print', 'id'=>$id_obj));?></li>
	</ul>
<?php if ($userTenant):?>
	<ul class="object_actions">
		<li class="object_actions_abuse"><a href="#" class="claim" id="obj<?php echo($id_obj);?>">Пожаловаться</a></li>
		<?php if (!$atNotepad) { ?><li class="object_actions_cptnp"><a href="#" class="tonotepad" id="obj<?php echo($id_obj);?>">Копировать в блокнот</a></li><?php } ?><span class="innote" id="spanInNotepad">в блокноте</span>
	</ul>	
<?php endif;?>
	<div class="green_top_border_block">
		<h2>Подробнее</h2>
		<table class="items_2">
			<tbody>
			<tr><td class="t">Функциональное назначение</td><td><?php echo $item->functionality;?></td></tr>
			<tr class="even"><td class="t">Тип сооружения</td><td><?php echo $item->kind_structure;?></td></tr>
			<tr><td class="t">Название</td><td><?php echo $item->name;?></td></tr>
			<tr class="even"><td class="t">Этаж</td><td><?php echo $item->storey;?></td></tr>
			<tr><td class="t">Коммунальные услуги</td><td><?php echo $item->pay_utilities;?></td></tr>
			<tr class="even"><td class="t">Состояние ремонта</td><td><?php echo $item->repair;?></td></tr>
			<tr><td>Корпус</td><td><?php echo $item->housing;?></td></tr>
			<tr class="even"><td>Этажность здания</td><td><?php echo $item->storeys;?></td></tr>
<?php
switch ($item->kind) {
	case 0 : {$s='info-ofice'; break;}
	case 1 : {$s='info-torg'; break;}
	case 2 : {$s='info-sklad'; break;}
	default:  $s='info-ofice';
}
$this->renderPartial($s, array('item' =>$item));
?>
			</tbody>
		</table>
	</div>
</div><!-- /right_col_42perc -->

<?php $this->widget('SimilarObjects', array('selfId'=>$item->id, 'kind'=>$item->kind, 'district'=>$item->districtID, 'price'=>$item->price, 'square'=>$item->total_item_sqr));?>
