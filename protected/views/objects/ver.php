<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>index</title>
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/carousel.css" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery.jcarousel.js"></script>
<script type="text/javascript" src="js/jquery.lightbox_me.js"></script>
<script type="text/javascript" src="js/client.js"></script>
</head>

<body>
<div class="wrap_for_max_width">
	<div class="top_wrap green_border_bottom">
   		<div class="rounded dotted user_inf">
        	<div><a href="">Выход</a>Екатерина Медведева-Двойная</div>
			<div><a href="">Личный кабинет</a>ООО "Рога и Копыта"</div>
        </div>
    	<a href="" class="logo"><img src="images/logo.png" /></a>
        <div class="city_select_wrap">
            <a class="city_select popup_link" href="#city_select">Екатеринбург</a>
            <div class="popup" id="city_select">
           		<a class="popup_close"></a>
                <ul class="city_select_list">
                    <li><a>Асбест</a></li>
                    <li><a>Берёзовский</a></li>
                    <li><a>Верхняя Пышма</a></li>
                    <li><a>Екатеринбург</a></li>
                    <li><a>Каменск-Уральский</a></li>
                    <li><a>Краснотурьинск</a></li>
                    <li><a>Нижний Тагил</a></li>
                    <li><a>Новоуральск</a></li>
                    <li><a>Первоуральск</a></li>
                    <li><a>Полевской</a></li>
                    <li><a>Ревда</a></li>
                    <li><a>Серов</a></li>
                    <li class="add_city">
                        Нет вашего города?
                        <a>Добавить...</a>
                    </li>
                </ul>	
            </div>
        </div>
        <div class="inf_block">
        	всего объектов<br />
			в аренду 1460
        </div>
        <div class="inf_block">
        	Телефон в Екатеринбурге<br />
			(343) 344-60-27
        </div>
        <div class="menu_wrap">
            <ul class="main_menu rounded">
                <li><a href="">О проекте</a></li>
                <li><a href="">Контакты</a></li>
                <li><a href="">Справка</a></li>
                <li><a href="">Пункт меню</a></li>
                <li><a href="">Еще пункт</a></li>
            </ul>
            
            <a href="" class="green_link_block">Как работает com-arenda?</a>
            <a href="" class="green_link_block">Разместить объект</a>
        </div>
    </div>
    
    <div class="content_wrap">
    	<div class="aside">
            <div class="gray_block">
            	Знаете ли вы, что: Ученые обнаружили следы ранее неизвестной рептилии, обитавшей около 245 миллионов лет назад на территории современной Антарктики.
            </div>
            <div class="gray_block">
            	Почему кошка приносит нам мышей? По мнению кошки, ее хозяева по части ловли мышей являются, говоря школьным языком, настоящими «двоечниками». Но кошка готова оказать нам в этом сложном деле посильную помощь. Сытая кошка делится добычей со своими собратьями.
            </div>
            <div class="gray_block">
            	Член полярной экспедиции Георгия Седова, штурман корабля "Святой Фока" Н. Сахаров, обязан жизнью своей преданной собаке Штурке. Сахаров отморозил руки и добирался до корабля один в сопровождении собаки. Выбиваясь из сил, прошел по льду пятьдесят километров, временами терял сознание, впадая в забытье. Тогда Штурка садился возле хозяина, лаял, теребил его за одежду, пытаясь привести друга в чувства. Сахаров поднимался и продолжал идти.
            </div>
            
        </div>
				<div class="content">
					<a href="" class="history_back">Назад к результатам поиска</a>
					<div class="left_col_56perc">
						<div class="gallery_block_wrap">
							<a href="content_images/gallery_1_big.jpg"><img src="content_images/gallery_1_big.jpg" style="width:100%;" /></a><br/>
							<ul class="gallery_block" id="object_card_gallery">
								<li>
									<img class="arrow_thumbs" src="images/arr_thumbs_left.png"/>
								</li><li>
									<a href="content_images/gallery_1_big.jpg"><img src="content_images/gallery_1.jpg" /></a>
								</li><li>
									<a href="content_images/gallery_2_big.jpg"><img src="content_images/gallery_2.jpg" /></a>
								</li><li>
									<a href="content_images/gallery_3_big.jpg"><img src="content_images/gallery_3.jpg" /></a>
								</li><li>
									<img class="arrow_thumbs" src="images/arr_thumbs_right.png"/>
								</li>
							</ul>
						</div>
						<div class="green_top_border_block">
							<h2>На карте:</h2>
							<div class="map">
								<img src="temp/google_map.jpg"/>
							</div>
						</div>
						<div class="green_top_border_block">
							<h3>Дополнительно:</h3>
							Член полярной экспедиции Георгия Седова, штурман корабля "Святой Фока" Н. Сахаров, обязан жизнью своей преданной собаке Штурке. Сахаров отморозил руки и добирался до корабля один в сопровождении собаки. Выбиваясь из сил, прошел по льду пятьдесят километров, временами терял сознание, впадая в забытье. Почему кошка приносит нам
						</div>
						<div class="green_top_border_block">
							<h3>Документы:</h3>
							<div class="download_list_files">
								<a class="docs_any" href="">Скачать</a><!--<span class="subt"> (DOC-файл)</span>--><br/>
								<a class="docs_any" href="">Скачать</a><!--<span class="subt"> (XLS-файл)</span>-->
							</div><div class="download_list_title">
								<div><div class="download_list_title_lbl">Чертежи помещения</div><div class="download_list_title_dots m_0000_0000_0002_0121">&nbsp;</div></div>
								<div><div class="download_list_title_lbl">План эвакуации</div><div class="download_list_title_dots m_0000_0000_0002_0093">&nbsp;</div></div>
							</div>
						</div>
					</div>
					
					<div class="right_col_42perc">
						<div class="object_main_inf">
							<div class="object_main_inf_date">
								13.06.2012
							</div><div class="object_main_inf_type">
								Офисное помещение
							</div><div class="object_main_inf_class">
								Аренда офиса класса «C»
							</div><div class="object_main_inf_price">
								57 750.00 руб.
							</div><br/><div class="object_main_inf_h">
								Район<br/>Адрес<br/>Помещение<br/>Цена за м<sup>2</sup><br/>Стоимость<br/>Сдает<br/>Контактное лицо<br/>Телефон
							</div><div class="object_main_inf_d">
								Центр<br/>ул. Радищева, 28<br/>Офисный центр <a href="">"Парус"</a><br/>650 руб.<br/>11050 руб.<br/>собственник<br/><a href="">Дьяченко О</a><br/>+7-912-229-77-89
							</div>
						</div>
						<ul class="object_actions">
							<!--<li class="object_actions_zip"><a href="">Скачать</a><span class="subt"> (ZIP-файл)</span></li>
							<li class="object_actions_pdf"><a href="">Скачать</a><span class="subt"> (PDF-файл)</span></li>-->
							<li class="object_actions_abuse"><a href="">Пожаловаться</a></li>
							<li class="object_actions_cptnp"><a href="">Копировать в блокнот</a><!--<span class="success">в блокноте</span></li>-->
							<li class="object_actions_print"><a href="">Распечатать</a></li>
						</ul>
						<ul class="object_actions">
							<li class="object_actions_zip"><a href="">Скачать</a></a><span class="subt"> (ZIP-файл)</span></li>
							<li class="object_actions_pdf"><a href="">Скачать</a><span class="subt"> (PDF-файл)</span></li>
						</ul>				

						<div class="green_top_border_block">
							<h2>Подробнее</h2>
							<table class="items_2">
								<tbody>
									<tr><td class="t">Этажность здания</td><td>12</td></tr>
									<tr class="even"><td class="t">Покрытие пола</td><td class="check">Антипылевое покрытие</td></tr>
									<tr><td class="t">Видеонаблюдение</td><td>нет</td></tr>
									<tr class="even"><td class="t">Система пожаротушения</td><td>нет</td></tr>
									<tr><td class="t">Пропускная система</td><td class="check">есть</td></tr>
									<tr class="even"><td class="t">Система вентиляции</td><td>естественная</td></tr>
									<tr><td class="t">Кондиционирование</td><td>нет</td></tr>
									<tr class="even"><td class="t">Форма договора</td><td>Прямой договор</td></tr>
									<tr><td class="t">Интернет</td><td>нет</td></tr>
									<tr class="even"><td class="t">Телефония</td><td class="check">Линолеум</td></tr>
									<tr><td class="t">Парковка</td><td>нет</td></tr>
									<tr class="even"><td class="t">Количество закрепленных мест</td><td>нет</td></tr>
									<tr><td class="t">Возможность размещения рекламы</td><td class="check">есть</td></tr>
									<tr class="even"><td class="t">Этажность здания</td><td>12</td></tr>
									<tr><td class="t">Покрытие пола</td><td class="check">Линолеум</td></tr>
									<tr class="even"><td class="t">Видеонаблюдение</td><td>нет</td></tr>
									<tr><td class="t">Система пожаротушения</td><td>нет</td></tr>
									<tr class="even"><td class="t">Пропускная система</td><td class="check">есть</td></tr>
									<tr><td class="t">Система вентиляции</td><td>естественная</td></tr>
									<tr class="even"><td class="t">Кондиционирование</td><td>нет</td></tr>
									<tr><td class="t">Форма договора</td><td>Прямой договор</td></tr>
									<tr class="even"><td class="t">Интернет</td><td>нет</td></tr>
								</tbody>
							</table>
						</div>
					</div><div class="green_top_border_block clear_both">
						<div class="also_to_view_wrap">
							<div class="recommend"><span>Рекомендуем к просмотру</span><div class="border"></div></div>
							<ul class="also_to_view">
								<li>
									<a href=""><img src="content_images/slider_1.jpg"/><div>Офис<br/>Центр, 220м<sup>2</sup></div></a>
								</li><li>
									<a href=""><img src="content_images/slider_2.jpg"/><div>Торговая площадь<br/>Академический, 300м<sup>2</sup></div></a>
								</li><li>
									<a href=""><img src="content_images/slider_3.jpg"/><div>Торговая площадь<br/>Завокзальный, 256м<sup>2</sup></div></a>
								</li><li>
									<a href=""><img src="content_images/slider_1.jpg"/><div>Торговая площадь<br/>Завокзальный, 256м<sup>2</sup></div></a>
								</li><li>
									<a href=""><img src="content_images/slider_2.jpg"/><div>Торговая площадь<br/>Академический, 300м<sup>2</sup></div></a>
								</li><li>
									<a href=""><img src="content_images/slider_3.jpg"/><div>Торговая площадь<br/>Завокзальный, 256м<sup>2</sup></div></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
    </div>
</div>
    <div class="footer">
        <div class="wrap_for_max_width">
            <img class="small_logo" src="images/logo_small.png" />
            © 2012 Портал Ком-Аренда<br />
            Контактный телефон: (343) 344-60-27<br />
            <a href="mailto:office@com-arenda.ru">office@com-arenda.ru</a>
        </div>
    </div>


<div id="gallery_popup">
	<a class="gallery_close"></a>
	<div class="gallery_big_image">
    	<img src="content_images/gallery_1_big.jpg" />
    </div>
	<ul id="gallery_carousel" class="jcarousel-skin-tango">
    	<li><a rel="content_images/gallery_1_big.jpg"><img src="content_images/gallery_1.jpg" /></a></li>
		<li><a rel="content_images/gallery_2_big.jpg"><img src="content_images/gallery_2.jpg" /></a></li>
		<li><a rel="content_images/gallery_3_big.jpg"><img src="content_images/gallery_3.jpg" /></a></li>
		<li><a rel="content_images/gallery_4_big.jpg"><img src="content_images/gallery_4.jpg" /></a></li>
        <li><a rel="content_images/gallery_5_big.jpg"><img src="content_images/gallery_5.jpg" /></a></li>
		<li><a rel="content_images/gallery_6_big.jpg"><img src="content_images/gallery_6.jpg" /></a></li>
		<li><a rel="content_images/gallery_7_big.jpg"><img src="content_images/gallery_7.jpg" /></a></li>
    </ul>
</div>


</body>
</html>