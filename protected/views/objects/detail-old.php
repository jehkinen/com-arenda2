<?php
	$bUrl = Yii::app()->baseUrl;
	// echo CHtml::scriptFile($bUrl.'/js/gallery.js');
	if ($userTenant) {
		echo CHtml::cssFile($bUrl.'/css/modal.css'); 
		echo CHtml::scriptFile($bUrl.'/js/jquery.simplemodal.js');
	}
	$noFoto=(empty($aFotos));
	switch ($item->kind) {
		case 1 : {$lbClass = 'торгового помещения'; break;}
		case 2 : {$lbClass = 'склада/производственного помещения'; break;}
		default: $lbClass = 'офиса';
	}
	switch ($item->itemType) {
		case 0 : { // Простой объект
			switch ($item->kind) {
				case 1 : {$lbTitle = 'Торговое помещение'; break;}
				case 2 : {$lbTitle = 'Склад или производственное помещение'; break;}
				default: $lbTitle = 'Офисное помещение';
			}
			break;
			$lbTitle.=' "'.$item->name.'"';
		} 
		case 1 : { // Объект-контейнер
			switch ($item->kind) {
				case 1 : {$lbTitle = 'Торговый центр'; break;}
				case 2 : {$lbTitle = 'Складской или производственный центр'; break;}
				default: $lbTitle = 'Офисный центр';
			}
			$lbTitle.=' "'.$item->name.'"';
			break;
		}
		case 2 : { // Вложенный объект
			switch ($item->kind) {
				case 1 : {$lbTitle = 'Помещение '.$item->name.' в торговом центре'; break;}
				case 2 : {$lbTitle = 'Помещение '.$item->name.' в складском или производственном центре'; break;}
				default: $lbTitle = 'Офис '.$item->name.' в офисном центре';
			}
			$lbTitle.=' "'.$item->parentName.'"';
			break;
		}
	}
	/*
	$ref = Yii::app()->request->urlReferrer;
	CVarDumper::Dump(Yii::app()->request->urlReferrer, 3, true); die;
	*/
?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#thumbs a').click(function(){
			var Small_obj = $(this);
			var mediumPath = Small_obj.attr('href');
			var largePath = Small_obj.attr('title');
			$('#firstImg').attr({'src':mediumPath, 'alt':largePath});
			return false;
		});

		$('#firstImg').click(function(){
			var openPic = $(this).attr('alt');
			window.open('<?php echo Yii::app()->params->site_base;?>storage/images/'+openPic, '_blank');
		});
	});
</script>
<?php if ($userTenant):?>
<script type="text/javascript">
	$(document).ready(function(){

		$('a.tonotepad').bind('click', function(e){
			var aLink = $(e.target);
			var id_obj = aLink.attr('id');
			e.preventDefault();
			show_modal('#owner-delete', 'ods', '<span>Блокнот</span>', 'Скопировать объект в блокнот?', '30%', function(){
				$.ajax({
					url: "/ajax/notepad_add",
					data: {id: id_obj},
					type: 'post',
					success: function(){
						aLink.html('Объект в блокноте');
					}
				});
			});
		});
		$('a.claim').bind('click', function(e){
			var labl = $(e.target);
			var obj_id = labl.attr('id');
			e.preventDefault();
			show_modal('#mwindow', 'ocs', '<span>Жалоба</span>', '', '20%', function(){
				var selOption = $('option:selected');
				var claim = selOption.val();
				if (typeof(claim) != 'undefined'){
					$.ajax({
						url: "/ajax/send_claim",
						data: {id: obj_id, clm: claim},
						type: 'post',
						success: function(data){}
					});
				}
			});
		});
	});
	
function show_modal(box, container, header, message, top, callback){
	$(box).modal({
		closeHTML: '<a href="#" title="Закрыть" class="x-close"><img src="/images/modal/mw-close.jpg" /></a>',
		position: [top,],
		opacity: 30,
		overlayId: 'overlay',
		containerId: container,
		closeClass: 'close',
		onShow: function (dialog){
			var modal = this;

			$('.header', dialog.data[0]).append(header);
			$('.message', dialog.data[0]).append(message);

			$('.yes', dialog.data[0]).click(function () {
				if ($.isFunction(callback)) {
					callback.apply();
				}
				modal.close();
			});
		}
	});
}
</script>
<div id="owner-delete">
	<div class="header"></div>
	<div class="message"></div>
	<div class="buttons">
		<div class="yes">Да</div><div class="no close">Нет</div>
	</div>
</div>
<div id="mwindow">
	<div class="header"></div>
	<div class="message">
		Мной обнаружено:<br />
		<select name="claim">
			<option value="1" selected="selected">Агент</option>
			<option value="2">Давно сдал и не убирает</option>
			<option value="3">Интересная личность</option>
		</select>
	</div>
	<div class="buttons">
		<div class="yes">Подать</div><div class="no close">Отмена</div>
	</div>
</div>
<?php endif;?>

<div class="a_privew">
<?php if ($item->itemType==2):?>
	<div class="link_center"><?php echo CHtml::link('Бизнес-Центр "'.$item->parentName.'"', array('objects/detail', 'id'=>$item->parent));?></div>
<?php endif;?>
	<img src="<?php echo $bUrl;?>/images/left_str.gif" alt="" /> <a href="javascript: history.go(-1);">Вернуться на предыдущую страницу</a>
</div><!-- /a_privew -->
<div class="<?php echo (($item->itemType==1)?'green_title':'orange_title');?>"><?php echo $lbTitle;?></div>
<div class="menu_card">
	<ul>
<?php if ($userTenant && $item->itemType!=1):?>
		<li><div><img src="<?php echo $bUrl;?>/images/ico_mc_01.png" alt="" /> <a href="#" class="tonotepad" id="obj<?php echo($id_obj);?>">Копировать в блокнот</a></div></li>
<?php endif;?>
		<li><div><img src="<?php echo $bUrl;?>/images/ico_mc_02.png" alt="" /> <a href="<?php echo $this->createUrl('objects/print', array('id'=>$id_obj)); ?>">Распечатать</a></div></li>
<?php if ($userTenant):?>
		<li><div><img src="<?php echo $bUrl;?>/images/ico_mc_03.png" alt="" /> <a href="#" class="claim" id="clm<?php echo($id_obj);?>">Пожаловаться</a></div></li>
<?php endif;?>
		<li><div><img src="<?php echo $bUrl;?>/images/ico_mc_04.png" alt="" /> <a href="<?php echo $this->createUrl('objects/download', array('id'=>$id_obj)); ?>">Скачать</a></div></li>
		<li><div><img src="<?php echo $bUrl;?>/images/ico_mc_04.png" alt="" /> <a href="<?php echo $this->createUrl('objects/load', array('id'=>$id_obj)); ?>">Загрузить</a></div></li>
<?php if ($item->itemType==1):?>
		<li><div><a href="<?php echo $this->createUrl('objects/contain', array('id'=>$id_obj)); ?>">Список помещений</a></div></li>
<?php endif;?>
	</ul>
</div>
<div class="foto_card"<?php if ($noFoto) echo 'style="display:none;"';?>>
<?php if (!$noFoto):?>
<img id="firstImg" style="cursor: hand;" src="<?php echo $bUrl;?>/storage/images/_thumbs/<?php echo $aFotos[0]->name_medium;?>" alt="<?php echo $aFotos[0]->img;?>" />
	<div id="thumbs" class="card_mini_gall">
<?php foreach ($aFotos as $f): ?>
		<a href="<?php echo $bUrl;?>/storage/images/_thumbs/<?php echo $f->name_medium;?>" title="<?php echo $f->img;?>"><img src="<?php echo $bUrl;?>/storage/images/_thumbs/<?php echo $f->name_small;?>" alt="" /></a>
<?php endforeach;?>
	</div>
<?php endif;?>
</div>
<div class="text_card">
		<div class="text_card_info">
		<h3><?php echo $item->city;?>, <?php echo $item->street;?>, <?php echo $item->house;?></h3>
		<i>Аренда <?php echo $lbClass;?> класса "<?php echo $item->class;?>"</i><br /><?php echo number_format($item->total_item_sqr, 2, '.', ' ');?> м<sup>2</sup> / <?php echo number_format($item->price, 2, '.', ' ');?> р.<br />
		<?php echo $item->about;?>
		</div>
	<div class="phone_card">Телефон: <span><?php echo $item->phone;?></span></div>
	<div class="name_autor_card">
		<?php echo CHtml::link($item->firstname.' '.$item->lastname.'.', array('person/card', 'id'=>$item->owner_id));?>
	</div>
<?php if (!empty($aFiles)):?>
	<div class="file_table">
		<h4>Прикрепленные файлы:</h4>
		<div class="file_table_background">
			<div class="file_table_background_02">
				<table cellspacing="0" cellpadding="0">
<?php foreach($aFiles as $f):?>
					<tr><td class="td_01"><?php echo $f->name;?></td><td><?php echo CHtml::image($bUrl.'/images/ico_mc_04.png');?> <?php echo CHtml::link('скачать', array('/storage/files/objs/'.$f->filename));?></td></tr>
<?php endforeach;?>
				</table>
			</div>
		</div>
	</div>
<?php endif;?>
</div>
<br /><br />
<div class="podrobnee_card">
<h3>Подробно</h3>
<div class="podrobnee_table">
<table cellspacing="0" cellpadding="0">
	<tr><td class="td_01">Функциональное назначение</td><td><?php echo $item->functionality;?></td></tr>
	<tr class="back_tr"><td class="td_01">Район</td><td><?php echo $item->district;?></td></tr>
	<tr><td class="td_01">Улица</td><td><?php echo $item->street;?></td></tr>
	<tr class="back_tr"><td class="td_01">Дом</td><td><?php echo $item->house;?></td></tr>
	<tr><td class="td_01">Корпус</td><td><?php echo $item->housing;?></td></tr>
</table>
</div>
<div class="podrobnee_table">
<table cellspacing="0" cellpadding="0">
	<tr><td class="td_01">Тип сооружения</td><td><?php echo $item->kind_structure;?></td></tr>
	<tr class="back_tr"><td class="td_01">Название</td><td><?php echo $item->name;?></td></tr>
	<tr><td class="td_01">Класс</td><td><?php echo $item->class;?></td></tr>
	<tr class="back_tr"><td class="td_01">Этаж</td><td><?php echo $item->storey;?></td></tr>
	<tr><td class="td_01">Этажность</td><td><?php echo $item->storeys;?></td></tr>
</table>
</div>
<?php
switch ($item->kind) {
	case 0 : {$s='info-ofice'; break;}
	case 1 : {$s='info-torg'; break;}
	case 2 : {$s='info-sklad'; break;}
	default:  $s='info-default';
}
$this->renderPartial($s, array('item' =>$item));
?>
</div>
<br /><br /><br /><br />
<?php $this->widget('SimilarObjects', array('selfId'=>$item->id, 'kind'=>$item->kind, 'district'=>$item->districtID, 'price'=>$item->price, 'square'=>$item->total_item_sqr));?>