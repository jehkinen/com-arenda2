<style type="text/css">
	#doc2, #doc3	
		{display: none;}
	#addBox, #addDocBox{
		margin: 10px 0;
	}
	div.master div.bordered {width:550px;}
</style>
<script type="text/javascript">
	$(document).ready(function(){		var addDocBox_obj = $('#addDocBox');
		addDocBox_obj.html('<a id="addDoc" href="#">[+] прикрепить еще файл</a>');
		$('#addDoc').click(function(){
			var NextDoc_obj = $('#documents').find('div:hidden:first');
			var id = NextDoc_obj.attr('id');
			if (id == 'doc3')
				addDocBox_obj.html('');
			NextDoc_obj.show();
			return false;
		});
	})
</script>
<div class="master">
	<div class="head">
		<h3>Добавление объекта</h3>/ Файлы
	</div>
	<div class="road">
		<ul>
			<li class="pred first"><div><?php echo CHtml::link('Категория', array('object/add', 'step'=>'step1')); ?></div><span>&rarr;</span></li>
			<li class="pred"><div><?php echo CHtml::link('Локация', array('object/add', 'step'=>'step2')); ?></div><span>&rarr;</span></li>
			<li class="pred"><div><?php echo CHtml::link('Параметры', array('object/add', 'step'=>'step3')); ?></div><span>&rarr;</span></li>
			<li class="pred"><div><?php echo CHtml::link('Описание', array('object/add', 'step'=>'step4')); ?></div><span>&rarr;</span></li>
			<li class="active"><div>Файлы</div></li>
		</ul>
	</div><!--  /road -->
	<div class="form">
		<?php echo CHtml::form('', 'post', array('enctype'=>'multipart/form-data')); ?>
		<?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:'); ?>

		<div class="bordered" title="Добавление файлов">
			<h4>Добавление файлов</h4>
			<div id="documents">
				<div id="doc1">Название: <?php echo CHtml::activeTextField($form, '[1]docname', array('value'=>'', 'size'=>20, 'maxlength'=>23)); ?> <?php echo CHtml::activeFileField($form, '[1]docums'); ?></div>
				<div id="doc2">Название: <?php echo CHtml::activeTextField($form, '[2]docname', array('value'=>'', 'size'=>20, 'maxlength'=>23)); ?> <?php echo CHtml::activeFileField($form, '[2]docums'); ?></div>
				<div id="doc3">Название: <?php echo CHtml::activeTextField($form, '[3]docname', array('value'=>'', 'size'=>20, 'maxlength'=>23)); ?> <?php echo CHtml::activeFileField($form, '[3]docums'); ?></div>
				<div id="addDocBox"></div>
			</div>
		</div>

	<div class="buttons">
		<div class="cancel">
			x <?php echo CHtml::link('Начать заново', array('object/add_new')); ?>
		</div>
		<div class="pred">&larr; <?php echo CHtml::link('Назад', array('object/add', 'step'=>'step4')); ?></div>
		<div><?php echo CHtml::submitButton('Сохранить', array('id'=>'submit', 'class'=>'next')); ?></div>
	</div><!--buttons-->
<?php echo CHtml::endForm(); ?>
	</div><!--form-->
</div><!-- master-->
<br /><br />
Если затрудняетесь, посетите страницу <a href="/">справки</a>
