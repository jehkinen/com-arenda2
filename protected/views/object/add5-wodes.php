<style type="text/css">
	input#Objects_add_2_image,
	input#Objects_add_3_image,
	input#Objects_add_4_image,
	input#Objects_add_5_image,
	#doc2, #doc3	
		{display: none;}
	#addBox, #addDocBox{
		margin: 10px 0;
	}
	#images, #documents{		background-color: #cccccc;
		padding: 10px 10px 5px;	}
</style>
<script type="text/javascript">
	$(document).ready(function(){		var addBox_obj = $('#addBox');
		addBox_obj.html('<a id="add" href="#">[+] добавить еще картинку</a>');
		$('#add').click(function(){			var NextFile_obj = $('#images').find('input[type=file]:hidden:first');
			var id = NextFile_obj.attr('id');
			if (id == 'Objects_add_5_image')
				addBox_obj.html('');
			NextFile_obj.show();
			return false;
		});
		var addDocBox_obj = $('#addDocBox');
		addDocBox_obj.html('<a id="addDoc" href="#">[+] прикрепить еще файл</a>');
		$('#addDoc').click(function(){
			var NextDoc_obj = $('#documents').find('div:hidden:first');
			var id = NextDoc_obj.attr('id');
			if (id == 'doc3')
				addDocBox_obj.html('');
			NextDoc_obj.show();
			return false;
		});
	})
</script>
<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/form.css'); ?>
<?php echo CHtml::link('Начать заново', array('object/add_new')); ?><br /><br />
<span style="background-color: #99ff99; padding: 5px 10px;"><?php echo CHtml::link('Категория', array('object/add', 'step'=>'step1')); ?></span> >
<span style="background-color: #99ff99; padding: 5px 10px;"><?php echo CHtml::link('Локация', array('object/add', 'step'=>'step2')); ?></span> >
<span style="background-color: #99ff99; padding: 5px 10px;"><?php echo CHtml::link('Параметры', array('object/add', 'step'=>'step3')); ?></span> >
<span style="background-color: #99ff99; padding: 5px 10px;"><?php echo CHtml::link('Описание', array('object/add', 'step'=>'step4')); ?></span> >
<span style="background-color: #33aa33; padding: 5px 10px; font-weight: bold;">Файлы</span>
<br /><br />
<div class="form">
  <?php echo CHtml::form('', 'post', array('enctype'=>'multipart/form-data')); ?>
  <?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:'); ?>
<div class="row">
  <fieldset title="Загрузка фотографий">
    <legend><?php echo CHtml::activeLabel($form, 'image'); ?></legend>
    <div id="images">
      <div><?php echo CHtml::activeFileField($form, '[1]image'); ?></div>
      <div><?php echo CHtml::activeFileField($form, '[2]image'); ?></div>
      <div><?php echo CHtml::activeFileField($form, '[3]image'); ?></div>
      <div><?php echo CHtml::activeFileField($form, '[4]image'); ?></div>
      <div><?php echo CHtml::activeFileField($form, '[5]image'); ?></div>
      <div id="addBox"></div>
    </div>
  </fieldset>
</div>
<div class="row">
  <fieldset title="Добавление файлов">
    <legend><?php echo CHtml::activeLabel($form, 'docums'); ?></legend>
    <div id="documents">
      <div id="doc1">Название: <?php echo CHtml::activeTextField($form, '[1]docname', array('value'=>'', 'size'=>20, 'maxlength'=>23)); ?> <?php echo CHtml::activeFileField($form, '[1]docums'); ?></div>
      <div id="doc2">Название: <?php echo CHtml::activeTextField($form, '[2]docname', array('value'=>'', 'size'=>20, 'maxlength'=>23)); ?> <?php echo CHtml::activeFileField($form, '[2]docums'); ?></div>
      <div id="doc3">Название: <?php echo CHtml::activeTextField($form, '[3]docname', array('value'=>'', 'size'=>20, 'maxlength'=>23)); ?> <?php echo CHtml::activeFileField($form, '[3]docums'); ?></div>
      <div id="addDocBox"></div>
    </div>
  </fieldset>
</div>
<div class="row buttons">
  <?php echo CHtml::link('Назад', array('object/add', 'step'=>'step4')); ?> <?php echo CHtml::submitButton('Добавить', array('id'=>"submit")); ?>
</div>
<?php echo CHtml::endForm(); ?>
</div><!--form-->
