<div class="master">
	<div class="head">
		<h3>Добавление объекта</h3>/ Описание
	</div>
	<div class="road">
		<ul>
			<li class="pred first"><div><?php echo CHtml::link('Категория', array('object/add', 'step'=>'step1')); ?></div><span>&rarr;</span></li>
			<li class="pred"><div><?php echo CHtml::link('Локация', array('object/add', 'step'=>'step2')); ?></div><span>&rarr;</span></li>
			<li class="pred"><div><?php echo CHtml::link('Параметры', array('object/add', 'step'=>'step3')); ?></div><span>&rarr;</span></li>
			<li class="active"><div>Описание</div><span>&rarr;</span></li>
			<li class="next"><div>Файлы</div></li>
		</ul>
	</div><!--  /road -->
	<div class="form">
		<?php echo CHtml::form('', 'post', array('enctype'=>'multipart/form-data')); ?>
		<?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:'); ?>

<?php if (($kind<2) && ($item_type!=1)):?>
		<?php echo CHtml::activeLabel($form, 'repair'); ?>
		<?php echo CHtml::dropDownList('Objects_add[repair]', $vrepair, $list_repair); ?>
<?php endif;?>
<?php if ($hasparent==0):?>
		<?php echo CHtml::activeLabel($form, 'parking'); ?>
		<?php echo CHtml::dropDownList('Objects_add[parking]', $vpark, $list_park); ?>
<?php endif;?>
		<?php echo CHtml::activeLabel($form, 'fixed_qty'); ?>
		<?php echo CHtml::activeTextField($form, 'fixed_qty', array('value'=>$vfixqty, 'size'=>10, 'maxlength'=>10, 'class'=>'inputbox')); ?>

		<p><?php if ($vvideo=='' || $vvideo=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'video', $cba); echo CHtml::activeLabel($form, 'video', array('class'=>'chkbxSpan'));?></p>

		<p><?php if ($vfirefi=='' || $vfirefi=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'firefighting', $cba); echo CHtml::activeLabel($form, 'firefighting', array('class'=>'chkbxSpan')); if ($kind>1) echo ' *'; ?></p>

<?php if ($kind!=1):?>
	<?php if ($vaccess=='' || $vaccess=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'access', $cba); echo CHtml::activeLabel($form, 'access', array('class'=>'chkbxSpan'));?></p>
<?php endif;?>

<?php if ($kind>0):?>
		<?php echo CHtml::activeLabel($form, 'welfare'); ?>
		<?php echo CHtml::activeTextField($form, 'welfare', array('value'=>$vwelfare, 'size'=>50, 'maxlength'=>255, 'class'=>'inputbox')); ?>
<?php endif;?>
<?php if (($hasparent==0) && ($kind>1)):?>
		<p><?php if ($vheating=='' || $vheating=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'heating', $cba); echo CHtml::activeLabel($form, 'heating', array('class'=>'chkbxSpan')); ?></p>
<?php endif;?>
<?php if ($kind>1):?>
		<?php echo CHtml::activeLabel($form, 'pallet_capacity'); ?>
		<?php echo CHtml::activeTextField($form, 'pallet_capacity', array('value'=>$vpalcap, 'size'=>15, 'maxlength'=>15, 'class'=>'inputbox')); ?>
		
		<?php echo CHtml::activeLabel($form, 'shelf_capacity'); ?>
		<?php echo CHtml::activeTextField($form, 'shelf_capacity', array('value'=>$vshelcap, 'size'=>15, 'maxlength'=>15, 'class'=>'inputbox')); ?>
<?php endif;?>
<?php if (($kind!=1) && ($item_type!=1)):?>
		<div class="<?php if ($kind>1) echo 'req'; ?>"><?php echo CHtml::activeLabel($form, 'flooring'); if ($kind>1) echo ' *'; ?></div>
		<?php echo CHtml::dropDownList('Objects_add[flooring]', $vflooring, $list_flooring); ?>
<?php endif;?>
		<div class="<?php if ($kind==1) echo 'req'; ?>"><?php echo CHtml::activeLabel($form, 'ventilation'); if ($kind==1) echo ' *'; ?></div>
		<?php echo CHtml::dropDownList('Objects_add[ventilation]', $vventil, $list_ventil); ?>
<?php if ($kind<2):?>
		<p><?php if ($vaircon=='' || $vaircon=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'air_condit', $cba); echo CHtml::activeLabel($form, 'air_condit', array('class'=>'chkbxSpan')); ?></p>
<?php endif;?>
<?php if ($hasparent==0):?>
		<p><?php if ($vreklam=='' || $vreklam=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'can_reclame', $cba); echo CHtml::activeLabel($form, 'can_reclame', array('class'=>'chkbxSpan')); ?></p>
<?php endif;?>
<?php if ($kind>0):?>
		<p><?php if ($vrampant=='' || $vrampant=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'rampant', $cba); echo CHtml::activeLabel($form, 'rampant', array('class'=>'chkbxSpan')); ?></p>
<?php endif;?>
<?php if (($hasparent==0) && ($kind>1)):?>
<fieldset><legend>Подъездные пути</legend>
		<p><?php if ($vaways=='' || $vaways=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'autoways', $cba); echo CHtml::activeLabel($form, 'autoways', array('class'=>'chkbxSpan')); ?></p>
		<p><?php if ($vrways=='' || $vrways=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'railways', $cba); echo CHtml::activeLabel($form, 'railways', array('class'=>'chkbxSpan')); ?></p>
</fieldset>
<?php endif;?>
		<?php echo CHtml::activeLabel($form, 'about'); ?>
		<?php echo CHtml::textArea('Objects_add[about]', $vabout, array('rows'=>5, 'cols'=>70)); ?>

	<div class="buttons">
		<div class="cancel">
			x <?php echo CHtml::link('Начать заново', array('object/add_new')); ?>
		</div>
		<div class="pred">&larr; <?php echo CHtml::link('Назад', array('object/add', 'step'=>'step3')); ?></div>
		<div><?php echo CHtml::submitButton('Дальше', array('id'=>'submit', 'class'=>'next')); ?></div>
	</div><!--buttons-->
<?php echo CHtml::endForm(); ?>
	</div><!--form-->
</div><!-- master-->
<br /><br />
Если затрудняетесь, посетите страницу <a href="/">справки</a>
