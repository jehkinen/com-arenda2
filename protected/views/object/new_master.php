<? $form = $this->beginWidget('CActiveForm'); ?>


<?php echo $form->Label($model, 'city'); ?>

<?php echo $form->DropDownList($model,
    'city',
    Chtml::listData(City::model()->findAll(), 'name', 'name' ),
    array('prompt'=>'Выберите город',
        'onchange'=> CHtml::ajax(
            array(
                'type'=>'get',
                'url'=>CController::createUrl('object/add'),
                'data' => array('city' => 'js:$(this).val()'),
                'update'=> '#districts'
            )
        )
    )
);?>

<?php echo $form->Label($model, 'district'); ?>
<?php echo $form->DropDownList($model,
    'district',
    array(),
    array('prompt'=>'Выберите район',
        'onchange'=> CHtml::ajax(
            array(
                'type'=>'get',
                'url'=>CController::createUrl('object/update'),
                'data' => array('district' => 'js:$(this).val()'),
                'update'=> '#street'
            )
        ),
        'id'=>'districts',
));?>

<? $this->endWidget(); ?>
