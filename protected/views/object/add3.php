<script type="text/javascript">
  $(document).ready(function(){    $('div#providers').hide();   <?php /* Вначале прячем div-ы в который "вложенны" свплывающие списки */?>
    $('div#tel_company').hide();

    $('input#Objects_add_internet').change(function(){
        if($(this).is(":checked")){
          $('div#providers').show(); 
        }else{
          $('div#providers').hide(); 
        }
    });

    $('input#Objects_add_telephony').change(function(){
        if($(this).is(":checked")){
          $('div#tel_company').show();
        }else{
          $('div#tel_company').hide();
        }
    });  });
</script>
<div class="master">
	<div class="head">
		<h3>Добавление объекта</h3>/ Параметры
	</div>
	<div class="road">
		<ul>
			<li class="pred first"><div><?php echo CHtml::link('Категория', array('object/add', 'step'=>'step1')); ?></div><span>&rarr;</span></li>
			<li class="pred"><div><?php echo CHtml::link('Локация', array('object/add', 'step'=>'step2')); ?></div><span>&rarr;</span></li>
			<li class="active"><div>Параметры</div><span>&rarr;</span></li>
			<li class="next"><div>Описание</div><span>&rarr;</span></li>
			<li class="next"><div>Файлы</div></li>
		</ul>
	</div><!--  /road -->
	<div class="form">
		<?php echo CHtml::form(); ?>
		<?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:'); ?>

		<?php echo CHtml::activeLabel($form, 'contr_condition'); ?>
		<?php echo CHtml::dropDownList('Objects_add[contr_condition]', $vcconds, $list_cconds); ?>

		<div class="req"><?php echo CHtml::activeLabel($form, 'price'); ?> *</div>
		<?php echo CHtml::activeTextField($form, 'price', array('value'=>$vprice, 'size'=>15, 'maxlength'=>15, 'class'=>'inputbox')); ?>

		<?php echo CHtml::activeLabel($form, 'pay_utilities'); ?>
		<?php echo CHtml::dropDownList('Objects_add[pay_utilities]', $vputils, $list_putils); ?>
<?php if ($item_type==1) $s = 'Общая площадь объекта'; else $s = 'Площадь';?>
		<div class="req"><label for="Objects_add_total_item_sqr"><?php echo $s;?></label> *</div>
		<?php echo CHtml::activeTextField($form, 'total_item_sqr', array('value'=>$vtisqr, 'size'=>15, 'maxlength'=>15, 'class'=>'inputbox')); ?>
<?php if (($hasparent==0) && ($kind>0)):?>
		<p><?php if ($vslift=='' || $vslift=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'service_lift', $cba); echo CHtml::activeLabel($form, 'service_lift', array('class'=>'chkbxSpan')); ?></p>
<?php endif;?>
		<p><?php if ($vinet=='' || $vinet=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'internet', $cba); echo CHtml::activeLabel($form, 'internet', array('class'=>'chkbxSpan')); ?></p>
		<div id="providers">
			<?php echo CHtml::activeLabel($form, 'providers'); ?>
			<?php echo CHtml::dropDownList('Objects_add[providers]', $vproviders, $list_providers); ?>
		</div>
		<p><?php if ($vtelephony=='' || $vtelephony=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'telephony', $cba); echo CHtml::activeLabel($form, 'telephony', array('class'=>'chkbxSpan')); ?></p>
		<div id="tel_company">
			<?php echo CHtml::activeLabel($form, 'tel_company'); ?>
			<?php echo CHtml::dropDownList('Objects_add[tel_company]', $vtcomp, $list_tcomp); ?>
		</div>
<?php if ($kind>0):?>
		<div class="<?php if ($kind>0) echo 'req'; ?>"><?php echo CHtml::activeLabel($form, 'ceiling_height'); if ($kind>0) echo ' *'; ?></div>
		<?php echo CHtml::activeTextField($form, 'ceiling_height', array('value'=>$vcellh, 'size'=>15, 'maxlength'=>15, 'class'=>'inputbox')); ?>		
<?php endif;?>
<?php if ($kind>1):?>
		<?php echo CHtml::activeLabel($form, 'work_height'); ?>
		<?php echo CHtml::activeTextField($form, 'work_height', array('value'=>$vworkh, 'size'=>15, 'maxlength'=>15, 'class'=>'inputbox')); ?>
		
		<p><?php if ($vcathead=='' || $vcathead=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'cathead', $cba); echo CHtml::activeLabel($form, 'cathead', array('class'=>'chkbxSpan')); ?></p>
<?php endif;?>
<?php if ($kind>0):?>
		<?php echo CHtml::activeLabel($form, 'floor_load'); ?>
		<?php echo CHtml::activeTextField($form, 'floor_load', array('value'=>$vfload, 'size'=>15, 'maxlength'=>15, 'class'=>'inputbox')); ?>

		<div class="<?php if ($kind>1) echo 'req'; ?>"><?php echo CHtml::activeLabel($form, 'el_power'); if ($kind>1) echo ' *'; ?></div>
		<?php echo CHtml::activeTextField($form, 'el_power', array('value'=>$vepower, 'size'=>15, 'maxlength'=>15, 'class'=>'inputbox')); ?>
<?php endif;?>
<?php if ($kind>1):?>
		<?php echo CHtml::activeLabel($form, 'gates_qty'); ?>
		<?php echo CHtml::activeTextField($form, 'gates_qty', array('value'=>$vgatesq, 'size'=>15, 'maxlength'=>15, 'class'=>'inputbox')); ?>

		<?php echo CHtml::activeLabel($form, 'gates_height'); ?>
		<?php echo CHtml::activeTextField($form, 'gates_height', array('value'=>$vgatesh, 'size'=>15, 'maxlength'=>15, 'class'=>'inputbox')); ?>
<?php endif;?>

	<div class="buttons">
		<div class="cancel">
			x <?php echo CHtml::link('Начать заново', array('object/add_new')); ?>
		</div>
		<div class="pred">&larr; <?php echo CHtml::link('Назад', array('object/add', 'step'=>'step2')); ?></div>
		<div><?php echo CHtml::submitButton('Дальше', array('id'=>'submit', 'class'=>'next')); ?></div>
	</div><!--buttons-->
<?php echo CHtml::endForm(); ?>
	</div><!--form-->
</div><!-- master-->
<br /><br />
Если затрудняетесь, посетите страницу <a href="/">справки</a>
