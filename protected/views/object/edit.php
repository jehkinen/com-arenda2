<script type="text/javascript">
  $(document).ready(function(){
    $('div#providers').hide();   <?php /* Вначале прячем div-ы в который "вложенны" свплывающие списки */?>
    $('div#tel_company').hide();

    $('input#Objects_edit_internet').change(function(){
        if($(this).is(":checked")){
          $('div#providers').show(); 
        }else{
          $('div#providers').hide(); 
        }
    });

    $('input#Objects_edit_telephony').change(function(){
        if($(this).is(":checked")){
          $('div#tel_company').show();
        }else{
          $('div#tel_company').hide();
        }
    });
  });
</script>
<div class="master">
	<div class="head">
		<h3>Редактирование объекта</h3>
	</div>
	<div style="float:right; margin-right:100px;">Страница <?php echo CHtml::link('редактирования фотографий', array('object/imagesedit', 'id'=>$form->id));?></div>
	<div class="form">
		<?php echo CHtml::form('', 'post', array('enctype'=>'multipart/form-data')); ?>
		<?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:'); ?>


<?php //  файл add2  ?>

		<?php echo CHtml::activeLabel($form, 'functionality'); ?>
		<?php echo CHtml::dropDownList('Objects_edit[functionality]', $form->functionality, $list_funcs); ?>

		<div class="req"><?php echo CHtml::activeLabel($form, 'class'); ?> *</div>
		<?php echo CHtml::dropDownList('Objects_edit[class]', $form->class, $list_class); ?>

		<div class="<?php if ($form->kind>0) echo 'req'; ?>"><?php echo CHtml::activeLabel($form, 'kind_structure'); if ($form->kind>0) echo ' *'; ?></div>
		<?php echo CHtml::dropDownList('Objects_edit[kind_structure]', $form->kind_structure, $list_strucs); ?>

		<div class="req"><?php echo CHtml::activeLabel($form, 'name'); ?> *</div>
		<?php echo CHtml::activeTextField($form, 'name', array('value'=>$form->name, 'size'=>30, 'maxlength'=>47, 'class'=>'inputbox')); ?>

<?php if ($form->itemType!=2):?>
		<div class="req"><?php echo CHtml::activeLabel($form, 'district'); ?> *</div>
		<?php echo CHtml::dropDownList('Objects_edit[district]', $form->district, $list_distrs); ?>

		<div class="req"><?php echo CHtml::activeLabel($form, 'street'); ?> *</div>
		<?php echo CHtml::dropDownList('Objects_edit[street]', $form->street, $list_streets); ?>

		<div class="req"><?php echo CHtml::activeLabel($form, 'house'); ?> *</div>
		<?php echo CHtml::activeTextField($form, 'house', array('value'=>$form->house, 'size'=>10, 'maxlength'=>15, 'class'=>'inputbox')); ?>
<?php endif;?>

		<?php echo CHtml::activeLabel($form, 'housing'); ?>
		<?php echo CHtml::activeTextField($form, 'housing', array('value'=>$form->housing, 'size'=>10, 'maxlength'=>15, 'class'=>'inputbox')); ?>

<?php if ($form->itemType!=1):?>
		<div class="<?php if ($form->kind<2) echo 'req'; ?>"><?php echo CHtml::activeLabel($form, 'storey'); if ($form->kind<2) echo ' *'; ?></div>
		<?php echo CHtml::activeTextField($form, 'storey', array('value'=>$form->storey, 'size'=>10, 'maxlength'=>3, 'class'=>'inputbox')); ?>
<?php endif;?>

<?php if ($form->itemType!=2):?>
		<div class="req"><?php echo CHtml::activeLabel($form, 'storeys'); ?> *</div>
		<?php echo CHtml::activeTextField($form, 'storeys', array('value'=>$form->storeys, 'size'=>10, 'maxlength'=>3, 'class'=>'inputbox')); ?>
<?php endif;?>

<?php //  файл add3  ?>

		<?php echo CHtml::activeLabel($form, 'contr_condition'); ?>
		<?php echo CHtml::dropDownList('Objects_edit[contr_condition]', $form->contr_condition, $list_cconds); ?>

		<div class="req"><?php echo CHtml::activeLabel($form, 'price'); ?> *</div>
		<?php echo CHtml::activeTextField($form, 'price', array('value'=>$form->price, 'size'=>15, 'maxlength'=>15, 'class'=>'inputbox')); ?>

		<?php echo CHtml::activeLabel($form, 'pay_utilities'); ?>
		<?php echo CHtml::dropDownList('Objects_edit[pay_utilities]', $form->pay_utilities, $list_putils); ?>
<?php if ($form->itemType==1) $s = 'Общая площадь объекта'; else $s = 'Площадь';?>
		<div class="req"><label for="Objects_edit_total_item_sqr"><?php echo $s;?></label> *</div>
		<?php echo CHtml::activeTextField($form, 'total_item_sqr', array('value'=>$form->total_item_sqr, 'size'=>15, 'maxlength'=>15, 'class'=>'inputbox')); ?>
<?php if (($form->itemType!=2) && ($form->kind>0)):?>
		<p><?php if ($form->service_lift=='' || $form->service_lift=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'service_lift', $cba); echo CHtml::activeLabel($form, 'service_lift', array('class'=>'chkbxSpan')); ?></p>
<?php endif;?>
		<p><?php if ($form->internet=='' || $form->internet=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'internet', $cba); echo CHtml::activeLabel($form, 'internet', array('class'=>'chkbxSpan')); ?></p>
		<div id="providers">
			<?php echo CHtml::activeLabel($form, 'providers'); ?>
			<?php echo CHtml::dropDownList('Objects_edit[providers]', $form->providers, $list_providers); ?>
		</div>
		<p><?php if ($form->telephony=='' || $form->telephony=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'telephony', $cba); echo CHtml::activeLabel($form, 'telephony', array('class'=>'chkbxSpan')); ?></p>
		<div id="tel_company">
			<?php echo CHtml::activeLabel($form, 'tel_company'); ?>
			<?php echo CHtml::dropDownList('Objects_edit[tel_company]', $form->tel_company, $list_tcomp); ?>
		</div>
<?php if ($form->kind>0):?>
		<div class="<?php if ($form->kind>0) echo 'req'; ?>"><?php echo CHtml::activeLabel($form, 'ceiling_height'); if ($form->kind>0) echo ' *'; ?></div>
		<?php echo CHtml::activeTextField($form, 'ceiling_height', array('value'=>$form->ceiling_height, 'size'=>15, 'maxlength'=>15, 'class'=>'inputbox')); ?>		
<?php endif;?>
<?php if ($form->kind>1):?>
		<?php echo CHtml::activeLabel($form, 'work_height'); ?>
		<?php echo CHtml::activeTextField($form, 'work_height', array('value'=>$form->work_height, 'size'=>15, 'maxlength'=>15, 'class'=>'inputbox')); ?>
		
		<p><?php if ($form->cathead=='' || $form->cathead=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'cathead', $cba); echo CHtml::activeLabel($form, 'cathead', array('class'=>'chkbxSpan')); ?></p>
<?php endif;?>
<?php if ($form->kind>0):?>
		<?php echo CHtml::activeLabel($form, 'floor_load'); ?>
		<?php echo CHtml::activeTextField($form, 'floor_load', array('value'=>$form->floor_load, 'size'=>15, 'maxlength'=>15, 'class'=>'inputbox')); ?>

		<div class="<?php if ($form->kind>1) echo 'req'; ?>"><?php echo CHtml::activeLabel($form, 'el_power'); if ($form->kind>1) echo ' *'; ?></div>
		<?php echo CHtml::activeTextField($form, 'el_power', array('value'=>$form->el_power, 'size'=>15, 'maxlength'=>15, 'class'=>'inputbox')); ?>
<?php endif;?>
<?php if ($form->kind>1):?>
		<?php echo CHtml::activeLabel($form, 'gates_qty'); ?>
		<?php echo CHtml::activeTextField($form, 'gates_qty', array('value'=>$form->gates_qty, 'size'=>15, 'maxlength'=>15, 'class'=>'inputbox')); ?>

		<?php echo CHtml::activeLabel($form, 'gates_height'); ?>
		<?php echo CHtml::activeTextField($form, 'gates_height', array('value'=>$form->gates_height, 'size'=>15, 'maxlength'=>15, 'class'=>'inputbox')); ?>
<?php endif;?>

<?php //  файл add4  ?>

<?php if (($form->kind<2) && ($form->itemType!=1)):?>
		<?php echo CHtml::activeLabel($form, 'repair'); ?>
		<?php echo CHtml::dropDownList('Objects_edit[repair]', $form->repair, $list_repair); ?>
<?php endif;?>
<?php if ($form->itemType!=2):?>
		<?php echo CHtml::activeLabel($form, 'parking'); ?>
		<?php echo CHtml::dropDownList('Objects_edit[parking]', $form->parking, $list_park); ?>
<?php endif;?>
		<?php echo CHtml::activeLabel($form, 'fixed_qty'); ?>
		<?php echo CHtml::activeTextField($form, 'fixed_qty', array('value'=>$form->fixed_qty, 'size'=>10, 'maxlength'=>10, 'class'=>'inputbox')); ?>

		<p><?php if ($form->video=='' || $form->video=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'video', $cba); echo CHtml::activeLabel($form, 'video', array('class'=>'chkbxSpan'));?></p>

		<p><?php if ($form->firefighting=='' || $form->firefighting=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'firefighting', $cba); echo CHtml::activeLabel($form, 'firefighting', array('class'=>'chkbxSpan')); if ($form->kind>1) echo ' *'; ?></p>

<?php if ($form->kind!=1):?>
	<?php if ($form->access=='' || $form->access=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'access', $cba); echo CHtml::activeLabel($form, 'access', array('class'=>'chkbxSpan'));?></p>
<?php endif;?>

<?php if ($form->kind>0):?>
		<?php echo CHtml::activeLabel($form, 'welfare'); ?>
		<?php echo CHtml::activeTextField($form, 'welfare', array('value'=>$form->welfare, 'size'=>50, 'maxlength'=>255, 'class'=>'inputbox')); ?>
<?php endif;?>
<?php if (($form->itemType!=2) && ($form->kind>1)):?>
		<p><?php if ($form->heating=='' || $form->heating=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'heating', $cba); echo CHtml::activeLabel($form, 'heating', array('class'=>'chkbxSpan')); ?></p>
<?php endif;?>
<?php if ($form->kind>1):?>
		<?php echo CHtml::activeLabel($form, 'pallet_capacity'); ?>
		<?php echo CHtml::activeTextField($form, 'pallet_capacity', array('value'=>$form->pallet_capacity, 'size'=>15, 'maxlength'=>15, 'class'=>'inputbox')); ?>
		
		<?php echo CHtml::activeLabel($form, 'shelf_capacity'); ?>
		<?php echo CHtml::activeTextField($form, 'shelf_capacity', array('value'=>$form->shelf_capacity, 'size'=>15, 'maxlength'=>15, 'class'=>'inputbox')); ?>
<?php endif;?>
<?php if (($form->kind!=1) && ($form->itemType!=1)):?>
		<div class="<?php if ($form->kind>1) echo 'req'; ?>"><?php echo CHtml::activeLabel($form, 'flooring'); if ($form->kind>1) echo ' *'; ?></div>
		<?php echo CHtml::dropDownList('Objects_edit[flooring]', $form->flooring, $list_flooring); ?>
<?php endif;?>
		<div class="<?php if ($form->kind==1) echo 'req'; ?>"><?php echo CHtml::activeLabel($form, 'ventilation'); if ($form->kind==1) echo ' *'; ?></div>
		<?php echo CHtml::dropDownList('Objects_edit[ventilation]', $form->ventilation, $list_ventil); ?>
<?php if ($form->kind<2):?>
		<p><?php if ($form->air_condit=='' || $form->air_condit=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'air_condit', $cba); echo CHtml::activeLabel($form, 'air_condit', array('class'=>'chkbxSpan')); ?></p>
<?php endif;?>
<?php if ($form->itemType!=2):?>
		<p><?php if ($form->can_reclame=='' || $form->can_reclame=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'can_reclame', $cba); echo CHtml::activeLabel($form, 'can_reclame', array('class'=>'chkbxSpan')); ?></p>
<?php endif;?>
<?php if ($form->kind>0):?>
		<p><?php if ($form->rampant=='' || $form->rampant=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'rampant', $cba); echo CHtml::activeLabel($form, 'rampant', array('class'=>'chkbxSpan')); ?></p>
<?php endif;?>
<?php if (($form->itemType!=2) && ($form->kind>1)):?>
<fieldset><legend>Подъездные пути</legend>
		<p><?php if ($form->autoways=='' || $form->autoways=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'autoways', $cba); echo CHtml::activeLabel($form, 'autoways', array('class'=>'chkbxSpan')); ?></p>
		<p><?php if ($form->railways=='' || $form->railways=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'railways', $cba); echo CHtml::activeLabel($form, 'railways', array('class'=>'chkbxSpan')); ?></p>
</fieldset>
<?php endif;?>
		<?php echo CHtml::activeLabel($form, 'about'); ?>
		<?php echo CHtml::textArea('Objects_edit[about]', $form->about, array('rows'=>5, 'cols'=>70)); ?>


	<div class="buttons">
		<div><?php echo CHtml::submitButton('Сохранить', array('id'=>'submit', 'class'=>'next')); ?></div>
	</div><!--buttons-->
<?php echo CHtml::endForm(); ?>
	</div><!--form-->
</div><!-- master-->
<br /><br />
Если затрудняетесь, посетите страницу <a href="/">справки</a>
