<link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/vader/jquery-ui.css" rel="stylesheet" />
<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/jcrop/jquery.Jcrop.min.css'); ?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo $this->createAbsoluteUrl('/js/ajaxupload.3.6.js'); ?>"></script>
<script type="text/javascript" src="<?php echo $this->createAbsoluteUrl('/js/jquery.Jcrop.min.js'); ?>"></script>
<?php if (!empty($fotos))	$qty = count($fotos); else $qty = 0;
	switch ($obj->kind) {
		case 1 : {$lbKind = 'Торговое'; $lbParent = 'Торговый центр'; break;}
		case 2 : {$lbKind = 'Склад / Производственное'; $lbParent = 'Центр Недвижимости'; break;}
		default: {$lbKind = 'Офисное'; $lbParent = 'Офисный центр';}
	}
	$titleBlank = 'Нажмите, чтобы загрузить';
	$titleFoto = 'Нажмите, чтобы заменить фотографию';
	$fileBlank = 'images/add_photo113.jpg';
?>

<script type="text/javascript">
	$(document).ready(function(){
		var glass = $('div#glass');
		var indic = $('div#indic');
		function ShowGlass() {
			var w = $(document).width();
			var h = $(document).height();
			glass.attr({'width': w,'height': h});
			glass.show();
			indic.css({top:h/4, left:w*0.4});
			indic.show();
		}
		function HideGlass() {
			glass.hide();
			indic.hide();
		}
		HideGlass();
		function RemoveBlock (e) {
			e.preventDefault();
			var s = $(this).attr('id').substr(5);
			var img = $('img#iitem'+s);
			var fName = $(img).attr('src');
			$.ajax({
				url: 'ajaxfoto/delete/',
				data: {	file: fName	},
				type: 'post',
				success: function(){
							$(img).attr({
								src: '<?php echo $fileBlank;?>',
								title: '<?php echo $titleBlank;?>'
							});
				}
			});
		}
		function showCoords(c){
			$('#x1').val(Math.round(c.x));
			$('#y1').val(Math.round(c.y));
			$('#x2').val(Math.round(c.x2));
			$('#y2').val(Math.round(c.y2));
		}
		function bindDownloader (img) {
			var bt = new AjaxUpload(img, {
						action: 'ajaxfoto/upload/',
						name: 'myfile',
						onSubmit : function(file, ext) {
							ShowGlass();
							if (ext && /^(jpg|png|jpeg|gif|JPG|PNG|JPEG|GIF)$/.test(ext)) {
								this.disable();
							} else {
								HideGlass();
								alert('Неверный тип картинки, допускается только: jpg, jpeg, png, gif');
								return false;
							}
						},
						onComplete: function(file, response) {
							HideGlass();
							var action = this;
							doClose = true;
							if (response != false){
								var data = response.split('|');
								var file_name = data[0];
								$('#load-here').append(
									jQuery('<img />')
										.attr({
											src: 'storage/images/temp/' + file_name,
											id:  'crop-image'
										})
								);
								$('#crop-box').dialog({
									modal: true,
									width: (parseInt(data[1])+34) + 'px',
									title: 'Редактирование картинки',
									close: function() {
											ShowGlass();
											action.enable();
											$('#crop-box').hide();
											$('#load-here').empty();
											if (doClose) {
												$.ajax({
													url: 'ajaxfoto/cancel/',
													data: {	file: file_name	},
													type: 'post'
												});
												HideGlass();
											}
									},
									resizable: false,
									position:['middle', 100]
								});
								$('#crop-image').Jcrop({
									setSelect: [10, 10, 410, 310],
									minSize: [400, 300], 
									aspectRatio: 4/3,
									allowSelect: false,
									bgColor: '#ffffff',
									bgOpacity: .5,
									onChange: showCoords,
									onSelect: showCoords
								});
								$('#save').click(function() {
									if (file_name != '') {
										$.ajax({
											url: 'ajaxfoto/crop/',
											data: {
												file: file_name,
												old:$(img).attr('src'),
												oid:<?php echo $obj->id;?>,
												x1: $('#x1').val(),
												y1: $('#y1').val(),
												x2: $('#x2').val(),
												y2: $('#y2').val()
											},
											dataType: 'html',
											type: 'post',
											success: function(data){
													$('#divAdelPhoto').removeClass('hide');
													img.attr({
														src: 'storage/images/_thumbs/' + data,
														alt: data,
														title: '<?php echo $titleFoto;?>'
													});
													file_name = '';
													HideGlass();
											}
										});
										doClose = false;
										$('#crop-box').dialog("close");
									}
								});
							}else{
								alert('Размер картинки слишком большой, разрешенно не более 6Мб');
								action.enable();
							}
						}
					});
		}

		for (i=0; i<5; i++) {
			var lnk = $("#aitem" + i);
			var img = $("#iitem" + i);
			bindDownloader(img);
			lnk.bind('click', {}, RemoveBlock);
		}
	});
</script>

<h1 style="font-size:16px;">Работа с фотографиями</h1><br />
<?php echo $lbKind;?> помещение <?php echo 'г. '.$obj->city.', ул. '.CHtml::link($obj->street.', '.$obj->house, array('objects/detail', 'id'=>$obj->id)).', этаж '.$obj->storey.', '.$obj->name;
if ($obj->itemType==2) echo ' ('.$lbParent.' '.CHtml::link('"'.$obj->parentName.'"', array('objects/contain', 'id'=>$obj->parent)).')';?>
<br /><br />
<div id="container" style="margin-left:-10px;">
<?php for($i=0; $i<5; $i++){?>
	<div style="border:1px solid #AAAAAA; padding:15px; margin:10px; float:left;" id="item<?php echo $i;?>">
		<img id="iitem<?php echo $i;?>" <?php if ($i<$qty):?>src="storage/images/_thumbs/<?php echo $fotos[$i]->name_gallery;?>" title="<?php echo $titleFoto;?>"<?php else:?>src="<?php echo $fileBlank;?>" title="<?php echo $titleBlank;?>"<?php endif;?> /><br /><br /><a id="aitem<?php echo $i;?>" href="#">Удалить фото</a>
	</div>
<?php } ?>
</div>
<div style="clear:left"></div>
<div style="font-size:14px;"><u>Ограничение:</u> размер фотографий <b>не может</b> превышать 6 мегабайт</div>
	
		<!-- Hidden dialog -->
		<div style="display:none;" id="crop-box">
			<div id="dialog">
				<div id="load-here"></div>
				<button id="save" style="margin-top:10px;">Cохранить</button>
			</div>
		</div>
		<!-- /Hidden dialog -->
		<div id="glass" style="position: absolute; top:0px; left:0px; right:0px; bottom:0px; opacity: 0.1; z-index:10005; background-color: #BED400; overflow: hidden;"></div>
		<div id="indic" style="position: absolute; top:0px; left:0px; font-size:16px; padding-left:25px; background: url('images/indicator.gif') no-repeat 0 0">Обработка...</div>

		<input type="hidden" id="x1" name="x1" />
		<input type="hidden" id="y1" name="y1" />
		<input type="hidden" id="x2" name="x2" />
		<input type="hidden" id="y2" name="y2" />
