<div class="master">
	<div class="head">
		<h3>Добавление объекта</h3>/ Локация
	</div>
	<div class="road">
		<ul>
			<li class="pred first"><div><?php echo CHtml::link('Категория', array('object/add', 'step'=>'step1')); ?></div><span>&rarr;</span></li>
			<li class="active"><div>Локация</div><span>&rarr;</span></li>
			<li class="next"><div>Параметры</div><span>&rarr;</span></li>
			<li class="next"><div>Описание</div><span>&rarr;</span></li>
			<li class="next"><div>Файлы</div></li>
		</ul>
	</div><!--  /road -->
	<div class="form">
		<?php echo CHtml::form(); ?>
		<?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:'); ?>

		<?php echo CHtml::activeLabel($form, 'functionality'); ?>
		<?php echo CHtml::dropDownList('Objects_add[functionality]', $vfunc, $list_funcs); ?>

<?php if ($hasparent==0):?>
		<div class="req"><?php echo CHtml::activeLabel($form, 'class'); ?> *</div>
		<?php echo CHtml::dropDownList('Objects_add[class]', $vclass, $list_class); ?>

		<div class="<?php if ($kind>0) echo 'req'; ?>"><?php echo CHtml::activeLabel($form, 'kind_structure'); if ($kind>0) echo ' *'; ?></div>
		<?php echo CHtml::dropDownList('Objects_add[kind_structure]', $vstruc, $list_strucs); ?>
<?php endif;?>

		<div class="req"><?php echo CHtml::activeLabel($form, 'name'); ?> *</div>
		<?php echo CHtml::activeTextField($form, 'name', array('value'=>$vname, 'size'=>30, 'maxlength'=>47, 'class'=>'inputbox')); ?>

<?php if ($hasparent==0):?>
		<div class="req"><?php echo CHtml::activeLabel($form, 'district'); ?> *</div>
		<?php echo CHtml::dropDownList('Objects_add[district]', $vdistr, $list_distrs); ?>

		<div class="req"><?php echo CHtml::activeLabel($form, 'street'); ?> *</div>
		<?php echo CHtml::dropDownList('Objects_add[street]', $vstrname, $list_streets); ?>

		<div class="req"><?php echo CHtml::activeLabel($form, 'house'); ?> *</div>
		<?php echo CHtml::activeTextField($form, 'house', array('value'=>$vhouse, 'size'=>10, 'maxlength'=>15, 'class'=>'inputbox')); ?>
<?php endif;?>

		<?php echo CHtml::activeLabel($form, 'housing'); ?>
		<?php echo CHtml::activeTextField($form, 'housing', array('value'=>$vhousing, 'size'=>10, 'maxlength'=>15, 'class'=>'inputbox')); ?>

<?php if ($item_type!=1):?>
		<div class="<?php if ($kind<2) echo 'req'; ?>"><?php echo CHtml::activeLabel($form, 'storey'); if ($kind<2) echo ' *'; ?></div>
		<?php echo CHtml::activeTextField($form, 'storey', array('value'=>$vstorey, 'size'=>10, 'maxlength'=>3, 'class'=>'inputbox')); ?>
<?php endif;?>

<?php if ($item_type!=2):?>
		<div class="req"><?php echo CHtml::activeLabel($form, 'storeys'); ?> *</div>
		<?php echo CHtml::activeTextField($form, 'storeys', array('value'=>$vstoreys, 'size'=>10, 'maxlength'=>3, 'class'=>'inputbox')); ?>
<?php endif;?>

	<div class="buttons">
		<div class="cancel">
			x <?php echo CHtml::link('Начать заново', array('object/add_new')); ?>
		</div>
		<div class="pred">&larr; <?php echo CHtml::link('Назад', array('object/add', 'step'=>'step1')); ?></div>
		<div><?php echo CHtml::submitButton('Дальше', array('id'=>'submit', 'class'=>'next')); ?></div>
	</div><!--buttons-->
<?php echo CHtml::endForm(); ?>
	</div><!--form-->
</div><!-- master-->
<br /><br />
Если затрудняетесь, посетите страницу <a href="/">справки</a>
