<script type="text/javascript">
  $(document).ready(function(){<?php if (Yii::app()->user->hasState('ses_itype')){/* Если сесиия существует у типа помещения, то прячем радиогруппы */ ?> 
	$('input.type').hide();
<?php }else{ /* Если же нет сессии, то предпологается что это начало заполнения, поетому прячем кнопку далее */ ?> 
    $('input#submit').hide();
<?php } ?>
    if($('input#Objects_add_itemType_2').is(":checked")){ <?php /* Вначале проверяем какой радиобутон активен, */ ?>
    	$('div#rootitems').show(); <?php /* чтобы спрятать нужный контент или показать */ ?>
    	$('div#divcity').hide();
    }else{    	$('div#rootitems').hide();
    	$('div#divcity').show();    }

    $('input#Objects_add_itemType_2').change(function(){
        if($(this).is(":checked")){
			$.ajax({
				url: "<?php echo $this->createUrl('ajaxobjects/get_parent_items'); ?>",
				data: {id: <?php echo (Yii::app()->user->isGuest)?'0':Yii::app()->user->id['user_id']; ?>},
				dataType: 'html',
				type: 'post',
				success: function(data){
				    $('div#rootitems').html(data);
				}
			});
			$('div#rootitems').show(); <?php /* Меняем на "сдаваемое помещение", показываем див */ ?>
			$('div#divcity').hide();
        }
    });

    $('input#Objects_add_itemType_0').change(function(){
        if($(this).is(":checked")){
          $('div#rootitems').hide(); <?php /* Меняем на "объект-контейнер", прячем див */ ?>
    	  $('div#divcity').show();
        }
    });
    $('input#Objects_add_itemType_1').change(function(){
        if($(this).is(":checked")){
          $('div#rootitems').hide(); <?php /* Меняем на "объект-контейнер", прячем див */ ?>
    	  $('div#divcity').show();
        }
    });

    $('input.type').change(function(){    	$('input#submit').show(); <?php /* Если меняем хоть один из радионбутонов типа помещения, то показываем кнопку далее */ ?>    });
  });
</script>
<div class="master">
	<div class="head">
		<h3>Добавление объекта</h3>/ Категория
	</div>
	<div class="road">
		<ul>
			<li class="active first"><div>Категория</div><span>&rarr;</span></li>
			<li class="next"><div>Локация</div><span>&rarr;</span></li>
			<li class="next"><div>Параметры</div><span>&rarr;</span></li>
			<li class="next"><div>Описание</div><span>&rarr;</span></li>
			<li class="next"><div>Файлы</div></li>
		</ul>
	</div><!--  /road -->
	<div class="form">
	<?php echo CHtml::form(); ?>
	<?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:'); ?>
		<div class="bordered" title="Укажите категорию">
			<h4><?php echo (Yii::app()->user->isGuest)?'Категория объекта':'Помещение или объект?';?></h4>
			<div class="oneCol"><?php echo CHtml::activeRadioButtonList($form, 'itemType', $listTypes); ?></div>
		</div>
		<div class="bordered" id="rootitems" title="Выберите родительский объект">
<?php if (isset($listItems)):?>
			<h4>Ваши центры недвижимости</h4>
			<div class="oneCol"><?php echo CHtml::activeRadioButtonList($form, 'parent', $listItems);?></div>
<?php endif;?>
		</div>
<?php if (isset(Yii::app()->user->ses_kind)):?>
<div class="row">
Тип помещения: <strong><?php echo($listKinds[Yii::app()->user->getState('ses_kind')]);?></strong>
</div>
<?php else:?>
		<div class="bordered" title="Укажите тип добавляемого помещения">
			<h4>Тип помещения</h4>
			<div class="oneCol"><?php echo CHtml::activeRadioButtonList($form, 'kind', $listKinds, array('class'=>'type')); ?></div>
		</div>
<?php endif;?>

	<div id="divcity">
		<?php echo CHtml::activeLabel($form, 'city'); ?>
		<?php echo CHtml::dropDownList('Objects_add[city]', $vcity, $list_city); ?>
	</div>
	<div class="buttons">
		<?php echo CHtml::submitButton('Дальше', array('id'=>'submit', 'class'=>'next')); ?>
	</div>
<?php echo CHtml::endForm(); ?>

	</div><!--form-->
</div><!-- master-->
<br /><br />
Если затрудняетесь, посетите страницу <a href="/">справки</a>
<br />
<br /><br />
