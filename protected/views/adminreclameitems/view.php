<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/form.css'); ?>
Фильтры: <?php echo CHtml::link('Сбросить', array('adminreclameitems/unset')); ?><br />
<div class="form">
	<?php echo CHtml::form(); ?>
	<?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:'); ?>
	<strong><?php echo CHtml::activeLabel($form, 'city'); ?>:</strong> <?php echo CHtml::dropDownList('AdminItems_form[city]', $city, $Cities); ?>
	<strong><?php echo CHtml::activeLabel($form, 'owner'); ?>:</strong> <?php echo CHtml::dropDownList('AdminItems_form[owner]', $owner, $Users); ?>
	<div class="row buttons">
		<br /><?php echo CHtml::submitButton('Найти', array('id'=>'submit')); ?><br />
	</div>
	<?php echo CHtml::endForm(); ?>
</div><!--form-->
<?php if (is_array($rows)):?>
<hr>Удовлетворяет фильтру: <?php echo $foundQty; ?> строк<br />
<?php echo CHtml::link('Добавить новую кампанию', array('adminreclameitems/add')); ?><br /><br />
<?php if (!empty($rows)):?>
<style type="text/css">
table.sample {
  border-spacing: 2px;
  border-collapse: collapse;
  width: 100%;
  text-align: left;
}
table.sample th {
  border: 1px solid #000000;
  padding: 5px;
  background-color: #aaaaaa;
  text-align: center;
}
table.sample td {
  border: 1px solid #000000;
  padding: 5px;
}
table.sample td.action {
  text-align: center;
}
</style>
<?php echo CHtml::link('Сбросить сортировку', array('adminreclameitems/view', 'page'=>$Cpage)); ?>
<table class="sample">
	<tr>
		<th rowspan="2">Создан <?php echo CHtml::link('&uarr;', array('adminreclameitems/view', 'page'=>$Cpage, 'sort'=>'created')); ?>&nbsp;<?php echo CHtml::link('&darr;', array('adminreclameitems/view', 'page'=>$Cpage, 'sort'=>'created', 'type'=>'desc')); ?>
		</th>
		<th rowspan="2">Название объекта</th>
		<th rowspan="2">Владелец объекта</th>
		<th rowspan="2">Глав</th>
		<th rowspan="2">Пск</th>
		<th rowspan="2">Друг</th>
		<th rowspan="2">Актив</th>
		<th colspan="2">Даты кампании</th>
		<th colspan="2">Показывать</th>
		<th rowspan="2">Р</th>
		<th rowspan="2">У</th>
	</tr>
	<tr>
		<th>старт <?php echo CHtml::link('&uarr;', array('adminreclameitems/view', 'page'=>$Cpage, 'sort'=>'start')); ?>&nbsp;<?php echo CHtml::link('&darr;', array('adminreclameitems/view', 'page'=>$Cpage, 'sort'=>'start', 'type'=>'desc')); ?>
		</th>
		<th>финиш <?php echo CHtml::link('&uarr;', array('adminreclameitems/view', 'page'=>$Cpage, 'sort'=>'stop')); ?>&nbsp;<?php echo CHtml::link('&darr;', array('adminreclameitems/view', 'page'=>$Cpage, 'sort'=>'stop', 'type'=>'desc')); ?>
		</th>
		<th>надо</th>
		<th>уже</th>
	</tr>
<?php
	foreach ($rows as $row)
		$this->renderPartial('_row', array('obj' =>$row));
?>
<?php endif; ?>
</table>
<div align="center">
<?php
  $this->widget('CLinkPager',array(
		'pages'          =>$pages,
		'maxButtonCount' =>5, # максимальное колличество вкладок на странице
		'header'         =>'<b>Перейти к странице:</b><br>', # заголовок
		'nextPageLabel'  =>'Вперед&gt;', # название кнопок навигаций next и prev
		'prevPageLabel'  =>'&lt;Назад',
  ));
?>
</div>
<i>Примечание</i>: При проверке кампании на ограничение по датам и кол-ву показов применяется <b>строгое</b> неравенство. Иными словами, если указана дата начала кампании 1 июня, то показы начнутся со 2-го июня. Если указана дата окончания 4 июня, то последним днем показов будет 3-е июня.<br />
<i>Примечание 2</i>: Кампании показываются без учета выбранного города. Как именно делать привязку кампании к городу - нужно еще долго думать. Фильтр по городу на этой странице отфильтровывает объекты, а не кампании.
<?php endif; ?>
