<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/form.css'); ?>
<script type="text/javascript" src="<?php echo $this->createAbsoluteUrl('/js/tiny_mce/tiny_mce.js'); ?>"></script>
<script type="text/javascript">
    tinyMCE.init({
        mode:"textareas",
        theme:"advanced",
        plugins : "spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,
        content_css : "http://localhost/site/css/content.css",
        style_formats : [
			{title : 'Red header', block : 'h1',  classes : 'example1'},
		],
        language:"ru"
    });
</script>
<style type="text/css">
	div.settings {display:inline-block;}
	div.settings {display:block;}
	div.settings {margin:20px;}
	div.settings input {display:inline;}
	div.settings label {display:block; margin-bottom:4px; font-size:14px;}
	div.settings label.chkbxSpan {display: inline; margin-bottom:5px; padding-left:5px;}
</style>
Добавление рекламной кампании [<?php echo CHtml::link('отмена', array('adminreclameitems/view'));?>]
<div class="form">
	<?php echo CHtml::form();?>
	<?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:'); ?>
<div class="row">
	<?php echo CHtml::activeLabel($form, 'name'); ?><br />
	<?php echo CHtml::activeTextField($form, 'name', array('size'=>100, 'maxlength'=>127));?>
</div>
<div class="row">
	<?php echo CHtml::activeLabel($form, 'txt'); ?><br />
	<?php echo CHtml::textArea('ReclameItem[txt]', $oTxt, array('rows'=>20, 'cols'=>70)); ?>
</div>
<div class="row">
	<?php echo CHtml::activeLabel($form, 'item'); ?><br />
	<?php echo CHtml::activeTextField($form, 'item', array('size'=>10, 'maxlength'=>10));?>
</div>
<fieldset title="Настройки">
<div class="settings">
	<div class="row">
		<?php echo CHtml::activeCheckBox($form, 'isActive').' '.CHtml::activeLabel($form, 'isActive', array('class'=>'chkbxSpan'));?>
	</div>
	<div class="row">
		<?php echo CHtml::activeCheckBox($form, 'onMain').' '.CHtml::activeLabel($form, 'onMain', array('class'=>'chkbxSpan'));?>
	</div>
	<div class="row">
		<?php echo CHtml::activeCheckBox($form, 'onSearch').' '.CHtml::activeLabel($form, 'onSearch', array('class'=>'chkbxSpan'));?>
	</div>
	<div class="row">
		<?php echo CHtml::activeCheckBox($form, 'onAny').' '.CHtml::activeLabel($form, 'onAny', array('class'=>'chkbxSpan'));?>
	</div>
	<div class="row">
		<?php echo CHtml::activeLabel($form, 'start'); ?><?php echo CHtml::activeTextField($form, 'start', array('size'=>10, 'maxlength'=>10));?>
	</div>
	<div class="row">
		<?php echo CHtml::activeLabel($form, 'stop'); ?><?php echo CHtml::activeTextField($form, 'stop', array('size'=>10, 'maxlength'=>10));?>
	</div>
	<div class="row">
		<?php echo CHtml::activeLabel($form, 'qty'); ?><?php echo CHtml::activeTextField($form, 'qty', array('size'=>10, 'maxlength'=>5));?>
	</div>
</div>
</fieldset>



<div class="row buttons">
	<?php echo CHtml::submitButton('Сохранить', array('id'=>"submit")); ?>
</div>
<?php echo CHtml::endForm(); ?>
</div><!--form-->
