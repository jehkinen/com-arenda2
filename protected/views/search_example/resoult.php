<?php $type_ses = 'tab1'; ?><!-- В эту переменную попадает переменная из сессии, т.е. запоминание активной вкладки -->
<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/form.css'); ?>
<style type="text/css">
	#example-box {
		padding: 10px 20px;
		border: 1px solid #999999;
	}
	#example-output {
		margin-top: 10px;
	}
	#example-box a.load {
		padding: 5px;
		color: #000000;
		background-color: #bdd400;
		text-decoration: none;
	}
	#example-box a.load:hover {
		text-decoration: underline;
	}
	#example-box a.active {
		background-color: #ff9000;
	}
	#example-box #else-box {
		display: none;
		padding: 10px 0;
	}
</style>
<script type="text/javascript">
	$(document).ready(function(){

	    var type_ses = "<?php echo $type_ses; ?>";
	    $('a.load').removeClass('active');
	    $('[id='+type_ses+']').addClass('active');

	    $.ajax({
				url: "/ajax_example/load_form",
				data: {type: type_ses},
				dataType: 'json',
				type: 'post',
				success: function(data){
										$('#example-output').html(data[0]);
										$('#else-box').html(data[1]);
										}
		});

		$('a.load').bind('click', function(e){
			$('#else-box').hide();
			var load_type = $(e.target).attr('id');
			$.ajax({
					url: "/ajax_example/load_form",
					data: {type: load_type},
					dataType: 'json',
					type: 'post',
					success: function(data){
											$('#example-output').html(data[0]);
											$('#else-box').html(data[1]);
											$('a.load').removeClass('active');
											$(e.target).addClass('active');
											}
			});
			return false;
		});

		$('#else').click(function(){
            $('#else-box').slideToggle(100);
            return false;
        });
	});
</script>
<div id="example-box">
	<a id="tab1" class="load" href="#">Офис</a> - <a id="tab2" class="load" href="#">Склад</a> - <a id="tab3" class="load active" href="#">Офис+склад/производство</a> - <a id="tab4" class="load" href="#">Торговое помещение</a><br />
	<div class="form">
		<?php echo CHtml::form('search_example/resoult'); ?>
		<?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:'); ?>
		<div id="example-output"></div>
		<div class="row buttons"><?php echo CHtml::submitButton('Найти'); ?></div>
		<a id="else" href="#">Дополнительные параметры</a>
	    <?php echo CHtml::endForm(); ?>
	</div><!--form-->
	<div id="else-box"></div>
</div>
<br />Результаты поиска:<br />
<?php
  if (!empty($Search_obj)){  	foreach ($Search_obj as $object){      $this->renderPartial('_resoult', array(
			                                 'id' =>$object->id,
		                                     ));  	}  }
?>
<div align="center">
<?php
  $this->widget('CLinkPager',array(
		'pages'          =>$pages,
		'maxButtonCount' =>5, # максимальное колличество вкладок на странице
		'header'         =>'<b>Перейти к странице:</b><br>', # заголовок
		'nextPageLabel'  =>'Вперед&gt;', # название кнопок навигаций next и prev
		'prevPageLabel'  =>'&lt;Назад',
  ));
?>
</div>
