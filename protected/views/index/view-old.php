<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/form.css'); ?>
<?php echo $text; ?><br /><br />
Поиск:<br /><br />
<?php echo CHtml::link('Сбросить', array('index/unset')); ?><br /><br />
<div class="form">
  <?php echo CHtml::form(); ?>
  <?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:'); ?>
  <div class="row">
	<fieldset style="border:1;">
	<legend>Тип помещения</legend>
		<?php echo CHtml::activeRadioButtonList($form, 'kind', $list_kinds); ?>
	</fieldset>
  </div>
  <div class="row">
    <strong><?php echo CHtml::activeLabel($form, 'sity'); ?>:</strong> комбобокс. От него зависят ряд других комбобоксов
  </div>
  <div class="row">
    <strong><?php echo CHtml::activeLabel($form, 'raion'); ?>:</strong> временно комбобокс<br />
    <?php echo CHtml::dropDownList('Search_form[raion]', $vraion, $list_districts); ?>
  </div>
  <div class="row">
    <strong><?php echo CHtml::activeLabel($form, 'ulica'); ?>:</strong> Зависит от выбранного города<br />
    <?php echo CHtml::dropDownList('Search_form[ulica]', $vulica, $list_streets); ?>
  </div>
  <div class="row">
    <strong><?php echo CHtml::activeLabel($form, 'tip_sooruj'); ?>:</strong><br />
    <?php echo CHtml::dropDownList('Search_form[tip_sooruj]', $vtip_sooruj, $list_kindStructure); ?>
  </div>
  <div class="row">
    <strong><?php echo CHtml::activeLabel($form, 'funkcional'); ?>:</strong><br />
    <?php echo CHtml::dropDownList('Search_form[funkcional]', $vfunkcional, $list_functionality); ?>
  </div>
  <div class="row">
    <strong><?php echo CHtml::activeLabel($form, 'klass'); ?>:</strong><br />
    <?php echo CHtml::dropDownList('Search_form[klass]', $vklass, $list_classes); ?>
  </div>
  <div class="row">
    <strong><?php echo CHtml::activeLabel($form, 'etaj_label'); ?>:</strong><br />
    <?php echo CHtml::activeTextField($form, 'etaj1', array('value'=>$vetaj1, 'size'=>5, 'maxlength'=>5)); ?> -
	<?php echo CHtml::activeTextField($form, 'etaj2', array('value'=>$vetaj2, 'size'=>5, 'maxlength'=>5)); ?>
  </div>
  <div class="row">
    <?php echo CHtml::activeCheckBox($form, 'gruz_lift', array('checked'=>$vgruz_lift)); ?>
    <strong><?php echo CHtml::activeLabel($form, 'gruz_lift'); ?></strong>
  </div>
  <div class="row">
    <strong><?php echo CHtml::activeLabel($form, 'sqr_label'); ?>:</strong><br />
    <?php echo CHtml::activeTextField($form, 'sqr1', array('value'=>$vsqr1, 'size'=>10, 'maxlength'=>10)); ?> -
	<?php echo CHtml::activeTextField($form, 'sqr2', array('value'=>$vsqr2, 'size'=>10, 'maxlength'=>10)); ?>
  </div>
  <div class="row">
    <strong><?php echo CHtml::activeLabel($form, 'sosremont'); ?>:</strong><br />
    <?php echo CHtml::dropDownList('Search_form[sosremont]', $vsosremont, $list_repair); ?>
  </div>
  <div class="row">
    <strong><?php echo CHtml::activeLabel($form, 'parkovka'); ?>:</strong> временно комбобокс, потом (возможно) набор чекбоксов<br />
    <?php echo CHtml::dropDownList('Search_form[parkovka]', $vparkovka, $list_parking); ?>
  </div>
  <div class="row">
    <strong><?php echo CHtml::activeLabel($form, 'prc_label'); ?>:</strong><br />
    <?php echo CHtml::activeTextField($form, 'prc1', array('value'=>$vprc1, 'size'=>10, 'maxlength'=>10)); ?> -
	<?php echo CHtml::activeTextField($form, 'prc2', array('value'=>$vprc2, 'size'=>10, 'maxlength'=>10)); ?>
  </div>
  <div class="row">
    <?php echo CHtml::activeCheckBox($form, 'est_inet', array('checked'=>$vest_inet)); ?>
    <strong><?php echo CHtml::activeLabel($form, 'est_inet'); ?></strong><br />
    <?php echo CHtml::activeCheckBox($form, 'est_telef', array('checked'=>$vest_telef)); ?>
    <strong><?php echo CHtml::activeLabel($form, 'est_telef'); ?></strong><br />
    <?php echo CHtml::activeCheckBox($form, 'est_voda', array('checked'=>$vest_voda)); ?>
    <strong><?php echo CHtml::activeLabel($form, 'est_voda'); ?></strong>
  </div>
  <div class="row">
    <strong><?php echo CHtml::activeLabel($form, 'potolok'); ?>:</strong><br />
    <?php echo CHtml::activeTextField($form, 'potolok', array('value'=>$vpotolok, 'size'=>10, 'maxlength'=>10)); ?>
  </div>
  <div class="row">
    <?php echo CHtml::activeCheckBox($form, 'kranb', array('checked'=>$vkranb)); ?>
    <strong><?php echo CHtml::activeLabel($form, 'kranb'); ?></strong>
  </div>
  <div class="row">
    <strong><?php echo CHtml::activeLabel($form, 'pokrytie'); ?>:</strong><br />
    <?php echo CHtml::dropDownList('Search_form[pokrytie]', $vpokrytie, $list_flooring); ?>
  </div>
  <div class="row">
    <strong><?php echo CHtml::activeLabel($form, 'ventil'); ?>:</strong><br />
    <?php echo CHtml::dropDownList('Search_form[ventil]', $vventil, $list_ventil); ?>
  </div>
  <div class="row">
    <?php echo CHtml::activeCheckBox($form, 'condic', array('checked'=>$vcondic)); ?>
    <strong><?php echo CHtml::activeLabel($form, 'condic'); ?></strong><br />
    <?php echo CHtml::activeCheckBox($form, 'pozhar', array('checked'=>$vpozhar)); ?>
    <strong><?php echo CHtml::activeLabel($form, 'pozhar'); ?></strong>
  </div>
  <div class="row">
    <strong><?php echo CHtml::activeLabel($form, 'vorota'); ?>:</strong><br />
    <?php echo CHtml::activeTextField($form, 'vorota', array('value'=>$vvorota, 'size'=>10, 'maxlength'=>10)); ?>
  </div>
  <div class="row">
    <?php echo CHtml::activeCheckBox($form, 'pandus', array('checked'=>$vpandus)); ?>
    <strong><?php echo CHtml::activeLabel($form, 'pandus'); ?></strong>
  </div>
  <div class="row">
    <?php echo CHtml::activeCheckBox($form, 'autoways', array('checked'=>$vaways)); ?>
    <strong><?php echo CHtml::activeLabel($form, 'autoways'); ?></strong><br />
    <?php echo CHtml::activeCheckBox($form, 'railways', array('checked'=>$vrways)); ?>
    <strong><?php echo CHtml::activeLabel($form, 'railways'); ?></strong>
  </div>
  <div class="row buttons">
    <br /><?php echo CHtml::submitButton('Найти', array('id'=>'submit')); ?><br /><br /><br />
  </div>
  <?php echo CHtml::endForm(); ?>
</div><!--form-->