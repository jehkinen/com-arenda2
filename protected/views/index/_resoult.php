<?php 
if ($odd)	$row_class = 'background_tr';
else		$row_class = '';
if ($obj->special==1) {
	$row_class .= ' red_tr';
	$fp = 'red';
} else	$fp = 'yes';
if ($row_class != '') $row_class = 'class="'.$row_class.'"';
$bUrl = Yii::app()->baseUrl;
?>
<tr <?php echo $row_class;?>>
	<td class="tt_01" onClick="doClick(<?php echo($obj->id);?>)"><img src="<?php echo $bUrl;?>/images/photo_<?php echo ($obj->hasFoto==0)?'no':$fp; ?>.png" alt="" /></td>
	<td class="tt_02" onClick="doClick(<?php echo($obj->id);?>)"><?php echo $obj->district; ?></td>
	<td class="tt_02" onClick="doClick(<?php echo($obj->id);?>)"><?php echo $obj->street; ?>, <?php echo $obj->house; ?></td>
	<td class="tt_04" onClick="doClick(<?php echo($obj->id);?>)"><?php echo number_format($obj->total_item_sqr, 2, ',', ' ');?></td>
<?php if ($obj->special==1):?>
	<td class="tt_03" onClick="doClick(<?php echo($obj->id);?>)"><span class="hot_span"><?php echo number_format($obj->price, 2, ',', ' ');?> Р <img src="<?php echo $bUrl;?>/images/hot_img.png" alt="" /></span></td>
<?php else:?>
	<td class="tt_03" onClick="doClick(<?php echo($obj->id);?>)"><?php echo number_format($obj->price, 2, ',', ' ');?> Р</td>
<?php endif;?>
	<td class="tt_03" onClick="doClick(<?php echo($obj->id);?>)"><?php echo number_format($obj->total_item_sqr*$obj->price, 2, ',', ' ');?></td>
	<td onClick="doClick(<?php echo($obj->id);?>)"><?php $get = $this->widget('DateFormat', array('kind'=>2,'dbDate'=>$obj->created)); echo $get->isEmpty?'-':$get->date; ?></td>
<?php if ($isUserTenant):?>
	<td class="tt_04">
<?php if ($obj->inNotepad==0){?>
		<a class="tonotepad" href="#" id="obj<?php echo($obj->id);?>"><img src="<?php echo $bUrl;?>/images/se_plus.gif" alt="" /></a>
<?php }else{?>
		<img src="<?php echo $bUrl;?>/images/se_yes.gif" alt="" />
<?php }?>
	</td>
<?php endif;?>
</tr>
