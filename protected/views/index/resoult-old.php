<?php echo CHtml::link('Вернуться к поиску', array('index/view')); ?><br />
<?php if ($userTenant):?>
<?php echo CHtml::link('Сохранить параметры поиска как заявку', array('orders/add')); ?><br />
<?php endif;?>
Найденно объектов: <?php echo $count_of_search; ?><br /><br />
<?php if (!empty($Search_obj)){?>
<style type="text/css">
table.sample {
  border-spacing: 2px;
  border-collapse: collapse;
  width: 100%;
  text-align: left;
}
table.sample th {
  border: 1px solid #000000;
  padding: 5px;
  background-color: #aaaaaa;
  text-align: center;
}
table.sample td {
  border: 1px solid #000000;
  padding: 5px;
}
table.sample td.action {
  text-align: center;
}
</style>
<table class="sample">
	<tr>
		<th>Фото</th>
		<th>Район <?php echo CHtml::link('&uarr;', array('index/search', 'sort'=>2));?>&nbsp;<?php echo CHtml::link('&darr;', array('index/search', 'sort'=>2, 'type'=>'desc'));?></th>
		<th>Улица <?php echo CHtml::link('&uarr;', array('index/search', 'sort'=>3));?>&nbsp;<?php echo CHtml::link('&darr;', array('index/search', 'sort'=>3, 'type'=>'desc'));?></th>
		<th>Площадь <?php echo CHtml::link('&uarr;', array('index/search', 'sort'=>4));?>&nbsp;<?php echo CHtml::link('&darr;', array('index/search', 'sort'=>4, 'type'=>'desc'));?></th>
		<th>Цена, кв.м <?php echo CHtml::link('&uarr;', array('index/search', 'sort'=>5));?>&nbsp;<?php echo CHtml::link('&darr;', array('index/search', 'sort'=>5, 'type'=>'desc'));?></th>
		<th>Сумма <?php echo CHtml::link('&uarr;', array('index/search', 'sort'=>6));?>&nbsp;<?php echo CHtml::link('&darr;', array('index/search', 'sort'=>6, 'type'=>'desc'));?></th>
		<th>Размещено <?php echo CHtml::link('&uarr;', array('index/search', 'sort'=>7));?>&nbsp;<?php echo CHtml::link('&darr;', array('index/search', 'sort'=>7, 'type'=>'desc'));?></th>
		<th>Телефон <?php echo CHtml::link('&uarr;', array('index/search', 'sort'=>8));?>&nbsp;<?php echo CHtml::link('&darr;', array('index/search', 'sort'=>8, 'type'=>'desc'));?></th>
<?php if ($userTenant):?>
		<th>Поместить</th>
<?php endif;?>
	</tr>
<?php foreach ($Search_obj as $object){
		$this->renderPartial('_resoult', array(
			'obj' =>$object,
			'isUserTenant' =>$userTenant
		));
	} ?>
</table>
<?php }else{ ?>
Поиск не дал результатов.
<?php } ?>
<br />
<div align="center">
<?php
  $this->widget('CLinkPager',array(
		'pages'          =>$pages,
		'maxButtonCount' =>5, # максимальное колличество вкладок на странице
		'header'         =>'<b>Перейти к странице:</b><br>', # заголовок
		'nextPageLabel'  =>'Вперед&gt;', # название кнопок навигаций next и prev
		'prevPageLabel'  =>'&lt;Назад',
  ));
?>
</div>
