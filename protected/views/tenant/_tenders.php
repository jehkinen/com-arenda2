<?php $s = ' помещение';
switch ($row->kind) {
	case 1: {$s = 'Торговое'.$s; break;}
	case 2: {$s = 'Складское\производственное'.$s; break;}
	default: $s = 'Офис';
}
?>
		<div class="infoBlock clrLtGray">
			<div class="rightCol">
				<span class="date"><?php $get = $this->widget('DataParser', array('data_db'=>$row->created)); echo $get->day.' '.$get->month.' '.$get->year; ?></span><br /><i>дата подачи</i>
			</div>
			<div class="leftCol">
				<span class="toObj titl"><?php echo $s; ?></span><br />
				<b>В районе:</b> <?php echo $row->district; if (!empty($row->city)):?> (город <?php echo $row->city; ?>)<?php endif;?><br />
<?php	$s = '';
	if (isset($row->squareMin)) {
		if (isset($row->squareMax))
				$s = $row->squareMin.'-'.$row->squareMax;
		else	$s = 'от '.$row->squareMin;
	} elseif (isset($row->squareMax))
			$s = 'до '.$row->squareMax;
	if ($s!='')
		$s .= ' м<sup>2</sup>';
	else $s = 'не указана';?>
				<b>Площадь:</b> <?php echo $s; ?>
<?php $s = '';
	if (isset($row->priceMin)) {
		if (isset($row->priceMax))
				$s = $row->priceMin.'-'.$row->priceMax;
		else	$s = 'от '.$row->priceMin;
	} elseif (isset($row->priceMax))
			$s = 'до '.$row->priceMax;
	if ($s!=''):?>
				<br /><b>По цене:</b> <?php echo $s; ?> руб.
<?php endif; ?>
				<div class="actions">
					<?php echo CHtml::link('Подробно', array('bid/view', 'id'=>$row->id), array('id'=>$row->id, 'class'=>'more')); ?> 
					<?php echo CHtml::link('Удалить', array('tenant/delete_order', 'id'=>$row->id), array('id'=>'od'.$row->id, 'class'=>'delorder del')); ?> 
				</div>
			</div>
		</div>
