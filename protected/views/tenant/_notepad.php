<?php
switch ($row->status) {
	case 0: {$stat = 'Новое!'; $color_block = '#f88f81'; break;}
	case 1: {$stat = 'Свежее!'; $color_block = '#fcd5cf'; break;}
	default: {$stat = ''; $color_block = '#eeeeee'; }
}
$borderColor='clrRed';
switch ($row->itemStatus) {
	case 0 : {$status='деактивирован'; break;}
	case 1 : {$status = 'отменен или сдан'; break;}
	// case 2 : {$status = 'сдан'; break;}
	case 3 : {$status='на модерации'; $borderColor='clrGray'; break;}
	// case 5 : {$status = 'приостановлен'; break;}
	case 6 : {$status='идут показы'; $borderColor='clrGreen'; break;}	
	default: {$status = 'Не известен'; }	
}
if (isset($row->ownSoName))
	$owner = CHtml::link($row->ownSoName.' '.$row->ownName.' '.$row->ownOtchestvo, array('person/card', 'id'=>$row->ownId)).'<br /><i>Тел: '.$row->ownPhone.'</i>';
else	$owner = 'Объект неавторизованного посетителя';
?>
		<div class="infoBlock <?php echo $borderColor ?>">
			<div class="rightCol">
				<?php echo $owner; ?>
			</div>
			<div class="leftCol">
				<?php echo CHtml::link($row->functionality, array('objects/detail', 'id'=>$row->item), array('target'=>'_blank', 'class'=>'toObj titl')); ?><br /> 
				<b>Адрес:</b> <?php echo $row->city; ?> ул. <?php echo $row->street; ?>, дом <?php echo $row->house; ?> <br /><b>Площадь:</b> <?php echo $row->total_item_sqr; ?> м<sup>2</sup> <b>Цена:</b> <?php echo $row->price; ?> р.<br /><br />
				<span class="<?php echo $borderColor ?>">Статус объекта:</span><br /><b class="<?php echo $borderColor ?>"><?php echo $status; ?></b>
				<div class="actions">
					<?php echo CHtml::link('Удалить', array('tenant/delete_notepad', 'id'=>$row->id), array('id'=>$row->id, 'class'=>'delete del')); ?> 
				</div>
			</div>
		</div>