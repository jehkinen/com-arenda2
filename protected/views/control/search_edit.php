<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/form.css'); ?>
Добавить выриант качества [<?php echo CHtml::link('отмена', array('control/search')); ?>]
<div class="form">
  <?php echo CHtml::form(); ?>
  <?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:'); ?>
<div class="row">
  <?php echo CHtml::activeLabel($form, 'name'); ?><br />
  <?php echo CHtml::activeTextField($form, 'name', array('value'=>$name, 'size'=>30, 'maxlength'=>30)); ?>
  <span id="nameSpan"></span>
</div>
<div class="row buttons">
  <?php echo CHtml::submitButton('Сохранить', array('id'=>"submit")); ?>
  <?php echo CHtml::resetButton('Сброс', array('id'=>'reset')); ?>
</div>
<?php echo CHtml::endForm(); ?>
</div><!--form-->
