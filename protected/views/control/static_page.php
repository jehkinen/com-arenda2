<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/form.css'); ?>
<script type="text/javascript" src="/js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
    tinyMCE.init({
        mode:"textareas",
        theme:"advanced",
        plugins : "spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,
        language:"ru"
    });
</script>
Изменение статичной страницы: "<?php echo $label; ?>" [<?php echo CHtml::link('отмена', array('control/static')); ?>]
<div class="form">
  <?php echo CHtml::form(); ?>
  <?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:'); ?>
<div class="row">
  <?php echo CHtml::activeLabel($form, 'title'); ?>
  <?php echo CHtml::activeTextField($form, 'title', array('value'=>$title, 'size'=>50, 'maxlength'=>100)); ?>
</div>
<div class="row">
  <?php echo CHtml::activeLabel($form, 'description'); ?>
  <?php echo CHtml::activeTextField($form, 'description', array('value'=>$description, 'size'=>50, 'maxlength'=>100)); ?>
</div>
<div class="row">
  <?php echo CHtml::activeLabel($form, 'keywords'); ?>
  <?php echo CHtml::activeTextField($form, 'keywords', array('value'=>$keywords, 'size'=>50, 'maxlength'=>100)); ?>
</div>
<div class="row">
  <?php echo CHtml::activeLabel($form, 'text_page'); ?>
  <?php echo CHtml::textArea('Statics[text_page]', $text, array('rows'=>20, 'cols'=>70)); ?>
</div>
<div class="row buttons">
  <?php echo CHtml::submitButton('Сохранить', array('id'=>"submit")); ?>
  <?php echo CHtml::resetButton('Сброс', array('id'=>'reset')); ?>
</div>
<?php echo CHtml::endForm(); ?>
</div><!--form-->
