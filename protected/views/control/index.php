<style type="text/css">
  div.pane{
    border: 2px solid #aaaaaa;
    padding: 5px 10px;
    margin-bottom: 10px;
  }
div.wrap {overflow:hidden; margin-bottom:30px;}
table.sample {
  border-spacing: 2px;
  border-collapse: collapse;
  float:left;
  width: 45%;
  text-align: left;
}
table.sample th {
  border: 1px solid #000000;
  padding: 5px;
  background-color: #aaaaaa;
  text-align: center;
}
table.sample td {
  border: 1px solid #000000;
  padding: 5px;
}
  
</style>
<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/modal.css'); ?>
<script type="text/javascript" src="js/jquery.simplemodal.js"></script>
<script type="text/javascript" src="js/control.js"></script>
<h4>Статистическая информация</h4>
<div class="wrap">
	<table class="sample">
		<tr><th colspan="2">Пользователи</th></tr>
		<tr><td>Собственников</td><td><?php echo $aUsers['owner'];?></td></tr>
		<tr><td>Арендаторов</td><td><?php echo $aUsers['tenant'];?></td></tr>
		<tr><td align="center">Общее кол-во юзеров</td><td><?php echo $aUsers['total'];?></td></tr>
		<tr><td align="center" colspan="2" style="font-weight:bold;">Зарегистрировано</td></tr>
		<tr><td>Вчера</td><td><?php echo $aUsers['yesterday'];?></td></tr>
		<tr><td>Сегодня</td><td><?php echo $aUsers['today'];?></td></tr>
	</table>
	<table class="sample" style="margin-left:15px;">
		<tr><th colspan="2">Объекты</th></tr>
		<tr><td>Офисов</td><td><?php echo $aItems[0];?></td></tr>
		<tr><td>Торговых</td><td><?php echo $aItems[1];?></td></tr>
		<tr><td>Складов</td><td><?php echo $aItems[2];?></td></tr>
		<tr><td align="center">Всего объектов в базе</td><td><?php echo $aItems['total'];?></td></tr>
		<tr><td align="center" colspan="2" style="font-weight:bold;">Зарегистрировано</td></tr>
		<tr><td>Вчера</td><td><?php echo $aItems['yesterday'];?></td></tr>
		<tr><td>Сегодня</td><td><?php echo $aItems['today'];?></td></tr>
	</table>
</div>
<h4>Модерируемые заявки:</h4>
<?php
	if (!empty($rows)){
		foreach ($rows as $row){
			$this->renderPartial('_index', array('row' =>$row));
		}
	}
?>
<div id="admin-allow">
	<div class="header"></div>
	<div class="message"></div>
	<div class="buttons">
		<div class="yes">Да</div><div class="no close">Нет</div>
	</div>
</div>
<div id="admin-disallow">
	<div class="header"></div>
	<div class="message">
		Можно указать причину, по которой происходит отказ:<br />
		(будет отправленно на e-mail)<br />
		<textarea name="disallow"></textarea>
	</div>
	<div class="buttons">
		<div class="yes">Отказать</div><div class="no close">Отмена</div>
	</div>
</div>
