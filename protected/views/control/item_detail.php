<h2>Детальная информация об объекте</h2>
<?php echo CHtml::link('вернуться', array('control/index')); 
switch ($item->kind) {
	case 1 : {$s = 'торговое'; break;}
	case 2 : {$s = 'склад\производство'; break;}
	default : $s = 'офис';
}?><br />
Добавлен <?php $get = $this->widget('DateFormat', array('kind'=>5,'dbDate'=>$item->created)); echo $get->date; ?> Статус = <?php echo $item->status; ?>.
<?php if (isset($item->status_changed)):?>
 Статус изменен собственником <?php $get = $this->widget('DateFormat', array('kind'=>1,'dbDate'=>$item->status_changed)); echo $get->date; ?>
<?php endif;?>
<?php if ($item->special==1):?>
 Спецпредложение!
<?php endif;?>
<br />Тип помещения: <?php echo $s; ?>; Собственник <?php if (isset($item->login)) { echo $item->firstname.' '.$item->lastname; ?> (<?php echo $item->login; ?>)<?php } else echo 'Аноним';?><br /><br />

<?php if ($item->itemType==0):?>
Объект является самостоятельным: не входит в состав центров и сам не является центром
<?php elseif ($item->itemType==1):?>
Объект является Центром, в который включены объекты:
<ol>
<?php foreach ($childs as $c):?>
	<li><?php echo CHtml::link('Этаж: '.$c->storey.', Название "'.$c->name.'"', array('obj_detail', 'id'=>$c->id));?></li>
<?php endforeach;?>
</ol>
<?php else:?>
Объект входит в состав <?php echo CHtml::link('Центра', array('obj_detail', 'id'=>$item->parent));?>
<?php endif;?>
<br /><br />
Адрес: г.<?php echo $item->city; ?> район <?php echo $item->district; ?> ул.<?php echo $item->street; ?> дом №<?php echo $item->house; ?> корпус <?php echo $item->housing; ?><br />
Класс: <?php echo $item->class; ?>; Тип сооружения: <?php echo $item->kind_structure; ?>; Функциональное назначение: <?php echo $item->functionality; ?>, Название: <?php echo $item->name; ?><br />
Этаж: <?php echo $item->storey; ?> Этажность: <?php echo $item->storeys; ?> Груз.лифт: <?php echo $item->service_lift; ?><br />
Площадь: <?php echo $item->total_item_sqr; ?><br />
Состояние ремонта: <?php echo $item->repair; ?>, Парковка: <?php echo $item->parking; ?>, Кол-во закр. м/м: <?php echo $item->fixed_qty; ?><br />
Система безопасности:<br />
Видеонаблюдение: <?php echo $item->video; ?>, Пропускная система: <?php echo $item->access; ?>, Система пожаротушения: <?php echo $item->firefighting; ?><br />
Арендная ставка: <?php echo $item->price; ?>, Коммунальные услуги: <?php echo $item->pay_utilities; ?><br />
Интернет: <?php echo $item->internet; ?>, Провайдер: <?php echo $item->providers; ?>, Телефония: <?php echo $item->telephony; ?>, Тел.компании: <?php echo $item->tel_company; ?><br />
Форма договора: <?php echo $item->contr_condition; ?>, Быт.помещения: <?php echo $item->welfare; ?>, Системы отопления и водосн.: <?php echo $item->heating; ?><br />
Емкость паллетного хранения: <?php echo $item->pallet_capacity; ?>, Емкость полочного хранения: <?php echo $item->shelf_capacity; ?>, Высота потолков: <?php echo $item->ceiling_height; ?>, Рабочая высота помещения: <?php echo $item->work_height; ?><br />
Кран-балка: <?php echo $item->cathead; ?>, Покрытие пола: <?php echo $item->flooring; ?>, Нагрузка на пол: <?php echo $item->floor_load; ?>, Система вентиляции: <?php echo $item->ventilation; ?><br />
Кондиционирование: <?php echo $item->air_condit; ?>, Возм. рекламы: <?php echo $item->can_reclame; ?><br />
Электр.мощность: <?php echo $item->el_power; ?>, Кол-во ворот: <?php echo $item->gates_qty; ?>, Высота ворот: <?php echo $item->gates_height; ?>, Пандус: <?php echo $item->rampant; ?>, Автомобильные п/п: <?php echo $item->autoways; ?>, Железнодорожные п/п: <?php echo $item->railways; ?><br />
Доп. информация: <?php echo $item->about; ?><br />
<?php if (empty($aFotos)):?>
Фотографий нет<br />
<?php else:?>
Фотографии:
<ul>
<?php foreach($aFotos as $f):?>
	<li><?php echo CHtml::image(Yii::app()->baseUrl.'/storage/images/_thumbs/'.$f->name_medium);?></li>
<?php endforeach;?>
</ul>
<?php endif;?>
<?php if (empty($aAttachs)):?>
Прикрепленных файлов нет<br />
<?php else:?>
Прикрепленные файлы:
<ul>
<?php foreach($aAttachs as $f):?>
	<li><?php echo CHtml::link($f->name, array(Yii::app()->baseUrl.'/storage/files/objs/'.$f->filename));?></li>
<?php endforeach;?>
</ul>
<?php endif;?>
<?php if (!empty($claims)){ ?>
<br /><h3>Жалобы на объект</h3>
<style type="text/css">
table.sample {
  border-spacing: 2px;
  border-collapse: collapse;
  width: 100%;
  text-align: left;
}
table.sample th {
  border: 1px solid #000000;
  padding: 5px;
  background-color: #aaaaaa;
  text-align: center;
}
table.sample td {
  border: 1px solid #000000;
  padding: 5px;
}
table.sample td.action {
  text-align: center;
}
</style>
<table class="sample">
	<tr>
		<th>Подана</th>
		<th>Пожаловался</th>
		<th>Содержание</th>
	</tr>
<?php foreach($claims as $claim) {
switch ($claim->kind) {
	case 1 : {$s = 'Неверная информация об объекте'; break;}
	case 2 : {$s = 'Объявление неактульно (сдано)'; break;}
	case 3 : {$s = 'Неадекватное поведение'; break;}
	default: $s = 'Неизвестный код';
}
?>
<tr>
	<td><?php $get = $this->widget('DateFormat', array('kind'=>5,'dbDate'=>$claim->sended)); echo $get->date; ?></td>
	<td><?php echo CHtml::link($claim->tenLogin.' ('.$claim->tenFirstname.' '.$claim->tenLastname.')', array('adminusers/detail', 'id'=>$claim->tenant));?></td>
	<td><?php echo $s; ?></td>
</tr>
<?php } ?>
</table>
<?php } ?>

