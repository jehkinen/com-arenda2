<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/form.css'); ?>
<style type="text/css">
	div.settings {display:inline-block;}
	div.settings {display:block;}
	div.settings {margin:20px;}
	div.settings input {display:inline;}
	div.settings label {display:block; margin-bottom:4px; font-size:14px;}
	div.settings label.chkbxSpan {display: inline; margin-bottom:5px; padding-left:5px;}
</style>
Редактирование рекламной кампании [<?php echo CHtml::link('отмена', array('adminreclamebanners/view'));?>]
<div class="form">
	<?php echo CHtml::form();?>
	<?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:'); ?>
<div class="row">
	<?php echo CHtml::activeLabel($form, 'name'); ?><br />
	<?php echo CHtml::activeTextField($form, 'name', array('value'=>$oName, 'size'=>100, 'maxlength'=>127));?>
</div>
<div class="row">
	<?php echo CHtml::activeLabel($form, 'width'); ?><br />
	<?php echo CHtml::activeTextField($form, 'width', array('value'=>$oWidth, 'size'=>7, 'maxlength'=>4));?>
</div>
<div class="row">
	<?php echo CHtml::activeLabel($form, 'height'); ?><br />
	<?php echo CHtml::activeTextField($form, 'height', array('value'=>$oHeight, 'size'=>7, 'maxlength'=>4));?>
</div>
<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash4/cabs/swflash.cab#version=4,0,0,0" width="<?php echo $oWidth;?>" height="<?php echo $oHeight;?>">
	<param name="movie" value="storage/images/banners/<?php echo $banName;?>">
	<param name="play" value="true">
	<param name="loop" value="true">
	<param name="quality" value="high">
	<embed src="storage/images/banners/<?php echo $banName;?>" play="true" loop="true" quality="high" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" width="<?php echo $oWidth;?>" height="<?php echo $oHeight;?>"></embed>
</object><br /><br />
<fieldset title="Настройки">
<div class="settings">
	<div class="row">
		<?php if ($oIsActive=='' || $oIsActive=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'isActive', $cba).' '.CHtml::activeLabel($form, 'isActive', array('class'=>'chkbxSpan')); ?>
	</div>
	<div class="row">
		<?php if ($oOnMain=='' || $oOnMain=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'onMain', $cba).' '.CHtml::activeLabel($form, 'onMain', array('class'=>'chkbxSpan')); ?>
	</div>
	<div class="row">
		<?php if ($oOnSearch=='' || $oOnSearch=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'onSearch', $cba).' '.CHtml::activeLabel($form, 'onSearch', array('class'=>'chkbxSpan')); ?>
	</div>
	<div class="row">
		<?php if ($oOnAny=='' || $oOnAny=='0') $cba = array(); else $cba = array('checked'=>'checked'); echo CHtml::activeCheckBox($form, 'onAny', $cba).' '.CHtml::activeLabel($form, 'onAny', array('class'=>'chkbxSpan')); ?>
	</div>
	<div class="row">
		<?php echo CHtml::activeLabel($form, 'start'); ?><?php $get = $this->widget('DateFormat', array('kind'=>1,'dbDate'=>$oStart)); echo CHtml::activeTextField($form, 'start', array('value'=>$get->date, 'size'=>10, 'maxlength'=>10)); ?>
	</div>
	<div class="row">
		<?php echo CHtml::activeLabel($form, 'stop'); ?><?php $get = $this->widget('DateFormat', array('kind'=>1,'dbDate'=>$oStop)); echo CHtml::activeTextField($form, 'stop', array('value'=>$get->date, 'size'=>10, 'maxlength'=>10)); ?>
	</div>
	<div class="row">
		<?php echo CHtml::activeLabel($form, 'qty'); ?><?php echo CHtml::activeTextField($form, 'qty', array('value'=>$oQty, 'size'=>10, 'maxlength'=>5)); ?>
	</div>
</div>
</fieldset>

<div class="row buttons">
	<?php echo CHtml::submitButton('Сохранить', array('id'=>"submit")); ?>
	<?php echo CHtml::resetButton('Сброс', array('id'=>'reset')); ?>
</div>
<?php echo CHtml::endForm(); ?>
</div><!--form-->