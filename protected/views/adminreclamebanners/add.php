<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/form.css'); ?>
<style type="text/css">
	div.settings {display:inline-block;}
	div.settings {display:block;}
	div.settings {margin:20px;}
	div.settings input {display:inline;}
	div.settings label {display:block; margin-bottom:4px; font-size:14px;}
	div.settings label.chkbxSpan {display: inline; margin-bottom:5px; padding-left:5px;}
</style>
Добавление рекламной кампании [<?php echo CHtml::link('отмена', array('adminreclamebanners/view'));?>]
<div class="form">
  <?php echo CHtml::form('', 'post', array('enctype'=>'multipart/form-data')); ?>
	<?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:'); ?>
<div class="row">
	<?php echo CHtml::activeLabel($form, 'name'); ?><br />
	<?php echo CHtml::activeTextField($form, 'name', array('size'=>100, 'maxlength'=>127));?>
</div>
<div class="row">
	<?php echo CHtml::activeLabel($form, 'width'); ?><br />
	<?php echo CHtml::activeTextField($form, 'width', array('size'=>7, 'maxlength'=>4));?>
</div>
<div class="row">
	<?php echo CHtml::activeLabel($form, 'height'); ?><br />
	<?php echo CHtml::activeTextField($form, 'height', array('size'=>7, 'maxlength'=>4));?>
</div>
<div class="row">
    <?php echo CHtml::activeLabel($form, 'filename'); ?><br />
    <?php echo CHtml::activeFileField($form, 'filename'); ?>
</div>
<fieldset title="Настройки">
<div class="settings">
	<div class="row">
		<?php echo CHtml::activeCheckBox($form, 'isActive').' '.CHtml::activeLabel($form, 'isActive', array('class'=>'chkbxSpan'));?>
	</div>
	<div class="row">
		<?php echo CHtml::activeCheckBox($form, 'onMain').' '.CHtml::activeLabel($form, 'onMain', array('class'=>'chkbxSpan'));?>
	</div>
	<div class="row">
		<?php echo CHtml::activeCheckBox($form, 'onSearch').' '.CHtml::activeLabel($form, 'onSearch', array('class'=>'chkbxSpan'));?>
	</div>
	<div class="row">
		<?php echo CHtml::activeCheckBox($form, 'onAny').' '.CHtml::activeLabel($form, 'onAny', array('class'=>'chkbxSpan'));?>
	</div>
	<div class="row">
		<?php echo CHtml::activeLabel($form, 'start'); ?><?php echo CHtml::activeTextField($form, 'start', array('size'=>10, 'maxlength'=>10));?>
	</div>
	<div class="row">
		<?php echo CHtml::activeLabel($form, 'stop'); ?><?php echo CHtml::activeTextField($form, 'stop', array('size'=>10, 'maxlength'=>10));?>
	</div>
	<div class="row">
		<?php echo CHtml::activeLabel($form, 'qty'); ?><?php echo CHtml::activeTextField($form, 'qty', array('size'=>10, 'maxlength'=>5));?>
	</div>
</div>
</fieldset>

<div class="row buttons">
	<?php echo CHtml::submitButton('Сохранить', array('id'=>"submit")); ?>
</div>
<?php echo CHtml::endForm(); ?>
</div><!--form-->