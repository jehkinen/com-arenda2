<?php if (empty($mess) || empty($user)):?>
Ошибка при получении данных. Не найдено сообщение или пользователь.<br />
Перейти на страницу <?php echo CHtml::link('управления пользователями', array('adminusers/view')); ?>.
<?php else:?>
Пользователь: <?php echo CHtml::link($user->so_name.' '.$user->name.' '.$user->otchestvo, array('adminusers/detail', 'id'=>$user->id));?>&nbsp;<?php echo CHtml::link('Написать сообщение пользователю', array('sendmessage', 'id'=>$user->id)); ?><br />
Сообщение от <?php $get = $this->widget('DateFormat', array('kind'=>1,'dbDate'=>$mess->sdate)); echo $get->date; ?> отправлено  <?php echo ($mess->success==1)?'успешно':', но не доставлено';?><br />
<?php
switch ($mess->kind) {
	case 0 : {$type = 'sms'; break;}
	case 1 : {$type = 'email уведомление'; break;}
	case 2 : {$type = 'отказ в модерации'; break;}
	case 3 : {$type = 'активация аккаунта'; break;}
	case 4 : {$type = 'письмо администратора'; break;}
	default : $type = 'в базе неопределен'; 
}
?>
Тип сообщения: <?php echo $type;?><br />
Текст:<br /><?php echo $mess->txt;?>
<?php endif;?>