<?php if (empty($user)):?>
Ошибка при получении данных. Не найден пользователь.<br />
Перейти на страницу <?php echo CHtml::link('управления пользователями', array('adminusers/view')); ?>.
<?php else:?>
<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/form.css'); ?>
<?php echo CHtml::link('Вернуться', array('adminusers/detail', 'id'=>$user->id)); ?><br />
<h3>Cообщение пользователю</h3>
Получатель: <?php echo $user->so_name.' '.$user->name.' '.$user->otchestvo;?><br />
<div class="form">
  <?php echo CHtml::form(); ?>
  <?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:'); ?>

<div class="row">
  <?php echo CHtml::activeLabel($form, 'subject'); ?><br />
  <?php echo CHtml::activeTextField($form, 'subject', array('value'=>$defSubject, 'size'=>25, 'maxlength'=>45)); ?>
</div>
<div class="row">
  <?php echo CHtml::activeLabel($form, 'txt'); ?><br />
  <?php echo CHtml::textArea('UserSendMessage_form[txt]', $oldTxt, array('rows'=>7, 'cols'=>80)); ?>
</div>
<div class="row buttons">
<?php echo CHtml::submitButton('Отправить', array('id'=>"submit")); ?>
</div>
<?php echo CHtml::endForm(); ?>
</div><!--form-->

<?php endif;?>