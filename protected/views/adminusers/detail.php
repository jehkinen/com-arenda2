<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/modal.css'); ?>
<style type="text/css">
table.sample {
  border-spacing: 2px;
  border-collapse: collapse;
  width: 100%;
  text-align: left;
}
table.sample th {
  border: 1px solid #000000;
  padding: 5px;
  background-color: #aaaaaa;
  text-align: center;
}
table.sample td {
  border: 1px solid #000000;
  padding: 5px;
}
table.sample td.action {
  text-align: center;
}
</style>
<script type="text/javascript" src="js/jquery.simplemodal.js"></script>
<script type="text/javascript" src="js/adminusers.js"></script>
<div id="mwindow">
	<div class="header"></div>
	<div class="message"></div>
	<div class="buttons">
		<div class="close">Закрыть</div>
	</div>
</div>
<h2>Детальная информация о пользователе</h2>
<?php echo CHtml::link('вернуться', array('adminusers/view')); 
switch ($row->state) {
	case 0 : {$s = 'удален'; break;}
	case 1 : {$s = 'заблокирован администратором'; break;}
	case 2 : {$s = 'ожидает активации'; break;}
	case 5 : {$s = 'действующий (полный доступ)'; break;}
}
switch ($row->role) {
	case 'admin' : {$r = 'администратор'; break;}
	case 'owner' : {$r = 'собственник'; break;}
	case 'tenant' : {$r = 'арендатор'; break;}
}?><br /><br />
Зарегистрирован <?php $get = $this->widget('DateFormat', array('kind'=>5,'dbDate'=>$row->created)); echo $get->date; ?>; Статус: <?php echo $s; ?>; Роль: <?php echo $r; ?>; Логин: <?php echo $row->username; ?><br />
Фамилия: <?php echo $row->so_name; ?>; Имя: <?php echo $row->name; ?>; Отчество: <?php echo $row->otchestvo; ?><br />
Электронная почта: <?php echo $row->e_mail; ?><br />
Телефон: <?php echo $row->phone; ?><br />
Настройки информирования:<br />
Оповещение по СМС - <?php echo ($row->info_flag_phone)?'Включено':'Отключено'; ?>. На телефон: <?php if (isset($row->info_phone)) echo $row->info_phone; ?><br />
Оповещение по e_mail - <?php echo ($row->info_flag_email)?'Включено':'Отключено'; ?>. На ящик: <?php if (isset($row->info_email)) echo $row->info_email; ?><br />
<?php if($row->role=='owner'):?>
Извещать меня об успешной модерации объектов (e-mail) - <?php echo ($row->info_moder)?'Включено':'Отключено'; ?><br />
Уведомлять меня о поступлении заявок от арендаторов - <?php echo ($row->info_order)?'Включено':'Отключено'; ?><br />
<?php if (!empty($items)){ ?>
<h3>Объекты пользователя</h3>
<table class="sample">
	<tr>
		<th>Создан</th>
		<th>Статус</th>
		<th>Объект</th>
		<th>Тип помещ.</th>
		<th>Адрес</th>
		<th>Показы</th>
	</tr>
<?php foreach($items as $obj) {
switch ($obj->kind) {
	case 1 : {$type = 'торговое'; break;}
	case 2 : {$type = 'склад\произв'; break;}
	default : $type = 'офис'; 
}
switch ($obj->status) {
	case 0 : {$status='деактивирован'; break;}
	case 1 : {$status = 'отменен'; break;}
	//case 2 : {$status = 'сдан'; break;}
	case 3 : {$status='на модерации'; break;}
	//case 5 : {$status = 'приостановлен'; break;}
	case 6 : {$status='идут показы'; break;}	
	default: {$status = 'Не известен'; }
}
?>
<tr>
	<td><?php $get = $this->widget('DateFormat', array('kind'=>5,'dbDate'=>$obj->created)); echo $get->date; ?></td>
	<td><?php echo $status; ?></td>
	<td><?php if ($obj->parent==0) if ($obj->hasChild==0) echo 'обычный'; else echo 'центр'; else echo CHtml::link('помещение', array('control/obj_detail', 'id'=>$obj->parent)); ?></td>
	<td><?php echo $type; ?></td>
	<td><?php echo CHtml::link($obj->city.': '.$obj->district.' / '.$obj->street.', '.$obj->house.'('.$obj->housing.')', array('control/obj_detail', 'id'=>$obj->id)); ?></td>
	<td><?php echo $obj->showQty; ?></td>
</tr>
<?php } ?>
</table>
<?php } ?>
<?php elseif($row->role=='tenant'):?>
<?php if (!empty($orders)){ ?>
<h3>Заявки пользователя</h3>
<table class="sample">
	<tr>
		<th>Создана</th>
		<th>Тип помещ.</th>
		<th>Адрес</th>
		<th>Цены</th>
		<th>Площади</th>
	</tr>
<?php foreach($orders as $tender) {
switch ($tender->kind) {
	case 1 : {$type = 'торговое'; break;}
	case 2 : {$type = 'склад\произв'; break;}
	default : $type = 'офис'; 
}
$s = $r = 'не указаны';
if (isset($tender->squareMin)) {
	if (isset($tender->squareMax))
			$s = $tender->squareMin.'-'.$tender->squareMax;
	else	$s = 'от '.$tender->squareMin;
} elseif (isset($tender->squareMax))
	$s = 'до '.$tender->squareMax;
if (isset($tender->priceMin)) {
	if (isset($tender->priceMax))
		$r = $tender->priceMin.'-'.$tender->priceMax;
	else	$r = 'от '.$tender->priceMin;
} elseif (isset($tender->priceMax))
	$r = 'до '.$tender->priceMax;
?>
<tr>
	<td><?php echo $tender->created; ?></td>
	<td><?php echo $type; ?></td>
	<td><?php echo CHtml::link($tender->city.': '.$tender->district.' / '.$tender->street, array('bid/view', 'id'=>$tender->id)); ?></td>
	<td><?php echo $r; ?></td>
	<td><?php echo $s; ?></td>
</tr>
<?php } ?>
</table>
<?php } ?>
<?php if (!empty($notes)){ ?>
<h3>Блокнот пользователя</h3>
<table class="sample">
	<tr>
		<th>Создана</th>
		<th>Тип помещ.</th>
		<th>Адрес</th>
	</tr>
<?php foreach($notes as $note) {
switch ($note->kind) {
	case 1 : {$type = 'торговое'; break;}
	case 2 : {$type = 'склад\произв'; break;}
	default : $type = 'офис'; 
}
?>
<tr>
	<td><?php $get = $this->widget('DateFormat', array('kind'=>5,'dbDate'=>$note->created)); echo $get->date; ?></td>
	<td><?php echo $type; ?></td>
	<td><?php echo CHtml::link($note->city.': '.$note->district.' / '.$note->street.', '.$note->house, array('control/obj_detail', 'id'=>$note->item)); ?></td>
</tr>
<?php } ?>
</table>
<?php } ?>
<?php endif;?>
<?php if (!empty($claims)){ ?>
<br /><h3>Жалобы <?php echo ($row->role=='tenant')?'поданные пользователем':'на объекты пользователя';?></h3>
<table class="sample">
	<tr>
		<th>Подана</th>
		<th>Объект</th>
		<th><?php echo ($row->role=='tenant')?'Владелец':'Пожаловался';?></th>
		<th>Содержание</th>
	</tr>
<?php foreach($claims as $claim) {
switch ($claim->kind) {
	case 1 : {$s = 'Неверная информация об объекте'; break;}
	case 2 : {$s = 'Объявление неактульно (сдано)'; break;}
	case 3 : {$s = 'Неадекватное поведение'; break;}
	default: $s = 'Неизвестный код';
}
?>
<tr>
	<td><?php $get = $this->widget('DateFormat', array('kind'=>5,'dbDate'=>$claim->sended)); echo $get->date; ?></td>
	<td><?php echo  CHtml::link($claim->itemName.', ул. '.$claim->itemStreet.', '.$claim->itemHouse, array('control/obj_detail', 'id'=>$claim->item));?></td>
	<td><?php if($row->role=='tenant') 
			echo CHtml::link($claim->ownLogin.' ('.$claim->ownFirstname.' '.$claim->ownLastname.')', array('adminusers/detail', 'id'=>$claim->itemOwner));
		else echo CHtml::link($claim->tenLogin.' ('.$claim->tenFirstname.' '.$claim->tenLastname.')', array('adminusers/detail', 'id'=>$claim->tenant));?></td>
	<td><?php echo $s; ?></td>
</tr>
<?php } ?>
</table>
<?php } ?>
<?php if (!empty($logMess)):?>
<br /><h3>Сообщения пользователю</h3>
<table class="sample">
	<tr>
		<th>Отправлено</th>
		<th>Вид / Тема</th>
		<th>Адрес</th>
		<th>Текст</th>
	</tr>
<?php foreach($logMess as $mess) {
switch ($mess->kind) {
	case 0 : {$type = 'sms'; break;}
	case 1 : {$type = 'email уведомление'; break;}
	case 2 : {$type = 'отказ в модерации'; break;}
	case 3 : {$type = 'активация аккаунта'; break;}
	case 4 : {$type = 'письмо администр.'; break;}
	case 5 : {$type = 'sms от администр.'; break;}
	default : $type = 'ОШИБКА!'; 
}
?>
<tr>
	<td><?php $get = $this->widget('DateFormat', array('kind'=>5,'dbDate'=>$mess->sdate)); echo $get->date; echo ($mess->success==1)?'<br />успешно':'<br />ошибка при отправке';?></td>
	<td><?php echo $type; ?></td>
	<td><?php echo $mess->address; /* $mess->txt*/ ?></td>
	<td><?php echo CHtml::link('весь текст', array('showmessage', 'id'=>$mess->id), array('class'=>'showmess', 'id'=>$mess->id)); ?></td>
</tr>
<?php } ?>
</table>
<?php endif;?>
<?php echo CHtml::link('Написать сообщение пользователю', array('sendmessage', 'id'=>$row->id));
if (isset($row->info_phone)) $b = true;
else {
	$fun = $this->widget('AnalyzePhone', array('phone'=>$row->phone));
	$b = $fun->isCellphone;
}
if ($b) echo ' | '.CHtml::link('Послать SMS-сообщение пользователю', array('sendsms', 'id'=>$row->id));
?>

<br /> Баланс, статистика платежей и списаний <br /> 
