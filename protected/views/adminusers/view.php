<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/form.css'); ?>
Фильтры: <?php echo CHtml::link('Сбросить', array('adminusers/unset')); ?><br />
<div class="form">
	<?php echo CHtml::form(); ?>
	<?php echo CHtml::errorSummary($form, 'Исправьте, пожалуйста, следующие ошибки:'); ?>
	<strong><?php echo CHtml::activeLabel($form, 'role'); ?>:</strong> <?php echo CHtml::dropDownList('AdminUsers_form[role]', $role, $Roles); ?>
	<strong><?php echo CHtml::activeLabel($form, 'status'); ?>:</strong> <?php echo CHtml::dropDownList('AdminUsers_form[status]', $status, $Statuses); ?>
	<div class="row buttons">
		<br /><?php echo CHtml::submitButton('Найти', array('id'=>'submit')); ?><br />
	</div>
	<?php echo CHtml::endForm(); ?>
</div><!--form-->
<?php if (is_array($rows)):?>
<hr>Найдено пользователей: <?php echo $foundQty; ?><br /><br />
<?php if (!empty($rows)):?>
<style type="text/css">
table.sample {
  border-spacing: 2px;
  border-collapse: collapse;
  width: 100%;
  text-align: left;
}
table.sample th {
  border: 1px solid #000000;
  padding: 5px;
  background-color: #aaaaaa;
  text-align: center;
}
table.sample td {
  border: 1px solid #000000;
  padding: 5px;
}
table.sample td.action {
  text-align: center;
}
</style>
<script type="text/javascript" src="js/jquery.simplemodal.js"></script>
<script type="text/javascript" src="js/adminusers.js"></script>
<?php echo CHtml::cssFile(Yii::app()->baseUrl.'/css/modal.css'); ?>
<div id="mwindow">
	<div class="header"></div>
	<div class="message">
		<input type="radio" name="stat" value="0" alt="удален" id="rb0" /><label for="rb0">удалить</label><br />
		<input type="radio" name="stat" value="1" alt="заблок." id="rb1" /><label for="rb1">заблокировать</label><br />
		<input type="radio" name="stat" value="5" alt="действ." id="rb2" /><label for="rb2">полные права</label>
	</div>
	<div class="buttons">
		<div class="yes">Сохранить</div><div class="no close">Отмена</div>
	</div>
</div>
<table class="sample">
	<tr>
		<th>Дата <?php echo CHtml::link('&uarr;', array('adminusers/view', 'sort'=>1));?>&nbsp;<?php echo CHtml::link('&darr;', array('adminusers/view', 'sort'=>1, 'type'=>'desc'));?></th>
		<th>Роль</th>
		<th>Статус</th>
		<th>Логин <?php echo CHtml::link('&uarr;', array('adminusers/view', 'sort'=>4));?>&nbsp;<?php echo CHtml::link('&darr;', array('adminusers/view', 'sort'=>4, 'type'=>'desc'));?></th>
		<th>Ф.И.О <?php echo CHtml::link('&uarr;', array('adminusers/view', 'sort'=>5));?>&nbsp;<?php echo CHtml::link('&darr;', array('adminusers/view', 'sort'=>5, 'type'=>'desc'));?></th>
		<th>e-mail</th>
		<th>Заявки<br />Объекты <?php echo CHtml::link('&uarr;', array('adminusers/view', 'sort'=>8));?>&nbsp;<?php echo CHtml::link('&darr;', array('adminusers/view', 'sort'=>8, 'type'=>'desc'));?></th>
		<th>Баланс <?php echo CHtml::link('&uarr;', array('adminusers/view', 'sort'=>9));?>&nbsp;<?php echo CHtml::link('&darr;', array('adminusers/view', 'sort'=>9, 'type'=>'desc'));?></th>
	</tr>
<?php
		foreach ($rows as $row)
			$this->renderPartial('_row', array('obj' =>$row));
?>
<?php endif; ?>
</table>
<div align="center">
<?php
  $this->widget('CLinkPager',array(
		'pages'          =>$pages,
		'maxButtonCount' =>5, # максимальное колличество вкладок на странице
		'header'         =>'<b>Перейти к странице:</b><br>', # заголовок
		'nextPageLabel'  =>'Вперед&gt;', # название кнопок навигаций next и prev
		'prevPageLabel'  =>'&lt;Назад',
  ));
?>
</div>
<?php endif; ?>
