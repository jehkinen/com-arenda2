<?php class ObjectAddFilter extends CFilter{
	public function filter($filterChain){

		if (!Yii::app()->user->isGuest)
			if (Yii::app()->user->id['user_role'] == 'admin' || Yii::app()->user->id['user_role'] == 'tenant')				$filterChain->controller->render('filter');			else	$filterChain->run();
		else{
			Yii::app()->user->setState('ses_want_url', 'object/add/step/step1');
			Yii::app()->request->redirect('/authorization/login', false);
		}
	}}?>