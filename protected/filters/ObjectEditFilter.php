<?php class ObjectEditFilter extends CFilter{

	public function filter($filterChain){
		if (Yii::app()->user->isGuest) {
			Yii::app()->user->setState('ses_want_url', Yii::app()->request->url);
			Yii::app()->request->redirect('/authorization/login', false);
		} elseif (Yii::app()->user->id['user_role'] == 'tenant')
				$filterChain->controller->render('filter-edit-tenant');
			else	$filterChain->run();
	}
}?>