$(document).ready(function() {
	$(".popup_link").click(function(e) {
		$($(this).attr("href")).lightbox_me({
			overlayCSS: {background: 'black', opacity: 0},
			closeSelector: ".popup_close",
			centered: false,
			overlaySpeed: 50,
			lightboxSpeed: 300
			});
		e.preventDefault();
		return false;
	});
	$("div.story table tr:odd").addClass("even");
});