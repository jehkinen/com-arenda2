﻿/*  аНаЕб?б?аАаНаДаАб?б?аНб?аЕ select 
    аВаЕб?б?аИб? 1.1
    аИб?аПаОаЛб?аЗб?аЕб?б?б? аБаИаБаЛаИаОб?аЕаКаА jQuery 1.2.6
	аАаВб?аОб? Ksayri
	б?б?б?аАаНаИб?аА б? аОаПаИб?аАаНаИаЕаМ http://www.xiper.net/html-and-css-tricks/verstka-form/nice-select-jquery.html      */

jQuery.noConflict();

jQuery(document).ready(function(){

// аПб?аИ аЗаАаГб?б?аЗаКаЕ б?б?б?аАаНаИб?б? аВб?аЗб?аВаАаЕаМ б?аКб?аИаПб? аЗаАаМаЕаНб? б?аЕаЛаЕаКб?аОаВ
changeSelects();
});

function optionClickHover()
{
	// аВб?аБаОб? аПаОаЗаИб?аИаИ аВ б?аПаИб?аКаЕ option
jQuery('div.optionsDivInvisible > span').mousedown(
function()
{
	jQuery(this.parentNode.parentNode.getElementsByTagName('input').item(1)).attr("value",jQuery(this).attr("name")); //  аЗаНаАб?аЕаНаИаЕ (value) option аЗаАаНаОб?аИаМ аВ input
	jQuery(this.parentNode.parentNode.getElementsByTagName('input').item(0)).attr("value",jQuery(this).text()); // б?аЕаКб?б? аДаЛб? аОб?аОаБб?аАаЖаЕаНаИб? аВ б?аЕаЛаЕаКб?аЕ
	jQuery(this.parentNode).css("display","none"); //  б?аКб?б?аВаАаЕаМ аВб?аПаАаДаАб?б?аИаЙ б?аПаИб?аОаК
});

//  аПаОаДб?аВаЕб?аКаА аОаПб?аИаОаНаА аПб?аИ аНаАаВаЕаДаЕаНаИаИ (б?.аК. аИаЕ6 аПаОаНаИаАаЕб? hover б?аОаЛб?аКаО аНаА б?б?б?аЛаКаАб?)
jQuery('div.optionsDivInvisible > span').mouseover(
function()
{
  this.className="over";
});

jQuery('div.optionsDivInvisible > span').mouseout(
function()
{
  this.className="";
});


// б?б?аНаКб?аИб? аОаБб?аАаБаОб?аКаИ аКаЛаИаКаА аНаА б?аЛаЕаМаЕаНаАб? б?аПаИб?аКаА аДаЛб? б?аЕаЛаЕаКб?аА б? аПб?аОаКб?б?б?аКаОаЙ
	jQuery('div.scroller-container > span').click(
	function()
		{
			
		var optionsDivInVisible = jQuery(this).parents("div.optionsDivInvisible");
		var hiddenInput = optionsDivInVisible.parent().find('input').eq(1); //б?аКб?б?б?б?аЙ аИаПб?б? б?аО аЗаНаАб?аЕаНаИаЕаМ б?аЕаЛаЕаКб?аА
		var valueOption = jQuery(this).attr("name"); //аЗаНаАб?аЕаНаИаЕ optiona
		var textOption = jQuery(this).text(); // б?аЕаКб?б? optiona
		var inputInSelect = optionsDivInVisible.parent().find("input").eq(0); // аПаОаЛаЕ, аКаОб?аОб?аОаЕ б?аОаДаЕб?аЖаИб? б?аЕаКб?б? б?аЕаЛаЕаКб?аА
		
		inputInSelect.val(textOption); // аОаБаНаОаВаЛб?аЕаМ б?аЕаКб?б? б?аЕаЛаЕаКб?аА
		if(hiddenInput) hiddenInput.val(valueOption); // аДаОаБаАаВаЛб?аЕаМ аЗаНаАб?аЕаНаИаЕ б?аЕаЛаЕаКб?аА аВ б?аКб?б?б?б?аЙ input
		optionsDivInVisible.css("display","none"); // б?аКб?б?аВаАаЕаМ аВб?аПаАаДаАб?б?аИаЙ б?аПаИб?аОаК
		}
);
	
// hover option for ie

	jQuery('div.scroller-container > span').mouseover(
	function()
		{
  		this.className="over";
		});

	jQuery('div.scroller-container > span').mouseout(
	function()
		{
  		this.className="";
		});
}

// аЗаАаМаЕаНаА аОаБб?б?аНб?б? select аПб?аИ аВб?аЗаОаВаЕ б?б?аНаКб?аИаИ
function changeSelects()
{ 
/* б?аКб?аИаПб? аНаАб?аОаДаИб? аВб?аЕ б?аЕаЛаЕаКб?б? аНаА б?б?б?аАаНаИб?аЕ аИ аЗаАаМаЕаНб?аЕб? аИб? аНаА б?аПаЕб? аКаОаНб?б?б?б?аКб?аИаИ */
jQuery("select").each(
function(num)
{
							// num - аНаОаМаЕб? б?аЕаЛаЕаКб?аА аНаА б?б?б?аАаНаИб?аЕ
var selectOrCombobox = 8; 	// аЕб?аЛаИ аВ б?аЕаЛаЕаКб?аЕ optionаОаВ <= б?аЛаАаГаА selectOrCombobox, б?аОаГаДаА б?аЕаЛаЕаКб? аБаЕаЗ аПб?аОаКб?б?б?аКаИ, аБаОаЛб?б?аЕ - аДаОаБаАаВаЛб?аЕб?б?б? аПб?аОаКб?б?б?аКаА
var kolOptions=jQuery(this).children().length; // б?аИб?аЛаО option аВ select

var className=this.className;//  аИаМб? аКаЛаАб?б?аА б?аЕаКб?б?аЕаГаО б?аЕаЛаЕаКб?аА
var selName=this.name;		 //  name б?аЕаЛаЕаКб?аА
var selID=this.id;			 // id б?аЕаЛаЕаКб?аА       	

// аОаПб?аЕаДаЕаЛб?аЕаМ б?аИаП б?аЕаЛаЕаКб?аА

if(kolOptions>selectOrCombobox)
{
	
	jQuery(this).css("display","none"); // б?аКб?б?аВаАаЕаМ select б?б?аОаБб? аЗаАаМаЕаНаА аВаИаЗб?аАаЛб?аНаО аПб?аОб?аЛаА аБаОаЛаЕаЕ аГаЛаАаДаКаО

	// б?б?аОаБб? аВаЕб?б?аНаИаЕ б?аЕаЛаЕаКб?б? аНаОб?аМаАаЛб?аНаО аПаЕб?аЕаКб?б?аВаАаЛаИ аНаИаЖаНаИаЕ, аАаВб?аОаМаАб?аИб?аЕб?аКаИ аДаОаБаАаВаЛб?аЕаМ z-index аВ аПаОб?б?аДаКаЕ б?аБб?аВаАаНаИб?
	// аДаЛб? б?аНаИаКаАаЛб?аНаОб?б?аИ id б?аЛаЕаМаЕаНб?аОаВ б?аЕаЛаЕаКб?аОаВ аИб?аПаОаЛб?аЗб?аЕб?б?б? num - аПаОб?б?аДаКаОаВб?аЙ аНаОаМаЕб? б?аЕаЛаЕаКб?аА аНаА б?б?б?аАаНаИб?аЕ
	jQuery(this).before("<div class='selectArea "+className+"' style='z-index:"+(100-num)+"'>"+
							"<div class='left'></div>"+ // аЛаЕаВаАб? б?аАб?б?б? select
							"<div class='center_a'></div>"+ // аПб?аАаВаАб? б?аАб?б?б? (б?б?б?аЕаЛаКаА) select
							"<div class='optionsDivInvisible' id='optInvis_"+num+"'>"+ // аКаОаНб?аЕаЙаНаЕб? аДаЛб? option
								"<div class='scrollbar-container' id='scroll_container_"+num+"'>"+ // аКаОаНб?аЕаЙаНаЕб? аДаЛб? б?аКб?аОаЛаЛаИаНаГаА   
									"<div class='scrollbar-up'></div>"+ // б?б?б?аЕаЛаКаА аВаВаЕб?б? аДаЛб? аПб?аОаКб?б?б?аКаИ
									"<div class='scrollbar-down' id='scrollbar-down'></div>"+ // б?б?б?аЕаЛаКаА аВаНаИаЗ
									"<div class='scrollbar-track' id='scrollbar-track'><b class='scrollbar-handle' id='scrollbar-handle'></b></div>"+ // б?б?б?аК б?аКб?аОаЛаЛаИаНаГаА
									"<div class='container2' id='container'>"+ 
										"<div class='scroller-1' id='scroller_"+num+"'>"+
										"<div class='scroller-container' id='"+selID+"_fake'></div>"+
										"</div>"+
									"</div>"+
								"</div>"+
							"</div>"+
							"<input id='v"+selID+"' />"+ // аВаИаДаИаМб?аЙ б?аЕаКб?б? select
							"<input type='hidden' name='"+selName+"' id='"+selID+"' />"+ // аЗаНаАб?аЕаНаИаЕ select
						"</div>");

// аЗаАаПаОаЛаНб?аЕаМ <option>

var containerFofSel=jQuery("#scroller_"+num+" > div");
var selArr=jQuery(this).children(); // аМаАб?б?аИаВ аВб?аЕб? option б?аЕаЛаЕаКб?аА
var sel_i; // б?аЕаКб?б?аИаЙ аНаОаМаЕб? option
for(var i=0;i<kolOptions;i++) // аПб?аЕаОаБб?аАаЗаОаВб?аВаАаЕаМ аВб?аЕ option аВ span
{							  // name spana - аЗаНаАб?аЕаНаИаЕ option
							  // б?аЕаКб?б? spana - б?аЕаКб? option
	sel_i = selArr.eq(i);
	containerFofSel.append("<span name='"+sel_i.val()+"'>"+sel_i.text()+"</span>");
}

// аНаАб?аАаЛб?аНаОаЕ аЗаНаАб?аЕаНаИаЕ б?аЕаЛаЕаКб?аА  
//jQuery("#"+selID).val(jQuery(this).children("option[selected='selected']").val());
jQuery("#"+selID).val(jQuery("#"+jQuery(this).attr('id')+" > option[selected='selected']").val());

// аНаАб?аАаЛб?аНб?аЙ б?аЕаКб?б? б?аЕаЛаЕаКб?аА
//jQuery("#v"+selID).val(jQuery(this).children("option[selected='selected']").eq(0).text());
jQuery("#v"+selID).val(jQuery("#"+jQuery(this).attr('id')+" > option[selected='selected']").text());


// аИаНаИб?аИаАаЛаИаЗаАб?аИб? б?аКб?аОаЛаЛаИаНаГаА
  var id_1='scroll_container_'+num;
  var id_2='scroller_'+num;
  scroller = new jsScroller(document.getElementById(id_2), 0, 143);
  scrollbar = new jsScrollbar(document.getElementById(id_1), scroller, false, false);
  
  	// б?аКб?б?аВаЕаМ аНаОаВб?аЙ б?аПаИб?аОаК option
 	jQuery("#optInvis_"+num).css("display","none").css("visibility","visible");
	 // б?аДаАаЛб?аЕаМ аОаБб?б?аНб?аЙ б?аЕаЛаЕаКб?  
	jQuery(this).remove();
}

// б?аЕаЛаЕаКб? аБаЕаЗ аПб?аОаКб?б?б?аКаИ  
else 
{
// б?аОб?аМаИб?б?аЕаМ аКаОб?б?б?аК
jQuery(this).before("<div class='selectArea "+className+"' style='z-index:"+(100-num)+"'>"+
						"<div class='left'></div>"+
						"<div class='center_a'></div>"+
						"<div class='optionsDivInvisible' id='"+selID+"_fake'></div>"+
						"<input type='text' readonly='readonly' name='v"+selName+"' id='v"+selID+"' />"+
						"<input type='hidden' name='"+selName+"' id='"+selID+"' />"+
					"</div>");

//  аЗаАаПаОаЛаНб?аЕаМ <option>

var sel_i; // б?аЕаКб?б?аИаЙ аНаОаМаЕб? option
var selArr=jQuery(this).children(); // аМаАб?б?аИаВ аВб?аЕб? option б?аЕаЛаЕаКб?аА
for(var i=0;i<kolOptions;i++)
{
	sel_i = selArr.eq(i);
	// name spana - аЗаНаАб?аЕаНаИаЕ option
	// б?аЕаКб?б? spana - б?аЕаКб? option
	jQuery("#"+selID+"_fake").append("<span name='"+sel_i.val()+"'>"+sel_i.text()+"</span>");
}

// аНаАб?аАаЛб?аНаОаЕ аЗаНаАб?аЕаНаИаЕ б?аЕаЛаЕаКб?аА 
jQuery("#"+selID).val(jQuery("#"+jQuery(this).attr('id')+" > option[selected='selected']").val());
//jQuery("#"+selID).val(jQuery(this).children("option.selected").eq(0).val());

// аНаАб?аАаЛб?аНб?аЙ б?аЕаКб?б? б?аЕаЛаЕаКб?аА
jQuery("#v"+selID).val(jQuery("#"+jQuery(this).attr('id')+" > option[selected='selected']").eq(0).text());
//jQuery("#v"+selID).val(jQuery(this).children("option.selected").eq(0).text());

// б?аКб?б?аВаАаЕаМ б?аПаИб?аОаК option
jQuery("#"+selID+"_fake").css("display","none").css("visibility","visible");

//  б?аДаАаЛб?аЕаМ аОаБб?б?аНб?аЙ б?аЕаЛаЕаКб?
jQuery(this).remove();
}

});

// аПаОаКаАаЗб?аВаАаЕаМ/б?аКб?б?аВаАаЕаМ б?аПаИб?аОаК option
jQuery('div.center_a').mousedown(
function()
{
	var optionsDivInvisible = this.parentNode.getElementsByTagName('div').item(2); // аКаОаНб?аЕаЙаНаЕб? аДаЛб? option
	if(jQuery(optionsDivInvisible).css("display")=="none") jQuery(optionsDivInvisible).slideDown(200); // аЕб?аЛаИ аКаОаНб?аЕаЙаНаЕб? б?аКб?б?б?, аПаОаКаАаЗб?аВаАаЕаМ 
	else jQuery(optionsDivInvisible).slideUp(200); // аИаНаАб?аЕ б?аКб?б?аВаАаЕаМ 
	
	// аЕб?аЛаИ б?б?аО select б?аО б?аКб?аОаОаМ, аОаБаНаОаВаЛб?аЕаМ б?аКб?аОаЛаЛ   
    var inp=this.parentNode.getElementsByTagName('input').item(0);
		if(!jQuery(inp).attr("readonly")) 
		{
		jQuery(inp).focus(); jQuery(inp).select(); // б?б?б?аАаНаАаВаЛаИаВаАаЕаМ б?аОаКб?б? аНаА б?аЕаЛаЕаКб?, б?б?аОаБб? аМаОаЖаНаО аБб?аЛаО аОб?аБаИб?аАб?б? аПаО аПаЕб?аВб?аМ аБб?аКаВаАаМ
		// аОаБаНаОаВаЛб?аЕаМ scroll
		var id_1=this.parentNode.getElementsByTagName('div').item(3).id;
		var id_2=this.parentNode.getElementsByTagName('div').item(8).id;
		scroller  = new jsScroller(document.getElementById(id_2), 0, 143);
		scrollbar = new jsScrollbar(document.getElementById(id_1), scroller, false, false);
		}
});

// аДб?аБаЛб?аЖ аПаОаКаАаЗаА/б?аКб?б?б?аИб? option аПб?аИ аКаЛаИаКаЕ аНаА input selectArea (аДаЛб? combobox)
jQuery('div.selectArea > input').mousedown(
function()
{

  if(jQuery(this.parentNode.getElementsByTagName('div').item(2)).css("display")=="none")
  {

  	jQuery(this.parentNode.getElementsByTagName('div').item(2)).slideDown(200);
	if(this.parentNode.getElementsByTagName('div').item(3))
	{
	var id_1=this.parentNode.getElementsByTagName('div').item(3).id;
	var id_2=this.parentNode.getElementsByTagName('div').item(8).id;
	scroller  = new jsScroller(document.getElementById(id_2), 0, 143);
	scrollbar = new jsScrollbar(document.getElementById(id_1), scroller, false, false);
	}
  }
});

// аВб?аБаОб? аПаОаЗаИб?аИаИ аВ б?аПаИб?аКаЕ option
optionClickHover();


//  б?аКб?б?аВаАаЕаМ б?аПаИб?аОаК option аКаОаГаДаА б?аБаИб?аАаЕаМ аКб?б?б?аОб? б? б?аЕаЛаЕаКб?аА
jQuery('div.selectArea').mouseout(
function(e)
{
var x = 0, y = 0; // аОаБб?б?аВаЛб?аЕаМ аИ аОаБаНб?аЛб?аЕаМ аКаОаОб?аДаИаНаАб?б? аКб?б?б?аОб?аА  
    if (!e) e = window.event;
    if (e.pageX || e.pageY){
        x = e.pageX;
        y = e.pageY;
    } else if (e.clientX || e.clientY){
        x = e.clientX + (document.documentElement.scrollLeft || document.body.scrollLeft) - document.documentElement.clientLeft;
        y = e.clientY + (document.documentElement.scrollTop || document.body.scrollTop) - document.documentElement.clientTop;
    }
  var obj = this;
  var posx=findPosX(obj);
  var posy=findPosY(obj);
  var length_obj = obj.offsetWidth-2;
  var left=this.getElementsByTagName('div')[0].offsetWidth;
  var h = this.getElementsByTagName('div')[1].offsetHeight;
  var minus;
  if(jQuery.browser.msie) {minus=0;min2=1;left-=1;}
  else {minus=2;min2=1;left-=3;}
  var h1 = this.getElementsByTagName('div')[2].offsetHeight-minus;
  // аЕб?аЛаИ аКб?б?б?аОб? аЛаЕаВаЕаЕ, аИаЛаИ аВб?б?аЕ, аИаЛаИ аПб?аАаВаЕаЕ, аИаЛаИ аВаНаЕ б?аПаИб?аКаА аОаПб?аИаОаНаОаВ - б?аКб?б?аВаАаЕаМ б?аПаИб?аОаК
  if((x<=posx+left || x>posx+length_obj+min2 || y<posy+2 || y>posy+h+h1) && jQuery(this).children('div.optionsDivInvisible').css("display")=="block")
  {
  jQuery(this).children('div.optionsDivInvisible').slideUp(200);
  }
});

//  аОб?аБаОб? option аО аПаЕб?аВб?аМ аБб?аКаВаАаМ
jQuery('div.selectArea > input').keyup(
function()
{
var id_1=this.parentNode.getElementsByTagName('div').item(3).id;
//id blocka scroll
var id_2=this.parentNode.getElementsByTagName('div').item(8).id;
//cicl proverka na sootvetstvie vvedenim simvolam
var col=jQuery("#"+id_2+" > div > span").length; // аК-аВаО аВб?аЕб? option
var span=jQuery("#"+id_2+" > div > span"); // аМаАб?б?аИаВ аВб?аЕб? option
var val_input=jQuery(this).val().toUpperCase(); // аПаЕб?аЕаВаОаДаИаМ аВб?аЕ б?аИаМаВаОаЛб? аВ аОаДаИаН б?аЕаГаИб?б?б?      
  for(i=0;i<col;i++)
  {
    var val_list=span.eq(i).text().toUpperCase();

    var pos=val_list.indexOf(val_input);
	if(pos!=0) span.eq(i).css("display","none"); // аЕб?аЛаИ аНб?аЖаНаОаЙ аПаОб?аЛаЕаДаОаВаАб?аЕаЛб?аНаОб?б?аИ аВ б?аЛаОаВаЕ аНаЕб?, б?аКб?б?аВаАаЕаМ option
    else span.eq(i).css("display","block");
  }
  //  аОаБаНаОаВаЛб?аЕаМ б?аКб?аОаЛаЛ
  scroller  = new jsScroller(document.getElementById(id_2), 0, 143);
  scrollbar = new jsScrollbar(document.getElementById(id_1), scroller, false, false);

});
function findPosY(obj) {
  var posTop = 0;
  while (obj.offsetParent) {posTop += obj.offsetTop; obj = obj.offsetParent;}
  return posTop;
}
function findPosX(obj) {
  var posLeft = 0;
  while (obj.offsetParent) {posLeft += obj.offsetLeft; obj = obj.offsetParent;}
  return posLeft;
}

}


