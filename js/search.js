﻿$(document).ready(function(){
    $('.add').live('click', function(){
	  var id_obj = $(this).attr('id');
      var elem = $(this);      
      var main = $(this).closest('.object');
	  $.ajax({
              url: "/ajax/notepad_add",
              data: {id: id_obj},
              dataType: 'text',
              type: 'post',
              success: function(){
                                  main.animate({ backgroundColor: '#99ff99' }, 'slow');
                                  elem.remove();
                                  }
              });
	  return false;
	});
});