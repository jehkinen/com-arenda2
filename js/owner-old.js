﻿jQuery(function($){

	$('a.change').bind('click', function(e){
		var pane = $(e.target).parents('div.infoBlock');
		var id_pane = pane.attr('id');
		var id_obj = id_pane.substring(3);
		e.preventDefault();
		show_modal('#owner-change', 'ocs', '<span>Сменить статус</span>', '', '30%', function(){
			var RadioChecked_obj = $('input[type=radio][name=stat]:checked');
			var new_stat = RadioChecked_obj.attr('alt');
			var id_stat = RadioChecked_obj.val();
			var bColor = 'clrRed';
			if (id_stat == 6)
				bColor = 'clrGreen';
			if (typeof(new_stat) != 'undefined'){
				$.ajax({
					url: "/ajax/notepad_owner_save",
					data: {id: id_obj, stat: id_stat},
					type: 'post',
					success: function(data){
						var div_status = $('[id='+id_pane+']').children('div.leftCol').children('div.statusBlock');
						var panel_text = div_status.children('b');
						var panel_labl = div_status.children('span');
						pane.removeClass().addClass('infoBlock ' + bColor); 
						panel_text.removeClass().addClass(bColor); 
						panel_labl.removeClass().addClass(bColor); 
						panel_text.html(new_stat);
					}
				});
			}
		});
	});

	$('a.delete').bind('click', function(e){
		var Parent_obj = $(e.target).parents('div.infoBlock');
		var id_pane = Parent_obj.attr('id');
		var id_obj = id_pane.substring(3);
		e.preventDefault();
		show_modal('#owner-delete', 'ods', '<span>Удаление</span>', 'Вы уверены?', '30%', function(){
			$.ajax({
				url: "/ajax/notepad_owner_delete",
				data: {id: id_obj},
				type: 'post',
				success: function(){
					Parent_obj.animate({opacity: 'hide'}, 400);
				}
			});
		});
	});

});

function show_modal(box, container, header, message, top, callback){
	$(box).modal({
		closeHTML: '<a href="#" title="Закрыть" class="x-close"><img src="/images/modal/mw-close.jpg" /></a>',
		position: [top,],
		opacity: 30,
		overlayId: 'overlay',
		containerId: container,
		closeClass: 'close',
		onShow: function (dialog){
			var modal = this;

			$('.header', dialog.data[0]).append(header);
			$('.message', dialog.data[0]).append(message);

			$('.yes', dialog.data[0]).click(function () {
				if ($.isFunction(callback)) {
					callback.apply();
				}
				modal.close();
			});
		}
	});
}