﻿(function($){
	$.fn.showModal = function(w,h,content){
		var is_modal_show = false;		
		var thismod = jQuery(this);
		var msiewid=0, msiehe=0, msieph=0;
		var id_box = thismod.attr('id');

		$(window).scroll(function(){
			if(is_modal_show && $.browser.msie){
				thismod.css({'margin-left': -Math.round(thismod.width()/2)+document.body.scrollLeft+'px', 'margin-top': -Math.round(thismod.height()/2)+document.body.scrollTop+'px'});
			}
		});

		thismod.addClass('jamodal');
		thismod.css({'left':'50%', 'top':'50%', 'z-index':9999, 'padding':0, 'outline':0});
		if($.browser.msie) {
			thismod.css('position','absolute'); 
			msiehe=document.body.scrollTop; msiewid=document.body.scrollLeft; 
			msieph=document.compatMode != 'CSS1Compat' ? document.body.scrollHeight : document.documentElement.scrollHeight; 
			/* thismod.css({'left':'50%', 'top':'50%', 'z-index':9999, 'padding':0, 'outline':0}); */
		} else { 
			thismod.css('position','fixed'); msieph=document.documentElement.clientHeight; 
		}
		if(typeof w!='undefined') thismod.width(w);
		if(typeof h!='undefined') thismod.height(h);
		thismod.css('margin-left',-Math.round(thismod.width()/2)+msiewid+'px');
		thismod.css('margin-top',-Math.round(thismod.height()/2)+msiehe+'px');
		if(typeof content!='undefined'){
			$('div#'+id_box+'-content').remove();
			thismod.prepend('<div id="'+id_box+'-content">'+content+'<br /><br />clientHeight=' + document.body.clientHeight + '<br />scrollHeight=' + document.window.Height + '</div>');
		}

		thismod.css('display','block');
		is_modal_show = true;

		thismod.after('<div class="jamodal-block"></div>');
		jQuery('.jamodal-block').css({'position':'fixed', 'z-index':9998,'margin':0,'padding':0, 'left':0, 'top':0,'width':'100%','height':msieph+'px','background-color':'#CCCCCC', 'opacity':'0.5'});
    };

	$.fn.hideModal = function(){
		jQuery('.jamodal-block').detach();
		jQuery(this).animate({opacity: 'hide'}, 400);
    };

})(jQuery);