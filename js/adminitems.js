jQuery(function($){

	$('a.change').bind('click', function(e){
		var labl = $(e.target);
		var obj_id = labl.attr('id');
		e.preventDefault();
		show_modal('#mwindow', 'ais', '<span>Изменить статус</span>', '', '20%', function(){
			var RadioChecked_obj = $('input[type=radio][name=stat]:checked');
			var new_stat = RadioChecked_obj.attr('alt');
			var id_stat = RadioChecked_obj.val();
			labl.html(new_stat);
			if (typeof(new_stat) != 'undefined'){
				$.ajax({
					url: "/ajaxobjects/change_state",
					data: {id: obj_id, stat: id_stat},
					type: 'post',
					success: function(data){}
				});
			}
		});
	});
	$('a.chspec').bind('click', function(e){
		var obj_id = $(e.target).attr('id');
		var labl = $('span#sp'+obj_id.substr(2));
		e.preventDefault();
		show_modal('#admin-allow', 'ocs', '<span>Спецпредложение</span>', '', '20%', function(){
			var RadioChecked_obj = $('input[type=radio][name=spec]:checked');
			var new_stat = RadioChecked_obj.attr('alt');
			var id_stat = RadioChecked_obj.val();
			labl.html(new_stat);
			if (typeof(new_stat) != 'undefined'){
				$.ajax({
					url: "/ajaxobjects/change_spec",
					data: {id: obj_id, spec: id_stat},
					type: 'post',
					success: function(data){}
				});
			}
		});
	});
});

function show_modal(box, container, header, message, top, callback){
	$(box).modal({
		closeHTML: '<a href="#" title="Закрыть" class="x-close">x</a>',
		position: [top,],
		opacity: 30,
		overlayId: 'overlay',
		containerId: container,
		closeClass: 'close',
		onShow: function (dialog){
			var modal = this;

			$('.header', dialog.data[0]).append(header);
			$('.message', dialog.data[0]).append(message);

			$('.yes', dialog.data[0]).click(function () {
				if ($.isFunction(callback)) {
					callback.apply();
				}
				modal.close();
			});
		}
	});
}