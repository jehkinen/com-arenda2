﻿$(document).ready(function(){
	$('input#submit').hide();

	var flds = $('input.input02');
	$(flds).css('color', '#AAAAAA');
	$(flds).attr('disabled', 'disabled');
	for (i=0;i<flds.length;i++) {
		var el = flds[i];
		if (el.value == '') {
			switch(el.id){
				case 'Registration_username'		:el.value = 'Логин...';break;
				case 'Registration_name'			:el.value = 'Имя...';break;
				case 'Registration_so_name'		:el.value = 'Фамилия...';break;
				case 'Registration_otchestvo'		:el.value = 'Отчество...';break;
				case 'Registration_phone'		:el.value = '+7 ___ _______';break;
				case 'Registration_e_mail'		:el.value = 'E-mail...';break;
			}
		}
	}
	
	$('div#container input').bind('focus', function() {
		$(this).css('color', '#000000');
		var parentDiv = $(this).parent().parent().children('div');
		for(i=0;i<parentDiv.length;i++) {
			var el = parentDiv[i];
			$(el).removeClass().addClass('input02_rf'+i);
		}
		if (this.value != '') {
			switch(this.id){
				case 'Registration_username'		:if (this.value=='Логин...') this.value = '';break;
				case 'Registration_name'			:if (this.value=='Имя...') this.value = '';break;
				case 'Registration_so_name'		:if (this.value=='Фамилия...') this.value = '';break;
				case 'Registration_otchestvo'		:if (this.value=='Отчество...') this.value = '';break;
				case 'Registration_phone'		:if (this.value=='+7 ___ _______') this.value = '';break;
				case 'Registration_e_mail'		:if (this.value=='E-mail...') this.value = '';break;
			}
		}
		return false;
	});	
	$('div#container input').bind('blur', function() {		
		var parentDiv = $(this).parent().parent().children('div');
		var s = '';
		if (parentDiv.parent().next().hasClass('input02_ner')) { s = 'input02_re'; } 
			else { s = 'input02_rb';	}
		for(i=0;i<parentDiv.length;i++) {
			var el = parentDiv[i];
			$(el).removeClass().addClass(s+i);
		}
		if (this.value == '') {
			$(this).css('color', '#AAAAAA');
			switch(this.id){
				case 'Registration_username'		:this.value='Логин...';break;
				case 'Registration_name'			:this.value='Имя...';break;
				case 'Registration_so_name'		:this.value='Фамилия...';break;
				case 'Registration_otchestvo'		:this.value='Отчество...';break;
				case 'Registration_phone'		:this.value='+7 ___ _______';break;
				case 'Registration_e_mail'		:this.value='E-mail...';break;
			}
		}
		return false;
	});

	function KeyPressEvent(e) {
		var t = this;
		var label = e.data.lab;
		if (this.value != this.lastValue){
			if (this.timer) clearTimeout(this.timer);
			this.timer = setTimeout(function(){
				label.html('проверка...');
				$.ajax({
					url: "/ajax/"+e.data.act,
					data: {val: t.value, req:e.data.req},
					dataType: 'json',
					type: 'post',
					success: function (data){
							label.html(data[0]);
							if (data[1]=='err') { label.removeClass().addClass('input02_ner');} 
								else { label.removeClass().addClass('input02_nte');}
							$(t).trigger('blur');
					}
				});
			}, 1000);
			this.lastValue = this.value;
		}
	}
	var llogin = $('div#llogin');
	$('#Registration_username').bind('keyup', {lab:llogin, act:'islogin', req:1}, KeyPressEvent);
	var lname = $('div#lname');
	$('#Registration_name').bind('keyup', {lab:lname, act:'regrusfields', req:1}, KeyPressEvent);
	var lsoname = $('div#lsoname');
	$('#Registration_so_name').bind('keyup', {lab:lsoname, act:'regrusfields', req:1}, KeyPressEvent);
	var lotch = $('div#lotch');
	$('#Registration_otchestvo').bind('keyup', {lab:lotch, act:'regrusfields', req:0}, KeyPressEvent);
	var lmail = $('div#lmail');
	$('#Registration_e_mail').bind('keyup', {lab:lmail, act:'isemail', req:1}, KeyPressEvent);
	var lpass = $('div#lpass');
	$('#Registration_password').bind('keyup', {lab:lpass, act:'chekpass', req:1}, KeyPressEvent);
	var lphone = $('div#lphone');
	$('#Registration_phone').keyup(function(){
		var t = this;
		if (this.value != this.lastValue){
			if (this.timer) clearTimeout(this.timer);
			this.timer = setTimeout(function(){
				$.ajax({
					url: "/ajax/checkphone",
					data: {val: t.value},
					dataType: 'json',
					type: 'post',
					success: function (data){
							t.value = data[0];
							if (data[1]=='err') { lphone.removeClass().addClass('input02_ner');} 
								else { lphone.removeClass().addClass('input02_nte');}
					}
				});
			}, 500);
			this.lastValue = this.value;
		}
	});
	var lconf = $('div#lconf');
	$('#Registration_confirm').keyup(function(){
		var t = this;
		if (this.value != this.lastValue){
			if (this.timer) clearTimeout(this.timer);
			this.timer = setTimeout(function(){
				lconf.html('проверка...');
				var password = $('#Registration_password').attr('value');
				$.ajax({
					url: "/ajax/confirmpass",
					data: {confirm: t.value, pass: password},
					dataType: 'json',
					type: 'post',
					success: function (data){
							lconf.html(data[0]);
							if (data[1]=='err') { lconf.removeClass().addClass('input02_ner');} 
								else { lconf.removeClass().addClass('input02_nte');}
					}
				});
			}, 1000);
			this.lastValue = this.value;
		}
	});

	$('label.switch01_lbl').click(function(e) {
		e.preventDefault();
		// Обработаем Профессионала
		var liID = $(this).attr("id");
		if (liID!='la2') {
			if ($('input#Registration_username').attr('disabled')) {
				// Включим все инпуты
				$(flds).removeAttr('disabled');
				$('#Registration_rules').change(function(){
					if($(this).is(":checked")){
						$('input#submit').show();
						$('label.check01_lbl > div').removeClass().addClass('fcheck01_ce');
					}else{
						$('input#submit').hide();
						$('label.check01_lbl > div').removeClass().addClass('fcheck01_ue');
					}
				});
			}
			$('label.switch01_lbl').children('div').removeClass('fswitch01_ce').addClass('fswitch01_ue');
			$(this).children('div').removeClass('fswitch01_ue').addClass('fswitch01_ce');
			if (liID=='la0')
				$("input#Registration_role").val('tenant');
			else $("input#Registration_role").val('owner');
		}
	});

});