﻿jQuery(function($){

	$('a.allow').bind('click', function(e){
		var Parent_obj = $(e.target).parents('div.pane');
		var id_obj = $(e.target).attr('id');
		e.preventDefault();
		show_modal('#admin-allow', 'aas', '<span>Разрешить объект</span>', 'Вы уверены?', '30%', function(){
			$.ajax({
				url: "/ajaxadmins/admin_allow",
				data: {id: id_obj},
				type: 'post',
				success: function(){
					Parent_obj.animate({opacity: 'hide'}, 400);
				}
			});
		});
	});
	
	$('a.disallow').bind('click', function(e){
		var Parent_obj = $(e.target).parents('div.pane');
		var id_obj = $(e.target).attr('id');
		e.preventDefault();
		show_modal('#admin-disallow', 'ads', '<span>Отказать (с причиной)</span>', '', '25%', function(){
			var message = $('#admin-disallow textarea[name=disallow]').val();
			$.ajax({
				url: "/ajaxadmins/admin_disallow",
				data: {id: id_obj, msg: message},
				type: 'post',
				success: function(){
					Parent_obj.animate({opacity: 'hide'}, 400);
				}
			});
		});
	});

});

function show_modal(box, container, header, message, top, callback){
	$(box).modal({
		closeHTML: '<a href="#" title="Закрыть" class="x-close">x</a>',
		position: [top,],
		opacity: 80,
		overlayId: 'overlay',
		containerId: container,
		closeClass: 'close',
		onShow: function (dialog){
			var modal = this;

			$('.header', dialog.data[0]).append(header);
			$('.message', dialog.data[0]).append(message);

			$('.yes', dialog.data[0]).click(function () {
				if ($.isFunction(callback)) {
					callback.apply();
				}
				modal.close();
			});
		}
	});
}