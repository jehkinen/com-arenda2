﻿jQuery(function($){

	$('span.mw-button').bind('click', function(e){
		var id_obj = $(e.target).attr('id');
		var span = $('span#'+id_obj);
		e.preventDefault();
		show_modal('#mwindow', 'smwp', '<span>Текст письма</span>', '', '25%', function(){
			var message = $('#mwindow textarea[name=disallow]').val();
			$.ajax({
				url: "/ajax/person_sendmail",
				data: {id: id_obj, msg: message},
				type: 'post',
				success: function(){
					span.html('Письмо отправлено');
				}
			});
		});
	});

});

function show_modal(box, container, header, message, top, callback){
	$(box).modal({
		closeHTML: '<a href="#" title="Закрыть" class="x-close"><img src="/images/modal/mw-close.jpg" /></a>',
		position: [top,],
		opacity: 30,
		overlayId: 'overlay',
		containerId: container,
		closeClass: 'close',
		onShow: function (dialog){
			var modal = this;

			$('.header', dialog.data[0]).append(header);
			$('.message', dialog.data[0]).append(message);

			$('.yes', dialog.data[0]).click(function () {
				if ($.isFunction(callback)) {
					callback.apply();
				}
				modal.close();
			});
		}
	});
}