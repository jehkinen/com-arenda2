﻿jQuery(function($){

	$("div.object_table div.post:odd").addClass("even");

	$('a.change').bind('click', function(e){
		var id_obj = $(e.target).attr('id');
		var id = id_obj.slice(3);
		$('div#row'+id).hide();
		e.preventDefault();
		show_modal('#owner-change', 'ocs', '<span>Сменить статус</span>', '', '30%', function(){
			var RadioChecked_obj = $('input[type=radio][name=stat]:checked');
			var new_stat = RadioChecked_obj.attr('alt');
			var id_stat = RadioChecked_obj.val();
			var statClass = 'object_status inactive';
			if (id_stat == 6)
				statClass = 'object_status active';
			if (typeof(new_stat) != 'undefined'){
				$.ajax({
					url: "/ajax/notepad_owner_save",
					data: {id: id, stat: id_stat},
					type: 'post',
					success: function(data){
						var div_status = $('div#dos'+id);
						div_status.removeClass().addClass(statClass);
						div_status.html(new_stat);
					}
				});
			}
		});
	});

	$('a.delete').bind('click', function(e){
		var id_obj = $(e.target).attr('id');
		var id = id_obj.slice(3);
		var id_pane = $('div#dop'+id);
		$('div#row'+id).hide();
		e.preventDefault();
		show_modal('#owner-delete', 'ods', '<span>Удаление</span>', 'Вы уверены?', '30%', function(){
			$.ajax({
				url: "/ajax/notepad_owner_delete",
				data: {id: id},
				type: 'post',
				success: function(){
					id_pane.animate({opacity: 'hide'}, 400,
						function(){
							id_pane.remove();
							$("div.object_table div.post").removeClass("even");
							$("div.object_table div.post:odd").addClass("even");
						}
					);
				}
			});
		});
	});

});

function show_modal(box, container, header, message, top, callback){
	$(box).modal({
		closeHTML: '<a href="#" title="Закрыть" class="x-close"><img src="/images/modal/mw-close.jpg" /></a>',
		position: [top,],
		opacity: 30,
		overlayId: 'overlay',
		containerId: container,
		closeClass: 'close',
		onShow: function (dialog){
			var modal = this;

			$('.header', dialog.data[0]).append(header);
			$('.message', dialog.data[0]).append(message);

			$('.yes', dialog.data[0]).click(function () {
				if ($.isFunction(callback)) {
					callback.apply();
				}
				modal.close();
			});
		}
	});
}