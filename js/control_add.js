﻿$(document).ready(function(){
  	var nameSpan = $('#nameSpan');

  	$('#Statics_name').keyup(function(){
      var t = this;
      if (this.value != this.lastValue){
      	if (this.timer) clearTimeout(this.timer);
        nameSpan.html('проверка...');
        this.timer = setTimeout(function(){
        $.ajax({
              url: "/ajax/isname",
              data: {name: t.value},
              dataType: 'json',
              type: 'post',
              success: function (data){
                                       nameSpan.text(data[0]);
                                       $('#Statics_name').removeClass().addClass(data[1]);
                                       }
              });
        }, 500);
        this.lastValue = this.value;
      }
    });

});