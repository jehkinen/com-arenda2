// JavaScript Document
function mycarousel_initCallback(carousel)
{
	// Disable autoscrolling if the user clicks the prev or next button.
	carousel.buttonNext.bind('click', function() {
		carousel.startAuto(0);
	});

	carousel.buttonPrev.bind('click', function() {
		carousel.startAuto(0);
	});

	// Pause autoscrolling if the user moves with the cursor over the clip.
	carousel.clip.hover(function() {
		carousel.stopAuto();
	}, function() {
		carousel.startAuto();
	});
};

$(document).ready(function() {
	var slider_padding = $(".slider .header_link.active").parent("dt").next("dd").css("height");
	$(".slider").css("padding-bottom", slider_padding);
	
	$('#mycarousel').jcarousel({
		auto: 4,
		wrap: 'circular',
		animation: 5000,
		initCallback: mycarousel_initCallback
	});

	$(".slider .header_link").click(function(){
		$(".slider .header_link.active").parent("dt").next("dd").toggleClass("display");
		$(".slider .header_link.active").toggleClass("active");
		$(this).parent("dt").next("dd").toggleClass("display");
		var dd_height = $(this).parent("dt").next("dd").css("height");
		$(".slider").css("padding-bottom", dd_height);
		$(this).toggleClass("active");
		return false;
	});

});