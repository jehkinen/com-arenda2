var js_support=(document.createElement && document.getElementsByTagName);
var aSwitch=new Array();																															/*Переключатели, выбранные на странице.*/
function fakeChecks(){
	var a=document.getElementsByTagName('input');
	for(var c=0;c<a.length;c++){
		var q=a[c].parentNode;
		if(q.className.indexOf('check01_')==0&&a[c].type=='checkbox'){
			a[c].className='dn';
			q.addEventListener?q.addEventListener('click',fakeChecksClick):q.attachEvent('onclick',fakeChecksClick);
			var s1='<div class=\"fcheck01_';
			var s2='\"><\/div>'+q.innerHTML;
			if(!a[c].disabled){
				if(a[c].checked){
					q.innerHTML=s1+'ce'+s2;
				}else{
					q.innerHTML=s1+'ue'+s2;
				}
			}else{
				if(a[c].checked){
					q.innerHTML=s1+'cd'+s2;
				}else{
					q.innerHTML=s1+'ud'+s2;
				}
			}
		}
	}
}function fakeChecksClick(e){
	var q=e.target?e.target:e.srcElement;
	/*alert(q.tagName);*/
	if(q.tagName=='DIV'){
		var q1=q.parentNode.childNodes[1];
	}else{
		var q1=q.childNodes[1];
		q=q.childNodes[0];
	}
	if(!q1.disabled){
		if(q1.checked){
			q.className='fcheck01_ue';
			q1.checked=false;
		}else{
			q.className='fcheck01_ce';
			q1.checked=true;
		}
	}
	e.preventDefault?e.preventDefault():e.returnValue=false;
}function fakeSwitches(){
		var a=document.getElementsByTagName('input');
		var an=new Array();
		var c2=new Number(0);
		var c3=new Number(0);
		for(var c=0;c<a.length;c++){	
			var b=new Number(1);
			var q=a[c].parentNode;																									
			if(q.className.indexOf('switch01_')==0&&a[c].type=='radio'){
				a[c].className='dn';
				q.addEventListener?q.addEventListener('click',fakeSwitchesClick):q.attachEvent('onclick',fakeSwitchesClick);
				var s1='<div class=\"fswitch01_';
				var s2='\"><\/div>'+q.innerHTML;
				if(a[c].checked){
					if(!a[c].disabled){
						q.innerHTML=s1+'ce'+s2;
					}else{
						q.innerHTML=s1+'cd'+s2;
					}
					an[c3]=a[c];
					c3++;
				}else{
					if(!a[c].disabled){
						q.innerHTML=s1+'ue'+s2;
					}else{
						q.innerHTML=s1+'ud'+s2;
					}
				}
				if(c2>0){
					for(var c4=0;c4<aSwitch.length;c4++){
						if(aSwitch[c4]==a[c].name){
							b=0;
							c4=aSwitch.length;
						}
					}
					if(b){
						aSwitch[c4]=a[c].name;
					}
				}else{
					aSwitch[0]=a[c].name;
				}
				c2++;
			}
		}
		/*alert('an[0]='+an[0]+'\nan[1]='+an[1]+'\nan[2]='+an[2]+'\nan[3]='+an[3]+'\nan[4]='+an[4]+'\nan[5]='+an[5]);*/
		for(c=0;c<aSwitch.length;c++){
			for(c2=0;c2<an.length;c2++){
				if(an[c2].name==aSwitch[c]){
					aSwitch[c]=an[c2];
				}
			}
		}
	/*alert('aSwitch[0]='+aSwitch[0]+'\naSwitch[1]='+aSwitch[1]+'\naSwitch[2]='+aSwitch[2]+'\naSwitch[3]='+aSwitch[3]+'\naSwitch[4]='+aSwitch[4]+'\naSwitch[5]='+aSwitch[5]);*/
	return aSwitch;
}function fakeSwitchesClick(e){
	var q=e.target?e.target:e.srcElement;
	if(q.tagName!='INPUT'){
		if(q.tagName=='DIV'){
			var q1=q.parentNode.childNodes[1];
		}else{
			var q1=q.childNodes[1];
			q=q.childNodes[0];
		}
		for(var c=0;c<aSwitch.length;c++){
			if(!q1.disabled){
				q.className='fswitch01_ce';
				q1.checked=true;
				if(typeof aSwitch[c]=='object'){
					if(q1.name==aSwitch[c].name&&q1!=aSwitch[c]){
						aSwitch[c].parentNode.childNodes[0].className='fswitch01_ue';
						aSwitch[c]=q1;
					}
				}else{
					if(q1.name==aSwitch[c]){
						aSwitch[c]=q1;
					}
				}
			}
		}
	}
	/*alert('aSwitch[0]='+aSwitch[0]+'\naSwitch[1]='+aSwitch[1]+'\naSwitch[2]='+aSwitch[2]+'\naSwitch[3]='+aSwitch[3]+'\naSwitch[4]='+aSwitch[4]+'\naSwitch[5]='+aSwitch[5]);*/
	return aSwitch;
}function fakeInputs(){
	var a=document.getElementsByTagName('input');
	for(var c=0;c<a.length;c++){
		var q=a[c];
		if(q.parentNode.className.indexOf('input02_')==0&&(q.type=='password'||q.type=='text')){
			if(q.type=='password')q=changeInputType(q,'text');
			var s=q.style;
			q.addEventListener?q.addEventListener('blur',fakeInputsBlur):q.attachEvent('onblur',fakeInputsBlur);
			q.addEventListener?q.addEventListener('focus',fakeInputsFocus):q.attachEvent('onfocus',fakeInputsFocus);
			switch(q.id){
				case 'Registration_username'		:q.value='Логин...'				;s.color='#aaa';break;
				case 'fake_Registration_password'	:q.value='Пароль...'			;s.color='#aaa';break;
				case 'fake_Registration_confirm'	:q.value='Повторите пароль...'	;s.color='#aaa';break;
				case 'Registration_name'			:q.value='Имя...'				;s.color='#aaa';break;
				case 'Registration_so_name'			:q.value='Фамилия...'			;s.color='#aaa';break;
				case 'Registration_otchestvo'		:q.value='Отчество...'			;s.color='#aaa';break;
				case 'Registration_phone'			:q.value='+7 ___ _______'		;s.color='#aaa';/*q.addEventListener?q.addEventListener('keydown',fakeInputsKeydown):q.attachEvent('onkeydown',fakeInputsKeydown);*/break;
				case 'Registration_email'			:q.value='E-mail...'			;s.color='#aaa';break;
			}
		}
	}
}function fakeInputsBlur(e){
	var q=e.target?e.target:e.srcElement;
	var a=q.parentNode.parentNode.childNodes;
	var v=q.value;
	switch(q.id){
		case 'Registration_username'		:if(v=='')						q.value='Логин...'				;break;
		case 'Registration_password'		:if(v=='')						{q=changeInputType(q,'text');q.addEventListener?q.addEventListener('blur',fakeInputsBlur):q.attachEvent('onblur',fakeInputsBlur);q.addEventListener?q.addEventListener('focus',fakeInputsFocus):q.attachEvent('onfocus',fakeInputsFocus);q.value='Пароль...'			;}break;
		case 'Registration_confirm'			:if(v=='')						{q=changeInputType(q,'text');q.addEventListener?q.addEventListener('blur',fakeInputsBlur):q.attachEvent('onblur',fakeInputsBlur);q.addEventListener?q.addEventListener('focus',fakeInputsFocus):q.attachEvent('onfocus',fakeInputsFocus);q.value='Повторите пароль...'	;}break;
		case 'Registration_name'			:if(v=='')						q.value='Имя...'				;break;
		case 'Registration_so_name'			:if(v=='')						q.value='Фамилия...'			;break;
		case 'Registration_otchestvo'		:if(v=='')						q.value='Отчество...'			;break;
		case 'Registration_phone'			:if(v=='')						q.value='+7 ___ _______'		;break;
		case 'Registration_email'			:if(v=='')						q.value='E-mail...'				;break;
	}
	if(v!=q.value){
		a[1].childNodes[0].style.color='#aaa';
	}
	a[0].className='input02_rb0';
	a[1].className='input02_rb1';
	a[2].className='input02_rb2';
}/*function fakeInputsChange(e){
	var q=e.target?e.target:e.srcElement;
	var t=e.keyCode||.which;
	t=String.fromCharCode(t);
	var r=/[0-9]/;
	if(!r.test(t)){
		e.preventDefault?e.preventDefault():e.returnValue=false;
	}
}*/function fakeInputsFocus(e){
	var q=e.target?e.target:e.srcElement;
	var a=q.parentNode.parentNode.childNodes;
	var v=q.value;
	switch(q.id){
		case 'Registration_username'		:if(v=='Логин...')				q.value='';break;
		case 'fake_Registration_password'	:if(v=='Пароль...')				{q=changeInputType(a[1].childNodes[0],'password');q.addEventListener?q.addEventListener('blur',fakeInputsBlur):q.attachEvent('onblur',fakeInputsBlur);q.addEventListener?q.addEventListener('focus',fakeInputsFocus):q.attachEvent('onfocus',fakeInputsFocus);}break;
		case 'fake_Registration_confirm'	:if(v=='Повторите пароль...')	{q=changeInputType(a[1].childNodes[0],'password');q.addEventListener?q.addEventListener('blur',fakeInputsBlur):q.attachEvent('onblur',fakeInputsBlur);q.addEventListener?q.addEventListener('focus',fakeInputsFocus):q.attachEvent('onfocus',fakeInputsFocus);}break;
		case 'Registration_name'			:if(v=='Имя...')				q.value='';break;
		case 'Registration_so_name'			:if(v=='Фамилия...')			q.value='';break;
		case 'Registration_otchestvo'		:if(v=='Отчество...')			q.value='';break;
		case 'Registration_phone'			:if(v=='+7 ___ _______')		q.value='';break;
		case 'Registration_email'			:if(v=='E-mail...')				q.value='';break;
	}
	if(v!=q.value){
		a[1].childNodes[0].style.color='#333';
	}
	a[0].className='input02_rf0';
	a[1].className='input02_rf1';
	a[2].className='input02_rf2';
}function fakeUploads(){
	if (!js_support) return;
	var q1=document.createElement('div');
	var q2=document.createElement('input');
	var q3=document.createElement('img');
	q1.className='dib';
	q2.className='fupload01_fld w280';
	q3.className='fupload01_btn';
	q1.appendChild(q2);
	q1.appendChild(q3);
	q3.src='images/fupload01_btn.png';
	var a=document.getElementsByTagName('input');
	for(var i=0;i<a.length;i++) {
		if (a[i].type!='file') continue;
		if (a[i].parentNode.className.indexOf('fupload01')) continue;
		a[i].className='upload01_hdn';
		var q0=q1.cloneNode(true);
		a[i].parentNode.appendChild(q0);
		a[i].relatedElement=q0.getElementsByTagName('input')[0];
		a[i].onchange=function(){
			this.relatedElement.value=this.value;
		}
	}
}function fakeUploadsOut(e){
	var q=e.target?e.target:e.srcElement;
	q.parentNode.childNodes[2].childNodes[1].src='images/fupload01_btn.png';
}function fakeUploadsMove(e){
	var q=e.target?e.target:e.srcElement;
	q.parentNode.childNodes[2].childNodes[1].src='images/fupload01_btn_hover.png';
}function fakeUploadsDown(e){
	var q=e.target?e.target:e.srcElement;
	q.parentNode.childNodes[2].childNodes[1].src='images/fupload01_btn_active.png';
}function fakeUploadsUp(e){
	var q=e.target?e.target:e.srcElement;
	q.parentNode.childNodes[2].childNodes[1].src='images/fupload01_btn_hover.png';
}function changeInputType(q,s){
	if(q.type!=s){
		var qn=document.createElement('input');
		var a=q.attributes;
		/*var t='';
		var t2='';*/
		for(var c=0;c<a.length;c++){
			/*t+=a[c].name+'\t='+a[c].value+'</br>';*/
			if(a[c].name=='id'){
				if(q.id.indexOf('fake_')!=0){
					qn.id='fake_'+q.id;
				}else{
					qn.id=q.id.slice(5);
				}
			}else if(a[c].name=='type'){
				qn.setAttribute('type',s);
			}else if(a[c].name=='value'){
				if(q.type!='password'&&s!='password'){
					qn.value=q.value;
				}
			}else{
				qn.setAttribute(a[c].name,a[c].value);
			}
		}
		/*a=qn.attributes;
		for(var c=0;c<a.length;c++){
			t2+=a.name+'\t='+a[c].value+'</br>';
		}
		document.getElementById('').innerHTML=t+'\n'+t2;*/
		if(q==document.activeElement&&document.hasFocus!=false)var b=1;
		q.parentNode.replaceChild(qn,q);
		b?setTimeout(function(){qn.focus()},100):b;
		return qn;
	}else{
		return q;
	}
}function checkboxBg(e){
	var q=e.target?e.target:e.srcElement;
	var q=q.parentNode;
	if(q.className.indexOf('checkbox02_lbl')>0){
		for(var c=0;c<q.parentNode.childNodes.length;c++){																							//Поиск чекбоксов с активным бекграундом
			if(q.parentNode.childNodes[c].className.indexOf('Active')>0&&q.className!=q.parentNode.childNodes[c].className){						//Если какой-либо чекбокс имеет активный бекграунд и не является чекбоксом, инициировавшим функцию
				var qq=q.parentNode.childNodes[c];
				qq.className=qq.className.substring(0,qq.className.indexOf('Active'))+qq.className.substring(qq.className.indexOf('Active')+6);	//Сменить бекграунд этого чекбокса на пассивный
			}
		}
		if(q.className.indexOf('bShop ')==0&&q.childNodes[0].checked==true){																		//Переключение бекграунда чекбокса
			q.className='bShopActive checkbox02_lbl';
		}else if(q.className.indexOf('bShopA')==0&&q.childNodes[0].checked!=true){
			q.className='bShop checkbox02_lbl';
		}else if(q.className.indexOf('bEnterprise ')==0&&q.childNodes[0].checked==true){
			q.className='bEnterpriseActive checkbox02_lbl';
		}else if(q.className.indexOf('bEnterpriseA')==0&&q.childNodes[0].checked!=true){
			q.className='bEnterprise checkbox02_lbl';
		}else if(q.className.indexOf('bOffice ')==0&&q.childNodes[0].checked==true){
			q.className='bOfficeActive checkbox02_lbl';
		}else if(q.className.indexOf('bOfficeA')==0&&q.childNodes[0].checked!=true){
			q.className='bOffice checkbox02_lbl';
		}
	}
}