﻿jQuery(function($){

	$("div.object_table div.post:odd").addClass("even");

	$('a.delorder').bind('click', function(e){
		var id_obj = $(e.target).attr('id');
		var id = id_obj.slice(3);
		var id_pane = $('div#dop'+id);
		$('div#row'+id).hide();
		e.preventDefault();
		show_modal('#owner-delete', 'ods', '<span>Удаление</span>', 'Вы уверены?', '30%', function(){
			$.ajax({
				url: "/ajax/order_tenant_delete",
				data: {id: id},
				type: 'post',
				success: function(){
					id_pane.animate({opacity: 'hide'}, 400,
						function(){
							id_pane.remove();
							$("div.object_table div.post").removeClass("even");
							$("div.object_table div.post:odd").addClass("even");
						}
					);
				}
			});
		});
	});

	$('a.delete').bind('click', function(e){
		var id_obj = $(e.target).attr('id');
		var id = id_obj.slice(3);
		var id_pane = $('div#dop'+id);
		$('div#row'+id).hide();
		e.preventDefault();
		show_modal('#owner-delete', 'ods', '<span>Удаление</span>', 'Вы уверены?', '30%', function(){
			$.ajax({
				url: "/ajax/notepad_tenant_delete",
				data: {id: id},
				type: 'post',
				success: function(){
					id_pane.animate({opacity: 'hide'}, 400,
						function(){
							id_pane.remove();
							$("div.object_table div.post").removeClass("even");
							$("div.object_table div.post:odd").addClass("even");
						}
					);
				}
			});
		});
	});

});

function show_modal(box, container, header, message, top, callback){
	$(box).modal({
		closeHTML: '<a href="#" title="Закрыть" class="x-close"><img src="/images/modal/mw-close.jpg" /></a>',
		position: [top,],
		opacity: 30,
		overlayId: 'overlay',
		containerId: container,
		closeClass: 'close',
		onShow: function (dialog){
			var modal = this;

			$('.header', dialog.data[0]).append(header);
			$('.message', dialog.data[0]).append(message);

			$('.yes', dialog.data[0]).click(function () {
				if ($.isFunction(callback)) {
					callback.apply();
				}
				modal.close();
			});
		}
	});
}