function mycarousel_initCallback(carousel)
{
	// Disable autoscrolling if the user clicks the prev or next button.
	carousel.buttonNext.bind('click', function() {
		carousel.startAuto(0);
	});

	carousel.buttonPrev.bind('click', function() {
		carousel.startAuto(0);
	});

	// Pause autoscrolling if the user moves with the cursor over the clip.
	carousel.clip.hover(function() {
		carousel.stopAuto();
	}, function() {
		carousel.startAuto();
	});
};

$(document).ready(function() {
	$('#gallery_carousel').jcarousel({
		auto: 0,
		wrap: 'last',
		initCallback: mycarousel_initCallback
	});

	$("#gallery_carousel a").click(function(){
		$("#gallery_popup .gallery_big_image img").attr("src",$(this).attr("rel"));
	})
	
	$(".gallery_block_wrap a").click(function(e) {
		$('#gallery_popup').lightbox_me({
			centered: true,
			closeSelector: ".gallery_close",
			defaultPosition: true
			});
		$("#gallery_popup .gallery_big_image img").attr("src",$(this).attr("href"));
		
		e.preventDefault();
	});
	$("#gallery_popup .gallery_big_image img").click(function(){
		var this_src=$(this).attr("src");
		var this_link = $("a[rel='"+this_src+"']").parent("li");
		if ($("#gallery_carousel li:last-child").get(0)===this_link.get(0))
			$(this).attr("src",$("#gallery_carousel li:first-child a").attr("rel"));
		else
			$(this).attr("src",this_link.next().children("a").attr("rel"));
	})
});