jQuery(function($){

	$('a.showmess').bind('click', function(e){
		var mess_id = $(e.target).attr('id');
		e.preventDefault();
		show_modal_id('#mwindow', 'smws', '<span>Текст письма</span>', '', mess_id, '20%', function(){
			}
		);
	});
	
	$('a.change').bind('click', function(e){
		var labl = $(e.target);
		var obj_id = labl.attr('id');
		e.preventDefault();
		show_modal('#mwindow', 'ocs', '<span>Изменить статус</span>', '', '20%', function(){
			var RadioChecked_obj = $('input[type=radio][name=stat]:checked');
			var new_stat = RadioChecked_obj.attr('alt');
			var id_stat = RadioChecked_obj.val();
			labl.html(new_stat);
			if (typeof(new_stat) != 'undefined'){
				$.ajax({
					url: "/ajaxadmins/change_user_state",
					data: {id: obj_id, stat: id_stat},
					type: 'post',
					success: function(data){}
				});
			}
		});
	});
});

function show_modal(box, container, header, message, top, callback){
	$(box).modal({
		closeHTML: '<a href="#" title="Закрыть" class="x-close">x</a>',
		position: [top,],
		opacity: 30,
		overlayId: 'overlay',
		containerId: container,
		closeClass: 'close',
		onShow: function (dialog){
			var modal = this;

			$('.header', dialog.data[0]).append(header);
			$('.message', dialog.data[0]).append(message);

			$('.yes', dialog.data[0]).click(function () {
				if ($.isFunction(callback)) {
					callback.apply();
				}
				modal.close();
			});
		}
	});
}
function show_modal_id(box, container, header, message, mail_id, top, callback){
	$(box).modal({
		closeHTML: '<a href="#" title="Закрыть" class="x-close">x</a>',
		position: [top,],
		opacity: 30,
		overlayId: 'overlay',
		containerId: container,
		closeClass: 'close',
		onShow: function (dialog){
			var modal = this;
			$.ajax({
				url: "/ajaxadmins/showmail",
				data: {id: mail_id},
				dataType: 'html',
				type: 'post',
				success: function(data){
					$('.message', dialog.data[0]).html(data);
				}
			});

			$('.header', dialog.data[0]).append(header);
			//$('.message', dialog.data[0]).append(message);

			$('.yes', dialog.data[0]).click(function () {
				if ($.isFunction(callback)) {
					callback.apply();
				}
				modal.close();
			});
		}
	});
}